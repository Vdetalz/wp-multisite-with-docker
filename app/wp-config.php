<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
///** The name of the database for WordPress */
define('DB_NAME', 'mctdocker_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '0.0.0.0:3309');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v[ 4l2Z]6*xI6gIKRhv+~} U#uOb+AP>T=yJ[lE(+MDxp{5xA|!v2oE,(qDq#tk3');
define('SECURE_AUTH_KEY',  'L|:#@zR4f86-Q;D anG7AfmTY:tyYm?7N7PdTDFX4i*-JsHk[J9P`}iMU^zQFc&U');
define('LOGGED_IN_KEY',    'oO.Z>xyxiXhQ_^Mmb2s4*cJ~}9LZsO0&<}-v<[-7{>5YH)A*s^yM*3=9FzpY?xk|');
define('NONCE_KEY',        'qirh_D#63nQp^ly<#xK{68x>D+z=tFDOq+UaW,W;*{Q*)=.`IEzcDB$i>&2 0Tq!');
define('AUTH_SALT',        '*9qv]H,YNCKx)L,0!#:-p;SfF*).P?m3Q}%ILeo<B u6(Bx^I!,Eak=k`xk;V9$-');
define('SECURE_AUTH_SALT', 'VZ2g$+z_N|CllAHo/(>WtaV{o+_oWo~[X=|[?}{3JhL78ta8>zy%X{f>e,+n~1yv');
define('LOGGED_IN_SALT',   '0{VKs)%J`u-D8xVkv6+8^a[xx+WKkkS<{+]4i2C|4vh`hn8~@>}W3;*(mV>||PC4');
define('NONCE_SALT',       'ke=y#9//^}.gnj[EN*(bqN:MgWg$50c.@QB>6S&$9]4=9Opsua5WGVBmJXl$V{vf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ch_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'mctdocker.loc');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define( 'SUNRISE', 'on' );
define( 'WP_SITEURL', 'http://mctdocker.loc' );
define( 'WP_HOME', 'http://mctdocker.loc' );
define('DISABLE_WP_CRON', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define('WP_ALLOW_MULTISITE', true);

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
