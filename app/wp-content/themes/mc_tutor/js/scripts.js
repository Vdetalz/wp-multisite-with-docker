(function ($, root, undefined) {
	
    $(function () {
		
		'use strict';
		
		// DOM ready, take it away
             
            
        // change courses description on digital products
        $('.courses-list').on('change', function(){
            var id= $(this).val();
            $('.course-description').html($('.description-' + id).html());
        });
        var config = {
            'select'            : {disable_search_threshold:10}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        /*** chosen pricing  */
        $('select[name=pricing_table_switcher]').change(function() {
            $('select[name=pricing_table_switcher] option[value=' + $(this).val() + ']').prop('selected', 'true');
            $("select[name=pricing_table_switcher]").trigger("chosen:updated");
        });

        var $w = $(window),
            $headerContentWrapper = $('#header .content-wrapper'),
            $rightNav = $('#header .right-menu'),
            $mainNav = $('#header .main-menu .menu'),
            $mobileNav = $('<div />', {
                'class' : 'b-mobile-nav'
            });


        function toggleMobNav(isMobile) {
            if(isMobile) {
                $mainNav.wrap($mobileNav);
                $('#header').find('.b-mobile-nav').append($rightNav);
                $('.menu-item-has-children .sub-menu').slideUp();
                $('.nav-btn').show();
            } else {
                $rightNav.appendTo($headerContentWrapper);
                $mainNav.unwrap();
                $('.menu-item-has-children .sub-menu').slideDown();
                $('.nav .nav-btn').removeClass('active');
                $('.nav .nav-btn').hide();
            }
        }

        if ($(window).width() < 1000) {
            toggleMobNav(true);
        }

        $w.resize(function() {
            if ($(window).width() < 1000) {
                if ($('#header').find('.b-mobile-nav').length === 0) {
                    toggleMobNav(true);
                }
            } else {
                if ($('#header').find('.b-mobile-nav').length > 0) {
                    toggleMobNav(false);
                }
            }
            1
            $('form.date-time-inputs #tutor-date').datepicker("hide");
        });

        /**  event to show main nav on mobile*/
        $('.nav .nav-btn').on('click', function(){
            $(this).toggleClass('active');
            $(this).next('.b-mobile-nav').toggleClass('is-open');
        });
        $('body').on('click', '.b-mobile-nav .menu-item-has-children a', mobileNavEvents());

        function mobileNavEvents() {
            return function (e) {
                
                if (!$(this).parents('.menu-item-has-children:first').hasClass('active')) {
                    e.preventDefault();
                    $(".sub-menu .sub-menu").slideUp();
                    $('.menu-item-has-children').removeClass('active');
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                } else {
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                }
                
            }
        }

        /** Double tap on logo */
        $( '#header .item-logo').on('touchstart', function(e) {
            e.stopPropagation();
            if(!$(this).next('ul').hasClass('is-open')) {
                e.preventDefault();
                if($('.b-mobile-nav').hasClass('is-open')) {
                    $('.nav .nav-btn').trigger('click');
                }
                $(this).next('ul').addClass('is-open');
            }
        });
        $('html').on('touchstart', function() {
            $( '#header .item-logo').next('ul').removeClass('is-open');
        });

        /**     Explore courses */
        $('.js-goToThis').click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop : $($(this).attr('href')).offset().top - 100
            }, 300);
        });

        /**     Calendar  scripts   */

            /**     accordions  */
        $('body').on('click', '.js-cal-time-toggle', function(e) {
            e.preventDefault();
            $(this).find('.e-toggle-arrow').toggleClass('fa-rotate-90');
            $(this).parents('.cl-time-block').find('.cl-time-block-bottom').slideToggle();
            $(this).parents('.cl-time-block').toggleClass('is-open');
        });
        
        //Tutor page filter datepicker
        $('form.date-time-inputs #tutor-date').datepicker();
        $('form.date-time-inputs #tutor-date').datepicker('setDate', new Date());
        
        //User account late lesson cancel popup 
        $('.account-page .bookings_list a.e-cancel').click(function(e){
            var date = new Date;
            if(Math.floor(date.getTime() / 1000) > ($(this).data('time') - 12 * 60 * 60)){
                $('#late-lesson-cancel-yes').attr('href', $(this).attr('href'));
                $.magnificPopup.open({
                    items: {
                        src: $(this).data('mfp-src'),
                        type: 'inline'
                    }
                });
                e.preventDefault();
            }
        });
        
        $('#late-cancel-popup #late-lesson-cancel-no').click(function(e){
            e.preventDefault();
            $.magnificPopup.instance.close();
        });

        var errorHeight = $(".affwp-errors").outerHeight() + 30;
        $('.affiliate-area .status-publish').css("padding-top", errorHeight);
        
        
        //Tutors filter within 12 hours popup
        $('form#show-available-tutors').submit(function(e){
            var tutorDate = $(this).find('#tutor-date').val();
            var tutorTime = $(this).find('#tutor-time option:selected').attr('value');
            var date = new Date;
            var currentDateTime = date.getTime();
            var requestedDateTime = new Date(tutorDate + ' ' + tutorTime).getTime();
            if(currentDateTime + 12 * 60 * 60 * 1000 > requestedDateTime){
                $.magnificPopup.open({
                    items: {
                        src: $(this).data('mfp-src'),
                        type: 'inline'
                    }
                });
                e.preventDefault(); 
            }
        });
        
        $('#close-too-early').click(function(){
            $.magnificPopup.instance.close();
        });

    });
	
})(jQuery, this);
