(function($){

    /**
     * Preloader function
     * @param elem
     * @returns {*}
     */

    function toggleLoader(elem) {
        if(elem) {
            elem.remove();
        } else {
            return jQuery('<div />', {
                'class': 'site-loader'
            }).appendTo(jQuery('body'));
        }
    }

    /**
     * Ajax Upload calendat
     */

    function updateCalendar(){

        var calendar_id = $('.tutor-calendar').attr('data-calendar-id');
        var loader = toggleLoader();

        $('.step-2').slideUp(500);
        $('.tutor-calendar').slideUp(500);


        $('.show-step-2').removeClass('active');

        jQuery.ajax({
            type: 'POST',
            url: EM.ajaxurl,
            data:  {
                action: 'get_tutor_calendar',
                action: 'get_tutor_calendar',
                author_id: calendar_id
            },
            success: function(data){
                jQuery('.tutor-calendar').html(data).slideDown(500);
                $('.step-1').slideDown(500);
                $('.show-step-1').addClass('active');
                toggleLoader(loader);
            }
        })

    }

    $(document).ready(function(){


        $(document).on('click', '.show-step-1, .show-step-2, .add-booking', function(){

            if($(this).hasClass('show-step-1')) {

            }

            var event_date = $(this).attr('data-event-date');
            var event_id = $(this).attr('data-event-id');
            var event_time = $(this).html();

            if(event_date && event_id && event_time){
                $('.event-datetime .lesson-date').html(event_date);
                $('.event-datetime .lesson-time').html(event_time);
                $('input[name=event_id]').val(event_id)
            }

            if($(this).hasClass('show-step-1')){
                $('.message').html('');
                updateCalendar();
            }
            else if(event_date && event_id && event_time){
                $('.step-1').slideUp(500);
                $('.step-2').slideDown(500);
                $('.show-step-1').removeClass('active');
                $('.show-step-2').addClass('active');
            }
            return false;
        });

        $('.book_lesson').click(function(){
            var loader = toggleLoader();
            var student_id = $('.student_id').val();
            var communication_type = $('.communication_type').val();
            var booker_comment = $('.booker-comment').val();

            jQuery.ajax({
                type: 'POST',
                url: EM.ajaxurl,
                data:  {
                    action: 'add_booking',
                    event_id: jQuery('.event_id').val(),
                    student_id: student_id,
                    comment: booker_comment,
                    communication_type: communication_type
                },
                success: function(data){
                    var patt = /(http|http)/;
                    if(data.substr(0, 5).match(patt)) {
                        location.href = data;
                        toggleLoader(loader);
                    } else {
                        jQuery('.step-2').slideUp(500);
                        $('.booking-message').html(data);
                        toggleLoader(loader);
                    }
                }
            });
        })

    })

})(jQuery);
