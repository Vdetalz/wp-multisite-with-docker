var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    videoId: video_id,
    height: '850',
    width: '1500',
    playerVars: {
      autoplay: 1,
      loop: 1,
      playlist: video_id,
      fs: 0,
      controls: 0,
      modestbranding: 1,
      rel: 0,
      showinfo: 0,
      autohide: 1,
      iv_load_policy: 3,
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}
function onPlayerReady(){
    player.mute();
}
function onPlayerStateChange(){
}