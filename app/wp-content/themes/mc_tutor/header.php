<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>
        <!--[if IE]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" media="all">
        <![endif]-->
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

    </head>
    <body <?php body_class(); ?>>
    <?php
        global $post, $edd_receipt_args, $edd_options;

        if ($post->ID == 7) {
	    $user_id = get_current_user_id();
	    $tutor_purchase_id = get_user_meta($user_id, 'tutor_purchase', true);
	    $hub_purchase_id = get_user_meta($user_id, 'hub_purchase', true);

	    $tutor_payment = get_post($tutor_purchase_id);
	    $tutor_price = edd_get_payment_subtotal($tutor_payment->ID);
	    switch_to_blog(5);
	    $hub_payment = get_post($hub_purchase_id);
	    $hub_price = edd_get_payment_subtotal($hub_payment->ID);
	    restore_current_blog();
	    $total = $tutor_price + $hub_price;
	    $currency = edd_get_currency();
	    ?>
	    <script>(function () {
			    var _fbq = window._fbq || (window._fbq = []);
			    if (!_fbq.loaded) {
				    var fbds = document.createElement('script');
				    fbds.async = true;
				    fbds.src = '//connect.facebook.net/en_US/fbds.js';
				    var s = document.getElementsByTagName('script')[0];
				    s.parentNode.insertBefore(fbds, s);
				    _fbq.loaded = true;
			    }
			    _fbq.push(['addPixelId', '650354991761995']);
		    })();
		    window._fbq = window._fbq || [];
		    window._fbq.push(['track', 'PixelInitialized', {}]);
	    </script>
	    <noscript><img height="1" width="1" alt="" style="display:none"
	                   src="https://www.facebook.com/tr?id=650354991761995&amp;ev=PixelInitialized"/></noscript>

	    <!-- Facebook Conversion Code for Checkouts - Sam Dean -->
	    <script>(function () {
			    var _fbq = window._fbq || (window._fbq = []);
			    if (!_fbq.loaded) {
				    var fbds = document.createElement('script');
				    fbds.async = true;
				    fbds.src = '//connect.facebook.net/en_US/fbds.js';
				    var s = document.getElementsByTagName('script')[0];
				    s.parentNode.insertBefore(fbds, s);
				    _fbq.loaded = true;
			    }
		    })();
		    window._fbq = window._fbq || [];
		    window._fbq.push(['track', '6025101624579', {'value': '<?php echo $total; ?>', 'currency': '<?php echo $currency; ?>'}]);
	    </script>
	    <noscript><img height="1" width="1" alt="" style="display:none"
	                   src="https://www.facebook.com/tr?ev=6025101624579&amp;cd[value]=<?php echo $total; ?>&amp;cd[currency]=<?php echo $currency; ?>&amp;noscript=1"/>
	    </noscript>

    <?php } else { ?>
	    <script>(function () {
			    var _fbq = window._fbq || (window._fbq = []);
			    if (!_fbq.loaded) {
				    var fbds = document.createElement('script');
				    fbds.async = true;
				    fbds.src = '//connect.facebook.net/en_US/fbds.js';
				    var s = document.getElementsByTagName('script')[0];
				    s.parentNode.insertBefore(fbds, s);
				    _fbq.loaded = true;
			    }
			    _fbq.push(['addPixelId', '650354991761995']);
		    })();
		    window._fbq = window._fbq || [];
		    window._fbq.push(['track', 'PixelInitialized', {}]);
	    </script>
	    <noscript><img height="1" width="1" alt="" style="display:none"
	                   src="https://www.facebook.com/tr?id=650354991761995&amp;ev=PixelInitialized"/></noscript>
    <?php } ?>

    <!-- wrapper -->
    <div class="wrapper">
                <div class="inner-wrapper">
                    <!-- header -->
                    <header id="header" class="header clear" role="banner">
                        <div class="content-wrapper">
                            <!-- logo -->
                            <div class="logo">
                                <?php  $sites = wp_get_sites();?>
                                <ul>
                                    <li>
	                                    <a class="item-logo" href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" ><?php echo get_bloginfo( 'name' ); ?></a>
                                        <?php if( !empty($sites) ):?>
                                            <ul>
                                                <?php foreach( $sites as $site ): ?>
                                                    <?php
                                                    switch_to_blog( $site['blog_id'] );
                                                    $site_title = get_bloginfo( 'name' );
                                                    $site_url = get_bloginfo( 'url' );
                                                    restore_current_blog();
                                                    ?>
                                                    <?php if( get_current_blog_id() <> $site['blog_id'] ):?>
                                                            <li><a href="<?php echo $site_url; ?>"  rel="nofollow"><?php echo $site_title; ?></a></li>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </ul>
                                        <?php endif;?>
                                    </li>
                                </ul>
                            </div>
                            <!-- /logo -->

                            <!-- nav -->
                            <?php if(is_user_logged_in()): ?>
                                <nav class="nav main-menu log-in" role="navigation">
                                    <span class="nav-btn">Open</span>
                                    <?php
                                        wp_nav_menu(
                                            array(
                                                'theme_location'  => 'header-menu-loginned',
                                                'container_class' => 'menu-{menu slug}-container',
                                                'container_id'    => '',
                                                'menu_class'      => 'menu',
                                                'walker'          => new t_Walker_Nav_Menu(),
                                            )
                                        );
                                    ?>
                                </nav>&nbsp;
                                <div class="right-menu log-in">
                                    <?php
                                        wp_nav_menu(
                                            array(
                                                'theme_location'  => 'right-header-menu-loginned',
                                                'container_class' => 'menu-{menu slug}-container',
                                                'items_wrap'      => '<ul>%3$s</ul>',
                                                'walker'          => new t_Walker_Nav_Menu(),
                                            )
                                        );
                                    ?>
                                </div>
                            <?php else: ?>
                                <nav class="nav main-menu" role="navigation">
                                    <span class="nav-btn">Open</span>
                                    <?php
                                        wp_nav_menu(
                                            array(
                                                'theme_location'  => 'header-menu-unloginned',
                                                'container_class' => 'menu-{menu slug}-container',
                                                'walker'          => new t_Walker_Nav_Menu(),
                                            )
                                        );
                                    ?>
                                </nav>&nbsp;
                                <div class="right-menu">
                                    <?php
                                        wp_nav_menu(
                                            array(
                                                'theme_location'  => 'right-header-menu-unloginned',
                                                'container_class' => 'menu-{menu slug}-container',
                                            )
                                        );
                                    ?>
                                </div>
                            <?php endif; ?>
                            <!-- /nav -->
                        </div>
                    </header>
                    <!-- /header -->
