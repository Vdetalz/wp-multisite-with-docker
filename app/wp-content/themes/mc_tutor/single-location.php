<?php
get_header();
$date = dut_get_current_user_timestamp();
$user_id = get_current_user_id();
?>
<section role="main" class="main-content content-wrapper">
    <!-- section -->
    <section>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <!--SELECT TIME / STEP 1-->
        <div class="page-back-link">
            <a href="<?php echo get_permalink(79);?>">< <?php _e('Back to all Tutors');?></a>
        </div>

        <h1><?php _e('BOOK A LESSON');?></h1>
        <div class="loc-step-title">
            <a href="#" class="show-step-1 active">1. <?php _e('SELECT TIME');?></a>
        </div>
        <!--Tutor info block wrapper-->

        <div class="loc-info-wrapper step-1">
            <div class="book_lessons_top">
                <!--Tutor Avatar, info-->
                <div class="book_lessons_profile_wrap">
                    <?php the_post_thumbnail(get_the_ID()); ?>
                    <div class="book_lessons_profile_title"><?php the_title();?></div>
                    <div class="book_lessons_profile_tags">
                        <div class="modal_lesson_teacher_time"><?php echo get_field('tutor-lesson-type'); ?> mins</div>
                        <div><?php echo count(get_field('tutor-skype-or-polycom')) ? implode(', ', get_field('tutor-skype-or-polycom')) : '';?></div>
                    </div>
                </div>
                <!--end of tutor avatar, info-->

                <!-- TIMENOW -->
                <div class="bl_timenow_wrap">
                    <div class="bl_timenow_days">
                        <div><?php echo date('d', $date);?></div>
                        <div><?php echo date('M', $date);?></div>
                    </div>
                    <div class="bl_timenow_time">
                        <?php echo date('h:i A', $date);?>
                    </div>
                </div>
                <!-- /TIMENOW -->
            </div>
            <div class="tutor-calendar" id="tutor-calendar-<?php echo get_the_ID();?>" data-calendar-id="<?php echo get_the_author_ID();?>">
                <?php echo do_shortcode("[mct_booking_calendar author_id=" . get_the_author_ID() . "]");?>
            </div>
        </div>

        <!--End of tutor info block wrapper-->

        <!--END OF STEP 1-->

        <!--START STEP 2-->

        <div class="loc-step-title">
            <a href="#" class="show-step-2">2. <?php _e('CONFIRM BOOKING');?></a>
        </div>
        <div class="loc-info-wrapper step-2" style="display:none;">
            <div class="bl_form_feedback_wrap">
                <div class="bl_text_info">
                    <?php if(get_user_meta( $user_id, 'tutor_trial_lesson', true )):?>
                        <div class="test_lesson_text">
                            <?php _e("This is a 30 minute trial lesson with", 'mct_tutor'); ?>
                            <span> <?php echo get_the_title(); ?></span>.
                            <?php _e("This is to make sure you and the tutor can see and hear each other properly before having your first real lesson", 'mct_tutor'); ?>.
                            <span> <?php echo get_the_title(); ?></span>
                            <?php _e("will contact you at the allotted time", 'mct_tutor'); ?>.
                        </div>
                        <div class="second_lesson_text" style="display:none;">
                            <?php _e("You’re about to book a", "mct_tutor"); ?>
                            <span><?php echo get_field('tutor-lesson-type');;?><?php _e("mins", "mct_tutor"); ?> </span>
                            <?php _e("lesson with", "mct_tutor"); ?> <span><?php echo get_the_title(); ?></span>
                            <?php _e(". If this is your first booked lesson with ", "mct_tutor"); ?>
                            <?php echo get_the_title(); ?><?php _e(", then ", "mct_tutor"); ?>
                            <?php echo get_the_title(); ?><?php _e(" will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time.", "mct_tutor"); ?>
                        </div>
                    <?php else:?>
                        <?php _e("You’re about to book a", 'mct_tutor'); ?>
                        <span><?php echo get_field('tutor-lesson-type');;?> <?php _e('mins', 'mct_tutor') ?></span>
                        <?php _e('lesson with ', 'mct_tutor'); ?>
                        <span><?php echo get_the_title(); ?></span>.
                        <?php _e("If this is your first booked lesson with", 'mct_tutor'); ?>
                        <?php echo get_the_title(); ?>, <?php _e('then ', 'mct_tutor'); ?>
                        <?php echo get_the_title(); ?> <?php _e("will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time", 'mct_tutor'); ?>.
                    <?php endif;?>
                </div>
                <div class="bl_text_info">
                    <?php _e("Please note that the start time has been adjusted to your local time through your computer's clock", 'mct_tutor'); ?>.
                </div>

            <!--Tutor's block info, current datetime block-->
                <div class="bl_form">
                    <form method="post">
                        <div class="bl_form_feedback_left">

                            <div class="bl_profile_info">
                                <div class="bl_feedback_timebox_wrap event-datetime">
                                    <span class="lesson-date"></span>,
                                    <span class="lesson-time"></span>
                                </div>
                            </div>

                            <div class="bl_form_profile">
                                <div class="bl_form_profile_photo">
                                    <?php echo get_the_post_thumbnail(get_the_ID(), array(90,90)); ?>
                                </div>
                                <div class="bl_form_profile_name"><?php the_title();?></div>
                            </div>

                        </div>

                        <input type='hidden' name='event_id' class="event_id" value=''>
                        <div class="bl_feedback_timebox_links">
                        <?php if(is_super_admin(get_current_user_id())):
                            $args = array(
                                'role' => 'subscriber',
                                'orderby' => 'login',
                                'order' => 'ASC'
                            );
                            $users = get_users($args); ?>
                            <div>
                                <label>
                                    <?php _e('Select student', 'mct_tutor'); ?>:
                                </label>
                                <select name='student_id' class='student_id'>
                                    <option value=''><?php _e('Select student'); ?></option>
                                    <?php foreach($users as $user): ?>
                                        <option value="<?php echo $user->ID; ?>"><?php echo $user->user_login; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif; ?>
                            <div>
                                <label>
                                    <?php _e('Select communication type', 'mct_tutor'); ?>:
                                </label>
                                <select name="communication_type" class="communication_type">
                                    <option value="skype"><?php _e('Skype', 'mct_tutor'); ?></option>
                                    <option value="polycom"><?php _e('Polycom', 'mct_tutor'); ?></option>
                                </select>
                            </div>

                        </div>

                        <div class="bl_form_feedback_right">
                            <textarea class="booker-comment bl_feedback_textarea" name="bookerComment" placeholder="Add your comment here" ></textarea>
                        </div>
                        <div class="button_submit_feedback">
                            <button type="button" class="book_lesson e-btn m-btn-orange">
                                <?php _e('Confirm Booking', 'mct_tutor'); ?>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--END OF STEP 2-->

        <div class="booking-message"></div>

    </article>

<?php endwhile; ?>

<?php else: ?>

    <article>

        <h1><?php _e( 'Sorry, nothing to display.', 'mct_tutor' ); ?></h1>

    </article>

<?php endif; ?>

    </section>
    <!-- /section -->
</section>
<?php get_footer(); ?>