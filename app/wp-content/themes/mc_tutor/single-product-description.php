<?php get_header(); ?>

	<section role="main" class="single-p-d main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<div class="bg-page">
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				<?php endif; ?>
			</div>
			<!-- /post thumbnail -->

			<div class="content-wrapper">
				<?php
                    $cid = get_the_ID();

				 // get posts of "digital products description" content type

				$args = array(
					'post_type' => 'product-description',
					'posts_per_page' => -1
				);

				$descriptions_query = new WP_Query( $args );

				?>
				<?php if ( $descriptions_query->have_posts() ) : ?>
					<ul class="descriptions-list">
						<?php while ( $descriptions_query->have_posts() ) : $descriptions_query->the_post(); ?>
							<li <?php if($cid == get_the_ID()):?> class="active"<?php endif;?>><a href="<?php the_permalink();?>"> <?php the_title();?></a></li>
						<?php endwhile;?>
					</ul>
				<?php endif; wp_reset_postdata();?>
				<div class="content-header">
					<!-- post title -->
					<h1>
						<?php the_title(); ?>
					</h1>
					<div class="contact-me">
                        <?php echo do_shortcode('[book_lesson]');?>
					</div>
					<!-- /post title -->
				</div>
				<div>
					<?php the_content(); // Dynamic Content ?>
                    <div class="contact-me m-ta-center">
                        <?php echo do_shortcode('[book_lesson]');?>
                    </div>
				</div>
			</div>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<div class="content-wrapper">
		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'mct_tutor' ); ?></h1>

		</article>
		<!-- /article -->
		</div>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</section>

<?php get_footer(); ?>
