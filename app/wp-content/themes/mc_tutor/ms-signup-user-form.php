<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<form id="setupform" method="post" action="<?php $template->the_action_url( 'register' ); ?>">
	<input type="hidden" name="action" value="register" />
	<input type="hidden" name="stage" value="validate-user-signup" />
	<?php do_action( 'signup_hidden_fields' ); ?>


	<input name="user_email" type="text" id="user_email<?php $template->the_instance(); ?>" value="<?php echo esc_attr( $user_email ); ?>" maxlength="200" placeholder="<?php _e( 'E-mail', 'mc_hub' ); ?>" /><br />
    <?php if ( $errmsg = $errors->get_error_message( 'user_email' ) ) { ?>
        <p class="error"><?php echo $errmsg; ?></p>
    <?php } ?>


	<input name="user_name" type="text" id="user_nickname<?php $template->the_instance(); ?>" value="<?php echo esc_attr( $user_name ); ?>" maxlength="60" placeholder="<?php _e( 'Nickname' ); ?>" /><br />
    <?php if ( $errmsg = $errors->get_error_message( 'user_name' ) ) { ?>
        <p class="error"><?php echo $errmsg; ?></p>
    <?php } ?>

	<?php if ( $errmsg = $errors->get_error_message( 'generic' ) ) { ?>
		<p class="error"><?php echo $errmsg; ?></p>
	<?php } ?>

	<?php do_action( 'signup_extra_fields', $errors ); ?>

	<p>
	<?php if ( $active_signup == 'blog' ) { ?>
		<input id="signupblog<?php $template->the_instance(); ?>" type="hidden" name="signup_for" value="blog" />
	<?php } elseif ( $active_signup == 'user' ) { ?>
		<input id="signupblog<?php $template->the_instance(); ?>" type="hidden" name="signup_for" value="user" />
	<?php } else { ?>
		<input id="signupblog<?php $template->the_instance(); ?>" type="radio" name="signup_for" value="blog" <?php if ( ! isset( $_POST['signup_for'] ) || $_POST['signup_for'] == 'blog' ) { ?>checked="checked"<?php } ?> />
		<label class="checkbox" for="signupblog"><?php _e( 'Gimme a site!' ); ?></label>
		<br />
		<input id="signupuser<?php $template->the_instance(); ?>" type="radio" name="signup_for" value="user" <?php if ( isset( $_POST['signup_for'] ) && $_POST['signup_for'] == 'user' ) { ?>checked="checked"<?php } ?> />
		<label class="checkbox" for="signupuser"><?php _e( 'Just a username, please.' ); ?></label>
	<?php } ?>
	</p>

	<p class="submit"><input class="e-btn e-btn-submit" type="submit" name="submit" class="submit" value="<?php esc_attr_e( 'Submit' ); ?>" /></p>
</form>
