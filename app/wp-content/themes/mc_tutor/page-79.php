<?php
global $am_option, $wpdb; get_header();

$date = dut_get_current_user_timestamp();
$user_id = get_current_user_id();
?>

<section role="main" class="main-content content-wrapper">
    <section>
        <h1><?php _e('Chinese Language Tutors') ?></h1>
        <?php if(!is_user_logged_in()): ?>
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php the_content(); ?>
            </article>
            <?php endwhile; endif; ?>
        <?php else: ?>
            <?php
            $banner = get_option('mc_tutor_baner');
            if(!empty($banner)): ?>
                <p class="banner"><?php echo $banner; ?></p>
            <?php endif; ?>
            <form method="get" id="show-available-tutors" data-mfp-src=#too-early action="" class="date-time-inputs">
                <p><?php _e('Find the tutors available at your preferred time:', 'mct'); ?></p>    
                <div class="tutor-date">
                    <input type="text" id="tutor-date" name="tutor-date" />    
                </div>
                <select name="tutor-time" id="tutor-time">
                    <?php for($i = 0; $i < 24; $i++): ?>
                        <?php for($j = 0; $j < 2; $j++): ?>
                            <?php $minutes =  $j ? ':30' : ':00'; ?>                        
                            <option value="<?php echo  $i . $minutes; ?>"><?php echo ($i < 12) ? $i . $minutes . ' AM' : $i - 12 . $minutes . ' PM'  ?></option>
                        <?php endfor;?>
                    <?php endfor;?>
                </select>
                <input type="submit" value="<?php _e('Show me the available tutors!', 'mct'); ?>" />
            </form>

            <div class="white-popup-block mfp-hide" id="too-early">
                <div class="too-early-content">
                    <p><?php _e('You have selected a time within 12 hours from now! We like to give our tutors more warning of a future lesson,'
                            . ' so please enter a preferred time more than 12 hours from now. Thanks for your understanding!', 'mct') ?></p>
                    <input type="button" class="e-btn" id="close-too-early" value="<?php _e('Okay!', 'mct'); ?>">
                </div>
            </div>
        <?php endif; ?>
<?php

$location_id = get_sorted_teachers();

$args = array(
    'role' => 'subscriber',
    'orderby' => 'login',
    'order' => 'ASC',
);

if( is_super_admin() ) {
    $users = get_mct_tutor_users();
} else {
    $users = get_users($args);
}

//Tutors Filter callback.
if($filter_ids = tutors_filter()){
    $location_id = $filter_ids;
}


$tutors = new WP_Query(array(
    'post_type' => 'location',
    'posts_per_page' => -1,
    'post__in' => $location_id,
    'orderby' => 'post__in',
    'order' => 'DESC'
));

/**
 *  SELECT EL.location_owner, COUNT(EV.event_id) FROM ch_em_locations AS EL LEFT JOIN ch_em_events AS EV ON EV.event_owner = EL.location_owner
    WHERE CONCAT(EV.event_start_date, ' ', EV.event_start_time ) >= (NOW() + INTERVAL 12 HOUR) AND EV.event_status = 1
    GROUP BY EV.event_owner ORDER BY COUNT(*) DESC
 */


?>
        <!-- EXPERT PANEL -->
        <div class="b-inline-blocks-wrap m-tutors-list">
<?php
if($tutors->have_posts()):
    while($tutors->have_posts()): $tutors->the_post(); ?>


            <div class="b-inline-block m-column-2">
                <div class="b-inline-block-content b-tutor-card">
                    <div class="b-tutor-photo">
                        <?php  echo get_the_post_thumbnail(get_the_ID(), array(211,257)); ?>
                    </div>
                    <div class="b-tutor-info">
                        <div class="e-title" ><?php echo get_the_title(); ?></div>
                        <div class="e-text">
                            <?php echo get_the_content(); ?>
                        </div>
                        <ul class="e-info-list">
                            <li>
                                <?php _e("Lesson length", 'mct_tutor'); ?>: <span><?php echo get_field('tutor-lesson-type'); ?> mins</span>
                            </li>

                            <?php $communications  = get_field('tutor-skype-or-polycom');
                            $count = count($communications);
                            $types_of_commun_span = '';
                            $types_of_commun = '';
                            for($i = 0; $i < $count; $i++) {
                                $types_of_commun_span .= "<span>" . ucfirst($communications[$i]) . "</span>";
                                $types_of_commun .= ucfirst($communications[$i]);
                                if( ($i !== $count - 1) ) {
                                    $types_of_commun_span .= " and ";
                                    $types_of_commun .= ", ";
                                }
                            } ?>
                            <li>
                                <?php _e("Lesson via", 'mct_tutor'); ?>:  <?php echo $types_of_commun_span; ?>
                            </li>
                        </ul>
                        <?php
                        $class = '';
                        if(is_user_logged_in()){
                            if(get_user_meta($user_id, 'tutor_trial_lesson', true)){
                                $text = __('Book $1 Demo Lesson');
                                $class = 'free_lesson';
                            } else{
                                $text = __('Book Lesson');
                            }
                        } else{
                            $text = __('Book $1 Demo Lesson');
                            $class = 'expert_free_lesson';
                        }

                        $location = EM_Locations::get(array('post_id' => get_the_ID()));
                        ?>
                        <div class="b-action-wrap">
                            <?php if ($class === 'expert_free_lesson'): ?>
                                <a href="<?php echo wp_registration_url(Theme_My_Login_Common::get_current_url()); ?>"
                                   class="<?php echo $class; ?> e-btn m-btn-orange"><?php echo $text ?></a>
                            <?php else: ?>
                                <?php if (get_user_balance(get_current_user_id()) < 0.5 && mct_get_current_user_role() != 'Author'): ?>
                                    <a href="" class="e-btn m-btn-orange"
                                       onclick="showNoBalancePopup(); return false;"><?php echo $text; ?></a>
                                <?php elseif (wp_is_mobile()): ?>
                                    <a href="<?php echo get_the_permalink($location[0]->ID); ?>"
                                       class="<?php echo $class; ?> e-btn m-btn-orange"><?php echo $text; ?></a>
                                <?php
                                else: ?>
                                    <a href="" class="<?php echo $class; ?> e-btn m-btn-orange"
                                       onclick="showBookPopup(this, '<?php echo get_the_ID(); ?>', '<?php echo $location[0]->post_author; ?>'); return false;"><?php echo $text; ?></a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /EXPERT -->
            <div id="tutor-info-<?php echo get_the_ID(); ?>" style="display:none;">
                <?php  echo get_the_post_thumbnail(get_the_ID(), array(90,90)); ?>
                <div class="book_lessons_profile_title"><?php echo get_the_title(); ?></div>
                <div class="book_lessons_profile_tags">
                    <div class="modal_lesson_teacher_time"><?php echo get_field('tutor-lesson-type'); ?> mins</div>
                    <div><?php echo $types_of_commun; ?></div>
                </div>
            </div>

        <!-- MODAL BOOK LESSONS -->


        <div class="mfp-hide js-shift" id="modal-wrap-<?php echo get_the_ID();?>">

            <h2 class="modal_title">Book a lesson</h2>
            <input type="hidden" name="author_id"/>
            <input type="hidden" name="post_id"/>

            <!-- STEPS TABS WRAP -->
            <div class="steps_tabs_wrap">
                <div class="steps_tab active">
                    <?php _e('1. Select time', 'mct_tutor'); ?>
                </div>
                <div class="steps_tab">
                    <?php _e('2. Confirm booking', 'mct_tutor') ?>
                </div>
            </div>
            <!-- /STEPS TABS WRAP -->

            <!-- STEPS BODY WRAP -->
            <div class="steps_body_wrap">
                <div class="step1-body steps_body active">
                    <div class="book_lessons_wrap clearfix">
                        <div class="tabletInfo">
                            <div class="book_lessons_profile_wrap tabletInfo-user"></div>
                            <div class="tabletInfo-dateTime">
                                <div class="tabletInfo-date">
                                    <?php echo date('d', $date);?>
                                    <?php echo date('M', $date);?>,
                                </div>
                                <div class="tabletInfo-time">
                                    <?php echo date('h:i A', $date);?>
                                </div>
                            </div>
                        </div>
                        <div class="book_lessons_col col2">

                            <!-- BOOK LESSONS TIMER -->
                            <div class="bl_wrap em-calendar-modal" id="tutor-calendar-<?php echo get_the_ID();?>"></div>
                            <!-- /BOOK LESSONS TIMER -->

                            <div class="bl_text_table">
                                <?php _e('* Available lesson times have been adjusted to the time zone set on your computer.', 'mct_tutor') ?>
                            </div>
                        </div>
                        <div class="book_lessons_col col1">
                            <div class="book_lessons_profile_wrap"></div>
                        </div>
                        <div class="book_lessons_col col3">

                                <!-- TIMENOW -->
                            <div class="bl_timenow_wrap">
                                <div class="bl_timenow_days">
                                    <div><?php echo date('d', $date);?></div>
                                    <div><?php echo date('M', $date);?></div>
                                </div>
                                <div class="bl_timenow_time">
                                    <?php echo date('h:i A', $date);?>
                                </div>

                            </div>
                            <!-- /TIMENOW -->

                        </div>
                    </div>
                </div>

                <div class="step2-body steps_body">
                    <div class="bl_form_feedback_wrap clearfix">
                        <div class="message bl_text_info_main"></div>
                        <div class="lesson_info">
                            <div class="load"></div>
                            <div class="bl_text_info">
                                <?php if(get_user_meta( $user_id, 'tutor_trial_lesson', true )):?>
                                    <div class="test_lesson_text"><?php _e("This is a 30 minute trial lesson with", 'mct_tutor'); ?> <span> <?php echo get_the_title(); ?></span>. <?php _e("This is to make sure you and the tutor can see and hear each other properly before having your first real lesson", 'mct_tutor'); ?>. <span> <?php echo get_the_title(); ?></span> <?php _e("will contact you at the allotted time", 'mct_tutor'); ?>.</div>
                                    <div class="second_lesson_text" style="display:none;"><?php _e("You’re about to book a", "mct_tutor"); ?> <span><?php echo get_field('tutor-lesson-type');;?><?php _e("mins", "mct_tutor"); ?> </span> <?php _e("lesson with", "mct_tutor"); ?> <span><?php echo get_the_title(); ?></span>.<?php _e(" If this is your first booked lesson with ", "mct_tutor"); ?> <?php echo 'Regina'; ?><?php _e(", then ", "mct_tutor"); ?><?php echo get_the_title(); ?><?php _e(" will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time.", "mct_tutor"); ?> </div>
                                <?php else:?>
                                    <?php _e("You’re about to book a", 'mct_tutor'); ?> <span><?php echo get_field('tutor-lesson-type');;?> <?php _e('mins', 'mct_tutor') ?></span> <?php _e('lesson with ', 'mct_tutor'); ?><span><?php echo get_the_title(); ?></span>.<?php _e(" If this is your first booked lesson with", 'mct_tutor'); ?> <?php echo get_the_title(); ?>, <?php _e('then ', 'mct_tutor'); ?><?php echo get_the_title(); ?> <?php _e("will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time", 'mct_tutor'); ?>.
                                <?php endif;?>
                            </div>
                            <div class="bl_text_info">
                                <?php _e("Please note that the start time has been adjusted to your local time through your computer's clock", 'mct_tutor'); ?>.
                            </div>

                            <div class="bl_form">
                                <form method="post">
                                    <div class="bl_form_feedback_left bl_form_column col1">

                                        <div class="bl_profile_info">
                                            <div class="bl_feedback_timebox_wrap">
                                                <span class="lesson-date"></span>,
                                                <span class="lesson-time"></span>
                                            </div>

                                            <div class="bl_feedback_timebox_links">
                                                <?php if(is_super_admin(get_current_user_id())): ?>
                                                <div>
                                                    <label>
                                                        <?php _e('Select student', 'mct_tutor'); ?>:
                                                    </label>
                                                    <select name='student_id' class='student_id'>
                                                        <option value=''><?php _e('Select student'); ?></option>
                                                        <?php foreach($users as $user): ?>
                                                            <option value="<?php echo $user->ID ; ?>" data-skype="<?php echo get_user_meta($user->ID, 'skype_name', true); ?>"><?php echo $user->user_login; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                    <?php endif; ?>
                                                <div>
                                                    <label>
                                                        <?php _e('Select communication type', 'mct_tutor'); ?>:
                                                    </label>
                                                    <select name="communication_type" class="communication_type">
                                                        <option value="skype"><?php _e('Skype', 'mct_tutor'); ?></option>
                                                        <option value="polycom"><?php _e('Polycom', 'mct_tutor'); ?></option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="bl_form_profile">
                                            <div class="bl_form_profile_photo">
                                                <?php  echo get_the_post_thumbnail(get_the_ID(), array(90,90)); ?>
                                            </div>
                                            <div class="bl_form_profile_name">
                                                <?php echo get_the_title(); ?>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="bl_form_feedback_right bl_form_column col2">
                                        <textarea class="booker-comment bl_feedback_textarea" name="bookerComment" placeholder="Add your comment here" ></textarea>
                                    </div>
                                    <div class="button_submit_feedback">
                                        <button type="button" class="book_lesson e-btn m-btn-orange">
                                            <?php _e('Confirm Booking', 'mct_tutor'); ?>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /STEPS BODY WRAP -->

        </div>


        <!-- /MODAL BOOK LESSONS -->

    <?php endwhile; ?>
<?php endif; ?>
        <?php if(is_super_admin($user_id)) {
            $skype_name = 'admin';
            $polycom_name = 'admin';
        } else {
            $skype_name = get_user_meta($user_id, 'skype_name', true);
            $polycom_name = get_user_meta($user_id, 'polycom_name', true);
        } ?>
        <input type="hidden" id="user_skype" name="user_skype" value="<?php echo $skype_name; ?>">
        <input type="hidden" id="user_polycom" name="user_polycom" value="<?php echo $polycom_name; ?>">
        <input type='hidden' name='event_id' class="event_id" value='' >
        <input type='hidden' name='communication_type' id="communication_type" value='' >
        <input type='hidden' name='booker_comment' id="booker_comment" value='' >
        <input type='hidden' name='student_id' id="student_id" value='' >
        <input type='hidden'  class="event_time" value='' >

        <div style="display: none">
            <div id="skype_form">
                <h3>Enter your skype name:</h3>
                <input type='text' id='skype_name' value=''>
                <input type="button" class="book_lesson e-btn m-btn-orange" value="Confirm booking">
            </div>
        </div>

        <div style="display: none">
            <div id="polycom_form">
                <h3>Enter your polycom number:</h3>
                <input type='text' id='polycom_name' value=''>
                <input type="button" class="book_lesson e-btn m-btn-orange" value="Confirm booking">
            </div>
        </div>

        <div style="display: none">
            <div class="no_balance">
                <div class="no-balance-popup">
                   <h3>
	                   <?php _e('Sorry, you have run out of lesson hours!', 'mct_tutor'); ?>
	                   <?php _e('Please subscribe to more hours to book more lessons', 'mct_tutor'); ?>
                   </h3>
                    <a href="<?php echo get_permalink(edd_get_option('mct_edd_pricing_page_redirect')); ?>" class="e-btn m-btn-orange">See subscription options</a>
                </div>
            </div>
        </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>
