<?php
/*
Template for login form
*/
?>
<div class="login login-form" id="theme-my-login<?php $template->the_instance(); ?>">
    <?php $template->the_action_template_message( 'login' ); ?>
    <?php $template->the_errors(); ?>
    <form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login' ); ?>" method="post">
        <div class="wrap-field-inputs">
            <div class="field-input">
               <input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'log' ); ?>" size="20" placeholder="<?php _e( 'E-mail', 'mc_hub' ); ?>" />
            </div>
            <div class="field-input">
                <input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" class="input" value="" size="20"  placeholder="<?php _e( 'Password' , 'mc_hub' ); ?>" />
            </div>
        </div>
        <?php $template->the_action_links( array( 'login' => false, 'register' => false ) ); ?>
        <?php do_action( 'login_form' ); ?>

        <p class="submit">
            <input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Log In' ); ?>" class="e-btn e-btn-login" />
            <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
            <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
            <input type="hidden" name="action" value="login" />
        </p>
    </form>

</div>
