function add_placeholder (id, placeholder){
    var el = document.getElementById(id);

    var isPassword = (el.type == 'password');
    if (isPassword)
    {
        var passwordValue = el.value;
        var passwordLabel = document.createElement('label');
        passwordLabel.innerHTML = placeholder;
        if (id == 'verify_password'){
            passwordLabel.className = 'placeholderLabelVerify';
        }
        else{
            passwordLabel.className = 'placeholderLabel';
        }
        passwordLabel.htmlFor = id;
        var parent = el.parentNode;
        var insertedElement = parent.insertBefore(passwordLabel, el);
    }

    el.placeholder = placeholder;

    el.onfocus = function ()
    {
        if (isPassword) {
            passwordLabel.style.display = 'none';
        }
        else if(this.value == this.placeholder)
        {
            this.value = '';
            el.style.cssText  = '';
        }
    };

    el.onblur = function ()
    {
        if(this.value.length == 0)
        {
            if (isPassword)
            {
                passwordLabel.style.display = 'inline-block';
                passwordLabel.innerHTML = this.placeholder;
            }
            else
            {
                this.value = this.placeholder;
                el.style.cssText = 'color:#bdbdbd;';
            }
        }
    };

    el.onblur();
}

jQuery('document').ready(function(){
    add_placeholder('loginPassword', 'Your Password');
    add_placeholder('reg_password', 'Your Password');
    add_placeholder('verify_password', 'Verify Password');
})
