<?php
/*
Template Name: My account
*/
?>

 <?php  global $am_option, $current_user;  get_header(); ?>
    <?php if(!empty($am_option['main']['note_my_account'])) :?>
        <div class="page_note_wrap">
            <div class="page_note_inner">
                <div class="page_note_close"></div>
                <?php echo $am_option['main']['note_my_account'];?>
            </div>
        </div>

    <?php endif;?>

<div class="content_wrap">
	<div class="content_big_inner">
        <?php if (is_user_logged_in()) :?>
		<div class="profile_form_fix">
			<div class="profile_form_wrap">
				<div class="profile_form_title">
					Hello <span><?php echo do_shortcode('[userinfo field="nickname"]'); ?> </span>, here are your account details:
				</div>

                <?php echo do_shortcode('[edit_profile_form]');?>
			</div>
		</div>
		
		<div class="credit_block_wrap">
			<span class="credit_block_text">
				Currently you have <span class="blue2"><?php echo $current_user_credits; ?></span> credits available.	
			</span>
			<div class="credit_block_button">Add more credits</div>
		</div>
					

		<div class="booked_title">
			You are currently booked for:
		</div>

		<div class="booked_table">
			<?php echo do_shortcode('[events_list pupil=1 scope=future user_registered_only=1 nilresults="You are not currently booked for any lessons."]');?>
		</div>
        <?php echo do_shortcode('[student_progress]');?>
		<div class="clear"></div>
		<div class="booked_footer">
			Help us get better. Provide service feedback!
		</div>

		<div class="booked_button_wrap">
			<div class="booked_button">Send feedback</div>
		</div>

	</div>
</div>


            <!-- MODAL FEEDBACK -->
                <?php echo do_shortcode('[modal_feedback]');?>
			<!-- /MODAL FEEDBACK -->
			

    <?php else:?>
    <div class="profile_form_fix">
        <div class="profile_form_wrap">
           <div class="profile_form_title">
                <p>Please login to see this page</p>
           </div>
        </div>
    </div>

    <?php endif;?>
			<!-- /MODAL CREDIT -->
<?php 
	get_footer(); 
?>