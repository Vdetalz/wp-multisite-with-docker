<?php global $am_option; get_header(); ?><?php    $result = $wpdb->get_results("SELECT * FROM `fmanager` ORDER BY `number` ASC, `level` ASC");    $str = explode('wp-admin/admin.php', $_SERVER['SCRIPT_URI']);    	$uploaddir = $str[0] . 'wp-content/uploads/lessons/'; 	$b=0;$bi=0;$in=0;$ia=0;$a=0;?>
<div class="content_wrap">
<div class="content_big_inner">
<div class="content_text_two">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</div>
<div class="content_suptext">			(The Comprehensive Chinese syllabus was kindly provided to us by Rutgers University)	</div>
	<div class="content_filter_category tabNavigation clearfix">			
	<?php if ( is_user_logged_in() ) : ?>
	<div class="active" data-default="show_all" >All</div>		
	<div data-default="business_advanced" class="big">Business<br> Advanced</div>	
	<div data-default="business_inter" class="big">Business<br> Intermediate</div>		
	<div data-default="business_beginner" class="big">Business<br> Beginner</div>	
	<div data-default="survival_chines" class="big">Survival<br> Chinese</div>			
	<?php endif; ?>						
	<div data-default="beginner" class=" <?php if ( is_user_logged_in() ) { echo 'last'; } else { echo 'active'; } ?>" >Beginner</div>	
	<div data-default="beginner_intermed" class="big">Beginner /<br> Intermediate</div>		<div data-default="intermediate" >Intermediate</div>						<div data-default="intermediate_advanced" class="big">Intermediate/<br> Advanced</div>		<div data-default="advanced">Advanced</div>											</div>						<div class="content_table_lessons clearfix">		<div class="content_tr_lessons clearfix content_tr_head">			<div class="ct_1">#</div>							<div class="ct_2">Lesson Name</div>							<div class="ct_3">Vocabulary</div>							<div class="ct_4">Grammar</div>							<div class="ct_5">Homework</div>						</div>													<?php if ( is_user_logged_in() ) : ?>		
	
	<div id="show_all" class="tab">				
	<?php for ($i = 0; $i < count($result); $i++) {		
	if ( $result[$i]->level == 1) { ?>			
	<div class="content_tr_lessons clearfix">	
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>	
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>		
	<div class="ct_3">		
	<?php if ($result[$i]->vpath != NULL) { ?>	
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>				
	</div>		
	<div class="ct_4">			
	<?php if ($result[$i]->gpath != NULL) { ?>	
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>			
	</div>			
	<div class="ct_4">		
	<?php if ($result[$i]->hpath != NULL) { ?>	
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>		
	</div>			
	</div>		
	<div class="clear"></div>		
	<?php } } ?>	
	
	<!-- lvl 2 -->		
	<?php	for ($i = 0; $i < count($result); $i++) 	{		
	if ( $result[$i]->level == 2) 	{ ?>		
	<div class="content_tr_lessons clearfix">	
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>
	<div class="ct_3">	<?php	if ($result[$i]->vpath != NULL)	{ 	?>
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>">	
	<div class="actobat_file" ></div></a>	
	<?php } ?>	
	</div>			<div class="ct_4">			
	<?php if ($result[$i]->gpath != NULL) { ?>		
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>							
	<div class="ct_4">				
	<?php if ($result[$i]->hpath != NULL) { ?>			
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>	
	</div>		
	<div class="clear"></div>	
	<?php } } ?>		
	<!-- lvl 3-->	
	<?php for ($i = 0; $i < count($result); $i++) {		
	if ( $result[$i]->level == 3) { ?>	
	<div class="content_tr_lessons clearfix">	
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>	
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>		
	<div class="ct_3">		
	<?php if ($result[$i]->vpath != NULL) { ?>		
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>		
	</div>		
	<div class="ct_4">		
	<?php if ($result[$i]->gpath != NULL) { ?>	
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>	
	</div>		
	<div class="ct_4">		
	<?php if ($result[$i]->hpath != NULL) { ?>		
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>		
	</div>			
	</div>		
	<div class="clear"></div>	
	<?php } } ?>		
	<?php for ($i = 0; $i < count($result); $i++) {	
	if ( $result[$i]->level == 4) { ?>	
	<div class="content_tr_lessons clearfix">		
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>			
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>			
	<div class="ct_3">					
	<?php if ($result[$i]->vpath != NULL) { ?>						
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>">
	<div class="actobat_file" ></div></a>				
	<?php } ?>							
	</div>							
	<div class="ct_4">						
	<?php if ($result[$i]->gpath != NULL) { ?>			
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>				
	</div>						
	<div class="ct_4">						
	<?php if ($result[$i]->hpath != NULL) { ?>						
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>							
	</div>								
	</div>											
	<div class="clear"></div>				
	<?php } } ?>					
	<!-- lvl 4 -->						
	<?php for ($i = 0; $i < count($result); $i++) {						
	if ( $result[$i]->level == 4) { ?>						
	<div class="content_tr_lessons clearfix">					
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>						
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>					
	<div class="ct_3">							
	<?php if ($result[$i]->vpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>					
	</div>							
	<div class="ct_4">							
	<?php if ($result[$i]->gpath != NULL) { ?>			
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>						
	<div class="ct_4">							
	<?php if ($result[$i]->hpath != NULL) { ?>				
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>						
	</div>								
	<div class="clear"></div>						
	<?php } } ?>							
	<?php for ($i = 0; $i < count($result); $i++) {							
	if ( $result[$i]->level == 5 ) { ?>					
	<div class="content_tr_lessons clearfix">				
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>		
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>			
	<div class="ct_3">								
	<?php if ($result[$i]->vpath != NULL) { ?>			
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>								
	<div class="ct_4">							
	
	<?php if ($result[$i]->gpath != NULL) { ?>						
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>					
	</div>								
	<div class="ct_4">							
	<?php if ($result[$i]->hpath != NULL) { ?>		
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>						
	</div>							
	</div>						
	<div class="clear"></div>			
	<?php } } ?>								
	<?php for ($i = 0; $i < count($result); $i++) {				
	if ( $result[$i]->level == 6 ) { ?>					
	<div class="content_tr_lessons clearfix">				
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>			
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>				
	<div class="ct_3">									
	<?php if ($result[$i]->vpath != NULL) { ?>				
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>							
	<div class="ct_4">							
	<?php if ($result[$i]->gpath != NULL) { ?>						
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>						
	<div class="ct_4">							
	<?php if ($result[$i]->hpath != NULL) { ?>				
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>						
	</div>						
	</div>							
	<div class="clear"></div>				
	<?php } } ?>							
	<?php for ($i = 0; $i < count($result); $i++) {					
	if ( $result[$i]->level == 7 ) { ?>							
	<div class="content_tr_lessons clearfix">					
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>			
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>			
	<div class="ct_3">							
	<?php if ($result[$i]->vpath != NULL) { ?>								
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>					
	</div>						
	<div class="ct_4">					
	<?php if ($result[$i]->gpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	</div>							
	<div class="ct_4">										
	<?php if ($result[$i]->hpath != NULL) { ?>			
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>						
	
	</div>							
	<div class="clear"></div>				
	<?php } } ?>							
	<?php for ($i = 0; $i < count($result); $i++) {					
	if ( $result[$i]->level == 8 ) { ?>					
	<div class="content_tr_lessons clearfix">				
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>				
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>					
	<div class="ct_3">						
	<?php if ($result[$i]->vpath != NULL) { ?>						
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>						
	<div class="ct_4">						
	<?php if ($result[$i]->gpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>		
	<?php } ?>						
	
	</div>							
	<div class="ct_4">							
	<?php if ($result[$i]->hpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>											
	</div>							
	<div class="clear"></div>					
	<?php } } ?>								
	<?php for ($i = 0; $i < count($result); $i++) {					
	if ( $result[$i]->level == 9 ) { ?>					
	<div class="content_tr_lessons clearfix">				
	<div class="ct_1"><?php echo $result[$i]->number; ?></div>			
	<div class="ct_2"><?php echo $result[$i]->lname; ?></div>				
	<div class="ct_3">						
	<?php if ($result[$i]->vpath != NULL) { ?>							
	<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>			
	<?php } ?>							
	</div>								
	<div class="ct_4">						
	<?php if ($result[$i]->gpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>				
	<?php } ?>					
	</div>									
	<div class="ct_4">								
	<?php if ($result[$i]->hpath != NULL) { ?>					
	<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>	
	<?php } ?>					
	</div>									
	</div>					
	<div class="clear"></div>					
	<?php } } ?>					
	</div>						
									
											
		<?php endif; ?>									
		<!-- beginner -->								
		<div id="beginner" class="tab">					
		<?php for ($i = 0; $i < count($result); $i++) {			
		if ( $result[$i]->level == 1) { ?>						
		<div class="content_tr_lessons clearfix">				
		<div class="ct_1"><?php echo $result[$i]->number; ?></div>		
		<div class="ct_2"><?php echo $result[$i]->lname; ?></div>			
		<div class="ct_3">										
		<?php if ($result[$i]->vpath != NULL) { ?>			
		<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>
		<?php } ?>											</div>							
		<div class="ct_4">				
		<?php if ($result[$i]->gpath != NULL) { ?>					
		<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>
		<?php } ?>								
		</div>									
		<div class="ct_4">						
		<?php if ($result[$i]->hpath != NULL) { ?>					
		<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>
		<?php } ?>								
		</div>							
		</div>						
		<div class="clear"></div>			
		<?php } } ?>						
		<!-- lvl 2 -->						
		</div>		
		
		<div id="beginner_intermed" class="tab">								<?php for ($i = 0; $i < count($result); $i++) {											if ( $result[$i]->level == 2 ) { ?>										<div class="content_tr_lessons clearfix">										<div class="ct_1"><?php echo $result[$i]->number; ?></div>											<div class="ct_2"><?php echo $result[$i]->lname; ?></div>										<div class="ct_3">														<?php if ($result[$i]->vpath != NULL) { ?>												<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>										<?php } ?>													</div>												<div class="ct_4">												<?php if ($result[$i]->gpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>												</div>													<div class="ct_4">												<?php if ($result[$i]->hpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>												<?php } ?>												</div>													</div>													<div class="clear"></div>												<?php } } ?>										</div>										<!--beginner_intermed-->									<div id="intermediate" class="tab">										<?php for ($i = 0; $i < count($result); $i++) {												if ( $result[$i]->level == 3 ) { ?>										<div class="content_tr_lessons clearfix">											<div class="ct_1"><?php echo $result[$i]->number; ?></div>											<div class="ct_2"><?php echo $result[$i]->lname; ?></div>											<div class="ct_3">												<?php if ($result[$i]->vpath != NULL) { ?>													<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>										<?php } ?>												</div>													<div class="ct_4">														<?php if ($result[$i]->gpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>												<?php } ?>														</div>												<div class="ct_4">												<?php if ($result[$i]->hpath != NULL) { ?>												<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>														</div>											</div>											<div class="clear"></div>										<?php } } ?>										</div>										<!--intermed-advanced-->								<div id="intermediate_advanced" class="tab">									<?php for ($i = 0; $i < count($result); $i++) {										if ( $result[$i]->level == 4 ) { ?>												<div class="content_tr_lessons clearfix">											<div class="ct_1"><?php echo $result[$i]->number; ?></div>											<div class="ct_2"><?php echo $result[$i]->lname; ?></div>										<div class="ct_3">													<?php if ($result[$i]->vpath != NULL) { ?>																				<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>												</div>													<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>																		<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>												<?php } ?>											</div>														<div class="ct_4">														<?php if ($result[$i]->hpath != NULL) { ?>														<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>											</div>											</div>												<div class="clear"></div>												<?php } } ?>								</div>														<!--advanced-->										<div id="advanced" class="tab">										<?php for ($i = 0; $i < count($result); $i++) {											if ( $result[$i]->level == 5 ) { ?>											<div class="content_tr_lessons clearfix">												<div class="ct_1"><?php echo $result[$i]->number; ?></div>									<div class="ct_2"><?php echo $result[$i]->lname; ?></div>											<div class="ct_3">												<?php if ($result[$i]->vpath != NULL) { ?>												<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>													<?php } ?>										</div>											<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>														<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>													</div>										<div class="ct_4">											<?php if ($result[$i]->hpath != NULL) { ?>															<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>															</div>							</div>							<div class="clear"></div>							<?php } } ?>							</div>							<!--business_advanced-->												<div id="business_advanced" class="tab">												<?php for ($i = 0; $i < count($result); $i++) {																if ( $result[$i]->level == 9 ) { ?>														<div class="content_tr_lessons clearfix">													<div class="ct_1"><?php echo $result[$i]->number; ?></div>								<div class="ct_2"><?php echo $result[$i]->lname; ?></div>													<div class="ct_3">											<?php if ($result[$i]->vpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>										</div>													<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>															<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>							<?php } ?>									   </div>													<div class="ct_4">																<?php if ($result[$i]->hpath != NULL) { ?>														<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>							<?php } ?>															</div>								 						</div>								  						<div class="clear"></div>												<?php } } ?>												</div>																	<!--business_advanced-->											<div id="business_inter" class="tab">							<?php for ($i = 0; $i < count($result); $i++) {															if ( $result[$i]->level == 8 ) { ?>													<div class="content_tr_lessons clearfix">															<div class="ct_1"><?php echo $result[$i]->number; ?></div>										<div class="ct_2"><?php echo $result[$i]->lname; ?></div>														<div class="ct_3">															<?php if ($result[$i]->vpath != NULL) { ?>															<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>							<?php } ?>											</div>										<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>														<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>														</div>										<div class="ct_4">											<?php if ($result[$i]->hpath != NULL) { ?>														<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>														</div>												</div>								   <div class="clear"></div>							<?php } } ?>						</div>								<!--business_beginner-->						<div id="business_beginner" class="tab">							<?php for ($i = 0; $i < count($result); $i++) {														if ( $result[$i]->level == 7 ) { ?>								   <div class="content_tr_lessons clearfix">										<div class="ct_1"><?php echo $result[$i]->number; ?></div>								<div class="ct_2"><?php echo $result[$i]->lname; ?></div>										<div class="ct_3">											<?php if ($result[$i]->vpath != NULL) { ?>										<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>														</div>										<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>															<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>													</div>										<div class="ct_4">											<?php if ($result[$i]->hpath != NULL) { ?>															<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>														</div>								   </div>								   <div class="clear"></div>							<?php } } ?>						</div>										<!--business_beginner-->						<div id="survival_chines" class="tab">							<?php for ($i = 0; $i < count($result); $i++) {															if ( $result[$i]->level == 6 ) { ?>								   <div class="content_tr_lessons clearfix">										<div class="ct_1"><?php echo $result[$i]->number; ?></div>									<div class="ct_2"><?php echo $result[$i]->lname; ?></div>										<div class="ct_3">											<?php if ($result[$i]->vpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->vpath; ?>"><div class="actobat_file" ></div></a>											<?php } ?>										</div>												<div class="ct_4">											<?php if ($result[$i]->gpath != NULL) { ?>																<a href="<?php echo $uploaddir . $result[$i]->gpath; ?>"><div class="actobat_file" ></div></a>													<?php } ?>									   </div>										<div class="ct_4">											<?php if ($result[$i]->hpath != NULL) { ?>											<a href="<?php echo $uploaddir . $result[$i]->hpath; ?>"><div class="actobat_file" ></div></a>								<?php } ?>										</div>								   </div>								   <div class="clear"></div>								<?php } } ?>						</div>												<div id="sarch_result" class="tab"></div>															</div>					<div class="content_table_loader_wrap">						<a href="#">							<div class="content_table_loader">Load More</div>								</a>					</div>									</div>			</div><?php get_footer(); ?>																							
		