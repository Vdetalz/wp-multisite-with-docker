<?php
/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('329_329', 329, 329, true);


    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('mc_teacher', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// navigation
function mc_teacher_nav()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'header-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => new c_Walker_Nav_Menu()
        )
    );

    wp_nav_menu(
        array(
            'theme_location'  => 'right-header-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}

// Load scripts (header.php)
function mc_teacher_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('chosen', get_template_directory_uri() . '/js/chosen.jquery.min.js', array('jquery'), '1.3.0'); // Custom scripts
        wp_enqueue_script('chosen'); // Enqueue it!

        wp_register_script('browser', get_template_directory_uri() . '/js/browser.min.js', array('jquery'), '1.3.0'); // Custom scripts
        wp_enqueue_script('browser'); // Enqueue it!

        wp_register_script('matchHeight', get_template_directory_uri() . "/js/jquery.matchHeight-min.js", array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('matchHeight'); // matchHeight plugin

        wp_register_script('mc_teacherscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('mc_teacherscripts'); // Enqueue it!

        wp_localize_script( 'jquery', 'mc_ajax', array('url' => admin_url('admin-ajax.php')));

        wp_register_script('mc_validate', get_template_directory_uri() . "/js/jquery.validate.js", array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('mc_validate'); // Enqueue it!

    }
}

// Load  conditional scripts
function mc_teacher_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load  styles
function mc_teacher_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    /*wp_register_style('mc_teacher', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('mc_teacher'); // Enqueue it!*/

	wp_register_style('screen', get_template_directory_uri() . '/stylesheets/screen.css', array(), '1.0', 'all');
	wp_enqueue_style('screen'); // Enqueue it!

}

// Register  Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
            'header-menu' => __('Header Menu', 'mc_teacher'), // Main Navigation
            'sidebar-menu' => __('Sidebar Menu', 'mc_teacher'), // Sidebar Navigation
            'footer-menu' => __('Footer Menu', 'mc_teacher'), // Extra Navigation if needed (duplicate as many as you need!)
            'right-header-menu' => __('Right Header Menu', 'mc_teacher') // Extra Navigation if needed (duplicate as many as you need!)
        ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
            'name' => __('Widget Area 1', 'mc_teacher'),
            'description' => __('Description for this widget-area...', 'mc_teacher'),
            'id' => 'widget-area-1',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
            'name' => __('Footer Widget', 'mc_teacher'),
            'description' => __('Widgets in footer', 'mc_teacher'),
            'id' => 'widget-footer',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

	// Define Sidebar Widget Area 3
	register_sidebar(array(
		'name' => __('Footer Widget 2', 'mc_teacher'),
		'description' => __('Widgets in footer', 'mc_teacher'),
		'id' => 'widget-footer-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function mc_teacher_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
            'prev_text' => __('Prev'),
            'next_text' => __('Next'),
        ));
}

// Create the Custom Excerpts callback
function mc_teacher_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Remove 'text/css' from our enqueued stylesheet
function mc_teacher_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function mc_teacher_comments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
    <div class="comment-author vcard">
        <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
        <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
    <?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
            <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
        <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'mc_teacher_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'mc_teacher_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'mc_teacher_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add  Menu
add_action('init', 'mc_teacher_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('style_loader_tag', 'mc_teacher_style_remove'); // Remove 'text/css' from enqueued stylesheet


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

add_shortcode('digital_products_descriptions', 'digital_products_descriptions');

/*
 * Shortcode with digital products description posts
 */
function digital_products_descriptions( $atts ){
    extract( shortcode_atts( array(
                'count' => 3,
            ), $atts ) );

    // get posts of "digital products description" content type
    $args = array(
        'post_type' => 'product-description',
        'posts_per_page' => $count
    );

    $descriptions_query = new WP_Query( $args );

    ob_start();
?>
<?php if ( $descriptions_query->have_posts() ) : ?>
        <ul class="digital-products-list">
    <?php while ( $descriptions_query->have_posts() ) : $descriptions_query->the_post(); ?>
            <li>
                <a href="<?php the_permalink();?>">
                    <?php the_post_thumbnail('329_329');?>
	                <span class="title"><?php the_title();?></span>
                </a>
            </li>
    <?php  endwhile;?>
        </ul>
<?php endif;

    return ob_get_clean();
}

// shortcode anchor link
add_shortcode('explore_courses', 'explore_courses');

function explore_courses($atts){

    if(!$atts['title']){
        $atts['title'] = __('Explore courses', 'mc_teacher');
    }

    return "<a class='e-btn js-goToThis' href='#" . $atts['target'] . "'>" . $atts['title'] . "</a>";

}

// Shortcode with contact me button
add_shortcode('contact_me', 'contact_me_function');

/*
 * Contact me callback function.
 */
function contact_me_function( $atts ) {
    ob_start();
    ?>
    <a class="popup-with-form" href=".contact-me-form"><?php _e('Contact Me');?></a>
    <?php
    return ob_get_clean();
}

// Add contact me send mail AJAX function.
add_action("wp_ajax_contact_me_send", "contact_me_send_callback");
add_action("wp_ajax_nopriv_contact_me_send", "contact_me_send_callback");

/*
 * Contact me send mail callback function.
 */
function contact_me_send_callback() {
    if(isset($_POST['submitted'])) {
        $emails = get_option('contact_me_emails');
        $raw_emails = explode("\n", $emails);
        $to = array();

        $zd = zendesk_send_form();

        foreach ($raw_emails as $value) {
            $to[] = trim($value);
        }
        $subject = __('Contact Me');

        ob_start();
        ?>
        <?php echo __('First Name') . ': ' . $_POST['first_name'] . "\n";?>
        <?php echo __('Last Name') . ': ' . $_POST['last_name'] . "\n";?>
        <?php echo __('School / Institution') . ': ' . $_POST['school'] . "\n";?>
        <?php echo __('Town') . ': ' . $_POST['town'] . "\n";?>
        <?php echo __('Email address') . ': ' . $_POST['z_email'] . "\n";?>
        <?php echo __('Phone Number') . ': ' . $_POST['phone_number'] . "\n";?>
        <?php echo __('Message') . ': ' . $_POST['text_message'] . "\n";?>
        <?php
        $message = ob_get_clean();
        wp_mail($to, $subject, $message);
        echo 1;
        die();
    }
}

add_filter( 'wp_mail_from', 'custom_wp_mail_from' );
function custom_wp_mail_from( $email ) {
	return get_option('admin_email');
}

add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
function custom_wp_mail_from_name( $original_email_from ) {
	return get_option('blogname');
}

add_action('admin_menu', 'add_contact_me_emails_field');

/*
 * Add contact me emails field to the general admin page.
 */
function add_contact_me_emails_field(){
    $option_name = 'contact_me_emails';
    register_setting('general', $option_name);

    add_settings_field(
        'contact_me_emails_id',
        __('Contact me e-mails'),
        'contact_me_emails_function',
        'general',
        'default',
        array(
            'id' => 'contact_me_emails_id',
            'option_name' => 'contact_me_emails',
        )
    );
}

/*
 * Contact me callback function.
 */
function contact_me_emails_function( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>

    <textarea
        name = "<?php echo $option_name ?>"
        id = "<?php echo $id ?>"
        rows = "3"
        cols = "25"
        wrap="hard"
    ><?php echo esc_attr( get_option($option_name) ) ?></textarea>
    <p class="description"><?php _e('These addresses are used for receiving contact me form mails. Enter each address on a new line.');?></p>

    <?php
}

// Change login form titles
add_filter( 'tml_title', 'login_form_titles', 0, 2 );

/*
 * tml_title filter callback
 */
function login_form_titles( $title, $action ){
    switch ( $action ) {
        case 'lostpassword':
        case 'retrievepassword':
        case 'resetpass':
        case 'rp':
            $title = __( 'Forgot Your Password?' );
            break;
    }
    return $title;
}


add_filter('login_redirect', 'mychineseteacher_login_redirect', 10, 3);
/*
 * Redirects authors and subscribers after login
 */
function mychineseteacher_login_redirect($redirect_to, $request, $user){
    global $wpdb;
    if (isset($user->roles) && is_array($user->roles)) {
        if(in_array('subscriber', $user->roles)){
            return home_url();
        } elseif (in_array('author', $user->roles)){
            switch_to_blog(5);
            $results = $wpdb->get_var( "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name = 'edd_settings'");
            $results = maybe_unserialize($results);
            $page_url = get_permalink($results['mct_edd_author_login_redirect']);
            restore_current_blog();
            if(!$page_url){
                $page_url = home_url();
            }
            return  $page_url;
        } elseif (in_array('administrator', $user->roles)){
            return admin_url();
        } elseif(in_array('students', $user->roles)) {
            switch_to_blog(5);
            $results = $wpdb->get_var( "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name = 'edd_settings'");
            $results = maybe_unserialize($results);
            $course_page_id = $results['mch_course_page_redirect'];
            $page_url = get_permalink($course_page_id);
            restore_current_blog();
            if(!$page_url){
                $page_url = home_url();
            }
            return $page_url;
        } else {
            return $redirect_to;
        }
    }
}
//
//add_action('the_post', 'mct_login_redirect', 10, 1);
///*
// * Redirects user from login page.
// */
//function mct_login_redirect($page){
//    global $wpdb;
//    if(is_page(edd_get_option('mch_login_page'))){
//        $user = get_userdata(get_current_user_id());
//        if (isset($user->roles) && is_array($user->roles)) {
//            if (in_array('author', $user->roles)){
//                switch_to_blog(5);
//                $results = $wpdb->get_var( "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name = 'edd_settings'");
//                $results = maybe_unserialize($results);
//                $page_url = get_permalink($results['mct_edd_author_login_redirect']);
//                restore_current_blog();
//                wp_redirect($page_url);
//            } elseif (in_array('administrator', $user->roles)){
//                wp_redirect(admin_url());
//            } else {
//                wp_redirect(home_url());
//            }
//        }
//    }
//}
//
////  Shortcode courses list of current project description
add_shortcode('courses_list', 'courses_list');

/*
 * Callback for courses_list shortcode
 */
function courses_list( $atts ){
    $courses = array();
    extract( shortcode_atts( array(
                'id' => null,
            ), $atts ) );

    if( empty( $id ) ){
        $id = get_the_ID();
    }

    if( !empty( $id ) ){
        $courses = get_field('related_courses', $id);
    }

    ob_start();
    ?>
    <?php if ( !empty( $courses ) ) : ?>
        <select class="courses-list">
            <?php foreach( $courses as $course ) :  ?>
                <option value="<?php echo $course->ID;?>"><?php echo get_the_title( $course->ID );?></option>
            <?php endforeach;?>
        </select>
        <div class="course-description">
            <?php echo apply_filters('the_content', $courses[0]->post_content);?>
        </div>
        <?php foreach( $courses as $course ) :  ?>
        <div class="description-<?php echo $course->ID;?> display-none">
            <?php echo apply_filters('the_content', $course->post_content);?>
        </div>
        <?php endforeach;?>
    <?php endif;

    return ob_get_clean();
}

function pricing_table_view($data, $list){

    if(!$data) return;

    if(isset($_GET['update_download'])) $download_id = $_GET['update_download'];
    else $download_id = '';

	?>
	<div class="table-wrap">
	<?php

    foreach($data as $k => $d):?>

    <table border="1" class="pricing_table pricing_table_<?php echo $k;?>">
        <tr>
            <td>
                <select name="pricint_table_select">
                    <?php foreach($list as $value):?>

                        <option value="<?php echo $value->ID;?>" <?php echo selected($download_id, $value->ID);?>><?php _e($value->post_title);?></option>

                    <?php endforeach;?>
                </select>
            </td>
            <td><?php _e('One Term');?></td>
            <td><?php _e('One Semester');?></td>
            <td><?php _e('Recommended');?></td>
        </tr>

        <?php foreach($d->posts as $child_post): $one_term = get_post_meta($child_post->ID, 'wpcf-one-term', true);
                                                       $one_semester = get_post_meta($child_post->ID, 'wpcf-one-semester', true);
                                                       $recommended = get_post_meta($child_post->ID, 'wpcf-recommended', true);?>

            <tr>
                <td><?php _e($child_post->post_title);?></td>
                <td>
                    <?php if($one_term == 'true' || $one_term == 'false'):?>
                        <img src="<?php echo get_template_directory_uri() . '/img/icon_' . trim($one_term) . '.png' ;?>" height="30"/>
                    <?php else:?>
                        <?php echo $one_term;?>
                    <?php endif;?>
                </td>
                <td>
                    <?php if($one_semester == 'true' || $one_semester == 'false'):?>
                        <img src="<?php echo get_template_directory_uri() . '/img/icon_' . trim($one_semester) . '.png' ;?>" height="30"/>
                    <?php else:?>
                        <?php echo $one_semester;?>
                    <?php endif;?>
                </td>
                <td>
                    <?php if($recommended == 'true' || $recommended == 'false'):?>
                        <img src="<?php echo get_template_directory_uri() . '/img/icon_' . trim($recommended) . '.png' ;?>" height="30"/>
                    <?php else:?>
                        <?php echo $recommended;?>
                    <?php endif;?>
                </td>
            </tr>

        <?php endforeach;?>

        <tr class="aux">
	        <td colspan="4"></td>
        </tr>
	    <tr>
            <td><?php _e('Price pre period');?></td>
            <td><span><?php echo get_post_meta($k, 'wpcf-one-term-price', true);?></span> USD</td>
            <td><span><?php echo get_post_meta($k, 'wpcf-one-semester-price', true);?></span> USD</td>
            <td><span><?php echo get_post_meta($k, 'wpcf-recommended-price', true);?></span> USD</td>
        </tr>
    </table>


    <?php endforeach; ?>
	</div>
    <?php

}

function pricing_table_callback($atts){

    $product_description = get_posts(array(
       'post_type' => 'product-description'
    ));

    if($product_description){
        foreach($product_description as $key => $pd){
            $child_posts[$pd->ID] = new WP_Query( "post_type=pricing_options&meta_key=_wpcf_belongs_product-description_id&meta_value=" . $pd->ID);
        }
    }

    pricing_table_view($child_posts, $product_description);

}

add_shortcode('pricing_table', 'pricing_table_callback');


// Assigns customize_text_sizes() to "tiny_mce_before_init" filter
function customize_text_sizes($initArray){
    $initArray['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 41px 42px 43px 44px 45px 46px 47px 48px 49px 50px 52px 56px 60px 65px 72px";
    return $initArray;
}
add_filter('tiny_mce_before_init', 'customize_text_sizes');


/**
 * shortcode for home page
 * @param $atts
 * @return string
 */

function mct_testimonial($atts){

    extract(
        shortcode_atts(
            array(
                'limit' => 4,
                'title' => 'Testimonials'
            ), $atts
        )
    );

    $testimonials_query = mct_testimonials_archive($limit);

    if( $testimonials_query ) : ob_start();?>

    <div id="e-btn m-btn-orange">
        <h1><?php _e($title);?></h1>

        <?php echo $testimonials_query;?>
		<div class="t-link-block">
			<a class="liak-all" href="<?php echo get_permalink( get_page_by_path( 'testimonials' ) );?>">
				<?php _e('See All');?>
			</a>
		</div>

    </div>
    <?php return ob_get_clean(); endif;

}

add_shortcode('testimonial', 'mct_testimonial');


/**
 * Archive handler
 * @param int $limit
 * @param int $offset
 * @return bool
 */

function mct_testimonials_archive($limit = 10, $offset = 0){

    $testimonials_query = new WP_Query(array(
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'showposts' => $limit,
        'post_per_page' => $limit,
        'offset' => $offset,
    ));

    if( $testimonials_query->post_count > 0 ) :
        ob_start();
        while( $testimonials_query->have_posts() ) : $testimonials_query->the_post();?>

	        <div class="testimonial-block">
		        <div class="testimonial">
			        <div class="testimonial-head">
				        <b class="testimonial-author"><?php echo the_title();?></b>
				        <span class="testimonial-school"><span class="student-ttl"><?php _e('Principal');?>,</span> <span><?php echo get_post_meta( get_the_ID(), 'wpcf-testimonial-school', true );?></span></span>
			        </div>
			        <div class="testimonial-content">
				        <?php the_content();?>
			        </div>
		        </div>
	        </div>

        <?php endwhile;

        return ob_get_clean();?>

        <?php else:
            return false;
        endif;

}


/**
 * Archive Shortcode for single page
 * @param $atts
 * @return string
 */

function mct_testimonials_archive_shortcode($atts){

    extract(
        shortcode_atts(
            array(
                'limit' => 4,
                'offset' => 0
            ), $atts
        )
    );

    if( wp_count_posts('testimonial')->publish > $limit)
        $more_link = '<a href="#" data-offset="' . $limit . '" data-limit="' . $limit . '" class="testimonials-load-more-link">' . __('SHOW ' . $limit . ' MORE') . '</a>';

    return mct_testimonials_archive($limit, $offset) . $more_link;

}

add_shortcode('testimonials_archive', 'mct_testimonials_archive_shortcode');


/**
 * ajax action for load testimonials by offset, limit
 */

function testimonials_load_more(){
    $testimonials = mct_testimonials_archive($_POST['limit'], $_POST['offset']);
    if($testimonials)
        echo $testimonials;
    else
        echo 0;
    die();
}

add_action('wp_ajax_testimonials_load_more', 'testimonials_load_more');

/**
 * ajax action for check is already_subscribed
 */

function is_already_subscribed() {
    $email = sanit_email($_POST['email']);
    echo is_public($email);
    die();
}

/**
Is the supplied email address a public subscriber?
 */
function is_public($email = '') {
    global $wpdb;
    $table = $wpdb->prefix . 'subscribe2';

    if ( '' == $email ) { return false; }

    // run the query and force case sensitivity
    $check = $wpdb->get_var($wpdb->prepare("SELECT active FROM $table WHERE CAST(email as binary)=%s", $email));
    if ( '0' == $check || '1' == $check ) {
        return $check;
    } else {
        return false;
    }
} // end is_public()

/**
Function to ensure email is compliant with internet messaging standards
 */
	function sanit_email($email) {
        $email = trim($email);
        if ( !is_email($email) ) { return; }

        // ensure that domain is in lowercase as per internet email standards http://www.ietf.org/rfc/rfc5321.txt
        list($name, $domain) = explode('@', $email, 2);
        return $name . "@" . strtolower($domain);
    } // end sanitize_email()

add_action('wp_ajax_nopriv_is_already_subscribed', 'is_already_subscribed');


class c_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * @param object $element
     * @param array $children_elements
     * @param int $max_depth
     * @param int $depth
     * @param array $args
     * @param string $output
     * @return null
     */


    function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        // check, whether there are children for the given ID and append it to the element with a (new) ID
        $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     */

    function start_el(&$output, $item, $depth, $args) {

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        //Get current User object
        $user = wp_get_current_user();

        /**
         * ***************************** DISPLAY ITEM MENU LOGIC *******************************
         */

        if ( in_array( 'mss-sister-school', $item->classes ) ) {
            if ( ! in_array( 'administrator', $user->roles ) && ! in_array( 'ss_users', $user->roles ) && ! in_array( 'editor', $user->roles ) ) {
                $output = '';
                return ;
            }
        }

        if ( in_array( 'mss-blog', $item->classes ) ) {
            if ( ! in_array( 'administrator', $user->roles ) && ! in_array( 'ss_users', $user->roles )  && ! in_array( 'editor', $user->roles ) ) {
                $output = '';
                return;
            }
        }

        if ( in_array( 'mss-photos', $item->classes ) ) {
            if ( ! in_array( 'administrator', $user->roles ) && ! in_array( 'ss_users', $user->roles )  && ! in_array( 'editor', $user->roles ) ) {
                $output = '';
                return;
            }
        }

        if ( in_array( 'unlogged' , $item->classes ) && get_current_user_id() ) {
            return;
        }

        if ( in_array( 'logged' , $item->classes ) && ! get_current_user_id() ) {
            return;
        }

        $output .= $indent . '<li' . $id . $value . $class_names . '>';

        /**
         * ***************************** END OF ITEM MENU LOGIC ********************************
         */

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

        if ( in_array( 'contact-popup', $item->classes ) ) {
            $attributes .= ! empty( $item->url )        ? ' href="'   . str_replace( 'http://', '', $item->url) .'"' : '';
            $attributes .= " class='popup-with-form' ";
        } else {
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        }


        $item_output = $args->before;
        $item_output .= '<a '. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;


        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( in_array( 'unlogged' , $item->classes ) && get_current_user_id() ) {
			$output .= "";
		}

		if ( in_array( 'logged' , $item->classes ) && ! get_current_user_id() ) {
			$output .= "";
		}
	}
}

add_action('wp_head','mss_pluginname_ajaxurl');
function mss_pluginname_ajaxurl() {
    ?>
    <script type="text/javascript">
        var Ajax = []; Ajax['ajaxurl'] = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}

add_action('init', 'mss_init_session', 1);
function mss_init_session() {
    if(!session_id()) {
        session_start();
    }
}
