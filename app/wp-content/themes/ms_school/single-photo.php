<?php get_header(); global $post;?>

	<div role="main" class="single-p-d main">
        <!-- section -->
        <section>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if( function_exists('check_if_user_can_upload') ){
                    $has_access = check_if_user_can_upload();
                }
                if($has_access): ?>
                    <div class="content-wrapper">

                        <div class="content-header">
                            <!-- post title -->
                            <div>
                                <a href="<?php echo get_permalink(get_option('photo_page')); ?>" class="e-btn"><?php _e('Back', 'mch'); ?></a>
                            </div>
                            <!-- /post title -->
                        </div>

                        <div class="photo-block">
                            <div class="photo-content">
                                    <div>
                                        <?php
                                        $cpost = get_post(get_the_ID());

                                        if ( get_current_blog_id() == 11 ) {
                                            $user = get_userdata($cpost->post_author);
                                            $name = $user->data->user_nicename;
                                            if ( function_exists( 'mss_get_user_school' ) ) {
                                                $school = mss_get_user_school( $cpost->post_author );
                                                if ( isset( $school->post_title ) && !empty( $school->post_title ) ) {
                                                    $name .= ', ' . $school->post_title;
                                                }
                                            }
                                        } else {
                                            $name = get_user_meta($cpost->post_author, 'first_name', true);
                                            $name .= ' ' . get_user_meta($cpost->post_author, 'last_name', true);
                                            $school = get_user_meta($cpost->post_author, 'school', true);
                                            if($school){
                                                $name .= ', ' . $school;
                                            }
                                        }


                                        $date = date(' (F d, Y)', strtotime(get_post(get_the_ID())->post_date));
                                        echo __('By: ') . $name . $date;
                                        ?>
                                    </div>
                                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                                    <?php else : ?>
                                        <?php $video = get_attached_media( 'video', get_the_ID() ); 
                                        if ( !empty( $video ) && is_array( $video ) ) : $video_id = key($video); ?>
                                              
                                            <?php if ( ! empty( $video[ $video_id ]->guid )  ) : ?>
                                                <video controls width="640">
                                                    <source src="<?php echo $video[ $video_id ]->guid; ?>">
                                                </video>
                                            <?php else: ?>
                                                <?php _e( 'Nothing to display.' ); ?>
                                            <?php endif; ?>
                                            
                                       <?php endif; ?>
                                    <?php endif; ?>

                                    <?php the_content(); // Dynamic Content ?>
                            </div>

                            <?php if (function_exists('mss_next_photo_link') && function_exists('mss_prev_photo_link')) : ?>
                                <div class="nav-block">
                                        <div class="prev">
                                            <?php echo mss_prev_photo_link($post); ?>
                                        </div>
                                        <div class="next">
                                            <?php echo mss_next_photo_link($post); ?>
                                        </div>
                                </div>
                            <?php endif; ?>

                            <?php

                                if( function_exists('mch_comment_form') ){
                                    mch_comment_form(get_the_ID());
                                }

                                if( function_exists('mch_get_list_comments') ){
                                    mch_get_list_comments(get_the_ID());
                                }

                            ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="error-msg"><h2><?php _e('Please login to view Photos.', 'mch'); ?></h2></div>
                <?php endif; ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        </section>
        <!-- /section -->
	</div>

<?php get_footer(); ?>
