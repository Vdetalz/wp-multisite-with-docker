

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <!-- article -->
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


        <!-- post title -->
        <h2>
            <?php the_title(); ?>
        </h2>
        <?php
        $term = get_term_by( 'slug', 'blog', 'category' );
        $terms = wp_get_post_terms( get_the_ID(), 'category', array( 'parent' => $term->term_id, 'exclude' => array( $term->term_id ) ) ); ?>
        <?php if ( $terms ) : ?>
            <?php foreach( $terms as $term ) : ?>
                    <?php if ( $term->slug == 'blog' ) continue; ?>
                <b><?php _e( 'Category: ' ); ?></b><a href="<?php echo get_term_link( $term ); ?>"><?php _e( $term->name); ?></a>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- /post title -->
        
        <div class="post-info">
        <!-- post thumbnail -->
        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <div class="post-thumbnail">
                <?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
            </div>
        <?php endif; ?>
        <!-- /post thumbnail -->

        <!-- post body -->
        <div class="post-body">
            <?php mc_teacher_excerpt('mc_teacher_index'); // Build your custom callback length in functions.php ?>
        </div>
        <!-- /post body -->
        </div>

        <!-- read more-->
        <div class="read-more">
            <a href="<?php the_permalink(); ?>"><?php _e('Read More'); ?></a>
        </div>
        <!-- /read more-->

    </article>
    <!-- /article -->

<?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>