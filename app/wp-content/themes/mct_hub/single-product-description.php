<?php get_header(); ?>

	<div role="main" class="single-p-d main">
        <!-- section -->
        <section>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="bg-page">
                    <!-- post thumbnail -->
                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                    <?php endif; ?>
                    <!-- /post thumbnail -->
                </div>

                <div class="content-wrapper">
                    <?php

                    $cid = get_the_ID();
                    // get posts of "digital products description" content type
                    $args = array(
                        'post_type' => 'product-description',
                        'posts_per_page' => -1
                    );

                    $descriptions_query = new WP_Query( $args );
                    ?>
                    <?php if ( $descriptions_query->have_posts() ) : ?>
                        <ul class="descriptions-list">
                            <?php while ( $descriptions_query->have_posts() ) : $descriptions_query->the_post(); ?>
                                <li <?php if($cid == get_the_ID()):?> class="active"<?php endif;?>><a href="<?php the_permalink();?>"> <?php the_title();?></a></li>
                            <?php endwhile;?>
                        </ul>
                    <?php endif; wp_reset_postdata();?>
                    <div class="content-header">
                        <!-- post title -->
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                        <div class="contact-me">
                        <?php if(!is_user_logged_in()): ?>
                            <?php echo do_shortcode('[register_btn title="GET FREE ALL ACCESS PASS"]');?>
                        <?php else : ?>
                            <?php echo do_shortcode('[subscription_options_btn title="SUBSCRIPTION OPTIONS"]');?>
                        <?php endif; ?>
                        </div>
                        <!-- /post title -->
                    </div>

                    <div>
                        <?php the_content(); // Dynamic Content ?>

                    </div>
                </div>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        </section>
        <!-- /section -->
	</div>

<?php get_footer(); ?>
