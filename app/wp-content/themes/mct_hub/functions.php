<?php
/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', TRUE); // Large Thumbnail
    add_image_size('medium', 250, '', TRUE); // Medium Thumbnail
    add_image_size('small', 120, '', TRUE); // Small Thumbnail
    add_image_size('custom-size', 700, 200, TRUE); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('329_329', 329, 329, TRUE);


    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('mct_hub', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// navigation
function mct_hub_nav()
{
	if(is_user_logged_in()){
/*
        wp_nav_menu(
            array(
                'theme_location'  => 'header-menu-loginned',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => '',
            )
        );

        wp_nav_menu(
            array(
                'theme_location'  => 'right-header-menu-loginned',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => new c_Walker_Nav_Menu()
            )
        );
*/
    }
    else{

        wp_nav_menu(
            array(
                'theme_location'  => 'header-menu-unloginned',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => TRUE,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            )
        );

        wp_nav_menu(
            array(
                'theme_location'  => 'right-header-menu-unloginned',
                'menu'            => '',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => TRUE,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            )
        );

    }
}

// Load scripts (header.php)
function mct_hub_header_scripts()
{

    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

	    wp_register_script('chosen', get_template_directory_uri() . '/js/chosen.jquery.min.js', array('jquery'), '1.3.0'); // Custom scripts
	    wp_enqueue_script('chosen'); // Enqueue it!

        wp_register_script('browser', get_template_directory_uri() . '/js/browser.min.js', array('jquery'), '1.3.0'); // Custom scripts
        wp_enqueue_script('browser'); // Enqueue it!

        wp_register_script('mct_zclip', get_template_directory_uri() . '/js/jquery.zclip.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('mct_zclip'); // Enqueue it!

        wp_register_script('mct_hubscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('mct_hubscripts'); // Enqueue it!

        wp_register_script('slick', get_template_directory_uri() . '/js/lib/slick.min.js', array('jquery'), '1.0.0'); // Slick slider library
        wp_enqueue_script('slick'); // Slick it!

        wp_register_script('customScroll', get_template_directory_uri() . '/js/lib/jquery.mCustomScrollbar.concat.min.js', array('jquery'), '1.0.0'); // custom scrollbar library
        wp_enqueue_script('customScroll'); // custom scroll it!

        wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0'); // my scripts
        wp_enqueue_script('custom'); // my scripts!

    }
}

// Load  conditional scripts
function mct_hub_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load  styles
function mct_hub_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    /*wp_register_style('mct_hub', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('mct_hub'); // Enqueue it!*/

    wp_register_style('screen', get_template_directory_uri() . '/stylesheets/screen.css', array(), '1.0', 'all');
    wp_enqueue_style('screen'); // Enqueue it!

}

// Register  Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'right-header-menu-loginned' => __(' Right Header Menu Loginned', 'mct_hub'), // Main Navigation,
        'header-menu-loginned' => __('Header Menu Loginned', 'mct_hub'), // Main Navigation,
        'header-menu-unloginned' => __('Header Menu Unloginned', 'mct_hub'), // Main Navigation
        'right-header-menu-unloginned' => __('Right Header Menu Unloginned', 'mct_hub'), // Main Navigation
        'header-menu' => __('Header Menu', 'mct_hub'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'mct_hub'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'mct_hub'), // Extra Navigation if needed (duplicate as many as you need!)
        'right-header-menu' => __('Right Header Menu', 'mct_hub'), // Extra Navigation if needed (duplicate as many as you need!)
		'footer-menu' => __('Footer Menu', 'mc_teacher'), // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = FALSE;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Wiki
    register_sidebar(array(
        'name' => __('Wiki', 'mct_hub'),
        'description' => __('Description for this widget-area...', 'mct_hub'),
        'id' => 'widget-area-wiki',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'mct_hub'),
        'description' => __('Description for this widget-area...', 'mct_hub'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'mct_hub'),
        'description' => __('Description for this widget-area...', 'mct_hub'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

	// Define Sidebar Widget Area 2
    register_sidebar(array(
            'name' => __('Footer Widget', 'mct_hub'),
            'description' => __('Widgets in footer', 'mct_hub'),
            'id' => 'widget-footer',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

	// Define Sidebar Widget Area 3
	register_sidebar(array(
		'name' => __('Footer Widget 2', 'mct_hub'),
		'description' => __('Widgets in footer', 'mct_hub'),
		'id' => 'widget-footer-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

}
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function mct_hub_comments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) {echo get_avatar( $comment, $args['180'] );} ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'mct_hub_header_scripts'); // Add Custom Scripts to wp_head
add_action('init', 'custom_wp_title'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'mct_hub_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'mct_hub_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add  Menu
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet


// Change login form titles
add_filter( 'tml_title', 'login_form_titles', 0, 2 );

/*
 * tml_title filter callback
 */
function login_form_titles( $title, $action ){
    switch ( $action ) {
        case 'lostpassword':
        case 'retrievepassword':
        case 'resetpass':
        case 'rp':
            $title = __( 'Forgot Your Password?' );
            break;
    }
    return $title;
}
/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

add_shortcode('digital_products_descriptions', 'digital_products_descriptions');

/*
 * Shortcode with digital products description posts
 */
function digital_products_descriptions( $atts ){
    extract( shortcode_atts( array(
        'count' => 6,
    ), $atts ) );

	//remove extra p before and after shortcode (added for seo)
	remove_filter( 'the_content', 'wpautop' );

    // get posts of "digital products description" content type
    $args = array(
        'post_type' => 'product-description',
        'posts_per_page' => $count
    );

    $descriptions_query = new WP_Query( $args );

    ob_start();
    ?>
    <?php if ( $descriptions_query->have_posts() ) : ?>
        <ul class="digital-products-list" id=<?php echo $atts['id'];?>>
            <?php while ( $descriptions_query->have_posts() ) : $descriptions_query->the_post(); ?>
                <li>
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail('329_329');?>
                        <span class="title"><?php the_title();?></span>
                    </a>
                </li>
            <?php  endwhile;?>
        </ul>
    <?php endif;

	$content = ob_get_clean();
	//add_filter( 'the_content', 'wpautop' , 99);
    add_filter( 'the_content', 99);

    return $content;
}


function explore_courses($atts){

//  Shortcode courses list of current project description
add_shortcode('courses_list', 'courses_list');

    if(!$atts['title']) {$atts['title'] = __('Explore courses');}

    return "<a class='e-btn m-shadow js-goToThis' href='#" . $atts['target'] . "'>" . $atts['title'] . "</a>";

}

add_shortcode('explore_courses', 'explore_courses');

function register_btn($atts){

    if(!$atts['page_id']){
        $atts['page_id'] = 312;
    }
    if(!$atts['title']){
        $atts['title'] = 'Get Free All Access Pass';
    }

    return "<a href='" . get_permalink($atts['page_id']) . "' class='e-btn " . $atts['class'] . "'>" . __($atts['title']) . "</a>";

}

add_shortcode('register_btn', 'register_btn');


//  Shortcode courses list of current project description
add_shortcode('courses_list', 'courses_list');

/*
 * Callback for courses_list shortcode
 */
function courses_list( $atts ){
    $courses = array();
    extract( shortcode_atts( array(
                'id' => NULL,
            ), $atts ) );

    if( empty( $id ) ){
        $id = get_the_ID();
    }

    if( !empty( $id ) ){
        $courses = get_field('related_courses', $id);
    }

    ob_start();
    ?>
    <?php if ( !empty( $courses ) ) : ?>
        <select class="courses-list">
            <?php foreach( $courses as $course ) :  ?>
                <option value="<?php echo $course->ID;?>"><?php echo get_the_title( $course->ID );?></option>
            <?php endforeach;?>
        </select>
        <div class="course-description">
            <?php echo apply_filters('the_content', $courses[0]->post_content);?>
        </div>
        <?php foreach( $courses as $course ) :  ?>
            <div class="description-<?php echo $course->ID;?> display-none">
                <?php echo apply_filters('the_content', $course->post_content);?>
            </div>
        <?php endforeach;?>
    <?php endif;

    return ob_get_clean();
}

class c_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * @param object $element
     * @param array $children_elements
     * @param int $max_depth
     * @param int $depth
     * @param array $args
     * @param string $output
     *
*@return null
     */


    function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        // check, whether there are children for the given ID and append it to the element with a (new) ID
        $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     */

    function start_el(&$output, $item, $depth, $args) {

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        //Get current User object
        $user = wp_get_current_user();
        $nickname = get_user_meta($user->ID, 'nickname', TRUE);

        /**
         * ***************************** DISPLAY ITEM MENU LOGIC *******************************
         */


        if( in_array('menu-my-courses-link', $item->classes) ){

            //if( !in_array('author', $user->roles) ) {
                $output .= $indent . '<li' . $id . $value . $class_names .'>';
            //} else return;

        //Forum Link

        } else if( in_array('menu-forum-link', $item->classes) ){

            if ( mch_user_has_access($user->ID, 'view_forum') || in_array('author', $user->roles) ) {
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {return;}

        //Teacher Portal

        } else if( in_array('menu-teacher-portal', $item->classes) ){

            if( in_array('author', $user->roles) ) {
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {return;}

        //User Account

        } else if( in_array('menu-user-info-link', $item->classes) ) {

            if (in_array('author', $user->roles)) {
                $output .= $indent . '<li' . $id . $value . $class_names . '><a href="' . get_permalink( edd_get_option('mct_edd_author_login_redirect', TRUE) ) . '" class="user-info">' . get_avatar($user->ID) . $nickname . '</a>';
            } else {
                $output .= $indent . '<li' . $id . $value . $class_names . '><a href="' . get_permalink( edd_get_option('mct_edd_subscriber_login_redirect', TRUE) ) . '" class="user-info">' . get_avatar($user->ID) . $nickname . '</a>';
            }

        //Photos

        } else if( in_array('menu-photos-link', $item->classes) ){
            if( function_exists('check_if_user_can_upload') ){
                $has_access = check_if_user_can_upload();
            }

            if($has_access){
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {return;}

        //Wiki

        } else if( in_array('menu-wiki-link', $item->classes) ) {

            if (current_user_can('edit_wiki_privileges')) {
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {
                return;
            }

        } else if( in_array('menu-lesson-history-link', $item->classes) ) {
            if (current_user_can('read_private_pages')) {
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {
                return;
            }
        } else {
            $output .= $indent . '<li' . $id . $value . $class_names .'>';
        }

        /**
         * ***************************** END OF ITEM MENU LOGIC ********************************
         */

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';


        $item_output = $args->before;
        $item_output .= '<a '. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$user = wp_get_current_user();
		if( in_array('menu-teacher-portal', $item->classes) ) {
			if ( !in_array( 'author', $user->roles ) ) {
				return;
			}
		}
	}
}


/*
 * Display promotion block in the registration form.
 */
add_action('after_signup_form', 'display_promotion_block');

function display_promotion_block(){
?>
    <div class="wrap-content-registration">
        <div class="inner-wrap-content-registration">
            <div class="content-registration">
                <?php if(has_post_thumbnail()) {
                    the_post_thumbnail(array(560, 350));
                }
                else {
                    the_content();
                }?>
            </div>
        </div>
    </div>
<?php
}

/*
 * Remove promotion block from the confirm registration message.
 */
add_action('signup_finished', 'remove_promotion_block');

function remove_promotion_block(){
    remove_action('after_signup_form', 'display_promotion_block');
    ?>
    <span class="img-mail"><?php _e('Send mail'); ?><span>
    <?php
}

// Assigns customize_text_sizes() to "tiny_mce_before_init" filter
function customize_text_sizes($initArray){
    $initArray['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 41px 42px 43px 44px 45px 46px 47px 48px 49px 50px 52px 56px 60px 65px 72px";
    return $initArray;
}
add_filter('tiny_mce_before_init', 'customize_text_sizes');

add_action('the_post', 'mch_login_redirect', 10, 1);
/*
 * Redirects user from login page.
 */
function mch_login_redirect($page){
    global $wpdb;
    $course_page_id = edd_get_option('mch_course_page_redirect');
    if(is_page(edd_get_option('mch_login_page'))){
        $user = get_userdata(get_current_user_id());
        if (isset($user->roles) && is_array($user->roles)) {
            if (in_array('administrator', $user->roles)){
                wp_redirect(admin_url());
            } elseif (in_array('author', $user->roles)){
                wp_redirect(get_permalink(edd_get_option('mct_edd_author_login_redirect')));
            } elseif (in_array('students', $user->roles)) {
                //Redirect students to default lesson or lesson which his teacher set
                $next_lesson_id = get_user_meta($user->ID, 'next_lesson_id', TRUE);
                if ($next_lesson_id && !empty($next_lesson_id)) {
                     $next_lesson_link = get_permalink($course_page_id) . '?download_number=' . $next_lesson_id['download_id'] .
                        '&level_grade=' . $next_lesson_id['course_id'] . '&module_number=' . $next_lesson_id['module_id'] . '&actual_lesson=' . $next_lesson_id['unit_id'];
                    wp_redirect($next_lesson_link);
                } else {
                    $default_lesson = (int)get_option('default_lesson');
                    if(!$default_lesson){
                        wp_redirect(get_permalink($course_page_id));
                    }
                    $sql = $wpdb->prepare("SELECT `parent_module_id`, `parent_course_id` FROM `" . $wpdb->prefix . "wpcw_units_meta` WHERE `unit_id` = %d", $default_lesson);
                    $query = $wpdb->get_row($sql);
                    $default_lesson_link = get_permalink($course_page_id) . '?download_number=16&level_grade=' .
                        $query->parent_course_id . '&module_number=' . $query->parent_module_id . '&actual_lesson=' . $default_lesson;
                    wp_redirect($default_lesson_link);
                }
            } else {
                $user_id = $user->ID;
                if ($user_id && !empty($user_id)) {
                    $user_last_lesson_id = get_user_meta($user_id, 'last_lesson_id', TRUE);

                    if ($user_last_lesson_id && !empty($user_last_lesson_id)) {
                        $last_lesson_link = get_permalink($course_page_id) . '?download_number=' . $user_last_lesson_id['download_id'] .
                            '&level_grade=' . $user_last_lesson_id['course_id'] . '&module_number=' . $user_last_lesson_id['module_id'] . '&actual_lesson=' . $user_last_lesson_id['unit_id'];
                        wp_redirect($last_lesson_link);
                    } else {
                        wp_redirect(home_url());
                    }
                }
            }
        }
    }
}

function mch_js_site_url(){
    wp_localize_script('mct_hubscripts', 'site_url', get_template_directory_uri());
}

add_action('init', 'mch_js_site_url');

add_shortcode('try_before_you_buy_section', 'try_before_you_buy_section_shortcode');
/*
 * Hide Try Before You Buy section in product description when user is logged in.
 */
function try_before_you_buy_section_shortcode( $atts, $content = NULL ) {
    if( !is_user_logged_in()){
        if(!$atts['button_title']){
            $title = '';
        } else {
            $title = $atts['button_title'];
        }
        ob_start();
        echo $content;
        ?>
        <p style="text-align: center;"><?php echo do_shortcode("[register_btn title='" . $title . "']");?></p>
        <p style="text-align: center;"></p>
        <?php
        $content = ob_get_clean();
        return $content;
    }
}

add_filter( 'wp_mail_from', 'mct_custom_wp_mail_from' );

function mct_custom_wp_mail_from( $original_email_address )
{
    return get_option('admin_email');
}


function custom_wp_mail_from_name($name)
{
    return __('My Chinese Hub', 'mch');
}

add_filter('wp_mail_from_name','custom_wp_mail_from_name');


add_action( 'wp_insert_comment', 'photo_comment_mail', 10, 2 );
/*
 * Send email to user who uploaded photo when users comment this photo.
 */
function photo_comment_mail($id, $comment){
    $photo_post = get_post($comment->comment_post_ID);
    if($photo_post->post_type === 'photo'){
        $photo_author = get_userdata($photo_post->post_author);
        if(get_current_user_id() !== $photo_author->ID){
            $author_nickname = get_user_meta($photo_author->ID, 'nickname', TRUE);
            $nickname = get_user_meta($comment->user_id, 'nickname', TRUE);
            $user_mail = $photo_author->user_email;
            $subject = __('Someone has commented on your Photo on the Hub', 'mch');
            $link = get_permalink($photo_post->ID);
            $message = __('Hi ', 'mch') . $author_nickname . "!\n" .
                    $nickname . __(' has commented on an image that you uploaded. Check it out: ', 'mch') . $link . "\n" .
                    __('Have a great day!', 'mch') . "\n" .
                    __('My Chinese Hub team', 'mch');
            wp_mail($user_mail, $subject, $message);
        }
    }
    return TRUE;
}

/*
 * Courses template text and button if user don't have access.
 */
function upgrade_subscription(){
    ob_start(); ?>
        <h4><?php _e("Unfortunately, your subscriptions doesn't give you access to this content. Would you like to upgrade your subscription?");?></h4>
        <div>
            <a class="upgrade_button" href="<?php echo get_permalink( edd_get_option('mct_edd_pricing_page_redirect', TRUE));?>"><?php _e('Upgrade', 'mch'); ?></a>
        </div>
    <?php return ob_get_clean();
}

add_action('admin_menu', 'add_default_lesson_field');

/*
 * Add default lesson field to the general admin page.
 */
function add_default_lesson_field(){
    $option_name = 'default_lesson';
    register_setting('general', $option_name);

    add_settings_field(
        'default_lesson_id',
        __('My Courses default lesson'),
        'default_lesson_function',
        'general',
        'default',
        array(
            'id' => 'default_lesson_id',
            'option_name' => 'default_lesson',
        )
    );
}

/*
 * Default lesson callback function.
 */
function default_lesson_function( $val ){
    global $wpdb;
    $id = $val['id'];
    $option_name = $val['option_name'];
    $sql = $wpdb->prepare("SELECT `p`.`post_title`, `u`.`unit_id`, `m`.`module_order`, `u`.`parent_module_id`, `c`.`course_title`, `c`.`course_id` FROM `" . $wpdb->prefix . "wpcw_courses` AS `c`"
            . " LEFT JOIN `" . $wpdb->prefix . "wpcw_units_meta` AS `u` ON `c`.`course_id` = `u`.`parent_course_id`"
            . " LEFT JOIN `" . $wpdb->prefix . "wpcw_modules` AS  `m` ON  `u`.`parent_module_id` =  `m`.`module_id` "
            . " LEFT JOIN `" . $wpdb->prefix . "posts` AS `p` ON `p`.`ID` = `u`.`unit_id` "
            . " WHERE `c`.`course_group_id` = %d ORDER BY `course_title`, `module_order`", 16);
    $query = $wpdb->get_results($sql);
    ?>
    <select name = "<?php echo $option_name ?>" id = "<?php echo $id ?>">
        <?php foreach($query as $row): ?>
            <option value="<?php echo $row->unit_id ?>" <?php selected( $row->unit_id, get_option($option_name) );?>>
                <?php echo $row->post_title . ' (Module ' . $row->module_order . ' - ' . $row->course_title . ')' ?>
           </option>
        <?php endforeach;?>
    </select>
    <p class="description"><?php _e('Choose the lesson which should show by default.');?></p>
    <?php
}


//Change pricing page title
function custom_wp_title( $title ) {
    if(is_page( 'pricing' )){
        add_filter('aioseop_title', 'get_custom_wp_title');
    }
}
add_filter('wp', 'custom_wp_title', 10);

function get_custom_wp_title($title){
    return __(' Pricing - Online Chinese Courses - Mandarin Language Courses', 'mch');
}

// function accordion2() {
//     wp_register_script( 'accordion2', get_template_directory_uri() . '/js/accordion.js', array(), 'all' );
//     wp_enqueue_script( 'accordion2' );
// }

function main() {
    wp_register_script( 'main', get_template_directory_uri() . '/js/main.js', array(), 'all' );
    wp_enqueue_script( 'main' );
}
// add_action( 'wp_enqueue_scripts', 'accordion2' );
add_action( 'wp_enqueue_scripts', 'main' );

/**
 * Enqueue script for pupup dialog window on the page lessons history.
 */
function lesson_history_save() {
    wp_enqueue_script( 'add_popup_lesson_history_js', get_template_directory_uri() . '/js/lesson_history.js', array( 'jquery' ), NULL, FALSE );
}
add_action( 'wp_enqueue_scripts', 'lesson_history_save' );

/*
 * Add Pop-up window to the teacher portal content.
 */
function add_popup_low_rank( $content ) {

	if( is_page('teacher-portal' ) ){
		$content .= '<div class="white-popup-block mfp-hide low-rank-message students-list-wrapper" id="low-rank-message"><p>';
		$content .= __("Can you explain what went wrong with the technology or classroom teacher please?", 'mch');
		$content .= '</p><p><textarea class="low-rank-message lesson-comment" rows="6" cols="30" name="cancel-comment"></textarea></p><p><a class="save-rank-message e-btn" type="button" href="#">';
		$content .= __('Save', 'mch');
		$content .= '</a></p></div>';
	}

	return $content;
}
add_filter( 'the_content', 'add_popup_low_rank' );

/*
 * Add Pop-up confirmation window to the teacher portal content.
 */
function add_popup_confirmation( $content ) {

	if( is_page('teacher-portal' ) ){
		$content .= '<div class="white-popup-block mfp-hide confirmation-message students-list-wrapper" id="confirmation-message"><p>';
		$content .= __("Are the following details correct:", 'mch') . '</p>';
		$content .= '<p class="lesson-comleted">' . __( 'Lesson completed - ', 'mch' );
		$content .= '<span class="path"></span>';
		$content .= '<span>' . __( ' Module ', 'mch' ) . '</span><span  class="module"></span>';
		$content .= '<span>' . __( ' Lesson ', 'mch' ) . '</span><span class="lesson">></span>';
		$content .= '</p>';
		$content .= '<p class="lesson-upcoming">' . __( 'Upcoming lesson - ', 'mch' );
		$content .= '<span class="next_path"></span>';
		$content .= '<span>' . __( ' Module ', 'mch' ) . '</span><span  class="next_module"></span>';
		$content .= '<span>' . __( ' Lesson ', 'mch' ) . '</span><span class="next_lesson">></span>';
		$content .= '</p>';

		$content .= '<div class="container_lesson_comment"><p>' . __( 'Comments: ', 'mch' ) . '</p><p class="lesson_comment"></p></div>';
		$content .= '<p><a class="confirmation-message-button e-btn" type="button" href="#">';
		$content .= __('Save', 'mch');
		$content .= '</a></p></div>';
	}

	return $content;
}
add_filter( 'the_content', 'add_popup_confirmation' );

if(
function_exists('gglcptch_signup_display') &&
function_exists('gglcptch_signup_display') &&
function_exists('gglcptch_signup_check')) {
  add_action('signup_extra_fields', 'gglcptch_signup_display');
  add_action('signup_blogform', 'gglcptch_signup_display');
  add_filter('wpmu_validate_user_signup', 'gglcptch_signup_check');
}
