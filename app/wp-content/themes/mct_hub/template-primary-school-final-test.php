<?php
/**
 * Unit Template Name: Primary School Final Test
 */

global $post;

if (isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])) {
    $post_id = $_GET['actual_lesson'];
    $download_id = $_GET['download_number'];
    $course_id = $_GET['level_grade'];
    $module_id = $_GET['module_number'];
    mct_last_lesson_write($download_id, $course_id, $module_id, $post_id);
} else {
    $post_id = $post->ID;
    get_header();
}

$post = get_post($post_id);
/**
 * Get User Info
 */

$user = wp_get_current_user();

get_currentuserinfo();

/**
 * Get Custom data of template
 */

$data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));

/**
 *
 * VIEW OF THE TEMPLATE START
 *
 */

if (is_user_logged_in()) :?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if ($data): ?>

            <?php if (!empty($data['psh_first_test_url'])): ?>


                <?php _e($post->post_content); ?>


                <?php if (mch_user_has_access($user->ID, 'end_test')): ?>
                    <div class="wrap-graduation">
                        <i class="fa fa-graduation-cap"></i>

                        <h2><?php _e('Take the Written Comprehension Test'); ?></h2>

                        <div class="wrap-copy-link">
                            <input class="e-btn e-btn-copy" id="btn_copy_link_first" type="button"
                                   value="<?php _e('COPY LINK'); ?>">

                            <div class="link-copy"><input type="text" id="input_copy_link_first"
                                                          value="<?php echo $data['psh_first_test_url']; ?>"
                                                          name="end_module_test_link_first" readonly></div>
                        </div>
                    </div>
                <?php else: ?>
                    <h2><?php _e('Take the Written Comprehension Test'); ?></h2>
                    <?php echo upgrade_subscription(); ?>
                <?php endif; ?>

            <?php endif; ?>

            <!-- Second test link -->

            <?php if (!empty($data['psh_second_test_url'])): ?>
                <br/>

                <?php _e($post->post_content); ?>


                <?php if (mch_user_has_access($user->ID, 'end_test')): ?>
                    <div class="wrap-graduation">
                        <i class="fa fa-graduation-cap"></i>

                        <h2><?php _e('Take the Aural Comprehension Test'); ?></h2>

                        <div class="wrap-copy-link">
                            <input class="e-btn e-btn-copy" id="btn_copy_link_second" type="button"
                                   value="<?php _e('COPY LINK'); ?>">

                            <div class="link-copy"><input type="text" id="input_copy_link_second"
                                                          value="<?php echo $data['psh_second_test_url']; ?>"
                                                          name="end_module_test_link_second" readonly></div>
                        </div>
                    </div>
                <?php else: ?>
                    <h2><?php _e('Take the Aural Comprehension Test'); ?></h2>
                    <?php echo upgrade_subscription(); ?>
                <?php endif; ?>
            <?php endif; ?>
            <br/>

            
            <h2><?php _e('Instructions'); ?></h2>
            <?php _e('1) Direct your students to the links above on their laptop(s) or ipad(s)'); ?>
            <br/>
            <?php _e('2) Get each one of them to fill out their name and email address
                        (if they don\'t have an email address,
                        then just make it up)'); ?>
            <br/>
            <?php _e('3) Students will then start the test - this will take at most 30 minutes'); ?>
            <br/>
            <?php _e('4) Once students have completed a test, they are taken to a screen that
                         says how well they did and outlines the
                        answers to the questions they got incorrect.'); ?>
            <br/> <br/>

            <?php _e('If you don\'t manage to record the results, then please send an email to your
                        Chinese Teacher and provide
                        them with the students\' first and last names, so they can lookup
                        on the system and get back to you with their results.'); ?>


        <?php endif; ?>

    </article>

<?php else: ?>

    <!-- article -->
    <article>

        <h2><?php _e('You have no access on this page.'); ?></h2>

    </article>
    <!-- /article -->

<?php endif;

if (!isset($_GET['actual_lesson']) || empty($_GET['actual_lesson'])) {
    get_footer();
}

/**
 *
 * VIEW OF THE TEMPLATE END
 *
 */