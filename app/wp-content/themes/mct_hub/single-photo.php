<?php get_header(); ?>

	<div role="main" class="single-p-d main">
        <!-- section -->
        <section>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if( function_exists('check_if_user_can_upload') ){
                    $has_access = check_if_user_can_upload();
                }
                if($has_access): ?>
                    <div class="content-wrapper">

                        <div class="content-header">
                            <!-- post title -->
                            <div>
                                <a href="<?php echo get_permalink(edd_get_option('mch_photos_page')); ?>" class="e-btn"><?php _e('Back', 'mch'); ?></a>
                            </div>
                            <!-- /post title -->
                        </div>

                        <div class="photo-block">
                            <div class="photo-content">
                                    <div>
                                        <?php 
                                        $name = get_user_meta(get_post(get_the_ID())->post_author, 'first_name', true);
                                        $name .= ' ' . get_user_meta(get_post(get_the_ID())->post_author, 'last_name', true);
                                        $school = get_user_meta(get_post(get_the_ID())->post_author, 'school', true);
                                        if($school){
                                            $name .= ', ' . $school;
                                        }
                                        $date = date(' (F d, Y)', strtotime(get_post(get_the_ID())->post_date));
                                        echo __('By: ') . $name . $date;
                                        ?>
                                    </div>
                                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                                    <?php else : ?>
                                        <?php $video = get_attached_media( 'video', get_the_ID() ); 
                                        if ( !empty( $video ) && is_array( $video ) ) : $video_id = key($video); ?>
                                              
                                            <?php if ( ! empty( $video[ $video_id ]->guid )  ) : ?>
                                                <video controls width="640">
                                                    <source src="<?php echo $video[ $video_id ]->guid; ?>">
                                                </video>
                                            <?php else: ?>
                                                <?php _e( 'Nothing to display.' ); ?>
                                            <?php endif; ?>
                                            
                                       <?php endif; ?>
                                    <?php endif; ?>

                                    <?php the_content(); // Dynamic Content ?>
                            </div>
                                                    <div class="nav-block">
                                                            <div class="prev">
                                                                    <?php echo next_post_link('%link', __('Previous', 'mch')) ?>
                                                            </div>
                                                            <div class="next">
                                                                    <?php echo previous_post_link('%link', __('Next', 'mch')) ?>
                                                            </div>
                                                    </div>
                            <?php

                                if( function_exists('mch_comment_form') ){
                                    mch_comment_form(get_the_ID());
                                }

                                if( function_exists('mch_get_list_comments') ){
                                    mch_get_list_comments(get_the_ID());
                                }

                            ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="error-msg"><h2><?php _e('Please login to view Photos.', 'mch'); ?></h2></div>
                <?php endif; ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        </section>
        <!-- /section -->
	</div>

<?php get_footer(); ?>
