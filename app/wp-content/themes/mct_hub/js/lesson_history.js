jQuery(document).ready(function ($) {

    $('.chosen-results > li.active-result').live('click', function () {

        var technologyClass = $('.technology-rating + .chosen-container > a');
        var classRoomClass = $('.ct-rating + .chosen-container > a');

        // Clean textarea before show Popup.
        var preLowRankParent = $('#low-rank-message');
        $('textarea', preLowRankParent).val('');

        var selectBlock = $(this).closest('.select-block');
        var id = selectBlock.children('select').attr('id');
        /*
         var path_number = $('#' + id + '.path-level-select').find(':selected').data('path-number');
         var user_id = $('#' + id + '.user-id').val();
         var teacher_id = $('#' + id + '.teacher-id').val();
         var course_id = $('#' + id + '.path-level-select').val();
         var module_id = $('#' + id + '.module-select').val();
         var lesson_id = $('#' + id + '.lesson-select').val();
         var technology_rating = $('#' + id + '.technology-rating').val();
         var ct_rating = $('#' + id + '.ct-rating').val();
         */
        //var comment = $('#' + id + '.lesson-comment').val();

        if ((technologyClass.hasClass('star1') || technologyClass.hasClass('star2') || classRoomClass.hasClass('star1') || classRoomClass.hasClass('star2')) && !$('.user-low-rank').is('#' + id)) {

            if (!$.magnificPopup.instance.isOpen) {
                //Poor tech popup
                $.magnificPopup.open({
                    items: {
                        src: $('#low-rank-message')
                    },
                    key: id,
                    closeOnBgClick: false,
                });
            } else {
                $.magnificPopup.instance.close();
            }
        }

        // Remove lowrank hidden.
        if (!(technologyClass.hasClass('star1') || technologyClass.hasClass('star2')) && !(classRoomClass.hasClass('star1') || classRoomClass.hasClass('star2'))) {
            $('input.user-low-rank#' + id).remove();
        }
    });

    var poor_tech;

    $('.save-rank-message').click(function (e) {
        e.preventDefault();
        poor_tech = 1;
        // Current student.
        var popup = $.magnificPopup.instance;
        var id = popup.st.key;

        // Message.
        var saveLowRankParent = $(this).parents('#low-rank-message');
        var saveLowRankContent = $('textarea', saveLowRankParent).val();
        var hiddenInput = $('input.user-id#' + id);
        var a = $('.user-low-rank');
        if ($(a).is('#' + id)) {
            $('input.user-low-rank#' + id).replaceWith($('<input type="hidden" name="user_id" class="user-low-rank" id="' + id + '" value="' + saveLowRankContent + '"/>'));
            //cancel_btn.trigger('click', cancelContent );
        } else {
            hiddenInput.after('<input type="hidden" name="user_id" class="user-low-rank" id="' + id + '" value="' + saveLowRankContent + '"/>');
        }
        $.magnificPopup.instance.close();
    });
});
