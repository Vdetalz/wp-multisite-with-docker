//accordion
jQuery(document).ready(function($) {
    //initAccordion();
		fadeMenu();
		closeAccordion();

    // function initAccordion() {
    //     $('div.user-history-wrapper').slideAccordion({
    //         opener: 'div.he-header',
    //         slider: 'div.he-body',
    //         animSpeed: 300
    //     });
    // }

    $(".wrap-select-block select").on("change", function(){
        var starCount = $(this).val();
        $(this).closest(".select-block").find(".chosen-single").attr("class", "chosen-single").addClass('star' + starCount);
    });

    function fadeMenu() {
		$('.he-header').each(function(){
			$(this).click(function(){
				$(this).css('display','none').siblings('.he-body').slideDown(300);
			})
		});
	};
    function closeAccordion() {
        $('.he-info-close').on('click', function(){
            $(this).parent().slideUp(300);
        })
    };
});