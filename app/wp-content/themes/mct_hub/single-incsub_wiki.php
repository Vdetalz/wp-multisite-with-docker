<?php get_header(); ?>

	<div role="main" class="single-p-d main">
        <!-- section -->
        <section>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <?php if ( current_user_can('edit_wiki_privileges')): ?>

                <div role="main" class="content-wiki">
                    <?php dynamic_sidebar('widget-area-wiki') ?>
                </div>

                <div class="content-wrapper">

                <!-- article -->
                    <?php if(!isset($_GET['action']) || (isset($_GET['action']) &&  $_GET['action'] !== 'edit')): ?>
                        <h1><?php the_title(); ?> </h1>
                    <?php endif; ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                        <?php endif; ?>

                        <?php the_content(); // Dynamic Content ?>

                    </article>
                <!-- /article -->
                </div>
            <?php else: ?>
                <div class="error-msg"><h2><?php _e('Please login to view Wiki.', 'mch'); ?></h2></div>
            <?php endif; ?>

        <?php endwhile; ?>



        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        </section>
        <!-- /section -->
	</div>

<?php get_footer(); ?>
