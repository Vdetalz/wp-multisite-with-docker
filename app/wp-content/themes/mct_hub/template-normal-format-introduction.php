<?php
/**
 * Unit Template Name: Normal Format Introduction
 */

global $post;

if(isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])){
    $post_id = $_GET['actual_lesson'];
    $download_id = $_GET['download_number'];
    $course_id = $_GET['level_grade'];
    $module_id = $_GET['module_number'];
    mct_last_lesson_write($download_id, $course_id, $module_id, $post_id);
}
else{
    $post_id = $post->ID;
    get_header();
}

$post = get_post($post_id);

/**
 * Get User Info
 */

$user = wp_get_current_user();
get_currentuserinfo();

/**
 * Get Custom data of template
 */

$data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
$words = get_post_meta($post->ID, 'vocab_list', true);
$module_learning_outcomes = get_post_meta($post->ID, 'unit_template_module_learning_outcomes', true);

/**
 *
 * VIEW OF THE TEMPLATE START
 *
 */

if(is_user_logged_in()) :?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="lesson-content">
            <?php _e($post->post_content);?>
        </div>

        <?php if($data):?>

                <div class="explain-lesson">
                    <div class="box-explain-lesson">
                        <div class="section-explain-lesson">
                            <h4><?php _e('Module Learning Outcomes');?></h4>
                            <div class="content-explain-lesson">
                                <?php echo $module_learning_outcomes;?>
                            </div>
                        </div>
                    </div>

                <?php if($words):?>

                    <div class="box-explain-lesson">
                        <div class="section-explain-lesson">
                            <h4><?php _e('Module Learning Vocab List');?></h4>
                            <?php if(mch_user_has_access($user->ID, 'vocabulary')):?>
                                <table class="box-translate">
                                    <tr>
                                        <th>English</th>
                                        <th>Pinyin</th>
                                        <th>Chinese</th>
                                    </tr>
                                    <?php foreach($words as $word):?>
                                        <tr>
                                            <td>
												<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[0]);?></a>
												<?php else:?>
													<?php _e($word[0]);?>
												<?php endif;?>		
											</td>
                                            <td>
											<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[1]);?></a>
												<?php else:?>
													<?php _e($word[1]);?>
												<?php endif;?>	
											</td>
                                            <td>
												<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[2]);?></a>
												<?php else:?>
													<?php _e($word[2]);?>
												<?php endif;?>		
											</td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            <?php else: 
                                echo upgrade_subscription();?>
                            <?php endif;?>
                        </div>
                    </div>

                <?php endif;?>

                </div>

        <?php endif;?>

    </article>

<?php else:?>

    <!-- article -->
    <article>

        <h2><?php _e('You have no access on this page.');?></h2>

    </article>
    <!-- /article -->

<?php endif;

if(!isset($_GET['actual_lesson']) || empty($_GET['actual_lesson'])){
    get_footer();
}

/**
 *
 * VIEW OF THE TEMPLATE END
 *
 */