</div>
<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="content-wrapper">
                    <div class="contact-me-form white-popup mfp-hide">
                        <div class="form-wrapper">
                            <?php echo do_shortcode('[gravityform id="1" title="false" ajax="true" description="false"]'); ?>
                        </div>
                    </div>
<!--                            <form method="post" class="contact-me-form white-popup mfp-hide">-->
<!--                                <div class="form-wrapper">-->
<!--	                                <h2>--><?php //_e('Contact Us');?><!--</h2>-->
<!--                                        -->
<!--                                        <div class="message"></div>-->
<!--                                        -->
<!--                                        <div class="input-wraper">-->
<!--                                            -->
<!--                                            <div class="wrap-row">-->
<!--		                                        <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('First Name');?><!--" name="first_name" type="text" value="" />-->
<!--	                                            </div>-->
<!---->
<!--	                                            <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('Last Name');?><!--" name="last_name" type="text" value="" />-->
<!--	                                            </div>-->
<!---->
<!--	                                            <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('School / Institution');?><!--" name="school" type="text" value="" />-->
<!--	                                            </div>-->
<!---->
<!--	                                            <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('Town');?><!--" name="town" type="text" value="" />-->
<!--	                                            </div>-->
<!---->
<!--	                                            <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('Email address');?><!--" name="z_email" type="email" value="" />-->
<!--	                                            </div>-->
<!---->
<!--	                                            <div class="input-wrap">-->
<!--	                                                    <input placeholder="--><?php //_e('Phone Number');?><!--" name="phone_number" type="text" value="" />-->
<!--	                                            </div>-->
<!--                                            </div>-->
<!--                                            <div class="input-wrap-textarea">-->
<!--                                                <textarea placeholder="--><?php //_e('Message');?><!--" name="text_message" cols="65" rows="3" ></textarea>-->
<!--                                            </div>-->
<!--                                            -->
<!--                                            <input type="hidden" name="z_is_zendesk_form" value="contact_us">-->
<!---->
<!--                                            <input type="submit" value="--><?php //_e('Submit');?><!--" />-->
<!--                                            <input type="hidden" name="submitted" value="true" />-->
<!--                                            -->
<!--                                        </div>-->
<!--                                </div>-->
<!--                            </form>-->

                            <div class="footer-widget">
                                <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-footer')) ?>
                            </div>

				<div class="footer-nav">
					<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-footer-2')) ?>
				</div>
				<!-- copyright -->
				<p class="copyright">
					<?php echo str_replace('[year]', date('Y'), get_option('mc_footer_text')); ?>
				</p>
				<!-- /copyright -->
				</div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>
	</body>
</html>
