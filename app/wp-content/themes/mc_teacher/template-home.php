<?php /* Template Name: Home page */?>
<?php get_header(); ?>

<?php

// get posts of "homepage slider" content type
$args = array(
    'post_type' => 'homepage_slider',
    'posts_per_page' => 1
);

$sliders_query = new WP_Query( $args );

?>
<?php if ( $sliders_query->have_posts() ) : ?>
    <ul class="digital-products-list-big">
        <?php while ( $sliders_query->have_posts() ) : $sliders_query->the_post(); ?>
            <?php //get video url from "homepage slider" custom field
            $video_url = get_field('video_file', $post->ID);  ?>
            <?php if($video_url): ?>
                <?php $IE8 = (preg_match('/MSIE 8/i',$_SERVER['HTTP_USER_AGENT'])) ? true : false;?>
                <?php if(!wp_is_mobile() && !$IE8): ?>
                <li>
                <?php echo do_shortcode( '[videojs mp4="' .  $video_url . '" autoplay="true" controls="false" loop="true" preload="auto" muted="true"]' ); ?>
                <?php else: ?>
                <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
                <li class="has-bg" style="background: url( <?php echo $large_image_url[0] ?> ) no-repeat center top; background-size: cover;">
                <?php endif; ?>
            <?php else: ?>
            <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
                <li class="has-bg" style="background: url( <?php echo $large_image_url[0] ?> ) no-repeat center top; background-size: cover;">
            <?php endif; ?>
                <div class="wrappper">
                    <div class="content-wrapper">
                        <?php the_content();?>
                    </div>
                </div>
            </li>
        <?php  endwhile;?>
    </ul>
<?php endif;?>

    <?php
    if (function_exists('dynamic_sidebar'))
        dynamic_sidebar('media_section');
    ?>

	<div role="main" class="content-home">
		<div class="content-wrapper">
		<!-- section -->
		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</div>
<?php get_footer(); ?>
