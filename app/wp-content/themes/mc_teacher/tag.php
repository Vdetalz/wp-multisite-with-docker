<?php get_header(); ?>

	<div role="content-wrapper">
		<!-- section -->
		<section>

			<h1><?php _e( 'Tag Archive: ', 'mc_teacher' ); echo single_tag_title('', false); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
