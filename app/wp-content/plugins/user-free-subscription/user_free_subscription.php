<?php
/*
 * Plugin name: Free User Subscription
 * Version: 1.0
 * Author: Web4Pro
 * Description: The plugin give a free subscription for user.
 */

add_action( 'show_user_profile', 'ufs_add_profile_subscription_field' );
add_action( 'edit_user_profile', 'ufs_add_profile_subscription_field' );


/**
 * Render subscription field in admin panel on user edit page
 * @param $user
 */

function ufs_add_profile_subscription_field( $user )
{

    $blog_id = get_current_blog_id();

    if( !current_user_can( 'edit_pages' ) ) return;

    if($blog_id != 1) {
        $current_downloads = ufs_get_expiration_downloads($user->data->ID);
    } ?>

    <h3>Free Subscription</h3>

    <table class="form-table">

        <tr>
            <th><label for="google_profile"><?php _e('Select type of subscription'); ?></label></th>
            <td>
                <?php

                $products = ufs_get_all_downloads();
                if ($products):
                    for ($i = 0; $i < count($products); $i++) {
                        ?>
                        <div class="dwn-prod-wrapper">

                            <span class="dwn-single-product">
                                <select name="ufs_downloads[]" id="">
                                    <option value=""><?php _e('No Subscription'); ?></option>
                                    <?php foreach ($products as $product): ?>
                                        <?php if ($product['info']['status'] == 'publish'): ?>
                                            <?php if ($current_downloads) {
                                                for ($j = $i; $j < count($products); $j++) {
                                                    $selected = '' ?>
                                                    <?php if ($current_downloads[$i]->download_id == $product['info']['id']) :
                                                        $selected = 'selected';
                                                        $selected_download_id = $product['info']['id'];
                                                        break; endif; ?>
                                                <?php
                                                }
                                            } ?>
                                            <option
                                                value="<?php echo $product['info']['id']; ?>" <?php echo $selected; ?>><?php echo $product['info']['title']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </span>

                            <span class="prod-price-id">
                                <select name="ufs_price_id[]">
                                    <option value=""><?php _e('-- Select price --'); ?></option>
                                    <?php
                                    if($selected_download_id) {
                                        $current_prices_array = edd_get_variable_prices($selected_download_id);
                                        foreach ($current_prices_array as $price_key => $current_price) {
                                            $selected_price = '';
                                            foreach($current_downloads as $current_download) {
                                                if($current_download->download_id == $selected_download_id) {
                                                    $current_exp_date = date('m/d/Y', $current_download->expired_date);
                                                    if ($current_download->price_id == $price_key) {
                                                        $selected_price = 'selected';
                                                        break;
                                                    }
                                                }
                                            }?>
                                            <option
                                                value="<?php echo $price_key; ?>" <?php echo $selected_price; ?>><?php echo $current_price['name']; ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </span>

                            <span class="prod-exp-date">
                                <input type="text" name="prod_expiry_date[]" class="form-control" placeholder="<?php _e('Expiration date'); ?>" style="width:18%" value="<?php echo $current_exp_date; ?>">
                            </span>
                        </div>
                    <?php } ?>
                    <input type="button" class="btn btn-primary add-product" value="<?php echo __('Add Subscription'); ?>">
                <?php endif; ?>

            </td>
        </tr>


    </table>
<?php
}


/**
 * Save field subscription and give access for user
 * @param $user_id
 */

function save_profile_subscription_field( $user_id ){

    if( !current_user_can('edit_pages') ) return;

    delete_current_subscriptions( $user_id );

    if( get_user_meta( $user_id, 'ufs_payment_meta', true ) ){
        wp_delete_post( get_user_meta( $user_id, 'ufs_payment_meta', true ) );
        delete_user_meta($user_id, 'ufs_payment_meta');
        edd_delete_purchase( get_user_meta( $user_id, 'ufs_payment_meta', true ) );
    }

    if( $payment_id = get_user_meta($user_id, 'ufs_insert_user_payment_meta', true) )
    edd_delete_purchase($payment_id);

    if( isset($_POST['ufs_downloads']) && !empty($_POST['ufs_downloads'][0])){
        ufs_give_user_free_access( $_POST['ufs_downloads'], $_POST['ufs_price_id'], $user_id, $_POST['prod_expiry_date'] );
    }

}

add_action( 'personal_options_update', 'save_profile_subscription_field' );
add_action( 'edit_user_profile_update', 'save_profile_subscription_field' );

function delete_current_subscriptions( $user_id = false ){

    if( !current_user_can('edit_pages') ) return;

    $user_id = !$user_id ? get_current_user_id() : $user_id;
    if( !$current_subscriptions = ufs_get_expiration_downloads( $user_id) ) return false;

    global $wpdb;

    foreach( $current_subscriptions as $key => $value ){

        $wpdb->delete( $wpdb->prefix . 'download_expiration', array(
            'id' => $value->id
        ) );

    }

}


/**
 * Get current subscriptions
 * @param bool $user_id
 * @return bool
 */

function ufs_get_expiration_downloads($user_id = false){

    $blog_id = get_current_blog_id();

    if($blog_id != 1) {
        $user_id = !$user_id ? get_current_user_id() : $user_id;

        if (!$user_id) return false;

        global $wpdb;

        $table = $wpdb->prefix . 'download_expiration';

        $sql = "SELECT * FROM " . $table . "
                               WHERE user_id = '$user_id'
                               AND expired_date > " . time();

        return $wpdb->get_results($sql);
    }
    else{
        $user_id = !$user_id ? get_current_user_id() : $user_id;

        if (!$user_id) return false;

        global $wpdb;

        $table = $wpdb->prefix . 'users_hours_balance';

        $sql = "SELECT * FROM " . $table . "
                               WHERE user_id = '$user_id'
                               AND expired_date > " . time();

        return $wpdb->get_results($sql);
    }

}


/**
 *
 * @return bool
 */

function ufs_get_all_downloads(){

    $EDD_API = new EDD_API;
    $products = $EDD_API->get_products();

    if( isset($products['products'][0]['info']) ) return $products['products'];
    else return false;

}


/**
 * Give user access to the specific (or all)
 * courses on specific (unlimited) period
 *
 * @param array $download_ids
 * @param bool $end_timestamp
 * @return bool
 */

function ufs_give_user_free_access( array $download_ids, $price_array = false, $user_id = false, $end_timestamp_array = false){

    global $wpdb;
    $blog = get_current_blog_id();

    if($blog != 1) {
        //If array with products is empty
        if (!count($download_ids)) return false;

        //If user is not loginned
        if (!$user_id && !get_current_user_id()) return false;

        //if user id is empty
        if (!$user_id)
            $user_id = get_current_user_id();

        $user = new WP_User($user_id);

        //If have no timestamp of ending subscription than give unlimited access


        //Dance with each download ID
        foreach ($download_ids as $key => $download_id) {

            if (!$end_timestamp_array[$key]) {
                $end_timestamp = strtotime('+10 years 00:00:00');
            } else {
                $end_timestamp = strtotime($end_timestamp_array[$key]);
            }

            $download = edd_get_download($download_id);

            $temp_prices = edd_get_variable_prices($download_id);

            foreach ($temp_prices as $temp_key => $temp_value)
                $temp_array_prices[] = $temp_key;

            //Max price ID
            if (!$price_array[$key]) {
                $price_id = max($temp_array_prices);
            } else {
                $price_id = $price_array[$key];
            }

            unset($temp_array_prices);

            //If no price id in product
            if (!$price_id) continue;

            //Insert expiration date for product (download)
            $insert[$key] = $wpdb->insert(
                $wpdb->prefix . 'download_expiration',
                array(
                    'user_id' => $user_id,
                    'download_id' => $download_id,
                    'expired_date' => $end_timestamp,
                    'price_id' => $price_id
                )
            );

            $period = mcn_get_normalized_period_array($download_id);

            if ($insert[$key]) {
                $purchase_downloads_temp = array(
                    'id' => $download_id,
                    'quantity' => 1,
                    'options' => array(
                        'price_id' => $price_id,
                        'recurring' => array(
                            'period' => $period[count($period) - 1],
                            'times' => 0,
                            'signup_fee' => 0
                        ),
                    ),

                );

                $purchase_downloads[] = $purchase_downloads_temp;

                $cart_details[] = array(
                    'name' => $download->post_title,
                    'id' => $download_id,
                    'item_number' => $purchase_downloads_temp,
                    'item_price' => edd_get_download_price($download_id),
                    'quantity' => 1,
                    'discount' => 0,
                    'subtotal' => edd_get_download_price($download_id),
                    'tax' => 0,
                    'fees' => array(),
                    'price' => edd_get_download_price($download_id)

                );

            }

        }

        if (count($purchase_downloads)) {

            $payment_id = edd_insert_payment(
                array(
                    'price' => 0,
                    'status' => 'publish',
                    'user_email' => $user->user_email,
                    'purchase_key' => strtolower(md5($user->user_email . date('Y-m-d H:i:s') . AUTH_KEY . uniqid('edd', true))),
                    'currency' => 'USD',
                    'downloads' => $purchase_downloads,
                    'user_info' => array(
                        'id' => $user->ID,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->user_email,
                        'discount' => 'none',
                        'address' => false,
                    ),
                    'cart_details' => $purchase_downloads,
                )
            );

        } else {

            return false;
        }


        return true;
    }
    else if ($blog == 1){
        //If array with products is empty
        if (!count($download_ids)) return false;

        //If user is not loginned
        if (!$user_id && !get_current_user_id()) return false;

        //if user id is empty
        if (!$user_id)
            $user_id = get_current_user_id();

        $user = new WP_User($user_id);

        //If have no timestamp of ending subscription than give unlimited access


        //Dance with each download ID
        foreach ($download_ids as $key => $download_id) {

            if (!$end_timestamp_array[$key]) {
                $end_timestamp = strtotime('+10 years 00:00:00');
            } else {
                $end_timestamp = strtotime($end_timestamp_array[$key]);
            }

            $download = edd_get_download($download_id);

            $temp_prices = edd_get_variable_prices($download_id);

            foreach ($temp_prices as $temp_key => $temp_value)
                $temp_array_prices[] = $temp_key;

            //Max price ID
            if (!$price_array[$key]) {
                $price_id = max($temp_array_prices);
            } else {
                $price_id = $price_array[$key];
            }

            unset($temp_array_prices);

            //If no price id in product
            if (!$price_id) continue;

            $hours_meta_value = get_post_meta($download_id, 'edd_variable_prices_' . $price_id, true);

            //Insert expiration date for product (download)
            $insert[$key] = $wpdb->insert(
                $wpdb->prefix . 'users_hours_balance',
                array(
                    'user_id' => $user_id,
                    'download_id' => $download_id,
                    'balance' => $hours_meta_value,
                    'expired_date' => $end_timestamp,
                    'price_id' => $price_id
                )
            );

            $period = mcn_get_normalized_period_array($download_id);

            if ($insert[$key]) {
                $purchase_downloads_temp = array(
                    'id' => $download_id,
                    'quantity' => 1,
                    'options' => array(
                        'price_id' => $price_id,
                        'recurring' => array(
                            'period' => $period[count($period) - 1],
                            'times' => 0,
                            'signup_fee' => 0
                        ),
                    ),

                );

                $purchase_downloads[] = $purchase_downloads_temp;

                $cart_details[] = array(
                    'name' => $download->post_title,
                    'id' => $download_id,
                    'item_number' => $purchase_downloads_temp,
                    'item_price' => edd_get_download_price($download_id),
                    'quantity' => 1,
                    'discount' => 0,
                    'subtotal' => edd_get_download_price($download_id),
                    'tax' => 0,
                    'fees' => array(),
                    'price' => edd_get_download_price($download_id)

                );

            }

        }

        if (count($purchase_downloads)) {
            $log = (object)array(
                'user_id'         => $user_id,
                'date'            => current_time('mysql'),
                'action'          => __('AdminAddSubscription'),
                'change_balance'  => '+' . $hours_meta_value,
                'balance'         => mct_get_user_balance($user_id),
                'delivery_method' => NULL
            );

            if (class_exists('CTRL_Balance_logs')) {
                $CTRL_Balance_log = new CTRL_Balance_logs();
                $CTRL_Balance_log->set($log);
            }
            $payment_id = edd_insert_payment(
                array(
                    'price' => 0,
                    'status' => 'publish',
                    'user_email' => $user->user_email,
                    'purchase_key' => strtolower(md5($user->user_email . date('Y-m-d H:i:s') . AUTH_KEY . uniqid('edd', true))),
                    'currency' => 'USD',
                    'downloads' => $purchase_downloads,
                    'user_info' => array(
                        'id' => $user->ID,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->user_email,
                        'discount' => 'none',
                        'address' => false,
                    ),
                    'cart_details' => $purchase_downloads,
                )
            );



        } else {

            return false;
        }


        return true;
    }
}


/**
 * Insert payment id into user meta
 * @param $payment_id
 * @return bool
 */

function ufs_insert_user_payment_meta( $payment_id, $payment_meta ){

    if( !$payment_id || !edd_get_payment_user_id($payment_id) ) return;
    $payment_meta['user_email'];
    $user_id = edd_get_payment_user_id($payment_id);
    if( !$user_id ) return false;

    update_user_meta( $user_id, 'ufs_payment_meta', $payment_id );

}

add_action('edd_insert_payment', 'ufs_insert_user_payment_meta', 10, 2);

function admin_enable_scripts(){
    if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
        wp_enqueue_script('bootstrap', plugins_url('/js/bootstrap.js', __FILE__));
        wp_enqueue_script('bootstrap_min', plugins_url('/js/bootstrap.min.js', __FILE__));
        wp_enqueue_script('datepicker', plugins_url('/js/bootstrap-datepicker.js', __FILE__));
        wp_enqueue_script('datepicker_init', plugins_url('/js/init.js', __FILE__));
        wp_enqueue_style('bootstrap_min_style', plugins_url('/css/bootstrap.min.css', __FILE__));
        wp_enqueue_style('datepicker_init_stype', plugins_url('/css/datepicker.css', __FILE__));
        wp_enqueue_style('bootstrap_theme', plugins_url('/css/bootstrap-theme.css', __FILE__));
        wp_enqueue_style('style', plugins_url('/css/style.css', __FILE__));
        wp_localize_script('datepicker_init', 'Ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
    }

}

add_action('admin_enqueue_scripts', 'admin_enable_scripts');

function ajax_get_price_id(){
    if(isset($_POST['download_id']) && !empty($_POST['download_id'])){
        $download_id = $_POST['download_id'];
        $prices_array = edd_get_variable_prices($download_id); ?>
        <option><?php echo __('-- Select price --'); ?></option>
        <?php
        foreach($prices_array as $key => $value){
            ?>
            <option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option>
        <?php
        }
    }
    die;
}

add_action('wp_ajax_get_price_id', 'ajax_get_price_id');