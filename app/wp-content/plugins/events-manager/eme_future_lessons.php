<?php 
function eme_future_lessons_page() {

    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    global $wpdb;

    if(!empty($_GET['start_period'])){
        $sqlWhere[] = "`event_start_date` >='" . date('Y-m-d', dut_prepare_date($_GET['start_period'], -8)) . "'";
    }

    if(!empty($_GET['end_period'])){
        $sqlWhere[] = "`event_start_date` <='" . date('Y-m-d', dut_prepare_date($_GET['end_period'], -8)) . "'";
    }
    else{
        $sqlWhere[] = "`event_start_date` >='" . date('Y-m-d', dut_prepare_date(date('Y-m-d'), -8)) . "'";
    }

    $sqlWhere = ' WHERE ' . implode(' AND ', $sqlWhere);

    $num = $wpdb->get_results("SELECT COUNT(*) FROM " . $wpdb->prefix . "em_bookings AS a
                                   LEFT JOIN " . $wpdb->prefix . "em_events AS b
                                   ON b.event_id = a.event_id
                                   " . $sqlWhere, ARRAY_A);

    $limit = 20;
    $current_page = !empty($_GET['event_page']) ? $_GET['event_page'] : 1;
    $num_rows = $num ? array_shift(array_shift($num)) : 0;
    $num_pages = ceil($num_rows/$limit);
    $step = ($current_page - 1) * $limit;



    $results = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "em_bookings AS a
                                   LEFT JOIN " . $wpdb->prefix . "em_events AS b
                                   ON b.event_id = a.event_id
                                   " . $sqlWhere . "
                                   ORDER BY event_end_date DESC
                                   LIMIT $step, $limit", ARRAY_A);

    $path = 'admin.php';
    $request_vars = array();
    parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $request_vars);

    ?>

    <div class="wrap">
        <h2><?php _e('Future Lessons');?></h2>

        <h3><?php _e('Select period');?></h3>

        <form name="completed_lessons" id="completed_lessonss" method="get"
              action="<?php echo admin_url("admin.php?page=events-manager-future-lessons"); ?>" class="validate">
            <input type="hidden" name="page" value="events-manager-future-lessons">
            Start date: <input id="localised-date" type="text" name="start_period" style="display: none;"
                               readonly="readonly" value="<?php if (isset($_GET['start_period'])) echo $_GET['start_period'];?>"/>
            <input id="date-to-submit" type="text"  style="background: #FCFFAA"/>
            End date: <input id="localised-end-date" type="text" name="end_period" style="display: none;"
                             readonly="readonly" value="<?php if (isset($_GET['end_period'])) echo $_GET['end_period'];?>"/>
            <input id="end-date-to-submit" type="text"  style="background: #FCFFAA"/>
            <input type="submit" value="Apply" id="show-lessons" class="button-secondary action">
        </form>

        <?php if($num_pages > 1):?>
            <div class="tablenav" style="margin:0;">
                <div class="tablenav-pages">
                    <?php if(empty($current_page) || $current_page == 1):?>
                    <span class="pagination-links"><a class="first-page disabled" title="Go to the first page">«</a>
                <a class="prev-page disabled" title="Go to the previous page">‹</a>
                        <?php else:?>
                        <?php $page_link = $path . '?' . http_build_query(array_merge($request_vars, array('event_page' => 1))); ?>
                        <span class="pagination-links"><a class="first-page" title="Go to the first page" href="<?php echo $page_link;?>">«</a>
                            <?php $page_link = $path . '?' . http_build_query(array_merge($request_vars, array('event_page' => ($current_page - 1)))); ?>
                            <a class="prev-page" title="Go to the previous page" href="<?php echo $page_link;?>">‹</a>
                            <?php endif;?>

                            <span class="paging-input">
                                 <input class="current-page" title="Current page" type="text" name="event_page" value="<?php echo $current_page;?>" size="1"> of
                                 <span class="total-pages"><?php echo $num_pages; ?></span>
                            </span>
                            <?php if($current_page <> $num_pages):?>
                                <?php $page_link = $path . '?' . http_build_query(array_merge($request_vars, array('event_page' => ($current_page + 1)))); ?>
                                <a class="next-page" title="Go to the next page" href="<?php echo $page_link; ?>">›</a>
                            <?php else:?>
                                <a class="next-page disabled" title="Go to the next page">›</a>
                            <?php endif;?>
                            <?php $page_link = $path . '?' . http_build_query(array_merge($request_vars, array('event_page' => $num_pages))); ?>
                            <a class="last-page" title="Go to the last page" href="<?php echo $page_link; ?>">»</a></span>
                </div>
            </div>
            <br class="clear">
        <?php endif;?>
        <table class="widefat" style="margin-top:10px;">
            <thead>
                <tr>
                    <th style="border-right:1px solid #dfdfdf;">Date and Time</th>
                    <th style="border-right:1px solid #dfdfdf;">Tutor</th>
                    <th style="border-right:1px solid #dfdfdf;">Student</th>
                    <th style="border-right:1px solid #dfdfdf;">Student nick name</th>
                    <th style="border-right:1px solid #dfdfdf;">Minutes</th>
                </tr>
            </thead>
            <tbody>

            <?php if($results): ?>

            <?php foreach ($results as $key => $result): ?>

                    <?php
                    $event = new EM_Event($result['event_id']);

                    if($event->ID == NULL) continue;

                    $user = new WP_User($event->get_bookings()->bookings[0]->person_id);?>

                    <tr>
                        <td style="border-right:1px solid #dfdfdf; width:20%;">
                            <?php echo date('Y-m-d H:i', dut_prepare_date($event->event_start_date . ' ' . $event->event_start_time, 8));?> GMT +8
                        </td>
                        <td style="border-right:1px solid #dfdfdf; width:20%;">
                            <?php echo $event->event_name;?>
                        </td>
                        <td style="border-right:1px solid #dfdfdf; width:20%;">
                            <?php echo $user->user_email;?>
                        </td>
                        <td style="border-right:1px solid #dfdfdf; width:20%;">
                            <?php echo $user->nickname;?>
                        </td>
                        <td>
                            <?php echo date('H:i', strtotime($event->event_end_time) - strtotime($event->event_start_time)); ?>
                        </td>
                    </tr>

            <?php endforeach;?>

        <?php endif;?>

            </tbody>
        </table>
    </div>
<?php
}
function future_min_id($array){


    $min = $array[0];
    for ($i = 0; $i < count($array); $i++){
        if ($min > $array[$i]){
            $min = $array[$i];
        }
    }
    return $min;
}

?>