/**     add and remove loader div, use if you get something via ajax */
function toggleLoader(elem) {
    if(elem) {
        elem.remove();
    } else {
        return jQuery('<div />', {
            'class': 'site-loader'
        }).appendTo(jQuery('body'));
    }
}
jQuery(document).ready(function($) {

	if($('.modal_wrap_all').length) {
        $('.modal_wrap_all').hide();
    }

    if($('.modal_wrap').length) {
        $('.modal_wrap').hide();
    }

    /* AJAX: save lesson history */
    if($('.plus-button').length){
        $('.plus-button').live('click', function () {
            var currentBlock = $(this).parents('.user-block');
            var id = currentBlock.attr('id');
            var event_id = $('#' + id + ' .event-id').val();
            var user_id = $('#' + id + ' .user-id').val();
            var teacher_id = $('#' + id + ' .teacher-id').val();
            var course_id = $('#' + id + ' .courses-group-select').val();
            var level_number = $('#' + id + ' .path-level-select').val();
            var module_id = $('#' + id + ' .module-select').val();
            var lesson_id = $('#' + id + ' .lesson-select').val();
            var comment = $('#' + id + ' .lesson-comment').val();

            var loader = toggleLoader();

            $.ajax({
                type   : 'post',
                url    : EM.ajaxurl,
                data   : {
                    action : 'mct_save_lesson_history',
                    user_id : user_id,
                    event_id : event_id,
                    teacher_id : teacher_id,
                    course_id : course_id,
                    level_id : level_number,
                    module_id : module_id,
                    lesson_id : lesson_id,
                    comment : comment
                },

                success: function (results) {
                    currentBlock.empty();
                    toggleLoader(loader);
                }
            });
        });

        /* AJAX: get levels after change course */
        $('.courses-group-select').change(function(){
            var group_id = $(this).val();
            var levelsBlock = $(this).parents('.lesson-number-block-wrapper');
            var loader = toggleLoader();

            $.ajax({
                type   : 'post',
                dataType: 'html',
                url    : EM.ajaxurl,
                data   : {
                    action : 'mct_change_levels',
                    group_id: group_id
                },
                success: function (date) {
                    var wrapper = $(levelsBlock).find('.course-group-wrapper');
                    wrapper.nextAll('div').remove();
                    wrapper.after(date);

                    levelsBlock.find('.wrap-select-block').find('select').chosen({disable_search_threshold: 10});
                    toggleLoader(loader);
                }
            });
        });

        /* AJAX: get modules after change level */
        $(document).on('change', '.path-level-select', function() {
            var level_id = $(this).val();
            var levelsBlock = $(this).parents('.lesson-number-block-wrapper');
            var loader = toggleLoader();

            $.ajax({
                type   : 'post',
                url    : EM.ajaxurl,
                data   : {
                    action : 'mct_change_levels',
                    level_id: level_id
                },
                success: function (date) {
                    var wrapper = $(levelsBlock).find('.path-level-wrapper');
                    wrapper.nextAll('div').remove();
                    wrapper.after(date);

                    levelsBlock.find('.wrap-select-block').find('select').chosen({disable_search_threshold: 10});
                    toggleLoader(loader);
                }
            });
        });

        /* AJAX: get lessons after change module */
        $(document).on('change', '.module-select', function() {
            var module_id = $(this).val();
            var levelsBlock = $(this).parents('.lesson-number-block-wrapper');
            var loader = toggleLoader();

            $.ajax({
                type   : 'post',
                url    : EM.ajaxurl,
                data   : {
                    action : 'mct_change_levels',
                    module_id: module_id
                },
                success: function (date) {
                    var wrapper = $(levelsBlock).find('.module-id-wrapper');
                    wrapper.nextAll('div').remove();
                    wrapper.after(date);

                    levelsBlock.find('.wrap-select-block').find('select').chosen({disable_search_threshold: 10});
                    toggleLoader(loader);
                }
            });
        });

    }

    /* add popup */
    $(document).on('click', '.modal_close', function() {
        $(this).parents('.modal_wrap').fadeOut(500);
        jQuery('html').css({
            'margin-right' : 0,
            'overflow' : 'visible'
        });
    });

    $(document).on('click', '.modal_wrap_all .modal_close, body', function(){
        $('.modal_wrap_all').fadeOut(500);
    });


    $('.mpf-link').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: false
    });
	
	/* Ajax: display lesson history in popup */
    $(document).on('click', '.all-lessons-history', function(e){
        e.preventDefault();

        var loader = toggleLoader();

        var user_id = $(this).parents('.user-block').find('.user-id').val();
        $.ajax({
            type: 'POST',
            url: EM.ajaxurl,
            data:  {
                action: 'user_lessons_history',
                user_id: user_id,
                full: true
            },
            success: function(data){

                $.magnificPopup.open({
                    items: {
                        src: data,
                        type: 'inline'
                    }
                });
                toggleLoader(loader);
            }
        });
    });
	
	if($('.block-slide-down').length){
        $(".slide").hide();
	}
	
	$('.block-slide-down').bind('click', function(e){
        e.preventDefault();

        var parentSlideBlock = $('.block-slide-down').parents('.lessons-block'),
            allHistoryModal = parentSlideBlock.find('.slide');

        if(allHistoryModal.is(':visible')) {
            allHistoryModal.slideToggle( "slow");
        } else {
            var loader = toggleLoader();
            $.ajax({
                type: 'POST',
                url: EM.ajaxurl,
                data:  {
                    action: 'get_tutor_lessons_history'
                },
                success: function(data){
                    var lessonsLis = $(data).find('.users');
                    parentSlideBlock.find('.users').html(lessonsLis);
                    allHistoryModal.slideToggle( "slow");
                    $('.mpf-link').magnificPopup({
                        type: 'inline',
                        preloader: false,
                        focus: false
                    });

                    toggleLoader(loader);

                }
            });
        }
    });
	

    jQuery(document).on('click', '.tutors_na, .taken', function(e) {
        return false;
    });

    jQuery(document).on('click', '.calendar_block_top', function(e) {
            var parent = jQuery(this).closest('.calendar_block_wrap');

            if (parent.hasClass('active'))
            {
                parent.removeClass('active');
                jQuery(this).find('.full').removeClass('down');
                jQuery(this).next().css({display: 'block'}).slideUp(500);
            }
            else
            {
                parent.addClass('active');
                jQuery(this).find('.full').addClass('down');
                jQuery(this).next().css({display: 'none'}).slideDown(500);
            }
        }
    );


    jQuery(document).on('click', '.em-calendar-modal .add-booking', function(e) {

            window.ajaxBackData = jQuery('input[name=em_cur_ajax_data]').val();

            jQuery('.m-book-lesson .lesson_info').css('display', 'block');
            jQuery('.m-book-lesson .step2-body .message').html('');



            // Set ajax load window on close button.
            $('.m-book-lesson .mfp-close').addClass('show-tutor-popup');

            var first_step = jQuery('.step1-body');
            var second_step = jQuery('.step2-body');

            var steps_tab_1 = jQuery('.steps_tab').eq(0);
            var steps_tab_2 = jQuery('.steps_tab').eq(1);
            steps_tab_1.removeClass('active');
            steps_tab_2.addClass('active');

            first_step.removeClass('active');
            second_step.addClass('active');

            var first_step_tab = jQuery('.step1-tab');
            var second_step_tab = jQuery('.step2-tab');

            first_step_tab.removeClass('active');
            second_step_tab.addClass('active');
            jQuery('.lesson-date').html(jQuery(this).data('event-date'));
            jQuery('.lesson-time').html(jQuery(this).text());
            jQuery('.event_id').val(jQuery(this).data('event-id'));

            second_step.find('select').chosen('destroy');
            second_step.find('select').chosen({disable_search_threshold:10});

            return false;
        }
    );

    jQuery(document).on('click', '.m-book-lesson .book_lesson', function(e) {
            var loader = toggleLoader();
            var communication_name = '';
            if(jQuery('#skype_name').val() !== '' && jQuery('#skype_name').length > 0) {
                communication_name = jQuery('#skype_name').val();
                jQuery('#user_skype').val(communication_name);
            }

            if(jQuery('#polycom_name').val() !== '' && jQuery('#polycom_name').length > 0) {
                communication_name = jQuery('#polycom_name').val();
                jQuery('#user_polycom').val(communication_name);
            }

            if(communication_name === '') {
                var el = jQuery(this).parent().parent();
                var communication_type = el.find('.communication_type').val();
                jQuery('#communication_type').val(communication_type);
                var student_id = el.find('.student_id').val();
                jQuery('#student_id').val(student_id);
                var booker_comment = jQuery(this).parent().prev().find('.booker-comment').val();
                jQuery('#booker_comment').val(booker_comment);
            }

            if(communication_type === 'skype' && jQuery('#user_skype').val() === '' && jQuery('#skype_name').length > 0) {
                jQuery.magnificPopup.open({
                    items: {
                        src: '#skype_form',
                        type: 'inline'
                    }
                });
                toggleLoader(loader);
                return false;
            }

            if(communication_type === 'polycom' && jQuery('#user_polycom').val() === '' && jQuery('#user_polycom').length > 0) {
                jQuery.magnificPopup.open({
                    items: {
                        src: '#polycom_form',
                        type: 'inline'
                    }
                });
                toggleLoader(loader);
                return false;
            }

            jQuery.ajax({
                type: 'POST',
                url: EM.ajaxurl,
                data:  {
                    action: 'add_booking',
                    event_id: jQuery('.event_id').val(),
                    student_id: jQuery('#student_id').val(),
                    comment: jQuery('#booker_comment').val(),
                    communication_type: jQuery('#communication_type').val(),
                    communication_name: communication_name
                },
                success: function(data){
                    var patt = /<div class="no_balance">/;
                    if(patt.test(data)) {
                        jQuery('.message').html(data);
                        jQuery('.lesson_info').hide();
                        toggleLoader(loader);
                        return;
                    } else {
                        if(communication_name && jQuery('#communication_type').val() === 'skype') {
                            jQuery('#skype_form').html(data);
                        }
                        if(communication_name && jQuery('#communication_type').val() === 'polycom') {
                            jQuery('#polycom_form').html(data);
                        }

                        jQuery('.message').html(data);
                        jQuery('.lesson_info').hide();
                        toggleLoader(loader);

                        // jQuery('.event_id').val('');
                        jQuery('#communication_type').val('');
                        jQuery('#booker_comment').val('');
                        jQuery('#student_id').val('');

                        // track free lesson
                        if(mct.is_free == 1){
                            _gaq.push(["_trackEvent", "Book Free Lesson", "Book Free Lesson", "Book Free Lesson"]);
                        }

                    }
                }
            });
            jQuery('#skype_name').val('');
            jQuery('#polycom_name').val('');
        }
    );

    jQuery(document).on('click', '.show-tutor-popup', function() {
        var post_id  = jQuery('input[name=post_id]').val();
        var author_id  = jQuery('input[name=author_id]').val();
        if (post_id && author_id) {
            showBookPopup(this, post_id, author_id, function() {
                $.ajax({
                    type: 'POST',
                    url: EM.ajaxurl,
                    data:  {
                        action: 'get_tutor_calendar',
                        day: args[0],
                        month: args[1],
                        year: args[2],
                        author_id: args[3]
                    },
                    success: function(data){
                        $('#tutor-calendar-'+post_id).show();
                        $('#tutor-calendar-'+args[4]).html(data).animate({'opacity': 1}, 500);

                        if($('.event_id').val()){
                            var elementId = '#event_' + $('.event_id').val();
                            var scrollTop = $(elementId).position().top;
                            $(".mfp-wrap").animate({
                                scrollTop: scrollTop - 150
                            }, 500);
                            $('.event_id').val('')

                        }
                        toggleLoader(loader)
                        addScroll();
                    }
                });
            });
        } else {
            $.magnificPopup.instance.close();
        }

        var loader = toggleLoader();
        var args = window.ajaxBackData.split(' ');
        $('#tutor-calendar-'+args[4]).css('opacity', 0);

        $(this).removeClass('show-tutor-popup');
    })

	jQuery('.modal_body').on({
		mouseleave: function() {
			jQuery(this).closest('.modal_wrap').removeClass('fix');
		},
		mouseenter: function() {
			jQuery(this).closest('.modal_wrap').addClass('fix');
		}
	});

	jQuery('.modal_wrap').on({
		click: function() {
			if (!jQuery(this).hasClass('fix'))
			{
				jQuery(this).fadeOut(500);
                jQuery('html').css({
                    'margin-right' : 0,
                    'overflow' : 'visible'
                });
			}
		}
	});

	jQuery('.modal_form_wrap input[type="text"]').each(function() {
		jQuery(this).data("default", jQuery(this).val());
		jQuery(this).on({
			focus: function() {
				if (jQuery(this).val() == jQuery(this).data("default"))
				{
					jQuery(this).val('').addClass('focus');
				}
			},
			focusout: function() {
				if (jQuery(this).val() == '')
				{
					jQuery(this).val(jQuery(this).data("default")).removeClass('focus');
				}
			}
		});
	});
});

function showBookPopup(el, post_id, author_id, callback){

    var steps_tab_1 = jQuery('.steps_tab').eq(0);
    var steps_tab_2 = jQuery('.steps_tab').eq(1);
    steps_tab_2.removeClass('active');
    steps_tab_1.addClass('active');

    jQuery('.step1-body').addClass('active');
    jQuery('.step2-body').removeClass('active');
    var first_step_tab = jQuery('.step1-tab');

    var second_step_tab = jQuery('.step2-tab');
    second_step_tab.removeClass('active');
    first_step_tab.addClass('active');

    jQuery('.book_lessons_profile_wrap').html(jQuery('#tutor-info-'+post_id).html());
    jQuery('.book_lessons_profile_wrap #tutor-info-'+post_id).css('display', 'block');

    var modal = jQuery('#modal-wrap-'+post_id);

    /*
     get ajax calendar!
     */
    var loader = toggleLoader();

    jQuery.ajax({
        type: 'POST',
        url: EM.ajaxurl,
        data:  {
            action: 'get_tutor_calendar',
            author_id: author_id
        },
        success: function(data){
            if (typeof callback  === 'function') {
                jQuery('#tutor-calendar-'+post_id).hide();
            }
            jQuery('#tutor-calendar-'+post_id).html(data);

            jQuery('input[name=author_id]').val(author_id);
            jQuery('input[name=post_id]').val(post_id);

            jQuery.magnificPopup.open({
                mainClass: post_id === 'calendar' ? '' : 'm-book-lesson',
                items: {
                    src: modal,
                    type: 'inline'
                },
                callbacks: {
                    open: function() {
                        addScroll();
                    }
                }
            });
            toggleLoader(loader);
            if (typeof callback  === 'function') {
                callback();
            }
        }
    })
}
function addScroll() {
    var controller = new ScrollMagic.Controller({container: ".mfp-wrap"}),
        triggerScroll = jQuery('#triggerScroll').offsetTop;

    new ScrollMagic.Scene({
        duration: 10000,
        offset: 200
    })
        .setPin(".js-sticky-header")
        .addTo(controller);

    new ScrollMagic.Scene({
        duration: 10000,
        offset: 200
    })
        .setPin(".js-sticky-header-1")
        .addTo(controller);
}
/**
 * Ajax function return Update my availability calendar.
 * @param el
 * @param author_id
 */
function showAvailabilityPopup(el, author_id){
    var modal = jQuery('#modal-wrap-calendar');
    var loader = toggleLoader();
    /*
     get ajax calendar!
     */
    jQuery.ajax({
        type: 'POST',
        url: EM.ajaxurl,
        data:  {
            action: 'get_availability_calendar',
            author_id: author_id
        },
        success: function(data){
            var content = jQuery(data).find('.em-calendar');
            jQuery('.em-calendar-wrapper').html(content);

            jQuery.magnificPopup.open({
                items: {
                    src: modal,
                    type: 'inline'
                }
            });
            toggleLoader(loader);
            addScrollOnAvailability();
        }
    })
}

function addScrollOnAvailability() {
    var controller = new ScrollMagic.Controller({container: ".mfp-wrap"}),
        triggerScroll = jQuery('#triggerScroll').offsetTop;

    new ScrollMagic.Scene({
        duration: 10000,
        offset: 50
    })
        .setPin(".js-sticky-header")
        .addTo(controller);

    new ScrollMagic.Scene({
        duration: 10000,
        offset: 50
    })
        .setPin(".js-sticky-header-1")
        .addTo(controller);
}

function showNoBalancePopup() {
    jQuery.magnificPopup.open({
        items: {
            src: jQuery('.no_balance').html(),
            type: 'inline'
        }
    });
}
