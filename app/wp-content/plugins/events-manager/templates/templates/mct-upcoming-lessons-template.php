<?php
/*
 * Tamplate for display bookings list, using with shortcode mct_lessons_list;
 */
?>
<div id="upcoming-lessons" class="lessons-block">
    <div class="lessons-block-title">
      <h3><?php print $title; ?></h3>
    </div>
    <div class="lessons">
        <div class="users">
        <?php if(!empty($events) && is_array($events)): ?>
            <?php foreach($events as $event_id => $event): ?>
                <?php $user_info = $event['user']; ?>
                        <div class="user-block-wrap">
                            <div class="user-block" id="event-<?php print $event_id; ?>">
                                <div class="event-time">
                                    <?php print date('dM Y, D h:i a', dut_prepare_date($event['event_start_date'] . ' ' . $event['event_start_time'], dut_get_user_timezone())); ?>
                                </div>
                                <div class="student-top-box">
                                    <div class="user-photo">
                                        <?php print get_avatar( $user_info->user_id, 64 ); ?>
                                    </div>
                                    <div class="user-info-box">
                                        <div class="user-name"><?php print $user_info->nickname; ?></div>
                                        <div class="user-message"><?php print $user_info->message; ?></div>
                                    </div>
                                    <div class="open-info"><?php _e('i'); ?></div>
                                </div>
                                <div class="hide-student-info-block student-bottom-box">
                                    <div class="student-info">
                                        <?php $skype_name = get_user_meta($uid, 'skype_name', true); ?>
                                        <?php if(!empty($skype_name)): ?>
                                            <div class="contact-info-item"><?php print __('Skype: ') . $skype_name; ?></div>
                                        <?php endif; ?>
                                        <div class="contact-info-item"><?php print __('Email: ') . $user_info->user_email; ?></div>
                                    </div>
                                    <?php if(!empty($user_info->history)): ?>
                                        <!-- lesson history block -->
                                        <div class="lessons-history lesson-history-wrapper">
                                            <span class="lesson-history-topic"><?php print __( 'Lesson history:' ); ?></span>
                                            <div class="lesson-history-buttons">
                                                <div class="lessons-loop">
                                                    <?php foreach($user_info->history as $history): ?>
                                                        <?php $teacher_first_name = get_user_meta( $history['author_id'], 'first_name', true ); ?>
                                                        <?php $teacher_last_name  = get_user_meta( $history['author_id'], 'last_name', true );  ?>
                                                        <div class="history-wrapper" id="history-<?php print $history['event_id']; ?>">
                                                            <div class="history-item-wrapper">
                                                                <a href="#modal-wrap-history-<?php print $history['event_id']; ?>" class="mpf-link">
                                                                    <?php echo $history['lesson_date']; ?>
                                                                </a>
                                                            </div>
                                                            <!-- History popup box -->
                                                            <div class="history-popup-box mfp-hide" id="modal-wrap-history-<?php print $history['event_id']; ?>">
                                                                <h2><?php echo __( 'Lesson history: ' ); ?></h2>
                                                                <div class="history-popup-content">
                                                                    <div class="history_course_id item-history">
                                                                        <span><?php echo __( 'Course: ' ); ?></span>
                                                                        <?php echo $history['course_group']; ?>
                                                                    </div>
                                                                    <div class="history_level_id item-history">
                                                                        <span><?php echo __( 'Level: ' ); ?></span>
                                                                        <?php echo $history['course_level']; ?>
                                                                    </div>
                                                                    <div class="history_module_id item-history">
                                                                        <span><?php echo __( 'Module: ' );?></span>
                                                                        <?php echo $history['module_id']; ?>
                                                                    </div>
                                                                    <div class="history_lesson_id item-history">
                                                                        <span><?php echo __( 'Lesson: ' ); ?></span>
                                                                        <?php echo $history['lesson_id']; ?>
                                                                    </div>
                                                                    <div class="history-comment item-history">
                                                                        <span><?php echo __( 'Comment: ' ); ?></span>
                                                                        <?php echo $history['comment']; ?>
                                                                    </div>
                                                                    <?php if(!empty($history_date)):?>
                                                                        <div class="history-complete-lesson-time item-history">
                                                                            <span><?php echo __( 'Lesson date:' ); ?></span>
                                                                            <?php echo $history_date; ?>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <div class="history-taecher-name item-history">
                                                                        <span><?php echo __( 'Teacher: ' ); ?></span>
                                                                        <?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- End history popup box -->
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                                <div class="all-complete-lessons-wrapper">
                                                    <div class="show-full-history-button">
                                                        <input type="button" class="all-lessons-history show-all-lessons-button e-btn-show-lesson" name="lessons-history" value="<?php print __('Show all history'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End lesson history block -->
                                    <?php endif; ?>
                                    <!-- User course info -->
                                    <?php if(!empty($user_info->history) || !empty($user_info->courses)):?>
                                        <div class="lesson-number-block-wrapper">
                                            <div class="wrap-select-block m-one-row course-group-wrapper">
                                                <div class="title-block">
                                                    <?php print __( 'Course: ' ); ?>
                                                </div>
                                                <div class="select-block course-removed-part">
                                                        <span><?php print $user_info->history[0]['course_group'];?></span>
                                                </div>
                                            </div>
                                            <div class="wrap-select-block path-level-wrapper">
                                                <div class="title-block">
                                                    <?php print __( 'Pathway/level:' ); ?>
                                                </div>
                                                <div class="select-block ch-select">
                                                    <div class="level-removed-part">
                                                        <span><?php print $user_info->history[0]['course_level'];?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wrap-select-block module-id-wrapper">
                                                <div class="title-block">
                                                    <?php print __( 'Module: ' ); ?>
                                                </div>
                                                <div class="select-block ch-select">
                                                    <div class="module-removed-part">
                                                      <span><?php print $user_info->history[0]['module_id'];?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wrap-select-block m-fr lesson-id-wrapper">
                                                <div class="title-block">
                                                    <?php print __( 'Lesson: ' ); ?>
                                                </div>
                                                <div class="select-block ch-select">
                                                    <div class="lesson-removed-part">
                                                      <span><?php print $user_info->history[0]['lesson_id'];?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="lesson-comment-wrapper">
                                            <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                <div class="title-block">
                                                    <?php print __( 'Comment: ' ); ?>
                                                </div>
                                                <div class="select-block lesson-comment">
                                                    <?php !empty($user_info->history[0]['comment']) ? print $user_info->history[0]['comment'] : ''; ?>
                                                </div>
                                            <?php else: ?>
                                                <textarea class="lesson-comment" cols="30" rows="6"></textarea>
                                            <?php endif; ?>
                                        </div>
                                        <!-- End user course info -->
                                    <?php endif; ?>
                                    <input type="hidden" value="<?php print $user_info->user_id; ?>" name="user_id" class="user-id">
                                    <input type="hidden" value="<?php print get_current_user_id(); ?>" name="teacher_id" class="teacher-id">
                                    <input type="hidden" value="<?php print $event_id; ?>" name="event_id" class="event-id">
                                </div>
                            </div>
                        </div>

            <?php endforeach; ?>
            <?php if($pages_num > 1): ?>
                <div class="mct-pagination">
                    <ul>
                        <li class="prev<?php ($current_page == 1)? print ' disabled' : ''; ?>">
                            <?php if($current_page == 1): ?>
                                <span><?php _e('Previous'); ?></span>
                            <?php else: ?>
                                <a href="#" data-page="<?php print $current_page - 1; ?>"><?php _e('Previous'); ?></a>
                            <?php endif; ?>
                        </li>
                        <?php for($i=1; $i <= $pages_num; $i++): ?>
                            <?php if($current_page == $i):?>
                                <li>
                                    <span class="active" data-page="<?php print $i; ?>"><?php print $i; ?></span>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="#" data-page="<?php print $i; ?>"><?php print $i; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endfor;?>
                        <li class="next<?php ($current_page == $pages_num)? print ' disabled' : ''; ?>">
                            <?php if($current_page == $pages_num): ?>
                                <span><?php _e('Next'); ?></span>
                            <?php else: ?>
                                <a href="#" data-page="<?php print $current_page + 1; ?>"><?php _e('Next'); ?></a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?php print __('Lessons not found'); ?>
        <?php endif;?>
        </div>
    </div>
</div>