<?php
/*
 * This file contains the HTML generated for full calendars. You can copy this file to yourthemefolder/plugins/events-manager/templates and modify it in an upgrade-safe manner.
 *
 * There are two variables made available to you:
 *
 * 	$calendar - contains an array of information regarding the calendar and is used to generate the content
 *  $args - the arguments passed to EM_Calendar::output()
 *
 * Note that leaving the class names for the previous/next links will keep the AJAX navigation working.
 */
$cal_count = count($calendar['cells']); //to prevent an extra tr
$col_count = $tot_count = 1; //this counts collumns in the $calendar_array['cells'] array
$col_max = count($calendar['row_headers']); //each time this collumn number is reached, we create a new collumn, the number of cells should divide evenly by the number of row_headers
?>
<?php if($cal_count > 0):?>
<div class="b-tutor-cal-wrap">
    <a href="" class="book_lesson e-btn" onclick="showBookPopup(this, 'calendar', 'calendar'); return false;"><?php _e('Update my availability'); ?></a>
</div>
<div class="em-calendar-modal em-calendar-wrapper mfp-hide" id="modal-wrap-calendar">
    <div class="em-calendar fullcalendar b-calendar mct-modal fixedHolder" id="triggerScroll">
        <div class="b-cal-header fixedHolder-item js-sticky-header">
            <ul class="cal-controls">
                <li class="cal-prev">
                    <a href="<?php echo $calendar['links']['previous_url']; ?>" class="em-calnav full-link em-calnav-prev fa fa-angle-left fa-2x"></a>
                </li>
                <li class="cal-next">
                    <a href="<?php echo $calendar['links']['next_url']; ?>" class="em-calnav full-link em-calnav-next fa fa-angle-right fa-2x"></a>
                </li>
            </ul>
            <div class="cl-title month_name"><?php echo ucfirst(date_i18n('d M', $calendar['start_day'])) . ' - ' . ucfirst(date_i18n('d M, Y', $calendar['end_day'])); ?></div>
        </div>
        <div class="cl-time-wrap">
            <div class="cl-time-cols cl-time-header fixedHolder-item fixedHolder-itemBottom js-sticky-header-1">
                <?php foreach($calendar['cells'] as $date => $cell_data ): ?>
                    <div class="cl-time-col">
                        <span><?php echo date('D, d', strtotime($date)); ?></span>
                        <span class="cl-time-col-month"><?php echo date('M', strtotime($date)); ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php foreach($calendar['periods'] as $period => $intervals): $is_open = FALSE; ?>
                <div class="cl-time-block">
                    <div class="cl-time-block-top calendar_block_wrap">
                        <div class="cl-time-cols">
                            <div class="cl-time-col m-full">
                                <a href="#" class="js-cal-time-toggle"><?php print MCT_EM_Calendar::get_time_period($period); ?><i class="e-toggle-arrow fa fa-caret-right fa-rotate-90"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="cl-time-block-bottom">
                        <?php foreach($intervals as $interval): ?>
                            <div class="cl-time-cols">
                                <?php foreach($calendar['cells'] as $day => $events ):  ?>
                                    <?php if( !empty($events[$period][$interval]['events']) && count($events[$period][$interval]['events']) > 0 ): ?>
                                        <?php foreach($events[$period][$interval]['events'] as $time => $event): ?>
                                            <div class="cl-time-col event_<?php echo $event->event_id; ?>">
                                                <?php if(!empty($event->get_bookings()->bookings[0])): ?>
                                                    <a href="" class="taken"><?php _e('Taken', 'mct_tutor'); ?></a>
                                                <?php else: ?>
                                                    <a href="<?php print $calendar['links']['delete_url']; ?>&post_id=<?php print $event->post_id; ?>" class="event_author_link del-lesson opened-event" ><?php print MCT_EM_Calendar::get_time_interval(dut_prepare_date(date('Y-m-d H:i:s', $time), dut_get_user_timezone($event->event_owner)), $calendar['lesson_type']); ?></a>
                                                    <div class="cl-event-time hidden">
                                                        <?php
                                                        $time_value = explode(':', $interval);
                                                        print MCT_EM_Calendar::get_time_interval(mktime($time_value[0], $time_value[1],0), $calendar['lesson_type']);
                                                        ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                        <?php if(!$is_open):
                                            $is_open = TRUE; ?>
                                            <script>
                                                var el_cl_time_block_bottom = jQuery('.event_<?php echo $event->event_id ?>').parent().parent();
                                                el_cl_time_block_bottom.parent().toggleClass('is-open').find('.e-toggle-arrow').toggleClass('fa-rotate-90');
                                                el_cl_time_block_bottom.show();
                                            </script>
                                        <?php endif ?>
                                    <?php else: ?>
                                        <div class="cl-time-col">
                                            <a href="<?php print $calendar['links']['add_url']; ?>&event_day=<?php print $day; ?>&event_time=<?php print $interval; ?>" class="event_author_link add-lesson"><?php _e('N/A', 'mct_tutor'); ?></a>
                                            <div class="cl-event-time">
                                                <?php
                                                $time_value = explode(':', $interval);
                                                print MCT_EM_Calendar::get_time_interval(mktime($time_value[0], $time_value[1],0), $calendar['lesson_type']);
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>
