<?php
class MCT_Lessons_History extends EM_Booking {

	public static function init(){
        //nothing
    }

	public function get($args = array()){
        $users_list = $subscriber = array();
        $author_id = get_current_user_id();
        $users_list = self::get_history_users_list($author_id, $args);

        if(empty($users_list)){
            return;
        }

        $count = self::get_history_users_count($author_id);

        foreach($users_list as $user_info){
            $subscriber_id = $user_info->subscriber_id;
            $user_data = get_userdata($subscriber_id);
            $subscriber[$subscriber_id]['id']       = $subscriber_id;
            $subscriber[$subscriber_id]['nickname'] = get_user_meta($user_info->subscriber_id, 'nickname', true);
            $subscriber[$subscriber_id]['email']    = $user_data->data->user_email;
            $subscriber[$subscriber_id]['skype']    = get_user_meta($subscriber_id, 'rpr_skype_name', true);;
            $subscriber[$subscriber_id]['history']  = MCT_Lessons_List::get_subscriber_history($user_info->subscriber_id);
            $subscriber[$subscriber_id]['complete_date']  = date('Y-m-d', $user_info->complete_date);
            $subscriber[$subscriber_id]['complete_time']  = date('H:i', $user_info->complete_date);
            $subscriber[$subscriber_id]['message']  = $user_info->message;
        }

        return $subscriber;
    }


	public function output($args) {
        $title = $users_list = '';
        $limit = 10;
        $current_page = 1;
        $author_id = get_current_user_id();
        $count = self::get_history_users_count($author_id);
        $pages_number = floor($count/$limit);

        if(!empty($args['page'])){
            $current_page = (int)$args['page'];
        }


        if(empty($args['display']) && $args['display'] !== 'slide'){
            $users_list = MCT_Lessons_History::get($args);
            $pagination = self::users_pagination($pages_number, $current_page);
        }

        if(!empty($args['title'])){
            $title = $args['title'];
        }

        ob_start();

        em_locate_template('templates/mct-lessons-history-template.php', true, array('users' => $users_list, 'title' => $title, 'pagination' => $pagination ));
        $users_array = ob_get_clean();

        return apply_filters('mct_lessons_history', $users_array);
	}

    protected function get_history_users_list($author_id, $args){
        global $wpdb;
        $start = 0;
        $limit = 10;

        if(!empty($args['page']) && $args['page'] != 1 ){
            $start = ($args['page']-1) * $limit;
        }


        $users_list = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM
                 (SELECT * FROM " . $wpdb->prefix . 'teachers_user_list' . " WHERE `author_id` = %d ORDER BY `complete_date` DESC ) `list`
                  GROUP BY `subscriber_id` LIMIT %d, %d",
                $author_id,
                $start,
                $limit
            )
        );

        return $users_list;

    }

    protected function get_history_users_count($author_id){
        global $wpdb;

        $users_count = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT `subscriber_id` FROM " . $wpdb->prefix . 'teachers_user_list' .
                " WHERE `author_id` = %d
                GROUP BY `subscriber_id`
                ",
                $author_id
            )
        );

        return count($users_count);
    }

    public function users_pagination($pages_number, $current_page){
        ob_start();
        ?>
        <?php if(!empty($pages_number) && $pages_number > 1): ?>
            <?php
                $range = 3;
                $showitems = ($range * 2)+1;

                $prevpagepug = $current_page - 1;
                if($prevpagepug < 1){
                    $prevpagepug = 1;
                }
                $nextpagepug = $current_page + 1;
            ?>
            <div class="mct_page_navi">
                <div class="page_paginat">
                    <div class="page_panel_doc">
                        <?php if($current_page > 2 && $pages_number > $range+1 && $showitems < $pages_number): ?>
                                <a href="" data-page="<?php echo 1; ?>" id="prev_box" class="page-box nav_click prev_box_n"><<</a>
                        <?php endif; ?>
                        <?php if($current_page > 1 && $showitems < $pages_number): ?>
                            <a href="" data-page="<?php echo $prevpagepug; ?>" id="prev_box" class="page-box nav_click prev_box_n"><</a>
                        <?php endif; ?>
                        <?php for($page = 1; $page <= $pages_number; $page++) :?>
                            <?php if (1 != $pages_number &&( !($page >= $current_page+$range+1 || $page <= $current_page-$range-1) || $pages_number <= $showitems )): ?>
                                <?php
                                    if($current_page == $page){
                                        $class = 'active';
                                    }else{
                                        $class = '';
                                    }
                                ?>
                                <a href="" data-page="<?php echo $page; ?>" class="page-box nav_click <?php print $class; ?>"><?php echo $page; ?></a>
                            <?php endif; ?>
                        <?php endfor; ?>
                        <?php if($current_page < $pages_number && $showitems < $pages_number): ?>
                            <a href="" data-page="<?php echo $nextpagepug; ?>" id="next_box" class="page-box nav_click next_box">></a>
                        <?php endif; ?>
                        <?php if ($current_page < $pages_number-1 &&  $current_page+$range-1 < $pages_number && $showitems < $pages_number): ?>
                            <a href="" data-page="<?php echo $pages_number; ?>" id="next_box" class="page-box nav_click next_box">>></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php
        $pagination = ob_get_clean();

        return $pagination;
    }
}

add_action('init', array('MCT_Lessons_History', 'init'));