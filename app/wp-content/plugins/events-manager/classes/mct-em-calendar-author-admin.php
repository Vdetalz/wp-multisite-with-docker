<?php
class MCT_EM_Author_Calendar extends MCT_EM_Calendar {

	public static function init(){
		//nothing to init anymore
	}

    public static function get( $args ){
        $users = get_users('role=author');
        foreach($users as $author){
            $authors[$author->ID] = $author->display_name;
        }

        return $authors;
    }

    public static function output($args = array(), $wrapper = true) {
        $calendar_array = array();
        //Let month and year REQUEST override for non-JS users
        $args['limit'] = !empty($args['limit']) ? $args['limit'] : get_option('dbem_display_calendar_events_limit'); //limit arg will be used per day and not for events search

        $args['full'] = 1;

        if( !empty($_REQUEST['mo']) || !empty($args['mo']) ){
            $args['month'] = ($_REQUEST['mo']) ? $_REQUEST['mo']:$args['mo'];
        }
        if( !empty($_REQUEST['yr']) || !empty($args['yr']) ){
            $args['year'] = (!empty($_REQUEST['yr'])) ? $_REQUEST['yr']:$args['yr'];
        }

        if( !empty($_REQUEST['dy']) || !empty($args['dy']) ){
            $args['day'] = (!empty($_REQUEST['dy'])) ? $_REQUEST['dy']:$args['dy'];
        }

        if( !empty($_REQUEST['author_id']) || !empty($args['author_id']) ){
            $args['author_id'] = (!empty($_REQUEST['author_id'])) ? $_REQUEST['author_id']:$args['author_id'];
            $args['owner'] = (!empty($_REQUEST['author_id'])) ? $_REQUEST['author_id']:$args['author_id'];

            $calendar_array  = parent::get($args);

        }


        $calendar_array['authors'] = self::get($args);

        $template = 'templates/author-calendar-full.php';
        ob_start();
        em_locate_template($template, true, array('calendar'=>$calendar_array,'args'=>$args));
        if($wrapper){
            $calendar = '<div id="em-calendar-'.rand(100,200).'" >'.ob_get_clean().'</div>';
        }else{
            $calendar = ob_get_clean();
        }

        return apply_filters('em_calendar_output', $calendar, $args);
    }

}
add_action('init', array('MCT_EM_Author_Calendar', 'init'));