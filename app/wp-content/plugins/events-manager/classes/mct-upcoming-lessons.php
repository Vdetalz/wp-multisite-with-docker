<?php
class MCT_Upcoming_Lessons extends EM_Booking {

	public static function init(){
        add_action( 'wp_enqueue_scripts', array('MCT_Upcoming_Lessons', 'requiring_upcoming_enqueue_scripts'));
	}

	public function get($args = array()){
	 	global $wpdb;
        $bookings = array();
        $lessons_list = array();

        $limit = !empty($args['limit']) ? (int)$args['limit'] : 10;
        $page = !empty($args['page']) ? (int)$args['page'] : 1;
        $offset = ($page-1) * $limit;

        $events_table = $wpdb->prefix . 'em_events';
        $teachers_user_list_table = $wpdb->prefix . 'teachers_user_list';
        $bookings_table = $wpdb->prefix . 'em_bookings';

        $event_owner = get_current_user_id();
        $date = date("Y-m-d H:i:s");

        $sql = "SELECT * FROM `{$bookings_table}` AS `bk`
                LEFT JOIN `{$events_table}` AS `ev` ON ev.event_id=bk.event_id
                WHERE event_owner=%d AND (`event_status`=1)
                AND ((event_end_date > CAST('{$date}' AS DATE)
                AND event_end_date != '0000-00-00' AND event_end_date IS NOT NULL)
                OR (event_end_date = CAST('{$date}' AS DATE) )
                AND (event_start_time >= CAST('{$date}' AS time)
                OR event_end_time >= CAST('{$date}' AS time)))
                GROUP BY ev.post_id
                ORDER BY event_start_date ASC, event_start_time ASC, event_name ASC LIMIT %d, %d";

        $lessons = $wpdb->get_results($wpdb->prepare($sql, $event_owner, $offset, $limit));


        foreach($lessons as $lesson){
            $lessons_list[$lesson->event_id]['user'] = new stdClass();
            $lessons_list[$lesson->event_id]['user']->user_id = $lesson->person_id;

            $user_info = get_userdata($lesson->person_id);
            $lessons_list[$lesson->event_id]['user']->user_email = $user_info->user_email;
            $lessons_list[$lesson->event_id]['user']->nickname   = get_user_meta($lesson->person_id, 'nickname', true);
            $lessons_list[$lesson->event_id]['user']->message    = $lesson->booking_comment;
            $lessons_list[$lesson->event_id]['user']->history    = self::get_subscriber_history($lesson->person_id);
            $lessons_list[$lesson->event_id]['event_start_date'] = $lesson->event_start_date;
            $lessons_list[$lesson->event_id]['event_start_time'] = $lesson->event_start_time;
            $lessons_list[$lesson->event_id]['event_end_time'] = $lesson->event_end_time;
        }

        return $lessons_list;
	}

    /**
     * @param $subscriber_id
     * @param bool $full
     * @return mixed
     * get history from teachers_user_list table
     */
     public function get_subscriber_history($subscriber_id, $full = false){
        global $wpdb;
        $limit = '';

        if(!$full){
            $limit = ' LIMIT 3';
        }

        $users_history = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM " . $wpdb->prefix . "teachers_user_list WHERE `subscriber_id` = %d ORDER BY `complete_date` DESC" . $limit,
                $subscriber_id
            ),
            ARRAY_A
        );

        if(empty($users_history) && count($users_history) < 1){
            return;
        }

        $i = 0;
        foreach($users_history as $history ){
            $lesson_date = date('d/m/y', $history['complete_date']);
            $user_history[$i]['lesson_date'] = $lesson_date;
            $user_history[$i]['event_id'] = $history['event_id'];
            $user_history[$i]['author_id'] = $history['author_id'];
            //get download title from mychinese hub site
            switch_to_blog(5);
            $user_history[$i]['course_group'] = get_the_title($history['course_group_id']);
            $module = self::get_module_details($history['module_id']);
            $user_history[$i]['module_id'] = $module->module_number;

	        if(!empty($history['level_id'])){
		        $course_details = self::mct_get_course_info($history['level_id']);
		        if(!empty($course_details->course_number)){
			        $user_history[$i]['course_level'] = $course_details->course_number;
		        }
	        }

            restore_current_blog();
            $user_history[$i]['lesson_id'] = $history['lesson_id'];
            $user_history[$i]['comment'] = $history['comment'];

            $i++;
        }

        return $user_history;
    }

    /**
     * Hook wp_enqueue_scripts callback function.
     */
    function requiring_upcoming_enqueue_scripts() {
        wp_enqueue_script( 'mct-ev-scripts', plugins_url( 'includes/js/scripts.js', dirname(__FILE__ ) ), array( 'jquery' ) );
        wp_localize_script( 'mct-ev-scripts', 'ajax', array( 'url' => admin_url( 'admin-ajax.php' )) );
    }

    /**
     * Shortcode viwe function.
     *
     * @param $args
     * @return mixed|void
     */
    public function output($args) {
        $events_list = '';
        $title = '';
        $args['owner'] = get_current_user_id();
        $args['scope'] = 'future';

        $limit = !empty($args['limit']) ? (int)$args['limit'] : 10;
        $events_list = self::get($args);
        $max_rows = self::get_total();
        $pages_num = ceil($max_rows/$limit);
        $page = !empty($args['page']) ? (int)$args['page'] : 1;

        $events_list = MCT_Upcoming_Lessons::get($args);
        if(!empty($args['title'])){
            $title = $args['title'];
        }

        ob_start();
        em_locate_template('templates/mct-upcoming-lessons-template.php', true, array('events' => $events_list, 'pages_num' => $pages_num, 'current_page' => $page, 'args'=>$args, 'title' => $title));
        $users_array = ob_get_clean();

        return apply_filters('mct_upcomming_lessons', $users_array, $args);
	}


    /**
     * Hook wp_enqueue_scripts callback function.
     */
    function upcoming_lessons_enqueue_scripts() {
        wp_enqueue_script( 'students_hider', plugins_url( 'includes/js/calendar/students_hider.js', dirname(__FILE__ ) ), array( 'jquery' ) );
        wp_enqueue_script( 'common_script', plugins_url( 'includes/js/calendar/common_script.js', dirname(__FILE__ ) ), array( 'jquery' ) );
    }

    /**
     * Return max rows number.
     * @return mixed
     */
    public function get_total(){
        global $wpdb;

        $event_owner = get_current_user_id();
        $events_table = $wpdb->prefix . 'em_events';
        $bookings_table = $wpdb->prefix . 'em_bookings';
        $date = date("Y-m-d H:i:s");


        $sql = "SELECT COUNT(*) FROM `{$bookings_table}` AS `bk`
                LEFT JOIN `{$events_table}` AS `ev` ON ev.event_id=bk.event_id
                WHERE event_owner=%d AND (`event_status`=1)
                AND ((event_end_date > CAST('{$date}' AS DATE)
                AND event_end_date != '0000-00-00' AND event_end_date IS NOT NULL)
                OR (event_end_date = CAST('{$date}' AS DATE) )
                AND (event_start_time >= CAST('{$date}' AS time)
                OR event_end_time >= CAST('{$date}' AS time)))
                GROUP BY ev.post_id
                ORDER BY event_start_date ASC, event_start_time ASC, event_name ASC";

        $lessons = $wpdb->get_var($wpdb->prepare($sql, $event_owner));

        $total = $wpdb->num_rows;

        return $total;
    }

    /**
     * Function return module info by module id.
     * @param $moduleID
     * @return bool
     */
    function get_module_details($moduleID)
    {
        if (!$moduleID) {
            return false;
        }

        global $wpdb;

        $wpdb->show_errors();
        $table = $wpdb->prefix . 'wpcw_modules';
        $SQL = $wpdb->prepare("SELECT *
			FROM $table
			WHERE module_id = %d
			", $moduleID );

        return $wpdb->get_row($SQL);
    }

    /**
     * Return courser info by course id.
     *
     * @param $course_id
     * @return bool
     */
	function mct_get_course_info($course_id)
	{
		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}wpcw_courses
			WHERE course_id={$course_id}
			ORDER BY course_title";

		$item = $wpdb->get_results($sql);

		if (isset($item[0]) && !empty($item[0])) {
			return $item[0];
		} else {
			return false;
		}
	}
}

add_action('init', array('MCT_Lessons_List', 'init'));