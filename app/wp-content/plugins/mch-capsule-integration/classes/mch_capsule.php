<?php

class MCH_Capsule {

	const ADD_TASK = '/api/task';

	protected $capsule_page = 'https://%s.capsulecrm.com';
	protected $app_name;
	protected $token;

	/**
	 * @param string $app_name Name of the APP
	 * @param string $token User token
	 */
	public function __construct($app_name, $token) {
		$this->app_name = $app_name;
		$this->token = $token;
	}

	/**
	 * Get Capsule App Url.
	 *
	 * @return string App url
	 */
	public function get_capsule_app_url() {
		return sprintf($this->capsule_page, $this->app_name);
	}

	/**
	 *
	 * Add new task to Capsule.
	 *
	 * @param array $task_details information to add new task
	 *
	 * Format of array:
	 * $task_details = array(
	 *		'description' => $description . ' | ' . $user_name,
	 *		'detail' => $message,
	 *		'category' => __('classroom teacher issue'),
	 *		'dueDate' => date('Y-m-d'),
	 * );
	 */
	public function add_task(array $task_details = array()) {
		$task_details = array('task' => $task_details);
		$task_details_json = json_encode($task_details);

		$ch = curl_init($this->get_capsule_app_url() . self::ADD_TASK);

		$options = array(
		  CURLOPT_USERPWD => $this->token,
		  CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
		  CURLOPT_HEADER => true,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_POST => true,
		  CURLOPT_POSTFIELDS => $task_details_json
		);

		curl_setopt_array($ch, $options);

		curl_exec($ch);
		curl_close($ch);
	}
}