<?php

/** var $wpdb wpdb */

class MCH_Capsule_Employers_Model {
	const TABLE_NAME = 'capsule_employers';
	private $wpdb;

	public $name;
	public $token;

	public function __construct(wpdb $wpdb) {
		$this->wpdb = $wpdb;
	}

	/**
	 * Return table name.
	 * @return string
	 */
	public function get_table_name() {
		return $this->wpdb->prefix . self::TABLE_NAME;
	}

	/**
	 * Insert New Employer.
	 */
	public function save() {
		if(!empty($this->name) && !empty($this->token)) {
			$this->wpdb->insert(
			  $this->get_table_name(),
			  array(
				'name' => trim($this->name),
				'token' => trim($this->token)
			  ),
			  array('%s', '%s')
			);
		} else {
			throw new InvalidArgumentException();
		}
	}

	/**
	 * Get Employer by ID.
	 *
	 * @param $employer_id
	 * @return mixed
	 */
	public function get_employer_by_id($employer_id) {
		return $this->wpdb->get_row($this->wpdb->prepare(
		  "SELECT * FROM {$this->get_table_name()} WHERE id = %d", $employer_id
		));
	}

	/**
	 * Return all employers.
	 * @return mixed
	 */
	public function get_all_employers() {
		return $this->wpdb->get_results("SELECT * FROM {$this->get_table_name()}");
	}

	/**
	 * Delete Employer.
	 * @param $id int Employer id.
	 */
	public function delete($id) {
		$this->wpdb->delete($this->get_table_name(), array('id' => $id));
		$this->wpdb->update(
		  $this->wpdb->usermeta,
		  array('meta_value' => ''),
		  array(
			'meta_key' => MCH_Capsule_Integration::USER_EMPLOYER_OPTION,
			'meta_value' => $id
		  )
		);
	}
}