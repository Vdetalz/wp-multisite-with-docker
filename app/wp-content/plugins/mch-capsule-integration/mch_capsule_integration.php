<?php

/*
 * Plugin name: My Chinese Hub Capsule Integration
 * Author: Web4Pro
 * Description: API intergration with Capsule
 * version: 1.0
 */

require 'classes/mch_capsule_page.php';
require 'classes/mch_capsule_employers_model.php';
require 'classes/mch_capsule.php';

/** var $mch_capsule_model MCH_Capsule_Employers_Model */
/** var $mch_capsule_page MCH_Capsule_Page */
/** var $wpdb wpdb */

class MCH_Capsule_Integration {
	private $mch_capsule_model;
	private $mch_capsule_page;
	private $wpdb;

	const USER_EMPLOYER_OPTION = 'mch_capsule_employer';

	/**
	 * Object Initialization.
	 *
	 * @param MCH_Capsule_Employers_Model $mch_capsule_model
	 * @param MCH_Capsule_Page $mch_capsule_page
	 * @param wpdb $wpdb
	 */
	public function __construct(MCH_Capsule_Employers_Model $mch_capsule_model, MCH_Capsule_Page $mch_capsule_page, wpdb $wpdb) {
		$this->mch_capsule_model = $mch_capsule_model;
		$this->mch_capsule_page = $mch_capsule_page;
		$this->wpdb = $wpdb;
	}

	/**
	 * Init function.
	 */
	public function init() {
		// register hooks.
		$this->hooks();

		// Settings page.
		if (is_admin()) {
			$this->mch_capsule_page->init();
		}
	}

	/**
	 * Add actions.
	 */
	public function hooks() {
		// registration action.
		register_activation_hook(__FILE__, array($this, 'plugin_activate'));

		// Add Employer field to user edit page.
		add_action('show_user_profile', array($this, 'mch_capsule_integration_field'));
		add_action('edit_user_profile', array($this, 'mch_capsule_integration_field'));
		// Add Employer field save action.
		add_action( 'personal_options_update', array($this, 'mch_save_capsule_integration_field'));
		add_action( 'edit_user_profile_update', array($this, 'mch_save_capsule_integration_field'));

		// Add new task actions.
		add_action('mch_cancelled_lesson', array($this, 'mch_add_task'), 10, 2);
		add_action('mch_low_ranking', array($this, 'mch_add_task'), 10, 2);
		add_action('mch_low_ranking_app', array($this, 'mch_add_task'), 10, 2);
	}

	/**
	 * On plugin activation.
	 */
	public function plugin_activate() {
		global $wpdb;
		$capsule_model = new MCH_Capsule_Employers_Model($wpdb);
		$table_name = $capsule_model->get_table_name();
		if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
			$sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  name VARCHAR(255) NOT NULL,
				  token VARCHAR(255) NOT NULL,
				  UNIQUE KEY id (id)
				);";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);
		}
	}

	/**
	 * Render Employers field in admin panel on user edit page.
	 * @param $user WP_User;
	 */
	public function mch_capsule_integration_field($user) {
		if(!current_user_can('edit_pages')) return;
		$employers = $this->mch_capsule_model->get_all_employers();
		$user_employer = get_user_meta($user->ID, self::USER_EMPLOYER_OPTION, true); ?>

		<h3><?php _e('Capsule Integration') ?></h3>
		<table class="form-table">
			<tr>
				<th>
					<label for="<?php echo self::USER_EMPLOYER_OPTION; ?>"><?php _e('Classroom teacher manager'); ?></label>
				</th>
				<td>
					<select name="<?php echo self::USER_EMPLOYER_OPTION; ?>" id="<?php echo self::USER_EMPLOYER_OPTION; ?>">
						<option value><?php _e('Select Manager...'); ?></option>
						<?php foreach($employers as $employer): ?>
							<option value="<?php echo $employer->id; ?>" <?php selected($employer->id, $user_employer) ?>><?php echo $employer->name; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
		</table>
		<?php
	}

	/**
	 * Save Employers field.
	 * @param $user_id int
	 */
	public function mch_save_capsule_integration_field($user_id) {
		if(!current_user_can('edit_pages')) return;

		$capsule_employer = $_POST[self::USER_EMPLOYER_OPTION];
		update_user_meta($user_id, self::USER_EMPLOYER_OPTION, $capsule_employer);
	}

	/**
	 *
	 * Add new task to Capsule.
	 *
	 * @param int $user_id
	 * @param array $task_details information to add new task
	 *
	 * Format of array:
	 * $task_details = array(
	 *		'description' => $description . ' | ' . $user_name,
	 *		'detail' => $message,
	 *		'category' => __('classroom teacher issue'),
	 *		'dueDate' => date('Y-m-d'),
	 * );
	 */
	public function mch_add_task($user_id, array $task_details) {
		$employer_id = get_user_meta($user_id, self::USER_EMPLOYER_OPTION, true);
		if($employer_id == '') return;

		$employer = $this->mch_capsule_model->get_employer_by_id($employer_id);
		$app_name = get_option(MCH_Capsule_Page::APP_NAME_OPTION);
		if(!empty($app_name) && !empty($employer)) {
			$capsule = new MCH_Capsule($app_name, $employer->token);
			$capsule->add_task($task_details);
		}
	}
}

global $wpdb;
$mch_capsule_model = new MCH_Capsule_Employers_Model($wpdb);
$mch_capsule_page = new MCH_Capsule_Page($mch_capsule_model);
$mch_capsule_integration = new MCH_Capsule_Integration($mch_capsule_model, $mch_capsule_page, $wpdb);
$mch_capsule_integration->init();