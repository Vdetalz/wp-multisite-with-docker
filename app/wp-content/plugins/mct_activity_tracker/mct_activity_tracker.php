<?php
/*
Plugin Name: MCT Activity Tracker
Plugin URI:
Description: Plugin tracks count of views of each lesson of each user. Also plugin tracks last viewed lesson for each user and loads it after login.
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

add_action('admin_menu', 'mct_activity_tracker_admin_menu');
add_action('admin_init', 'mct_bootstrap_init');
add_action('admin_enqueue_scripts', 'mct_admin_enqueue_scripts');

add_filter('login_redirect', 'last_lesson_redirect', 10, 3);

/**
 * Connect bootstrap to admin side
 */
function mct_bootstrap_init()
{
    if (isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] == 'mct_activity_tracker.php' || $_GET['page'] == 'admin_most_recent_lesson') {
        wp_register_style('bootstrap_min', plugins_url('css/bootstrap.min.css', __FILE__));
        wp_enqueue_style('bootstrap_min');
        wp_register_style('bootstrap_theme', plugins_url('css/bootstrap-theme.min.css', __FILE__));
        wp_enqueue_style('bootstrap_theme');
        wp_register_style('main_plugin_style', plugins_url('css/style.css', __FILE__));
        wp_enqueue_style('main_plugin_style');
    }

}

function mct_admin_enqueue_scripts()
{
    wp_register_script('table_toggler', plugins_url('js/table_toggler.js', __FILE__));
    wp_enqueue_script('table_toggler');
}

/**
 * MCT Activity admin menu
 */
function mct_activity_tracker_admin_menu()
{

    if (function_exists('add_menu_page')) {
        add_menu_page(
            __('MCT Activity Tracker Menu'),
            __('MCT Activity Tracker'),
            null,
            basename(__FILE__),
            'mct_activity_tracker_options'
        );
    }

    if (function_exists('add_submenu_page')) {
        add_submenu_page(
            basename(__FILE__),
            __('Last Lesson Tracker'),
            __('Last Lesson Tracker'),
            1,
            'admin_last_lesson_view_remember',
            'mct_last_lesson_view_remember'
        );
    }

}

/**
 * Main page of MCT Activity plugin
 */
function mct_activity_tracker_options()
{
    $args = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => '',
        'meta_key' => '',
        'meta_value' => '',
        'meta_compare' => '',
        'meta_query' => array(),
        'include' => array(),
        'exclude' => array(),
        'orderby' => 'login',
        'order' => 'ASC',
        'offset' => '',
        'search' => '',
        'number' => '',
        'count_total' => false,
        'fields' => 'all',
        'who' => ''
    );

    $users = get_users($args);
    if (isset($_POST['users_dropdown']) && !empty($_POST['users_dropdown'])) {
        update_option('selected_user', $_POST['users_dropdown']);
    }
    ?>
    <h2><?php echo __('MCT Activity Tracker'); ?></h2>
    <form method="post">
        <p>
            <label for="users-dropdown"><?php echo __('Select user name'); ?></label>
            <select name="users_dropdown" id="users-dropdown">
                <option><?php echo __('--Select none--'); ?></option>
                <?php
                $selected_user = get_option('selected_user');
                foreach ($users as $user) {
                    ?>

                    <option
                        value="user-<?php echo $user->data->ID ?>" <?php if (isset($_POST['users_dropdown']) && !empty($_POST['users_dropdown'])) {
                        selected('user-' . $user->data->ID, $selected_user);
                    } ?>><?php echo $user->data->user_nicename; ?></option>

                <?php } ?>
            </select>
        </p>
        <input type="submit" class="btn btn-primary" value="<?php echo __('Show list'); ?>">
    </form>
    <br/>
    <?php
    if (isset($_POST['users_dropdown']) && !empty($_POST['users_dropdown'])) {
        global $wpcwdb, $wpdb;
        $user = $_POST['users_dropdown'];

        $last_post_user_id = substr(strstr($user, '-'), 1, strlen($user));
        $viewed_lesson_id = get_user_meta($last_post_user_id, 'last_lesson_id', true);
        $user_meta = get_user_meta($last_post_user_id);

        $args = array(
            'post_type' => 'download',
            'post_status' => 'publish'
        );

        $query = new WP_Query($args);
        $i = 1; ?>
        <ul class="nav nav-tabs" id="myTab">
            <?php
            while ($query->have_posts()): $query->the_post(); ?>
                <li>
                    <a href="#" class="activity-tab"
                       id="tab-<?php echo $i; ?>"><?php echo get_the_title(get_the_ID()); ?></a>
                </li>
                <?php
                $i++;
            endwhile; ?>
        </ul>
        <?php
        $k = 1;
        while ($query->have_posts()): $query->the_post();
            ?>
            <table class="table table-striped table-bordered main-course-table" id="table-<?php echo $k; ?>">
                <tbody>
                <?php
                $k++;
                $course_list = WPCW_courses_getCourseList();
                foreach ($course_list as $course) {
                    $checker = false;
                    $sql = $wpdb->prepare("SELECT *
				FROM $wpcwdb->courses
				WHERE course_title=%d
				ORDER BY course_title
				", $course);
                    $items = $wpdb->get_results($sql);

                    foreach ($items as $item) {
                        if ($item->course_title == $course) {
                            $module_details = WPCW_courses_getModuleDetailsList($item->course_id);
                            if ($module_details && !empty($module_details)) {
                                foreach ($module_details as $module) {
                                    $units_list = WPCW_units_getListOfUnits($module->module_id);
                                    if ($units_list && !empty($units_list)) {
                                        foreach ($units_list as $unit) {
                                            $restrict = get_post_meta($unit->ID, '_edd_cr_restricted_to');
                                            if ($restrict[0][0]['download'] == get_the_ID()) {
                                                $checker = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($checker != true) {
                        continue;
                    }

                    ?>
                    <tr class="course">
                        <th colspan="3" class="course-title">
                            <h4><?php echo $course; ?></h4>
                        </th>
                    </tr>

                    <?php


                    foreach ($items as $item) {
                        if ($item->course_title == $course) {
                            $module_details = WPCW_courses_getModuleDetailsList($item->course_id);
                            if ($module_details && !empty($module_details)) {
                                foreach ($module_details as $module) {

                                    ?>
                                    <tr class="module">
                                        <th colspan="3" class="module-title">
                                            <?php echo $module->module_title; ?>
                                        </th>
                                    </tr>
                                    <tr class="unit-header">
                                        <td><?php echo __('Lesson ID'); ?></td>
                                        <td><?php echo __('Lesson name'); ?></td>
                                        <td class="count-header"><?php echo __('Count of views'); ?></td>
                                    </tr>
                                    <?php
                                    $units_list = WPCW_units_getListOfUnits($module->module_id);
                                    if ($units_list && !empty($units_list)) {
                                        foreach ($units_list as $unit) {
                                            if ($user_meta && !empty($user_meta)) {
                                                foreach ($user_meta as $key => $value) {
                                                    $regexp = preg_match('/(lesson)(-)(\\d+)/', $key);
                                                    if ($regexp == 1) {
                                                        $lesson_id = substr(strstr($key, '-'), 1, strlen($key));
                                                        if ($lesson_id == $unit->ID) {
                                                            $views_count = $value[0];
                                                            $unit_title = $unit->post_title; ?>
                                                            <tr class="unit">
                                                                <td><?php echo $unit->ID; ?></td>
                                                                <td><?php echo $unit_title; ?></td>
                                                                <td class="counter"><?php echo $views_count; ?></td>
                                                            </tr>
                                                        <?php
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
                </tbody>
            </table>
        <?php endwhile;
    }
}

/**
 * Admin page of Last Lesson Tracker
 */
function mct_last_lesson_view_remember()
{
    $last_lesson_tracker = 'last_lesson_tracker';
    $last_lesson_tracker_option = get_option($last_lesson_tracker);

    if (isset($_POST['last_lesson_tracker']) && !empty($_POST['last_lesson_tracker'])) {
        $last_lesson_tracker_option = $_POST['last_lesson_tracker'];
        update_option('last_lesson_tracker', $_POST['last_lesson_tracker']);
    }
    ?>
    <h2><?php echo __('Last Lesson Tracker'); ?></h2>
    <form method="post" action>
        <p>
            <label for="last_lesson_tracker"><?php echo __('Track last viewed lesson?'); ?></label>
            <input type="checkbox" value="on" name="last_lesson_tracker"
                   id="last_lesson_tracker" <?php checked($last_lesson_tracker_option, 'on'); ?>/>
        </p>
        <input type="submit" value="<?php echo __('Save settings'); ?>" class="btn btn-primary">
    </form>

<?php
}

/**
 * Writing to DB last viewed lesson & counting all views of each lesson
 */
function mct_last_lesson_write($download_id, $course_id, $module_id, $post_id)
{
    $current_time = time();
    $user_id = get_current_user_id();
    $trial_period_time = get_user_meta($user_id, 'ending_trial_time', true);

    if ($post_id && !empty($post_id)) {

        $user_id = get_current_user_id();
        if ($user_id && !empty($user_id)) {

            $counts_lesson_view = get_user_meta($user_id, 'lesson-' . $post_id, true);

            if (isset($counts_lesson_view) && !empty($counts_lesson_view)) {
                $new_count_lesson_view = (int)$counts_lesson_view + 1;
            } else {
                $new_count_lesson_view = 1;
            }

            //Number of views writing when trial period finished
            if ($current_time > $trial_period_time) {
                update_user_meta($user_id, 'lesson-' . $post_id, $new_count_lesson_view);
            }

            if ($download_id && $course_id && $module_id && $post_id) {
                $update_data = array(
                    'download_id' => $download_id,
                    'course_id' => $course_id,
                    'module_id' => $module_id,
                    'unit_id' => $post_id
                );

                update_user_meta($user_id, 'last_lesson_id', $update_data);
            }
        }
    }

}

/**
 * Redirect to last viewed lesson after login
 *
 * @param $redirect_to
 * @param $request
 * @param $user
 *
 * @return bool|string|void
 */
function last_lesson_redirect($redirect_to, $request, $user)
{
    global $wpdb;
    $course_page_id = edd_get_option('mch_course_page_redirect');

    if (isset($user->roles) && is_array($user->roles)) {

        //check for admins
        if (in_array('administrator', $user->roles)) {
            // Redirect to default admin area
            return admin_url();
        } elseif (in_array('students', $user->roles)) {
            //Redirect students to default lesson or lesson which his teacher set
            $next_lesson_id = get_user_meta($user->ID, 'next_lesson_id', true);
            if ($next_lesson_id && !empty($next_lesson_id)) {
                 $next_lesson_link = get_permalink($course_page_id) . '?download_number=' . $next_lesson_id['download_id'] .
                    '&level_grade=' . $next_lesson_id['course_id'] . '&module_number=' . $next_lesson_id['module_id'] . '&actual_lesson=' . $next_lesson_id['unit_id'];
                return $next_lesson_link;
            } else {
                $default_lesson = (int)get_option('default_lesson');
                if(!$default_lesson){
                    return get_permalink($course_page_id);
                }
                $sql = $wpdb->prepare("SELECT `parent_module_id`, `parent_course_id` FROM `" . $wpdb->prefix . "wpcw_units_meta` WHERE `unit_id` = %d", $default_lesson);
                $query = $wpdb->get_row($sql);
                $default_lesson_link = get_permalink($course_page_id) . '?download_number=16&level_grade=' .
                    $query->parent_course_id . '&module_number=' . $query->parent_module_id . '&actual_lesson=' . $default_lesson;
                return $default_lesson_link;
            }
        } elseif (in_array('author', $user->roles)) {
            // Redirect to teacher's portal
            return get_permalink(edd_get_option('mct_edd_author_login_redirect'));
        } else {
            $user_id = $user->ID;
            if ($user_id && !empty($user_id)) {
                $user_last_lesson_id = get_user_meta($user_id, 'last_lesson_id', true);

                if ($user_last_lesson_id && !empty($user_last_lesson_id)) {
                    $last_lesson_link = get_permalink($course_page_id) . '?download_number=' . $user_last_lesson_id['download_id'] .
                        '&level_grade=' . $user_last_lesson_id['course_id'] . '&module_number=' . $user_last_lesson_id['module_id'] . '&actual_lesson=' . $user_last_lesson_id['unit_id'];
                    return $last_lesson_link;
                }
                else{
                    $last_lesson_link = get_permalink($course_page_id);
                    return $last_lesson_link;
                }
            }
        }
    }
    return home_url();
}
