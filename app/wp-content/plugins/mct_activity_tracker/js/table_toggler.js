jQuery(document).ready(function($){
	$('.main-course-table:first').addClass('selected');
	$('.activity-tab:first').addClass('selected-tab');
	$('.activity-tab:first').parent().addClass('active');

	$('.main-course-table').each(function(){
		if(!$(this).hasClass('selected')){
			$(this).hide();
		}
	});

	$('a.activity-tab').click(function(e){
		e.preventDefault();
		$('a.activity-tab').each(function(){
			$(this).parent().removeClass('active');
			$(this).removeClass('selected-tab');
		});
		$(this).parent().addClass('active');
		$(this).addClass('selected-tab');
		var tab_id_full = $(this).attr('id');
		var tab_id = tab_id_full.split('-')[1];
		$('.main-course-table').each(function(){
			var table_id_full = $(this).attr('id');
			var table_id = table_id_full.split('-')[1];
			if(table_id != tab_id){
				$(this).removeClass('selected');
				$(this).hide();
			}
			else{
				$(this).addClass('selected');
				$(this).show();
			}
		});

		$('.unit-header').each(function () {
			$(this).show();
		});

		$('tr.module').each(function () {
			$(this).show();
		});

		$('.course').each(function () {
			$(this).show();
		});

		$('table.table').each(function() {
			$('.unit-header').each(function () {
				if (!$(this).next().hasClass('unit')) {
					$(this).hide();
				}
			});

			$('tr.module').each(function () {
				if ($(this).next().is(':hidden')) {
					$(this).hide();
				}
			});

			$('.course').each(function () {
				if ($(this).next().is(':hidden')) {
					$(this).hide();
				}
			});
		});

	});

	$('table.table').each(function() {
		$('.unit-header').each(function () {
			if (!$(this).next().hasClass('unit')) {
				$(this).hide();
			}
		});

		$('tr.module').each(function () {
			if ($(this).next().is(':hidden')) {
				console.log($(this).next().is(':hidden'));
				$(this).hide();
			}
		});

		$('.course').each(function () {
			if ($(this).next().is(':hidden')) {
				$(this).hide();
			}
		});
	});

});