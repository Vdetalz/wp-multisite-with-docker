(function($) {

$(document).ready( function() {
	var file_frame; // variable for the wp.media file_frame
	// attach a click event (or whatever you want) to some element on your page
	$( '#frontend-button' ).on( 'click', function( event ) {
		event.preventDefault();

		// if the file_frame has already been created, just reuse it
		if ( file_frame ) {
			file_frame.open();
			return;
		}

		$('.screen-reader-text').html("");
		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Upload Image',
			button: {
				text: $( this ).data( 'uploader_button_text' ),
			},
            library: {
                type: [ 'image/jpeg', 'image/png' ]
            },
			multiple: false // set this to true for multiple file selection
		});

		file_frame.on( 'select', function() {
			attachment = file_frame.state().get('selection').first().toJSON();
            if(!tinyMCE.activeEditor) {
            	$('.wp-editor-wrap .switch-tmce').trigger('click')
            }
            tinyMCE.activeEditor.dom.add(tinyMCE.activeEditor.getBody(), 'img', {src: attachment.url});
		});

		file_frame.open();
	});
});

})(jQuery);