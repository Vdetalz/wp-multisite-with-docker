CMA_Utils = {};

CMA_Utils.addSingleHandler = function(handlerName, selector, action, func) {
	jQuery(selector).each(function() {
		var obj = jQuery(this);
		if (obj.data(handlerName) != '1') {
			obj.data(handlerName, '1');
			obj.on(action, func);
		}
	});
};


CMA_script_init = function ($) {
	
	$.limitify = function() {
		if (typeof CMA_LIMITIFY == 'undefined') return;
		$('*[data-limitify]').filter(function() {
			return ($(this).attr('data-limitify') > 0 && !this.limitifyWorking);
		}).each(function() {
			this.limitifyWorking = true;
			var obj = $(this);
			var limit = obj.data('limitify');
			var tooltip = $(document.createElement('div'));
			tooltip.addClass('cma-limitify');
			obj.after(tooltip);
			
			var update = function() {
				var len = obj.val().length;
				if (len > limit) {
					obj.val(obj.val().substr(0, limit));
					len = limit;
				}
				tooltip.text(len +"/"+ limit);
			};
			update();
			obj.keyup(update);
			
			return this;
		});
		
	};
	

	function thumbsUp (url, answerId) {
		thumbs(url, answerId, 'up');
	}

	function thumbsDown (url, answerId) {
		thumbs(url, answerId, 'down');
	}

	function thumbs (url, answerId, upDown) {
		if (answerId != null) {
			$('#answer-' + answerId + ' .cma-rating-loading').show();
		} else {
			$('.cma-question-table .cma-rating-loading').show();
		}
		$.post(url, {'cma-action': 'vote', 'cma-answer': answerId, 'cma-value': upDown}, function (data) {
			if (data.success == 1) {
				if (answerId != null) {
					$('tr[data-answer-id=' + answerId + ']').attr('data-rating', data.message);
					$('tr[data-answer-id=' + answerId + '] .cma-rating-count').text(data.message);
				} else {
					$('.cma-question-table').attr('data-rating', data.message);
					$('.cma-question-table .cma-rating-count').text(data.message);
				}
				$().toastmessage('showSuccessToast', CMA_Variables.messages.thanks_for_voting);
			} else {
				$().toastmessage('showErrorToast', data.message);
			}
			if (answerId != null) {
				$('#answer-' + answerId + ' .cma-rating-loading').hide();
			} else {
				$('.cma-question-table .cma-rating-loading').hide();
			}
		});
	}
	
	
	
	CMA_Utils.addSingleHandler('bestAnswerHandler', '.cma-mark-best-answer a, .cma-unmark-best-answer a', 'click', function() {
		var answerId = $(this).data('answer-id');
		var table = $(this).parents('table');
		var url = $(this).parents('[data-permalink]').data('permalink');
		var data = {'cma-action': 'mark-best-answer', 'cma-answer-id': answerId};
		$.post(url, data, function (data) {
			if (data.success == 1) {
				table.find('.cma-best-answer').removeClass('cma-best-answer');
				if (data.marked == 1) {
					table.removeClass('cma-best-answer-undefined').addClass('cma-best-answer-defined');
					table.find('tr[data-answer-id='+ answerId +']').addClass('cma-best-answer');
				} else {
					table.removeClass('cma-best-answer-defined').addClass('cma-best-answer-undefined');
				}
				$().toastmessage('showSuccessToast', data.message);
			} else {
				$().toastmessage('showErrorToast', data.message);
			}
		});
		return false;
	});
	
	
	CMA_Utils.addSingleHandler('favoriteQuestionHandler', '.cma-question-favorite-link', 'click', function() {
		var obj = $(this);
		$.post(obj.parents('[data-permalink]').data('permalink'), {'cma-action': 'favorite'}, function (data) {
			if (data.success == 1) {
				if (data.favorite) {
					obj.parents('table').addClass('cma-question-favorite');
				} else {
					obj.parents('table').removeClass('cma-question-favorite');
				}
				obj.attr('title', data.title);
				obj.find('.number').text(data.number);
				$().toastmessage('showSuccessToast', data.message);
			} else {
				$().toastmessage('showErrorToast', data.message);
			}
		});
		return false;
	});
	

		
	$.limitify();
		
	CMA_Utils.addSingleHandler('ratingUpHandler', '.cma-rating .cma-thumbs-up', 'click', function() {
		var $this, $parentTr;
		$this = $(this);
		thumbsUp($this.parents('*[data-permalink]').data('permalink'), $this.parents('tr.cma-answer').data('answer-id'));
	});
	
	CMA_Utils.addSingleHandler('ratingDownHandler', '.cma-rating .cma-thumbs-down', 'click', function() {
		var $this, $parentTr;
		$this = $(this);
		thumbsDown($this.parents('*[data-permalink]').data('permalink'), $this.parents('tr.cma-answer').data('answer-id'));
	});
	
	CMA_Utils.addSingleHandler('cmaFilterHandler', 'form.cma-filter', 'submit', function(ev) {
		// Set form action to the choosen category (or subcategory) URL
		var form = $(this);
		var secondaryCategoryUrl = form.find('.cma-filter-category-secondary').find(":selected").data('url');
		if (secondaryCategoryUrl) {
			form.attr('action', secondaryCategoryUrl);
		} else {
			var primaryCategoryUrl = form.find('.cma-filter-category-primary').find(":selected").data('url');
			if (primaryCategoryUrl) {
				form.attr('action', primaryCategoryUrl);
			} else {
				var categoryUrl = form.find('.cma-filter-category').find(':selected').data('url');
				if (categoryUrl) {
					form.attr('action', categoryUrl);
				}
			}
		}
	});
	
	CMA_Utils.addSingleHandler('filterCategoryPrimaryHandler', '.cma-filter-category-primary', 'change', function() {
		// Load subcategories of choosen primary category.
		var selectBox = $(this);
		var option = this.options[this.selectedIndex];
		var subcategoriesBox = selectBox.parents('form').find('.cma-filter-category-secondary').first();
		var createOption = function(parent, value, content, url) {
			var option = document.createElement('option');
			parent.append(option);
			$(option).attr('value', value).data('url', url).html(content);
		};
		$.post(selectBox.parents('form').attr('action'), {'cma-action': 'load-subcategories', 'cma-category-id': option.value}, function(categories) {
			subcategoriesBox.find('option').remove();
			for (var i=0; i<categories.length; i++) {
				createOption(subcategoriesBox, categories[i].id, categories[i].name, categories[i].url);
			}
		});
	});
	
	CMA_Utils.addSingleHandler('threadFollowHandler', '.cma-follow-link', 'click', function() {
		var link = $(this);
		var data = {'cma-action': 'follow', 'categoryId' : link.data('categoryId')};
		$.post(link.attr('href'), data, function (response) {
			if (response.success) {
				$().toastmessage('showSuccessToast', response.message);
			} else {
				$().toastmessage('showErrorToast', response.message);
			}
		}, 'JSON');
		return false;
	});
	
	CMA_Utils.addSingleHandler('reportSpamHandler', '.cma-report-spam', 'click', function() {
		var link = $(this);
		var data = {'cma-action': 'report-spam'};
		data.answerId = link.parents('tr').data('answerId');
		$.post(link.attr('href'), data, function (response) {
			if (response.success) {
				link.parents(data.answerId ? 'tr' : 'table').addClass('cma-marked-spam');
				$().toastmessage('showSuccessToast', response.message);
			} else {
				$().toastmessage('showErrorToast', response.message);
			}
		}, 'JSON');
		return false;
	});
	
	CMA_Utils.addSingleHandler('unmarkSpamHandler', '.cma-unmark-spam', 'click', function() {
		var link = $(this);
		var data = {'cma-action': 'unmark-spam'};
		data.answerId = link.parents('tr').data('answerId');
		$.post(link.parents('[data-permalink]').data('permalink'), data, function (response) {
			if (response.success) {
				link.parents(data.answerId ? 'tr' : 'table').removeClass('cma-marked-spam');
				$().toastmessage('showSuccessToast', response.message);
			} else {
				$().toastmessage('showErrorToast', response.message);
			}
		}, 'JSON');
		return false;
	});
	
	var answersWidgetPaginationHandler = function() {
		var link = $(this);
		var container = link.parents('.cma-answers-widget');
		container.addClass('cma-loading');
		container.append($('<div/>', {"class":"cma-loader"}));
		$.ajax({
			url: this.href,
			success: function(response) {
				var html = $(response);
				container.find('.cma-loader').remove();
				container.html(html.find('.cma-answers-widget').html());
				container.find('.cma-pagination a').click(answersWidgetPaginationHandler);
			}
		});
		return false;
	};
	CMA_Utils.addSingleHandler('answersWidgetPaginationHandler', '.cma-answers-widget[data-ajax=1] .cma-pagination a', 'click', answersWidgetPaginationHandler);
	
	
	$('.cma-file-upload').on('dragenter', function(e) {
		e.stopPropagation();
		e.preventDefault();
		
	});
	$('.cma-file-upload').on('dragleave', function(e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).removeClass('dragover');
	});
	$('.cma-file-upload').on('dragover', function(e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).addClass('dragover');
	});
	function readFiles(container, files) {
		if (typeof FormData == 'undefined' || typeof FileReader == 'undefined' || !'draggable' in document.createElement('span')) return false;
		if (container.data('progress') == 1) return true;
		container.data('progress', 1);
		var disabledElements = container.find('input[type=file]');
		disabledElements.attr('disabled', 'disabled');
		var list = container.find('.cma-file-upload-list');
		var formData = new FormData();
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			formData.append('cma-file[]', file);
			var item = document.createElement('li');
			item.appendChild(document.createTextNode(file.name));
			item.setAttribute('class', 'ajax progress');
			item.setAttribute('data-file-name', file.name);
			list.append(item);
		}
		formData.append('cma-action', 'upload');
		var xhr = new XMLHttpRequest();
		console.log(container.parents('form').attr('action'));
		xhr.open('POST', container.parents('form').attr('action'));
		xhr.onload = function(e) {
			var response;
			try {
				response = $.parseJSON(e.target.response);
			} catch (e) {}
			if (e.target.status == 200 && typeof(response) == 'object') {
				for (var i=0; i<response.length; i++) {
					var fileResult = response[i];
					var item = list.find('li[data-file-name="'+ fileResult.name +'"]');
					item.removeClass('progress');
					if (fileResult.status == 'OK') {
						var hidden = document.createElement('input');
						hidden.setAttribute('type', 'hidden');
						hidden.setAttribute('name', 'attached[]');
						hidden.setAttribute('value', fileResult.id);
						item.append(hidden);
					} else {
						item.addClass('error');
						item.append('<span>' + (fileResult.msg ? fileResult.msg : 'error') + '</span>');
					}
				}
			}
			list.find('li.progress').remove();
			disabledElements.removeAttr('disabled');
			container.data('progress', 0);
		};
		if ("upload" in new XMLHttpRequest) {
			xhr.upload.onprogress = function (event) {
				if (event.lengthComputable) {
					var complete = (event.loaded / event.total * 100 | 0);
					//progress.value = progress.innerHTML = complete;
				}
			};
		}
		xhr.send(formData);
		return true;
	};
	$('.cma-file-upload').on('drop', function(e) {
		var obj = $(this);
		e.stopPropagation();
		e.preventDefault();
		$(this).removeClass('dragover');
		if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files) {
			readFiles(obj, e.originalEvent.dataTransfer.files);
		}
	});
	$('.cma-file-select').click(function(e) {
		e.stopPropagation();
		e.preventDefault();
		var file = $(this).parent().find('input[type=file]');
		file.click();
	});
	$('.cma-file-upload input[type=file]').on('change', function(e) {
		var container = $(this).parents('.cma-file-upload');
		if (readFiles(container, this.files)) {
			$(this).removeAttr('name');
		} else {
			var list = container.find('.cma-file-upload-list');
			list.find('li.input').remove();
			for (var i=0; i<this.files.length; i++) {
				var file = this.files[i];
				var item = document.createElement('li');
				item.appendChild(document.createTextNode(file.name));
				item.setAttribute('class', 'input');
				list.append(item);
			}
		}
	});
	
	$('table.cma-question-table:not(.cma-count-view-sent)').each(function() {
		var obj = $(this);
		obj.addClass('cma-count-view-sent');
		$.post(obj.data('permalink'), {"cma-action":"count-view"}, function() {
//			console.log('count-view-ok');
		});
	});
	

	
};


jQuery(CMA_script_init);




(function () {
	var po = document.createElement('script');
	po.type = 'text/javascript';
	po.async = true;
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(po, s);
})();

(function (d, s, id) {
	var fbAsyncInitCMA = function () {
		// Don't init the FB as it needs API_ID just parse the likebox
		FB.XFBML.parse();
	};
	if (typeof(window.fbAsyncInit) == 'function') {
		var fbAsyncInitOld = window.fbAsyncInit;
		window.fbAsyncInit = function() {
			fbAsyncInitOld();
			fbAsyncInitCMA();
		};
	} else {
		window.fbAsyncInit = fbAsyncInitCMA;
	}

	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id))
		return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));