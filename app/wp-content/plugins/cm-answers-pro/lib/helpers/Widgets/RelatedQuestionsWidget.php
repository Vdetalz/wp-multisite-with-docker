<?php

class CMA_RelatedQuestionsWidget extends WP_Widget
{

    public function CMA_RelatedQuestionsWidget()
    {
        $widget_ops = array('classname' => 'CMA_RelatedQuestionsWidget', 'description' => 'Shows related questions');
        $this->WP_Widget('CMA_RelatedQuestionsWidget', 'CM Answers Related Questions Widget', $widget_ops);
    }

    public static function getInstance()
    {
        return register_widget(get_class());
    }

    /**
     * Widget options form
     * @param WP_Widget $instance
     */
    public function form($instance)
    {
        if(isset($instance['title']))
        {
            $title = $instance['title'];
        }
        else
        {
            $title = CMA::__('Related questions');
        }
        
        $limit = isset($instance['limit']) ? $instance['limit'] : 10;
        
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_name('title')); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_name('limit')); ?>"><?php _e('Limit:'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('limit')); ?>" name="<?php echo esc_attr($this->get_field_name('limit')); ?>" type="text" value="<?php echo esc_attr($limit); ?>" />
        </p>

        <?php
    }

    /**
     * Update widget options
     * @param WP_Widget $new_instance
     * @param WP_Widget $old_instance
     * @return WP_Widget
     */
    public function update($new_instance, $old_instance)
    {
        $instance          = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['limit'] = (!empty($new_instance['limit']) ) ? strip_tags($new_instance['limit']) : '';

        return $instance;
    }

    /**
     * Render widget
     *
     * @param array $args
     * @param WP_Widget $instance
     */
    public function widget($args, $instance)
    {
    	global $wp_query;
    	
    	if (empty($args['limit'])) $args['limit'] = 5;
    	if ($wp_query->get('post_type') != CMA_Thread::POST_TYPE OR !$wp_query->is_single()) return;
    	$post = $wp_query->get_posts();
    	$post = reset($post);
    	if (!empty($post) AND $post->post_type == CMA_Thread::POST_TYPE) {
			$download = CMA_Thread::getInstance($post->ID);
			$questions = $download->getRelated($args['limit']);
		}
		if (empty($questions)) return;
    	
        extract($args, EXTR_SKIP);
        $title = apply_filters('widget_title', $instance['title']);
		
        echo $before_widget;
        if(!empty($title)) echo $before_title . $title . $after_title;
        
        ?>
        <div class="cma-related-questions-widget"><ul><?php
        
        foreach ($questions as $question) {
			$thread = CMA_Thread::getInstance($question->ID);
			printf('<li><a href="%s"><span>%d</span><span>%s</span></a></li>', $thread->getPermalink(), $thread->getViews(), $thread->getTitle());
		}
        
        ?></ul></div>
        <?php
        echo $after_widget;
    }

}


function cma_register_related_questions_widget()
{
    register_widget('CMA_RelatedQuestionsWidget');
}
add_action('widgets_init', 'cma_register_related_questions_widget');
