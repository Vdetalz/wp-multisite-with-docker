<?php

class CMA {
	
    const TEXT_DOMAIN = 'cm-answers-pro';
    const OPTION_VERSION = 'cma_version';

    static $version;
    protected static $isLicenseOk;

    public static function init($pluginFilePath)
    {

        register_activation_hook($pluginFilePath, array(__CLASS__, 'install'));
        register_uninstall_hook($pluginFilePath, array(__CLASS__, 'uninstall'));

        self::update();

        // Check licensing API before controller init
        $licensingApi = new CMA_Cminds_Licensing_API('CM Answers Pro', CMA_Thread::ADMIN_MENU, 'CM Answers Pro', CMA_PLUGIN_FILE,
        	array('release-notes' => 'http://answers.cminds.com/release-notes/'), '', array('CM Answers Pro'));
        self::$isLicenseOk = $licensingApi->isLicenseOk();
        
        CMA_Thread::init();
        CMA_BuddyPress::init();
		
        add_action('init', array('CMA_BaseController', 'bootstrap'));
        add_action('wp_enqueue_scripts', array(__CLASS__, 'enable_scripts'));
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enable_admin_scripts'));
        add_action('wp_head', array(__CLASS__, 'add_base_url'));

        add_filter('bp_blogs_record_comment_post_types', array(get_class(), 'bp_record_my_custom_post_type_comments'));
        
        add_filter('plugin_action_links_' . CMA_PLUGINNAME, array(__CLASS__, 'cma_settings_link'));
        add_filter('cm_micropayments_integrations', function($a = array()) {
        	if (!is_array($a)) $a = array();
        	$a[] = 'CM Answers Pro';
        	return $a;
        });
        
    }
    
    
    public static function isLicenseOk() {
    	return self::$isLicenseOk;
    }
    
    
    public static function getPostingUserId() {
    	if (is_user_logged_in()) {
    		return get_current_user_id();
    	} else {
    		return apply_filters('cma_anonymous_user_id', array());
    	}
    }
    
    
    /**
     * Get CMA index permalink.
     * 
     * @return string
     */
    public static function permalink() {
    	return get_post_type_archive_link(CMA_Thread::POST_TYPE);
    }
    

    /**
     * Add settings link on plugin page
     * @param type $links
     * @return type
     */
    public static function cma_settings_link($links)
    {
        $links['delete cma_tables'] = '<a href="' . esc_attr(admin_url('admin.php?page=CMA_admin_settings&flush_cma_db=1'))
        	. '" class="delete">' . __('Clear database', 'cm-answers-pro') . '</a>';
        return $links;
    }

    public static function install($networkwide)
    {
        global $wpdb;

        flush_rewrite_rules();

        if( function_exists('is_multisite') && is_multisite() )
        {
// check if it is a network activation - if so, run the activation function for each blog id
            if( $networkwide )
            {
                $old_blog = $wpdb->blogid;
// Get all blog ids
                $blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM {$wpdb->blogs}"));
                foreach($blogids as $blog_id)
                {
                    switch_to_blog($blog_id);
                    self::install_blog();
                }
                switch_to_blog($old_blog);
                return;
            }
        }
        self::install_blog();
    }

    public static function uninstall()
    {
        flush_rewrite_rules();
    }

    public static function install_blog()
    {

    }

    public static function add_base_url()
    {
    }

    public static function enable_scripts()
    {
        wp_enqueue_script('jquery');
//        wp_enqueue_script('jquery-bbq', CMA_URL . '/views/resources/ajax/jquery.ba-bbq.min.js', array('jquery'));
//        wp_enqueue_script('cma-ajax', CMA_URL . '/views/resources/ajax/ajax.js', array('jquery', 'jquery-bbq'));
    }

    public static function enable_admin_scripts()
    {
        wp_enqueue_script('cma-admin-script', CMA_RESOURCE_URL . 'admin_script.js', array('jquery'), false, true);
    }
    
    
    public static function getReferer() {
    	global $wp_query;
    	
    	$isEditPage = function($url) {
    		if (is_array($url)) $params = $url;
    		else parse_str(parse_url($url, PHP_URL_QUERY), $params);
    		$editParams = array(CMA_AnswerController::PARAM_EDIT_ANSWER_ID, CMA_AnswerController::PARAM_EDIT_QUESTION_ID);
    		return (count(array_intersect(array_keys($params), $editParams)) > 0);
    	};
    	$isTheSameHost = function($a, $b) {
    		return parse_url($a, PHP_URL_HOST) == parse_url($b, PHP_URL_HOST);
    	};
    	
    	$canUseReferer = (!empty($_SERVER['HTTP_REFERER'])
    			AND $isTheSameHost($_SERVER['HTTP_REFERER'], site_url())
    			AND !$isEditPage($_SERVER['HTTP_REFERER']));
    	$canUseCurrentPost = (is_single() AND !empty($wp_query->post) AND $wp_query->post->post_type == CMA_Thread::POST_TYPE
    			AND $isEditPage($_GET));
    	
    	if (!empty($_GET['backlink'])) { // GET backlink param
    		return base64_decode(urldecode($_GET['backlink']));
    	}
    	else if (!empty($_POST['backlink'])) { // POST backlink param
    		return $_POST['backlink'];
    	}
    	else if ($canUseReferer) { // HTTP referer
    		return $_SERVER['HTTP_REFERER'];
    	}
    	else if ($canUseCurrentPost) { // Question permalink
    		return get_permalink($wp_query->post->ID);
    	} else { // CMA index page
    		return get_post_type_archive_link(CMA_Thread::POST_TYPE);
    	}
    }
    

    /**
     * BuddyPress record custom post type comments
     * @param array $post_types
     * @return string
     */
    public static function bp_record_my_custom_post_type_comments($post_types)
    {
        $post_types[] = CMA_Thread::POST_TYPE;
        return $post_types;
    }

    /**
     * Get localized string.
     *
     * @param string $msg
     * @return string
     */
    public static function __($msg)
    {
        return __($msg, self::TEXT_DOMAIN);
    }

    /**
     * Get plugin's version.
     *
     * @return string|null
     */
    public static function version()
    {
        if( !empty(self::$version) )
        {
            return self::$version;
        }
        else
        {
            $readme = file_get_contents(CMA_PATH . '/readme.txt');
            preg_match('/Stable tag\: ([0-9\.]+)/i', $readme, $match);
            if( isset($match[1]) )
            {
            	self::$version = $match[1];
                return $match[1];
            }
        }
    }

    /**
     * Check if plugin has been updated and perform some changes.
     */
    protected static function update()
    {
        global $wpdb;
        
        
        $oldVersion = get_option(self::OPTION_VERSION);
        $currentVersion = self::version();
        if( empty($oldVersion) OR version_compare($oldVersion, $currentVersion, '<') )
        {
            update_option(self::OPTION_VERSION, $currentVersion);

            if (version_compare($oldVersion, '2.1.6', '<')) {
				// Update comment_type
	            $commentsIds = $wpdb->get_col($wpdb->prepare("SELECT `$wpdb->comments`.`comment_ID`
					FROM `$wpdb->comments`
					LEFT JOIN `$wpdb->posts` ON `$wpdb->comments`.`comment_post_ID` = `$wpdb->posts`.`ID`
					WHERE `$wpdb->posts`.`post_type` = %s AND `$wpdb->comments`.`comment_type` = ''", CMA_Thread::POST_TYPE));
	            $data = array('comment_type' => CMA_Answer::COMMENT_TYPE);
	            foreach($commentsIds as $comment_ID)
	            {
	                $rval = $wpdb->update($wpdb->comments, $data, compact('comment_ID'));
	            }
            }
            
            // Update new post meta: question+answers votes count
            if (version_compare($oldVersion, '2.1.7', '<')) {
	            $posts = get_posts(array('post_type' => CMA_Thread::POST_TYPE));
	            foreach ($posts as $post) {
	            	$thread = CMA_Thread::getInstance($post->ID);
	            	update_post_meta($post->ID, CMA_Thread::$_meta['votes_question'], 0);
	            	$thread->recountVotes();
	            }
            }
            
            // Update users counters
            if (version_compare($oldVersion, '2.1.9', '<=')) {
            	CMA_Thread::updateAllQA();
            }
            
            if (version_compare($oldVersion, '2.1.13', '<=')) {
            	// Create logs records for old posts
            	$posts = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type = %s", CMA_Thread::POST_TYPE));
            	$postsLog = new CMA_QuestionPostLog;
            	foreach ($posts as $post) {
            		$count = $wpdb->get_var($wpdb->prepare('SELECT COUNT(*) FROM '. $postsLog->getMetaTableName() .' m
            				JOIN '. $postsLog->getTableName() .' l ON l.id = m.log_id
            				WHERE m.meta_name = %s AND m.meta_value = %s AND l.log_type = %s',
            				CMA_QuestionPostLog::META_QUESTION_ID,
            				(string)$post->ID,
            				CMA_QuestionPostLog::LOG_TYPE));
            		if ($count == 0) {
	            		$postsLog->create(array(
	            			CMA_Log::FIELD_CREATED => $post->post_date,
	            			CMA_Log::FIELD_USER_ID => $post->post_author,
	            			CMA_Log::FIELD_IP_ADDR => NULL,
	            		), array(
	            			CMA_QuestionPostLog::META_QUESTION_ID => $post->ID,
	            		));
            		}
            	}
            	// Create logs records for old comments
            	$answers = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->comments WHERE comment_type = %s", CMA_Answer::COMMENT_TYPE));
            	$answersLog = new CMA_AnswerPostLog();
            	foreach ($answers as $answer) {
            		$count = $wpdb->get_var($wpdb->prepare('SELECT COUNT(*) FROM '. $answersLog->getMetaTableName() .' m
            			JOIN '. $answersLog->getTableName() .' l ON l.id = m.log_id
            			WHERE m.meta_name = %s AND m.meta_value = %s AND l.log_type = %s',
            			CMA_QuestionPostLog::META_QUESTION_ID,
            			(string)$answer->comment_ID,
            			CMA_AnswerPostLog::LOG_TYPE));
            		if ($count == 0) {
	            		$postsLog->create(array(
	            			CMA_Log::FIELD_CREATED => $answer->comment_date,
	            			CMA_Log::FIELD_USER_ID => $answer->user_id,
	            			CMA_Log::FIELD_IP_ADDR => NULL,
	            		), array(
	            			CMA_AnswerPostLog::META_QUESTION_ID => $answer->comment_post_ID,
	            			CMA_AnswerPostLog::META_ANSWER_ID => $answer->comment_ID,
	            		));
            		}
            	}
            }
            
            if (version_compare($oldVersion, '2.2.2', '<=')) {
            	// Votes for questions
            	$metaKeys = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = %s", CMA_Thread::$_meta['usersRated']));
            	foreach ($metaKeys as $mk) {
            		$value = @unserialize($mk->meta_value);
            		if (is_array($value)) {
            			$value = array_filter($value);
            			foreach ($value as $userId) {
            				add_post_meta($mk->post_id, CMA_Thread::$_meta['usersRated'], $userId, $uniq = false);
            			}
            			$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_id = %d", $mk->meta_id));
            		}
            	}
            	// Votes for answers
            	$metaKeys = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->commentmeta WHERE meta_key = %s", CMA_Answer::META_USERS_RATED));
            	foreach ($metaKeys as $mk) {
            		$value = @unserialize($mk->meta_value);
            		if (is_array($value)) {
            			$value = array_filter($value);
            			foreach ($value as $userId) {
            				add_comment_meta($mk->comment_id, CMA_Answer::META_USERS_RATED, $userId, $uniq = false);
            			}
            			$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->commentmeta WHERE meta_id = %d", $mk->meta_id));
            		}
            	}
            }
            
            
        }
    }

    public static function flushDatabase()
    {
        global $wpdb;

        /* @var $wpdb wpdb */
        $posts = $wpdb->delete($wpdb->posts, array('post_type' => CMA_Thread::POST_TYPE));
        $answers = $wpdb->delete($wpdb->comments,  array('comment_type' => CMA_Answer::COMMENT_TYPE));
        $categories = $wpdb->delete($wpdb->term_taxonomy,  array('taxonomy' => CMA_Category::TAXONOMY));
        $wpdb->query('DELETE FROM '.$wpdb->options.' WHERE option_name LIKE  \'%cma_%\'');
    }
    
    
    public static function parse_php_info()
    {
    	ob_start();
    	phpinfo(INFO_MODULES);
    	$s = ob_get_clean();
    	$s = strip_tags($s, '<h2><th><td>');
    	$s = preg_replace('/<th[^>]*>([^<]+)<\/th>/', "<info>\\1</info>", $s);
    	$s = preg_replace('/<td[^>]*>([^<]+)<\/td>/', "<info>\\1</info>", $s);
    	$vTmp = preg_split('/(<h2>[^<]+<\/h2>)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
    	$vModules = array();
    	for($i = 1; $i < count($vTmp); $i++) {
    		if( preg_match('/<h2>([^<]+)<\/h2>/', $vTmp[$i], $vMat) ) {
    			$vName = trim($vMat[1]);
    			$vTmp2 = explode("\n", $vTmp[$i + 1]);
    			foreach($vTmp2 AS $vOne) {
    				$vPat = '<info>([^<]+)<\/info>';
    				$vPat3 = "/$vPat\s*$vPat\s*$vPat/";
    				$vPat2 = "/$vPat\s*$vPat/";
    				if( preg_match($vPat3, $vOne, $vMat) ) { // 3cols
    					$vModules[$vName][trim($vMat[1])] = array(trim($vMat[2]), trim($vMat[3]));
    				}
    				elseif( preg_match($vPat2, $vOne, $vMat) ) { // 2cols
    					$vModules[$vName][trim($vMat[1])] = trim($vMat[2]);
    				}
    			}
    		}
    	}
    	return $vModules;
   	}

}
