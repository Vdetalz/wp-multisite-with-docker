<?php

class CMA_AnswerController extends CMA_BaseController {

    const PARAM_EDIT_QUESTION_ID = 'editQuestionId';
    const PARAM_EDIT_ANSWER_ID = 'editAnswerId';
    const PARAM_RESOLVE_QUESTION_ID = 'resolveQuestionId';
    
    const NONCE_FOLLOW = 'cma_follow';
    

    public static function initialize()
    {
    	
    	add_filter('manage_edit-' . CMA_Thread::POST_TYPE . '_columns', array(get_class(), 'registerAdminColumns'));
    	add_filter('manage_' . CMA_Thread::POST_TYPE . '_posts_custom_column', array(get_class(), 'adminColumnDisplay'), 10, 2);
    	do_action('CMA_custom_post_type_nav', CMA_Thread::POST_TYPE);
    	add_filter('CMA_admin_settings', array(get_class(), 'addAdminSettings'));
    	add_action('pre_get_posts', array(get_class(), 'registerCustomOrder'), 9999, 1);
    	add_action('pre_get_posts', array(get_class(), 'registerTagFilter'), 9999, 1);
    	add_action('pre_get_posts', array(get_class(), 'registerCustomFilter'), 9999, 1);
    	add_action('pre_get_posts', array(get_class(), 'registerPageCount'), 9999, 1);
    	add_action('pre_get_posts', array(get_class(), 'registerAsHomepage'), 99, 1);
    	add_action('pre_get_posts', array(get_class(), 'checkIfDisabled'), 98, 1);
    	add_filter('the_posts', array(__CLASS__, 'thePostsFilter'));
    	add_action('cma_index_header_after', array(__CLASS__, 'indexHeaderAfter'));
    	add_filter('posts_where', array(get_class(), 'registerCommentsFiltering'), 1, 1);
    	
    	if (!CMA::isLicenseOk()) return;
        add_filter('posts_search', array(get_class(), 'alterSearchQuery'), 99, 2);
        add_filter('template_include', array(get_class(), 'overrideTemplate'));
        add_action('parse_query', array(get_class(), 'processStatusChange'));
        add_filter('wp_nav_menu_items', array(get_class(), 'addMenuItem'), 1, 1);
        /* add_action('pre_get_posts', array(get_class(), 'registerStickyPosts'), 1, 1); */
        

        add_action('wp_set_comment_status', array(get_class(), 'processAnwserStatusChange'), 999, 2);
        add_action('CMA_display_question_form_upload', array(get_class(), 'displayQuestionFormUpload'), 98, 1);
        add_action('CMA_display_answer_form_upload', array(get_class(), 'displayAnswerFormUpload'), 98, 1);
        add_action('CMA_form_tags', array(__CLASS__, 'displayFormTags'));
        add_action('CMA_breadcrumbs', array(get_class(), 'displayBreadcrumbs'), 98, 1);
        add_action('CMA_questions_table_before', array('CMA_Ads', 'display'));
        add_action('CMA_questions_table_after', array('CMA_Ads', 'display'));
        add_action('CMA_thread_before', array('CMA_Ads', 'display'));
        add_action('CMA_thread_answers_before', array('CMA_Ads', 'display'));
        add_action('CMA_thread_answers_after', array('CMA_Ads', 'display'));
        add_action('CMA_thread_answers_form_after', array('CMA_Ads', 'display'));
        add_action('wp_head', array(__CLASS__, 'wp_head'));

        if( is_admin() )
        {
            add_filter('page_row_actions', array(get_class(), 'pageRowActions'), 1, 2);
        }

        if (CMA_Settings::getOption(CMA_Settings::OPTION_SHOW_LOGIN_WIDGET)) {
        	add_action('CMA_login_form', array(get_class(), 'showLoginForm'));
        }

        $sidebarSettings = array(
            'id'          => 'cm-answers-sidebar',
            'name'        => 'CM Answers Sidebar',
            'description' => 'This sidebar is shown on CM Answers pages',
        );
        $sidebarExtra = array_filter(array(
        	'before_widget' => CMA_Thread::getSidebarSettings('before_widget'),
        	'after_widget' => CMA_Thread::getSidebarSettings('after_widget'),
        	'before_title' => CMA_Thread::getSidebarSettings('before_title'),
        	'after_title' => CMA_Thread::getSidebarSettings('after_title'),
        ));
        register_sidebar(array_merge($sidebarSettings, $sidebarExtra));
        
//         add_action('wp_dashboard_setup', function() {
//         	wp_add_dashboard_widget('cma_followed', CMA_Labels::getLocalized('dashboard_followed'), array(__CLASS__, 'dashboardFollowed'));
//         });
		
		add_shortcode('cma-followed', array(__CLASS__, 'shortcodeFollowed'));

        // Function runs only once on script install or update
        self::postsStickyReset();
    }
    


    public static function wp_head() {
    	echo self::_loadView('answer/widget/inline-styles');
    }
    
    
    public static function shortcodeFollowed() {
    	
    	if (!get_current_user_id()) return;
    	
    	if (CMA_Thread::canBeFollower()) {
    		$postsIds = CMA_FollowersEngine::getFollowed(CMA_Thread::FOLLOWERS_USER_META_PREFIX);
    	}
    	if (!empty($postsIds)) {
	    	$posts = get_posts(array(
	    			'include' => $postsIds,
	    			'post_type' => CMA_Thread::POST_TYPE,
	    			'number' => 100,
	    		));
    	} else {
    		$posts = array();
    	}
    	$threads = array();
    	foreach ($posts as $post) {
    		$threads[] = CMA_Thread::getInstance($post->ID);
    	}
    	
    	if (CMA_Category::canBeFollower()) {
    		$termsIds = CMA_FollowersEngine::getFollowed(CMA_Category::FOLLOWERS_USER_META_PREFIX);
    	}
    	if (!empty($termsIds)) {
	    	$terms = get_terms(CMA_Category::TAXONOMY, array(
	    		'include' => $termsIds,
	    		'hide_empty' => 0,
	    	));
    	} else {
    		$terms = array();
    	}
    	
    	$categories = array();
    	foreach ($terms as $term) {
    		$categories[] = CMA_Category::getInstance($term->term_id);
    	}
    	
    	return self::_loadView('answer/widget/followed', compact('threads', 'categories'));
    	
    }
    
    

    public static function pageRowActions($actions, $post)
    {

        if( $post->post_type === CMA_Thread::POST_TYPE )
        {
            $cmaPost = CMA_Thread::getInstance($post->ID);
            if( $cmaPost->isResolved() )
            {
                $actions['unresolved'] = '<a class="" href="' . esc_attr(add_query_arg(array('cma-action' => 'unresolve', 'cma-id' => $post->ID)))
                	. '" title="'. esc_attr(__('Mark as unresolved', 'cm-answers-pro')) . '">' . __('Mark as unresolved', 'cm-answers-pro') . '</a>';
            }
            else
            {
                $actions['resolved'] = '<a class="" href="' . esc_attr(add_query_arg(array('cma-action' => 'resolve', 'cma-id' => $post->ID)))
                	. '" title="' . esc_attr(__('Mark as resolved', 'cm-answers-pro')) . '">' . __('Mark as resolved', 'cm-answers-pro') . '</a>';
            }
        }

        return $actions;
    }

    public static function postsStickyReset()
    {
        global $wpdb;
        if( get_option('cma_sticky_posts_updated', 0) == 0 )
        {
            $wpdb->query("insert into {$wpdb->postmeta} (post_id, meta_key, meta_value)
                          select ID, '_sticky_post', '0' from $wpdb->posts");
            add_option('cma_sticky_posts_updated', 1);
        }
    }

    public static function alterSearchQuery($search, $query)
    {
        if( ($query->query_vars['post_type'] == CMA_Thread::POST_TYPE OR !empty($query->query_vars['cma_category'])) && !$query->is_single && !$query->is_404 && !$query->is_author && isset($query->query['search']) )
        {
            global $wpdb;
            global $wp_version;
            $search_term = $query->query['search'];
            if( !empty($search_term) )
            {
                $search = '';
                $query->is_search = true;
                // added slashes screw with quote grouping when done early, so done later
                $search_term = stripslashes($search_term);
                preg_match_all('/".*?("|$)|((?<=[\r\n\t ",+])|^)[^\r\n\t ",+]+/', $search_term, $matches);

                if( version_compare($wp_version, '3.7', '<') )
                {
                    $terms = array_map('_search_terms_tidy', $matches[0]);
                }
                else
                {
                    $terms = array_map(function($t)
                    {
                        return trim($t, "\"'\n\r ");
                    }, $matches[0]);
                }

                $n = '%';
                $searchand = ' AND ';
                foreach((array) $terms as $term)
                {
                    $term = esc_sql(method_exists($wpdb, 'esc_like') ? $wpdb->esc_like($term) : like_escape($term));
                    $search .= "{$searchand}(($wpdb->posts.post_title LIKE '{$n}{$term}{$n}') OR ($wpdb->posts.post_content LIKE '{$n}{$term}{$n}'))";
                }
                
                add_filter('get_search_query', create_function('$q', 'return "' . $search_term . '";'), 99, 1);
                remove_filter('posts_request', 'relevanssi_prevent_default_request');
                remove_filter('the_posts', 'relevanssi_query');
            }
        }
        return $search;
    }

    public static function addMenuItem($items)
    {
        if( CMA_Settings::getOption(CMA_Settings::OPTION_ADD_ANSWERS_MENU) )
        {
            $link = self::_loadView('answer/meta/menu-item', array(
            	'checkPermissions' => true,
            	'widget' => true,
            	'listUrl' => CMA::permalink(),
            ));
            return $items . $link;
        }
        return $items;
    }

    public static function showLoginForm($options)
    {
        echo self::_loadView('answer/widget/login', $options);
    }

    public static function processStatusChange()
    {
        if( is_admin() && get_query_var('post_type') == CMA_Thread::POST_TYPE && isset($_REQUEST['cma-action']) )
        {
            switch($_REQUEST['cma-action'])
            {
                case 'approve':
                    $id = $_REQUEST['cma-id'];
                    if( is_numeric($id) )
                    {
                        $thread = CMA_Thread::getInstance($id);
                        $thread->approve();
                        $thread->notifyAboutNewQuestion();
                        add_action('admin_notices', create_function('$q', 'echo "<div class=\"updated\"><p>Question: \"' . $thread->getTitle() . '\" has been succesfully approved</p></div>";'));
                    }
                    break;
                case 'trash':
                    $id = $_REQUEST['cma-id'];
                    if( is_numeric($id) )
                    {
                        $thread = CMA_Thread::getInstance($id);
                        $thread->trash();
                        add_action('admin_notices', create_function('$q', 'echo "<div class=\"updated\"><p>Question: \"' . $thread->getTitle() . '\" has been succesfully moved to trash</p></div>";'));
                    }
                    break;
                case 'resolve':
                    $id = $_REQUEST['cma-id'];
                    if( is_numeric($id) )
                    {
                        $thread = CMA_Thread::getInstance($id);
                        $thread->setResolved(true);
                        add_action('admin_notices', create_function('$q', 'echo "<div class=\"updated\"><p>Question: \"' . $thread->getPost()->post_title . '\" has been succesfully marked as resolved</p></div>";'));
                    }
                    break;
                case 'unresolve':
                    $id = $_REQUEST['cma-id'];
                    if( is_numeric($id) )
                    {
                        $thread = CMA_Thread::getInstance($id);
                        $thread->setResolved(false);
                        add_action('admin_notices', create_function('$q', 'echo "<div class=\"updated\"><p>Question: \"' . $thread->getPost()->post_title . '\" has been succesfully marked as unresolved</p></div>";'));
                    }
                    break;
            }
        }
    }

    public static function processAnwserStatusChange($answerId, $status)
    {
        /*
         * Get the comment, author, thread
         */
        $answer = get_comment($answerId);

        /*
         * Comment not found - abort
         */
        if( !$answer )
        {
            return false;
        }

        $thread = CMA_Thread::getInstance($answer->comment_post_ID);
        /*
         * Fix in case $thread isn't found
         */
        if( is_object($thread) )
        {
        	$thread->updateThreadMetadata(array('answerId' => $answerId));
        	
        	if ($status == 'approve') { // Send notifications
	        	if ($answer->comment_type == CMA_Answer::COMMENT_TYPE) {
	        		$thread->notifyAboutNewAnswer($answerId);
	        	}
	        	else if ($comment = CMA_Comment::getById($answer->comment_id)) {
	        		$comment->sendNotifications();
	        	}
        	}
    	}
    }

    public static function registerCustomOrder($query)
    {
    	if (is_admin()) return;
        if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == CMA_Thread::POST_TYPE && (!isset($query->query_vars['widget']) || $query->query_vars['widget'] !== true) && !$query->is_single && !$query->is_404 && !$query->is_author && isset($_GET['sort']) )
        {
            if( !$query->get('widget') && !$query->get('user_questions') )
            {
                $query = CMA_Thread::customOrder($query, $_GET['sort']);
                $query->is_top = true;
            }
        }
        
    	if ($displayOptions = self::restoreWidgetOptions()) {
    		if (!empty($displayOptions['order'])) {
        		$query->set('order', $displayOptions['order']);
    		}
    		if (!empty($displayOptions['sort'])) {
        		$query = CMA_Thread::customOrder($query, $displayOptions['sort']);
                $query->is_top = true;
    		}
        }
        
    }

    public static function registerTagFilter($query)
    {
        if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == CMA_Thread::POST_TYPE
        	&& (!isset($query->query_vars['widget']) || $query->query_vars['widget'] !== true)
        	&& !$query->is_single && !$query->is_404 && !$query->is_author && isset($_GET['cmatag']) )
        {
            if( !$query->get('widget') && !$query->get('user_questions') )
            {
                $query = CMA_Thread::tagFilter($query, $_GET['cmatag']);
            }
        }
        
    }
    
    
    public static function registerCustomFilter(WP_Query $query) {
    	if( !is_admin() AND isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == CMA_Thread::POST_TYPE ) {
    		
    		if ((!isset($query->query_vars['widget']) || $query->query_vars['widget'] !== true) && !$query->is_single && !$query->is_404 && !$query->is_author) {
    			
    			if ($displayOptions = self::restoreWidgetOptions()) {
    				if (!empty($displayOptions['author'])) {
    					$query->set('author', $displayOptions['author']);
    				}
    			}
    			else if (CMA_Thread::showOnlyOwnQuestions()) {
		    		$userId = get_current_user_id();
		    		if (empty($userId)) $userId = 99999999;
		    		$query->set('author', $userId);
    			}
	    		
    		}
    		
    		add_filter('posts_where_request', array(__CLASS__, 'categoryAccessFilter'));
    		
    	}
    	
    }
    
    
    public static function thePostsFilter($posts) {
    	remove_filter('posts_where_request', array(__CLASS__, 'categoryAccessFilter'));
    	return $posts;
    }
    
    
    public static function categoryAccessFilter($val) {
    	
    	// Don't add a subquery if no category restricted (query time optimization):
    	if (!CMA_Category::isAnyCategoryResticted()) return $val;
    	
    	$val .= ' AND (ID IN ('. CMA_Thread::getCategoryAccessFilterSubquery() .')
    					OR ID NOT IN ('. CMA_Thread::getCategorizedThreadIdsSubquery() .'))';
    	return $val;
    	
    }
    

    public static function registerPageCount($query)
    {
    	if (is_admin()) return;
        if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == CMA_Thread::POST_TYPE && (!isset($query->query_vars['widget']) || $query->query_vars['widget'] !== true) && !$query->is_single && !$query->is_404 && !$query->is_author )
        {
            if( !$query->get('widget') && !isset($_GET["pagination"]) && !$query->get('user_questions') )
            {
                $query->set('posts_per_page', CMA_Settings::getOption(CMA_Settings::OPTION_ITEMS_PER_PAGE));
            }
        }
        
        if ($displayOptions = self::restoreWidgetOptions() AND !empty($displayOptions['limit'])) {
        	$query->set('posts_per_page', $displayOptions['limit']);
        }
        
    }
    
    
    
    public static function restoreWidgetOptions() {
    	if (!session_id() AND !headers_sent()) session_start();
    	if (isset($_GET['widgetCacheId']) AND !empty($_SESSION['cma']['widgetCache'][$_GET['widgetCacheId']])) {
    		return $_SESSION['cma']['widgetCache'][$_GET['widgetCacheId']];
    	}
    }
    

    public static function registerAsHomepage($query)
    {
        /* @var $query WP_Query */
        if( $query->is_main_query() && is_home() && CMA_Settings::getOption(CMA_Settings::OPTION_ANSWERS_AS_HOMEPAGE)
        	&& empty($query->query_vars['post_type']) && !$query->is_page )
        {
            $query->set('post_type', CMA_Thread::POST_TYPE);
            $query->is_archive = true;
            $query->is_page = false;
            $query->is_post_type_archive = true;
            $query->is_home = true;
        }
    }

    public static function checkIfDisabled($query)
    {
        /* @var $query WP_Query */
        if( $query->is_main_query() && CMA_Settings::getOption(CMA_Settings::OPTION_ANSWER_PAGE_DISABLED) && !is_single()
        	&& isset($query->query_vars['post_type']) AND $query->query_vars['post_type'] == CMA_Thread::POST_TYPE )
        {
            $query->is_404 = true;
        }
    }
    

    /* public static function registerStickyPosts($query) {
      if ($query->query_vars['post_type'] == CMA_Thread::POST_TYPE && $query->query_vars['widget'] !== true && !$query->is_single && !$query->is_404 && !$query->is_author) {
      $query->set('meta_query',) ;
      $query->set('meta_query' ,array(
      array(
      'key' => '_sticky_post',
      'value' => 1,
      )
      )
      );
      }
      } */

    public static function registerCommentsFiltering($sql, $questionType = null)
    {
    	if (empty($questionType) AND !empty($_GET['question_type'])) {
    		$questionType = $_GET['question_type'];
    	}
        if( !empty($questionType) )
        {
            global $wpdb;
            
            switch ($questionType) {
            	case 'ans':
            		$expr = ' > 0 ';
            		break;
            	case 'unans':
            		$expr = ' = 0 ';
            		break;
            	default:
            		return $sql;
            }
            
            $sql .= $wpdb->prepare(" AND (SELECT COUNT(*) FROM {$wpdb->comments} WHERE comment_type = %s AND comment_post_ID = ID) $expr ", CMA_Answer::COMMENT_TYPE);
            
        }
        return $sql;
    }

    public static function overrideTemplate($template)
    {
        
    	self::beforeTemplateInclude();
    	self::loadScripts();
    	
        if( get_query_var('post_type') == CMA_Thread::POST_TYPE || is_tax(CMA_Category::TAXONOMY) )
        {
            wp_enqueue_style('CMA-css', CMA_URL . '/views/resources/app.css?v='. urlencode(CMA::version()));
            wp_enqueue_style('dashicons');

            $canBeViewed = CMA_Thread::canBeViewed();
            
            if( is_404() || !$canBeViewed )
            {
                if( CMA_Settings::getOption(CMA_Settings::OPTION_ANSWER_PAGE_DISABLED) )
                {
                    $template = self::locateTemplate(array(
                                '404'
                                    ), $template);
                }
                else
                {
                    $template = self::locateTemplate(array('permissions'), $template);
                }
                return $template;
            }
            
            if( self::_isPost() ) self::processQueryVars();

            if( is_single() )
            {
                
                wp_enqueue_script('jquery-toast', CMA_URL . '/views/resources/toast/js/jquery.toastmessage.js', array('jquery'), false, true);
                wp_enqueue_style('jquery-toast-css', CMA_URL . '/views/resources/toast/resources/css/jquery.toastmessage.css', array(), false);
//                 wp_enqueue_script('jquery-dialog', CMA_URL . '/views/resources/dialog/js/jquery-ui-1.10.4.custom.min.js', array('jquery'), false, true);
//                 wp_enqueue_style('jquery-dialog-css', CMA_URL . '/views/resources/dialog/css/ui-lightness/jquery-ui-1.10.4.custom.min.css', array(), false);

                if( CMA_Thread::showSocial() )
                {
                    wp_enqueue_script('cma-twitter', 'https://platform.twitter.com/widgets.js', array(), false, true);
                    wp_enqueue_script('cma-linkedin', 'https://platform.linkedin.com/in.js', array(), false, true);
                }

                if (!empty($_GET[self::PARAM_EDIT_QUESTION_ID])) {
                	self::_processEditQuestionView();
                	$template = self::locateTemplate(array('answer/full-width'), $template);
                }
                else if (!empty($_GET[self::PARAM_EDIT_ANSWER_ID])) {
                	self::_processEditAnswerView();
                	$template = self::locateTemplate(array('answer/full-width'), $template);
                }
                else {
                	if (CMA_AJAX) $template = self::locateTemplate(array('answer/widget/single-question'), $template);
                	else $template = self::locateTemplate(array('answer/single'), $template);
                }

                if( !self::_isPost() )
                {
                    self::_processViews();
                }
            }
            else
            {
            	if (CMA_AJAX) $template = self::locateTemplate(array('answer/widget/questions'), $template);
                else $template = self::locateTemplate(array('answer/index'), $template);
            }
            add_filter('body_class', array(get_class(), 'adjustBodyClass'), 20, 2);
        }
        return $template;
    }


    protected static function _processEditQuestionView() {
    	global $answer, $post, $cmaPageContent, $populatedData;
    	$thread = CMA_Thread::getInstance($post->ID);
    	if ($thread->canEditQuestion()) {
	    	$_SESSION['CMA_populate'] = array(
    			'thread_title' => $post->post_title,
    			'thread_content' => $post->post_content,
    			'thread_tags' => $thread->getTagsString(),
    		);
    	} else {
    		self::addMessage(self::MESSAGE_ERROR, CMA_Labels::getLocalized('Cannot edit this question.'));
    		wp_safe_redirect(CMA::getReferer());
    		exit;
    	}
    	$edit = true;
    	$cmaPageContent = CMA_BaseController::_loadView('answer/widget/question-form', compact('post', 'thread', 'edit', 'populatedData'));
    	
    }

    
    protected static function _processEditAnswerView() {
    	global $answer, $post, $cmaPageContent, $populatedData;
    	$thread = CMA_Thread::getInstance($post->ID);
    	$answerId = $_GET[self::PARAM_EDIT_ANSWER_ID];
    	$answer = CMA_Answer::getById($answerId, get_current_user_id());
    	if ($thread AND $answer AND $answer->canEdit()) {
    		$populatedData['content'] = $answer->getContent();
    	} else {
    		self::addMessage(self::MESSAGE_ERROR, CMA_Labels::getLocalized('Cannot edit this answer.'));
    		wp_safe_redirect(CMA::getReferer());
    		exit;
    	}
    	$edit = true;
    	$cmaPageContent = CMA_BaseController::_loadView('answer/widget/answer-form', compact('post', 'thread', 'edit', 'populatedData'));
    }

    protected static function _processViews()
    {
        global $wp_query;
        $post = $wp_query->post;
        $thread = CMA_Thread::getInstance($post->ID);
//         $thread->addView();
    }

    protected static function _processEditQuestion() {
    	global $wp_query;

    	if (isset($_GET['editQuestionId']) AND is_numeric($_GET['editQuestionId']) AND $thread = CMA_Thread::getInstance($_GET['editQuestionId'])
    		AND $thread->canEditQuestion()) {
    		
	    	try {
	    		
	    		// Update content
	    		$thread->updateQuestionContent(get_current_user_id(), $_POST['thread_title'], $_POST['thread_content']);
	    		
	    		// Update tags
	    		$tags = (isset($_POST['thread_tags']) ? $_POST['thread_tags'] : null);
	    		wp_set_post_tags($thread->getId(), $tags);
	    		
	    		self::addMessage(self::MESSAGE_SUCCESS, CMA::__('Question has been saved.'));
	    		wp_safe_redirect($thread->getPermalink());
	    		exit;
	    		
	    	} catch (Exception $e) {
	    		self::addMessage(self::MESSAGE_ERROR, $e);
	    	}
	    	
    	} else {
    		self::addMessage(self::MESSAGE_ERROR, CMA::__('You cannot edit this question.'));
    	}

    }
    
    

    protected static function _processEditAnswer($answerId) {
    	global $wp_query;

    	$post = $wp_query->post;
    	$thread = CMA_Thread::getInstance($post->ID);
    	$answer = CMA_Answer::getById($answerId, get_current_user_id());
    	if ($thread AND $answer AND $answer->canEdit()) {
	
	    	$error = false;
	    	$messages = array();
	
	    	try {
	    		$answer->updateContent($_POST['content'], get_current_user_id());
	    		self::addMessage(self::MESSAGE_SUCCESS, CMA_Labels::getLocalized('Answer has been updated.'));
		    	wp_safe_redirect(CMA::getReferer());
		    	exit;
	    	} catch (Exception $e) {
	    		self::addMessage(self::MESSAGE_ERROR, $e);
	    		$error = true;
		    }
		    
    	} else {
    		self::addMessage(self::MESSAGE_ERROR, CMA_Labels::getLocalized('Cannot edit this answer.'));
    		$error = true;
    	}

    }

    protected static function _processAddAnswerToThread()
    {
        global $wp_query;
        $post = $wp_query->post;
        $thread = CMA_Thread::getInstance($post->ID);
        $content = (isset($_POST['content']) ? $_POST['content'] : '');
        $author_id = CMA::getPostingUserId();
        $error = false;
        $messages = array();
        try
        {
            $answerId = $thread->addAnswer($content, $author_id,
            	$follow = !empty($_POST['thread_notify']),
            	$resolved = !empty($_POST['thread_resolved']),
            	$private = !empty($_POST['private'])
            );
        }
        catch(Exception $e)
        {
            $messages = @unserialize($e->getMessage());
            if (!is_array($messages)) $messages = array($e->getMessage());
            $error = true;
        }
//         if( CMA_AJAX )
//         {
//             header('Content-type: application/json');
//             echo json_encode(array('success'     => (int) (!$error), 'answer_id'  => $answerId, 'message'     => $messages));
//             exit;
//         } else {
            if( $error )
            {
                foreach((array) $messages as $message)
                {
                    self::addMessage(self::MESSAGE_ERROR, $message);
                }
            }
            else
            {
                $autoApprove = CMA_Settings::getOption(CMA_Settings::OPTION_ANSWER_AUTO_APPROVE) || CMA_Thread::isAuthorAutoApproved(CMA::getPostingUserId());

                if( $autoApprove )
                {
                    $msg = __('Your answer has been succesfully added.', 'cm-answers-pro');
                }
                else
                {
                    $msg = __('Thank you for your answer, it has been held for moderation.', 'cm-answers-pro');
                }
                $msg = apply_filters('cma_answer_post_msg_success', $msg);
                self::addMessage(self::MESSAGE_SUCCESS, $msg);
                if( !empty($_POST['cma-referrer']) ) wp_redirect($_POST['cma-referrer'], 303);
                else wp_redirect($_SERVER['REQUEST_URI'] . ($autoApprove ? '#answer-' . $answerId : ''), 303);
                exit;
            }
//         }
    }

    protected static function _processAddThread()
    {
        global $wp_query;
        
        $post = $wp_query->post;
        $title = $_POST['thread_title'];
        $content = $_POST['thread_content'];
        $tags = (isset($_POST['thread_tags']) ? $_POST['thread_tags'] : null);
        $notify = (isset($_POST['thread_notify']) ? (bool) $_POST['thread_notify'] : false);
        
        if (!empty($_POST['thread_subcategory'])) {
        	$cat = $_POST['thread_subcategory'];
        }
        else if (!empty($_POST['thread_category'])) {
        	$cat = $_POST['thread_category'];
        } else {
        	$cat = null;
        }
        
        $author_id = CMA::getPostingUserId();
        $error = false;
        $messages = array();
        $data = array(
            'title'     => $title,
            'content'   => $content,
            'notify'    => $notify,
            'author_id' => $author_id,
            'category'  => $cat,
            'tags'      => $tags,	
        );
        try
        {
            $thread = CMA_Thread::newThread($data);
            $thread_id = $thread->getId();
        }
        catch(Exception $e)
        {
            $messages = @unserialize($e->getMessage());
            if (!is_array($messages)) $messages = array($e->getMessage());
            $error = true;
        }
        if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest' )
        {
            header('Content-type: application/json');
            echo json_encode(array('success'   => (int) (!$error), 'thread_id' => $thread_id,
                'message'   => $messages));
            exit;
        }
        else
        {
            if( $error )
            {
                foreach((array) $messages as $message)
                {
                    self::addMessage(self::MESSAGE_ERROR, $message);
                }
                self::_populate($_POST);
                if( !empty($_POST['cma-referrer']) ) wp_redirect($_POST['cma-referrer'], 303);
                else wp_redirect(get_post_type_archive_link(CMA_Thread::POST_TYPE), 303);
            }
            else
            {
                $heldForModeration = $thread->wasHeldForModeration();
                if( !$heldForModeration ) {
                    $msg = CMA::__('New question has been succesfully added.');
                } else {
                    $msg = CMA::__('Thank you for your question, it has been held for moderation.');
                }
                self::addMessage(self::MESSAGE_SUCCESS, apply_filters('cma_question_post_msg_success', $msg));
                
                if (!empty($_POST['cma-redirect'])) {
                	if ($_POST['cma-redirect'] == '_thread') {
                		wp_redirect(get_permalink($thread_id), 303);
                	} else {
                		wp_redirect($_POST['cma-redirect'], 303);
                	}
                }
                else if( !empty($_POST['cma-referrer']) ) wp_redirect($_POST['cma-referrer'], 303);
                else wp_redirect(get_post_type_archive_link(CMA_Thread::POST_TYPE), 303);
            }

            exit;
        }
    }
    
    
    
    protected static function _processCountView() {
    	global $wp_query;
    	if (isset($wp_query->post) AND $thread = CMA_Thread::getInstance($wp_query->post->ID)) {
    		$thread->countView();
    	}
    	exit;
    }
    
    
    protected static function _processResolve() {
    	global $wp_query;
    	
    	$error = false;
    	$messages = array();
    	
    	$post = $wp_query->post;
    	$thread = CMA_Thread::getInstance($post->ID);
    	
    	try {
    		$thread->resolve();
    		self::addMessage(self::MESSAGE_SUCCESS, CMA_Labels::getLocalized('Thread has been marked as resolved.'));
    		wp_safe_redirect(CMA::getReferer()); // get_permalink($post->ID)
    		exit;
    	} catch (Exception $e) {
    		self::addMessage(self::MESSAGE_ERROR, $e);
    		$error = true;
    	}
    }
    
    
    protected static function _processFollow() {
    	global $wp_query;
    	
    	$result = array('success' => 0, 'message' => CMA::__('An error occurred.'));

    	// Follow thread
    	if (!empty($wp_query->post) AND $thread = CMA_Thread::getInstance($wp_query->post->ID) AND empty($_POST['categoryId'])) {
    		$followersEngine = $thread->getFollowersEngine();
    		if ($thread->canSubscribe()) {
    			if ($followersEngine->isFollower()) {
    				$followersEngine->removeFollower();
    				$result = array('success' => 1, 'message' => CMA_Labels::getLocalized('unfollow_success'));
    			} else {
    				$followersEngine->addFollower();
    				$result = array('success' => 1, 'message' => CMA_Labels::getLocalized('follow_success'));
    			}
    		}
    	}

    	// Follow category
    	if (CMA_Settings::getOption(CMA_Settings::OPTION_ENABLE_CATEGORY_FOLLOWING)
    			AND !empty($_POST['categoryId']) AND $category = CMA_Category::getInstance($_POST['categoryId'])) {
    		$followersEngine = $category->getFollowersEngine();
    		if (CMA_FollowersEngine::canBeFollower()) {
    			if ($followersEngine->isFollower()) {
    				$followersEngine->removeFollower();
    				$result = array('success' => 1, 'message' => CMA_Labels::getLocalized('unfollow_category_success'));
    			} else {
    				$followersEngine->addFollower();
    				$result = array('success' => 1, 'message' => CMA_Labels::getLocalized('follow_category_success'));
    			}
    		}
    	}
    	
    	header('Content-type: application/json');
    	echo json_encode($result);
    	exit;
    	 
    }
    
    
    
    protected static function _processLoadSubcategories() {
    	$categoryId = self::_getParam('cma-category-id');
    	$result = array();
    	if (!CMA_Settings::getOption(CMA_Settings::OPTION_ALLOW_POST_ONLY_SUBCATEGORIES)) {
    		$result[] = array('id' => 0, 'name' => CMA_Labels::getLocalized('all_subcategories'), 'url' => '');
    	}
    	if (!empty($categoryId)) {
    		$categories = CMA_Category::getSubcategories($categoryId);
    		foreach ($categories as $category_id => $name) {
    			$result[] = array(
    				'id' => $category_id,
    				'name' => $name,
    				'url' => get_term_link($category_id, CMA_Category::TAXONOMY)
    			);
    		}
    	}
    	
    	header('Content-type: application/json');
    	echo json_encode($result);
    	exit;
    	
    }
    

    protected static function _processVote()
    {
    	
        if( is_single() )
        {
            global $wp_query;
            $post = $wp_query->post;
            if( !empty($post) )
            {
            	
            	$response = array(
            		'success' => 0,
            		'message' => __('There was an error while processing your vote', 'cm-answers-pro')
            	);
            	
            	if( !is_user_logged_in() )
            	{
            		$response['success'] = 0;
            		$response['message'] = __('You have to be logged-in.', 'cm-answers-pro');
            	} else {
	            	
	                $thread = CMA_Thread::getInstance($post->ID);
	                $answerId = self::_getParam('cma-answer');
	                $value = self::_getParam('cma-value');
	                if( !empty($answerId) )
	                { // Voting for answer
	                    
	                	if ($answer = CMA_Answer::getById($answerId)) {
		                    if( $answer->isVotingAllowed(CMA::getPostingUserId()) )
		                    {
		                        $response['success'] = 1;
		                        if( $value == 'up' )
		                        {
		                            $response['message'] = $answer->voteUp();
		                        }
		                        else $response['message'] = $answer->voteDown();
		                    } else
		                    {
		                        $response['message'] = CMA::__('You cannot vote.');
		                    }
	                	}
                    
	                } else { // Voting for question
	                	
	                	if( $thread->isVotingAllowed(CMA::getPostingUserId()) )
	                	{
	                		$response['success'] = 1;
	                		if( $value == 'up' )
	                		{
	                			$response['message'] = $thread->voteUp(null);
	                		}
	                		else $response['message'] = $thread->voteDown(null);
	                	} else {
                			$response['message'] = CMA::__('You cannot vote.');
	                	}
	                	
	                }
            	}
            	
            	header('Content-type: application/json');
            	echo json_encode($response);
            	exit;
            	
            }
        }
        
    }
    
    
    protected static function _processUpload() {
    	header('content-type: application/json');
    	require_once(ABSPATH . 'wp-admin/includes/image.php');
    	$attachments = array();
    	if( CMA_Settings::areAttachmentsAllowed() AND !empty($_FILES['cma-file'])) {
    		$maxFileSize = CMA_Settings::getOption(CMA_Settings::OPTION_ATTACHMENTS_MAX_SIZE);
    		foreach ($_FILES['cma-file']['name'] as $i => $name) {
    			$attachment = array('name' => $name);
    			if ($_FILES['cma-file']['size'][$i] > $maxFileSize) {
    				$attachments[] = array_merge($attachment, array('status' => 'ERROR', 'msg' => CMA::__('File is too large.')));
    			}
    			else if (!CMA_Thread::checkAttachmentAllowed($name)) {
    				$msg = CMA::__('Filetype is not allowed. Allowed extensions:');
    				$msg .= ' '. implode(', ', CMA_Settings::getOption(CMA_Settings::OPTION_ATTACHMENTS_FILE_EXTENSIONS));
    				$attachments[] = array_merge($attachment, array('status' => 'ERROR', 'msg' => $msg));
    			} else {
	    			$newName = floor(microtime(true)*1000) . '_' . sanitize_file_name($name);
	    			$target = CMA_Attachment::getUploadPath() . $newName;
	    			if( move_uploaded_file($_FILES['cma-file']['tmp_name'][$i], $target) ) {
	    				$data = array(
	    					'guid'           => $target,
	    					'post_mime_type' => $_FILES['cma-file']['type'][$i],
	    					'post_title'     => $name,
	    					'post_content'   => '',
	    					'post_status'    => 'draft'
	    				);
	    				$attach_id = wp_insert_attachment($data, $target);
	    				$attach_data = wp_generate_attachment_metadata($attach_id, $target);
	    				wp_update_attachment_metadata($attach_id, $attach_data);
	    				$attachments[] = array_merge($attachment, array(
	    					'status' => 'OK',
	    					'id' => $attach_id,
	    				));
	    			} else {
	    				$attachments[] = array_merge($attachment, array('status' => 'ERROR'));
	    			}
    			}
    		}
    	}
    	echo json_encode($attachments);
    	exit;
    }
    
    
    protected static function _processReportSpam() {
    	if( is_single() ) {
    		global $wp_query;
    		$post = $wp_query->post;
    		if( !empty($post) ) {
    			
    			$response = array(
    				'success' => 0,
    				'message' => CMA::__('An error occurred.')
    			);
    			
    			if( CMA_Settings::canReportSpam() ) {
    			
    				$thread = CMA_Thread::getInstance($post->ID);
    				
    				$answerId = self::_getParam('answerId');
    				
    				if ($userId = CMA::getPostingUserId()) {
    					$user = apply_filters('cma_filter_author', get_user_by('id', $userId), array('thread' => $thread));
    					$user = $user->display_name;
    				} else {
    					$user = 'Guest';
    				}
    				
    				
    				if ($answerId AND $answer = CMA_Answer::getById($answerId)) {
    					$answer->markAsSpam(true);
    					$url = $answer->getPermalink();
    					$author = $answer->getAuthorLink(true);
    					$content = CMA_Thread::lightContent($answer->getContent());
    					$datetime = $answer->getDate();
    					$trashLink = get_admin_url(null, sprintf('comment.php?c=%d&action=trashcomment', $answerId));
    					$spamLink = get_admin_url(null, sprintf('comment.php?c=%d&action=spamcomment', $answerId));
    				} else {
    					$thread->markAsSpam(true);
    					$url = get_permalink($post->ID);
    					$author = $thread->getAuthorLink(true);
    					$content = $thread->getLightContent();
    					$datetime = $post->post_date;
    					$trashLink = get_admin_url(null, sprintf('post.php?post=%d&action=trash', $post->ID));
    					$spamLink = '--';
    				}
    				
    				$replace = array(
    					'[blogname]' => get_bloginfo('name'),
    					'[url]' => $url,
    					'[title]' => $thread->getTitle(),
    					'[author]' => $author,
    					'[content]' => $content,
    					'[user]' => $user,
    					'[datetime]' => $datetime,
    					'[trash]' => $trashLink,
    					'[spam]' => $spamLink,
    				);
    				$subject = strtr(CMA_Settings::getOption(CMA_Settings::OPTION_SPAM_REPORTING_EMAIL_SUBJECT), $replace);
    				$template = strtr(CMA_Settings::getOption(CMA_Settings::OPTION_SPAM_REPORTING_EMAIL_TEMPLATE), $replace);
    				
    				$emails = explode(',', CMA_Settings::getOption(CMA_Settings::OPTION_SPAM_REPORTING_EMAIL_ADDR));
    				
    				$headers = array();
		            foreach($emails as $email) {
		            	$email = trim($email);
		            	if (is_email($email)) {
		            		$headers[] = ' Bcc: '. $email;
		            	}
		            }
		            
		            if (!empty($headers)) wp_mail(null, $subject, $template, $headers);
    				
    				$response['success'] = 1;
    				$response['message'] = CMA_Labels::getLocalized('spam_report_sent');
    			
    			}
    			 
    			header('Content-type: application/json');
    			echo json_encode($response);
    			exit;
    			
    		}
    	}
    }
    
    
    protected static function _processUnmarkSpam() {
    	if( is_single() ) {
    		global $wp_query;
    		$post = $wp_query->post;
    		if( !empty($post) ) {
    			
    			$response = array(
    				'success' => 0,
    				'message' => CMA::__('An error occurred.')
    			);
    			
    			$thread = CMA_Thread::getInstance($post->ID);
    			
    			if ($answerId = self::_getParam('answerId') AND $answer = CMA_Answer::getById($answerId) AND $answer->canUnmarkSpam()) {
    				$answer->markAsSpam(0);
    				$response['success'] = 1;
    				$response['message'] = CMA::__('Content has been unmarked as a spam.');
    			}
    			else if ($thread AND $thread->canUnmarkSpam()) {
    				$thread->markAsSpam(0);
    				$response['success'] = 1;
    				$response['message'] = CMA::__('Content has been unmarked as a spam.');
    			}
    			
    			header('Content-type: application/json');
    			echo json_encode($response);
    			exit;
    			
    		}
    	}
    }
    
    
    
    protected static function _processMarkBestAnswer()
    {
    	 
    	if( is_single() )
    	{
    		global $wp_query;
    		$post = $wp_query->post;
    		if( !empty($post) )
    		{
    			
    			$response = array(
    				'success' => 0,
    				'message' => CMA::__('An error occurred.')
    			);
    			 
    			if( !is_user_logged_in() )
    			{
    				$response['message'] = CMA::__('You have to be logged-in.');
    			} else {
    
    				$thread = CMA_Thread::getInstance($post->ID);
    				$answerId = self::_getParam('cma-answer-id');
    				if( $thread->canMarkBestAnswer() ) {
    					if ($answer = CMA_Answer::getById($answerId)) {
    						if ($thread->getBestAnswerId() == $answerId) {
    							$thread->unmarkBestAnswer();
    							$response['message'] = CMA::__('The best answer has been unmarked.');
    							$response['marked'] = 0;
    						} else {
		    					$thread->setBestAnswer($answerId);
		    					$response['message'] = CMA::__('The best answer has been marked.');
		    					$response['marked'] = 1;
    						}
    						$response['success'] = 1;
    					} else {
    						$response['message'] = CMA::__('Answer not found.');
    					}
    				}
    				
    			}
    			
    			header('Content-type: application/json');
    			echo json_encode($response);
    			exit;
    			
    		}
    	}
    
    }
    
    
    protected static function _processFavorite()
    {
    
    	if( is_single() )
    	{
    		global $wp_query;
    		$post = $wp_query->post;
    		if( !empty($post) )
    		{
    			 
    			$response = array(
    					'success' => 0,
    					'message' => CMA::__('An error occurred.')
    			);
    
    			if( !is_user_logged_in() )
    			{
    				$response['message'] = CMA::__('You have to be logged-in.');
    			} else {
    				
    				$thread = CMA_Thread::getInstance($post->ID);
    				
    				if( $thread->canMarkFavorite() ) {
    					$response['number'] = count($thread->getUsersFavorite());
    					$initialState = $thread->isFavorite();
    					$thread->setFavorite(!$initialState);
    					$response['success'] = 1;
    					if (!$initialState) {
    						$response['message'] = CMA::__('Marked as favorite.');
    						$response['title'] = CMA::__('Unmark as favorite');
    						$response['number']++;
    					} else {
    						$response['message'] = CMA::__('Unmarked as favorite.');
    						$response['title'] = CMA::__('Mark as favorite');
    						$response['number']--;
    					}
    					$response['favorite'] = !$initialState;
    				}
    
    			}
    			 
    			header('Content-type: application/json');
    			echo json_encode($response);
    			exit;
    			 
    		}
    	}
    
    }
    
    
    public static function beforeTemplateInclude() {
//     	$action = self::_getParam('cma-action');
//     	switch($action) {
// 	    	case 'follow':
// 	    		self::_processFollow();
// 	    		break;
//     	}
    }
    

    public static function processQueryVars()
    {
    	
        $action = self::_getParam('cma-action');
        if( !empty($action) )
        {
            switch($action)
            {
                case 'add':
                    if( is_single() ) self::_processAddAnswerToThread();
                    else self::_processAddThread();
                    break;
                case 'vote':
                    self::_processVote();
                    break;
                case 'upload':
                    self::_processUpload();
                    break;
                case 'report-spam':
                    self::_processReportSpam();
                    break;
                case 'follow':
                    self::_processFollow();
                    break;
                case 'unmark-spam':
                    self::_processUnmarkSpam();
                    break;
                case 'mark-best-answer':
                 	self::_processMarkBestAnswer();
                  	break;
                case 'tags-autocomplete':
                	self::_processTagsAutocomplete();
                	break;
                case 'favorite':
                	self::_processFavorite();
                	break;
                case 'load-subcategories':
                    self::_processLoadSubcategories();
                    break;
                case 'display-private-question-form':
                    self::displayPrivateQuestionForm();
                    break;
                case 'private-question-send':
                    self::_processPrivateQuestionSend();
                    break;
                case 'widget':
                	self::_displayWidget();
                	break;
                case 'resolve':
                	self::_processResolve();
                	break;
                case 'count-view':
                	self::_processCountView();
                	break;
                case 'edit':
                	if (!empty($_GET[self::PARAM_EDIT_ANSWER_ID])) {
                		self::_processEditAnswer($_GET[self::PARAM_EDIT_ANSWER_ID]);
                	} else {
                   		self::_processEditQuestion();
                	}
                   	break;
            }
        }
    }
    
    
    protected static function _displayWidget() {
    	if ($widgetOptions = self::restoreWidgetOptions()) {
    		echo CMA_Shortcodes::general_shortcode($widgetOptions, true);
    		exit;
    	}
    }
    

    public static function adjustBodyClass($wp_classes, $extra_classes)
    {
        foreach($wp_classes as $key => $value)
        {
            if( $value == 'singular' ) unset($wp_classes[$key]);
        }

        if( in_array('cma_thread', $wp_classes) && (!CMA_Thread::isSidebarEnabled() || !is_active_sidebar('cm-answers-sidebar') ) )
        {
//            $extra_classes[] = 'full-width';
        }
        return array_merge($wp_classes, (array) $extra_classes);
    }

    public static function registerAdminColumns($columns)
    {
        $columns['author'] = 'Author';
        $columns['views'] = 'Views';
        $columns['status'] = 'Status';
        $columns['comments'] = 'Answers';
        return $columns;
    }

    public static function adminColumnDisplay($columnName, $id)
    {
        $thread = CMA_Thread::getInstance($id);
        if( !$thread ) return;
        switch($columnName)
        {
            case 'author':
                echo $thread->getAuthor()->display_name;
                break;
            case 'views':
                echo $thread->getViews();
                break;
            case 'status':
                echo $thread->getStatus();
                if( strtolower($thread->getStatus()) == strtolower(__('pending', 'cm-answers-pro')) )
                {
                    ?>
                    <a href="<?php
                    echo esc_attr(add_query_arg(array(
						'cma-action' => 'approve',
                        'cma-id'     => $id)));
                    ?>">(Approve)</a>
                    <?php
                }
                break;
        }
    }
    
    

    public static function addAdminSettings($params = array())
    {
    	
        $params['DisclaimerContent'] = CMA_Thread::getDisclaimerContent();
        $params['DisclaimerContentAccept'] = CMA_Thread::getDisclaimerContentAccept();
        $params['DisclaimerContentReject'] = CMA_Thread::getDisclaimerContentReject();
        $params['DisclaimerApproved'] = CMA_Thread::isDisclaimerApproved();
        $params['sidebarBeforeWidget'] = CMA_Thread::getSidebarSettings('before_widget');
        $params['sidebarAfterWidget'] = CMA_Thread::getSidebarSettings('after_widget');
        $params['sidebarBeforeTitle'] = CMA_Thread::getSidebarSettings('before_title');
        $params['sidebarAfterTitle'] = CMA_Thread::getSidebarSettings('after_title');
        $params['showUserStats'] = CMA_Thread::getShowUserStats();
        $params['showGravatars'] = CMA_Thread::showGravatars();
        $params['showSocial'] = CMA_Thread::showSocial();
        $params['answerSortingBy'] = CMA_Answer::getOrderBy();
        $params['answerSortingDesc'] = CMA_Thread::isAnswerSortingDesc();
        
        $params['sidebarEnable'] = CMA_Thread::isSidebarEnabled();
        $params['sidebarContributorEnable'] = CMA_Thread::isSidebarContributorEnabled();
        $params['sidebarMaxWidth'] = CMA_Thread::getSidebarMaxWidth();

        $params['referralEnable'] = CMA_Thread::isReferralEnabled();
        $params['affiliateCode'] = CMA_Thread::getAffiliateCode();

        $params['customCSS'] = CMA_Thread::getCustomCss();

        $params['codeSnippetColor'] = CMA_Thread::getCodeSnippetColor();
        $params['spamFilter'] = CMA_Thread::getSpamFilter();

        return $params;
    }
    


    public static function showPagination($arguments = array(), $base = null)
    {
        global $wp_query;
        
        if (empty($base)) $base = get_post_type_archive_link(CMA_Thread::POST_TYPE);
        if (strpos($base, '?') !== false) {
			$base = str_replace('?', 'page/%#%/?', $base);
		} else {
			$base .= 'page/%#%/';
		}

        $params = array(
            'maxNumPages' => isset($arguments['maxNumPages']) ? $arguments['maxNumPages'] : $wp_query->max_num_pages,
            'paged'       => isset($arguments['paged']) ? $arguments['paged'] : $wp_query->query_vars['paged'],
            'add_args'    => array_filter($arguments),
			'base' => $base,
        );

        $pagination = CMA_BaseController::_loadView('answer/widget/pagination', $params);
        return $pagination;
    }
    
    
    public static function displayQuestionFormUpload() {
		if (CMA_Thread::areQuestionAttachmentsAllowed()) {
			echo self::_loadView('answer/meta/form-upload');
		}
	}
	
	
	public static function displayAnswerFormUpload() {
		if (CMA_Answer::areAnswerAttachmentsAllowed()) {
			echo self::_loadView('answer/meta/form-upload');
		}
	}
    
    
    public static function writeCategoriesTableBody(&$categories, $parentCategoryId = 0, $depth = 0) {
		if (!empty($categories[$parentCategoryId]) AND is_array($categories[$parentCategoryId])) {
			foreach ($categories[$parentCategoryId] as $category) {
				$categoryId = $category->term_id;
				echo self::_loadView('answer/meta/categories-row', compact('category', 'depth'));
				if (!empty($categories[$categoryId])) {
					self::writeCategoriesTableBody($categories, $categoryId, $depth+1);
				}
			}
		}
	}
	
	
	public static function indexHeaderAfter() {
		$content = CMA_Settings::getOption(CMA_Settings::OPTION_INDEX_HEADER_AFTER_TEXT);
		if (strlen($content)) {
			echo self::_loadView('answer/meta/index_header_after', compact('content'));
		}
	}
	
	
	public static function displayFormTags($tags) {
		if (is_array($tags)) $tags = implode(',', $tags);
		echo self::_loadView('answer/meta/form-tags', compact('tags'));
	}
	
	
	protected static function _processTagsAutocomplete() {
		$result = array();
		$search = trim(self::_getParam('cma-tag'));
		if (strlen($search)) {
			$tags = get_tags(array(
				'orderby' => 'count',
				'order' => 'DESC',
				'number' => 10,
				'search' => $search,
			));
		}
		header('content-type: application/json');
		echo json_encode($result);
		exit;
	}
	
	
	
	public static function displayPrivateQuestionForm() {
		if ($userId = self::_getParam('user') AND $user = get_userdata($userId)) {
			echo self::_loadView('answer/meta/private-question-form', compact('user'));
		}
		exit;
	}
	
	
	protected static function _processPrivateQuestionSend() {
		header('content-type: application/json');
		try {
			if (!CMA_Settings::getOption(CMA_Settings::OPTION_PRIVATE_QUESTIONS_ENABLED)) {
				throw new Exception(serialize(array('global' => CMA::__('Private questions are disabled.'))));
			}
			if (CMA_PrivateQuestion::send(get_current_user_id(), self::_getParam('user'), self::_getParam('title'), self::_getParam('question'))) {
				echo json_encode(array('success' => 1, 'msg' => CMA_Labels::getLocalized('private_question_sent_success')));
			} else {
				throw new Exception(serialize(array('email' => CMA::__('Cannot send email. Please try again.'))));
			}
		} catch (Exception $e) {
			echo json_encode(array('success' => 0, 'msg' => CMA::__('An error occured.'), 'errors' => unserialize($e->getMessage())));
		}
		exit;
	}
	
	
	public static function displayBreadcrumbs($threadId = null, $basedOnSettings = true) {

		if ($basedOnSettings AND !CMA_Settings::getOption(CMA_Settings::OPTION_BREADCRUMBS_ENABLED)) {
			return;
		}

		global $queriedObject, $post;
		$indexLink = sprintf('<a href="%s">%s</a>',
			esc_attr(get_post_type_archive_link(CMA_Thread::POST_TYPE)),
			esc_html(CMA_Settings::getOption(CMA_Settings::OPTION_QUESTIONS_TITLE))
		);
		$categoryLink = null;
		$paretnCategoryLink = null;
		$threadLink = null;
		if (empty($threadId) AND is_single() AND !empty($post) AND $post->post_type = CMA_Thread::POST_TYPE) {
			$threadId = $post->ID;
		}
		
		if (!empty($threadId) AND $thread = CMA_Thread::getInstance($threadId)) {
			if ($category = $thread->getCategory()) {
				$categoryLink = $category->getLink();
				if ($parentCategory = $category->getParentInstance()) {
					$parentCategoryLink = $parentCategory->getLink();
				}
			}
			$threadLink = sprintf('<a href="%s">%s</a>',
				esc_attr(!empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $thread->getPermalink()),
				esc_html($thread->getTitle())
			);
		} else {
			if (isset($queriedObject->term_id) AND $category = get_term($queriedObject->term_id, CMA_Category::TAXONOMY)) {
				$categoryLink = sprintf('<a href="%s">%s</a>',
					esc_attr(get_term_link($category->term_id, CMA_Category::TAXONOMY)),
					esc_html($category->name)
				);
				if (!empty($category->parent) AND $parent = get_term($category->parent, CMA_Category::TAXONOMY)) {
					$parentCategoryLink = sprintf('<a href="%s">%s</a>',
						esc_attr(get_term_link($parent->term_id, CMA_Category::TAXONOMY)),
						esc_html($parent->name)
					);
				}
			}
		}
		if (!empty($categoryLink) OR !empty($threadLink)) {
			if (empty($threadLink)) $categoryLink = '<span>'. strip_tags($categoryLink) .'</span>';
			else $threadLink = '<span>'. strip_tags($threadLink) .'</span>';
			echo self::_loadView('answer/nav/breadcrumbs', compact('indexLink', 'categoryLink', 'parentCategoryLink', 'threadLink'));
		}
	}
	
	
	public static function answersTitle() {
		global $wp_query;
		if ($authorSlug = self::_getParam('author')) {
			if ($user = get_user_by('slug', $authorSlug)) {
				return esc_html($user->display_name) . ' - '. CMA_Labels::getLocalized('Answers');
			}
		}
	}
	
	
	public static function answersAction() {
		
		$limit = intval(self::_getParam('limit'));
		if (empty($limit)) $limit = 5;
		$currentPage = max(1, intval(self::_getParam('page')));
		$totalPages = 1;
		$answers = array();
		$authorSlug = '';
		$ajax = false;
		
		if ($authorSlug = self::_getParam('author')) {
			if ($user = get_user_by('slug', $authorSlug)) {
				$authorSlug = $user->user_nicename;
				$answers = CMA_Answer::getByUser($user->ID, $approved = true, $limit, $currentPage, $onlyVisible = true);
				$totalPages = ceil(CMA_Answer::countForUser($user->ID, $approved = true, $limit, $currentPage, $onlyVisible = true)/$limit);
			}
		}
		return array(
			'content' => self::_loadView('answer/widget/answers-list', compact('answers', 'currentPage', 'totalPages', 'authorSlug', 'limit', 'ajax'))
		);
	}
    

}
