��    w      �      �      �     �     �     �     �     �          %     =     J     V     d     s     �     �     �     �     �     �     �     �     �     	  
   	     	     8	     N	     Z	     a	     j	  5   z	     �	     �	     �	     �	     �	  &   
     ,
     F
     `
     i
     
     �
     �
     �
     �
     �
     �
                    -     >     O     c  
   r  
   }  (   �     �     �     �     �     �               %     E  	   N     X     p     y     �     �     �     �     �     �     �     �          
  ;      =   \  %   �      �  $   �        -   '     U     k     �     �     �     �     �     �     �     �     �  '     )   7  6   a  4   �  :   �  8         A     b  '   |     �     �     �     �     �     �     �       
     
        *  |  0     �     �     �  !     #   #     G     e          �     �     �     �     �     �     �  	                  -  	   @  	   J     T  	   d     n     �     �     �  	   �     �  5   �  %        2     H     M     ^  &   q     �     �     �     �     �     �               '  "   G     j     �     �     �     �     �     �     �            )   (     R     `     t     �  +   �     �  
   �     �     	            
   -     8     D     I     V     h     m     �  %   �     �     �     �  9     7   <  %   t      �     �  #   �  /   �     -     M     b     k     }     �     �  
   �     �     �     �  *      )   +  6   U  4   �  :   �  8   �  ,   5     b  +   |     �  
   �     �  
   �     �     �          $     7  
   M     X   1 day ago %d days ago 1 hour ago %d hours ago 1 minute ago %d minutes ago 1 month ago %d months ago 1 second ago %d seconds ago 1 week ago %d weeks ago 1 year ago %d years ago Add Question Add comment All Questions All categories All questions All subcategories Allow markup An error occurred. Answer Answer not found. Answered on %s Answered questions Answers Ask a Question Asked on %s Attachment Back to Full Questions List Back to Previous Page Best answer Cancel Category Choose category Choose the filter options and click the submit button Content cannot be empty Contributor Profile Edit Edit your Answer Edit your Question Enter more details about your question Enter your basic question Enter your question title Favorite Favorite for %d users Follow Follow category Followed categories Followed threads Followed threads and categories Go back to previous page Highest Rating Hottest Lowest Rating Mark as best answer Mark as favorite Mark as resolved Marked as favorite. Marked as spam Most views Most votes New question has been succesfully added. Newest No questions found. Notify me of follow Oldest Please log in to post questions Post your Answer Posted by %s Private question has been sent. Question Questions Questions from category RESOLVED Report spam Save Save comment Search Questions... Send Send private question Send private question to %s Social Login via Social Profiles Status Thank you for voting! Thank you for your answer, it has been held for moderation. Thank you for your question, it has been held for moderation. Thank you! Spam report has been sent. The best answer has been marked. The file you uploaded is not allowed The file you uploaded is too big There was an error while processing your vote Title cannot be empty Unanswered questions Unfollow Unfollow category Unmark as favorite Unmark best answer Unmarked as favorite. Views Votes Wrap your code using Write your Answer here You have already voted for this comment You have already voted for this question. You have been added to the followers of this category. You have been added to the followers of this thread. You have been removed from the followers of this category. You have been removed from the followers of this thread. You have to be logged-in to vote You have to be logged-in. Your answer has been succesfully added. answer answers approved asked %s by %s optional pending posted %s in reply to %s some time ago updated %s by %s view views vote votes votes Project-Id-Version: CM Answers PRO v1.8.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-06-06 08:09+0100
PO-Revision-Date: 2013-08-17 00:25+0100
Last-Translator: wajrou Tomas Goldir <wajrou@gmail.com>
Language-Team: 
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 včera před %d dny před hodinou před %d hodinami před minutou před %d minutami před měsícem před %d měsíci před vteřinou před %d vteřinami před týdnem před %d týdny před rokem před %d roky Přidat otázku Add comment Všechny otázky Všechny kategorie All questions All subcategories Povolit značky An error occurred. Odpověď Answer not found. Odpověděl %s Answered questions Odpovědi Zeptat se Zeptal se na %s Příloha Back to Full Questions List Zpět na předchozí stránku Best answer Cancel Kategorie Vybrat kategorii Choose the filter options and click the submit button Tělo zprávy nemůže být prázdné Profil přispěvatele Edit Edit your Answer Edit your Question Enter more details about your question Enter your basic question Enter your question title Favorite Favorite for %d users Follow Follow category Followed categories Followed threads Followed threads and categories Jít zpět na předchozí stránku Nejvyšší hodnocení Nejaktuálnější Lowest Rating Mark as best answer Mark as favorite Označit jako vyřešené Marked as favorite. Marked as spam Nejvíce zobrazení Nejvíce hlasů Nová otázka byla úspěšně přidána. Nejnovější No questions found. Upozorni mě při reakci Nejstarší Prosím přihlašte se k odeslání otázky Odešlete svou odpověď Odeslal %s Private question has been sent. Otázka Otázky Otázky z kategorie VYŘEŠENO Report spam Save Save comment Hledat otázky... Send Send private question Send private question to %s Přihlášení přes sociální síť Profily ze sociálních sítí Stav Děkujeme za Váš hlas! Děkujeme za Vaši odpověď, je uložena ke schválení. Děkujeme za Vaši otázku, je uložena ke schválení. Thank you! Spam report has been sent. The best answer has been marked. Nahraný soubor není povolen Nahraný soubor je příliš velký Došlo k chybě při zpracování Vašeho hlasu Titulek nemůže být prázdný Unanswered questions Unfollow Unfollow category Unmark as favorite Unmark best answer Unmarked as favorite. Zobrazení Hlasy Zalamovat kód použitím Sem napište svou odpověď Již jste hlasoval(a) pro tento komentář You have already voted for this question. You have been added to the followers of this category. You have been added to the followers of this thread. You have been removed from the followers of this category. You have been removed from the followers of this thread. Pro hlasování musíte být přihlášen(a) You have to be logged-in. Vaše odpověď byla úspěšně přidána. odpověď odpovědi schváleno asked %s by %s volitelné čekající odeslal %s jako odpověď na %s před nějakým časem %s aktualizoval %s zobrazení zobrazení hlas hlasy hlasy 