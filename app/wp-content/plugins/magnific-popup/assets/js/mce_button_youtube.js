(function() {
    tinymce.PluginManager.add('mch_youtube_button', function( editor, url ) {
        editor.addButton( 'mch_youtube_button', {
            title: 'Insert Youtube Video',
            icon: 'icon mch_youtube_button',

            onclick: function() {
                editor.windowManager.open( {
                    title: 'Insert YouTube Video',
                    body: [{   type: 'textbox',
                        size: 40,
                        name: 'link',
                        label: 'Youtube Video Link'
                    }],
                    onsubmit: function( e ) {
                        editor.insertContent( '[video]' + e.data.link + '[/video]');
                    }
                });
            }

        });
    });
})();