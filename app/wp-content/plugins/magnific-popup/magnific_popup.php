<?php

/*
Plugin Name: Magnific Popup
Description: Magnific Popup with TinyMCE Video Buttons.
Version: 1.0
Author: Web4Pro
*/

/**
 * The function append magnific popup js files
 */

function mch_magnific_popup_js(){

    wp_register_script('mct_popup', plugins_url('/assets/js/jquery.magnific-popup.js', __FILE__) , array('jquery'), '1.0.0'); // not Custom scripts:)
    wp_enqueue_script('mct_popup'); // Enqueue it!

    wp_register_script('mct_popup_init', plugins_url('/assets/js/magnific-popup-init.js', __FILE__) , array('jquery'), '1.0.0'); // not Custom scripts:)
    wp_enqueue_script('mct_popup_init'); // Enqueue it!

}

/**
 * The function append magnific popup css files
 */

function mct_magnific_popup_css(){

    wp_register_style('mct_css_popup', plugins_url('/assets/css/magnific-popup.css', __FILE__), array(), '1.0', 'all');
    wp_enqueue_style('mct_css_popup'); // Enqueue it!

}

/**
 * The function include css file with styles of buttons
 */

function mch_tinyMCEButtonsCSS() {
    wp_enqueue_style('tinyMCECustomButtons', plugins_url('/assets/css/style.css', __FILE__));
}

/**
 * The function init custom tinyMCE buttons
 */

function mch_initTinyMCEButtons() {

    add_filter( 'mce_buttons', 'mch_pushTinyMCEButtons');
    add_filter( 'mce_external_plugins', 'mch_TinyMCEIncludeJSHandlers');

}

/**
 * The function include .js file with button features in your plugin
 * @param $plugin_array
 * @return mixed
 */

function mch_TinyMCEIncludeJSHandlers( $plugin_array ) {

    $plugin_array['mch_youtube_button'] = plugin_dir_url(__FILE__) . 'assets/js/mce_button_youtube.js';
    $plugin_array['mch_vimeo_button'] = plugin_dir_url(__FILE__) .  'assets/js/mce_button_vimeo.js';
    $plugin_array['mch_image_button'] = plugin_dir_url(__FILE__) .  'assets/js/mce_button_image.js';

    return $plugin_array;
}

/**
 * @param $buttons
 * @return mixed
 */

function mch_pushTinyMCEButtons( $buttons ) {
    array_push( $buttons, 'mch_youtube_button' , 'mch_vimeo_button', 'mch_image_button');
    return $buttons;
}

add_action( 'init', 'mch_initTinyMCEButtons');
add_action('wp_print_scripts', 'mch_magnific_popup_js'); // Add Conditional Page Scripts
add_action('wp_enqueue_scripts', 'mct_magnific_popup_css'); // Add Theme Stylesheet
add_action( 'wp_enqueue_scripts', 'mch_tinyMCEButtonsCSS' );