(function ($, root, undefined) {

    $(document).ready(function() {

        $('.mch-like-comment').live('click', function(){
            var $like_type       = $(this).attr('data-comment-like-type');
            var $comment_id      = $(this).attr('data-comment-id');

            if(!$like_type || !$comment_id) return false;

            jQuery.ajax({
                type : 'post',
                url : Ajax.ajaxurl,
                data : {
                    action : 'mch-comment-like',
                    'like_type' : $like_type,
                    'comment_id' : $comment_id
                },
                success : function( response ) {
                    response = JSON.parse(response);
                    console.log(response);

                    if(response['error'] == 0){
                        if(response['likes'] > 0){
                            $('.mch-comment-' + $comment_id).find('.comment-likes-count').addClass('mch-counter-green').html('+' + response['likes']);
                        }
                        else if(response['likes'] < 0){
                            $('.mch-comment-' + $comment_id).find('.comment-likes-count').addClass('mch-counter-red').html(response['likes']);
                        }
                        else{
                            $('.mch-comment-' + $comment_id).find('.comment-likes-count').html(response['likes']);
                        }
                    }
                    else{
                        alert(response['message']);
                    }

                    $('.mch-comment-' + $comment_id).find('.mch-like-comment').hide();

                }
            });

            return false;

        })

        $('.mch-comment-image').magnificPopup({
            type: 'image',
            fixedContentPos: false
            // other options
        });

    });

})(jQuery, this);