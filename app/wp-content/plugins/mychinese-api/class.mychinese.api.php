<?php
if(class_exists('WP_REST_Controller')) {
  class MyChineseApi extends WP_REST_Controller {

    /**
     * Register the routes for the objects of the controller.
     */
    public function register_routes() {

      $version = '2';
      $namespace = 'mychinese-api/v' . $version;
      $base = 'sessions';

      register_rest_route($namespace, '/' . $base, array(
        array(
          'methods' => WP_REST_Server::CREATABLE,
          'callback' => array($this, 'sessions'),
          'args' => $this->get_endpoint_args_for_item_schema(TRUE),
        )
      ));

      register_rest_route($namespace, '/lessons/next', array(
        array(
          'methods' => WP_REST_Server::READABLE,
          'callback' => array($this, 'next_lesson'),
          'args' => $this->get_endpoint_args_for_item_schema(TRUE),
        )
      ));

      register_rest_route($namespace, "/lessons/(?P<id>\d+)", array(
        array(
          'methods' => WP_REST_Server::READABLE,
          'callback' => array($this, 'get_lesson'),
          'args' => $this->get_endpoint_args_for_item_schema(TRUE),
        )
      ));

        register_rest_route($namespace, '/devices/register', array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'register_device'),
                'args' => $this->get_endpoint_args_for_item_schema(TRUE),
            )
        ));

        register_rest_route($namespace, '/devices/unregister', array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'unregister_device'),
                'args' => $this->get_endpoint_args_for_item_schema(TRUE),
            )
        ));

        register_rest_route($namespace, '/settings', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_settings'),
                'args' => $this->get_endpoint_args_for_item_schema(TRUE),
            )
        ));

        register_rest_route($namespace, '/surveys/current', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_current_survey'),
                'args' => $this->get_endpoint_args_for_item_schema(TRUE),
            )
        ));

        register_rest_route($namespace, "/surveys/(?P<id>\d+)", array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'update_survey_lesson'),
                'args' => $this->get_endpoint_args_for_item_schema(TRUE),
            )
        ));
    }

	/**
	 * Callback for route 'sessions'.
	 * Send and check user email.
	 *
	 * @param $request
	 * @return WP_REST_Response
	 */
    public function sessions($request) {
      $user = '';
      $data = array();

      $session = $request->get_param('session');

      if(empty($session['email'])){
        return new WP_REST_Response(array(
          'error' => 'Email not found',
        ), 404);
      }

      $user = $this->user_validate($session['email']);

      if( is_a($user, 'WP_REST_Response') && $user->is_error() ) {
	      return new WP_REST_Response(array(
		      'error' => 'Email not found',
	      ), 404);
      }

      $data = array(
        'id'         => $user->ID,
        'email'      => $user->user_email,
        'first_name' => get_user_meta($user->ID, 'first_name', TRUE),
        'last_name'  => get_user_meta($user->ID, 'last_name', TRUE),
        'token'      => $user->user_email
      );

      session_logs($user->ID, 'login');

      return new WP_REST_Response($data, 200);
    }

	/**
	 * Callback for route 'lessons/next'.
	 * Get information about next user lesson
	 *
	 * @param $request
	 * @return WP_REST_Response
	 */
    public function next_lesson($request) {
      $user = '';
      $data = array();
      $token = $request->get_param( 'token' );

      $user = $this->user_validate($token);

      if( is_a($user, 'WP_REST_Response') && $user->is_error() ) {
	      return new WP_REST_Response(array(
	          'error' => 'Token is not correct',
		  ), 404);
	  }
      global $wpdb;
      global $blog_id;
      if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
      }

	  //Clear W3 Total cache, without it return old result.
	  if (function_exists('w3tc_dbcache_flush')) { w3tc_dbcache_flush(); }

      $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM ' .$wpdb->prefix  . 'next_week_lesson WHERE user_id=%d', $user->ID), ARRAY_A);

      //Show error message in not found users next lesson
      if(empty($results[0])){
        return new WP_REST_Response(array(), 404);
      }

      //Formating response data
      $data = array(
        'id'      => $results[0]['pathway'],
        'pathway' => $results[0]['pathway'],
        'module'  => $results[0]['module'],
        'lesson'  => $results[0]['lesson']
      );

      restore_current_blog();

      return new WP_REST_Response($data, 200);
    }

	/**
	 * Callback for route 'lessons/(?P<id>\d+)'.
	 * Get lesson info by lesson id.
	 *
	 * @param $request
	 * @return WP_REST_Response
	 */
    public function get_lesson($request) {
      global $wpdb;
      global $blog_id;

      $data = array();
      $teacher = $this->user_validate($request['token']);

      //Return error if user not found
	  if( is_a($teacher, 'WP_REST_Response') && $teacher->is_error() ) {
		    return new WP_REST_Response(array(
			    'error' => 'Token is not correct',
          ), 404);
      }

      if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
      }
      $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM ' .$wpdb->prefix  . 'lesson_survey WHERE lesson_id=%d', $request['id']), ARRAY_A);

      if(empty($results)){
        return new WP_Error(
          'broke',
          __('Lessons not found.'),
          array(
            'status' => 403,
          )
        );
      }

      //Formating response data
      $data = array(
        'id' => $results[0]['id'],
        'classroom_teacher_id' => $results[0]['classroom_teacher_id'],
        'chineese_teacher_id' => $results[0]['chinese_teacher_id'],
        'pathway' => $results[0]['pathway'],
        'module' => $results[0]['module'],
        'lesson' => $results[0]['lesson']
      );

      restore_current_blog();

      return new WP_REST_Response($data, 200);
    }

      /**
       * Callback for route '/devices/register'.
       * Insert info about classroom teachers device.
       *
       * @param $request
       * @return WP_REST_Response
       */
      public function register_device($request) {
          $teacher = $this->user_validate($request['token']);
          $device = $request['device'];

          $token = $device['token'];
          $platform = $device['platform'];

	      if( is_a($teacher, 'WP_REST_Response') && $teacher->is_error() ) {
		      return new WP_REST_Response(array(
			      'error' => 'Token is not correct',
		      ), 404);
	      }

          if(empty($token) || empty($platform)) {
              return new WP_REST_Response(array(
                  'message' => 'Bad request'
              ), 400);
          }

          $last_id = insert_device_info(array(
              'classroom_teacher_id' => $teacher->ID,
              'platform' => $platform,
              'token' => $token
          ));

	      if(empty($last_id)){
		      return new WP_REST_Response(array(
			      'message' => 'Already Exist'
		      ), 201);
	      }

          $data = array(
              'id' => $last_id,
              'platform' => $platform,
              'token' => $token
          );

          session_logs($teacher->ID, 'login');

          return new WP_REST_Response($data, 200);
      }

      /**
       * Callback for route '/devices/unregister'.
       * Delete info about classroom teachers device.
       *
       * @param $request
       * @return WP_REST_Response
       */
      public function unregister_device($request) {
        $user_token = $request->get_param( 'token' );
        $device = $request->get_param( 'device' );

        $teacher = $this->user_validate($user_token);

        $token = $device['token'];
        $platform = $device['platform'];


        if( is_a($teacher, 'WP_REST_Response') && $teacher->is_error() ) {
		    return new WP_REST_Response(array(
		       'error' => 'Token is not correct',
		    ), 404);
	    }

        if(empty($token) || empty($platform)) {
            return new WP_REST_Response(array(
                'message' => 'Bad request',
                'data' => array('status' => 400)
            ), 400);
        }


        $delete_device = delete_device_info($teacher->ID, $token, $platform);
        if($delete_device){
            session_logs($teacher->ID, 'logout');
        }

        return new WP_REST_Response(array(), 204);
      }

      /**
       * Callback for route '/settings'.
       * Return settings from emails page in admin area.
       *
       * @param $request
       * @return WP_REST_Response
       */
	  public function get_settings($request) {
		  global $blog_id;

		  if($blog_id != HUB_BLOG_ID){
			  switch_to_blog(HUB_BLOG_ID);
		  }
		  $data = array();

		  $of1_name = get_option('of1_name', '');
		  $of1_f1_lable = get_option('of1_f1_lable', '');
		  $of1_f1_value = get_option('of1_f1_value', '');
		  $of1_f2_lable = get_option('of1_f2_lable', '');
		  $of1_f2_value = get_option('of1_f2_value', '');

		  $data['offices'][] = array(
			  'name' => $of1_name,
			  'contacts' => array(
				  array(
					  'label' => $of1_f1_lable,
					  'value' => $of1_f1_value
				  ),
				  array(
					  'label' => $of1_f2_lable,
					  'value' => $of1_f2_value
				  )
			  )
		  );

		  $of2_name = get_option('of2_name', '');
		  $of2_f1_lable = get_option('of2_f1_lable', '');
		  $of2_f1_value = get_option('of2_f1_value', '');
		  $of2_f2_lable = get_option('of2_f2_lable', '');
		  $of2_f2_value = get_option('of2_f2_value', '');

		  $data['offices'][] = array(
			  'name' => $of2_name,
			  'contacts' => array(
				  array(
					  'label' => $of2_f1_lable,
					  'value' => $of2_f1_value
				  ),
				  array(
					  'label' => $of2_f2_lable,
					  'value' => $of2_f2_value
				  )
			  )
		  );

		  $data['surveys_items'] = array(
			  'Technology Issues',
			  'Chinese Teacher Issues',
			  'Lesson Plan Issues'
		  );

		  return new WP_REST_Response($data, 200);
	  }

      /**
       * Callback for route "/surveys/(?P<id>\d+)".
       * Update current survey lesson: set rating and comment.
       *
       * @param $request
       * @return WP_REST_Response
       */
      public function update_survey_lesson($request) {
          $user = $this->user_validate($request->get_param('token'));
          $survey = $request->get_param('survey');
          $survey_id = $request['id'];
          $rating = $survey['rating'];
          $comment = $survey['comment'];

	      if( is_a($user, 'WP_REST_Response') && $user->is_error() ) {
		      return new WP_REST_Response(array(
			      'error' => 'Token is not correct',
		      ), 404);
	      }

          if(empty($survey_id) || empty($rating)) {
              return new WP_REST_Response(array(
                  'message' => 'Bad request',
                  'data' => array('status' => 400)
              ), 400);
          }

          // get lesson info
          global $wpdb;
          global $blog_id;

          if($blog_id != HUB_BLOG_ID){
              switch_to_blog(HUB_BLOG_ID);
          }

          $survey_table = $wpdb->prefix . 'lesson_survey';
          $lesson_info = $wpdb->get_row(
              $wpdb->prepare(
                  "SELECT pathway, module, lesson, chinese_teacher_id, survey_date FROM $survey_table WHERE survey_id = %d AND classroom_teacher_id = %d AND is_rated = %d AND is_cancelled=%d",
                  $survey_id,
                  $user->ID,
                  0,
                  0
              ),
              ARRAY_A
          );
          if(!is_null($lesson_info)) {
              // Update survey rating
              if (update_survey($survey_id, $rating, $comment, $user->ID)) {
                  // send emails if rating less than 2
                  if ($rating < 2) {
                      if (($emails = get_option('mch_emails')) !== false) {
                          $emails = json_decode($emails, true);
                          send_emails($emails, array(
                              'user' => $user,
                              'teacher_id' => $lesson_info['chinese_teacher_id'],
                              'pathway' => $lesson_info['pathway'],
                              'module' => $lesson_info['module'],
                              'lesson' => $lesson_info['lesson'],
                              'survey_date' => $lesson_info['survey_date'],
                              'rating' => $rating,
                              'comment' => $comment
                          ));
                      }
                  }

                  unset($lesson_info['chinese_teacher_id'], $lesson_info['survey_date']);
                  $data = array(
                      'id' => $survey_id,
                      'rating' => $rating,
                      'comment' => $comment,
                      'lesson_info' => $lesson_info
                  );
                  return new WP_REST_Response($data, 200);
              }
          }

          return new WP_REST_Response(null, 204);
      }

      /**
       * Callback for route '/surveys/current'.
       * Get information about current survey lesson.
       *
       * @param $request
       * @return WP_REST_Response
       */
      public function get_current_survey($request) {
          $user = $this->user_validate($request['token']);

          //Return error if user not found
	      if( is_a($user, 'WP_REST_Response') && $user->is_error() ) {
		      return new WP_REST_Response(array(
			      'error' => 'Token is not correct',
		      ), 404);
	      }

          // get lesson info
          global $wpdb;
          global $blog_id;

          if($blog_id != HUB_BLOG_ID){
              switch_to_blog(HUB_BLOG_ID);
          }

          $survey_table = $wpdb->prefix . 'lesson_survey';
          $lesson_info = $wpdb->get_row(
              $wpdb->prepare(
                  "SELECT * FROM $survey_table WHERE classroom_teacher_id = %d AND is_rated = %d AND is_cancelled=%d LIMIT 1",
                  $user->ID,
                  0,
                  0
              )
          );
          if(!is_null($lesson_info)) {
              $data = array(
                  'id' => $lesson_info->survey_id,
                  'rating' => $lesson_info->rating,
                  'comment' => $lesson_info->comment,
                  'lesson_info' => array(
                      'pathway' => $lesson_info->pathway,
                      'module' => $lesson_info->module,
                      'lesson' => $lesson_info->lesson
                  )
              );
              return new WP_REST_Response($data, 200);
          }

          return new WP_REST_Response(array(), 204);
      }


	  /**
	   * Check user by email.
	   *
	   * @param $email
	   * @return bool|WP_Error|WP_User
	   */
      public function user_validate($email) {
        if (is_email($email)) {
          $user = get_user_by('email', $email);
        }

        if (empty($user)) {
	        return new WP_REST_Response(array(
		        'error' => 'Email not found',
	        ), 404);
        }

        return $user;
      }

    }
}