<?php $cluster_id = get_post_meta( $post->ID, 'mss_cluster_id', true ); ?>
<select name="mss_cluster" id="mss_cluster_selectbox">
    <option value="">-- <?php _e( 'Select Cluster' ); ?> --</option>
    <?php if ( $clusters = mss_get_all_clusters() ) : ?>
        <?php foreach ( $clusters as $cluster ) : ?>
            <option value="<?php echo $cluster->term_id; ?>" <?php selected( $cluster->term_id, $cluster_id ); ?>><?php _e( $cluster->name ); ?></option>
        <?php endforeach; ?>
    <?php endif; ?>
    <option value="all" <?php selected( $cluster_id, 'all' ); ?>><?php _e( 'All' ); ?></option>
</select>