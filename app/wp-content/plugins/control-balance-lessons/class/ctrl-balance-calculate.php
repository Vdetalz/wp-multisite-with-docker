<?php

class CTRL_Balance_Calculate {

    function set($log){
        if(empty($log) || !is_object($log)){
            return;
        }

        global $wpdb;

        $wpdb->insert(
            $wpdb->prefix . USER_LOGS_TABLE,
            array(
                'user_id'          =>  $log->user_id,
                'date'             =>  $log->date,
                'action'           =>  $log->action,
                'change_balance'   =>  $log->change_balance,
                'balance'          =>  $log->balance,
                'delivery_method'  =>  $log->delivery_method,
            )
        );

    }

    function get($user_id){
        if(empty($user_id)){
            return;
        }

        global $wpdb;
        $user_logs = array();

        $user_logs = $wpdb->get_results(
            "SELECT date, action, change_balance, balance, delivery_method
             FROM " . $wpdb->prefix . "users_logs_info
             WHERE user_id = $user_id"
        );


        return $user_logs;
    }

    function output($user_id){
        $user_logs = array();
        $users_array = array();

        if(!empty($user_id) && is_int($user_id)){
            $user_logs = self::get($user_id);
        }

        $users_array = self::ctrl_get_users();


        ctrl_locate_template('ctrl_balance_history.php', true, array('user_logs' => $user_logs, 'users_array' => $users_array ));
    }


    function ctrl_get_users(){
        global $wpdb;
        $blog_id = get_current_blog_id();
        $users_array = array();

        $args = array(
            'orderby' => 'login',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => $wpdb->get_blog_prefix( $blog_id ) . 'capabilities',
                    'value' => 'subscriber',
                    'compare' => 'like'
                ),
                array(
                    'key' => $wpdb->get_blog_prefix( $blog_id ) . 'capabilities',
                    'value' => 'contributor',
                    'compare' => 'like'
                ),
            )
        );

        $users = get_users($args);

        if(empty($users)){
            return;
        }

        foreach($users as $user){
            $users_array[$user->ID] = $user->data->display_name;
        }

        return $users_array;
    }

}