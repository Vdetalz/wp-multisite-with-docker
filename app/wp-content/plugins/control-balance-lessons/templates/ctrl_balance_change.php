<div class="wrap">
    <h2><?php _e('Change balance'); ?></h2>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>"class="form-horizontal" role="form">
        <div class="form-group">
            <div class="input-group">
                <label class="sr-only" for="ctrl_balance_user"><?php _e('Select user'); ?></label>
                <div class="input-group-addon"><?php _e('Select user: '); ?></div>
                <select class="form-control ctrl-balance-select-user" name="ctrl_balance_user" style="width:18%">
                    <option value=""><?php _e('-Select user-'); ?></option>
                    <?php foreach($subscribers as $id => $subscriber): ?>
                        <option value="<?php print $id; ?>"><?php print $subscriber['display_name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <label class="sr-only" for="ctrl-hors-balance"><?php _e('Hours'); ?></label>
                <div class="input-group-addon"><?php _e('Hours'); ?></div>
                <input type="text" name="ctrl_balnce_value" class="form-control" id="ctrl-hors-balance" placeholder="<?php _e('Enter how do you want to change the balance.'); ?>" style="width:20%">
            </div>
            <p class="text-info"><?php _e('To decrease the balance , enter, for example -5.  To increase the balance    5.'); ?></p>
        </div>
        <div class="form-group">
            <div class="input-group">
                <label class="sr-only" for="ctrl-hors-balance"><?php _e('Expiry date'); ?></label>
                <div class="input-group-addon"><?php _e('Expiry date'); ?></div>
                <input type="text" name="ctrl_balnce_expiry_date" class="form-control" id="ctrl-balance-date" placeholder="<?php _e('Enter how do you want to change the balance.'); ?>" style="width:18%">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <input type="submit" class="btn btn-default" value="<?php _e('Save'); ?>" name="ctrl_balance_submit">
            </div>
        </div>
    </form>
</div>

