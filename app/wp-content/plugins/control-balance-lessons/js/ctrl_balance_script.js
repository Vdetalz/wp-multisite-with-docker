jQuery(document).ready(function($){
    $(function() {
        $('#ctrl-balance-date').datepicker()
    });

    var table = $('#users-info').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": ctrlbalance.ajaxurl + '?action=users_info_table'
    } );

    if($('#users-info').length){
        $('body').css('background', 'transparent');
        $('#users-info').addClass('widefat');
    }

    if($('#info-table').length){
        $('#info-table').dataTable( {
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "searching": false,
            "lengthChange": false,
            "ajax": ctrlbalance.ajaxurl + '?action=balance_info_table'
        } );
    }

    /* AJAX: add user's corses list */
    if($('.ctrl-balance-select-user').length){
        $('.ctrl-balance-select-user').on('change', function(){
            var userId = $(this).val();
            var parentBlock = $(this).closest('.form-group');

            if($('.ctrl-course-list').length){
                $('.ctrl-course-list').remove();
            }

            parentBlock.prepend('<div class="loading" id="em-loading"></div>');
            $.ajax({
                type:'POST',
                url:ctrlbalance.ajaxurl,
                data: {
                    action: 'user_course_list',
                    user_id: userId
                },
                dataType: "html",
                success: function(data){
                    parentBlock.after(data);
                    parentBlock.find('.loading').remove();
                }

            });
        });
    }

});