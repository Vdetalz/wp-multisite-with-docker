<div class="wrap">
	<?php include( dirname( __FILE__ ) . '/tabs.php' ); ?>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<td align="center" scope="row"><img src="<?php echo plugins_url('wp-sms-pro/assets/images/logo-250.png'); ?>"></td>
			</tr>
			
			<tr valign="top">
				<td scope="row" align="center"><h2><?php echo sprintf(__('WP SMS Pro V%s', 'wp-sms'), WP_SMS_VERSION_PRO); ?></h2></td>
			</tr>
			
			<tr valign="top">
				<td align="center" scope="row"><?php _e('Premium version from Wordpress SMS Plugin with feature and new web services.', 'wp-sms'); ?></td>
			</tr>
			
			<tr valign="top">
				<td align="center" scope="row"><hr></td>
			</tr>
			
			<tr valign="top">
				<td colspan="2" scope="row"><h2><?php _e('Support', 'wp-sms'); ?></h2></td>
			</tr>
			
			<tr valign="top">
				<td colspan="2" scope="row">
					<ul>
						<li><?php _e('do you have problem with plugin?', 'wp-sms'); ?></li>
						<li><?php _e('are you a translator?', 'wp-sms'); ?></li>
						<li><?php _e('You want add your web service to this plugin?', 'wp-sms'); ?></li>
					</ul>
					<p><?php echo sprintf(__('Please contact with email %s', 'wp-sms'), '<code>mst404@gmail.com</code>'); ?></p>
				</td>
			</tr>
		</tbody>
</table>
</div>