<?php
	class infobip extends WP_SMS {
		private $wsdl_link = null;
		public $tariff = "http://infobip.com/";
		public $unitrial = true;
		public $unit;
		public $flash = "disable";
		public $isflash = false;

		public function __construct() {
			parent::__construct();
			require_once 'includes/oneapi/client.php';
		}

		public function SendSMS() {
			$smsClient = new SmsClient($this->username, $this->password);
			
			$smsClient->login();
			
			$smsMessage = new SMSRequest();
			$smsMessage->senderAddress = $this->from;
			$smsMessage->address = $this->to;
			$smsMessage->message = $this->msg;
			
			$result = $smsClient->sendSMS($smsMessage);
			
			if($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}

		public function GetCredit() {
			$customerProfileClient = new CustomerProfileClient($this->username, $this->password);
			$customerProfileClient->login();

			$accountBalance = $customerProfileClient->getAccountBalance();

			if(!$accountBalance->isSuccess()) {
				return false;
			}

			return $accountBalance->balance;
		}
	}
?>