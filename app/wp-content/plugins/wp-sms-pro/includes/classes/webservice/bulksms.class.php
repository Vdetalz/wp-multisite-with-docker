<?php
	class bulksms extends WP_SMS {
		private $wsdl_link = "http://bulksms.vsms.net/eapi/submission/send_sms/2/2.0";
		public $tariff = "http://www.bulksms.com/int/";
		public $unitrial = true;
		public $unit;
		public $flash = "disable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
		}
		
		public function SendSMS() {
			$params = array(
				'http' => array(
					'method'	=> 'POST',
					'content'	=> "username={$this->username}&password={$this->password}&message=".urlencode($this->msg)."&msisdn=".implode(',', $this->to),
					'header'	=> 'Content-type:application/x-www-form-urlencoded'
				)
			);
			
			$ctx = stream_context_create($params);
			
			$response = @file_get_contents($this->wsdl_link, false, $ctx);
			
			if($response) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $response;
		}
		
		public function GetCredit() {
			$result = @file_get_contents("http://bulksms.vsms.net/eapi/user/get_credits/1/1.1?username={$this->username}&password={$this->password}");
			
			if($result == 0) {
				return substr($result, 2);
			}
		}
	}
?>