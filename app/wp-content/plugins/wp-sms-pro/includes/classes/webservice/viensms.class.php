<?php
	class viensms extends WP_SMS {
		private $wsdl_link = "http://viensms.com/api.php";
		public $tariff = "http://viensms.com/";
		public $unitrial = false;
		public $unit;
		public $flash = "enable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
		}
		
		public function SendSMS() {
			
			$msg = urlencode($this->msg);
			
			foreach($this->to as $number) {
				$result = file_get_contents("{$this->wsdl_link}?command=push&username={$this->username}&api_key={$this->password}&to={$number}&from={$this->from}&message={$msg}");
			}
			
			if ($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}
		
		public function GetCredit() {
			$result = file_get_contents("{$this->wsdl_link}?command=balance&username={$this->username}&api_key={$this->password}");
			return $result;
		}
	}
?>