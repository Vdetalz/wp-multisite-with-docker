<?php
	class twilio extends WP_SMS {
		private $wsdl_link = null;
		private $client = null;
		public $tariff = "http://twilio.com/";
		public $unitrial = true;
		public $unit;
		public $flash = "enable";
		public $isflash = false;

		public function __construct() {
			parent::__construct();
			require "includes/twilio/Twilio.php";
		}

		public function SendSMS() {
			$client = new Services_Twilio($this->username, $this->password);
			
			foreach ($this->to as $number) {
				if( strstr($number, "+") == false ) {
					$number = '+' . $number;
				}
				$result = $client->account->messages->sendMessage($this->from, $number, $this->msg);
			}
			
			if($result->sid) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			
			return $result;
		}

		public function GetCredit() {
			if(!$this->username && !$this->password) return;
			
			$client = new Services_Twilio($this->username, $this->password);
			$result = $client->accounts->get($this->username);
			
			if($result->status == 'active')
				return true;
		}
	}
?>