<?php
/*
Plugin Name: Mychinese Tutor Configuration
Description:  Mychinese Tutor Configuration plugin
Version: 1.0
Author: Web4Pro
Author URI: 
*/

/**
 * On plugin activation - creates new DB for the teachers complete lessons
 */

register_activation_hook(__FILE__, 'mct_teachers_user_list_table_install');
register_activation_hook(__FILE__, 'mct_canceled_lessons_statistic_table_install');

function mct_canceled_lessons_statistic_table_install () {
    global $wpdb;

    $cencel_lessons_table_name = $wpdb->prefix . 'canceled_lessons_statistic';
    if($wpdb->get_var("show tables like '$cencel_lessons_table_name'") != $cencel_lessons_table_name) {

        $sql = "CREATE TABLE IF NOT EXISTS " . $cencel_lessons_table_name . " (
                  id bigint(11) NOT NULL AUTO_INCREMENT,
                  event_id bigint(11) DEFAULT '0' NOT NULL,
				  student_id bigint(11) DEFAULT '0' NOT NULL,
				  canceled_data TIMESTAMP NULL,
				  UNIQUE KEY id (id)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

function mct_teachers_user_list_table_install () {
    global $wpdb;

    $teachers_user_table_name = $wpdb->prefix . 'teachers_user_list';
    if($wpdb->get_var("show tables like '$teachers_user_table_name'") != $teachers_user_table_name) {

        $sql = "CREATE TABLE IF NOT EXISTS " . $teachers_user_table_name . " (
                  id bigint(11) NOT NULL AUTO_INCREMENT,
                  event_id bigint(11) DEFAULT '0' NOT NULL,
				  author_id bigint(11) DEFAULT '0' NOT NULL,
				  subscriber_id bigint(11) DEFAULT '0' NOT NULL,
				  course_group_id bigint(11) DEFAULT '0' NOT NULL,
				  level_id bigint(11) DEFAULT '0' NOT NULL,
				  module_id bigint(11) DEFAULT '0' NOT NULL,
				  lesson_id bigint(11) DEFAULT '0' NOT NULL,
				  comment text NOT NULL,
				  cancel_comment text NOT NULL,
				  complete_date VARCHAR(255) NULL,
				  UNIQUE KEY id (id)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

add_action( 'init', 'remove_edd_ajax_action' );

function remove_edd_ajax_action() {
	remove_action( 'wp_ajax_edd_add_to_cart', 'edd_ajax_add_to_cart' );
	remove_action( 'wp_ajax_nopriv_edd_add_to_cart', 'edd_ajax_add_to_cart' );
}

add_action( 'edd_add_to_cart', 'mct_clear_cart', 1 );

function mct_clear_cart(){
    edd_empty_cart();
}

add_action( 'wp_ajax_edd_add_to_cart', 'mct_edd_add_to_cart' );
add_action( 'wp_ajax_nopriv_edd_add_to_cart', 'mct_edd_add_to_cart' );

function mct_edd_add_to_cart() {

	if ( isset( $_POST['download_id'] ) ) {
		edd_empty_cart();

		$to_add = array();

		if ( isset( $_POST['price_ids'] ) && is_array( $_POST['price_ids'] ) ) {
			foreach ( $_POST['price_ids'] as $price ) {
				$to_add[] = array( 'price_id' => $price );
			}
		}

		$items = '';

		foreach ( $to_add as $options ) {

			if ( $_POST['download_id'] == $options['price_id'] )
				$options = array();

			$key  = edd_add_to_cart( $_POST['download_id'], $options );
			$item = array(
				'id'      => $_POST['download_id'],
				'options' => $options
			);
			$item = apply_filters( 'edd_ajax_pre_cart_item_template', $item );
			$items .= html_entity_decode( edd_get_cart_item_template( $key, $item, true ), ENT_COMPAT, 'UTF-8' );
		}

		$return = array(
			'subtotal'  => html_entity_decode( edd_currency_filter( edd_format_amount( edd_get_cart_subtotal() ) ), ENT_COMPAT, 'UTF-8' ),
			'total'     => html_entity_decode( edd_currency_filter( edd_format_amount( edd_get_cart_total() ) ), ENT_COMPAT, 'UTF-8' ),
			'cart_item' => $items
		);

		echo json_encode( $return );
	} else {
		$return = array();
		echo json_encode( $return );
	}

	edd_die();
}


/**
 * Adds a box to EDD product page metabox hours value
 */

function mct_edd_download_table_head( $post_id ) {
	?>
	<th style="width: 15px"><?php _e( 'Hours', 'edd' ); ?></th>
<?php
}

add_action( 'edd_download_price_table_head', 'mct_edd_download_table_head' );

function edd_hours_metabox( $download_id, $key, $args ) {
	$hours = get_post_meta( $download_id, 'edd_variable_prices_' . $key, true );
	?>
	<td>
		<?php echo EDD()->html->text( array(
			'name'        => 'edd_variable_prices[' . $key . '][hours]',
			'value'       => esc_attr( $hours ),
			'placeholder' => __( 'Hours', 'edd' ),
			'class'       => 'edd_variable_prices_hours large-text'
		) ); ?>
	</td>
<?php
}

add_action( 'edd_download_price_table_row', 'edd_hours_metabox', 999, 3 );

function edd_hours_metabox_save( $post_id ) {
	$is_autosave    = wp_is_post_autosave( $post_id );
	$is_revision    = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['mct_nonce'] ) && wp_verify_nonce( $_POST['mct_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';

	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}

	if ( isset( $_POST['edd_variable_prices'] ) && is_array( $_POST['edd_variable_prices'] ) ) {
		foreach ( $_POST['edd_variable_prices'] as $key => $value ) {
			update_post_meta( $post_id, 'edd_variable_prices_' . $key, sanitize_text_field( $value['hours'] ) );
		}
	}
}

add_action( 'save_post', 'edd_hours_metabox_save' );


add_action('wp_enqueue_scripts', 'mct_shortcodes_enqueue_scripts');
function mct_shortcodes_enqueue_scripts()
{
	wp_enqueue_script('form_validation', plugins_url('assets/js/form_validation.js', __FILE__), array('jquery'), '', true);
	wp_enqueue_script('magnific_popup_user', plugins_url('assets/js/jquery.magnific-popup.js', __FILE__), array('jquery'), '', true);
	wp_enqueue_script('magnific_popup_init', plugins_url('assets/js/popup_init.js', __FILE__), array('jquery'), '', true);

    /**
     * include lesson_history_save.js script for ajax save lesson history on button click
     */
}

function get_booking_by_id($booking_id)
{
	global $wpdb;

	if (!$booking_id) {
		return false;
	}

	$table_name = $wpdb->prefix . 'em_bookings';

	$sql = $wpdb->get_results(" SELECT * FROM " . $table_name . " WHERE booking_id =" . $booking_id, OBJECT);

	return $sql;
}

add_action('user_register', 'mc_set_default_timezone');
add_action( 'personal_options_update', 'mc_set_default_timezone' );
add_action( 'edit_user_profile_update', 'mc_set_default_timezone' );


function mc_set_default_timezone($user_id){

    $user = new WP_User($user_id);

    if(in_array('author', $user->roles)){
        if($_POST['_user_timezone'] != 'default'){
            update_user_meta($user->ID, '_user_timezone', $_POST['_user_timezone']);
        }
        else{
            update_user_meta($user->ID, '_user_timezone', 'Asia/Hong_Kong');
        }
    }

    //Asia/Hong_Kong

    /*if(!isset($_POST['_user_timezone']) || $_POST['_user_timezone'] == 'default') {

        if(in_array('author', $user->roles)) {
            $timezone = 8;
        }
        else{
            $timezone = 0;
        }

    }
    else {
        $timezone = $_POST['_user_timezone'];
    }

    update_user_meta($user_id, '_user_timezone', $timezone);*/

}


add_action( 'show_user_profile', 'mc_add_user_timezone' );
add_action( 'edit_user_profile', 'mc_add_user_timezone' );
add_action( 'user_new_form', 'mc_add_user_timezone' );

function mc_add_user_timezone( $user ) { ?>

    <h3><?php _e('User Timezone');?></h3>

    <table class="form-table">

        <tr>
            <th><label for="_user_timezone"><?php _e('Select User Timezone');?></label></th>

            <td>
                <select name="_user_timezone">

                    <option value="default">-<?php _e('Select timezone');?>-</option>

                    <?php foreach(dut_get_country_timezones() as $timezone):
                        $dateTimeZoneTaipei = new DateTimeZone($timezone);
                        $dateTimeTaipei = new DateTime("now", $dateTimeZoneTaipei);
                        $timeOffset = $dateTimeZoneTaipei->getOffset($dateTimeTaipei);?>
                        <option value="<?php echo $timezone;?>" <?php echo selected($timezone, get_user_meta($user->ID, '_user_timezone', true));?>>(<?php echo dut_prepare_timezone($timeOffset);?>) <?php echo $timezone;?></option>
                    <?php endforeach;?>

<!--                    --><?php //for($i = -12; $i<=13; $i++):?>
<!--                        <option --><?php //selected($i, get_user_meta($user->ID, '_user_timezone', true));?><!-- value=--><?php //echo $i;?><!-->GMT --><?php //echo $i>0? '+' . $i : $i ;?><!--:00</option>-->
<!--                    --><?php //endfor;?>

                </select>

                <br>
                <span class="description"><?php _e('Only for authors');?></span>
            </td>
        </tr>

    </table>
<?php
}


/* Save info about canceled lessons about 12 hours before lessons */

add_action('mct_cancel_booking', 'mct_cancel_booking_function');

function mct_cancel_booking_function($event){
    global $wpdb;

    $event_start_time = strtotime($event->event_start_date . ' ' . $event->event_start_time);
    $dif_hours = (int)floor(($event_start_time- time())/3600);

    if($dif_hours <= 12){
        $wpdb->insert(
            $wpdb->prefix . 'canceled_lessons_statistic',
            array(
                'event_id'       =>  $event->event_id,
                'student_id'     =>  get_current_user_id(),
                'canceled_data'  =>  date('Y-m-d h:i:s',time())
            )
        );
    }
}

add_filter( 'wp_mail_from', 'mct_custom_wp_mail_from' );

function mct_custom_wp_mail_from( $original_email_address )
{
    return get_option('admin_email');
}


function custom_wp_mail_from_name($name) {
    return __('My Education Group');
}

add_filter('wp_mail_from_name','custom_wp_mail_from_name');

function mct_homepage_badge_settings(){
    add_options_page('Homepage Badges Settings', __('Homepage Badges'), __('Homepage Badges'), 'homepage-badge', 'mct_homepage_settings');
}

add_action('admin_menu', 'mct_homepage_badge_settings');

function mct_homepage_settings(){
    if(isset($_POST['submit']) && !empty($_POST['submit'])){
        if(isset($_POST['hp_badge_price']) && !empty($_POST['hp_badge_price'])) {
            $badge_price = $_POST['hp_badge_price'];
            update_option('hp_badge_price', $badge_price);
        }

        if(isset($_POST['hp_badge_text']) && !empty($_POST['hp_badge_text'])){
            $badge_text = $_POST['hp_badge_text'];
            update_option('hp_badge_text', $badge_text);
        }

    }
    else {
        $badge_price = get_option('hp_badge_price');
        $badge_text = get_option('hp_badge_text');
    }
    ?>
    <h2><?php echo __('Homepage badges settings'); ?></h2>
    <form method="post">
        <p>
            <label for="hp_badge_price"><?php echo __('Price in badge'); ?></label>
            <input type="text" name="hp_badge_price" id="hp_badge_price" value="<?php echo $badge_price; ?>">
        </p>
        <p>
            <label for="hp_badge_text"><?php echo __('Text in badge'); ?></label>
            <input type="text" name="hp_badge_text" id="hp_badge_text" value="<?php echo $badge_text; ?>">
        </p>
        <input type="submit" value="<?php echo __('Save changes'); ?>" class="button button-primary" name="submit" id="submit">
    </form>
<?php
}

function mct_add_badge_metabox(){
    add_meta_box(
        'mch_download_badge_metabox',
        __('Pricing Badge Settings'),
        'mct_download_badge_settings',
        'download'
    );
}

add_action('add_meta_boxes', 'mct_add_badge_metabox');

function mct_download_badge_settings(){
    global $post;

    $badge_price = get_post_meta( $post->ID, 'dwn_badge_price', true );
    $badge_text = get_post_meta( $post->ID, 'dwn_badge_text', true );

    ?>
    <p>
        <label for="dwn_badge_price"><?php echo __('Price in badge'); ?></label>
        <input type="text" name="dwn_badge_price" id="dwn_badge_price" value="<?php echo $badge_price; ?>">
    </p>
    <p>
        <label for="dwn_badge_text"><?php echo __('Text in badge'); ?></label>
        <input type="text" name="dwn_badge_text" id="dwn_badge_text" value="<?php echo $badge_text; ?>">
    </p>
<?php
}

function mct_download_badge_save_post($post_id){
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['mct_nonce']) && wp_verify_nonce($_POST['mct_nonce'], basename(__FILE__))) ? 'true' : 'false';

    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }

    if(isset($_POST['dwn_badge_price']) && !empty($_POST['dwn_badge_price'])){
        update_post_meta($post_id, 'dwn_badge_price', $_POST['dwn_badge_price']);
    }

    if(isset($_POST['dwn_badge_text']) && !empty($_POST['dwn_badge_text'])){
        update_post_meta($post_id, 'dwn_badge_text', $_POST['dwn_badge_text']);
    }
}

add_action('save_post', 'mct_download_badge_save_post');

// activation validate.
add_filter('wp_authenticate_user', 'mct_auth_login', 10, 2);
function mct_auth_login($user, $password) {
    $active = get_user_meta($user->ID, "active", true);
    if ($active === '0') {
        $user = new WP_Error('403', __('Your account is not active'));
    }
    return $user;
}