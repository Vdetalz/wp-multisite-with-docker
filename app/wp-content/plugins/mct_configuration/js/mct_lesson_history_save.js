jQuery(document).ready(function ($) {
	$('.plus-button').live('click', function () {
        var currentBlock = $(this).parents('.user-block');
		var id = currentBlock.attr('id');
        var event_id = $('#' + id + ' .event-id').val();
		var user_id = $('#' + id + ' .user-id').val();
		var teacher_id = $('#' + id + ' .teacher-id').val();
		var course_id = $('#' + id + ' .path-level-select').val();
		var module_id = $('#' + id + ' .module-select').val();
		var lesson_id = $('#' + id + ' .lesson-select').val();
		var comment = $('#' + id + ' .lesson-comment').val();

		$.ajax({
			type   : 'post',
			url    : ajax.ajaxurl,
			data   : {
                    action : 'mct_save_lesson_history',
                    user_id : teacher_id,
                    event_id : event_id,
                    teacher_id : teacher_id,
                    course_id : course_id,
                    module_id : module_id,
                    lesson_id : lesson_id,
                    comment : comment
            },

			success: function (results) {
                currentBlock.empty();
			}
		});
	});
});
