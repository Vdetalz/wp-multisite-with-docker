(function() {
    tinymce.PluginManager.add('mch_vimeo_button', function( editor, url ) {
        editor.addButton( 'mch_vimeo_button', {
            title: 'Insert Vimeo Video',
            icon: 'icon mch_vimeo_button',

            onclick: function() {
                editor.windowManager.open( {
                    title: 'Insert Vimeo Video',
                    body: [{   type: 'textbox',
                        size: 40,
                        name: 'name',
                        label: 'Popup Link Name:'
                    },{   type: 'textbox',
                        size: 40,
                        name: 'link',
                        label: 'Vimeo Video Link'
                    }],
                    onsubmit: function( e ) {
                        editor.insertContent( '<a href="' + e.data.link + '" class="popup-vimeo">' + e.data.name + '</a>');
                    }
                });
            }

        });
    });
})();