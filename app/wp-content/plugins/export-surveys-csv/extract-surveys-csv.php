<?php
/*
Plugin Name: Export Surveys CSV
Description:  Export surveys to CSV file
Version: 1.0
Author: Web4Pro
Author URI:
*/

class Export_Surveys_CSV {
    const PAGE_SLUG = 'export-surveys-csv';
	const TIME_ZONE = '8';

    public $from;
    public $to;
    public $validate;
    public $status = array();

    /**
     * Start up
     */
    public function __construct()
    {
        if(!function_exists('get_user_by')) {
            include(ABSPATH . "wp-includes/pluggable.php");
        }

        add_action('admin_menu', array($this, 'extract_surveys_page'));

        add_action('wp_enqueue_scripts', array($this, 'register_plugin_scripts'));
        add_action('wp_enqueue_scripts', array($this, 'register_plugin_styles'));
    }

    /**
     * Run plugin
     */
    public function run()
    {
        if(filter_input(INPUT_GET, 'page') === self::PAGE_SLUG) {
            $this->register_plugin_scripts();
            $this->register_plugin_styles();
        }

        $surveys = $this->get_surveys();
        if($this->validate && !empty($surveys)) {
            $this->array_to_csv_download($surveys, $this->create_file_name(str_replace('/', '-', $this->from), str_replace('/', '-', $this->to)));
        }
    }

    /**
     * Add extract surveys page
     */
    public function extract_surveys_page()
    {
        // This page will be under "Settings"
        add_options_page(
            __('Export Surveys CSV'),
            __('Export Surveys CSV'),
            'manage_options',
            self::PAGE_SLUG,
            array( $this, 'create_surveys_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_surveys_page()
    {
        ?>
        <div class="wrap">

            <?php if($this->status['success'] === false): ?>
                <div class="update-nag">
                    <?php echo $this->status['message']; ?>
                </div>
            <?php endif; ?>

            <h2><?php _e('Extract Surveys to CSV file') ?></h2>

            <form method="POST">
                <label for="from"><?php _e('From:'); ?></label>
                <input type="text" id="from" name="from" value="<?php echo $this->from; ?>">

                <label for="to"><?php _e('To:'); ?></label>
                <input type="text" id="to" name="to" value="<?php echo $this->to; ?>">

                <?php submit_button( __('Export Surveys'), 'primary', 'submit_export'); ?>
            </form>
        </div>

        <?php
    }

    /**
     * Callback for init plugin scripts
     */
    public function register_plugin_scripts()
    {
        wp_register_script('common-script', plugins_url('/js/common.js', __FILE__));
        wp_enqueue_script('common-script');

        wp_enqueue_script('datepicker', plugins_url('/js/datepicker/jquery-ui.min.js', __FILE__), array('jquery'));
    }

    /**
     * Callback for init plugin styles
     */
    public function register_plugin_styles()
    {
        wp_enqueue_style('datepicker-style', plugins_url('/js/datepicker/jquery-ui.css', __FILE__));
    }

    /**
     * Get surveys from date
     * @return null|array
     */
    private function get_surveys()
    {
        if(filter_input(INPUT_POST, 'submit_export')) {
            $this->from = filter_input(INPUT_POST, 'from');
            $this->to = filter_input(INPUT_POST, 'to');
            if(empty($this->from) && empty($this->to)) {
                $this->validate = false;
                $this->status = array(
                        'success' => false,
                        'message' => __("From and To fields can't be blank.")
                );
                return null;
            }

            global $wpdb;
            $date_from = strtotime($this->from);
            $date_to = strtotime($this->to . " + 1 day");

            if(empty($date_from)) {
                $where = "WHERE survey_date <= %d";
                $value_parameter = $date_to;
            } else if(empty($date_to)) {
                $where = "WHERE survey_date >= %d";
                $value_parameter = $date_from;
            } else {
                $where = "WHERE survey_date BETWEEN %d AND %d";
                $value_parameter = array($date_from, $date_to);
            }

            $table = $wpdb->prefix . 'lesson_survey';
            $surveys = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table $where ORDER BY survey_date", $value_parameter));
            if(empty($surveys)) {
                $this->validate = false;
                $this->status = array(
                    'success' => false,
                    'message' => __("No results.")
                );
                return null;
            }

            $this->validate = true;
            $this->status = array(
                'success' => true,
            );

            $surveys = $this->formation_of_array_for_csv($surveys);
            return $surveys;
        }
    }

    /**
     * Get full info about surveys
     * @param array $array
     * @return array
     */
    private function formation_of_array_for_csv(array $array)
    {
        $final_surveys_array[] = array(
            "Lesson ID", "Lesson Name", "Lesson Date",
            "Pathway Number", "Module Number", "Lesson Number",
            "Chinese Teacher ID", "Chinese Teacher Name", "Classroom Teacher ID",
            "Classroom Teacher Name", "Classroom Teacher Location", "Classroom Teacher School",
            "Classroom Teacher Year Level", "Star Rating", "Low Star Reason", "Technology",
            "Classroom Teacher Quality"
        );

        foreach($array as $survey) {
            $row = array();
            $row[0] = $survey->lesson_id;
            $row[1] = get_the_title($survey->lesson_id);
            $row[2] = date('d/m/y H:i', $this->ex_prepare_date($survey->survey_date));
            $row[3] = $survey->pathway;
            $row[4] = $survey->module;
            $row[5] = $survey->lesson;

	        $row[6] = $survey->chinese_teacher_id;
            $teacher_first_name = get_user_meta($survey->chinese_teacher_id, 'first_name', true);
            $teacher_last_name = get_user_meta($survey->chinese_teacher_id, 'last_name', true);
            $row[7] = $teacher_first_name . ' ' . $teacher_last_name;

	        $row[8] = $survey->classroom_teacher_id;
	        $classroom_teacher = get_user_by('id', $survey->classroom_teacher_id);
	        $classroom_first_name = get_user_meta($classroom_teacher->ID, 'first_name', true);
	        $classroom_last_name = get_user_meta($classroom_teacher->ID, 'last_name', true);
	        $row[9] = $classroom_first_name . ' ' . $classroom_last_name;
            $row[10] = get_user_meta($classroom_teacher->ID, 'location', true);
            $row[11] = get_user_meta($classroom_teacher->ID, 'school', true);
            $row[12] = get_user_meta($classroom_teacher->ID, 'year_level', true);
            $row[13] = $survey->star_rating;
            $row[14] = $survey->comment;
            $row[15] = $survey->technology_rating;
            $row[16] = $survey->ct_rating;
            
            //for cancelled lessons
            if ($survey->is_cancelled) {
                $needless_indexes = array(0, 1, 3, 4, 5, 15, 16);
                foreach ($needless_indexes as $index) {
                    $row[$index] = NULL;
                }
            } 
            
            $final_surveys_array[] = $row;
        }

        return $final_surveys_array;
    }

    /**
     * Start download csv file
     * @param array $array
     * @param string $filename
     * @param string $delimiter
     */
    private function array_to_csv_download(array $array, $filename = "export.csv", $delimiter=",") {
            $f = fopen('php://memory', 'w');
            foreach ($array as $line) {
                fputcsv($f, $line, $delimiter);
            }
            fseek($f, 0);
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);
            exit;
    }

    /**
     * Create current CSV file name
     * @param $from
     * @param $to
     * @return string
     */
    private function create_file_name($from, $to)
    {
        if(empty($from)) {
            $from = 'Beginning';
        }
        if(empty($to)) {
            $to = 'End';
        }

        return "survey_csv_FROM_{$from}_TO_{$to}.csv";
    }

	/**
	 * Prepare date with timezone
	 * @param $datetime
	 * @param $timezone
	 * @return timestamp
	 */
	private function ex_prepare_date($datetime, $timezone = self::TIME_ZONE)
	{
		if(!$datetime) return $datetime;

		return $datetime + ($timezone * 60 * 60);
	}
}

// init page
if(is_admin()) {
    $export = new Export_Surveys_CSV();
    $export->run();
}