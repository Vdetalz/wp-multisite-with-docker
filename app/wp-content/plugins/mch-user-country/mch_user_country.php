<?php
/*
 * Plugin name: My Chinese Hub User Country
 * Author: Web4Pro
 * Description: Sending files in depends on their country
 */

defined('MUC_COUNTRY_META_KEY') || define('MUC_COUNTRY_META_KEY', 'country');

add_action( 'show_user_profile', 'muc_add_profile_country_field', 20 );
add_action( 'edit_user_profile', 'muc_add_profile_country_field', 20 );

/**
 * Render country field
 * @param $user
 */

function muc_add_profile_country_field( $user )
{
    if( !current_user_can( 'edit_pages' ) ) return;
    $user_country = get_user_meta($user->ID, MUC_COUNTRY_META_KEY, true); ?>
    <table class="form-table">
        <tr>
            <th><label for="country"><?php _e('Select Geography'); ?></label></th>
            <td>
                <select name="country" id="country">
                    <option value=""><?php _e('Select Geography'); ?></option>
                    <?php foreach (muc_get_country_array() as $country): ?>
                        <option value="<?php echo $country ?>" <?php selected($country, $user_country); ?>>
                            <?php echo $country ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
<?php
}


/**
 * Save country field
 * @param $user_id
 */

function muc_profile_country_field( $user_id ) {
    if( !current_user_can('edit_pages') ) return;
    update_user_meta($user_id, MUC_COUNTRY_META_KEY, $_POST['country']);
}

add_action( 'personal_options_update', 'muc_profile_country_field' );
add_action( 'edit_user_profile_update', 'muc_profile_country_field' );

/**
 * Get array of all countries around the world.
 * @return array
 */
function muc_get_country_array() {
    $countries = array("Afghanistan", "Alabama", "Alaska", "Albania", "Alberta", "Algeria", "America", "Andorra", "Antigua", "Argentina", "Arizona", "Arkansas", "Armenia", "Australia", "Australia Capital Territory", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bissau", "Bolivia", "Bosnia", "Botswana", "Brazil", "British", "British Columbia", "Brunei", "Bulgaria", "Burkina", "Burma", "Burundi", "California", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Colorado", "Comoros", "Congo", "Connecticut", "Costa Rica", "country debt", "Croatia", "Cuba", "Cyprus", "Czech", "Delaware", "Denmark", "District of Columbia", "Djibouti", "Dominica", "East Timor", "Ecuador", "Egypt", "El Salvador", "Emirate", "England", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "Florida", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Great Britain", "Greece", "Grenada", "Grenadines", "Guatemala", "Guinea", "Guyana", "Haiti", "Hawaii", "Herzegovina", "Honduras", "Hungary", "Iceland", "Idaho", "Illinois", "India", "Indiana", "Indonesia", "Iowa", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jordan", "Kansas", "Kazakhstan", "Kentucky", "Kenya", "Kiribati", "Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Maine", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Manitoba", "Marshall", "Maryland", "Massachusetts", "Mauritania", "Mauritius", "Mexico", "Michigan", "Micronesia", "Minnesota", "Mississippi", "Missouri", "Moldova", "Monaco", "Mongolia", "Montana", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nebraska", "Nepal", "Netherlands", "Nevada", "New Brunswick", "New Hampshire", "New Jersey", "New Mexico", "New South Wales", "New York", "New Zealand", "Newfoundland and Labrador", "Nicaragua", "Niger", "Nigeria", "North Carolina", "North Dakota", "Northern Territory", "Northwest Territories", "Norway", "Nova Scotia", "Nunavut", "Ohio", "Oklahoma", "Oman", "Ontario", "Oregon", "Pakistan", "Palau", "Panama", "Papua", "Paraguay", "Pennsylvania", "Peru", "Philippines", "Poland", "Portugal", "Prince Edward Island", "Qatar", "Quebec", "Queensland", "Rhode Island", "Romania", "Russia", "Rwanda", "Saint Kitts", "Samoa", "San Marino", "Santa Lucia", "Sao Tome", "Saskatchewan", "Saudi Arabia", "Scotland", "Scottish", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon", "Somalia", "South Africa", "South Australia", "South Carolina", "South Dakota", "South Sudan", "Spain", "Sri Lanka", "St Kitts", "St Lucia", "St. Kitts", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Tasmania", "Tennessee", "Texas", "Thailand", "Tobago", "Togo", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Kingdom", "United States", "Uruguay", "USA", "Utah", "Uzbekistan", "Vanuatu", "Vatican", "Venezuela", "Vermont", "Victoria", "Vietnam", "Virginia", "Wales", "Washington", "Welsh", "West Virginia", "Western Australia", "Wisconsin", "Wyoming", "Yemen", "Yukon", "Yugoslavia", "Zambia", "Zimbabwe");
    return $countries;
}
