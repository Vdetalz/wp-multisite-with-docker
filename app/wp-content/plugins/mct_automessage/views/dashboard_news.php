<div class="postbox ">
    <h3 class="hndle"><span><?php _e('Automessage','automessage'); ?></span></h3>
    <div class="inside">
        <p><?php _e('You are running Automessage version ','automessage');?>
            <strong><?php echo $plugin['Version'];?></strong>
        </p>

        <p>
        <?php echo _e('Debug mode is ','automessage');?>
            <strong>
                <?php if($debug):?>
                    <?php _e('Enabled','automessage');?>
                <?php else:?>
                    <?php _e('Disabled','automessage');?>
                <?php endif;?>
            </strong>
        </p>

        <br class="clear">
    </div>
</div>