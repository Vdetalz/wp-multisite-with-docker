<table width="100%" cellpadding="3" cellspacing="3" class="widefat">

    <thead>

        <tr>
            <th scope="col" class="check-column"></th>

            <th scope="col">
                <?php _e('Action','automessage');?>
                </th>

            <th scope="col">
                <?php _e('Time delay','automessage');?>
                </th>

            <th scope="col">
                <?php _e('Subject','automessage');?>
                </th>

            <th scope="col">
                <?php _e('Queued','automessage');?>
                </th>

            </tr>

    </thead>

    <tbody id="the-list">

    <?php if($results):?>

    <?php foreach($results as $result): $hooks = get_post_meta($result->ID, '_automessage_hook', true);?>

            <?php if(count($hooks)):?>

            <?php foreach($hooks as $key => $hook) $hooks[$key] = $this->hooks[$hook];?>

            <?php $hooks = implode(', ', $hooks); ?>

            <tr>
                <th scope="row" class="check-column" >
                    <input type="checkbox" id="schedule_<?php echo $result->ID;?>" name="allschedules[]" value="<?php echo $result->ID;?>" />
                </th>

                <td scope="row">
                    <?php if($result->post_status == 'draft'):?>
                    <?php _e('[Paused] ','automessage');?>
                    <?php endif;?>

                    <?php _e($hooks);?>

                    <div class="row-actions">
                        <a href="?page=<?php echo $page;?>&action=editaction&id=<?php echo $result->ID;?>" title="<?php _e('Edit this message','automessage');?>"><?php _e('Edit','automessage');?></a>
                        <?php if($result->post_status == 'private'):?>
                            <a href="?page=<?php echo $page;?>&action=pauseaction&id=<?php echo $result->ID;?>" title="<?php _e('Pause this message','automessage');?>"><?php _e('Pause','automessage');?></a>
                        <?php else:?>
                            <a href="?page=<?php echo $page;?>&action=unpauseaction&id=<?php echo $result->ID;?>" title="<?php _e('Unpause this message','automessage');?>"><?php _e('Unpause','automessage');?></a>
                        <?php endif;?>
                        <a href="?page=<?php echo $page;?>&action=process<?php echo $type;?>action&id=<?php echo $result->ID;?>" title="<?php _e('Process this message','automessage');?>"><?php _e('Process','automessage');?></a>
                        <a href="?page=<?php echo $page;?>&action=deleteaction&id=<?php echo $result->ID;?>" title="<?php _e('Delete this message','automessage');?>"><?php _e('Delete','automessage');?></a>
                    </div>

                </td>

                <td scope="row" valign="top">
                    <?php _e(get_post_meta($result->ID, '_automessage_period', true));?>
                </td>

                <td scope="row" valign="top">
                    <?php _e(stripslashes($result->post_title));?>
                </td>

                <td scope="row" valign="top">
                    <?php
                    $period = get_post_meta( $result->ID, '_automessage_period', true );
                    $use_once = get_post_meta( $result->ID, '_automessage_use_once', true );
                    $from_registered = get_post_meta( $result->ID, '_automessage_from_registered', true );
                    $students_send = get_post_meta( $result->ID, '_automessage_students_send', true );

                        echo count($this->get_users_by_post_criteria($result->ID, array(
	                        'period' => $period,
	                        'use_once' => $use_once,
	                        'from_registered' => $from_registered,
	                        'action_id' => $post->ID,
	                        'students_send' => $students_send,
                        )));
                    ?>
                </td>

            </tr>

    <?php endif;?>

    <?php endforeach;?>

    <?php else:?>

    <tr>
        <td colspan="5"><?php _e('No actions set for this level.');?></td>
    </tr>

    <?php endif;?>

    </tbody>

</table>