jQuery(document).ready(function($){

    $('.dont-hide, .user-account-info-block input[type=file], .user-account-info-block input[type=button]').show();
    $('.user-manage-subscription').hide();

    $('.user-account-edit').click(function(){
        $(this).closest('.user-account-info-block').find('input').not('.user-account-info-block .submit .button').show();
        $(this).closest('.user-account-info-block').find('span.user-account-data').not('.dont-hide').hide();
        if($(this).prop('type') == 'button'){
            $(this).prop('type', 'submit');
            return false;
        }
        else{
            return true;
        }
    })

    $('.user-show-manage-subscription').click(function(){
        $(this).closest('div').find('.user-manage-subscription').toggle();
        return false;
    })

    $('.user-account-info-block .wp-user-avatar').click(function(){
        $(this).hide();
        $('.wpua-edit').show();
    })

})