jQuery(document).ready(function($){

    //By default hide all bodies (accordion collapsed)
    $('.he-body').hide();

    //On click "i" button we replace header with body element and show it
    $('.he-info-open').on('click', function(){
        $(this).parent().hide();
        $(this).parents().eq(1).find('.he-body').slideDown();
        $(this).parents().eq(1).addClass('extended');
    });

    //In click 'minus' button - collapse body and show header (accordion collapse)
    $('.he-info-close').on('click', function(){
        $(this).parent().slideUp();
        $(this).parents().eq(1).find('.he-header').show();
        $(this).parents().eq(1).removeClass('extended');
    });

    //Enabling pagination
    $('.history-elem-wrap').each(function(i){
        var page_number = Math.ceil((i + 1) / 10);
        $(this).attr('data-page', page_number);
        if($(this).data('page') !== 1){
            $(this).hide();
        }
    });

    //On page will be shows 10 items
    var pages_count = Math.ceil($('.history-elem-wrap').size() / 10);

    //If we have more than one page enable pagination
    if (pages_count > 1) {
        if ($('#jquery-pagination') && $('#jquery-pagination').length > 0) {
            $('#jquery-pagination').twbsPagination({
                totalPages: pages_count,
                visiblePages: 7,
                first: '',
                last: '',
                onPageClick: function (event, page) {
                    $('.history-elem-wrap').each(function () {
                        if ($(this).data('page') == page) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    });

                }
            });
        }
    }
    else{
        $('.pagination').remove();
    }

});