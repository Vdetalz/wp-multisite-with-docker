<?php

/**
 * @return string
 */
function external_pricing_page()
{
    global $blog_id;
    $user_id = get_current_user_id();
    $serialize_site_options = get_site_option('mychinese_auto_subscription', true);
    $site_options = maybe_unserialize($serialize_site_options);
    $common_cart_serialize = get_user_meta($user_id, 'common_cart');
    $common_cart = maybe_unserialize($common_cart_serialize);
    if ($common_cart && !empty($common_cart)) {
        $tutor_cart = $common_cart[0]['tutor_cart'];
        $hub_cart = $common_cart[0]['hub_cart'];
    }
    update_user_meta($user_id, 'common_cart', array('hub_cart' => $hub_cart, 'tutor_cart' => $tutor_cart));
    if (isset($tutor_cart) && !empty($tutor_cart) && isset($common_cart) && !empty($common_cart) && isset($hub_cart) && !empty($hub_cart)) {
        $this_site_home = get_bloginfo('url');
        wp_redirect($this_site_home . '/checkout');
    }

    if ($blog_id == 1) {
        if (isset($_GET['download_id']) && !empty($_GET['download_id'])) {
            $download_id = $_GET['download_id'];
            $this_site_home = get_bloginfo('url');
            $permalink = get_permalink();

            switch_to_blog(5);

            ob_start();
            $rows = mcn_get_pricing_table_rows('mch_course_group', $download_id);
            $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $download_id);
            $periods = mcn_get_normalized_period_array($download_id);

            if ($rows && $characteristics): $_id = $download_id;?>
                <table border="1"
                       class="pricing_table pricing_table_<?php echo $download_id; ?>" <?php if ($_id != $download_id) echo 'style="display:none;"'; ?>>
                    <tr class="header-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>">  <?php /*returned classes pricing_table pricing_row_recommended, pricing_table pricing_row_unrecommended*/ ?>
                                <?php if ($rec == 'true'): ?>
                                    <?php _e('Recommended'); ?>
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr class="title-td-table">
                        <td>
                            <?php
                            $download_title = get_the_title($_GET['download_id']);
                            echo $download_title;
                            ?>
                        </td>
                        <?php foreach ($rows as $key => $row): ?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"><?php _e($row); ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($characteristics as $characteristic): ?>
                        <tr class="content-table">
                            <td><?php _e($characteristic->post_title); ?></td>
                            <?php foreach ($rows as $key => $row):
                                $check = get_post_meta($characteristic->ID, 'post-' . $key . '-check', true);?>
                                <td class="<?php echo mcn_get_recommended_row_class($key); ?> <?php echo mcn_get_characteristic_class($characteristic->ID, $key); ?>">     <?php /* return class="pricing_table pricing_table_check" or class="pricing_table pricing_table_uncheck"*/ ?>
                                    <?php if ($check): ?>
                                        <i class="fa fa-check"></i>
                                    <?php else: ?>
                                        <i class="fa fa-minus"></i>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="separate-table border">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>

                    <tr class="separate-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($periods as $key => $period): ?>
                        <?php if (isset($_GET['download_id']) && !empty($_GET['download_id'])): ?>
                            <?php $_id = $_GET['download_id']; ?>
                        <?php endif; ?>
                        <tr class="footer-table">

                            <td><?php echo(ucfirst($key) . " Subscription: "); ?></td>

                            <?php foreach ($period as $post => $value): ?>

                                <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                    <?php if (array_key_exists($post, $rows)): ?>
                                        <a href="<?php echo $permalink; ?>?cart_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>">$<?php echo $value['amount']; ?></a>
                                    <?php endif; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>

                    <?php endforeach; ?>
                </table>
            <?php
            endif;
            $result = ob_get_clean();

            restore_current_blog();

            return $result;
        } else {
            $this_site_home = get_bloginfo('url');

            wp_redirect($this_site_home . '/pricing');
        }
    } elseif ($blog_id == 5) {
        if (isset($_GET['download_id']) && !empty($_GET['download_id'])) {
            $download_id = $_GET['download_id'];
            $this_site_home = get_bloginfo('url');
            $permalink = get_permalink();

            switch_to_blog(1);

            ob_start();
            $rows = mcn_get_pricing_table_rows('mch_course_group', $download_id);
            $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $download_id);
            $periods = mcn_get_normalized_period_array($download_id);

            if ($rows && $characteristics): $_id = $download_id;?>
                <table border="1"
                       class="pricing_table pricing_table_<?php echo $download_id; ?>" <?php if ($_id != $download_id) echo 'style="display:none;"'; ?>>
                    <tr class="header-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>">  <?php /*returned classes pricing_table pricing_row_recommended, pricing_table pricing_row_unrecommended*/ ?>
                                <?php if ($rec == 'true'): ?>
                                    <?php _e('Recommended'); ?>
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr class="title-td-table">
                        <td>
                            <?php
                            $download_title = get_the_title($_GET['download_id']);
                            echo $download_title;
                            ?>
                        </td>
                        <?php foreach ($rows as $key => $row): ?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"><?php _e($row); ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($characteristics as $characteristic): ?>
                        <tr class="content-table">
                            <td><?php _e($characteristic->post_title); ?></td>
                            <?php foreach ($rows as $key => $row):
                                $check = get_post_meta($characteristic->ID, 'post-' . $key . '-check', true);?>
                                <td class="<?php echo mcn_get_recommended_row_class($key); ?> <?php echo mcn_get_characteristic_class($characteristic->ID, $key); ?>">     <?php /* return class="pricing_table pricing_table_check" or class="pricing_table pricing_table_uncheck"*/ ?>
                                    <?php if ($check): ?>
                                        <i class="fa fa-check"></i>
                                    <?php else: ?>
                                        <i class="fa fa-minus"></i>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="separate-table border">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>

                    <tr class="separate-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($periods as $key => $period): ?>
                        <?php if (isset($_GET['download_id']) && !empty($_GET['download_id'])): ?>
                            <?php $_id = $_GET['download_id']; ?>
                        <?php endif; ?>
                        <tr class="footer-table">

                            <td><?php echo(ucfirst($key) . " Subscription: "); ?></td>

                            <?php foreach ($period as $post => $value): ?>

                                <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                    <?php if (array_key_exists($post, $rows)): ?>
                                        <a href="<?php echo $permalink; ?>?cart_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>">$<?php echo $value['amount']; ?></a>
                                    <?php endif; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>

                    <?php endforeach; ?>
                </table>
            <?php
            endif;
            $result = ob_get_clean();

            restore_current_blog();

            return $result;
        } else {
            $this_site_home = get_bloginfo('url');
            wp_redirect($this_site_home . '/pricing');
        }
    }
}

add_shortcode('external_pricing', 'external_pricing_page');

function subtotal_tutor_cart($subtotal)
{
    global $blog_id;
    $user_id = get_current_user_id();
    $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
    $hub_cart = get_user_meta($user_id, 'hub_cart', true);
    if ($blog_id == 5) {
        if ($tutor_cart && !empty($tutor_cart)) {

            switch_to_blog(1);
            $subtotal = null;
            $download_id = $tutor_cart[0]['id'];
            $price_id = $tutor_cart[0]['options']['price_id'];
            $subtotal = edd_get_price_option_amount($download_id, $price_id);
            restore_current_blog();
            return $subtotal;

        } else {
            return $subtotal;
        }
    }
    if ($blog_id == 1) {
        if ($hub_cart && !empty($hub_cart)) {

            switch_to_blog(5);
            $subtotal = null;
            $download_id = $hub_cart[0]['id'];
            $price_id = $hub_cart[0]['options']['price_id'];
            $subtotal = edd_get_price_option_amount($download_id, $price_id);
            restore_current_blog();
            return $subtotal;

        } else {
            return $subtotal;
        }
    }

}

add_filter('edd_get_cart_subtotal', 'subtotal_tutor_cart');

/**
 * Switch to needle blog
 * @param $payment_data
 */
function switch_external_payment($payment_data)
{
    global $blog_id;
    $user_id = get_current_user_id();
    $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
    $hub_cart = get_user_meta($user_id, 'hub_cart', true);
    $User = new WP_User($user_id);

    if (isset($tutor_cart[0]) && !empty($tutor_cart[0])) {
        switch_to_blog(1);
    } elseif (isset($hub_cart[0]) && !empty($hub_cart[0])) {
        switch_to_blog(5);
    }
}

add_action('pre_switch_external_purchase', 'switch_external_payment');


/**
 * On complete new order - update expiration time for it's download
 * @param $payment_id
 * @param $new_status
 * @param $old_status
 */
function update_expiration_date($payment_id, $new_status, $old_status)
{
    global $blog_id;
    $user_id = get_current_user_id();
    $hub_cart = get_user_meta($user_id, 'hub_cart', true);


    if ($blog_id == 5) {
        if ($new_status == 'publish') {
            $user_id = edd_get_payment_user_id($payment_id);
            $payment_meta = edd_get_payment_meta_cart_details($payment_id);
            foreach ($payment_meta as $current_download) {
                $download_id = $current_download['id'];
                $options = $current_download['item_number']['options'];
                $price_id = $options['price_id'];
                update_download_expiration($user_id, $download_id, $price_id);
            }
        }
    } elseif ($hub_cart) {
        if ($new_status == 'publish') {
            switch_to_blog(5);
            $user_id = edd_get_payment_user_id($payment_id);
            $payment_meta = edd_get_payment_meta_cart_details($payment_id);
            foreach ($payment_meta as $current_download) {
                $download_id = $current_download['id'];
                $options = $current_download['item_number']['options'];
                $price_id = $options['price_id'];
                update_download_expiration($user_id, $download_id, $price_id);
            }
            restore_current_blog();
        }
    }

}

add_action('edd_update_payment_status', 'update_expiration_date', 10, 3);

/**
 * Correcting expiration on "quart" subscription
 * @param $expiration
 * @param $user_id
 * @param $payment_id
 * @param $period
 * @return int
 */
function correcting_expiration_date($expiration, $user_id, $payment_id, $period)
{
    // Retrieve the items purchased from the original payment
    $downloads = edd_get_payment_meta_downloads($payment_id);

    $download = $downloads[0]; // We only care about the first (and only) item
    $period = $download['options']['recurring']['period'];
    if ($period == 'quart') {
        $expiration = strtotime('+ 3 month 23:59:59');
    } else {
        $expiration = strtotime('+ 1 ' . $period . ' 23:59:59');
    }
    return $expiration;
}

add_filter('edd_recurring_calc_expiration', 'correcting_expiration_date', 10, 4);

/**
 * Add validation scripts and styles
 */
function common_page_enqueue_script()
{
    $checkout_url = edd_get_checkout_uri();

    if (get_the_permalink() == $checkout_url) {
        wp_register_script('purchase_validation', plugins_url('assets/js/checkout_field_validation.js', __FILE__), array('jquery'));
        wp_enqueue_script('purchase_validation');
        wp_enqueue_style('purchase_style', plugins_url('assets/css/validation.css', __FILE__));
    }
}

add_action('wp_enqueue_scripts', 'common_page_enqueue_script');

/**
 * @return bool|string
 */
function after_payment_redirect_shortcode()
{

    if (isset($_SESSION['tutor_purchase']) && $_SESSION['tutor_purchase'] == true) {
        unset($_SESSION['tutor_purchase']);
        ob_start(); ?>
        <div class="purchase-success-message">
            <h2><?php echo __('Thanks for your purchase! You have subscribed on MyChineseTutor just now'); ?></h2>
        </div>
        <?php
        $return = ob_get_clean();
        return $return;
    } else if (isset($_SESSION['hub_purchase']) && $_SESSION['hub_purchase'] == true) {
        unset($_SESSION['hub_purchase']);
        ob_start(); ?>
        <div class="purchase-success-message">
            <h2><?php echo __('Thanks for your purchase! You have subscribed on MyChineseHub just now'); ?></h2>
        </div>
        <?php
        $return = ob_get_clean();
        return $return;
    } else {
        $pricing_page = edd_get_option('mct_edd_pricing_page_redirect');
        return wp_redirect(get_the_permalink($pricing_page));
    }
}

add_shortcode('empty_cart', 'after_payment_redirect_shortcode');

/**
 * @param $payment_id
 */
function paypal_pending_redirect($payment_id)
{
    global $blog_id;

    if (isset($payment_id) && !empty($payment_id)) {
        $redirect = edd_get_success_page_uri();
        $payment_meta = get_post_meta($payment_id);
        $edd_payment_meta = maybe_unserialize($payment_meta['_edd_payment_meta'][0]);
        $download_id = $edd_payment_meta['downloads'][0]['id'];
        $user_id = get_current_user_id();
        $hub_cart = get_user_meta($user_id, 'hub_cart');
        $tutor_cart = get_user_meta($user_id, 'tutor_cart');
        $serialize_site_options = get_site_option('mychinese_auto_subscription', true);
        $site_options = maybe_unserialize($serialize_site_options);
        $period = $edd_payment_meta['downloads'][0]['options']['recurring']['period'];

        if ($blog_id == 5) {
            if (isset($tutor_cart[0]) && !empty($tutor_cart[0])) {
                switch_to_blog(1);
                wp_redirect($redirect);
            } elseif ($hub_cart && !empty($hub_cart)) {
                $hub_purchase_id = $_GET['payment-id'];
                $success_page_id = get_edd_settings_blog(1, 'success_page');
                $success_page = get_blog_permalink(1, $success_page_id);
                wp_redirect($success_page . '/?hub_purchase_id=' . $hub_purchase_id);
                delete_user_meta($user_id, 'hub_cart');
                edd_die();
            } else {
                if ($download_id && !empty($download_id)) {
                    foreach ($site_options as $key => $option) {
                        $tutor_download = preg_replace("/[^0-9]/", '', $key);
                        $hub_download = preg_replace("/[^0-9]/", '', $option);
                        if (isset($hub_download) && !empty($hub_download)) {

                            if ($download_id == $hub_download) {
                                $link = $tutor_download;
                                break;
                            }
                        }
                    }
                }
            }

            if ($link && !empty($link)) {
                $redirect = $redirect . '/?download_id=' . $link . '&period=' . $period;
            }
        } else if ($blog_id == 1) {
            if (isset($hub_cart[0]) && !empty($hub_cart[0])) {
                switch_to_blog(1);
                wp_redirect($redirect);
            } elseif ($tutor_cart && !empty($tutor_cart)) {
                $tutor_purchase_id = $_GET['payment-id'];
                $success_page_id = get_edd_settings_blog(5, 'success_page');
                $success_page = get_blog_permalink(5, $success_page_id);
                wp_redirect($success_page . '/?tutor_purchase_id=' . $tutor_purchase_id);
                delete_user_meta($user_id, 'tutor_cart');
                edd_die();
            } else {
                if ($download_id && !empty($download_id)) {
                    foreach ($site_options as $key => $option) {
                        $tutor_download = preg_replace("/[^0-9]/", '', $key);
                        $hub_download = preg_replace("/[^0-9]/", '', $option);
                        if (isset($tutor_download) && !empty($tutor_download)) {
                            if ($download_id == $tutor_download) {
                                if ($hub_download && !empty($hub_download)) {
                                    $link = $hub_download;
                                }
                            }
                        }
                    }
                }

                if ($link && !empty($link)) {
                    $redirect = $redirect . '/?download_id=' . $link . '&period=' . $period;
                }
            }
        }

        wp_redirect($redirect);
        edd_die();
    }
}

add_action('paypal_pending_redirect', 'paypal_pending_redirect');

/**
 * Redirect to success page from mecca
 */
function payment_history_redirect()
{
    global $blog_id;

    if ($blog_id == 5) {
        $user_id = get_current_user_id();
        $hub_purchase_id = $_GET['payment-id'];
        $hub_cart = get_user_meta($user_id, 'hub_cart', true);
        if ($hub_cart && !empty($hub_cart)) {
            $success_page_id = get_edd_settings_blog(1, 'success_page');
            $success_page = get_blog_permalink(1, $success_page_id);
            wp_redirect($success_page . '/?hub_purchase_id=' . $hub_purchase_id);
        }
    }
}

add_action('edd_purchase_history_header_before', 'payment_history_redirect');


/**
 * Get EDD settings from specific blog
 * @param $need_blog_id
 * @param $setting_name
 * @return bool
 */
function get_edd_settings_blog($need_blog_id, $setting_name)
{
    global $wpdb;

    switch_to_blog($need_blog_id);
    $table_name = $wpdb->prefix . 'options';
    $sql = $wpdb->prepare("SELECT option_value FROM `$table_name` WHERE option_name = '%s'", 'edd_settings');
    $query = $wpdb->get_results($sql);

    if ($query && !empty($query)) {
        $settings_array = maybe_unserialize($query[0]->option_value);
    }

    if ($settings_array && !empty($settings_array)) {
        if (isset($settings_array[$setting_name]) && !empty($settings_array[$setting_name])) {
            restore_current_blog();
            return $settings_array[$setting_name];
        }
    }

    restore_current_blog();
    return false;

}

function remember_insert_payment($payment, $payment_data)
{
    global $blog_id;

    $user_id = get_current_user_id();

    if ($blog_id == 1) {
        if ($payment && !empty($payment)) {
            update_user_meta($user_id, 'tutor_purchase', $payment);
        }
    } elseif ($blog_id == 5) {
        if ($payment && !empty($payment)) {
            update_user_meta($user_id, 'hub_purchase', $payment);
        }
    }
}

add_action('edd_insert_payment', 'remember_insert_payment', 10, 2);

function delete_both_purchase_information()
{
    $user_id = get_current_user_id();
    $purchase_page = edd_get_settings('success_page');

    if (get_permalink() !== get_permalink($purchase_page['success_page'])) {
        if (get_permalink() !== get_permalink($purchase_page['purchase_page'])) {
            delete_user_meta($user_id, 'tutor_purchase');
            delete_user_meta($user_id, 'hub_purchase');
            delete_user_meta($user_id, 'hub_cart');
            delete_user_meta($user_id, 'tutor_cart');
            delete_user_meta($user_id, 'hub_discount_name');
            delete_user_meta($user_id, 'tutor_discount_name');
        }
    }

}

add_action('wp', 'delete_both_purchase_information');

function delete_purchase_and_cat_after_login($login)
{
    $user = get_user_by('login', $login);
    $user_id = $user->ID;

    delete_user_meta($user_id, 'tutor_purchase');
    delete_user_meta($user_id, 'hub_purchase');
    delete_user_meta($user_id, 'hub_cart');
    delete_user_meta($user_id, 'tutor_cart');
}

add_action('wp_login', 'delete_purchase_and_cat_after_login', 99);

function change_paypal_back_button($paypal_args, $purchase_data)
{
    if ($paypal_args) {
        $paypal_args['cbt'] = __('MyChinese', 'edd');
        return $paypal_args;
    }
}

add_filter('edd_paypal_redirect_args', 'change_paypal_back_button', 10, 2);

function edd_change_ajax_discount($return)
{
        $user_id = get_current_user_id();
        $hub_cart = get_user_meta($user_id, 'hub_cart', true);
        $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
        if ($hub_cart && !empty($hub_cart)) {

            switch_to_blog(5);



            if (isset($_POST['code'])) {
                $discount_code = $_POST['code'];

                $return = array(
                    'msg' => '',
                    'code' => $discount_code
                );

                if (edd_is_discount_valid($discount_code)) {
                    $discount = edd_get_discount_by_code($discount_code);
                    $amount = edd_format_discount_rate(edd_get_discount_type($discount->ID), edd_get_discount_amount($discount->ID));
                    $discounts = edd_set_cart_discount($discount_code);
                    $total = edd_get_cart_total($discounts);
                    $return = array(
                        'msg' => 'valid',
                        'amount' => $amount,
                        'total_plain' => $total,
                        'total' => html_entity_decode(edd_currency_filter(edd_format_amount($total)), ENT_COMPAT, 'UTF-8'),
                        'code' => $_POST['code'],
                        'html' => edd_get_cart_discounts_html($discounts)
                    );
                    update_user_meta($user_id, 'hub_discount_name', $_POST['code']);
                } else {
                    $errors = edd_get_errors();
                    $return['msg'] = $errors['edd-discount-error'];
                    edd_unset_error('edd-discount-error');
                }

                restore_current_blog();
            }
        }
    elseif($tutor_cart && !empty($tutor_cart)){

        switch_to_blog(1);

        if (isset($_POST['code'])) {
            $discount_code = $_POST['code'];

            $return = array(
                'msg' => '',
                'code' => $discount_code
            );

            if (edd_is_discount_valid($discount_code)) {
                $discount = edd_get_discount_by_code($discount_code);
                $amount = edd_format_discount_rate(edd_get_discount_type($discount->ID), edd_get_discount_amount($discount->ID));
                $discounts = edd_set_cart_discount($discount_code);
                $total = edd_get_cart_total($discounts);
                $return = array(
                    'msg' => 'valid',
                    'amount' => $amount,
                    'total_plain' => $total,
                    'total' => html_entity_decode(edd_currency_filter(edd_format_amount($total)), ENT_COMPAT, 'UTF-8'),
                    'code' => $_POST['code'],
                    'html' => edd_get_cart_discounts_html($discounts)
                );
                update_user_meta($user_id, 'tutor_discount_name', $_POST['code']);
            } else {
                $errors = edd_get_errors();
                $return['msg'] = $errors['edd-discount-error'];
                edd_unset_error('edd-discount-error');
            }

            restore_current_blog();
        }
    }

    return $return;
}

add_filter('edd_ajax_discount_response', 'edd_change_ajax_discount');

function discount_validation_common_page($return, $discount_id, $code, $user){
    $user_id = get_current_user_id();
    $hub_cart = get_user_meta($user_id, 'hub_cart', true);
    $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
    $set_error = true;

    if($hub_cart){
        switch_to_blog(5);
        if ( $discount_id ) {
            if (
                edd_is_discount_active( $discount_id ) &&
                edd_is_discount_started( $discount_id ) &&
                !edd_is_discount_maxed_out( $discount_id ) &&
                !edd_is_discount_used( $code, $user, $discount_id ) &&
                edd_discount_is_min_met( $discount_id ) &&
                edd_discount_product_reqs_met( $discount_id )
            ) {
                $return = true;
            }
        } elseif( $set_error ) {
            edd_set_error( 'edd-discount-error', __( 'This discount is invalid.', 'edd' ) );
        }
        restore_current_blog();
    }
    elseif($tutor_cart){
        switch_to_blog(1);
        if ( $discount_id ) {
            if (
                edd_is_discount_active( $discount_id ) &&
                edd_is_discount_started( $discount_id ) &&
                !edd_is_discount_maxed_out( $discount_id ) &&
                !edd_is_discount_used( $code, $user, $discount_id ) &&
                edd_discount_is_min_met( $discount_id ) &&
                edd_discount_product_reqs_met( $discount_id )
            ) {
                $return = true;
            }
        } elseif( $set_error ) {
            edd_set_error( 'edd-discount-error', __( 'This discount is invalid.', 'edd' ) );
        }
        restore_current_blog();
    }
    return $return;
}

add_filter('edd_is_discount_valid', 'discount_validation_common_page', 10, 4);

function fill_user_name_after_payment($payment, $payment_data){
    $user_id = $payment_data['user_info']['id'];
    $user_first_name = $payment_data['user_info']['first_name'];
    $user_last_name = $payment_data['user_info']['last_name'];

    if($user_first_name && !empty($user_first_name)){
        update_user_meta($user_id, 'first_name', $user_first_name);
    }

    if($user_last_name && !empty($user_last_name)){
        update_user_meta($user_id, 'last_name', $user_last_name);
    }

}

add_action('edd_insert_payment', 'fill_user_name_after_payment', 10, 2);