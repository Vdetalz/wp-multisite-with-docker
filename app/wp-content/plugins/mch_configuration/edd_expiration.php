<?php
register_activation_hook(__FILE__, 'create_edd_expiration_database');

/**
 *
 */
function create_edd_expiration_database()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "download_expiration";

    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id bigint(11) NOT NULL AUTO_INCREMENT,
				  user_id bigint(11) DEFAULT '0' NOT NULL,
				  download_id bigint(11) DEFAULT '0' NOT NULL,
				  price_id bigint(11) DEFAULT '0' NOT NULL,
				  expired_date VARCHAR(255) NULL,
				  UNIQUE KEY id (id)
				);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}


/**
 * Getting all expiration dates for user and download
 * @param $user_id
 * @param $download_id
 * @return bool
 */
function get_user_and_download_expiration_dates($user_id, $download_id)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_expiration';

    if($download_id == 'any'){
        $sql = $wpdb->prepare("SELECT expired_date FROM " . $table_name . " WHERE user_id = %d ", $user_id);
        $query = $wpdb->get_col($sql);
    }
    else{
        $sql = $wpdb->prepare("SELECT expired_date FROM " . $table_name . " WHERE user_id = %d AND download_id = %d", $user_id, $download_id);
        $query = $wpdb->get_col($sql);
    }

    if (isset($query) && !empty($query)) {
        return $query;
    } else {
        return false;
    }
}

/**
 * Filter to EDD_HAS_ACCESS function, which looks at expiration download time
 * @param $has_access
 * @param $user_id
 * @param $restricted_to
 * @return bool
 */
function download_expiration_date($has_access, $user_id, $restricted_to)
{

    $download_id = $restricted_to[0]['download'];
    $price_id = $restricted_to[0]['price_id'];

    $expiration_date = get_expiration_date($user_id, $download_id, $price_id);
    $current_time = time();

    $trial_period_time = get_user_meta($user_id, 'hub_ending_trial_time', true);

    //admin have all accesses
    if(current_user_can('manage_options')){
        return true;
    }

   if((int)$current_time < (int)$trial_period_time){
       return true;
   }

    if ($price_id == 'all') {
        $all_expiration_dates = get_user_and_download_expiration_dates($user_id, $download_id);
        if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
            foreach ($all_expiration_dates as $exp_date) {
                if ($current_time < $exp_date) {
                    return $has_access;
                    break;
                }
            }
        } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
            if ($current_time < $all_expiration_dates) {
                return $has_access;
            }
        }
    } else {
        if ($expiration_date && !empty($expiration_date)) {
            if ($current_time > $expiration_date) {
                return false;
            } else {
                return $has_access;
            }
        } else {
            return false;
        }
    }
}

add_filter('edd_cr_user_can_access', 'download_expiration_date', 10, 3);

/**
 * @param $user_id
 * @return bool
 */
function get_all_expirations_by_user($user_id){
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_expiration';

    $sql = $wpdb->prepare("SELECT expired_date FROM " . $table_name . " WHERE user_id = %d ", $user_id);
    $query = $wpdb->get_col($sql);

    if($query && !empty($query)){
        return $query;
    }
    else{
        return false;
    }
}