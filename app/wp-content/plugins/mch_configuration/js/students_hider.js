jQuery(document).ready(function($){

	$('.open-info').click(function(){
        $(this).toggleClass('active');
        if(!$(this).hasClass('active')){
            $(this).parents().eq(4).find('.edit-hide').show();
            $(this).parents().eq(4).find('.edit-show').hide().parent().parent().find('.user-account-title').removeClass('active');
            $(this).parents().eq(4).find('.edit-submit-show').hide().parent().parent().find('.user-account-title').removeClass('active');
            $(this).parents().eq(4).find('.photo-show').hide();
            $(this).parents().eq(4).find('.photo-hide').show();
            $(this).parents().eq(4).find('.user-edition').show();
            $('.right-block').show();
        }
		$(this).parent().next().slideToggle('slow');
		$(this).parent().find('.student-email').toggle();
		$(this).parent().find('.student-level').toggle();
        $(this).closest('.single-student-wrapper').toggleClass('active');
	});

    $('.user-edition').on('click', function(){
        $(this).hide();
        $(this).parents().eq(4).find('.edit-hide').hide().parent().parent().find('.user-account-title').removeClass('active');
        $(this).parents().eq(4).find('.edit-show').show().parent().parent().find('.user-account-title').addClass('active');
        $(this).parents().eq(4).find('.edit-submit-show').show();
        $('.right-block').show();
    });

    $('.photo-hide').on('click', function(){
       $(this).hide();
       $('.photo-show').show();
    });
});