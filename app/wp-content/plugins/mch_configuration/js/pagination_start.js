jQuery(document).ready(function ($) {
    var pages_count = $('.people-page').size();
    $('.people-page').hide();
    $('.people-page').first().show();

    if (pages_count > 1) {
        if ($('#jquery-pagination') && $('#jquery-pagination').length > 0) {
            $('#jquery-pagination').twbsPagination({
                totalPages: pages_count,
                visiblePages: 7,
                first: '',
                last: '',
                onPageClick: function (event, page) {
                    $('.people-page').each(function () {
                        if ($(this).data('page') == page) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    });

                }
            });
        }
    }
    else{
        $('.pagination-sm').remove();
    }
});