jQuery(document).ready(function ($) {
    // Current student data
    var id, path_number, user_id, teacher_id, course_id, module_id, lesson_id, technology_rating, ct_rating, comment, next_course_id, next_module_id, next_lesson_id, next_path_number, clicked_element, new_history_item;
    // Collect data about curent student
    $('.plus-button').live('click', function () {

        id = $(this).attr('id');
        path_number = $('#' + id + '.path-level-select').find(':selected').data('path-number');
        user_id = $('#' + id + '.user-id').val();
        teacher_id = $('#' + id + '.teacher-id').val();
        course_id = $('#' + id + '.path-level-select').val();
        module_id = $('#' + id + '.module-select').val();
        lesson_id = $('#' + id + '.lesson-select').val();
        technology_rating = $('#' + id + '.technology-rating').val();
        ct_rating = $('#' + id + '.ct-rating').val();
        comment = $('#' + id + '.lesson-comment').val();
        // If rank lower than 2.
        lowRankComment = $('input.user-low-rank#' + id).val();

        if (lowRankComment != undefined) {
            if (lowRankComment.length >= 1) {
                comment += '. Explaination what went wrong with the technology or classroom: ';
                comment += $('input.user-low-rank#' + id).val();
            }
        }

        if ($('div#' + id + '.next-lesson-number-block-wrapper').is(':hidden')) {
            next_course_id = '';
            next_module_id = '';
            next_lesson_id = '';
        }
        else {
            next_course_id = $('select#' + id + '.next-path-level-select').val();
            next_module_id = $('select#' + id + '.next-module-select').val();
            next_lesson_id = $('select#' + id + '.next-lesson-select').val();
            next_path_number = $('#' + id + '.next-path-level-select').find(':selected').data('path-number');
        }

        if (!$.magnificPopup.instance.isOpen) {
            $.magnificPopup.open({
                items: {
                    src: $('#confirmation-message')
                },
                key: id,
                closeOnBgClick: false,
            });

        } else {
            $.magnificPopup.instance.close();
        }

        // Confirmation data
        var confirmationContainer, lessonCompleted, lessonUpcoming, course_name, module_name, lesson_name, next_course_name, next_module_name, next_lesson_name, commentsConfirmation;
        confirmationContainer = $('body').find('#confirmation-message');
        lessonCompleted = confirmationContainer.find('.lesson-comleted');
        lessonUpcoming = confirmationContainer.find('.lesson-upcoming');

        course_name = $('#' + id + '.path-level-select option:selected').text();
        module_name = $('#' + id + '.module-select option:selected').text();
        lesson_name = $('#' + id + '.lesson-select option:selected').text();

        next_course_name = $('select#' + id + '.next-path-level-select option:selected').text();
        next_module_name = $('select#' + id + '.next-module-select option:selected').text();
        next_lesson_name = $('select#' + id + '.next-lesson-select option:selected').text();

        lessonCompleted.find('.path' ).text(course_name + ' / ');
        lessonCompleted.find('.module').text(module_name + ' / ');
        lessonCompleted.find('.lesson').text(lesson_name);
        lessonUpcoming.find('.next_path' ).text(next_course_name + ' / ');
        lessonUpcoming.find('.next_module').text(next_module_name + ' / ');
        lessonUpcoming.find('.next_lesson').text(next_lesson_name);

        if(comment == ''){
            confirmationContainer.find('.container_lesson_comment').hide();
        }else {
            confirmationContainer.find('.container_lesson_comment').show();
            confirmationContainer.find('.lesson_comment').text(comment);
        }

    });


    // Sends request after confirmation window
    $('.confirmation-message-button').click(function (e) {

        $.ajax({
            type: 'post',
            url: Ajax.ajaxurl,
            data: 'action=save_lesson_history&user_id=' + user_id + '&teacher_id=' + teacher_id + '&course_id=' + course_id + '&module_id=' + module_id + '&lesson_id=' + lesson_id + '&comment=' + comment + '&next_course_id=' + next_course_id + '&next_module_id=' + next_module_id + '&next_lesson_id=' + next_lesson_id + '&path_number=' + path_number + '&next_path_number=' + next_path_number + '&technology_rating=' + technology_rating + '&ct_rating=' + ct_rating,
            success: function (results) {

                new_history_item = $(results).find('div.history-wrapper');
                if (id != 1) {
                    new_history_item = new_history_item.attr('id', 'a-' + id + '-0');
                    new_history_item.find('.history-item-wrapper').attr('id', 'a-' + id + '-0');
                    new_history_item.find('.history-popup-box').attr('id', 'popup-box-a-' + id + '-0');
                }

                $('.history-item-wrapper').each(function () {

                    if (id == 1) {

                        if ($(this).attr('id') == 2) {
                            $(this).remove();
                            $(this).parent().remove();
                        }
                        else if ($(this).attr('id') == 1) {
                            $(this).attr('id', 2);
                        }
                        else if ($(this).attr('id') == 0) {
                            $(this).attr('id', 1);
                        }
                    }
                    else {

                        if ($(this).attr('id') == 'a-' + id + '-2') {
                            $(this).remove();
                            $(this).parent().remove();
                        }
                        else if ($(this).attr('id') == 'a-' + id + '-1') {
                            $(this).attr('id', 'a-' + id + '-2');
                        }
                        else if ($(this).attr('id') == 'a-' + id + '-0') {
                            $(this).attr('id', 'a-' + id + '-1');
                        }
                    }
                });

                $('.history-popup-box').each(function () {

                    if (id == 1) {
                        if ($(this).attr('id') == 'popup-box-2') {
                            $(this).remove();
                            $(this).parent().remove();
                        }
                        else if ($(this).attr('id') == 'popup-box-1') {
                            $(this).attr('id', 'popup-box-2');
                        }
                        else if ($(this).attr('id') == 'popup-box-0') {
                            $(this).attr('id', 'popup-box-1');
                        }
                    }
                    if (id != 1) {

                        if ($(this).attr('id') == 'popup-box-a-' + id + '-2') {
                            $(this).remove();
                            $(this).parent().remove();
                        }
                        else if ($(this).attr('id') == 'popup-box-a-' + id + '-1') {
                            $(this).attr('id', 'popup-box-a-' + id + '-2');
                        }
                        else if ($(this).attr('id') == 'popup-box-a-' + id + '-0') {
                            $(this).attr('id', 'popup-box-a-' + id + '-1');
                        }
                    }
                });

                $('#' + id + '.lessons-loop').prepend(new_history_item);

                var new_full_history = $(results).filter('div.full-history-popup-box').attr('id', 'popup-box-full-history-' + id);

                $('#popup-box-full-history-' + id + '.full-history-popup-box').remove();

                $('#' + id + '.all-complete-lessons-wrapper').append(new_full_history);

                $('.history-wrapper').each(function () {
                    if ($(this).index() > 2) {
                        $(this).remove();
                    }
                });
            },
            complete: function () {
                id = '';
                path_number = '';
                user_id = '';
                teacher_id = '';
                course_id = '';
                module_id = '';
                lesson_id = '';
                technology_rating = '';
                ct_rating = '';
                comment = '';
                next_course_id = '';
                next_module_id = '';
                next_lesson_id = '';
                next_path_number = '';
                clicked_element = '';
                new_history_item = '';
            }
        });
        var popup = $.magnificPopup.instance;
        $.magnificPopup.instance.close();
    });

    $('.student-about-save').live('click', function () {
        var student_id = $(this).data('user-id');
        var student_about = $("textarea.student-details[data-student-id='" + student_id + "']").val();
        $.ajax({
            type: 'post',
            url: Ajax.ajaxurl,
            data: {
                action: 'save_student_about',
                student_id: student_id,
                student_about: student_about
            },
            success: function (result) {
                if (result) {
                    var textarea_selector = $("textarea.student-details[data-student-id='" + student_id + "']");
                    textarea_selector.addClass('saved');
                    setTimeout(function () {
                        textarea_selector.removeClass('saved');
                    }, 500);
                }
            }
        })
    });
    
    var cancel_btn;
    var poor_tech;
    
    $('#poor-tech-popup #poor-tech-no').click(function(e){
        e.preventDefault();
        poor_tech = 0;
        cancel_btn.trigger('click');
    });

	$('.save-cancel-detail').click(function(e){
		e.preventDefault();
		poor_tech = 1;
		var cancelParent = $(this).parents('#cancel-detail-message');
		var cancelContent = $('textarea', cancelParent).val();
		cancel_btn.trigger('click', cancelContent );
	});

	$('#poor-tech-yes').magnificPopup({
		closeBtnInside:true
	});

//CANCEL LESSON HANDLER
    $('.cancel-button-wrapper .cancel-btn').on('click', function (e, string) {
        if(!$.magnificPopup.instance.isOpen){
            cancel_btn = $(this);
            //Poor tech popup
            $.magnificPopup.open({
                items: {
                    src: $(this).data('mfp-src')
                }
            });
        } else {
            $.magnificPopup.instance.close();
            var id = $(this).parent().attr('id');
            var user_id = $('#' + id + '.user-id').val();
            var teacher_id = $('#' + id + '.teacher-id').val();
            var course_id = $('#' + id + '.path-level-select').val();
            var module_id = $('#' + id + '.module-select').val();
            var lesson_id = $('#' + id + '.lesson-select').val();
            var technology_rating = $('#' + id + '.technology-rating').val();
            var ct_rating = $('#' + id + '.ct-rating').val();
            var comment = $('#' + id + '.lesson-comment').val();
            var path_number = $('#' + id + '.path-level-select').find(':selected').data('path-number');

            if ($('div#' + id + '.next-lesson-number-block-wrapper').is(':hidden')) {
                var next_course_id = '';
                var next_module_id = '';
                var next_lesson_id = '';
            }
            else {
                var next_course_id = $('select#' + id + '.next-path-level-select').val();
                var next_module_id = $('select#' + id + '.next-module-select').val();
                var next_lesson_id = $('select#' + id + '.next-lesson-select').val();
            }

            var clicked_element = $(this);
            clicked_element.parent().find('.aj-loader').show();
            clicked_element.hide();

			var dataString = 'action=save_lesson_history&user_id=' + user_id + '&teacher_id=' + teacher_id + '&course_id='
				+ course_id + '&module_id=' + module_id + '&lesson_id=' + lesson_id + '&comment='
				+ comment + '&next_course_id=' + next_course_id + '&next_module_id='
				+ next_module_id + '&next_lesson_id=' + next_lesson_id + '&path_number='
				+ path_number + '&cancel_lesson=true' + '&technology_rating='
				+ technology_rating + '&ct_rating=' + ct_rating + '&poor_tech=' + poor_tech;

			if( string != null ){
				dataString += '&cancel_comment=' + string;
			}

            $.ajax({
                type: 'post',
                url: Ajax.ajaxurl,
                data: dataString,
                success: function (results) {

                    var new_history_item = $(results).find('div.history-wrapper');
                    if (id != 1) {
                        new_history_item = new_history_item.attr('id', 'a-' + id + '-0');
                        new_history_item.find('.history-item-wrapper').attr('id', 'a-' + id + '-0');
                        new_history_item.find('.history-popup-box').attr('id', 'popup-box-a-' + id + '-0');
                    }

                    $('.history-item-wrapper').each(function () {

                        if (id == 1) {

                            if ($(this).attr('id') == 2) {
                                $(this).remove();
                                $(this).parent().remove();
                            }
                            else if ($(this).attr('id') == 1) {
                                $(this).attr('id', 2);
                            }
                            else if ($(this).attr('id') == 0) {
                                $(this).attr('id', 1);
                            }
                        }
                        else {

                            if ($(this).attr('id') == 'a-' + id + '-2') {
                                $(this).remove();
                                $(this).parent().remove();
                            }
                            else if ($(this).attr('id') == 'a-' + id + '-1') {
                                $(this).attr('id', 'a-' + id + '-2');
                            }
                            else if ($(this).attr('id') == 'a-' + id + '-0') {
                                $(this).attr('id', 'a-' + id + '-1');
                            }
                        }
                    });


                    $('.history-popup-box').each(function () {

                        if (id == 1) {
                            if ($(this).attr('id') == 'popup-box-2') {
                                $(this).remove();
                                $(this).parent().remove();
                            }
                            else if ($(this).attr('id') == 'popup-box-1') {
                                $(this).attr('id', 'popup-box-2');
                            }
                            else if ($(this).attr('id') == 'popup-box-0') {
                                $(this).attr('id', 'popup-box-1');
                            }

                        }
                        if (id != 1) {

                            if ($(this).attr('id') == 'popup-box-a-' + id + '-2') {
                                $(this).remove();
                                $(this).parent().remove();
                            }
                            else if ($(this).attr('id') == 'popup-box-a-' + id + '-1') {
                                $(this).attr('id', 'popup-box-a-' + id + '-2');
                            }
                            else if ($(this).attr('id') == 'popup-box-a-' + id + '-0') {
                                $(this).attr('id', 'popup-box-a-' + id + '-1');
                            }
                        }


                    });

                    $('#' + id + '.lessons-loop').prepend(new_history_item);

                    var new_full_history = $(results).filter('div.full-history-popup-box').attr('id', 'popup-box-full-history-' + id);

                    $('#popup-box-full-history-' + id + '.full-history-popup-box').remove();


                    $('#' + id + '.all-complete-lessons-wrapper').append(new_full_history);


                    $('.history-wrapper').each(function () {
                        if ($(this).index() > 2) {
                            $(this).remove();
                        }
                    });

                    clicked_element.parent().find('.aj-loader').hide();
                    clicked_element.parent().find('.aj-accept').show().css("display", "inline-block");

                    if( $("textarea[name='cancel-comment']").length ){
                      $("textarea[name='cancel-comment']").val('');
                    }

                    setTimeout(function () {
                        clicked_element.parent().find('.aj-accept').hide();
                        clicked_element.show();
                    }, 3000)
                }
            });
        }
    });
});
