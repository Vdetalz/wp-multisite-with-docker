jQuery(document).ready(function ($) {
	$('.path-level-select').on('change', function () {
		var course_id = $(this).val();
		var id = $(this).attr('id');
		$.ajax({
			type: 'post',
			url : Ajax.ajaxurl,
			data: 'action=ajax_module_level_switcher&course_id=' + course_id + '&student_id=' + id,
			beforeSend: function(){
				$('#' + id + '.module-removed-part select').remove();
				$('#' + id + '.lesson-removed-part select').remove();
                $('#' + id + '.module-removed-part .chosen-container').remove();
                $('#' + id + '.lesson-removed-part .chosen-container').remove();

			},
			success: function(result){
				var res =  $($.parseHTML(result));
				var module_select = res[1];
				var lesson_select = res[3];


				$('#' + id + '.module-removed-part').append(module_select).children().attr('id', id);
				$('#' + id + '.lesson-removed-part').append(lesson_select).children().attr('id', id);
                if(res[5]){
                    if(res[5] != undefined){
                        $('#' + id + '.next-lesson-number-block-wrapper').show();
                    }
                    else{
                        $('#' + id + '.next-lesson-number-block-wrapper').hide();
                    }
                }
                else{
                    $('#' + id + '.next-lesson-number-block-wrapper').hide();
                }

                // Custom select
                var config = {
                    'select'            : {disable_search_threshold:10}
                };
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
			}
		});
	});

	$('.module-select').live('change', function(){

		var module_id = $(this).val();
		var id = $(this).attr('id');
		$.ajax({
			type: 'post',
			url: Ajax.ajaxurl,
			data: 'action=ajax_lesson_switcher&module_id=' + module_id,
			beforeSend: function(){
				$('#' + id + '.lesson-removed-part select').remove();
                $('#' + id + '.lesson-removed-part .chosen-container').remove();
			},
			success: function(result){

				$('#' + id + '.lesson-removed-part').append(result).children().attr('id', id);

                // Custom select
                var config = {
                    'select'            : {disable_search_threshold:10}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }

			}
		});
	});

    $('.next-path-level-select').on('change', function () {
        var course_id = $(this).val();
        var id = $(this).attr('id');
        $.ajax({
            type: 'post',
            url : Ajax.ajaxurl,
            data: 'action=ajax_new_lesson_select&next_course_id=' + course_id,
            beforeSend: function(){
                $('#' + id + '.next-module-removed-part select').remove();
                $('#' + id + '.next-lesson-removed-part select').remove();
                $('#' + id + '.next-module-removed-part .chosen-container').remove();
                $('#' + id + '.next-lesson-removed-part .chosen-container').remove();

            },
            success: function(result){
                var res =  $($.parseHTML(result));
                var module_select = res[1];
                var lesson_select = res[3];

                $('#' + id + '.next-module-removed-part').append(module_select).children().attr('id', id);;
                $('#' + id + '.next-lesson-removed-part').append(lesson_select).children().attr('id', id);


                // Custom select
                var config = {
                    'select'            : {disable_search_threshold:10}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
            }
        });
    });

    $('.next-module-select').live('change', function(){
        var module_id = $(this).val();
        var id = $(this).attr('id');
        $.ajax({
            type: 'post',
            url: Ajax.ajaxurl,
            data: 'action=ajax_new_lesson_select&next_module=' + module_id,
            beforeSend: function(){
                $('#' + id + '.next-lesson-removed-part select').remove();
                $('#' + id + '.next-lesson-removed-part .chosen-container').remove();
            },
            success: function(result){
                $('#' + id + '.next-lesson-removed-part').append(result).children().attr('id', id);

                // Custom select
                var config = {
                    'select'            : {disable_search_threshold:10}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
            }
        });
    });

	$('select[name="location_name"]').live('change', function(){
		var location = $(this).val();
			console.log(location);
		$.ajax({
			type: 'post',
			url: Ajax.ajaxurl,
			data: {
				action: 'mch_location_schools',
				location:location
			},
			success: function(result){
				$('select[name="school_name"]').html(result);
				$("#location-scools").trigger("chosen:updated");
			}
		});
	});
});

