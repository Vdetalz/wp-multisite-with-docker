jQuery(document).ready(function($){
    $('.downloads-list').change(function(){
        $('.level-grade').val(null);
        $('.module-number').val(null);
        $('.actual-lesson').val(null);

        $('form.lesson-switch-form').submit();
    });

    $('.level-grade').change(function(){
        $('.module-number').val(null);
        $('.actual-lesson').val(null);

        $('form.lesson-switch-form').submit();
    });

    $('.module-number').change(function(){
        $('.actual-lesson').val(null);

        $('form.lesson-switch-form').submit();
    });

    $('.single-lesson-item-wrapper a').click(function(e){
        e.preventDefault();

        var current_lesson = $(this).parent().next().val();
        $('.actual-lesson').val(current_lesson);

        $('form.lesson-switch-form').submit();
    });
});