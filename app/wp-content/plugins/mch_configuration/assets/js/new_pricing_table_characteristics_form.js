(function ($, root, undefined) {

    window.delete_pricing_characteristics_col = function(element){

        var data = $(element).attr('data');

        var col = $(element).parent().parent().parent();

        if(data == ''){
            col.remove();
            return;
        }
        else{

            data = {
                'id' : data,
                'action' : 'delete_pricing_characteristic'
            }

            console.log($(this), $(element).closest('tr').find('input'));

            //$(element).closest('tr')
            $(element).closest('tr').find('input').attr('disabled', 'disabled');
            $(element).closest('tr').find('input').prop('disabled', 'disabled');

            $.post(ajaxurl, data, function(response) {

                if(response == 1)
                    col.remove();
                else
                    alert('Failed!');

            });



        }
    }

    $(document).ready(function(){


        $('select[name=pricing_table_switcher]').change(function(){

            var val = $(this).val();

            if(val){
                $('.pricing_table').hide();
                $('.pricing_table.pricing_table_' + val).show();
                $('select[name=pricing_table_switcher] option[value=' + val + ']').prop('selected', 'true');
            }

        })


        var i = parseInt($('input[name=entities_num]').val());


        $('.add_pricing_table_characteristic').click(function(){

            i++;

            var data = $('input[name=json_price_entity_data]').val();
            data = JSON.parse(data);

            //console.log(data);

            var tr = '<tr class="edd_variable_prices_wrapper edd_repeatable_row add_pricing_table_characteristic_form">';

            var tds = '<td><input type="text" value="" name="pricing_table_characteristics[' + i + '][title]" placeholder="Enter your Characteristic Title here" style="width:100%"/></td>'

            for( j in data ){
                tds += '<td align="center"><input type="checkbox" name="pricing_table_characteristics[' + i + '][checkboxes][' + j + ']"></td>';
            }

            tds += '<td><span class="trash"><a href="javascript:void(0)" onclick="delete_pricing_characteristics_col(this)" class="delete_characteristics_link" data="">Delete</a></span></td>';

            var content = tr + tds + '</tr>';

            $(content).insertBefore($('.pricing_table_sharacteristics_body'));

            return false;

        })


        $('.delete_characteristics_link').click(function(){

            delete_pricing_characteristics_col(this);

        })

    })

})(jQuery, this);

