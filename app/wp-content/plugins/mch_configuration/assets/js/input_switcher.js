(function ($, root, undefined) {

//    jQuery(document).ready(function($) {
//        var image_field;
//        $(document).on('click', '.upload_image_test', function(evt){
//            image_field = $(this).siblings('.img');
//            tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
//            return false;
//        });
//        window.send_to_editor = function(html) {
//            imgurl = $('img', html).attr('src');
//            image_field.val(imgurl);
//            $('.image').attr('src', imgurl);
//            tb_remove();
//        }
//    });

    function show_template_metabox(val){

        $('.templates_metaboxes').hide();
        $('.hidden_active_template_data').val('false');

        switch(val){
            case 'template-normal-format-final-test.php':
                $('#normal-format-final-test').show();
                $('#normal-format-final-test').find('.hidden_active_template_data').val('true');
                break;
            case 'template-normal-format-introduction.php':
                $('#normal-format-introduction').show();
                $('#normal-format-introduction').find('.hidden_active_template_data').val('true');
                break;
            case 'template-normal-format.php':
                $('#normal-format').show();
                $('#normal-format').find('.hidden_active_template_data').val('true');
                break;
            case 'template-normal-format-end-of-course-exam.php':
                $('#normal-format-end-of-course-exam').show();
                $('#normal-format-end-of-course-exam').find('.hidden_active_template_data').val('true');
                break;
            case 'template-primary-school-course-format.php':
                $('#primary-school-course-format').show();
                $('#primary-school-course-format').find('.hidden_active_template_data').val('true');
                break;
            case 'template-primary-school-course-introduction-page.php':
                $('#primary-school-course-introduction').show();
                $('#primary-school-course-introduction').find('.hidden_active_template_data').val('true');
                break;
            case 'template-primary-school-final-test.php':
                $('#primary-school-final-test').show();
                $('#primary-school-final-test').find('.hidden_active_template_data').val('true');
                break;

        }

    }

    window.delete_word = function(element){

        var col = $(element).parent().parent();
        col.remove();

    }

    window.delete_file = function(element){


        var col = $(element).closest('tr');

        var data = $(element).attr('data');

        col.css('opacity', '0.3');


        if(data){

            data = {
                'id' : data,
                'action' : 'delete_files'
            }

            $.post(ajaxurl, data, function(response) {

                if(response == 1)
                    col.remove();

            });
        }
        else col.remove();

    }

    $(document).ready(function(){

        $('.delete_word_link').click(function(){
            delete_word(this);
        });

        $('.templates_metaboxes').hide();
        $('.hidden_active_template_data').val('false');

        var temp_val = $('select[name=wpcw_units_choose_template_list]').val();

        if(temp_val){
            show_template_metabox(temp_val);
        }

        $('select[name=wpcw_units_choose_template_list]').change(function(){
            var val = $(this).val();

            show_template_metabox(val)

        });

        $('.delete_file_link').click(function(){
            delete_file(this)
        });

        $('button.add-new-file').click(function(){

            var data = $(this).attr('data');
            var countries_select = courseData.countries_select.replace('%name', 'unit_templates[' + data + '][file_country][]" style="width:100%;"');
            var column = '<tr>';
            column += '<td><input type="text" value="" name="unit_templates[' + data + '][file_title][]" style="width:100%;"/></td>';
            column += '<td><input type="file" name="unit_templates[' + data + '][file][]" style="width:100%;"/></td>';
            column += '<td>' + countries_select + '</td>';
            column += '<td valign="middle"><a href="javascript:void(0)" onclick="delete_file(this);" class="delete_file_link">Delete</a></td>';
            column += '</tr>';

            var selector = $(this).closest('.inside').find('.table-files');
            //console.log(selector)
            selector.append(column)

            return false;

        });

        $('button.add-new-word').click(function(){

            var data = $(this).attr('data');

            var column = '<tr>';
            column+= '<td><input type="text" name="words[' + data + '][english][]" style="width:100%;"/></td>';
            column+= '<td><input type="text" name="words[' + data + '][pinyin][]" style="width:100%;"/></td>';
            column+= '<td><input type="text" name="words[' + data + '][chinese][]" style="width:100%;"/></td>';
            column+= '<td><input type="text" name="words[' + data + '][url][]" style="width:100%;"/></td>';
            column+= '<td><a href="javascript:void(0)" onclick="delete_word(this)" class="delete_word_link">Delete</a></td>';
            column+= '</tr>';

            var selector = $(this).closest('.inside').find('.table-words');
            //console.log(selector)
            selector.append(column);

            return false;

        })




    })

})(jQuery, this);
