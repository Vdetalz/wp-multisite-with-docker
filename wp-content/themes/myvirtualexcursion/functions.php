<?php
/* SAME THING FOR STYLES */

function myvirtualexcursion_styles() {
    // deregister parent style
    wp_dequeue_style( 'screen' );
    wp_deregister_style( 'screen' );

    // register child theme
    wp_register_style('screen', get_stylesheet_directory_uri() . '/stylesheets/screen.css', array(), '1.0', 'all');
    wp_enqueue_style('screen'); // Enqueue it!
}

add_action('wp_enqueue_scripts','myvirtualexcursion_styles', 20);

