<?php get_header(); ?>

	<div role="main" class="content-wrapper">
		<!-- section -->
		<section>

			<h1><?php _e( 'Categories for ', 'mc_teacher' ); single_cat_title(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
