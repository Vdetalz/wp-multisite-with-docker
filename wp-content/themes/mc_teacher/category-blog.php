<?php wp_redirect(get_home_url()); get_header(); ?>

<div role="main" class="content-wrapper">
    <h1><?php _e('Blog'); ?></h1>

    <!-- section -->
    <section>

        <!-- subcribe-form -->
        <div class="subscribe-wrapp">
            <?php
            echo do_shortcode('[subscribe2 hide="unsubscribe"]');
            ?>
        </div>
        <!-- /subcribe-form -->

        <!-- before-posts -->
        <div class="before-posts">
            <?php echo category_description( get_category_by_slug('blog')->term_id ); ?>
        </div>
        <!-- /before-posts -->

        <?php get_template_part('blog-loop'); ?>

        <?php get_template_part('pagination'); ?>

    </section>
    <!-- /section -->

</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
