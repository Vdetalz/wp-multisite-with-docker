<?php get_header(); ?>

<div role="main" class="single-p-d main">
    <!-- section -->
    <section>

        <div class="content-wrapper">
            <!-- article -->
            <article>

                <h1><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h1>

            </article>
            <!-- /article -->
        </div>

    </section>
    <!-- /section -->
</div>

<?php get_footer(); ?>
