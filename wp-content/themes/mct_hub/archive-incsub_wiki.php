<?php get_header(); ?>

<div role="main" class="content-wiki">
    <div class="main-content content-wrapper">
        <!-- section -->
        <section>

            <?php if ( current_user_can('edit_wiki_privileges')): ?>

                <?php dynamic_sidebar('widget-area-wiki') ?>

            <?php else: ?>
                <div class="error-msg"><h2><?php _e('Please login to view Wiki.', 'mch'); ?></h2></div>
            <?php endif; ?>

        </section>
        <!-- /section -->
    </div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
