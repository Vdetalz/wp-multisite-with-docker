<?php get_header(); ?>

    <div role="main" class="main-content content-wrapper">
		<!-- section -->
		<section>

			<h1><?php _e( 'Tag Archive: ', 'mct_hub' ); echo single_tag_title('', false); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
