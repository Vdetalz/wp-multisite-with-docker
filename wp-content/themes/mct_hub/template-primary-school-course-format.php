<?php
/**
 * Unit Template Name: Primary School Course Format
 */

global $post;

if(isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])){
    $post_id = $_GET['actual_lesson'];
    $download_id = $_GET['download_number'];
    $course_id = $_GET['level_grade'];
    $module_id = $_GET['module_number'];
    mct_last_lesson_write($download_id, $course_id, $module_id, $post_id);
}
else{
    $post_id = $post->ID;
    get_header();
}

$post = get_post($post_id);
/**
 * Get User Info
 */

$user = wp_get_current_user();
get_currentuserinfo();

/**
 * Get Custom data of template
 */

$data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
$files = get_attached_media('', $post->ID);
$interesting_links = get_post_meta($post->ID, 'unit_template_interesting_links', true);

/**
 *
 * VIEW OF THE TEMPLATE START
 *
 */

if(is_user_logged_in()) :?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="video-description">
            <?php _e($post->post_content);?>
        </div>

        <?php if($data):?>

            <div class="wrap-box-video">
            <?php if($data['pr_video_name'] && $data['pr_video_link']):?>
                <div class="box-video">
                    <h4><?php echo $data['pr_video_name'];?></h4>
                    <?php if(mch_user_has_access($user->ID, 'vocab_video')) :?>
                        <div><a href="<?php echo $data['pr_video_link'];?>" data-mce-href="<?php echo $data['pr_video_link'];?>" class="popup-youtube"><img src="<?php echo get_template_directory_uri();?>/img/player.png"/></a></div>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                </div>
            <?php endif;?>

            <?php if($data['lr_video_name'] && $data['lr_video_link']):?>
                <div class="box-video">
                    <h4><?php echo $data['lr_video_name'];?></h4>
                    <?php if(mch_user_has_access($user->ID, 'vocab_video')) :?>
                        <div><a href="<?php echo $data['lr_video_link'];?>" data-mce-href="<?php echo $data['lr_video_link'];?>" class="popup-youtube"><img src="<?php echo get_template_directory_uri();?>/img/player.png"/></a></div>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                </div>
            <?php endif;?>
            </div>

            <?php if($files):?>
                <div class="wrap-downloads">
                    <h2><?php _e('DOWNLOADS');?></h2>
                    <?php if(mch_user_has_access($user->ID, 'files')):?>
                        <ul>
                            <?php foreach($files as $file):?>
                                <li><a href="<?php echo $file->guid;?>" target="_blank"><?php _e($file->post_title);?></a></li>
                            <?php endforeach;?>
                        </ul>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                </div>
            <?php endif;?>

            <?php if(!empty($interesting_links)):?>
                <div class="interesting-links">
                    <h2><?php _e('INTERESTING LINKS');?></h2>
                    <?php if(mch_user_has_access($user->ID, 'links')):?>
                        <?php echo $interesting_links; ?>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                </div>
            <?php endif;?>

            <div class="box-quiz">
                <h2><?php _e('QUICK QUIZ');?></h2>
                <?php if($data['quiz_id']):?>
                    <?php if(mch_user_has_access($user->ID, 'quiz')):?>
                        <?php do_shortcode("[mch_show_quiz post_id=$post->ID]");?>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                <?php else:?>
                    <p class="community-section-description" style="margin-top:42px;"><?php _e('A quiz is currently not provided for this lesson plan.');?></p>
                <?php endif;?>
            </div>

            <h1 class="community-section-title" id="community_section"><?php _e('Community Section');?></h1>
            <p class="community-section-description">
                <?php _e('This is where you can share with other school teachers how you have extended this lesson with students at your school. Share your great ideas with them!');?>
            </p>
            <?php if(mch_user_has_access($user->ID, 'community')): ?>
                <?php
                if( function_exists('mch_comment_form') ){
                    mch_comment_form($post_id);
                }

                if( function_exists('mch_get_list_comments') ){
                    mch_get_list_comments($post_id);
                }
                ?>
            <?php else: 
                echo upgrade_subscription();?>
            <?php endif;?>

        <?php endif;?>

    </article>

<?php else:?>

    <!-- article -->
    <article>

        <h2><?php _e('You have no access on this page.');?></h2>

    </article>
    <!-- /article -->

<?php endif;

if(!isset($_GET['actual_lesson']) || empty($_GET['actual_lesson'])){
    get_footer();
}

/**
 *
 * VIEW OF THE TEMPLATE END
 *
 */