<?php
/**
 * Unit Template Name: Normal Format - Final Test
 */

global $post;

if(isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])){
    $post_id = $_GET['actual_lesson'];
    $download_id = $_GET['download_number'];
    $course_id = $_GET['level_grade'];
    $module_id = $_GET['module_number'];
    mct_last_lesson_write($download_id, $course_id, $module_id, $post_id);
}
else{
    $post_id = $post->ID;
    get_header();
}

$post = get_post($post_id);
/**
 * Get User Info
 */

$user = wp_get_current_user();

get_currentuserinfo();

/**
 * Get Custom data of template
 */

$data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));

/**
 *
 * VIEW OF THE TEMPLATE START
 *
 */

if(is_user_logged_in()) :?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if($data):?>

            <?php if(!empty($data['quiz_url'])):?>


                <?php _e($post->post_content);?>


                <div class="wrap-graduation">
                <?php if(mch_user_has_access($user->ID, 'end_test')):?>
                    <i class="fa fa-graduation-cap"></i>
                    <a class="e-btn" href="<?php echo $data['quiz_url'];?>"><?php _e('Take the online test');?></a>
                <?php else: ?>
                    <h2><?php _e('TAKE THE ONLINE TEST');?></h2>
                    <?php  echo upgrade_subscription();?>
                <?php endif;?>
                </div>
            <?php endif;?>

        <?php endif;?>

    </article>

<?php else:?>

    <!-- article -->
    <article>

        <h2><?php _e('You have no access on this page.');?></h2>

    </article>
    <!-- /article -->

<?php endif;

if(!isset($_GET['actual_lesson']) || empty($_GET['actual_lesson'])){
    get_footer();
}

/**
 *
 * VIEW OF THE TEMPLATE END
 *
 */