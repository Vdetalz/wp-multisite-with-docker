<?php get_header(); ?>

    <div role="main" class="main-content content-wrapper">
		<!-- section -->
		<section>

			<h1><?php _e( 'Archives', 'mct_hub' ); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
