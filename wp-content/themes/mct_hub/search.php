<?php get_header(); ?>

    <div role="main" class="main-content content-wrapper">
		<!-- section -->
		<section>

			<h1><?php echo sprintf( __( '%s Search Results for ', 'mct_hub' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
