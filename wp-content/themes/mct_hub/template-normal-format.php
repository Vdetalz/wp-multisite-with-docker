<?php
/**
 * Unit Template Name: Normal Format
 */

global $post;

if(isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])){
    $post_id = $_GET['actual_lesson'];
    $download_id = $_GET['download_number'];
    $course_id = $_GET['level_grade'];
    $module_id = $_GET['module_number'];
    mct_last_lesson_write($download_id, $course_id, $module_id, $post_id);
}
else{
    $post_id = $post->ID;
    get_header();
}

$post = get_post($post_id);
/**
 * Get User Info
 */

$user = wp_get_current_user();

get_currentuserinfo();

/**
 * Get Custom data of template
 */

$data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
$words = get_post_meta($post->ID, 'vocab_list', true);
$quiz_id = $data['quiz_id'];
$dialogue = get_post_meta($post->ID, 'unit_template_dialogue', 'true');
$written_explanations = get_post_meta($post->ID, 'unit_template_written_explanations', 'true');

/**
 *
 * VIEW OF THE TEMPLATE START
 *
 */



if(is_user_logged_in()) :?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if($data):?>

            <?php if(!empty($data['video_name']) && !empty($data['video_link'])):?>
                <div class="video-explain">
                    <h3><?php echo __('Saying Hello: Formal and Informal'); ?></h3>
                    <?php if(mch_user_has_access($user->ID, 'video_explainer')):?>
                        <h4><?php echo $data['video_name'];?></h4>
                        <div><a href="<?php echo $data['video_link'];?>" data-mce-href="<?php echo $data['video_link'];?>" class="popup-youtube"><img src="<?php echo get_template_directory_uri();?>/img/player.png"/></a></div>
                    <?php else: 
                        echo upgrade_subscription();?>
                    <?php endif;?>
                </div>
            <?php endif;?>

            <div class="explain-lesson">

                <div class="box-explain-lesson">
                    <?php if(!empty($dialogue)):?>
                        <div class="section-explain-lesson">
                            <h4><?php _e('Dialogue');?></h4>
                            <?php if(mch_user_has_access($user->ID, 'dialogues')):?>
                                <div class="content-explain-lesson content-dialogue">
                                    <?php echo $dialogue;?>
                                </div>
                            <?php else: 
                                echo upgrade_subscription();?>
                            <?php endif;?>
                        </div>
                    <?php endif;?>

                    <?php if(!empty($written_explanations)):?>
                        <div class="section-explain-lesson">
                            <h4><?php _e('Written Explanation');?></h4>
                            <?php if(mch_user_has_access($user->ID, 'explanation')):?> 
                                <div class="content-explain-lesson">
                                    <?php echo $written_explanations;?>
                                </div>
                            <?php else: 
                                echo upgrade_subscription();?>
                            <?php endif;?>
                        </div>
                    <?php endif;?>
                </div>

                <?php if($words):?>
                    <div class="box-explain-lesson">
                        <div class="section-explain-lesson">
                            <h4><?php _e('Module Learning Vocab List');?></h4>
                            <?php if(mch_user_has_access($user->ID, 'vocabulary')):?>
                                <table class="box-translate">
                                    <tr>
                                        <th>English</th>
                                        <th>Pinyin</th>
                                        <th>Chinese</th>
                                    </tr>
                                    <?php foreach($words as $word):?>
                                        <tr>
                                           <td>
												<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[0]);?></a>
												<?php else:?>
													<?php _e($word[0]);?>
												<?php endif;?>		
											</td>
                                            <td>
											<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[1]);?></a>
												<?php else:?>
													<?php _e($word[1]);?>
												<?php endif;?>	
											</td>
                                            <td>
												<?php if(!empty($word[3])):?>
													<a href="<?php echo $word[3];?>" data-mce-href="<?php echo $word[3];?>" class="popup-vimeo"><?php _e($word[2]);?></a>
												<?php else:?>
													<?php _e($word[2]);?>
												<?php endif;?>		
											</td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            <?php else: 
                                echo upgrade_subscription();?>
                            <?php endif;?>
                        </div>
                    </div>
                <?php endif;?>
                

            </div>

            
                <div class="box-quiz">
                    <h2><?php _e('QUICK QUIZ');?></h2>
                    <?php if($data['quiz_id']):?>
                        <?php if(mch_user_has_access($user->ID, 'quiz')):?>
                            <?php do_shortcode("[mch_show_quiz quiz_id=$quiz_id post_id=$post->ID]");?>
                        <?php else: 
                            echo upgrade_subscription();?>
                        <?php endif;?>
                    <?php else:?>
                        <p class="community-section-description" style="margin-top:42px;"><?php _e('A quiz is currently not provided for this lesson plan.');?></p>
                    <?php endif;?>
                </div>
            

        <?php endif;?>

    </article>

<?php else:?>

    <!-- article -->
    <article>

        <h2><?php _e('You have no access on this page.');?></h2>

    </article>
    <!-- /article -->

<?php endif;

if(!isset($_GET['actual_lesson']) || empty($_GET['actual_lesson'])){
    get_footer();
}

/**
 *
 * VIEW OF THE TEMPLATE END
 *
 */