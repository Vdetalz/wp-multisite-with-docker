(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

        // change courses description on digital products
        $('.courses-list').on('change', function(){
            var id= $(this).val();
            $('.course-description').html($('.description-' + id).html());
        });

        // Custom select
        var config = {
            'select'            : {disable_search_threshold:10}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $(document).ajaxSuccess(function() {
            $( "select" ).chosen(config[selector]);

        });
        /*** chosen pricing  */
        $('select[name=pricing_table_switcher]').change(function() {
            $('select[name=pricing_table_switcher] option[value=' + $(this).val() + ']').prop('selected', 'true');
            $("select[name=pricing_table_switcher]").trigger("chosen:updated");
        });

        var $w = $(window),
            $headerContentWrapper = $('#header .content-wrapper'),
            $rightNav = $('#header .right-menu'),
            $mainNav = $('#header .main-menu .menu'),
            $mobileNav = $('<div />', {
                'class' : 'b-mobile-nav'
            });


        function toggleMobNav(isMobile) {
            if(isMobile) {
                $mainNav.wrap($mobileNav);
                $('#header').find('.b-mobile-nav').append($rightNav);
                $('.menu-item-has-children .sub-menu').slideUp();
                $('.nav-btn').show();
            } else {
                $rightNav.appendTo($headerContentWrapper);
                $mainNav.unwrap();
                $('.menu-item-has-children .sub-menu').slideDown();
                $('.nav .nav-btn').removeClass('active');
                $('.nav .nav-btn').hide();
            }
        }

        if ($(window).width() < 1000) {
            toggleMobNav(true);
        }

        $w.resize(function() {
            if ($(window).width() < 1000) {
                if ($('#header').find('.b-mobile-nav').length === 0) {
                    toggleMobNav(true);
                }
            } else {
                if ($('#header').find('.b-mobile-nav').length > 0) {
                    toggleMobNav(false);
                }
            }
        });

        /**  event to show main nav on mobile*/
        $('.nav .nav-btn').on('click', function(){
            $(this).toggleClass('active');
            $(this).next('.b-mobile-nav').toggleClass('is-open');
        });

        $('body').on('click', '.b-mobile-nav .menu-item-has-children a', mobileNavEvents());

        function mobileNavEvents() {
            return function (e) {
                
                if (!$(this).parents('.menu-item-has-children:first').hasClass('active')) {
                    e.preventDefault();
                    $(".sub-menu .sub-menu").slideUp();
                    $('.menu-item-has-children').removeClass('active');
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                } else {
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                }
                
            }
        }

        /** Double tap on logo */
        $( '#header .item-logo').on('touchstart', function(e) {
            e.stopPropagation();
            if(!$(this).next('ul').hasClass('is-open')) {
                e.preventDefault();
                if($('.b-mobile-nav').hasClass('is-open')) {
                    $('.nav .nav-btn').trigger('click');
                }
                $(this).next('ul').addClass('is-open');

            }
        });
        $('html').on('touchstart', function() {
            $( '#header .item-logo').next('ul').removeClass('is-open');
        });

        /** Explore courses */
        $('.js-goToThis').click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop : $($(this).attr('href')).offset().top - 100
            }, 300);
        });


	});




    $(document).ready(function(){

        var temp_text = $('#input_copy_link').val();
        /* Использование плагина только с двумя обязательными опциями */
        $("#btn_copy_link").zclip({
            path:site_url + '/js/ZeroClipboard.swf',
            copy:$('#input_copy_link').val(),
            beforeCopy:function(){
            },
            afterCopy:function(){
                $('#input_copy_link').val('Your link is copied.');
                setTimeout(function(){
                    $('#input_copy_link').val(temp_text);
                }, 2000)
            }
        });


        var temp_text_first = $('#input_copy_link_first').val();

        $("#btn_copy_link_first").zclip({
            path:site_url + '/js/ZeroClipboard.swf',
            copy:$('#input_copy_link_first').val(),
            beforeCopy:function(){
            },
            afterCopy:function(){
                $('#input_copy_link_first').val('Your link is copied.');
                setTimeout(function(){
                    $('#input_copy_link_first').val(temp_text_first);
                }, 2000)
            }
        });

        var temp_text_second = $('#input_copy_link_second').val();

        $("#btn_copy_link_second").zclip({
            path:site_url + '/js/ZeroClipboard.swf',
            copy:$('#input_copy_link_second').val(),
            beforeCopy:function(){
            },
            afterCopy:function(){
                $('#input_copy_link_second').val('Your link is copied.');
                setTimeout(function(){
                    $('#input_copy_link_second').val(temp_text_second);
                }, 2000)
            }
        });
        //
        ///* Использование плагина с пустыми функциями обратного отклика */
        //$(".no-feedback").zclip({
        //    path:'js/ZeroClipboard.swf',
        //    copy:$('.no-feedback').text(),
        //    beforeCopy:function(){},
        //    afterCopy:function(){}
        //});
        //
        ///* Использование плагина с функциями обратного отклика */
        //$(".feedback").zclip({
        //    path:'js/ZeroClipboard.swf',
        //    copy:$('.feedback').text(),
        //    beforeCopy:function(){
        //        $(this).css('background','yellow');
        //        $(this).css('color','orange');
        //    },
        //    afterCopy:function(){
        //        alert("Текст скопирован в буфер обмена!");
        //        alert("И мы изменим цвет фона и текста :)");
        //        $(this).css('background','green');
        //        $(this).css('color','white');
        //    }
        //});
    var errorHeight = $(".affwp-errors").outerHeight() + 30;
    $('.affiliate-area .status-publish').css("padding-top", errorHeight);

    });

})(jQuery, this);