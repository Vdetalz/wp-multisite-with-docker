jQuery(document).ready(function($) {

    var media = $(".media-icons");
    var scrollDown = true;
    var mediaInterval;

    mediaSlickInit();
    mediaHeightInit();
    goAutomaticallyScroll();

    media.on("mouseenter", function(){
        stopAutomaticallyScroll();
    });
    media.on("mouseleave", function(){
        goAutomaticallyScroll();
    });




    function mediaSlickInit() {
        $(".js-slick-init").find(".media-icons").slick ({
            slidesToShow: 5,
            arrows: false,
            infinite: false,
            edgeFriction: 0.1,
            responsive: [
            {
              breakpoint: 999,
              settings: {
                slidesToShow: 4,
              }
            }
          ]
        });
    };


    // custom scroll on media section
        //calculation of height media section
        function mediaHeightInit() {
            var mediaScrollElement = $(".js-custom-scroll").find(".media-icons");
            mediaScrollElement.height(mediaHeightCalculation());
            mediaCustomScroll(mediaScrollElement);
        }

        function mediaHeightCalculation() {
            var calculationHeight = 0;
            var top = parseInt($(".js-custom-scroll").css("top"));
            var endOfEach;

            $(".js-custom-scroll").find("li").each(function(index){
                if((index <= 3) && (parseInt($(".js-custom-scroll").height()) + top <= parseInt($(".digital-products-list-big").height()))) {
                    calculationHeight = calculationHeight + parseInt($(this).height()) + parseInt($(this).css("margin-bottom"));
                }
                else {
                    endOfEach = true;
                }
            });
            return calculationHeight;
        }

        //working of custom scroll
        function mediaCustomScroll(mediaScrollElement) {
            mediaScrollElement.mCustomScrollbar({
                theme:"light-thick",
                scrollbarPosition:"outside",
                callbacks:{
                    onScroll:function(){
                      if( parseInt(media.find(".mCSB_container").css('top')) < scrollPosition ){
                          scrollDown = true;
                      }
                      else {
                          scrollDown = false;
                      }
                      scrollPosition = parseInt(media.find(".mCSB_container").css('top'));
                    }
                }
            });
        }


        //automaticaly scroll
        var scrollPosition = parseInt(media.find(".mCSB_container").css('top'));
        function goAutomaticallyScroll() {
            if ((media.height() < media.find(".mCSB_container").height()) && (innerWidth > 1199)) {
                mediaInterval = setInterval(automaticallyScroll, 60);
            }
        }

        function stopAutomaticallyScroll() {
            if (media.height() < media.find(".mCSB_container").height()) {
                clearInterval(mediaInterval);
                mediaInterval = false;
            }
        }

        function automaticallyScroll() {
            if(scrollDown && ((media.height() - scrollPosition) < media.find(".mCSB_container").height())) {
                scrollPosition--;
            }
            else if(scrollDown && ((media.height() - scrollPosition) >= media.find(".mCSB_container").height())) {
                scrollDown = false;
                scrollPosition++;
            }
            else if((!scrollDown) && (scrollPosition < 0)) {
                scrollPosition++;
            }
            else {
                scrollDown = true;
                scrollPosition--;
            }
            media.find(".mCSB_container").css("top", (scrollPosition + "px"));
        }


        $( window ).on("resize", function(){
            mediaHeightInit();
            //switch on and switch off automatically scroll on resize
            if((innerWidth > 1199) && (!mediaInterval)) {
                goAutomaticallyScroll();
            }
            else if((innerWidth <= 1199) && (mediaInterval)) {
                clearInterval(mediaInterval);
                mediaInterval = false;
            };
        });
});

