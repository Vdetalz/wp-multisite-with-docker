<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">
            <div class="inner-wrapper">
                <!-- header -->
                <header id="header" class="header clear" role="banner">

                    <div class="content-wrapper">
                        <!-- logo -->
                        <div class="logo">
                            <?php  $sites = wp_get_sites();?>
                            <ul>
                                <li>
	                                <a class="item-logo" href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" ><?php echo get_bloginfo( 'name' ); ?></a>
                                    <?php if( !empty($sites) ):?>
                                        <ul>
                                            <?php foreach( $sites as $site ): ?>
                                                <?php
                                                switch_to_blog( $site['blog_id'] );
                                                $site_title = get_bloginfo( 'name' );
                                                $site_url = get_bloginfo( 'url' );
                                                restore_current_blog();
                                                ?>
                                                <?php if( get_current_blog_id() <> $site['blog_id'] ):?>
                                                    <li><a href="<?php echo $site_url; ?>" rel="nofollow"><?php echo $site_title; ?></a></li>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                </li>
                            </ul>
                        </div>
                        <!-- /logo -->

                        <!-- nav -->

                        <?php if(is_user_logged_in()) {?>
                            &nbsp;<nav class="nav main-menu log-in" role="navigation">
                                <span class="nav-btn">Open</span>
                                <?php wp_nav_menu(array('theme_location' => 'header-menu-loginned', 'walker' => new c_Walker_Nav_Menu())); ?>
                            </nav>&nbsp;
                            <div class="right-menu log-in">
                                <?php wp_nav_menu( array( 'theme_location' => 'right-header-menu-loginned', 'walker' => new c_Walker_Nav_Menu() ) ); ?>
                            </div>
                        <?php }
                        else { ?>
                            &nbsp;<nav class="nav main-menu" role="navigation">
                                <span class="nav-btn">Open</span>
                                <?php wp_nav_menu(array('theme_location' => 'header-menu-unloginned')); ?>
                            </nav>&nbsp;
                            <div class="right-menu">
                                <?php wp_nav_menu( array( 'theme_location' => 'right-header-menu-unloginned' ) ); ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>

                </header>
                <!-- /header -->
