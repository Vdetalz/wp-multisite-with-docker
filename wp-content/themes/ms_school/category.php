<?php get_header(); ?>

<?php $user_id = get_current_user_id(); $user = new WP_User( $user_id ); ?>

<?php if ( ! in_array( 'administrator', $user->roles ) && ! in_array( 'ss_users', $user->roles ) && ! in_array( 'editor', $user->roles ) ) : ?>

<div role="main" class="content-wrapper">
    <div class="content-header">
        <h1>
            <?php _e('Blog'); ?>
        </h1>
    </div>
    <section>
        <?php _e( 'You have no access on this page.' ); ?>
    </section>
</div>

<?php else: mss_category_wp_query(); // change global $wp_query. ?>

<div role="main" class="content-wrapper">
    <div class="content-header">
        <h1>
            <?php _e('Blog'); ?>
        </h1>
        <!--Create post Link-->
        <?php if ( get_option( 'create_post_page' ) ): ?>
            <div class="create-post">
                <a href="<?php echo esc_url( get_permalink( get_option( 'create_post_page' ) ) ); ?>" class="create-post-link"><?php _e('Create post'); ?></a>
            </div>
        <?php endif; ?>
        <!-- /Create post Link-->
    </div>


    <!-- section -->
    <section>

        <!-- subcribe-form -->
        <div class="subscribe-wrapp">
            <?php
            echo do_shortcode('[mss_subscribe2_shortcode_extended]');
            ?>
        </div>

        <?php if ( function_exists( 'mss_get_user_categories' ) ) : ?>
            <?php if ( $categories = mss_get_user_categories() ) : ?>
                <div class="post-category">
                    <b><?php _e( 'Filter posts by:' ); ?></b>
                    <ul>
                        <?php foreach ( $categories as $category ) : $category = get_term( $category, 'category' ); ?>
                            <li>
                                <a <?php echo $category->term_id == $cat ? 'class="active"' : ''; ?> href="<?php echo esc_url( get_term_link( $category ) ); ?>"><?php _e( $category->name ); ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <!-- /subcribe-form -->

        <!-- before-posts -->
        <div class="before-posts">
            <?php echo category_description( get_category_by_slug('blog')->term_id ); ?>
        </div>
        <!-- /before-posts -->

        <?php if ( ! is_null( $wp_query ) ) : ?>
            <?php get_template_part('blog-loop'); ?>
            <?php get_template_part('pagination'); ?>
        <?php else : ?>
            <article>
                <h2><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h2>
            </article>
        <?php endif; ?>

    </section>
    <!-- /section -->

</div>

<?php endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
