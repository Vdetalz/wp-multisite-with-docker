<?php get_header();?>

<div role="main" class="single-p-d main">
    <!-- section -->
    <section>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <!-- post thumbnail -->
                <div class="bg-page">
                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                    <?php endif; ?>
                </div>
                <!-- /post thumbnail -->

                <div class="content-wrapper">

                    <div class="content-header">
                        <!-- post title -->
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                        <!-- /post title -->
                    </div>
                    <div>

                        <?php the_content();?>

                    </div>
                </div>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <div class="content-wrapper">
                <!-- article -->
                <article>

                    <h1><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h1>

                </article>
                <!-- /article -->
            </div>

        <?php endif; ?>

    </section>
    <!-- /section -->
</div>

<?php get_footer(); ?>
