<?php global $post; ?>
<?php get_header(); ?>
<?php unset($_SESSION['post_create']); ?>
	<div role="main" class="content-wrapper">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="content-header">

                <!-- post title -->
                <h1>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </h1>
                <!-- /post title -->

                <div class="create-post">
                    <?php if ( $post->post_author == get_current_user_id() || current_user_can( 'edit_posts' ) ) : ?>
                        <span class="edit">
					<a class="e-btn" href="<?php echo esc_url( add_query_arg( array( 'post_id' => $post->ID ), get_permalink( get_option( 'create_post_page' ) ) ) );?>"><?php _e('Edit Post'); ?></a></span>
                    <?php endif; ?>
                    <span class="edit">
                        <?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Comment on this post', 'mc_teacher' ), __( '1 Comment', 'mc_teacher' ), __( '% Comments', 'mc_teacher' ), 'e-btn'); ?>
                    </span>
                </div>

            </div>


			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                <div class="post-image">
                <a class="post-image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                </a>
                </div>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post details -->
			<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
			<span class="author"><?php _e( 'Published by', 'mc_teacher' ); ?> <?php echo mss_user_publish_string( $post->post_author ); ?></span>


            <div class="content-wrap">
			    <?php echo do_shortcode(get_the_content()); // Dynamic Content ?>
            </div>

            <div id="comments">
            <h2><?php _e( 'Comment on this post' ); ?></h2>
                <?php
                if ( function_exists( 'mch_comment_form' ) )
                    mch_comment_form( get_the_ID() );

                if ( function_exists( 'mch_get_list_comments' ) )
                    mch_get_list_comments( get_the_ID() );
                ?>
            </div>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'mc_teacher' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
