(function ($, root, undefined) {

	$(function () {
		'use strict';
		// DOM ready, take it away

        var iframeInterval = setInterval(function() {
            var head = $("#post_content_ifr, #comment_ifr");
            if (head.contents().find("head").length > 0) {

                if (head[0].id == 'comment_ifr') {
                    var ifrHeight = '65px';
                } else {
                    var ifrHeight = '210px';
                }

            var css = '<style type="text/css">' +
                    'html{cursor:text;} body{min-height:' + ifrHeight + '}  ' +
                    '</style>';
            jQuery(head).contents().find("head").append(css);
                clearInterval(iframeInterval)
            }
        }, 50);


        $('.testimonials-load-more-link').live('click', function(){
            var el = $(this);

            if(el.hasClass('testimonials-load-wait')) return;
            el.addClass('testimonials-load-wait');
            jQuery.ajax({
                url: mc_ajax['url'],
                type: 'POST',
                data: {
                    action: 'testimonials_load_more',
                    offset: el.attr('data-offset'),
                    limit: el.attr('data-limit')
                },
                success: function(html){
                    if(html == 0){
                        el.hide();
                        return;
                    }
                    else{
                        el.attr('class', 'testimonials-load-more-link');
                        el.attr('data-offset', parseInt(el.attr('data-offset')) + parseInt(el.attr('data-limit')));
                        el.before(html)
                    }

                }
            });

            return false;

        })

        // change courses description on digital products
        $('.courses-list').on('change', function(){
            var id= $(this).val();
            $('.course-description').html($('.description-' + id).html());
        });

        $('.popup-with-form').magnificPopup({
            type: 'inline',
            midClick: true,
        });

        $(".contact-me-form").validate({
            rules: {
                first_name: {
                    required: true, maxlength: 25
                },
                last_name: {
                    required: true, maxlength: 25
                },
                school: {
                    maxlength: 50
                },
                town: {
                    maxlength: 50
                },
                z_email: {
                    required: true, email: true
                },
                phone_number: {
                    maxlength: 25
                }
            },
            submitHandler: function (form) {
                var values;
                values = $(".contact-me-form").serialize();
                $(".contact-me-form .form-wrapper .input-wraper").hide();
                $(".contact-me-form .message").css('color','#aaaaaa');
                $(".contact-me-form .message").html("The message is processed. Please wait...");
                $.ajax({
                    type: "post",
                    url: mc_ajax.url + "/?action=contact_me_send",
                    data: values,
                    success: function (response) {
                        if (response == "1") {
                            $(".contact-me-form .message").css('color','#008000');
                            $(".contact-me-form .message").html("Your message has been sent. We will contact you shortly.");
                        } else {
                            $(".contact-me-form .message").css('color','#ff0000');
                            $(".contact-me-form .message").html("Your message hasn't been sent. Please try again later.");
                        }
                    }
                });
            }
        });

        function pricing_table_switch(val){
            $('.pricing_table').hide();
            $('.pricing_table_' + val).show();
            $('select[name=pricint_table_select] option[value=' + val + ']').prop('selected', 'true');
        }

        var id = $('select[name=pricint_table_select]').val();
        pricing_table_switch(id);

        $('select[name=pricint_table_select]').change(function(){
            pricing_table_switch($(this).val());
        })

        var config = {
            'select'            : {disable_search_threshold:10}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        /*** chosen pricing  */
        $('select[name=pricint_table_select]').change(function() {
            $('select[name=pricint_table_select] option[value=' + $(this).val() + ']').prop('selected', 'true');
            $("select[name=pricint_table_select]").trigger("chosen:updated");
        });


        $('input').blur(function() {
            if($(this).val().length != 0) {
                $(this).addClass('filled');
            }
        });

        $('textarea ').blur(function() {
            if($(this).val().length != 0) {
                $(this).addClass('filled');
            }
        });

        var $w = $(window),
            $headerContentWrapper = $('#header .content-wrapper'),
            $rightNav = $('#header .right-menu'),
            $mainNav = $('#header .main-menu .menu'),
            $mobileNav = $('<div />', {
                'class' : 'b-mobile-nav'
            });


        function toggleMobNav(isMobile) {
            if(isMobile) {
                $mainNav.wrap($mobileNav);
                $('#header').find('.b-mobile-nav').append($rightNav);
                $('.menu-item-has-children .sub-menu').slideUp();
                $('.nav-btn').show();
            } else {
                $rightNav.appendTo($headerContentWrapper);
                $mainNav.unwrap();
                $('.menu-item-has-children .sub-menu').slideDown();
                $('.nav .nav-btn').removeClass('active');
                $('.nav .nav-btn').hide();
            }
        }

        /*function mobileNavEvents() {
            return function () {
                $(this).toggleClass('active');
                $(this).find('ul').slideToggle();
            }
        }*/

        if ($(window).width() < 1000) {
            toggleMobNav(true);
        }

        $w.resize(function() {
            if ($(window).width() < 1000) {
                if ($('#header').find('.b-mobile-nav').length === 0) {
                    toggleMobNav(true);
                }
            } else {
                if ($('#header').find('.b-mobile-nav').length > 0) {
                    toggleMobNav(false);
                }
            }
        });

        /**  event to show main nav on mobile*/
        $('.nav .nav-btn').on('click', function(){
            $(this).toggleClass('active');
            $(this).next('.b-mobile-nav').toggleClass('is-open');
        });
        $('body').on('click', '.b-mobile-nav .menu-item-has-children a', mobileNavEvents());

        function mobileNavEvents() {
            return function (e) {

                if (!$(this).parents('.menu-item-has-children:first').hasClass('active')) {
                    e.preventDefault();
                    $(".sub-menu .sub-menu").slideUp();
                    $('.menu-item-has-children').removeClass('active');
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                } else {
                    $(this).parents('.menu-item-has-children:first').toggleClass('active');
                    $(this).siblings(".sub-menu").slideToggle();
                }

            }
        }

        /** Double tap on logo */
        $( '#header .item-logo').on('touchstart', function(e) {
            e.stopPropagation();
            if(!$(this).next('ul').hasClass('is-open')) {
                e.preventDefault();
                if($('.b-mobile-nav').hasClass('is-open')) {
                    $('.nav .nav-btn').trigger('click');
                }
                $(this).next('ul').addClass('is-open');

            }
        });
        $('html').on('touchstart', function() {
            $( '#header .item-logo').next('ul').removeClass('is-open');
        });

        /**     Explore courses */
        $('.js-goToThis').click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop : $($(this).attr('href')).offset().top - 100
            }, 300);
        });

        $("#s2email").on('change', function(e) {
            var email = $('#s2email').val();
            if(isValidEmailAddress(email)) {
                jQuery.ajax({
                    url: mc_ajax['url'],
                    type: 'POST',
                    async: false,
                    data: {
                        action: 'is_already_subscribed',
                        email: email
                    },
                    success: function (response) {
                        if(response === '1' || response === '0' ) {
                            $('.subscribe-wrapp :submit').replaceWith('<input name="unsubscribe" value="Unsubscribe" type="submit">');
                        }
                        else{
                            $('.subscribe-wrapp :submit').replaceWith('<input name="subscribe" value="Subscribe" type="submit">');
                        }
                    }
                });
            }
        });
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };

    $(window).on('load resize', function(){
        matchHeightInit();
    });

    function matchHeightInit(){
        jQuery('.item-content').matchHeight();
        jQuery('.item-content-title').matchHeight();
    }

})(jQuery, this);
