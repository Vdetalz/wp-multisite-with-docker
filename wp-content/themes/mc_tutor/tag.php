<?php get_header(); ?>

	<section role="main" class="main-content content-wrapper">
		<!-- section -->
		<section>

			<h1><?php _e( 'Tag Archive: ', 'mct_hub' ); echo single_tag_title('', false); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
