            </div>

            <!-- footer -->
            <footer class="footer" role="contentinfo">
                <div class="content-wrapper">

                    <div class="footer-widget">
                            <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-footer')) ?>
                    </div>

                    <div class="footer-nav">
                            <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-footer-2')) ?>
                    </div>
                    <!-- copyright -->
                    <p class="copyright">
	                    <?php echo str_replace('[year]', date('Y'), get_option('mc_footer_text')); ?>
                    </p>
                    <!-- /copyright -->
                </div>
            </footer>
            <!-- /footer -->
		</div>
		<!-- /wrapper -->
		<?php wp_footer(); ?>
	</body>
</html>
