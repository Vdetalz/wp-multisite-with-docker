<?php
/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('329_329', 329, 329, true);


    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('mct_hub', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/


// Load scripts (header.php)
function mct_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('chosen', get_template_directory_uri() . '/js/chosen.jquery.min.js', array('jquery'), '1.3.0'); // Custom scripts
        wp_enqueue_script('chosen'); // Enqueue it!

        wp_register_script('browser', get_template_directory_uri() . '/js/browser.min.js', array('jquery'), '1.3.0'); // Custom scripts
        wp_enqueue_script('browser'); // Enqueue it!

        wp_register_script('mct_scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('mct_scripts'); // Enqueue it!

        wp_register_script('slick', get_template_directory_uri() . '/js/lib/slick.min.js', array('jquery'), '1.0.0'); // Slick slider library
        wp_enqueue_script('slick'); // Slick it!

        wp_register_script('customScroll', get_template_directory_uri() . '/js/lib/jquery.mCustomScrollbar.concat.min.js', array('jquery'), '1.0.0'); // custom scrollbar library
        wp_enqueue_script('customScroll'); // custom scroll it!
        
        wp_register_script('scrollMagic', get_template_directory_uri() . '/js/lib/scrollmagic.min.js', array('jquery'), '1.0.0'); // scroll magic library
        wp_enqueue_script('scrollMagic'); // custom scroll it!

        wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0'); // my scripts
        wp_enqueue_script('custom'); // my scripts!


        if(is_user_logged_in()){
            $user_id = get_current_user_id();
            if(get_user_meta($user_id, 'tutor_trial_lesson', true)){
                wp_localize_script( 'mct_scripts', 'mct',
                    array(
                        'is_free'   => 1,
                    )
                );
            }
        }
    }
}

// Load  conditional scripts
function mct_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load  styles
function mct_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('mct_hub', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('mct_hub'); // Enqueue it!

	//wp_register_style('screen', get_template_directory_uri() . '/stylesheets/screen.css', array(), '1.0', 'all');
	//wp_enqueue_style('screen'); // Enqueue it!

    /***************************************/
    wp_register_style('mct_css_popup', get_template_directory_uri() . '/magnific-popup.css', array(), '1.0', 'all');
    wp_enqueue_style('mct_css_popup'); // Enqueue it!

}

// Register  Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'right-header-menu-loginned' => __(' Right Header Menu Loginned', 'mct_hub'), // Main Navigation,
        'header-menu-loginned' => __('Header Menu Loginned', 'mct_hub'), // Main Navigation,
        'header-menu-unloginned' => __('Header Menu Unloginned', 'mct_hub'), // Main Navigation
        'right-header-menu-unloginned' => __('Right Header Menu Unloginned', 'mct_hub'), // Main Navigation
        'header-menu' => __('Header Menu', 'mct_hub'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'mct_hub'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'mct_hub'), // Extra Navigation if needed (duplicate as many as you need!)
        'right-header-menu' => __('Right Header Menu', 'mct_hub'), // Extra Navigation if needed (duplicate as many as you need!)
		'footer-menu' => __('Footer Menu', 'mc_teacher'), // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
            'name' => __('Widget Area 1', 'mct_hub'),
            'description' => __('Description for this widget-area...', 'mct_hub'),
            'id' => 'widget-area-1',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
            'name' => __('Widget Area 2', 'mct_hub'),
            'description' => __('Description for this widget-area...', 'mct_hub'),
            'id' => 'widget-area-2',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

    	// Define Sidebar Widget Area 2
    register_sidebar(array(
            'name' => __('Footer Widget', 'mct_hub'),
            'description' => __('Widgets in footer', 'mct_hub'),
            'id' => 'widget-footer',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

	// Define Sidebar Widget Area 3
	register_sidebar(array(
		'name' => __('Footer Widget 2', 'mct_hub'),
		'description' => __('Widgets in footer', 'mct_hub'),
		'id' => 'widget-footer-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages
        ));
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function mct_comments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
    <div class="comment-author vcard">
        <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
        <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
    <?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
            <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
        <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'mct_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'mct_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'mct_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add  Menu
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Change login form titles
add_filter( 'tml_title', 'login_form_titles', 0, 2 );

/*
 * tml_title filter callback
 */
function login_form_titles( $title, $action ){
    switch ( $action ) {
        case 'lostpassword':
        case 'retrievepassword':
        case 'resetpass':
        case 'rp':
            $title = __( 'Forgot Your Password?' );
            break;
    }
    return $title;
}

/*
 * Display promotion block in the registration form.
 */
add_action('after_signup_form', 'display_promotion_block');

function display_promotion_block(){
    ?>
    <div class="wrap-content-registration">
        <div class="inner-wrap-content-registration">
            <div class="content-registration">
                <?php if(has_post_thumbnail()) {
                        the_post_thumbnail(array(560, 350));
                     }
                    else {
                        the_content();
                    }?>
            </div>
        </div>
    </div>
<?php }

/*
 * Remove promotion block from the confirm registration message.
 */
add_action('signup_finished', 'remove_promotion_block');

function remove_promotion_block(){
    remove_action('after_signup_form', 'display_promotion_block');
    ?>
    <span class="img-mail"><?php _e('Send mail'); ?><span>
<?php }

/**
 * Ajax function to get calendar of tutor for tutors page
 */
add_action('wp_ajax_get_tutor_calendar', 'get_tutor_calendar');
add_action('wp_ajax_nopriv_get_tutor_calendar', 'get_tutor_calendar');

function get_tutor_calendar() {
    $args = '';
    if(!empty($_POST['author_id'])) {
        $author_id = $_POST['author_id'];
        $args .= "author_id='$author_id' ";
    }
    if(!empty($_POST['day'])) {
        $day = $_POST['day'];
        $args .= "day='$day' ";
    }
    if(!empty($_POST['month'])) {
        $month = $_POST['month'];
        $args .= "month='$month' ";
    }
    if(!empty($_POST['year'])) {
        $year = $_POST['year'];
        $args .= "year='$year'";
    }

    echo do_shortcode("[mct_booking_calendar $args]");
    die;
}

/**
 * Add booking
 */
add_action('wp_ajax_add_booking', 'add_booking');
add_action('wp_ajax_nopriv_add_booking', 'add_booking');

function add_booking() {
    $event_id = $_POST['event_id'];
    $user_id = $_POST['student_id'] != '' ? $_POST['student_id'] : get_current_user_id();
    $comment = $_POST['comment'] === __('Add your comment here') ? '' : htmlentities($_POST['comment']);
    $communication_type = $_POST['communication_type'];

    if($_POST['communication_name'] !== '' && $communication_type === 'skype') {
        update_user_meta($user_id, 'skype_name', $_POST['communication_name']);
    } elseif($_POST['communication_name'] !== '' && $communication_type === 'polycom') {
        update_user_meta($user_id, 'polycom_name', $_POST['communication_name']);
    }

    $user_balance = get_user_balance($user_id);
    $EM_Event = new EM_Event($event_id);

    $time_plus_12h = time() + 60*60*12;
    $event_start_time = strtotime("$EM_Event->event_start_date $EM_Event->event_start_time");
    if($time_plus_12h > $event_start_time) {
        echo "Lesson unavailable";
        die;
    }

    $EM_Location = em_get_location($EM_Event->location_id);
    $tutor_lesson_type = get_field('tutor-lesson-type', $EM_Location->post_id) == 60 ? 1 : 0.5;
    if($tutor_lesson_type > $user_balance) { ?>
        <div class="no_balance">
            <div class="tutor-message-box">
                <?php _e('Sorry, you have run out of lesson hours!', 'mct_tutor'); ?>
                <?php _e('Please subscribe to more hours to book more lessons', 'mct_tutor'); ?>
            </div>

            <a href="<?php echo get_permalink(edd_get_option('mct_edd_pricing_page_redirect')); ?>" class="e-btn m-btn-orange">See subscription options</a>
        </div>
        <?php die;
    }

    if($EM_Event->get_bookings()->get_available_spaces()) {
        $EM_Booking = em_get_booking(array('event_id' => $EM_Event->event_id, 'booking_spaces' => 1, 'booking_comment' => $comment));
        $EM_Booking->person_id = $user_id;
        $EM_Booking->booking_meta['communication_type'] = $communication_type;
        $EM_Ticket = $EM_Event->get_bookings()->get_tickets()->get_first();
        $EM_Ticket_Booking = new EM_Ticket_Booking(array('ticket_id' => $EM_Ticket->ticket_id, 'ticket_booking_spaces' => 1));
        $EM_Booking->tickets_bookings = new EM_Tickets_Bookings();
        $EM_Booking->tickets_bookings->booking = $EM_Ticket_Booking->booking = $EM_Booking;
        $EM_Booking->tickets_bookings->add($EM_Ticket_Booking);
        echo $EM_Event->get_bookings()->add($EM_Booking) ? __('Booking confirmed! Enjoy your lesson!', 'mct_tutor') : __('Booking confirm failed', 'mct_tutor');
    } else {
        echo __('Already booked', 'mct_tutor');
    }
    die;
}

//  Shortcode courses list of current project description
add_shortcode('courses_list', 'courses_list');

/*
 * Callback for courses_list shortcode
 */
function courses_list( $atts ){
    $courses = array();
    extract( shortcode_atts( array(
                'id' => null,
            ), $atts ) );

    if( empty( $id ) ){
        $id = get_the_ID();
    }

    if( !empty( $id ) ){
        $courses = get_field('related_courses', $id);
    }

    ob_start();
    ?>
    <?php if ( !empty( $courses ) ) : ?>
        <select class="courses-list">
            <?php foreach( $courses as $course ) :  ?>
                <option value="<?php echo $course->ID;?>"><?php echo get_the_title( $course->ID );?></option>
            <?php endforeach;?>
        </select>
        <div class="course-description">
            <?php echo apply_filters('the_content', $courses[0]->post_content);?>
        </div>
        <?php foreach( $courses as $course ) :  ?>
        <div class="description-<?php echo $course->ID;?> display-none">
            <?php echo apply_filters('the_content', $course->post_content);?>
        </div>
        <?php endforeach;?>
    <?php endif;

    return ob_get_clean();
}

add_shortcode('digital_products_descriptions', 'digital_products_descriptions');

/*
 * Shortcode with digital products description posts
 */
function digital_products_descriptions( $atts ){
    extract( shortcode_atts( array(
                'count' => 3,
            ), $atts ) );

    // get posts of "digital products description" content type
    $args = array(
        'post_type' => 'product-description',
        'posts_per_page' => $count
    );

    $descriptions_query = new WP_Query( $args );

    ob_start();
?>
<?php if ( $descriptions_query->have_posts() ) : ?>
        <ul class="digital-products-list" id="<?php echo $atts['id'];?>">
    <?php while ( $descriptions_query->have_posts() ) : $descriptions_query->the_post(); ?>
            <li>
                <a href="<?php the_permalink();?>">
                    <?php the_post_thumbnail('329_329');?>
	                <span class="title"><?php the_title();?></span>
                </a>
            </li>
    <?php  endwhile;?>
        </ul>
<?php endif;

    return ob_get_clean();
}


add_shortcode('explore_courses', 'explore_courses');

function explore_courses($atts){
    if(wp_is_mobile()){
        $atts['title'] = __('Book $1 Demo Lesson', 'mct');
        return book_lesson($atts);
    }
    if(!$atts['title']){
        $atts['title'] = __('Explore courses', 'mct');
    }

    return "<a class='e-btn m-shadow js-goToThis' href='#" . $atts['target'] . "'>" . $atts['title'] . "</a>";

}


add_shortcode('book_lesson', 'book_lesson');

function book_lesson($atts){
    if(!isset($atts['title'])) {
        $atts['title'] = __('Book a free lesson!', 'mct_tutor');
    }
    ob_start();
    ?>
    <a href="<?php
    if(is_user_logged_in()) {
        $atts['title'] = __('Book a lesson', 'mct_tutor');
        echo get_permalink(edd_get_option('mct_edd_tutors_page_redirect'));
    } else {
        echo get_permalink(Theme_My_Login::get_page_id('register'));
    }
    ?>" class="e-btn"><?php echo $atts['title']; ?></a>
    <?php
    return ob_get_clean();
}


class t_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     */

    function start_el(&$output, $item, $depth, $args) {

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        //Get user object
        $user = wp_get_current_user();
        $roles = $user->roles;
        $nickname = get_user_meta($user->ID, 'nickname', true);

        /**
         * *********************** START MENU LOGIC **************************
         */

        //Tutor Portal
        if( in_array('menu-tutor-portal-link', $classes) ){

            if( in_array('author', $roles) ){
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            }
            else{
                return;
            }

        //Tutors account
        } else if( in_array('menu-tutors-account-link', $classes) ){

            if( in_array('administrator', $roles) ){
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            }
            else{
                return;
            }

        //User Account
        } else if( in_array('menu-user-info-link', $classes) ){

            if( !in_array('author', $roles) ) {
                $output .= $indent . '<li' . $id . $value . $class_names . '><a class="user-info" href="' . get_page_link( edd_get_option('mct_edd_subscriber_login_redirect', true) ) . '">' . get_avatar($user->ID) . $nickname . '</a> ';
            }
            else{
                $output .= $indent . '<li' . $id . $value . $class_names . '><a class="user-info" href="' . get_page_link( edd_get_option('mct_edd_author_login_redirect', true) ) . '">' . get_avatar($user->ID) . $nickname . '</a> ';
            }

        } else if( in_array('menu-lesson-history-link', $item->classes) ) {
            if (current_user_can('read_private_pages')) {
                $output .= $indent . '<li' . $id . $value . $class_names . '>';
            } else {
                return;
            }
        //Tutor Portal
        } else{
            $output .= $indent . '<li' . $id . $value . $class_names . '>';
        }

        /**
         * ************************ END OF MENU LOGIC *******************************
         */

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';


        $item_output = $args->before;

        if( in_array('menu-user-info-link', $classes) ) {
            $item_output .= '<a class="e-btn-transparent"'. $attributes .'>';
        }
        else{
            $item_output .= '<a '. $attributes .'>';
        }

        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}


// Assigns customize_text_sizes() to "tiny_mce_before_init" filter
function customize_text_sizes($initArray){
    $initArray['fontsize_formats'] = "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 41px 42px 43px 44px 45px 46px 47px 48px 49px 50px 52px 56px 60px 65px 72px";
    return $initArray;
}

add_filter('tiny_mce_before_init', 'customize_text_sizes');

//Check if the user browser is IE 8
function is_IE8 (){
    $browserInfo = $_SERVER['HTTP_USER_AGENT'];

//    if(isset($browserInfo['browser']) && $browserInfo['browser']=='IE' && $browserInfo['majorver'] == '8')
//        return true;
//    return false;
}



add_filter('login_redirect', 'mychinese_login_redirect', 10, 3);
/*
 * Redirects authors and subscribers after login
 */
function mychinese_login_redirect($redirect_to, $request, $user){
    global $wpdb;
    if (isset($user->roles) && is_array($user->roles)) {
        switch_to_blog(5);
        $is_student = FALSE;
        if (in_array('students', get_userdata($user->ID)->roles)) {
            $is_student = TRUE;
        }
        $results = $wpdb->get_var( "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name = 'edd_settings'");
        $results = maybe_unserialize($results);
        $course_page_id = $results['mch_course_page_redirect'];
        $page_url = get_permalink($course_page_id);
        restore_current_blog();
        if(!$page_url){
            $page_url = home_url();
        }
        if ($is_student) {
            wp_redirect($page_url);
            exit;
        } elseif (in_array('author', $user->roles)) {
            return get_permalink(edd_get_option('mct_edd_author_login_redirect'));
        } elseif (in_array('administrator', $user->roles)) {
            return admin_url();
        } elseif (in_array('subscriber', $user->roles)) {
            return get_permalink(edd_get_option('mct_edd_subscriber_login_redirect'));
        } else {
            return $redirect_to;
        }
    }
}

add_action('the_post', 'mct_login_redirect', 10, 1);
/*
 * Redirects user from login page.
 */
function mct_login_redirect($page){
    if(is_page(edd_get_option('mch_login_page'))){
        $user = get_userdata(get_current_user_id());
        if (isset($user->roles) && is_array($user->roles)) {
            if(in_array('subscriber', $user->roles)){
                wp_redirect(get_permalink(edd_get_option('mct_edd_subscriber_login_redirect')));
            } elseif (in_array('author', $user->roles)){
                 wp_redirect(get_permalink(edd_get_option('mct_edd_author_login_redirect')));
            } elseif (in_array('administrator', $user->roles)){
                 wp_redirect(admin_url());
            } else {
                 wp_redirect(home_url());
            }
        }
    }
}

/**
 * *********************************** BOOKINGS FOR MOBILE **********************************************
 */

function mct_append_bookings_mobile_scripts(){

    global $post;

    if($post->post_type != 'location') return;

    if(!is_user_logged_in()) wp_redirect(get_permalink(224));

    wp_register_script('mct_mobile_accordeon', get_template_directory_uri() . '/js/mct_mobile_accordeon.js');
    wp_enqueue_script('mct_mobile_accordeon');

}

add_action('wp', 'mct_append_bookings_mobile_scripts');


add_filter( 'wp_mail_from', 'mct_wp_mail_from' );

function mct_wp_mail_from( $original_email_address )
{
    return get_option('admin_email');
}


function wp_mail_from_name($name) {
    return __('My Chinese Tutor', 'mct');
}

add_filter('wp_mail_from_name','wp_mail_from_name');


add_action('wp_head', 'add_description_tags_to_pricing_page');
/*
 * Add description meta tag
 */
function add_description_tags_to_pricing_page(){
    if(is_page(edd_get_option('mct_edd_pricing_page_redirect'))){
        $path = $_SERVER['REQUEST_URI'];
        $path_parts = explode('update_download_id=', $path);
        if($path_parts[1]){
            $post_id = $path_parts[1];
            $description = get_post_meta($post_id, '_aioseop_description', true);
        } else {
            $description = get_post_meta(edd_get_option('mct_edd_pricing_page_redirect'), '_aioseop_description', true);
        }
        echo '<meta name="description" content ="' . $description . '">';
    }
}

add_action( 'wp_title', 'add_meta_tags_to_pricing_page');
/*
 * Change pricing page title if isset get parameter
 */
function add_meta_tags_to_pricing_page(){
    if(is_page(edd_get_option('mct_edd_pricing_page_redirect'))){
        $path = $_SERVER['REQUEST_URI'];
        $path_parts = explode('update_download_id=', $path);
        if($path_parts[1]){
            $post_id = $path_parts[1];
            $title = get_post_meta($post_id, '_aioseop_title', true);
            return $title;
        }else{
            return get_post_meta(edd_get_option('mct_edd_pricing_page_redirect'), '_aioseop_title', true);
        }
    }
}

add_action('admin_menu', 'add_book_tutor_emails_field');
/*
 * Add book tutor emails field to the general admin page.
 */
function add_book_tutor_emails_field(){
    $option_name = 'book_tutor_emails';
    register_setting('general', $option_name);

    add_settings_field(
        'book_tutor_emails_id',
        __('Book tutor e-mails'),
        'book_tutor_emails_function',
        'general',
        'default',
        array(
            'id' => 'book_tutor_emails_id',
            'option_name' => 'book_tutor_emails',
        )
    );
}

/*
 * Book tutor callback function.
 */
function book_tutor_emails_function( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <textarea
        name = "<?php echo $option_name ?>"
        id = "<?php echo $id ?>"
        rows = "5"
        cols = "30"
        wrap="hard"
    ><?php echo esc_attr( get_option($option_name) ) ?></textarea>
    <p class="description"><?php _e('These addresses are used for receiving book tutor mails if no tutors is available. Enter each address on a new line.');?></p>
    <?php
}

/*
 * Tutors filter.
 */
function tutors_filter(){
    global $wpdb;
    $events_table = $wpdb->prefix . 'em_events';
    $location_table = $wpdb->prefix . 'em_locations';
    $no_tutors = false;
    $regexp_time = '/^[0-9]{1,2}:[0-9]{2}$/';
    $regexp_date = '/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/';
    if(is_user_logged_in() && isset($_REQUEST['tutor-date']) && preg_match($regexp_date, $_REQUEST['tutor-date']) && isset($_REQUEST['tutor-time']) && preg_match($regexp_time, $_REQUEST['tutor-time'])){
        $raw_start_date_time = $_REQUEST['tutor-date'] . ' ' . $_REQUEST['tutor-time'] . ':00';
        $raw_start_date_time_timestamp = strtotime($raw_start_date_time);
        $start_date_time = $raw_start_date_time_timestamp - dut_get_user_timezone() * 60 * 60;
        $start_date = date('Y-m-d', $start_date_time);
        $start_time = date('H:i:s', $start_date_time);
        if(time() + 60 * 60 * 12 < $start_date_time){
            $query = $wpdb->prepare("SELECT event_owner, event_id FROM $events_table WHERE event_start_time = %s AND event_start_date = %s AND event_status = 1", $start_time, $start_date);
            $event_results = $wpdb->get_results($query);
            $location_id = array();
            foreach ($event_results as $value) {
                $em_event = new EM_Event($value->event_id);
                if($em_event->get_bookings()->get_available_spaces()){
                    $query = $wpdb->prepare("SELECT post_id FROM $location_table WHERE location_owner = %s", $value->event_owner);
                    $location_post = $wpdb->get_row($query);
                    $location_id[] = $location_post->post_id;
                }
            }
            if(!$location_id){
                $no_tutors = true;
            }else {
                return $location_id;
            }
        } else {
            $no_tutors = true;
        }

        //Looks for the closest tutors ( +/-12 hours ) and sends e-mails to tutors
        if($no_tutors): ?>
            <p class="no-tutors"><?php _e('Sorry, but no tutors are available at this time however see below a couple of tutors with times close to what you are after.'
                    . ' We will make sure someone is available at this time in the future.', 'mct'); ?></p>
            <?php
            $emails = get_option('book_tutor_emails');
            $raw_emails = explode("\n", $emails);
            $to = array();
            foreach ($raw_emails as $email) {
                $to[] = trim($email);
            }
            $subject = __('No tutor available at the requested day and time on My Chinese Tutor', 'mct');
            $start_date_time_gmt8 = $start_date_time + 8 * 60 * 60;
            $message =  __('User: ', 'mct') . get_user_meta(get_current_user_id(), 'nickname', true) . "\n" .
                        __('Date: ', 'mct') .  date('d/m/Y', $start_date_time_gmt8) . "\n" .
                        __('Day: ', 'mct') . date('l', $start_date_time_gmt8) . "\n" .
                        __('Time: ', 'mct') . date('H:i', $start_date_time_gmt8) . __(' GMT+8', 'mct');
            wp_mail($to, $subject, $message);

            // +/-12 hours
            for($i = 1, $j = -1; $i <= 12 * 2; $i++, $j--){
                $additionals = array($i, $j);
                foreach ($additionals as $add_hour){
                    $start_date_time_with_add = $start_date_time + $add_hour/2 * 60 * 60;
                    $start_date = date('Y-m-d', $start_date_time_with_add);
                    $start_time = date('H:i:s', $start_date_time_with_add);

                    $query = $wpdb->prepare("SELECT event_owner, event_id FROM $events_table WHERE event_start_time = %s "
                        . "AND event_start_date = %s AND event_status = 1", $start_time, $start_date);
                    $event_results = $wpdb->get_results($query);
                    $location_id = array();
                    foreach ($event_results as $value) {
                        $em_event = new EM_Event($value->event_id);
                        if($em_event->get_bookings()->get_available_spaces()){
                            $query = $wpdb->prepare("SELECT post_id FROM $location_table WHERE location_owner = %s", $value->event_owner);
                            $location_post = $wpdb->get_row($query);
                            $location_id[] = $location_post->post_id;
                        }
                    }
                    if($location_id){
                        return array_slice($location_id, 0, 2);
                        break(2);
                    }
                }
            }
        endif;
    }
}

/*
 * Get teachers, sorted by available lessons
 */
function get_sorted_teachers(){
    global $wpdb;
    $events_table = $wpdb->prefix . 'em_events';
    $location_table = $wpdb->prefix . 'em_locations';
    $locations_sql = $wpdb->prepare("SELECT EL.post_id FROM $location_table AS EL LEFT JOIN $events_table AS EV ON EV.event_owner = EL.location_owner
                              WHERE CONCAT(EV.event_start_date, ' ', EV.event_start_time ) >= (NOW() + INTERVAL 12 HOUR) AND EV.event_status = %d
                              GROUP BY EV.event_owner ORDER BY COUNT(*) DESC", 1);

    $all_locations_sql = $wpdb->prepare("SELECT EL.post_id FROM $location_table AS EL WHERE EL.location_status = %d", 1);

    $locations_id_array = $wpdb->get_results($locations_sql);

    $all_locations_array = $wpdb->get_results($all_locations_sql);
    $location_id = array();
    foreach($locations_id_array as $id){
        $location_id[] = $id->post_id;
    }

    foreach($all_locations_array as $location){
        if(!in_array($location->post_id, $location_id)){
            $location_id[] = $location->post_id;
        }
    }

    return $location_id;
}

// function accordion2() {
//     wp_register_script( 'accordion2', get_template_directory_uri() . '/js/accordion.js', array(), 'all' );
//     wp_enqueue_script( 'accordion2' );
// }

function main() {
    wp_register_script( 'main', get_template_directory_uri() . '/js/main.js', array(), 'all' );
    wp_enqueue_script( 'main' );
}
//add_action( 'wp_enqueue_scripts', 'accordion2' );
add_action( 'wp_enqueue_scripts', 'main' );

// Save event-manager' phone number into wp-sms-pro number.
function add_phone_wp_sms() {
	if( is_plugin_active( 'wp-sms-pro/wp-sms.php' ) && current_user_can('author') ){
		$id = get_current_user_id();
		$right_code = '+86';
		$phone = get_user_meta( $id, 'dbem_phone', TRUE );
		$code = mb_substr( $phone, 0, 3, 'utf-8' );

		if( $right_code === $code && strlen( $phone ) === 14 ) {
			update_user_meta( $id, 'rpr_phone', $phone );
		} else {
			$phone_rpr = get_user_meta( $id, 'rpr_phone', TRUE );

			if( $right_code !== $code && strlen( $phone_rpr ) !== 14 ) {
				delete_user_meta( $id, 'rpr_phone', $phone_rpr );
			}
			delete_user_meta( $id, 'dbem_phone', $phone );
		}
	}
}
add_action( 'profile_update', 'add_phone_wp_sms' );

// Set ERROR message.
function mct_admin_notices_action() {
	if( is_plugin_active( 'wp-sms-pro/wp-sms.php' ) && current_user_can('author') && is_admin() ){
		$id = get_current_user_id();
		$phone = get_user_meta( $id, 'dbem_phone', TRUE );
		$phone_rpr = get_user_meta( $id, 'rpr_phone', TRUE);
		if( empty($phone) || empty($phone_rpr) ){
			add_settings_error( 'error_phone', 'settings_updated', __( 'ERROR: Please check your phone number, the format +86...' ), 'error' );
			settings_errors( 'error_phone' );
		}
	}
}
add_action( 'admin_notices', 'mct_admin_notices_action' );

/**
 * Exclude " ' " from school name.
 */
function check_school_user_meta( $meta_id, $object_key, $meta_key, $meta_value ) {
    if ( 'school' == $meta_key && ! empty( $meta_value ) ) {
        $meta_value = str_replace( '\'', '', $meta_value );
        update_user_meta( $object_key, 'school', $meta_value );
    }
}
add_action('updated_user_meta', 'check_school_user_meta', 10, 4);

/**
 * Get users subscribers for the tutor page when logged as super admin.
 *
 * @return mixed
 */
function get_mct_tutor_users() {
    global $wpdb;
    $users    = $wpdb->get_results(
    "SELECT * FROM $wpdb->users
     INNER JOIN $wpdb->usermeta
     ON ( $wpdb->users.ID = $wpdb->usermeta.user_id )
     WHERE 1=1
     AND ( ( $wpdb->usermeta.meta_key = 'ch_capabilities'
     AND CAST( $wpdb->usermeta.meta_value AS CHAR ) LIKE '%subscriber%' ) )
     ORDER BY $wpdb->users.ID" );

    return $users;
}
  