//accordion
jQuery(document).ready(function($) {
    //initAccordion();
		fadeMenu();
		closeAccordion();

    // function initAccordion() {
    //     $('div.user-history-wrapper').slideAccordion({
    //         opener: 'div.he-header',
    //         slider: 'div.he-body',
    //         animSpeed: 300
    //     });
    // }


    function fadeMenu() {
		$('.he-header').each(function(){
			$(this).click(function(){
				$(this).css('display','none').siblings('.he-body').slideDown(300);
			})
		});
	}
    function closeAccordion() {
        $('.he-info-close').on('click', function(){
            $(this).parent().slideUp(300);
        })
    }
});
