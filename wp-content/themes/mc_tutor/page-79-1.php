<?php global $am_option; get_header(); ?>
    <?php /*?>
    <div class="modal_wrap" id="welcomes_popap">
        <div class="modal_body add_margin_top">
            <div class="modal_close"></div>
            <div class="modal_form_wrap_welcome">
                <p>Congratulations! You have now registered with My Chinese Tutor!</p>
                <p>It is time to take advantage of your Free Trial Lesson: simply select a tutor from the list and book a time!</p>
                <p>Don't want to book now? No problems, return another time to the Tutors page to book your Free Trial Lesson.</p>
                <div class="modal_button_section"><a href="" class="modal_button defaultButton">Great, I'm ready to book a tutor now!</a></div>
            </div>
        </div>
    </div>
    <?php */?>
    <!-- welcomes  -->
    <div class="content_wrap">
    <div class="content_big_inner">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <!-- Page content -->
        <div class="content_text">
            <?php the_content(); ?>
        </div>
        <?php
            global $wpdb;
            $catID = 0;
            if (is_page('our-tutors')) {
                $catID=3;
            } else {
                $catID=9;
            }
            if ($catID) {
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $posts = query_posts("cat=$catID&paged=$paged&posts_per_page=-1");

                for ($i = 0; $i<count($posts); $i++){
                    $sql = "SELECT `uid`, `lid` FROM  wp_dbem_relationship WHERE `pid`=%d";
                    $results = $wpdb->get_results( $wpdb->prepare($sql, $posts[$i]->ID),ARRAY_A );

                    $start_period = date( 'Y-m-d', gmmktime());
                    $current_month = date( 'm', gmmktime());
                    $current_day = date( 'd', gmmktime());
                    $current_year = date( 'Y', gmmktime());
                    $end_period = mktime(0,0,0,$current_month, $current_day + 7, $current_year);
                    $end_period = date('Y-m-d', $end_period);

                    $sql_all = "SELECT COUNT(event_id) FROM `wp_dbem_events` WHERE `event_author`=%d AND `event_start_date` BETWEEN '".$start_period."' AND '".$end_period."'";
                    $all = $wpdb->get_results($wpdb->prepare($sql_all,$results[0]['uid']), ARRAY_A );
                    $sql_bookings = "SELECT COUNT(e.event_id) as bookings_lessons FROM `wp_dbem_bookings` b LEFT JOIN `wp_dbem_events` e ON b.event_id=e.event_id WHERE e.event_author=%d AND `event_start_date` BETWEEN '".$start_period."' AND '".$end_period."'";
                    $bookings = $wpdb->get_results( $wpdb->prepare($sql_bookings ,$results[0]['uid']), ARRAY_A);

                    $avaliable_lessons = $all[0]['COUNT(event_id)'] - $bookings[0]['bookings_lessons'];
                    $result[] = array('post' => $posts[$i], 'avaliable' => $avaliable_lessons, 'lid' => $results[0]['lid']);
                }
                $order_posts = array_sorting_key_another($result, 'avaliable');

                $bookerSkype = get_user_meta($current_user->ID, 'what_is_your_skype_name', true);
                $bookerPolycom = get_user_meta($current_user->ID, 'polycom_number', true);
                $user_datetime = strtotime($_COOKIE['user_date'] . " " . $_COOKIE['user_time']);
                    // The Loop
                ?>
                <!-- EXPERT PANEL -->
                <div class="expert_panel_wrap clearfix">
                <?php
                if($order_posts){
                foreach($order_posts as $post): ?>
                    <?php
                    $types[$post['post']->ID] = array();
                    $location = eme_get_location($post['lid']);
                    $lesson_type = get_user_lesson_type($post['lid']);
                    if ($lesson_type == 2){
                        $length  = '25';
                    }
                    else{
                        $length  = '50';
                    }

                    if (!empty($location['location_private'])){
                        $types[$post['post']->ID][] =  'Private';
                    }
                    if (!empty($location['location_classroom'])){
                        $types[$post['post']->ID][] =  'Classroom';
                    }

                    if($location['location_communication'] == 1){
                        $lesson_via[$post['post']->ID][] = 'Skype';
                        $lesson_via[$post['post']->ID][] = 'Polycom';
                    }
                    else{
                        $lesson_via[$post['post']->ID][] = 'Skype';
                    }
                    ?>
                    <div class="expert_wrap">
                        <div class="expert_photo">
                            <?php  echo get_the_post_thumbnail($post['post']->ID,array(211,257)); ?>
                        </div>
                        <div class="expert_info">
                            <div class="expert_title title_teacher" ><?php echo $post['post']->post_title; ?></div>
                            <div class="expert_text">
                                <?php $content = strip_tags($post['post']->post_content);?>
                                <?php $content = strip_shortcodes($content); ?>
                                <?php echo substr($content, 0, 100);?><?php if (strlen($content) > 100) echo '...';?>
                            </div>
                            <div class="expert_tags_wrap">
                                <div class="expert_tag">
                                    Lesson length: <span><?php echo $length;?> mins</span>
                                </div>
                                <div class="expert_tag">
                                    Lesson via:  <span class="blue"><?php echo implode(' </span> and <span class="blue">', $lesson_via[$post['post']->ID]);?></span>
                                </div>
                                <div class="expert_tag">
                                    Teaches:  <span class="blue"><?php echo implode(' </span> and <span class="blue">', $types[$post['post']->ID]);?></span>
                                </div>
                            </div>
                            <?php
                            $class = '';
                            if(is_user_logged_in()){
                                if(is_free($current_user->ID)){
                                    $text = 'Book $1 Demo Lesson';
                                    $class = 'free_lesson ';
                                }
                                else{
                                    $text = 'Book Lesson';
                                }
                                $current_user_credits = balans($current_user->ID);
                                if($current_user_credits <= 0){
                                    $class = 'add_credits ';
                                }
                            }
                            else{
                                $text = 'Book $1 Demo Lesson';
                                $class = 'expert_free_lesson ';
                            }
                            ?>
                            <a href="" class="<?php echo $class;?>book_lesson" onclick="showBookPopup(this, '<?php echo $post['post']->ID;?>', '<?php echo $post['lid'];?>'); return false;"><?php echo $text;?></a>
                        </div>
                    </div>
                    <!-- /EXPERT -->
                    <div id="tutor-info-<?php echo $post['post']->ID;?>" style="display:none;">
                        <?php  echo get_the_post_thumbnail($post['post']->ID,array(90,90)); ?>
                        <div class="book_lessons_profile_title"><?php echo $post['post']->post_title; ?></div>
                        <div class="book_lessons_profile_tags">
                            <div class="modal_lesson_teacher_time"><?php echo $length;?> mins</div>
                            <div><?php echo implode(', ', $types[$post['post']->ID]);?></div>
                            <div><?php echo implode(', ', $lesson_via[$post['post']->ID]);?></div>
                        </div>
                    </div>

                    <!-- MODAL BOOK LESSONS -->
                    <div class="modal_wrap modal_wrap_big" id="modal-wrap-<?php echo $post['post']->ID;?>">
                        <div class="modal_body">
                            <div class="modal_close"></div>
                            <div class="modal_title">Book a lesson</div>

                            <!-- STEPS TABS WRAP -->
                            <div class="steps_tabs_wrap">
                                <div class="steps_tab active">
                                    1. Select time
                                    <div></div>
                                </div>
                                <div class="steps_tab">
                                    2. Confirm booking
                                    <div></div>
                                </div>
                            </div>
                            <div class="steps_body_wrap">
                                <div class="step1-body steps_body active" id="popup1">
                                    <div class="book_lessons_wrap clearfix">
                                        <div class="tabletInfo">
                                            <div class="book_lessons_profile_wrap tabletInfo-user"></div>
                                            <div class="tabletInfo-dateTime">
                                                <div class="tabletInfo-date">
                                                    <?php echo date('d', $date);?>
                                                    <?php echo date('M', $date);?>,
                                                </div>
                                                <div class="tabletInfo-time">
                                                    <?php echo date('h:i A', $date);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="book_lessons_col col1">
                                            <div class="book_lessons_profile_wrap"></div>
                                        </div>
                                        <div class="book_lessons_col col2">
                                            <div class="bl_wrap" id="tutor-calendar-<?php echo $post['post']->ID;?>"></div>
                                            <div class="bl_text_table">
                                                * Available lesson times have been adjusted to the time zone set on your computer.
                                            </div>
                                            <div class="bl_button_wrap clearfix">
                                                <!--<div id="step1" class="bl_button">Book Your lesson</div>-->
                                            </div>
                                        </div>
                                        <div class="book_lessons_col col3">
                                            <!-- TIMENOW -->
                                            <div class="bl_timenow_wrap">
                                                <div class="bl_timenow_days">
                                                    <?php
                                                    $utc_time = strtotime(gmdate('d-m-Y H:i', time()));
                                                    $timezone = $_COOKIE['timezone'];
                                                    if(empty($timezone) || !isset($timezone)){
                                                        $timezone = 'GMT 0';
                                                    }
                                                    $sign = substr($timezone, 3, 1);
                                                    if ($sign == ' '){
                                                        $sign = '+';
                                                    }
                                                    $user_timezone = $sign . substr($timezone, 4, 2) . ' hours';
                                                    $user_time = strtotime($user_timezone, $utc_time);
                                                    ?>
                                                    <div><?php echo date('d', $user_time);?></div>
                                                    <div><?php echo date('M', $user_time);?></div>
                                                </div>
                                                <div class="bl_timenow_time">
                                                    <?php echo date('h:i A', $user_time);?>
                                                </div>
                                            </div>
                                            <!-- /TIMENOW -->

                                        </div>
                                    </div>

                                </div>
                                <div class="step2-body steps_body" id="popup2">
                                    <div class="bl_form_feedback_wrap clearfix">
                                        <div class="message bl_text_info_main"></div>
                                        <div class="lesson_info">
                                            <div class="load"></div>
                                            <div class="bl_text_info_main">
                                                <?php if(is_free($current_user->ID)):?>
                                                    <div class="test_lesson_text">This is a 30 minute trial lesson with <span> <?php echo $post['post']->post_title; ?></span>. This is to make sure you and the tutor can see and hear each other properly before having your first real lesson. <span> <?php echo $post['post']->post_title; ?></span> will contact you at the allotted time.</div>
                                                    <div class="second_lesson_text" style="display:none;">You’re about to book a <span><?php echo $length;?> mins</span> lesson with <span><?php echo $post['post']->post_title; ?></span>.If this is your first booked lesson with <?php echo $post['post']->post_title; ?>, then <?php echo $post['post']->post_title; ?> will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time.</div>
                                                <?php else:?>
                                                    You’re about to book a <span><?php echo $length;?> mins</span> lesson with <span><?php echo $post['post']->post_title; ?></span>.If this is your first booked lesson with <?php echo $post['post']->post_title; ?>, then <?php echo $post['post']->post_title; ?> will add you onto Skype soon or will contact you on your video conferencing unit at the allotted time.
                                                <?php endif;?>
                                            </div>
                                            <div class="bl_text_info">
                                                Please note that the start time has been adjusted to your local time through your computer's clock.
                                            </div>

                                            <div class="bl_form_feedback_wrap clearfix">
                                                <form method="post">
                                                    <div class="bl_form_feedback_left">
                                                        <div class="bl_form_profile">
                                                            <div class="bl_form_profile_photo">
                                                                <?php  echo get_the_post_thumbnail($post['post']->ID,array(90,90)); ?>
                                                            </div>
                                                            <div class="bl_form_profile_name">
                                                                <?php echo $post['post']->post_title; ?>
                                                            </div>
                                                        </div>

                                                        <div class="bl_feedback_timebox_wrap">
                                                            <span class="lesson-date"></span>,
                                                            <span class="lesson-time"></span>
                                                        </div>

                                                        <div class="bl_feedback_timebox_links">
                                                            <?php echo  renderBookingOptionsSelectBoxes($current_user, $location);?>
                                                        </div>

                                                    </div>
                                                    <div class="bl_form_feedback_right">
                                                        <textarea class="booker-comment bl_feedback_textarea" name="bookerComment" onfocus="if( $(this).text() == 'Add your comment here' ) {$(this).text('');}">Add your comment here</textarea>
                                                        <div class="book_lesson button_submit_feedback">Confirm Booking</div>
                                                    </div>
                                                    <input type='hidden' name='eme_eventAction' value='add_booking'/>
                                                    <input type='hidden' name='event_id' class="event_id" value=''/>
                                                    <input type='hidden'  class="event_time" value=''/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- /STEPS BODY WRAP -->
                        </div>
                    </div>
                    <!-- /MODAL BOOK LESSONS -->
                <?php endforeach;
                }?>
                <!-- EXPERT -->

<!--                <a href="--><?php //echo site_url() . '/?page_id=2';?><!--">-->
<!--                    <div class="expert_notice">-->
<!--                        <div class="expert_notice_text">-->
<!--                            We have SIX different teaching modules!-->
<!--                        </div>-->
<!--                        <div class="expert_notice_text">-->
<!--                            Click here to find out what works for you best!-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </a>-->
                <!-- /EXPERT -->
                </div>
                <!-- EXPERT PANEL -->
<?php
// Reset Query
                wp_reset_query();
                ?>
                <input type='hidden' id="booker_skype" value='<?php echo $bookerSkype;?>'/>
                <input type='hidden' id="booker_polycom" value='<?php echo $bookerPolycom;?>'/>
                <input type='hidden' id="booker_time" value='<?php echo $user_datetime;?>'/>
                <input type='hidden' name='bookedSeats' id="booked-seats" value='1'/>
                <input type='hidden' name='bookerEmail' id="booker-email" value='<?php echo $current_user->user_email;?>'/>
                <input type='hidden' name='bookerName' id="booker-name" value='<?php echo $current_user->user_login;?>'/>
                <input type='hidden' name='bookerSkype' id='bookerSkype_hidden' value=''>
                <input type='hidden' name='bookerPolycom' id='bookerPolycom_hidden' value=''>
                <div id='skype-dialog' style='display:none'>
                    <p>What is your skype handle?</p>
                    <input type='text' id='bookerSkype' value=''  class="defaultInput">
                </div>
                <div id='polycom-dialog' style='display:none'>
                    <p>What is your polycom number?</p>
                    <input type='text' id='bookerPolycom' class="defaultInput" value='' >
                </div>
            <? }?>
        <!-- End for blog in our tutor page -->
    <?php endwhile; endif; ?>
    </div>
    </div>
<?php get_footer(); ?>
