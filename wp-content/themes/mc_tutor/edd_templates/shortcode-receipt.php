<?php
/**
 * This template is used to display the purchase summary with [edd_receipt]
 */
global $edd_receipt_args, $edd_options;

$user_id = get_current_user_id();
$tutor_purchase_id = get_user_meta($user_id, 'tutor_purchase', true);
$hub_purchase_id = get_user_meta($user_id, 'hub_purchase', true);

$payment = get_post($edd_receipt_args['id']);
$meta = edd_get_payment_meta($payment->ID);
$cart = edd_get_payment_meta_cart_details($payment->ID, true);
$user = edd_get_payment_meta_user_info($payment->ID);
$email = edd_get_payment_user_email($payment->ID);
$status = edd_get_payment_status($payment, true);

$hub_cart = get_user_meta($user['id'], 'hub_cart');

if (isset($_GET['cart_action']) && $_GET['cart_action'] == 'add_to_cart') {

    if (isset($_GET['download_id']) && !empty($_GET['download_id'])) {
        $hub_download_id = $_GET['download_id'];
    }
    if (isset($_GET['edd_options']['price_id']) && !empty($_GET['edd_options']['price_id'])) {
        $hub_price_id = $_GET['edd_options']['price_id'];
        $hub_options = array();
        $hub_options['price_id'] = $hub_price_id;
    }

    $checkout_url = edd_get_checkout_uri();

    switch_to_blog(5);
    edd_add_to_cart($hub_download_id, $hub_options);
    $hub_site_cart = EDD()->session->get('edd_cart');
    update_user_meta($user['id'], 'hub_cart', $hub_site_cart);
    restore_current_blog();

    edd_remove_from_cart(0);

    wp_redirect($checkout_url);
}

?>
<?php if ($hub_cart[0] && !empty($hub_cart[0])): ?>
    <table id="edd_purchase_receipt" class="purchase-data">
        <thead>
        <?php do_action('edd_payment_receipt_before', $payment, $edd_receipt_args); ?>

        <?php if ($edd_receipt_args['payment_id']) : ?>
            <tr>
                <th><strong><?php _e('Payment', 'edd'); ?>:</strong></th>
                <th><?php echo edd_get_payment_number($payment->ID); ?></th>
            </tr>
        <?php endif; ?>
        </thead>

        <tbody>

        <tr>
            <td class="edd_receipt_payment_status"><strong><?php _e('Payment Status', 'edd'); ?>:</strong></td>
            <td class="edd_receipt_payment_status <?php echo strtolower($status); ?>"><?php echo $status; ?></td>
        </tr>

        <?php if ($edd_receipt_args['payment_key']) : ?>
            <tr>
                <td><strong><?php _e('Payment Key', 'edd'); ?>:</strong></td>
                <td><?php echo get_post_meta($payment->ID, '_edd_payment_purchase_key', true); ?></td>
            </tr>
        <?php endif; ?>

        <?php if ($edd_receipt_args['payment_method']) : ?>
            <tr>
                <td><strong><?php _e('Payment Method', 'edd'); ?>:</strong></td>
                <td><?php echo edd_get_gateway_checkout_label(edd_get_payment_gateway($payment->ID)); ?></td>
            </tr>
        <?php endif; ?>
        <?php if ($edd_receipt_args['date']) : ?>
            <tr>
                <td><strong><?php _e('Date', 'edd'); ?>:</strong></td>
                <td><?php echo date_i18n(get_option('date_format'), strtotime($meta['date'])); ?></td>
            </tr>
        <?php endif; ?>

        <?php if (($fees = edd_get_payment_fees($payment->ID, 'fee'))) : ?>
            <tr>
                <td><strong><?php _e('Fees', 'edd'); ?>:</strong></td>
                <td>
                    <ul class="edd_receipt_fees">
                        <?php foreach ($fees as $fee) : ?>
                            <li>
                                <span class="edd_fee_label"><?php echo esc_html($fee['label']); ?></span>
                                <span class="edd_fee_sep">&nbsp;&ndash;&nbsp;</span>
                                <span
                                    class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
        <?php endif; ?>

        <?php if ($edd_receipt_args['discount'] && $user['discount'] != 'none') : ?>
            <tr>
                <td><strong><?php _e('Discount(s)', 'edd'); ?>:</strong></td>
                <td><?php echo $user['discount']; ?></td>
            </tr>
        <?php endif; ?>

        <?php if (edd_use_taxes()) : ?>
            <tr>
                <td><strong><?php _e('Tax', 'edd'); ?></strong></td>
                <td><?php echo edd_payment_tax($payment->ID); ?></td>
            </tr>
        <?php endif; ?>

        <?php if ($edd_receipt_args['price']) : ?>

            <tr>
                <td><strong><?php _e('Subtotal', 'edd'); ?></strong></td>
                <td>
                    <?php echo edd_payment_subtotal($payment->ID); ?>
                </td>
            </tr>

            <tr>
                <td><strong><?php _e('Total Price', 'edd'); ?>:</strong></td>
                <td><?php echo edd_payment_amount($payment->ID); ?></td>
            </tr>

        <?php endif; ?>

        </tbody>
    </table>

    <?php do_action('edd_payment_receipt_after_table', $payment, $edd_receipt_args); ?>

    <?php if ($edd_receipt_args['products']) : ?>

        <h3><?php echo apply_filters('edd_payment_receipt_products_title', __('Products', 'edd')); ?></h3>

        <table id="edd_purchase_receipt_products" class="purchase-data">
            <thead>
            <th><?php _e('Name', 'edd'); ?></th>
            <?php if (edd_use_skus()) { ?>
                <th><?php _e('SKU', 'edd'); ?></th>
            <?php } ?>
            <?php if (edd_item_quantities_enabled()) : ?>
                <th><?php _e('Quantity', 'edd'); ?></th>
            <?php endif; ?>
            <th><?php _e('Price', 'edd'); ?></th>
            </thead>

            <tbody>
            <?php if ($cart) : ?>
                <?php foreach ($cart as $key => $item) : ?>

                    <?php if (!apply_filters('edd_user_can_view_receipt_item', true, $item)) : ?>
                        <?php continue; // Skip this item if can't view it ?>
                    <?php endif; ?>

                    <?php if (empty($item['in_bundle'])) : ?>
                        <tr>
                            <td>

                                <?php
                                $price_id = edd_get_cart_item_price_id($item);
                                $download_files = edd_get_download_files($item['id'], $price_id);
                                ?>

                                <div class="edd_purchase_receipt_product_name">
                                    <?php echo esc_html($item['name']); ?>
                                    <?php if (!is_null($price_id)) : ?>
                                        <span
                                            class="edd_purchase_receipt_price_name">&nbsp;&ndash;&nbsp;<?php echo edd_get_price_option_name($item['id'], $price_id, $payment->ID); ?></span>
                                    <?php endif; ?>
                                </div>

                                <?php if ($edd_receipt_args['notes']) : ?>
                                    <div
                                        class="edd_purchase_receipt_product_notes"><?php echo wpautop(edd_get_product_notes($item['id'])); ?></div>
                                <?php endif; ?>

                                <?php
                                if (edd_is_payment_complete($payment->ID) && edd_receipt_show_download_files($item['id'], $edd_receipt_args)) : ?>
                                    <ul class="edd_purchase_receipt_files">
                                        <?php
                                        if (!empty($download_files) && is_array($download_files)) :

                                            foreach ($download_files as $filekey => $file) :

                                                $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $item['id'], $price_id);
                                                ?>
                                                <li class="edd_download_file">
                                                    <a href="<?php echo esc_url($download_url); ?>"
                                                       download="<?php echo edd_get_file_name($file); ?>"
                                                       class="edd_download_file_link"><?php echo edd_get_file_name($file); ?></a>
                                                </li>
                                                <?php
                                                do_action('edd_receipt_files', $filekey, $file, $item['id'], $payment->ID, $meta);
                                            endforeach;

                                        elseif (edd_is_bundled_product($item['id'])) :

                                            $bundled_products = edd_get_bundled_products($item['id']);

                                            foreach ($bundled_products as $bundle_item) : ?>
                                                <li class="edd_bundled_product">
                                                    <span
                                                        class="edd_bundled_product_name"><?php echo get_the_title($bundle_item); ?></span>
                                                    <ul class="edd_bundled_product_files">
                                                        <?php
                                                        $download_files = edd_get_download_files($bundle_item);

                                                        if ($download_files && is_array($download_files)) :

                                                            foreach ($download_files as $filekey => $file) :

                                                                $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $bundle_item, $price_id); ?>
                                                                <li class="edd_download_file">
                                                                    <a href="<?php echo esc_url($download_url); ?>"
                                                                       class="edd_download_file_link"><?php echo esc_html($file['name']); ?></a>
                                                                </li>
                                                                <?php
                                                                do_action('edd_receipt_bundle_files', $filekey, $file, $item['id'], $bundle_item, $payment->ID, $meta);

                                                            endforeach;
                                                        else :
                                                            echo '<li>' . __('No downloadable files found for this bundled item.', 'edd') . '</li>';
                                                        endif;
                                                        ?>
                                                    </ul>
                                                </li>
                                            <?php
                                            endforeach;

                                        else :
                                            echo '<li>' . __('No downloadable files found.', 'edd') . '</li>';
                                        endif; ?>
                                    </ul>
                                <?php endif; ?>

                            </td>
                            <?php if (edd_use_skus()) : ?>
                                <td><?php echo edd_get_download_sku($item['id']); ?></td>
                            <?php endif; ?>
                            <?php if (edd_item_quantities_enabled()) { ?>
                                <td><?php echo $item['quantity']; ?></td>
                            <?php } ?>
                            <td>
                                <?php if (empty($item['in_bundle'])) : // Only show price when product is not part of a bundle ?>
                                    <?php echo edd_currency_filter(edd_format_amount($item['price'])); ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if (($fees = edd_get_payment_fees($payment->ID, 'item'))) : ?>
                <?php foreach ($fees as $fee) : ?>
                    <tr>
                        <td class="edd_fee_label"><?php echo esc_html($fee['label']); ?></td>
                        <?php if (edd_item_quantities_enabled()) : ?>
                            <td></td>
                        <?php endif; ?>
                        <td class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>

        </table>
    <?php endif; ?>
<?php else: ?>
    <?php
    if (isset($_GET['download_id']) && !empty($_GET['download_id'])) {
        $download_id = $_GET['download_id'];
        $this_site_home = get_bloginfo('url');
        $permalink = get_permalink();
        $buyed_period = $_GET['period'];
        $success_page = edd_get_settings('success_page');

        $payment = get_post($tutor_purchase_id);
        $total = edd_get_payment_amount($payment->ID);

        foreach($cart as $key => $item){
            $order_items[$key]['name'] = $item['name'];
            $order_items[$key]['sku'] = $item['name'];
            $order_items[$key]['price'] = $item['item_price'];
            $price_id = edd_get_cart_item_price_id($item);
            if (!is_null($price_id)){
                $order_items[$key]['variation'] = edd_get_price_option_name($item['id'], $price_id, $payment->ID);
            }
        }

        $cart = edd_get_payment_meta_cart_details($payment->ID, true);
        do_action('first_purchase_page', $tutor_purchase_id, __('My Chinese Group'), $total, $order_items );

        switch_to_blog(5);

        $rows = mcn_get_pricing_table_rows('mch_course_group', $download_id);
        $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $download_id);
        $periods = mcn_get_normalized_period_array($download_id);

        if ($rows && $characteristics): $_id = $download_id;?>
            <p><?php echo __('Great! You have now subscribed to classes with our Chinese tutor!
    Would you like to subscribe to our learning material on our sister site to assist with your learning?
It comes with video lessons to supplement your live lessons, quizes, forums and much more!'); ?></p>
            <table border="1"
                   class="pricing_table pricing_table_<?php echo $download_id; ?>" <?php if ($_id != $download_id) echo 'style="display:none;"'; ?>>
                <tr class="header-table">
                    <td></td>
                    <?php foreach ($rows as $key => $row):
                        $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                        <td class="<?php echo mcn_get_recommended_row_class($key); ?>">  <?php /*returned classes pricing_table pricing_row_recommended, pricing_table pricing_row_unrecommended*/ ?>
                            <?php if ($rec == 'true'): ?>
                                <?php _e('Recommended'); ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <tr class="title-td-table">
                    <td>
                        <?php
                        echo $download_title;
                        ?>
                    </td>
                    <?php foreach ($rows as $key => $row): ?>
                        <td class="<?php echo mcn_get_recommended_row_class($key); ?>"><?php _e($row); ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($characteristics as $characteristic): ?>
                    <tr class="content-table">
                        <td><?php _e($characteristic->post_title); ?></td>
                        <?php foreach ($rows as $key => $row):
                            $check = get_post_meta($characteristic->ID, 'post-' . $key . '-check', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?> <?php echo mcn_get_characteristic_class($characteristic->ID, $key); ?>">     <?php /* return class="pricing_table pricing_table_check" or class="pricing_table pricing_table_uncheck"*/ ?>
                                <?php if ($check == 'true' || $check == 'on'): ?>
                                    <i class="fa fa-check"></i>
                                <?php elseif($check && $check != 'false' && $check != 'off' ): ?>
                                    <?php echo $check;?>
                                <?php else:?>
                                    <i class="fa fa-minus"></i>
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>

                <tr class="separate-table border">
                    <td></td>
                    <?php foreach ($rows as $key => $row):
                        $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                        <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                    <?php endforeach; ?>
                </tr>

                <tr class="separate-table">
                    <td></td>
                    <?php foreach ($rows as $key => $row):

                        $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                        <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($periods as $key => $period): ?>
                    <?php
                    if ($key != $buyed_period) {
                        continue;
                    }
                    ?>
                    <?php if (isset($_GET['download_id']) && !empty($_GET['download_id'])): ?>
                        <?php $_id = $_GET['download_id']; ?>
                    <?php endif; ?>
                    <tr class="footer-table">

                        <td><?php echo(ucfirst($key) . " Subscription: "); ?></td>
                        <?php $i = false; ?>
                        <?php foreach ($period as $post => $value): ?>

                            <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                <?php if (array_key_exists($post, $rows)): ?>
                                    <?php if (!$i): ?>
                                        <span class="already-accessed"><?php echo __('Already Accessed'); $i = true;?></span>
                                    <?php else : ?>
                                    <a href="<?php echo $permalink; ?>?cart_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>">$<?php echo $value['amount']; ?></a>
                                        <?php endif; ?>
                                <?php endif; ?>
                            </td>

                        <?php endforeach; ?>

                    </tr>

                <?php endforeach; ?>
            </table>

        <?php
        endif;
        restore_current_blog(); ?>
        <a href="<?php echo get_the_permalink($success_page['success_page']); ?>"><?php echo __('No thanks, skip this'); ?></a>

    <?php } else { ?>
        <?php if (isset($hub_purchase_id) && !empty($hub_purchase_id)): ?>
            <?php
            switch_to_blog(5);
            if (isset($hub_purchase_id) && !empty($hub_purchase_id)) {

                $payment = get_post($hub_purchase_id);
                $meta = edd_get_payment_meta($payment->ID);
                $cart = edd_get_payment_meta_cart_details($payment->ID, true);
                $user = edd_get_payment_meta_user_info($payment->ID);
                $email = edd_get_payment_user_email($payment->ID);
                $status = edd_get_payment_status($payment, true);

                $total = edd_get_payment_amount($payment->ID);

                foreach($cart as $key => $item){
                    $order_items[$key]['name'] = $item['name'];
                    $order_items[$key]['sku'] = $item['name'];
                    $order_items[$key]['price'] = $item['item_price'];
                    $price_id = edd_get_cart_item_price_id($item);
                    if (!is_null($price_id)){
                        $order_items[$key]['variation'] = edd_get_price_option_name($item['id'], $price_id, $payment->ID);
                    }

                }
                do_action('common_thank_you_page', $hub_purchase_id, __('My Chinese Group'), $total, $order_items );
            }

            ?>
            <h3><?php echo __('My Chinese Hub purchase:', 'edd'); ?></h3>
            <table id="edd_purchase_receipt" class="purchase-data">
                <thead>
                <?php do_action('edd_payment_receipt_before', $payment, $edd_receipt_args); ?>

                <?php if ($payment->ID) : ?>
                    <tr>
                        <th><strong><?php _e('Payment', 'edd'); ?>:</strong></th>
                        <th><?php echo edd_get_payment_number($payment->ID); ?></th>
                    </tr>
                <?php endif; ?>
                </thead>

                <tbody>

                <tr>
                    <td class="edd_receipt_payment_status"><strong><?php _e('Payment Status', 'edd'); ?>:</strong></td>
                    <td class="edd_receipt_payment_status <?php echo strtolower($status); ?>"><?php echo $status; ?></td>
                </tr>

                <?php if ($edd_receipt_args['payment_key']) : ?>
                    <tr>
                        <td><strong><?php _e('Payment Key', 'edd'); ?>:</strong></td>
                        <td><?php echo get_post_meta($payment->ID, '_edd_payment_purchase_key', true); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['payment_method']) : ?>
                    <tr>
                        <td><strong><?php _e('Payment Method', 'edd'); ?>:</strong></td>
                        <td><?php echo edd_get_gateway_checkout_label(edd_get_payment_gateway($payment->ID)); ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($edd_receipt_args['date']) : ?>
                    <tr>
                        <td><strong><?php _e('Date', 'edd'); ?>:</strong></td>
                        <td><?php echo date_i18n(get_option('date_format'), strtotime($meta['date'])); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if (($fees = edd_get_payment_fees($payment->ID, 'fee'))) : ?>
                    <tr>
                        <td><strong><?php _e('Fees', 'edd'); ?>:</strong></td>
                        <td>
                            <ul class="edd_receipt_fees">
                                <?php foreach ($fees as $fee) : ?>
                                    <li>
                                        <span class="edd_fee_label"><?php echo esc_html($fee['label']); ?></span>
                                        <span class="edd_fee_sep">&nbsp;&ndash;&nbsp;</span>
                                <span
                                    class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['discount'] && $user['discount'] != 'none') : ?>
                    <?php $hub_discount_name = get_user_meta($user_id, 'hub_discount_name', true); ?>
                    <?php if ($hub_discount_name && !empty($hub_discount_name)): ?>
                        <tr>
                            <td><strong><?php _e('Discount(s)', 'edd'); ?>:</strong></td>
                            <td><?php echo $hub_discount_name; ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (edd_use_taxes()) : ?>
                    <tr>
                        <td><strong><?php _e('Tax', 'edd'); ?></strong></td>
                        <td><?php echo edd_payment_tax($payment->ID); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['price']) : ?>

                    <tr>
                        <td><strong><?php _e('Subtotal', 'edd'); ?></strong></td>
                        <td>
                            <?php echo edd_payment_subtotal($payment->ID); ?>
                        </td>
                    </tr>

                    <tr>
                        <td><strong><?php _e('Total Price', 'edd'); ?>:</strong></td>
                        <td><?php echo edd_payment_amount($payment->ID); ?></td>
                    </tr>

                <?php endif; ?>

                </tbody>
            </table>

            <?php do_action('edd_payment_receipt_after_table', $payment, $edd_receipt_args); ?>

            <?php if ($edd_receipt_args['products']) : ?>

                <h3><?php echo apply_filters('edd_payment_receipt_products_title', __('Products', 'edd')); ?></h3>

                <table id="edd_purchase_receipt_products" class="purchase-data">
                    <thead>
                    <th><?php _e('Name', 'edd'); ?></th>
                    <?php if (edd_use_skus()) { ?>
                        <th><?php _e('SKU', 'edd'); ?></th>
                    <?php } ?>
                    <?php if (edd_item_quantities_enabled()) : ?>
                        <th><?php _e('Quantity', 'edd'); ?></th>
                    <?php endif; ?>
                    <th><?php _e('Price', 'edd'); ?></th>
                    </thead>

                    <tbody>
                    <?php if ($cart) : ?>
                        <?php foreach ($cart as $key => $item) : ?>

                            <?php if (!apply_filters('edd_user_can_view_receipt_item', true, $item)) : ?>
                                <?php continue; // Skip this item if can't view it ?>
                            <?php endif; ?>

                            <?php if (empty($item['in_bundle'])) : ?>
                                <tr>
                                    <td>

                                        <?php
                                        $price_id = edd_get_cart_item_price_id($item);
                                        $download_files = edd_get_download_files($item['id'], $price_id);
                                        ?>

                                        <div class="edd_purchase_receipt_product_name">
                                            <?php echo esc_html($item['name']); ?>
                                            <?php if (!is_null($price_id)) : ?>
                                                <span
                                                    class="edd_purchase_receipt_price_name">&nbsp;&ndash;&nbsp;<?php echo edd_get_price_option_name($item['id'], $price_id, $payment->ID); ?></span>
                                            <?php endif; ?>
                                        </div>

                                        <?php if ($edd_receipt_args['notes']) : ?>
                                            <div
                                                class="edd_purchase_receipt_product_notes"><?php echo wpautop(edd_get_product_notes($item['id'])); ?></div>
                                        <?php endif; ?>

                                        <?php
                                        if (edd_is_payment_complete($payment->ID) && edd_receipt_show_download_files($item['id'], $edd_receipt_args)) : ?>
                                            <ul class="edd_purchase_receipt_files">
                                                <?php
                                                if (!empty($download_files) && is_array($download_files)) :

                                                    foreach ($download_files as $filekey => $file) :

                                                        $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $item['id'], $price_id);
                                                        ?>
                                                        <li class="edd_download_file">
                                                            <a href="<?php echo esc_url($download_url); ?>"
                                                               download="<?php echo edd_get_file_name($file); ?>"
                                                               class="edd_download_file_link"><?php echo edd_get_file_name($file); ?></a>
                                                        </li>
                                                        <?php
                                                        do_action('edd_receipt_files', $filekey, $file, $item['id'], $payment->ID, $meta);
                                                    endforeach;

                                                elseif (edd_is_bundled_product($item['id'])) :

                                                    $bundled_products = edd_get_bundled_products($item['id']);

                                                    foreach ($bundled_products as $bundle_item) : ?>
                                                        <li class="edd_bundled_product">
                                                    <span
                                                        class="edd_bundled_product_name"><?php echo get_the_title($bundle_item); ?></span>
                                                            <ul class="edd_bundled_product_files">
                                                                <?php
                                                                $download_files = edd_get_download_files($bundle_item);

                                                                if ($download_files && is_array($download_files)) :

                                                                    foreach ($download_files as $filekey => $file) :

                                                                        $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $bundle_item, $price_id); ?>
                                                                        <li class="edd_download_file">
                                                                            <a href="<?php echo esc_url($download_url); ?>"
                                                                               class="edd_download_file_link"><?php echo esc_html($file['name']); ?></a>
                                                                        </li>
                                                                        <?php
                                                                        do_action('edd_receipt_bundle_files', $filekey, $file, $item['id'], $bundle_item, $payment->ID, $meta);

                                                                    endforeach;
                                                                else :
                                                                    echo '<li>' . __('No downloadable files found for this bundled item.', 'edd') . '</li>';
                                                                endif;
                                                                ?>
                                                            </ul>
                                                        </li>
                                                    <?php
                                                    endforeach;

                                                else :
                                                    echo '<li>' . __('No downloadable files found.', 'edd') . '</li>';
                                                endif; ?>
                                            </ul>
                                        <?php endif; ?>

                                    </td>
                                    <?php if (edd_use_skus()) : ?>
                                        <td><?php echo edd_get_download_sku($item['id']); ?></td>
                                    <?php endif; ?>
                                    <?php if (edd_item_quantities_enabled()) { ?>
                                        <td><?php echo $item['quantity']; ?></td>
                                    <?php } ?>
                                    <td>
                                        <?php if (empty($item['in_bundle'])) : // Only show price when product is not part of a bundle ?>
                                            <?php echo edd_currency_filter(edd_format_amount($item['price'])); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (($fees = edd_get_payment_fees($payment->ID, 'item'))) : ?>
                        <?php foreach ($fees as $fee) : ?>
                            <tr>
                                <td class="edd_fee_label"><?php echo esc_html($fee['label']); ?></td>
                                <?php if (edd_item_quantities_enabled()) : ?>
                                    <td></td>
                                <?php endif; ?>
                                <td class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>

                </table>
            <?php endif; ?>
            <?php restore_current_blog(); ?>
            <?php //endif; ?>
        <?php endif; ?>

        <?php if (isset($tutor_purchase_id) && !empty($tutor_purchase_id)): ?>
            <?php

            $payment = get_post($tutor_purchase_id);
            $meta = edd_get_payment_meta($payment->ID);
            $cart = edd_get_payment_meta_cart_details($payment->ID, true);
            $user = edd_get_payment_meta_user_info($payment->ID);
            $email = edd_get_payment_user_email($payment->ID);
            $status = edd_get_payment_status($payment, true);

            ?>
            <h3><?php echo __('My Chinese Tutor purchase:', 'edd'); ?></h3>
            <table id="edd_purchase_receipt" class="purchase-data">
                <thead>
                <?php do_action('edd_payment_receipt_before', $payment, $edd_receipt_args); ?>

                <?php if ($tutor_purchase_id) : ?>
                    <tr>
                        <th><strong><?php _e('Payment', 'edd'); ?>:</strong></th>
                        <th><?php echo edd_get_payment_number($tutor_purchase_id); ?></th>
                    </tr>
                <?php endif; ?>
                </thead>

                <tbody>

                <tr>
                    <td class="edd_receipt_payment_status"><strong><?php _e('Payment Status', 'edd'); ?>:</strong></td>
                    <td class="edd_receipt_payment_status <?php echo strtolower($status); ?>"><?php echo $status; ?></td>
                </tr>

                <?php if ($edd_receipt_args['payment_key']) : ?>
                    <tr>
                        <td><strong><?php _e('Payment Key', 'edd'); ?>:</strong></td>
                        <td><?php echo get_post_meta($tutor_purchase_id, '_edd_payment_purchase_key', true); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['payment_method']) : ?>
                    <tr>
                        <td><strong><?php _e('Payment Method', 'edd'); ?>:</strong></td>
                        <td><?php echo edd_get_gateway_checkout_label(edd_get_payment_gateway($payment->ID)); ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($edd_receipt_args['date']) : ?>
                    <tr>
                        <td><strong><?php _e('Date', 'edd'); ?>:</strong></td>
                        <td><?php echo date_i18n(get_option('date_format'), strtotime($meta['date'])); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if (($fees = edd_get_payment_fees($payment->ID, 'fee'))) : ?>
                    <tr>
                        <td><strong><?php _e('Fees', 'edd'); ?>:</strong></td>
                        <td>
                            <ul class="edd_receipt_fees">
                                <?php foreach ($fees as $fee) : ?>
                                    <li>
                                        <span class="edd_fee_label"><?php echo esc_html($fee['label']); ?></span>
                                        <span class="edd_fee_sep">&nbsp;&ndash;&nbsp;</span>
                                <span
                                    class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['discount'] && $user['discount'] != 'none') : ?>
                    <tr>
                        <td><strong><?php _e('Discount(s)', 'edd'); ?>:</strong></td>
                        <td><?php echo $user['discount']; ?></td>
                    </tr>
                <?php endif; ?>

                <?php if (edd_use_taxes()) : ?>
                    <tr>
                        <td><strong><?php _e('Tax', 'edd'); ?></strong></td>
                        <td><?php echo edd_payment_tax($payment->ID); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if ($edd_receipt_args['price']) : ?>

                    <tr>
                        <td><strong><?php _e('Subtotal', 'edd'); ?></strong></td>
                        <td>
                            <?php echo edd_payment_subtotal($payment->ID); ?>
                        </td>
                    </tr>

                    <tr>
                        <td><strong><?php _e('Total Price', 'edd'); ?>:</strong></td>
                        <td><?php echo edd_payment_amount($payment->ID); ?></td>
                    </tr>

                <?php endif; ?>

                </tbody>
            </table>

            <?php do_action('edd_payment_receipt_after_table', $payment, $edd_receipt_args); ?>

            <?php if ($edd_receipt_args['products']) : ?>

                <h3><?php echo apply_filters('edd_payment_receipt_products_title', __('Products', 'edd')); ?></h3>

                <table id="edd_purchase_receipt_products" class="purchase-data">
                    <thead>
                    <th><?php _e('Name', 'edd'); ?></th>
                    <?php if (edd_use_skus()) { ?>
                        <th><?php _e('SKU', 'edd'); ?></th>
                    <?php } ?>
                    <?php if (edd_item_quantities_enabled()) : ?>
                        <th><?php _e('Quantity', 'edd'); ?></th>
                    <?php endif; ?>
                    <th><?php _e('Price', 'edd'); ?></th>
                    </thead>

                    <tbody>
                    <?php if ($cart) : ?>
                        <?php foreach ($cart as $key => $item) : ?>

                            <?php if (!apply_filters('edd_user_can_view_receipt_item', true, $item)) : ?>
                                <?php continue; // Skip this item if can't view it ?>
                            <?php endif; ?>

                            <?php if (empty($item['in_bundle'])) : ?>
                                <tr>
                                    <td>

                                        <?php
                                        $price_id = edd_get_cart_item_price_id($item);
                                        $download_files = edd_get_download_files($item['id'], $price_id);
                                        ?>

                                        <div class="edd_purchase_receipt_product_name">
                                            <?php echo esc_html($item['name']); ?>
                                            <?php if (!is_null($price_id)) : ?>
                                                <span
                                                    class="edd_purchase_receipt_price_name">&nbsp;&ndash;&nbsp;<?php echo edd_get_price_option_name($item['id'], $price_id, $payment->ID); ?></span>
                                            <?php endif; ?>
                                        </div>

                                        <?php if ($edd_receipt_args['notes']) : ?>
                                            <div
                                                class="edd_purchase_receipt_product_notes"><?php echo wpautop(edd_get_product_notes($item['id'])); ?></div>
                                        <?php endif; ?>

                                        <?php
                                        if (edd_is_payment_complete($payment->ID) && edd_receipt_show_download_files($item['id'], $edd_receipt_args)) : ?>
                                            <ul class="edd_purchase_receipt_files">
                                                <?php
                                                if (!empty($download_files) && is_array($download_files)) :

                                                    foreach ($download_files as $filekey => $file) :

                                                        $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $item['id'], $price_id);
                                                        ?>
                                                        <li class="edd_download_file">
                                                            <a href="<?php echo esc_url($download_url); ?>"
                                                               download="<?php echo edd_get_file_name($file); ?>"
                                                               class="edd_download_file_link"><?php echo edd_get_file_name($file); ?></a>
                                                        </li>
                                                        <?php
                                                        do_action('edd_receipt_files', $filekey, $file, $item['id'], $payment->ID, $meta);
                                                    endforeach;

                                                elseif (edd_is_bundled_product($item['id'])) :

                                                    $bundled_products = edd_get_bundled_products($item['id']);

                                                    foreach ($bundled_products as $bundle_item) : ?>
                                                        <li class="edd_bundled_product">
                                                    <span
                                                        class="edd_bundled_product_name"><?php echo get_the_title($bundle_item); ?></span>
                                                            <ul class="edd_bundled_product_files">
                                                                <?php
                                                                $download_files = edd_get_download_files($bundle_item);

                                                                if ($download_files && is_array($download_files)) :

                                                                    foreach ($download_files as $filekey => $file) :

                                                                        $download_url = edd_get_download_file_url($meta['key'], $email, $filekey, $bundle_item, $price_id); ?>
                                                                        <li class="edd_download_file">
                                                                            <a href="<?php echo esc_url($download_url); ?>"
                                                                               class="edd_download_file_link"><?php echo esc_html($file['name']); ?></a>
                                                                        </li>
                                                                        <?php
                                                                        do_action('edd_receipt_bundle_files', $filekey, $file, $item['id'], $bundle_item, $payment->ID, $meta);

                                                                    endforeach;
                                                                else :
                                                                    echo '<li>' . __('No downloadable files found for this bundled item.', 'edd') . '</li>';
                                                                endif;
                                                                ?>
                                                            </ul>
                                                        </li>
                                                    <?php
                                                    endforeach;

                                                else :
                                                    echo '<li>' . __('No downloadable files found.', 'edd') . '</li>';
                                                endif; ?>
                                            </ul>
                                        <?php endif; ?>

                                    </td>
                                    <?php if (edd_use_skus()) : ?>
                                        <td><?php echo edd_get_download_sku($item['id']); ?></td>
                                    <?php endif; ?>
                                    <?php if (edd_item_quantities_enabled()) { ?>
                                        <td><?php echo $item['quantity']; ?></td>
                                    <?php } ?>
                                    <td>
                                        <?php if (empty($item['in_bundle'])) : // Only show price when product is not part of a bundle ?>
                                            <?php echo edd_currency_filter(edd_format_amount($item['price'])); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (($fees = edd_get_payment_fees($payment->ID, 'item'))) : ?>
                        <?php foreach ($fees as $fee) : ?>
                            <tr>
                                <td class="edd_fee_label"><?php echo esc_html($fee['label']); ?></td>
                                <?php if (edd_item_quantities_enabled()) : ?>
                                    <td></td>
                                <?php endif; ?>
                                <td class="edd_fee_amount"><?php echo edd_currency_filter(edd_format_amount($fee['amount'])); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>

                </table>
            <?php endif; ?>
        <?php endif; ?>
    <?php } ?>
<?php endif; ?>