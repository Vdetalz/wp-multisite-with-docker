<?php /* Template Name: Home page */?>
<?php get_header(); ?>

<?php

// get posts of "homepage slider" content type
$args = array(
    'post_type' => 'homepage_slider',
    'posts_per_page' => 1
);

$sliders_query = new WP_Query( $args );

$badge_price = get_option('hp_badge_price');
$badge_text = get_option('hp_badge_text');
?>
<?php if ( $sliders_query->have_posts() ) : ?>
    <ul class="digital-products-list-big">
        <?php while ( $sliders_query->have_posts() ) : $sliders_query->the_post(); ?>
            <?php //get video url from "homepage slider" custom field
            $video_url = get_field('video_file', $post->ID);  ?>
            <?php if($video_url): ?>
                <?php $IE8 = (preg_match('/MSIE 8/i',$_SERVER['HTTP_USER_AGENT'])) ? true : false;?>
                <?php if(!wp_is_mobile() && !$IE8): ?>
                    <![if (!IE) | (gte IE 9)]>
                <li>
                <?php echo do_shortcode( '[videojs mp4="' .  $video_url . '" autoplay="true" controls="false" loop="true" preload="auto" muted="true"]' ); ?>
                    <![endif]>
                    <!--[if (IE) & (lt IE 9)]>
                    <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
                    <li class="has-bg" style="background: url( <?php echo $large_image_url[0] ?> ) no-repeat center top; background-size: cover;">
                    <![endif]-->
                <?php else: ?>
                <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
                <li class="has-bg" style="background: url( <?php echo $large_image_url[0] ?> ) no-repeat center top; background-size: cover;">
                <?php endif; ?>
            <?php else: ?>
            <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
                <li class="has-bg" style="background: url( <?php echo $large_image_url[0] ?> ) no-repeat center top; background-size: cover;">
            <?php endif; ?>
                <div class="wrappper">
                    <div class="content-wrapper">
                        <?php the_content(); ?>
                        <?php if (isset($badge_price) && !empty($badge_price) && isset($badge_text) && !empty($badge_text)) { ?>
                            <div class="hp-badge">
                                <span class="hp-badge-header"><?php echo __('From'); ?></span>
                                <span class="hp-badge-body"><?php echo $badge_price; ?></span>
                                <span class="hp-badge-footer"><?php echo $badge_text; ?></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </li>
        <?php  endwhile;?>
    </ul>
<?php endif;?>

    <?php
    if (function_exists('dynamic_sidebar'))
        dynamic_sidebar('media_section');
    ?>

	<section role="main" class="content-home">
        <div class="main-content content-wrapper">
		<!-- section -->
		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'mct_hub' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</section>
<?php get_footer(); ?>
