<?php /* Template Name: Login Page Template */ get_header(); ?>

	<section role="main" class="main-content content-wrapper">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="wrap-registration login-form">
                    <?php echo do_shortcode('[theme-my-login]'); ?>
                    <div class="login_image">
                    	<?php echo get_the_post_thumbnail(); ?>
                    </div>
                </div>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'mct_hub' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
