<!DOCTYPE html>
<?php global $am_option, $query_string;
    global $current_user;
    global $current_user_credits;
    $user = new WP_User($current_user->id);
?>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
<title>
    <?php
	if (is_category()) {	
		echo __('Category: ','neo_wdl');
		wp_title(''); echo ' - ';	
	} 
	elseif (function_exists('is_tag') && is_tag()) {	
		single_tag_title(__('Tag Archive for &quot;','neo_wdl'));
		echo '&quot; - ';	
	} 
	elseif (is_archive()) {	
		wp_title(''); 
		echo __(' Archive - ','neo_wdl');	
	}
	elseif (is_page()) {	
		echo wp_title(''); echo ' - ';
	}
	elseif (is_search()) {	
		echo __('Search for &quot;','neo_wdl').esc_html($s).'&quot; - ';	
	} 
	elseif (!(is_404()) && (is_single()) || (is_page())) {	
		wp_title('');
		echo ' - ';	
	} 
	elseif (is_404()) {	
		echo __('Not Found - ','neo_wdl');
	}
?></title>
<?php if(is_category(16)){?>
<meta name="description" content="Looking for Chinese language education? My Chinese Tutor helps you to learn Chinese fast with highly capable Chinese language tutors in an affordable and convenient way."/>	
<?php	} ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/other.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/common.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/body_msg.css" type="text/css" media="all" />
    <!--[if lt IE 9]><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/ie.css" type="text/css" media="all" /><![endif]-->
    <!--[if lt IE 8]><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/ie7.css" type="text/css" media="all" /><![endif]-->
<link rel="shortcut icon" type="image/png" href="favicon.png"/>
<script type="text/javascript" >var ROOT_URL = '<?php bloginfo('template_url'); ?>'; </script>
<script type="text/javascript" >var SITE_URL = '<?php bloginfo('url'); ?>'; </script>
<script type="text/javascript" >var USER_ID = <?php echo $current_user->ID ?>; </script>
<script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/jquery.cookie.js"></script>
<script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/common.js"></script>
<script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/general.js"></script>
<script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/body_msg.js"></script>
    <!--[if IE]><script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/ie.js"></script><![endif]-->

<?php wp_head(); ?>
<script type='text/javascript'>
	function showvideo(){
           jQuery('#home_video_video').html('<iframe width="325" height="200" src="http://www.youtube.com/embed/j9EWPfgqQ_Q?rel=0&amp;fs=1&amp;controls=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" wmode="opaque" allowfullscreen></iframe>');
       }

	jQuery(document).ready(function() {
		jQuery(".text-prev-post p").append("...");
		jQuery("table tr:even").addClass("odd");
		jQuery("table tr:first").addClass("first");
		jQuery("table tr td:last-child").addClass("last");
		jQuery(".footer-widgets .widget-container:last-child").addClass("last");
		
		jQuery(function () {
		var tabContainers = jQuery('.tabcontent div.tabs-cont');
		tabContainers.hide().filter(':first').show();	
		jQuery('.tabNavigation a').click(function () {		
			tabContainers.hide();	
			tabContainers.filter(this.hash).fadeIn("slow");		
			jQuery('.tabNavigation a').removeClass('selected');	
			jQuery(this).addClass('selected');		
			return false;	
		}).filter(':first').click();});

		
		<?php if (is_page(307) || is_page(314) ):?>
			<?php $current_user = wp_get_current_user(); ?>
			<?php if (!empty($current_user->user_login)):?>
				jQuery('input[name=user_name]').val('<?php echo $current_user->user_login; ?>');
			<?php endif;?>
		<?php endif;?>
		
	});
</script>

<!--  register ajax form ***************** -->
<script type='text/javascript'>
	
	function getPostData(selector){
		var postData = {};	
		$.each($(selector), function(index,el){			
			el = $(el);		
			postData[el.attr('name')] = el.attr('value');		
		});		
		return postData
	}
	var where_are_you_studying;
	var where_did_you_hear_about_us;
	var what_is_your_level;
	var privacy;
	jQuery(document).ready( function(){
		jQuery('input[name=where_are_you_studying]:radio').click(function() {
			where_are_you_studying = jQuery(this).val();	
		});	
		jQuery('input[name=where_did_you_hear_about_us]:radio').click(function() {	
			where_did_you_hear_about_us = jQuery(this).val();	
		});	
		jQuery('input[name=what_is_your_level]:radio').click(function() {	
			what_is_your_level = $(this).val();	});
			jQuery('ul.smooth_sliderb  li:last').click(function() {
				registration_form();
			});
		
	});
	
</script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/redmond/jquery-ui.css"/>
    <script type='text/javascript' src="<?php bloginfo('template_url'); ?>/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type='text/javascript' src="<?php bloginfo('url'); ?>/wp-content/plugins/events-manager-extended/js/client-timezone.js"></script>
</head>
<body>
<div class="wrapper">
		<div class="wrap">
			<div class="header_wrap">
				<div class="header_inner">
					<!-- LOGO -->
					<div class="logo_wrap">
						<a href="<?php echo site_url();?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
						</a>
					</div>
					<!-- /LOGO -->

					<!-- HEADER NAVIGATION -->
					<div class="header_navigator">
						<a href="<?php echo site_url();?>">HOME</a>
                        <a class="<?php if(is_page(2)) echo 'active'; ?>" href="<?php echo get_permalink(2); ?>">SERVICE</a>
                        <a class="<?php if(is_page(265)) echo 'active'; ?>" href="<?php echo get_permalink(265); ?>">TUTORS</a>
						<a class="<?php if(is_page(760)) echo 'active'; ?>" href="<?php echo get_permalink(760); ?>">SYLLABUS</a>
                        <a class="show_credits_popap" href="#">PRICING</a>
                        <a class="<?php if(is_category(16)) echo 'active'; ?>" href="<?php echo site_url();?>/?cat=16">BLOG</a>
                        <a href="https://mychinesetutor.zendesk.com/home" target="_blank">SUPPORT</a>
                        <?php if ($user->roles[0] == 'administrator' || $user->roles[0] == 'editor'):?>
                         <a class="<?php if(is_page(1343)) echo 'active'; ?>" href="<?php echo site_url();?>/?page_id=1343">User's Account</a>
                         <a class="<?php if(is_page(1345)) echo 'active'; ?>" href="<?php echo site_url();?>/?page_id=1345">Tutor Account</a>
                        <?php endif;?>
					</div>
					<!-- /HEADER NAVIGATION -->
					
					<!-- HEADER BUTTONS -->
					<div class="header_buttons_wrap">
						<?php
							//$current_user_credits = balans($current_user->ID);

                        ?>
						<?php if (is_user_logged_in()) { ?>
							<a class="logout_button" href="<?php echo wp_logout_url(); ?>">
								<div class="header_button_log_out">Log out</div>
							</a>
							<a class="show_credits_popap" href="#">
								<div class="header_button_log_out credits_block"><?php echo $current_user_credits.' CREDITS'; ?></div>
							</a>
						<?php } else { ?>
							<a href="#">
								<div class="header_button_log">Log In</div>
							</a>
						<?php } ?>
						
							<?php if (is_user_logged_in()) { ?>
                                <?php
                                if ($user->roles[0] == 'author'){
                                      $page_id =   209;
                               }
                                else if($user->roles[0] == 'contributor'){
                                    $page_id =   1351;
                                }
                                else{
                                    $page_id =   206;
                                }
                                ?>
                                 <a href="<?php echo get_permalink($page_id); ?> ">
								<div class="header_button_profile">
									&nbsp;<?php 
										 
										echo $current_user->nickname;
									?>
									<span></span>
								</div>
								</a>
							<?php } else { ?>
								<a href="#">
									<div class="header_button_reg">Register</div>
								</a>
							<?php } ?>
							
						<a href="https://www.facebook.com/pages/My-Chinese-Tutor/147202535364787">
							<div class="header_button_facebook"></div>
						</a>
						<a href="https://twitter.com/@mychinesetutor">
							<div class="header_button_twitter"></div>
						</a>
					</div>
					<!-- /HEADER BUTTONS -->
				</div>
			</div>
