<?php
/*
Template Name: Static
*/
global $am_option;
get_header();
?>
				
<div class="content_wrap">	
	<div class="content_big_inner">		
	<!-- BLOG PAGE -->			
	<div class="blog_page_wrap">		
	<div class="blog_wrap">			
		
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="blog_title"><?php the_title(); ?></div>
			<div class="blog_text">
			<?php if(!empty($am_option['main']['ads_single_300'])) : ?>
				<div class="ads ads_big"><?php echo $am_option['main']['ads_single_300']; ?></div>
			<?php endif; ?>
				<div class="tutor-profile">
				<div class="img-single"><?php the_post_thumbnail('thumbnail'); ?></div>
					<?php the_content(__('Read more', 'neo_wdl')); ?>
				</div>
				<div class="clear"></div>
				<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'neo_wdl' ) . '</span>', 'after' => '</div>' ) ); ?>
				<?php if(!empty($am_option['main']['ads_single_468'])) : ?>
			<div class="ads ads_small"><?php echo $am_option['main']['ads_single_468']; ?></div>
			<?php endif; ?>
			</div><!-- /post -->
		
		<?php endwhile;	endif; ?>		</div>		</div>			<!-- /BLOG PAGE -->		</div>	</div>	
<?php get_footer(); ?>