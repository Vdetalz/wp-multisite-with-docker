<?php
global $am_option;

$am_themename = "Neo_wdl";
$am_shortname = "neo_wdl";
$am_textdomain = "neo_wdl";

$am_option['url']['includes_path'] = 'includes';
$am_option['url']['extensions_path'] = $am_option['url']['includes_path'].'/extensions';
$am_option['url']['extensions_url'] = get_template_directory_uri().'/'.$am_option['url']['extensions_path'];
$am_option['url']['themeoptions_path'] = $am_option['url']['includes_path'].'/theme-options';
$am_option['url']['themeoptions_url'] = get_template_directory_uri().'/'.$am_option['url']['themeoptions_path'];

// Functions
require_once($am_option['url']['includes_path'].'/fn-core.php');
require_once($am_option['url']['includes_path'].'/fn-custom.php');

// Extensions
require_once($am_option['url']['extensions_path'].'/breadcrumb-trail.php');

/* Theme Init */
require_once($am_option['url']['includes_path'].'/theme-init.php');

/* Admin */
require_once($am_option['url']['themeoptions_path'].'/fn-admin.php');
require_once($am_option['url']['themeoptions_path'].'/am-main.php');


/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Shoping_cart', 'twentyten' ),
		'id' => '666',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Footer', 'twentyten' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'twentyten' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'twentyten' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 6, located in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Side-right not home', 'twentyten' ),
		'id' => 'right-not-home-widget-area',
		'description' => __( 'Side-right not home', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Area 7, located in the sidebar on homepage. Empty by default.
	register_sidebar( array(
		'name' => __( 'Side-right on home', 'twentyten' ),
		'id' => 'right-home-widget-area',
		'description' => __( 'Side-right on home', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 8, located in the sidebar on content. Empty by default.
	register_sidebar( array(
		'name' => __( 'Content sidebar', 'twentyten' ),
		'id' => 'content-sidebar',
		'description' => __( 'Content sidebar', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init' );

function neo_register_new_user( $user_login, $user_email, $user_nickname ) {
	$errors = new WP_Error();

	$sanitized_user_login = sanitize_user( $user_login );
	$user_email = apply_filters( 'user_registration_email', $user_email );


	// Check the e-mail address
	if ( $user_email == '' ) {
		$errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your e-mail address.' ) );
	} elseif ( ! is_email( $user_email ) ) {
		$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
		$user_email = '';
	} elseif ( email_exists( $user_email ) ) {
		$errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
	}

	do_action( 'register_post', $sanitized_user_login, $user_email, $errors );

	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );

	if ( $errors->get_error_code() )
		return $errors;

	$user_pass = wp_generate_password( 12, false);
	$user_id = wp_create_user( $sanitized_user_login, $user_pass, $user_email );
	if ( ! $user_id ) {
		$errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you... please contact the <a href="mailto:%s">webmaster</a> !' ), get_option( 'admin_email' ) ) );
		return $errors;
	}

    update_user_option( $user_id, 'nickname', $user_nickname, true) ;
	update_user_option( $user_id, 'default_password_nag', true, true ); //Set up the Password change nag.

	wp_new_user_notification( $user_id, $user_pass );

	return $user_id;
}

function my_function_admin_bar(){
return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

function the_content_limit($max_char, $more_link_text = '', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);

   if (strlen($_GET['p']) > 0) {
      echo "";
      echo $content;
	  echo "[...]";
      /*echo "&nbsp;<a href='";
      the_permalink();
      echo "'>"."Read more &rarr;</a>";*/
      echo "";
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo "";
        echo $content;
        echo "[...]";
       /* echo "&nbsp;<a href='";
        the_permalink();
        echo "'>"."</a>";*/
        echo "";
   }
   else {
      echo "";
      echo $content;
	  echo "[...]";
      /*echo "&nbsp;<a href='";
      the_permalink();
      echo "'>"."Read more &rarr;</a>";*/
      echo "";
   }
}

function array_sorting_asc_another($a,$b){
	if ($a['_fafa']==$b['_fafa']) { return 0; }
	if ($a['_fafa']>$b['_fafa']) { return -1; }
	return 1;
}

function array_sorting_key_another($array,$key){
	$r=$array;
	foreach($r as &$v) { $v['_fafa']=$v[$key]; }
	usort($r, 'array_sorting_asc_another');
	foreach($r as &$v) { unset($v['_fafa']); }
	return $r;
}
function _substr($text, $length)
{
    $length = strripos(substr($text, 0, $length), ' ');
    return substr($text, 0, $length);
}

function get_tutor_slider()
{?>

<?php query_posts('ignore_sticky_posts=1&post_type=post&meta_key=_slider&meta_value=yes&showposts=' . $am_option['main']['number_posts']); ?>
<?php if(have_posts()) { ?>
<div class="slider-bottom">
    <h4 class="headding-our-tutor">Our Tutors</h4>

    <?php
    global $wpdb;
    $result=$wpdb->get_results("SELECT wp_posts.post_title,wp_posts.ID,wp_posts.post_content  FROM `wp_posts`
    INNER JOIN `wp_term_relationships`
        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id = '3'
    INNER JOIN `wp_postmeta`
      ON wp_postmeta.post_id = wp_posts.ID AND wp_postmeta.meta_key='_slider' AND wp_postmeta.meta_value = 'yes'
    WHERE wp_posts.post_type='post' AND wp_posts.post_status='publish'");

    //query_posts('ignore_sticky_posts=1&post_type=post&meta_key=_slider&meta_value=yes&showposts='.$am_option['main']['number_posts']);
   ?>
    <div id="slides-box" class="pics">
        <?php foreach($result as $tutor) { ?>
        <div class="item-sld">
            <table style="width:100%;">
                <tr>
                    <td>
                        <?php $img = get_the_post_thumbnail($tutor->ID);
                        if($img) { ?>
                            <div class="left-img"><?php echo $img; ?></div>
                            <?php } ?>

                        <div class="text-slider">
                            <p class="title-prof">
                                <?php echo $tutor->post_title; ?><br/>
                            </p>

                            <p class="tex">
                                <?php echo strip_tags(substr($tutor->post_content, 0, 150)) . "&nbsp;..."; ?>
                            </p>

                            <div class="link-more">
                                <a href="<?php echo home_url() . '/?p=' . $tutor->ID; ?>">
                                    <img alt="" src="<? bloginfo('template_url'); ?>/images/view-small.png" />
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <?php }  ?>
    </div>
    <span id="prev_slide"></span> <span id="next_slide"></span>
</div>
<?php
}
    wp_reset_query();
}

add_shortcode( 'edit_profile_form', 'edit_profile_form' );

function edit_profile_form(){
    global $wpdb;
    $user = wp_get_current_user();
    $output = '';
    $show_message = False;

    if(isset($_POST['update'])){

        update_user_option( $user->data->ID, 'what_is_your_skype_name', $_POST['what_is_your_skype_name'], true);
        update_user_option( $user->data->ID, 'polycom_number', $_POST['polycom_number'], true);
        update_user_option( $user->data->ID, 'first_name', $_POST['nickname'], true);
        update_user_option( $user->data->ID, 'nickname', $_POST['nickname'], true);
        update_user_option( $user->data->ID, 'last_name', $_POST['last_name'], true);
        update_user_option( $user->data->ID, 'country_of_residence', $_POST['country_of_residence'], true);
        update_user_option( $user->data->ID, 'what_is_your_level', $_POST['what_is_your_level'], true);
        $user = get_user_by('id',$user->data->ID);

        if ($_POST['subscribe'] == 'subscribe'){
            $sql = "SELECT email FROM " . $wpdb->prefix . wpns_TABLE_NAME . "
                     WHERE email=" . $user->user_email;
            $result = $wpdb->get_results($sql, ARRAY_A);
            if(!$result){
                $wpdb->insert(
                    $wpdb->prefix . wpns_TABLE_NAME,
                    array( 'email' => $user->user_email),
                    array( '%s' )
                );
            }

        }
        elseif($_POST['subscribe'] == 'unsubscribe'){
            $wpdb->query("DELETE FROM " . $wpdb->prefix . wpns_TABLE_NAME . " WHERE email ='" . $user->user_email . "'");
        }

        $show_message = True;
    }

    $sql = "SELECT * FROM ".$wpdb->prefix."mct_subscribers WHERE email = '".$user->user_email."'";
    $subscribe = $wpdb->get_row($sql);

    if($show_message){
        $output .= '<div class="updated" id="message">
                        <p class="succesMessage">Profile was updated</p>
                    </div>';
    }

    $output .= '<div class="profile_form_table add_width_table">
    <form  method="post">
		<div class="profile_form_tr">
			<div class="profile_form_td first_inp">Nick Name :<div>*</div></div>
			<div class="profile_form_td add_width">
				<input name="nickname" required="required" type="text" value="'.$user->nickname.'" />
			</div>
		</div>

		<div class="profile_form_tr">
			<div class="profile_form_td first_inp">Skype Name : <div>*</div></div>
			<div class="profile_form_td add_width">
				<input name="what_is_your_skype_name" required="required" type="text" value="'.$user->what_is_your_skype_name.'" />
			</div>
		</div>

		<div class="profile_form_tr">
			<div class="profile_form_td first_inp">Polycom Number :</div>
			<div class="profile_form_td add_width">
				<input name="polycom_number" type="text" value="'.$user->polycom_number.'" />
			</div>
		</div>';


         if(empty($subscribe)){
             $output .= '<div class="subscribe_block"><input type="checkbox" value="subscribe" name="subscribe">Subscribe to the weekly blog</div>';
         }
         else{
             $output .= '<div class="subscribe_block"><input type="checkbox" value="unsubscribe" name="subscribe">Unsubscribe to the Blog</div>';
         }

      $output .= '<div class="profile_form_buttons">
            <input class="save_block_button" name="update" type="submit" value="Save"/>
        </div>
        </form>
    </div>';

        return $output;
}

add_shortcode( 'register_form', 'register_form' );

function register_form($attrs){
    if(isset($_POST['register'])){
        $user_login = $_POST['user_email'];
        $user_email = $_POST['user_email'];
        $user_nickname = $_POST['user_nickname'];
        $_POST['firstname'] = $user_nickname;
        $_POST['pass2'] = $_POST['pass1'];

        $errors = neo_register_new_user($user_login, $user_email, $user_nickname);
        if (is_int($errors)){
            $message  = sprintf(__('New user Register on your blog %s:', 'regplus'), get_option('blogname')) . "\r\n\r\n";
            $message .= sprintf(__('Nickname: %s', 'regplus'), $_POST['user_nickname']) . "\r\n\r\n";
            $message .= sprintf(__('E-mail: %s', 'regplus'), $_POST['user_email']) . "\r\n\r\n";
            @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Register', 'regplus'), get_option('blogname')), $message);

            $creds = array();
            $creds['user_login'] = $user_login;
            $creds['user_password'] = $_POST['pass1'];
            $creds['remember'] = true;

            $user = wp_signon( $creds, false );

            if ( is_wp_error($user) ){
                $error_message = '<div class="error-register">';
                $error_message .= $user->get_error_message();
                $error_message .= '</div>';
            }
            else{
                setcookie ('register', 'yes',  (time() + 3600),"/");
                wp_redirect(get_bloginfo('url').'?page_id=265');
            }
        }
        else{
            $error_message = '<div class="error-register">';
            foreach ($errors as $error){
                        foreach ($error as $err){
                            foreach($err as $e){
                                $error_message .= str_replace('First Name', 'Nickname', $e). '<br>';
                            }
                        }
                    }
            $error_message .= '</div>';
        }
    }

if(!empty($error_message)){
    echo $error_message;
}
?>
<div id="form_registration">
    <form name="registerform" id="registerform" action="#" method="post">
        <p>
            Enter your email : *<br/>
             <input type="text" required="required" name="user_email" id="user_email" class="input" value="<?php echo $_POST['user_email'];?>"/><br/><br/>
        </p>
        <p>
            What would you like your nickname to be? : *<br/>
            <input type="text" required="required" name="user_nickname" id="user_nickname" class="input" value="<?php echo $_POST['user_nickname'];?>" /><br/><br/>
        <p>
            Enter password : *<br/>
            <input autocomplete="off" required="required" name="pass1" id="pass1" type="password" class="input" value="<?php echo $_POST['user_nickname'];?>"/><br/><br/>
        </p>
        <p class="submit"><input type="submit" name="register" id="wp-register" class="button-primary" value="<?php esc_attr_e('Register'); ?>" tabindex="100" /></p>
        <input type="hidden" name="lwa" />
    </form>
</div>
<?php
}

add_filter('affiliates_before_register_affiliate', 'additional_register_fields');
function additional_register_fields(){
    $_POST['firstname'] = $_POST['nickname'];
    $_POST['pass1'] = wp_generate_password(6, false);
    $_POST['pass2'] = $_POST['pass1'];
}


 
  //  Ajax Login
  function ajax_login_init(){
	
	
   // wp_redirect($link);
	
	/* Подключаем скрипт для авторизации */
    wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/ajax-login-script.js', array('jquery') ); 
    wp_enqueue_script('ajax-login-script');
 
	/* Локализуем параметры скрипта */
    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'redirecturl' => $link,
      'loadingmessage' => __('Verified data, wait a second....')
    ));
 
    // Разрешаем запускать функцию ajax_login() пользователям без привелегий
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
  }
 
  // Выполняем авторизацию только если пользователь не вошел
  if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
  }
 
  function ajax_login(){

    // Первым делом проверяем параметр безопасности
  //  check_ajax_referer( 'ajax-login-nonce', 'security' );
 
    // Получаем данные из полей формы и проверяем их
    $info = array();
	
	
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;
	
    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
      echo json_encode(array('loggedin'=>false, 'message'=>__('<p class="errorMessage">Wrong username or password!</p>')));
    } else {
	$user = get_user_by( 'login', $_POST['username'] );
    //$user = new WP_User($current_user->id);
    if (isset($user_signon->data->wp_capabilities['subscriber']) && $user_signon->data->wp_capabilities['subscriber'] == 1){
        $link = site_url().'/?page_id=206';
    }
    elseif (isset($user_signon->data->wp_capabilities['author']) && $user_signon->data->wp_capabilities['author'] == 1){
        $link = site_url().'/?page_id=209';
    }
    elseif ($user_signon->data->wp_capabilities['administrator'] == 1 || $user_signon->data->wp_capabilities['editor'] == 1){
        $link = site_url().'/wp-admin';
    }
    elseif($user_signon->data->wp_capabilities['contributor'] == 1){
        $link = site_url().'/?page_id=1351';
    }
	
      echo json_encode(array('loggedin'=>true, 'message'=>__('<p class="succesMessage">Excellent! There is a redirect ...</p>'),'link'=>$link));
	  
    }
 
    die();
  }
  
	function my_login($username,$password)
	{
		$info = array();
		
		$info['user_login'] = $username;
		$info['user_password'] = $password;
		$info['remember'] = true;
		
		$user_signon = wp_signon( $info, false );
		if ( is_wp_error($user_signon) )
			return false;
		else
			return true;
	}
  
  function delete_text_in_p_tag($text)
  {
	return preg_replace('#(<p>.*</p>)#s', '', $text);
  }


	function loock_booked_user_before($id)
	{
		global $wpdb;
		
		$query = "SELECT `person_id` FROM `wp_dbem_people` WHERE `wp_id` = " . $id;
		$myrows = $wpdb->get_results($query);

		$person_id = $myrows[0]->person_id;

		if (empty($person_id))
		{
			$person_id = $id;
		}
		
		$query_check_booking_before = "SELECT 
				COUNT(*) as count_bookings 
			FROM `wp_dbem_bookings` 
			WHERE 
				`person_id` = " . $person_id;
		
		$rows_result = $wpdb->get_results($query_check_booking_before);
		return $rows_result[0]->count_bookings;
	}


add_shortcode( 'account_details', 'account_details' );

function account_details(){
    global $current_user;
    $output = '<div class="profile_form_table">
					<div class="profile_form_tr">
						<div class="profile_form_td">Your Nickname <div>*</div></div>
						<div class="profile_form_td">
							<input type="text" value="'.$current_user->nickname.'">
						</div>
					</div>
					<div class="profile_form_tr">
						<div class="profile_form_td">Skype Name <div>*</div></div>
						<div class="profile_form_td">
							<input type="text" value="'.$current_user->what_is_your_skype_name.'">
						</div>
					</div>
					<div class="profile_form_tr">
						<div class="profile_form_td">Polycom Number <div></div></div>
						<div class="profile_form_td">
							<input type="text" value="'.$current_user->polycom_number.'">
						</div>
					</div>
				</div>
				<div class="update_profile">
					<a href="'.get_permalink(1329).'">Update profile</a>
				</div>';
    return $output;
}

/*pass reset fnctions*/

add_shortcode( 'tc_email_form', 'tc_email_form' );

function tc_email_form($attrs){
    $output = '';
    if(isset($_POST['login'])){
        $user_login = $_POST['user_login'];
        $_POST['pass2'] = $_POST['pass1'];


        $creds = array();
        $creds['user_login'] = $user_login;
        $creds['user_password'] = $_POST['pass1'];
        $creds['remember'] = false;


        $user = wp_signon( $creds, false );

         if ( is_wp_error($user) ){
             $user->errors['incorrect_password'][0] = '<strong>ERROR</strong>: The password you entered is incorrect. ';
             $error_message = '<div class="errorMessage">';
             $error_message .= $user->get_error_message();
             $error_message .= '</div>';
         }
         else{
             setcookie ('register', 'yes',  (time() + 3600),"/");
             $_SESSION['lls_user_id'] = '';
             $_SESSION['lls_user_type'] = '';
             wp_redirect(get_bloginfo('url').'?page_id=265');
         }
    }
    else if(isset($_POST['new_password']) && !empty($_POST['new_password'])){
        $result = neo_retrieve_password();
        if ( $result === true ) {
            //Password correctly remembered
            $return['result'] = true;
            $return['message'] = __("We have sent you an email", 'login-with-ajax');
        } elseif ( strtolower(get_class($result)) == 'wp_error' ) {
            //Something went wrong
            /* @var $result WP_Error */
            $return['result'] = false;
            $return['message'] = $result->get_error_message();
        } else {
            //Undefined Error
            $return['result'] = false;
            $return['message'] = __('An undefined error has ocurred', 'login-with-ajax');
        }

    }
    $user_login = $_POST['new_password'];
    $user = get_user_by('login', $user_login);
    if(!empty($error_message)){
        $output .= $error_message;
    }

    $msg = __("Enter your email");
    $output .=  '<form name="sentmail"  action="#" method="post">
                    <div class="formRow">
                        <input type="hidden" name="user_login" class="input" value="'.$_POST['new_password'].'"/>
                    </div>

                     <div id="forgot-pass-email" style="display:none;">
                     <div class="succesMessage" style="display:none;"></div>
                        <div class="formRow">
                            <input type="text" class="defaultInput" name="user_login_ps" id="lwa_user_remember" value="'.$msg.'" onfocus="if(this.value == \''.$msg.'\'){this.value = \'\';}" onblur="if(this.value == \'\'){this.value = \''.$msg.'\'}" />
                        </div>
                        <div class="formRow leftAlign modal_form_buttons">
                            <input type="submit" value="Get New Password" class="defaultButton" name="new_password">
                            <div class="link_button"><a id="cancel-lost-password" href="#" class="linkButton" >'.__('Cancel').'</a></div>
                        </div>
                    </div>
                </form>
                </div>';

    return $output;
}

function neo_retrieve_password() {
    global $wpdb, $current_site;

    $errors = new WP_Error();

    if ( empty( $_POST['user_login_ps'] ) )
        $errors->add('empty_username', __('<strong>ERROR</strong>: Enter e-mail address.'));

    $user_data = get_user_by_email(trim($_POST['user_login_ps']));

     if ( empty($user_data) )
			
            $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));


    do_action('lostpassword_post');

    if ( $errors->get_error_code() )
        return $errors;

    if ( !$user_data ) {
        $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid e-mail.'));
        return $errors;
    }

    // redefining user_login ensures we return the right case in the email
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

    do_action('retreive_password', $user_login);  // Misspelled and deprecated
    do_action('retrieve_password', $user_login);

    $allow = apply_filters('allow_password_reset', true, $user_data->ID);

    if ( ! $allow )
        return new WP_Error('no_password_reset', __('Password reset is not allowed for this user'));
    else if ( is_wp_error($allow) )
        return $allow;

    $key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
    if ( empty($key) ) {
        // Generate something random for a key...
        $key = wp_generate_password(20, false);
        do_action('retrieve_password_key', $user_login, $key);
        // Now insert the new md5 key into the db
        $wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
    }
    $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
    $message .= network_site_url() . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    $message .= '<' . network_site_url("?page_id=1384&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

    if ( is_multisite() )
        $blogname = $GLOBALS['current_site']->site_name;
    else
        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $title = sprintf( __('[%s] Password Reset'), $blogname );

    $title = apply_filters('retrieve_password_title', $title);
    $message = apply_filters('retrieve_password_message', $message, $key);

    if ( $message && !wp_mail($user_email, $title, $message) )
        wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );

    return true;
}

add_shortcode( 'reset_password', 'reset_password_shortcode' );

function neo_check_password_reset_key($key, $login) {
    global $wpdb;

    $key = preg_replace('/[^a-z0-9]/i', '', $key);

    if ( empty( $key ) || !is_string( $key ) )
        return new WP_Error('invalid_key', __('Invalid key'));

    if ( empty($login) || !is_string($login) )
        return new WP_Error('invalid_key', __('Invalid key'));

    $user = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $key, $login));

    if ( empty( $user ) )
        return new WP_Error('invalid_key', __('Invalid key'));

    return $user;
}

function reset_password_shortcode(){
    $user = neo_check_password_reset_key($_GET['key'], $_GET['login']);

    $output = "<script type='text/javascript' src='".trailingslashit(get_option('siteurl'))."wp-includes/js/jquery/jquery.color.js?ver=2.0-4561'></script>";
    $output .= "<script type='text/javascript'>
                        /* <![CDATA[ */
                            var pwsL10n = {
                            empty: '".esc_js( __( 'Strength indicator' ) )."',
                            short: '". esc_js( __( 'Very weak' ) )."',
                            bad: '".esc_js( __( 'Weak' ) )."',
                            good: '".esc_js( _x( 'Medium', 'password strength' ) )."',
                            strong: '".esc_js( __( 'Strong' ) )."',
                            mismatch:'".esc_js( __( 'Mismatch' ) )."'
                         }
                /* ]]> */
                </script>";

    $output .= "<script type='text/javascript' src='".trailingslashit(get_option('siteurl'))."wp-admin/js/password-strength-meter.js?ver=20070405'></script>";
    $output .= "<script type='text/javascript'>
                    function check_pass_strength ( ) {

                        var pass = jQuery('#pass1').val();
                        var pass2 = jQuery('#pass2').val();
                        var user = jQuery('#user_login').val();

                        // get the result as an object, i'm tired of typing it
                        var res = jQuery('#pass-strength-result');

                        var strength = passwordStrength(pass, user, pass2);

                        jQuery(res).removeClass('short bad good strong');

                        if ( strength == 2 ) {
                            jQuery(res).addClass('bad');
                            jQuery(res).html( pwsL10n.bad );
                        }
                        else if ( strength == 3 ) {
                            jQuery(res).addClass('good');
                            jQuery(res).html( pwsL10n.good );
                        }
                        else if ( strength == 4 ) {
                            jQuery(res).addClass('strong');
                            jQuery(res).html( pwsL10n.strong );
                        }
                        else if ( strength == 5) {
                            jQuery(res).addClass('short');
                            jQuery(res).html( pwsL10n.mismatch );
                        }
                        else {
                            // this catches 'Too short' and the off chance anything else comes along
                            jQuery(res).addClass('short');
                            jQuery(res).html( pwsL10n.short );
                        }

                    }

                    jQuery(function($) {
                        $('#pass1').keyup( check_pass_strength )
                        $('#pass2').keyup( check_pass_strength )
                        $('.color-palette').click(function(){
                        $(this).siblings('input[name=admin_color]').attr('checked', 'checked')
                        });
                    } );

                </script>";
    $output .= '<div class="loginFormBlock">';


    $errors = '';
    if ( isset($_POST['pass1']) && $_POST['pass1'] != $_POST['pass2'] ) {
        $errors = new WP_Error('password_reset_mismatch', __('The passwords do not match.'));
    }

    if ( is_wp_error($user) ) {
        $output .= '<p class="errorMessage">' . __('Sorry, that key does not appear to be valid.') . '</p>';
    }
    elseif ( isset($_POST['pass1']) && !empty($_POST['pass1']) && !is_wp_error($user) &&  $_POST['pass1'] == $_POST['pass2']) {
        neo_reset_password($user, $_POST['pass1']);
        $output .= '<p class="succesMessage ">' . __('Your password has been reset.') . '</p>';
    }
    else{
        if(!empty($errors)){
            $error_message = '<div class="errorMessage">';
            foreach ($errors as $error){
                foreach ($error as $err){
                    foreach($err as $e){
                        $error_message .= $e;
                    }
                }
            }

            $error_message .= '</div>';
            $output .= $error_message;
        }


        $output .= '<p class="message reset-pass">' . __('Enter your new password below.') . '</p>';

        $output .= '<form name="resetpassform" id="resetpassform" action="" method="post">
        <input type="hidden" id="user_login" value="'.$_GET['login'] .'" autocomplete="off" />
        <div style="margin-bottom:30px;">
            <label>
            <input type="password" name="pass1" id="pass1" class="input defaultInput" size="20" value="" autocomplete="off"  placeholder="New PASSWORD"/></label>
        </div>
        <div style="margin-bottom:30px;">
            <label>
            <input type="password" name="pass2" id="pass2" class="input defaultInput" size="20" value="" autocomplete="off" placeholder="Confirm new PASSWORD"/></label>
        </div>

        <div id="pass-strength-result" class="hide-if-no-js">'. __('Strength indicator').'</div>
        <div class="description indicator-hint">'. __('Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).').'</div>

        <br class="clear" />
        <div class="submit" style="margin-bottom:30px;"><input type="submit" name="wp-submit" id="wp-submit" class="defaultButton" value="'. __('Reset Password').'" tabindex="100" /></p>
        </form></div>';
    }


    return $output;
}

function neo_reset_password($user, $new_pass) {
    do_action('password_reset', $user, $new_pass);

    wp_set_password($new_pass, $user->ID);

    wp_password_change_notification($user);
}

/*end of pass reset functions*/

add_shortcode('modal_feedback', 'modal_feedback_shortcode');

function modal_feedback_shortcode(){
    global $current_user;
    $email = '';
    if (!empty($current_user->user_login)){
        $email = $current_user->user_email;
    }

    $output = '<div class="modal_wrap modal_feedback" id="modal_feedback">
    <div class="modal_body">
            <div class="modal_close"></div>
            <div class="modal_title">Provide service feedback</div>
            <form method="post" action="" id="feedback_form">
            <div class="modal_form2_wrap">
                <div class="modal_form2_tr clearfix">
                    <div class="modal_form2_td">
                        Your email address <span>*</span>
                    </div>
                    <div class="modal_form2_td">
                        <input type="text" name="z_email" value="'.$email.'" class="delete_error" id="email_feedback" value="Email">
                    </div>
                </div>
                <div class="modal_form2_tr clearfix">
                    <div class="modal_form2_td large">
                        <div>
                            <span>*</span> Where 10 is competely satisfied,
                                           how satisfied are you with this service?
                        </div>
                    </div>
                    <div class="modal_form2_td">
                        <div class="dropdown_wrap">
                            <div class="dropdown_value">10</div>
                            <ul class="dropdown_list">
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                                <li>6</li>
                                <li>7</li>
                                <li>8</li>
                                <li>9</li>
                                <li>10</li>
                            </ul>
                        </div>
                        <input type="hidden" name="grade" class="grade" value="10" />
                    </div>
                </div>
                <div class="modal_form2_tr clearfix">
                    <div class="modal_form2_td">
                        Your message <span>*</span>
                    </div>
                    <div class="modal_form2_td">
                        <textarea name="z_msg" id="feedback_text" class="delete_error" >Message</textarea>
                    </div>
                </div>
            </div>
            <div class="modal_feedback_footer clearfix">
                <div class="modal_feedback_button" id="send_feedback">Send feedback</div>
                <input type="hidden" value="Send feedback" name="action">
            </div>
        </form>
        </div>
    </div>';
    return $output;
}

wp_localize_script( 'jquery', 'neoajaxurl', array('neoajaxurl' => admin_url('admin-ajax.php')));

add_action('wp_ajax_get_tutor_calendar', 'get_tutor_calendar');
add_action('wp_ajax_nopriv_get_tutor_calendar', 'get_tutor_calendar');

function get_tutor_calendar(){
    if(!empty($_POST['lid'])){
        echo do_shortcode('[events_weeklycalendar location_id='.$_POST['lid'].']');
    }

    die;
}


add_action('wp_ajax_get_user_balance_ajax', 'get_user_balance_ajax');
add_action('wp_ajax_nopriv_get_user_balance_ajax', 'get_user_balance_ajax');

function get_user_balance_ajax(){
    if(!empty($_POST['user_id'])){
        echo balans($_POST['user_id']);
    }

    die;
}
add_action('wp_logout', 'go_home');

function go_home(){
    wp_redirect( home_url() );
    exit();
}