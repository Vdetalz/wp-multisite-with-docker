var slider_fix;
var urlVideoHP;
jQuery(document).ready(function() {

    /* Progress bar on My accoint page */
    if(jQuery('#statbar1').length > 0){
        for(i = 1; i< 11; i++){

            if(parseFloat(jQuery('#statbar'+i).find('span').html()) != 0){

                jQuery("#statbar"+i).progressbar({
                    value: parseFloat(jQuery('#statbar'+i).find('span').html())
                });
                jQuery("#statbar"+i).find( ".ui-progressbar-value").css('background','#56a7f3');
                jQuery("#statbar"+i).find( ".ui-progressbar-value").css('opacity','1');

            } else {
                jQuery("#statbar"+i).css('display','none');
            }

        }

        jQuery(".progressbar").css({ 'background': '#cccccc' });
    }


    jQuery('#email_feedback').on({
		focus:function(){
			if (jQuery(this).val() == 'Email'){
                jQuery(this).val('');
            }
		}
	});
	
	jQuery('#feedback_text').on({
		focus : function(){
			if (jQuery(this).val() == 'Message'){
                jQuery(this).val('');
            }
		}
	});
	
	/* SERVISE PAGE OPEN REGISTER POPAP IF USER UNREGISTER */
	jQuery('a.service_a').on('click',function(e) {
		if (jQuery('.header_button_reg').length)
		{
			e.preventDefault();
			jQuery('.header_button_reg').click();
			
			return false;
		}
	
	})
	/* END SERVISE PAGE OPEN REGISTER POPAP IF USER UNREGISTER */	
	
	// удалить заказанные уроки из личного кабинета
	jQuery('.booked_remove').on('click', function() {
		var b_id = jQuery(this).data('id');
		
		var my_this = jQuery(this);
		
		jQuery.ajax({
			type: 'POST',
			url: ROOT_URL + '/ajax/remove_my_booking.php',
			data:  {
				b_id: b_id
			},
			success: function(data){
				if (data == 'good')
					my_this.parent().parent().fadeOut(200);
			}
		});
	});


    jQuery(document).on('click', '.calendar_block_top', function(e) {
            var parent = jQuery(this).closest('.calendar_block_wrap');

            if (parent.hasClass('active'))
            {
                parent.removeClass('active');
                jQuery(this).find('.full').removeClass('down');
                jQuery(this).next().css({display: 'block'}).slideUp(500);
            }
            else
            {
                parent.addClass('active');
                jQuery(this).find('.full').addClass('down');
                jQuery(this).next().css({display: 'none'}).slideDown(500);
            }
        }
    );

	jQuery(document).on('focus', '.booker-comment', function(e) {
        if( jQuery(this).text() == 'Add your comment here' ) {
            jQuery('.booker-comment').val('');
        }
    })

    jQuery(document).on('click', '.modal_wrap .event_link', function(e) {
            jQuery('.modal_wrap .lesson_info').css('display', 'block');
            jQuery('.modal_wrap .step2-body .message').html('');
            jQuery('.booker-comment').val('Add your comment here');

            var first_step = jQuery('.step1-body');
            var second_step = jQuery('.step2-body');

            first_step.removeClass('active');
            second_step.addClass('active');

            var first_step_tab = jQuery('.step1-tab');
            var second_step_tab = jQuery('.step2-tab');

            first_step_tab.removeClass('active');
            second_step_tab.addClass('active');
            jQuery('.lesson-date').html(jQuery(this).attr('data-date'));
            jQuery('.lesson-time').html(jQuery(this).text());
            jQuery('.event_id').val(jQuery(this).attr('data-event-id'));
            jQuery('.event_time').val(jQuery(this).attr('data-event-time'));
            return false;
        }
    );

    jQuery(document).on('click', '.modal_wrap .book_lesson', function(e) {
            var total_user_credits = jQuery('.credits_block').text();
            var total_number = total_user_credits.split(' ');
            var credits_number = total_number[0];
            var is_book = true;
            var parent = jQuery(this).closest('.modal_wrap');

            // number credits at user
            credits_number = parseFloat(credits_number);
            // number credits at user
            credits_number = parseFloat(credits_number);

            var learn_type = parseInt(parent.find('.learn_type').val());

            //check 50 or 25 minuts lesson
            var check_type = jQuery.trim(parent.find('.modal_lesson_teacher_time').text());
            check_type = parseFloat(check_type.substring(0,2));

            if (check_type == 50)// type lesson 50 min
            {
                if ( learn_type == 1 ) // privat
                {
                    if( credits_number >= 1.0)
                    {
                        is_book = true;
                    }
                    else
                    {
                        is_book = false;
                    }
                }
                else // classroom
                {
                    if( credits_number >= 2.0)
                    {
                        is_book = true;
                    }
                    else
                    {
                        is_book = false;

                    }
                }
            }
            else
            {
                if ( learn_type == 1 ) // privat
                {
                    if( credits_number >= 0.5)
                    {
                        is_book = true;
                    }
                    else
                    {
                        is_book = false;
                    }

                }
                else // classroom
                {
                    if( credits_number >= 1.0)
                    {
                        is_book = true;
                    }
                    else
                    {
                        is_book = false;
                    }
                }
            }
            var time = parseInt(jQuery('#booker_time').val()) + 60*60*12;

            if (is_book){
                if(time > jQuery('.event_time').val()){
                    alert('Lessons unavailable for booking within 12 hours of the lesson start time.');
                }
                else{
                    _gaq.push(['_trackEvent', 'Confirm Booking', 'Confirm Booking']);
                    if(jQuery('#booker_skype').val() == '' &&  jQuery(".communication").val() == 1 && jQuery('#bookerSkype_hidden').val() == ''){
                        jQuery( "#skype-dialog" ).dialog({
                            resizable: false,
                            modal:true,
                            buttons: {
                                "Confirm booking": function() {
                                    if (jQuery("#bookerSkype").val() != ""){
                                        jQuery("#bookerSkype_hidden").val(jQuery("#bookerSkype").val());
                                        jQuery( this ).dialog( "close" );
                                        _gaq.push(['_trackEvent', 'Confirm booking click', 'Confirm booking click']);
                                        BookingLesson(jQuery(this).parent().parent());
                                    }
                                    else{
                                        jQuery( this ).dialog( "close" );
                                    }
                                },
                                Cancel: function() {
                                    _gaq.push(['_trackEvent', 'Cancel button click', 'Cancel button click']);
                                    jQuery( this ).dialog( "close" );
                                }
                            }
                        });
                    }
                    else if(jQuery('#booker_polycom').val() == '' && jQuery(".communication").val() == 2 && jQuery('#bookerPolycom_hidden').val() == ''){
                        jQuery( "#polycom-dialog" ).dialog({
                            resizable: false,
                            modal:true,
                            buttons: {
                                "Confirm booking": function() {
                                    if (jQuery("#bookerPolycom").val() != ""){
                                        jQuery("#bookerPolycom_hidden").val(jQuery("#bookerPolycom").val());
                                        jQuery( this ).dialog( "close" );
                                        _gaq.push(['_trackEvent', 'Confirm booking click', 'Confirm booking click']);
                                        BookingLesson(jQuery(this).parent().parent());
                                    }
                                    else{
                                        jQuery( this ).dialog( "close" );
                                    }
                                },
                                Cancel: function() {
                                    _gaq.push(['_trackEvent', 'Cancel button click', 'Cancel button click']);
                                    jQuery( this ).dialog( "close" );
                                }
                            }
                        });
                    }
                    else{

                        BookingLesson(jQuery(this).parent().parent());
                    }
                }
            }
            else{
                jQuery('.show_credits_popap').click();
            }

        }
    );

	/* Фиксируем высоту слайдера по ширине фотографии */
	slider_fix = setInterval(function() {
		var slider_height = jQuery('.slider_block.active').height();
		if (slider_height > 20)
		{
			if (slider_height > 560)
			{
				slider_height = 560;
			}

			jQuery('.slider_top_wrap, .slider_fix').css({height: slider_height});
			clearInterval(slider_fix);
		}
	}, 0);

	jQuery(window).on({
		resize: function() {
			var slider_height = jQuery('.slider_block.active').height();
			if (slider_height > 560)
			{
				slider_height = 560;
			}

			jQuery('.slider_top_wrap, .slider_fix').css({height: slider_height});
		}
	});

	/* Вверхний слайдер */
	jQuery('.slider_right, .slider_left').on({
		mouseenter: function() {
			jQuery(this).closest('.slider_wrap').addClass('fix');
		},
		mouseleave: function() {
			jQuery(this).closest('.slider_wrap').removeClass('fix');
		}
	});

	jQuery('.slider_right').on({
		click: function() {
			
			if (jQuery('.slider_wrap').hasClass('fix'))
			{
				return false;
			}

			jQuery('.slider_wrap').addClass('fix');
			
			var active = jQuery('.slider_block.active');
			var slider_move = active.next();
			if (slider_move.length == 0)
			{
				slider_move = jQuery('.slider_block:eq(0)');
			}

			var left = active.width();

			active.animate({left: left * (-1)}, 500, function() {
				jQuery(this).removeClass('active');
			});
			
			slider_move.css({left: left}).animate({left: 0}, 500, function() {
				jQuery(this).addClass('active');

				jQuery('.slider_wrap').removeClass('fix');
			});
		}
	});

	jQuery('.slider_left').on({
		click: function() {
			
			if (jQuery('.slider_wrap').hasClass('fix'))
			{
				return false;
			}

			jQuery('.slider_wrap').addClass('fix');

			var active = jQuery('.slider_block.active');
			var slider_move = active.prev();
			if (slider_move.length == 0)
			{
				slider_move = jQuery('.slider_block:last-child');
			}

			var left = active.width();

			active.animate({left: left}, 500, function() {
				jQuery(this).removeClass('active');
			});
			
			slider_move.show().css({left: left * (-1)}).animate({left: 0}, 500, function() {
				jQuery(this).addClass('active');

				jQuery('.slider_wrap').removeClass('fix');
			});
		}
	});

	setInterval(function() {
		if (!jQuery('.slider_wrap').hasClass('fix'))
		{
			
			if (jQuery('.modal_wrap:visible').length)
			{
				return false;
			}

			jQuery('.slider_right').click();
		}
	}, 10000);

	jQuery('.slider_block_full_reg').on({
		click: function() {
			jQuery('#reg_email').val('Your email address').removeClass('focus');
			jQuery('#reg_nickname').val('Prefered nickname').removeClass('focus');			
			jQuery('.modal_form_wrap_error').hide();
			
			var modal = jQuery('#modal_registry');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_registry .modal_body')[0].offsetHeight / 2);

			jQuery('#modal_registry .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);
		}
	});

	jQuery('.slider_block_full_query').on({
		click: function() {
			jQuery('#query_email').val('YOUR EMAIL');
			jQuery('#your_message_query').val('YOUR MESSAGE');
			var modal = jQuery('#modal_query');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_query .modal_body')[0].offsetHeight / 2);
	
			jQuery('#modal_query .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);
		}
	});

	jQuery('.video_block_icon').on({
		click: function() {
			var modal = jQuery('#modal_video');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_video .modal_body')[0].offsetHeight / 2);
	
			jQuery('#modal_video .modal_body').css({marginTop: top});
			jQuery('#modal_video .modal_form_wrap').html('<iframe width="640" height="400" src="' + urlVideoHP + '?rel=0&fs=1&controls=0&showinfo=0&autoplay=1&wmode=opaque" frameborder="0" wmode="opaque" allowfullscreen></iframe>');
			modal.animate({opacity: 1}, 500);
            return false;
		}
	});

	/* Нижний слайдер */
	jQuery('.slider_bottom_right').on({
		click: function() {
			var active = jQuery('.slider_bottom_block.active');
			var slider_move = active.next();
			if (slider_move.length == 0)
			{
				slider_move = jQuery('.slider_bottom_block:eq(0)');
			}

			active.animate({
				left: '-707px'
			}, 500, function() {
				jQuery(this).removeClass('active');
			});

			slider_move.css({left: '707px'}).addClass('active').animate({
				left: '0px'
			}, 500);
		}
	});

	jQuery('.slider_bottom_left').on({
		click: function() {
			var active = jQuery('.slider_bottom_block.active');
			var slider_move = active.prev();
			if (slider_move.length == 0)
			{
				slider_move = jQuery('.slider_bottom_block:last-child');
			}

			active.animate({
				left: '707px'
			}, 500, function() {
				jQuery(this).removeClass('active');
			});

			slider_move.css({left: '-707px'}).addClass('active').animate({
				left: '0px'
			}, 500);
		}
	});

	/* Форма подписаться */
	jQuery('#form_subscribe_email').on({
		focus: function() {
			if (jQuery(this).val() == 'YOUR EMAIL')
			{
				jQuery(this).val('').addClass('focus');
			}
		},
		focusout: function() {
			if (jQuery(this).val() == '')
			{
				jQuery(this).val('YOUR EMAIL').removeClass('focus');
			}
		}
	});

	jQuery('#coupon_code').on({
		focus: function() {
			if (jQuery(this).val() == 'Have coupon?')
			{
				jQuery(this).val('').addClass('focus');
			}
		},
		focusout: function() {
			if (jQuery(this).val() == '')
			{
				jQuery(this).val('Have coupon?').removeClass('focus');
			}
		}
	});
	
	jQuery('#form_subscribe_text').on({
		focus: function() {
			if (jQuery(this).val() == 'MESSAGE')
			{
				jQuery(this).val('').addClass('focus');
			}
		},
		focusout: function() {
			if (jQuery(this).val() == '')
			{
				jQuery(this).val('MESSAGE').removeClass('focus');
			}
		}
	});

	/* Категории на поиске */
	jQuery('.content_filter_category > div').on({
		click: function() {
			jQuery('.content_filter_category > div.active').removeClass('active');
			jQuery(this).addClass('active');
		}
	});

	jQuery('.checkbox_wrap').on({
		click: function() {
			jQuery(this).toggleClass('active');
            if (jQuery(this).hasClass('active')){
                jQuery('.pay_submit_wrap .input-checkbox').attr('checked', 'checked') ;
            }
            else{
                jQuery('.pay_submit_wrap .input-checkbox').removeAttr('checked') ;
            }
		}
	});
		
	jQuery('#page_search_input').on({
		focus: function() {
			if (jQuery(this).val() == 'Search for lesson by keyword')
			{
				jQuery(this).val('').addClass('focus');
			}
		},
		focusout: function() {
			if (jQuery(this).val() == '')
			{
				jQuery(this).val('Search for lesson by keyword').removeClass('focus');
			}
		}
	});

	/* Функции для закрытия попапов */
	jQuery('.modal_close').on({
		click: function() {
			jQuery('.modal_wrap').fadeOut(500);
		}
	});

	jQuery('.modal_body').on({
		mouseleave: function() {
			jQuery(this).closest('.modal_wrap').removeClass('fix');
		},
		mouseenter: function() {
			jQuery(this).closest('.modal_wrap').addClass('fix');
		}
	});

	jQuery('.modal_wrap').on({
		click: function() {
			if (!jQuery(this).hasClass('fix'))
			{
				jQuery(this).fadeOut(500);
			}
		}
	});

	jQuery('.modal_form_wrap input[type="text"]').each(function() {
		jQuery(this).data("default", jQuery(this).val());
		jQuery(this).on({
			focus: function() {
				if (jQuery(this).val() == jQuery(this).data("default"))
				{
					jQuery(this).val('').addClass('focus');
				}
			},
			focusout: function() {
				if (jQuery(this).val() == '')
				{
					jQuery(this).val(jQuery(this).data("default")).removeClass('focus');
				}
			}
		});
	});

	/* Попап Авторизации */
	jQuery('.header_button_log').on({
		click: function() {
			var modal = jQuery('#modal_login');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_login .modal_body')[0].offsetHeight / 2);
			
			jQuery('#modal_login .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);
		}
	});

	/* Попап Регистрации */
	jQuery('.header_button_reg').on({
		click: function() {
            _gaq.push(['_trackEvent', 'Open registration form', 'Open registration form']);
			jQuery('#reg_email').val('Your email address').removeClass('focus');
			jQuery('#reg_nickname').val('Prefered nickname').removeClass('focus');			
			jQuery('.modal_form_wrap_error').hide();

			var modal = jQuery('#modal_registry');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_registry .modal_body')[0].offsetHeight / 2);

			jQuery('#modal_registry .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);

		}
	});
	
	
	
	/* Tabs lessons */
	jQuery('.content_filter_category div ').click(function(){
		jQuery('.tab').hide();
		var name_block_show = jQuery(this).attr('data-default');
		jQuery('#' + name_block_show).show();
	});
	
	
	jQuery('#register_ajax').on('click',function(){
		var status_reg = true;
		var email = jQuery.trim(jQuery('#reg_email').val());
		var nickname = jQuery.trim(jQuery('#reg_nickname').val());
		var password = jQuery.trim(jQuery('#reg_password').val());
		var verify_password = jQuery.trim(jQuery('#verify_password').val());
		var r = /^[0-9a-z_\.]+@[0-9a-z_^\.]+\.[a-z]{2,6}$/i;
		
		
		if( email == '' || !r.test(email) )
		{
			jQuery('.modal_form_wrap_error').html('<p class="errorMessage">Email address incorrect</p>').show();
            status_reg = false;
		}
		
		if( password == '' || verify_password != password )
		{
            if(status_reg == false){
                jQuery('.modal_form_wrap_error').html(jQuery('.modal_form_wrap_error').html() + '<p class="errorMessage">Check password</p>').show();
            }
            else{
                jQuery('.modal_form_wrap_error').html('<p class="errorMessage">Check password</p>').show();
            }

            status_reg = false;
		}
		
		if ( status_reg == true )
		{
            jQuery('#modal_registry .modal_form_wrap_error').css('display', 'none');
            jQuery('#modal_registry .load').html('<div><img src="'+SITE_URL+'/wp-content/uploads/loader.gif"></div>');
            jQuery('#modal_registry .noticeMessage').html('Wait a second....');
            jQuery('#modal_registry .noticeMessage').show();
			 jQuery.ajax({
			  type: 'POST',
			  url: ROOT_URL + '/ajax/reg.php',
			  data:  'email=' + email + '&nickname=' + nickname + '&password=' + password,
			  success: function(data){
                  jQuery('#modal_registry .load').html('');
                  jQuery('#modal_registry .noticeMessage').css('display', 'none');
					if ( data.search('Sucessfully registration') != -1)
					{
                        _gaq.push(['_trackEvent', 'Registration', 'Registration']);
						jQuery('.modal_form_wrap_sucessfully').html(data).show();
						jQuery('#modal_registry').fadeOut(500);
						jQuery.cookie('welcome' , 'welcome');
						window.location.href=SITE_URL+'/?page_id=265';
					}
					else
					{
						jQuery('.modal_form_wrap_error').html(data).show();
					}
				}
			});
		}
	});
	
	/* show welcome popap to resister user */
    if( jQuery.cookie('welcome') && jQuery.cookie('welcome') != '' )
	{
		var margin_top_popap_welcome = jQuery(window).height()/2 - jQuery('.add_margin_top').height()/2 - 90;
		jQuery('.add_margin_top').css({ marginTop : margin_top_popap_welcome });
		jQuery('#welcomes_popap').show();
		jQuery.cookie('welcome','');
	}
	/* show welcome popap to resister user */

    jQuery('.modal_button').click(function(){
        jQuery('#welcomes_popap').hide();
        return false;
    })

	var r = /^[0-9a-z_\.]+@[0-9a-z_^\.]+\.[a-z]{2,6}$/i; // global regex
	
	jQuery('#send_letter').click(function(e){
		e.preventDefault();
		var status = true;
		var email_user = jQuery.trim(jQuery('#form_subscribe_email').val());
		var message_user = jQuery.trim(jQuery('#form_subscribe_text').val());
		
		if ( email_user  == '' || email_user == 'YOUR EMAIL' || !r.test(email_user))
		{
			jQuery('#form_subscribe_email').addClass('error');
			body_msg('Check your email address' , 5000 , 'fail');
			status = false;
		}
		
		if ( message_user  == '' || message_user == 'MESSAGE' )
		{
			jQuery('#form_subscribe_text').addClass('error');
			body_msg('Email are empty' , 5000 , 'fail');
			status = false;
		}
		
		if( status == true )
		{
            jQuery('#footer_form').submit();
		}
   });

    jQuery('#query_email').on({
        focus:function(){
            if (jQuery(this).val() == 'YOUR EMAIL') jQuery(this).val('');
        },
        focusout: function(){
            if (jQuery(this).val() == '') jQuery(this).val('YOUR EMAIL');
        }
    });

    jQuery('#your_message_query').on({
        focus : function(){
            if (jQuery(this).val() == 'YOUR MESSAGE') jQuery(this).val('');
        },
        focusout: function(){
            if (jQuery(this).val() == '')  jQuery(this).val('YOUR MESSAGE');
        }
    });


    jQuery('#send_letter_query_popap').click(function(e){
		var status = true;
		var email_user = jQuery.trim(jQuery('#query_email').val());
		var message_user = jQuery.trim(jQuery('#your_message_query').val());
		
		if ( email_user  == '' || email_user == 'YOUR EMAIL' || !r.test(email_user))
		{
			jQuery('#query_email').addClass('error');
			body_msg('Check your email address' , 5000 , 'fail');
			status = false;
			e.preventDefault();
		}
		
		if ( message_user  == '' || message_user == 'YOUR MESSAGE' )
		{
			jQuery('#your_message_query').addClass('error');
			body_msg('Message is empty' , 5000 , 'fail');
			status = false;
			e.preventDefault();
		}
   });
   
    jQuery('#forgot-pass-email input[type="submit"]').click(function(e){
		var email_user = jQuery.trim(jQuery('#lwa_user_remember').val());		
		if ( email_user  == '' || email_user == 'ENTER YOUR EMAIL' || !r.test(email_user))
		{
			jQuery('#query_email').addClass('error');
			body_msg('Check your email address' , 5000 , 'fail');
			status = false;
			e.preventDefault();
		}
        else{
            jQuery('.succesMessage').text('Check your email to reset your password');
            jQuery('.succesMessage').css('display', 'block');
        }
   });
   
   jQuery('#query_email , #your_message_query').on( 'focus' , function(){
		jQuery(this).removeClass('error');
   });
   
   jQuery('.credit_block_button , .show_credits_popap').on({
		click: function() {
            _gaq.push(['_trackEvent', 'Open credit popup', 'Open credit popup']);
			var modal = jQuery('.popap_credits_show');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('.popap_credits_show .modal_body')[0].offsetHeight / 2);
			if (top <= 50)
			{
				top = 50;
			}
			
			jQuery('.popap_credits_show .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);
		}
	});

   jQuery('#form_subscribe_email').focus(function(){
		jQuery(this).removeClass('error');
   });
   
   jQuery('#form_subscribe_text').focus(function(){
		jQuery(this).removeClass('error');
   });
   
   /* login */
   jQuery('#login').on('submit', function(e){
	e.preventDefault();
       jQuery('.modal_form_wrap_error').html('');
       var name = jQuery.trim(jQuery('#login input[name=username]').val());
       var pass = jQuery.trim(jQuery('#login input[name=password]').val());
       var status = true;
    if (name == 'Your name' || name == ''){
        jQuery('.modal_form_wrap_error').html('<p class="errorMessage">Enter your name!</p>').show();
        status = false;
    }
    if( pass == '')
    {
         jQuery('.modal_form_wrap_error').html(jQuery('.modal_form_wrap_error').html() + '<p class="errorMessage">Enter your password!</p>').show();
         status = false;
    }

    if ( status == true )
    {

    jQuery('.status_login').show().text(ajax_login_object.loadingmessage);
	
    jQuery.ajax({
      type: 'POST',
      dataType: 'json',
      url: ajax_login_object.ajaxurl,
      data: { 
        'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
        'username': jQuery('.username_input').val(), 
        'password': jQuery('.username_password').val(), 
        'security': jQuery('#security').val()
      },
      success: function(data){
        jQuery('.status_login').html(data.message);
        if (data.loggedin == true){
          document.location.href = data.link;
        }
		
      }
    });
    }
	
	/* end login */
  });
  
  jQuery('.slider_bottom_block:first').addClass('active');


	/* Списки на странице Сервис */
	jQuery('.service_block_top').on({
		click: function() {
			var parent = jQuery(this).closest('.service_block_wrap');

			if (parent.hasClass('active'))
			{
				parent.removeClass('active');
				jQuery(this).next().css({display: 'block'}).slideUp(500);
			}
			else
			{
				jQuery('.service_block_wrap.active .service_block_bottom').css({display: 'block'}).slideUp(500);
				jQuery('.service_block_wrap.active').removeClass('active');

				parent.addClass('active');
				jQuery(this).next().css({display: 'none'}).slideDown(500);
			}
		}
	});

	/* Закрываем сообщение */
	jQuery('.page_note_close').on({
		click: function() {
			jQuery('.page_note_wrap').slideUp(300, function() {
				jQuery(this).remove();
			});
		}
	});

	/* Feedback popup */
	jQuery('.booked_button').on({
		click: function() {
			var modal = jQuery('#modal_feedback');
			modal.css({display: 'block', opacity: 0});
			var top = (jQuery(window).height() / 2) - (jQuery('#modal_feedback .modal_body')[0].offsetHeight / 2);
			if (top <= 50)
			{
				top = 50;
			}
			
			jQuery('#modal_feedback .modal_body').css({marginTop: top});
			modal.animate({opacity: 1}, 500);
		}
	});

    /* Drop down */
	jQuery('.dropdown_value').on({
		click: function() {
			if (jQuery(this).hasClass('active'))
			{
				jQuery(this).next().slideUp(300);
				jQuery(this).removeClass('active');
			}
			else
			{
				jQuery(this).next().slideDown(300);
				jQuery(this).addClass('active');
			}
		}
	});


	jQuery('.dropdown_list li').on({
		click: function() {
			var value = jQuery(this).text();
			jQuery(this).closest('.dropdown_wrap').find('.dropdown_value').html(value);
			jQuery(this).closest('.dropdown_wrap').find('.dropdown_input').removeClass('active');
			jQuery(this).closest('.dropdown_list').slideUp(300);
            $('.grade').val(value);
		}
	});
	
	
	
	/* feedback */
	jQuery('#send_feedback').on( 'click', function(){
		var status = true;
		var email_feedback = jQuery.trim(jQuery('#email_feedback').val());
		var satisfied  = jQuery('.dropdown_value').text();
		var feedback_text = jQuery.trim(jQuery('#feedback_text').val());
		
		
		if ( email_feedback  == '' || !r.test(email_feedback))
		{
			jQuery('#email_feedback').addClass('error');
			body_msg('Check email address' , 5000 , 'fail');
			status = false;
		}
		
		if ( feedback_text  == '' )
		{
			jQuery('#feedback_text').addClass('error');
			body_msg('Check your message' , 5000 , 'fail');
			status = false;
		}

		if( status == true )
		{
		    jQuery('#feedback_form').submit();
		}
   	});
   
   	jQuery('.delete_error').on('focus',function(){
		if( jQuery(this).hasClass('error') )	
			jQuery(this).removeClass('error');
   	})
   	/* end feedback */

	/* hide all slide */
	jQuery('.button_hide').next().addClass('open');
	jQuery('.slider_up_down').hide();
	
	/* hide all slide */
	
	jQuery(document).on('click', '.button_hide' , function(){
		block_slide = jQuery(this).next();
		if( block_slide.hasClass('open') )
		{
			jQuery(this).find('.full').removeClass('down');
			block_slide.slideDown(500);
			block_slide.removeClass('open');
		}
		else
		{
			jQuery(this).find('.full').addClass('down');
			block_slide.slideUp(500);
			block_slide.addClass('open');
		}

	});
	
	/* tutors slide up down */
	
	/* Search */
	
	jQuery('#page_search_input').on({
		keypress: function(event) {
		if (event.which == 13)
			{
				var user_status = jQuery('.user_login').val();
				var string_searh = jQuery(this).val();
				if ( string_searh == '' ) 
					return false;
					
				
				jQuery.ajax({
					url 		: ROOT_URL + '/ajax/search.php',
					type    	: 'POST',
					data    	: 'string_searh=' + string_searh + '&user_status=' + user_status,
					success     : function(html){
						jQuery('.tab').hide();
						jQuery('#sarch_result').html(html).show();
						
					}
			}); 
				
			}
		}
	});
	
	/* end search */
	
	jQuery('.radio_button').on({
		click: function() {
			jQuery('.radio_button.active').removeClass('active');
			jQuery(this).addClass('active');
		}
	});
	
	jQuery('.pay_systems_value img').on({
		click: function() {
			jQuery(this).prev().click();
		}
	});

	
    jQuery('.CheckBoxLabelClass').on('click',function(){
		var element_status = jQuery(this).prev().is(":checked");
		if ( element_status == true)
		{
			return false;
		}
		else
		{
			jQuery('label').removeClass('LabelSelected');
			jQuery(this).addClass('LabelSelected');
		}
	});
	
	/* start open lists */
	
	
	
	/* end open lists */
	jQuery('.CheckBoxLabelClass ').first().addClass('LabelSelected');
	jQuery('.tabNavigation div:first-child').addClass('active');

	jQuery('.button-buy-tracking').on({
		click: function(event) {
			event.preventDefault();

			jQuery.get(jQuery(this).attr('href'));

			var parent_tr = jQuery(this).closest('.credit_pay_tr');
			var counter = jQuery.trim(parent_tr.find('.credit_pay_td_1').html());
			var price = jQuery.trim(parent_tr.find('.credit_pay_td_2').text());
			price = (price.split(" "))[1];

            var product_id = getParameterByName(jQuery(this).attr('href'), 'add-to-cart');
			var new_block = true;
			jQuery('.pay_tr .par_td_1').each(function() {
				if (jQuery.trim(jQuery(this).text()) == counter)
				{
					new_block = false;
					
					var block_summ_wrap = jQuery(this).closest('.pay_tr').find('.pay_block_summ');
					var block_summ = (block_summ_wrap.text()).split(" ");
					var block_summ_number = Number(block_summ[1]) + 1;
					block_summ_wrap.html('x ' + block_summ_number);
				}
			});

			if (new_block == true)
			{
				var html = jQuery('<div class="pay_tr" data-id="'+product_id+'"><div class="par_td par_td_1 rel">' + counter + '</div><div class="par_td par_td_2">USD ' + price + '</div><div class="par_td par_td_3"><div class="pay_block_calc"><div class="pay_block_plus"></div><div class="pay_block_minus"></div><div class="pay_block_summ">x 1</div></div></div><div class="par_td par_td_4">' + price + '</div></div>');
				jQuery('.pay_table').append(html);
			}
            jQuery('.update_card_button').click();
			calcCreditTable();
			return false;
		}
	});
	
	/* plus in cart row*/
	
	jQuery(document).on('click', '.pay_block_plus', function() {
		var block_summ_wrap = jQuery(this).closest('.pay_block_calc').find('.pay_block_summ');
		var block_summ = (block_summ_wrap.text()).split(" ");
		var block_summ_number = Number(block_summ[1]) + 1;
		
		if (block_summ_number < 1)
		{
			return false;
		}

        //jQuery('.update_card_button').click();
		block_summ_wrap.html('x ' + block_summ_number);

	//	calcCreditTable();
		
	});
	
	
	/* update cart button */
	jQuery(document).on( 'click' , '.update_card_button' , function(e){
		e.preventDefault();

		var data_array = [];
		jQuery('.pay_tr:not(.pay_tr:first-child)').each(function(){
			var product_id = jQuery(this).data('id');
			var product_quantity_array = jQuery(this).find('.pay_block_summ').text().split(' ');
			var product_quantity = product_quantity_array[1];

			data_array[data_array.length] = [ product_id , product_quantity ];
			
		});


        var coupon_code = jQuery('input[name=coupon_code]').val();
        if(coupon_code == 'Have coupon?'){
            coupon_code = '';
        }


		jQuery.ajax({ 
			type: 'POST',
			url: jQuery('.form_update').attr('action'),
			data: {
				data_array : data_array,
                coupon_code: coupon_code
			},
			success: function(msg){
                var IS_JSON = true;
                try
                {
                    var json = jQuery.parseJSON(msg);
                }
                catch(err)
                {
                    IS_JSON = false;
                }

                if(IS_JSON && coupon_code != ''){
                   if(json.amount){
                       jQuery('.discount-value').val(json.amount);
                   }

                    jQuery('.coupon-info').html(json.message);
                }

				calcCreditTable();
			}
		});
		
	});
	
	/* end update cart button */
	/* plus in cart row*/
	
	jQuery(document).on('click', '.pay_block_minus', function() {
		var block_summ_wrap = jQuery(this).closest('.pay_block_calc').find('.pay_block_summ');
		var block_summ = (block_summ_wrap.text()).split(" ");
		var block_summ_number = Number(block_summ[1]) - 1;
		
		var active_row = jQuery(this).closest('.pay_tr');
		
		if (block_summ_number == 0)
		{
			jQuery.get( active_row.find('a.remove').attr('href') , function(){
				active_row.fadeOut(200).remove();
                jQuery('.coupon-info').html('');
                jQuery('.discount-value').val('')
                jQuery('input[name=coupon_code]').val('Have coupon?');
                jQuery('.update_card_button').click();
				calcCreditTable();
			})
						
			return false;
		}

       // jQuery('.update_card_button').click();
		block_summ_wrap.html('x ' + block_summ_number);
		//calcCreditTable();
		
	});
	/* end minus in cart row and delete if itame = 1 */
	
});

function calcCreditTable()
{
	var summara = 0;
	jQuery('.pay_tr:not(.pay_tr_header)').each(function() {
		var block_summ_wrap = jQuery(this).find('.pay_block_summ');
		var block_summ = (block_summ_wrap.text()).split(" ");
		var block_summ_number = Number(block_summ[1]);

		var price_block = jQuery.trim(jQuery(this).find('.par_td_2').text());
		var price_number = (((price_block.split("$"))[1]).split("."))[0];
        var discount_value = jQuery('.discount-value').val();
		price_number = price_number.split(",").join("");
		price_number = parseInt(price_number);

		var summ_total = price_number * block_summ_number;
		summara += summ_total;
        if(discount_value != ''){
            summara = summara - parseFloat(discount_value);
        }

		jQuery(this).find('.par_td_4').html('USD $' + summ_total + '.00');
	});

	jQuery('.pay_footer_money span').html('USD $' + summara + '.00');
	
	
}

function showBookPopup(el, post_id, location_id){
    alert()
    if (jQuery(el).hasClass('expert_free_lesson'))
    {
        jQuery('.header_button_reg').click();
    }
    else if(jQuery(el).hasClass('add_credits')){
        jQuery('.show_credits_popap').click();
    }
    else{
        if (jQuery(el).text() == 'Book $1 Demo Lesson'){
            _gaq.push(['_trackEvent', 'Book $1 Demo Lesson', 'Book $1 Demo Lesson']);
        }
        else{
            _gaq.push(['_trackEvent', 'Book lesson', 'Book lesson']);
        }
        jQuery('.step1-body').addClass('active');
        jQuery('.step2-body').removeClass('active');
        var first_step_tab = jQuery('.step1-tab');
        var second_step_tab = jQuery('.step2-tab');

        second_step_tab.removeClass('active');
        first_step_tab.addClass('active');

        jQuery('.book_lessons_profile_wrap').html(jQuery('#tutor-info-'+post_id).html());
        jQuery('.book_lessons_profile_wrap #tutor-info-'+post_id).css('display', 'block');

        var modal = jQuery('#modal-wrap-'+post_id);
        modal.css({display: 'block', opacity: 0});


        jQuery('#modal-wrap-'+post_id+' .modalBody').css({marginTop: '50px'});
        modal.animate({opacity: 1}, 500);

        var  html = jQuery('#tutor-calendar-'+post_id).html();

            jQuery('#tutor-calendar-'+post_id).html('<div class="loader"><img src="'+SITE_URL+'/wp-content/uploads/loader.gif"></div>');

            var data = {
                action: 'get_tutor_calendar',
                lid: location_id
            }

            jQuery.post(
                neoajaxurl.neoajaxurl,
                data = data,
                function( response ) {
                    jQuery('#tutor-calendar-'+post_id).html(response);
                    jQuery.each(jQuery('.calendar_block_top'), function() {
                       /* var today = new Date();
                        var hour = today.getHours();
                        var cur_part = 'AM';
                        if (hour >= 12){
                            hour = hour - 12;
                            cur_part = 'PM';
                        }
                        var time = jQuery(this).attr('data-time');
                        var part_day = jQuery(this).attr('data-part-day');
                        var time_arr = time.split(",");

                        if (jQuery.inArray(hour.toString(), time_arr) >= 0  && cur_part == part_day){
                            jQuery(this).next().css({display: 'block'});
                            jQuery(this).find('.full').addClass('down');
                        }*/

                    });

                    jQuery.each(jQuery('.calendar_block_wrap'), function() {
                        if( jQuery(this).find('a.event_link').length ){
                            jQuery(this).find('.calendar_block_bottom').css({display: 'block'});
                            jQuery(this).find('.full').addClass('down');
                            jQuery(this).addClass('active');
                        }
                    });
                }
            );
    }
}


function BookingLesson(el){
    jQuery('.load').html('<div><img src="'+SITE_URL+'/wp-content/uploads/loader.gif"></div>');
    var data = {
        action: 'book_lesson_ajax',
        eme_eventAction: 'add_booking',
        event_id: jQuery('.event_id').val(),
        bookedSeats: jQuery('#booked-seats').val(),
        bookerEmail: jQuery('#booker-email').val(),
        bookerName: jQuery('#booker-name').val(),
        bookerComment: jQuery('.booker-comment', el).val(),
        learn_type:  jQuery('.learn_type', el).val(),
        communication: jQuery('.communication', el).val(),
        student_id: jQuery('.student_id').val(),
        bookerPolycom: jQuery('#bookerPolycom_hidden').val(),
        bookerSkype: jQuery('#bookerSkype_hidden').val()
    }

    jQuery.post(
        neoajaxurl.neoajaxurl,
        data = data,
        function( response ) {
            jQuery('.load').html('');
            jQuery('.modal_wrap .lesson_info').css('display', 'none');
            if (response.search('recorded') != -1){
                jQuery('.modal_wrap .step2-body .message').html('<div class="succesMessage" style="padding-bottom:30px;">'+response+'</div>');
                jQuery('.test_lesson_text').css('display', 'none');
                jQuery('.second_lesson_text').css('display', 'block');
            }
            else{
                jQuery('.modal_wrap .step2-body .message').html('<div class="errorMessage" tyle="padding-bottom:30px;">'+response+'</div>');
            }
            jQuery.post(
                neoajaxurl.neoajaxurl,
                data = {
                    action: 'get_user_balance_ajax',
                    user_id: USER_ID
                },
                function( response ) {
                    jQuery('.credits_block').text(jQuery.trim(response) + ' credits');
                    jQuery('.book_lesson').text('Book Lesson');
                       if(parseInt(response) <= 0){
                            jQuery('.book_lesson').addClass('add_credits');
                       }
                }
            );

        }
    );
}

function error()
{
    jQuery('.update_card_button').click();
    if(  !jQuery('.checkbox_wrap').hasClass('active') || jQuery('.pay_tr').length == '1' )
        alert('You have not accepted the terms & conditions, you must do this before you continue or cart is empty');
    else{
        if(jQuery('#user_first_name').val() == ''){
            jQuery('#purchase_first_name').removeClass('error');
            jQuery('.required_fields_dialog').dialog({
                resizable: false,
                modal:true,
                buttons: {
                    "Place Order": function() {
                        if(jQuery('#purchase_first_name').val() != ''){
                            _gaq.push(['_trackEvent', 'Place order', 'Place order']);
                            jQuery('#billing-first_name').val(jQuery('#purchase_first_name').val())
                            jQuery('.checkout').submit();
                        }
                        else{
                            jQuery('#purchase_first_name').addClass('error');
                        }

                    },

                    "Cancel": function() {
                        jQuery( this ).dialog( "close" );
                    }
                }
            });
        }
        else{
            _gaq.push(['_trackEvent', 'Place order', 'Place order']);
            jQuery('.checkout').submit();
        }

    }
}

function  SetUrlVideoHP(url){
    urlVideoHP = url;
}

function getParameterByName(url, name){
    var count   = url.indexOf(name);
    sub     = url.substring(count);
    amper   = sub.indexOf("&");

    if(amper == "-1"){
        var param = sub.split("=");
        return param[1];
    }else{
        var param = sub.substr(0,amper).split("=");
        return param[1];
    }

}