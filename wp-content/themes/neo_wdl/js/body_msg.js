function body_msg(text, time, type)
{
	if (!text) return false;
	if (!type) type = "success";

	if (jQuery('.body_msg_wrap .body_msg').text() == text)
	{
		return false;
	}

	jQuery('.body_msg_wrap').remove();

	var body_msg = jQuery('<div class="body_msg_wrap body_msg_'+type+'"><div class="body_msg">' + text + '<div class="body_msg_close"></div></div></div>');
	jQuery('body').append(body_msg);
	
	body_msg.css({top: (-1) * body_msg.height()});
	body_msg.animate({top: 0}, 300);

	body_msg.find('.body_msg_close').click(function() {
		jQuery(this).closest('.body_msg_wrap').animate({top: (-1) * jQuery(this).closest('.body_msg_wrap').height()}, 300, function() {
			jQuery(this).remove();
		});
	});

	if (time != null)
	{
		setTimeout(function() {
			jQuery('.body_msg_wrap').animate({top: (-1) * jQuery('.body_msg_wrap').height()}, 300, function() {
				jQuery(this).remove();
			});
		}, time);
	}
}