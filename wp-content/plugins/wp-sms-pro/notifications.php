<?php
	if ( ! defined( 'ABSPATH' ) ) exit;
	
	// Wordpress new version
	if(get_option('wp_notification_new_wp_version')) {
		$update = get_site_transient('update_core');
		$update = $update->updates;
		if($update[1]->current > $wp_version) {
			if(get_option('wp_last_send_notification') == false) {
				$webservice = get_option('wp_webservice');
				include_once dirname( __FILE__ ) . "/includes/classes/wp-sms.class.php";
				include_once dirname( __FILE__ ) . "/includes/classes/webservice/{$webservice}.class.php";
				$sms = new $webservice;
				$sms->to = array(get_option('wp_admin_mobile'));
				$sms->msg = sprintf(__('WordPress %s is available! Please update now', 'wp-sms'), $update[1]->current);
				$sms->SendSMS();
				update_option('wp_last_send_notification', true);
			}
		} else {
			update_option('wp_last_send_notification', false);
		}
	}
	
	// Register new user
	function wps_notification_new_user($username_id) {
		global $sms, $date;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_nrnu_tt');
		$username_info = get_userdata($username_id);
		$template_vars = array(
			'user_login'	=> $username_info->user_login,
			'user_email'	=> $username_info->user_email,
			'date_register'	=> $date,
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_nrnu_stats'))
		add_action('user_register', 'wps_notification_new_user', 10, 1);
	
	// New Comment
	function wps_notification_new_comment($comment_id, $comment_smsect){
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_gnc_tt');
		$template_vars = array(
			'comment_author'		=> $comment_smsect->comment_author,
			'comment_author_email'	=> $comment_smsect->comment_author_email,
			'comment_author_url'	=> $comment_smsect->comment_author_url,
			'comment_author_IP'		=> $comment_smsect->comment_author_IP,
			'comment_date'			=> $comment_smsect->comment_date,
			'comment_content'		=> $comment_smsect->comment_content
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_gnc_stats'))
		add_action('wp_insert_comment', 'wps_notification_new_comment',99,2);
	
	// User login
	function wps_notification_login($username_login, $username){
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_ul_tt');
		$template_vars = array(
			'username_login'	=> $username->username_login,
			'display_name'	=> $username->display_name
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_ul_stats'))
		add_action('wp_login', 'wps_notification_login', 99, 2);
	
	// Contact Form 7 Hooks
	if( get_option('wps_add_wpcf7') ) {
		add_action('wpcf7_admin_after_form', 'wps_setup_wpcf7_form'); 
		add_action('wpcf7_after_save', 'wps_save_wpcf7_form');
		add_action('wpcf7_before_send_mail', 'wps_send_wpcf7_sms');
	}
	
	function wps_setup_wpcf7_form($form) {
		$options = get_option('wpcf7_sms_' . $form->id);
		include_once dirname( __FILE__ ) . "/includes/templates/wp-sms-wpcf7-form.php";
	}
	
	function wps_save_wpcf7_form($form) {
		update_option('wpcf7_sms_' . $form->id, $_POST['wpcf7-sms']);
	}
	
	function wps_send_wpcf7_sms($form) {
		global $sms;
		$options = get_option('wpcf7_sms_' . $form->id);
		if( $options['message'] && $options['phone'] ) {
			// Replace merged Contact Form 7 fields
			/*if( defined( 'WPCF7_VERSION' ) && WPCF7_VERSION < 3.1 ) {
				$regex = '/\[\s*([a-zA-Z_][0-9a-zA-Z:._-]*)\s*\]/';
			} else {
				$regex = '/(\[?)\[\s*([a-zA-Z_][0-9a-zA-Z:._-]*)\s*\](\]?)/';
			}
			$callback = array( &$form, 'mail_callback' );
			$message = preg_replace_callback( $regex, $form, $options['message'] );*/
			$sms->to = array( $options['phone'] );
			$sms->msg = $options['message'];
			$sms->SendSMS();
		}
	}
	
	// Woocommerce Hooks
	function wps_woocommerce_new_order($order_id){
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_wc_no_tt');
		$template_vars = array(
			'order_id'	=> $order_id,
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_wc_no_stats'))
		add_action('woocommerce_new_order', 'wps_woocommerce_new_order');
	
	function wps_woocommerce_low_stock($stock) {
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_wc_ls_tt');
		$template_vars = array(
			'product_id'	=> $stock->id,
			'product_name'	=> $stock->post->post_title
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_wc_ls_stats'))
		add_action('woocommerce_low_stock', 'wps_woocommerce_low_stock');
	
	// Easy Digital Downloads Hooks
	function wps_edd_new_order($payment_id) {


		// Basic payment meta
		$payment_meta = edd_get_payment_meta( $payment_id );
		
		// Cart details
		
		$cart_items = edd_get_payment_meta_cart_details( $payment_id );
		$value = $cart_items[0]['price'];
		$product_name = $cart_items[0]['name'];
		$user_name = $payment_meta['user_info']['first_name'];
		$price_id  = $payment_meta['downloads'][0]['options']['price_id'];
		
		$meta = get_post_meta($cart_items[0]['id'], 'edd_variable_prices', true);

		$user_id = get_current_user_id();
		$hub_purchase = get_user_meta($user_id, 'hub_purchase', true);
		$tutor_purchase = get_user_meta($user_id, 'tutor_purchase', true);

		$pricing_table_row = $meta[$price_id]['pricing_table_row'];

		$cycle = get_the_title($pricing_table_row);
		$price = $cart_items[0]['price'];
		$product_name = $cart_items[0]['name'];
		$user_name = $payment_meta['user_info']['first_name'];

		
		$site_name = get_bloginfo('name'); 
	
		$msg = get_option('wpsms_edd_no_tt');
		

		$msg = str_replace('%user_name%', $user_name, $msg);
		$msg = str_replace('%site_name%', $site_name, $msg);
		$msg = str_replace('%price%', $price, $msg);
		$msg = str_replace('%product_name%', $product_name, $msg);
		$msg = str_replace('%cycle%', $cycle, $msg);

		$numbers[] = get_option('wp_admin_mobile');
		if(get_option('wp_admin_mobile_second')){
			$numbers[] = get_option('wp_admin_mobile_second');
		}

		global $sms;
		
		$sms->to = $numbers;
		$sms->msg = $msg;
		$sms->SendSMS();

	}

	if(get_option('wpsms_edd_no_stats'))
		add_action('edd_complete_purchase', 'wps_edd_new_order');

	//Events 
	function wps_event_new_booking($event) {

		$EM_Booking = $event->get_booking();
    	$EM_Event = $EM_Booking->get_event();
    	
    	$user = wp_get_current_user();
    	$user = $user->user_firstname . ' ' . $user->user_lastname;

    	$post = get_post($EM_Event->post_id);
    	$phone = get_user_meta($post->post_author, 'rpr_phone', true);
    	$timezone = dut_get_user_timezone($post->post_author)*60*60; //teacher timezone (timestamp)
		$time_start =  strtotime($EM_Event->event_start_date . $EM_Event->event_start_time) + $timezone;
    	$time_end   = strtotime($EM_Event->event_end_date . $EM_Event->event_end_time) + $timezone;
    	$time = date('Y-m-d H:i' ,$time_start) . ' - ' . date('Y-m-d H:i' ,$time_end);
    		
    	$msg = get_option('wpsms_event_no_tt');
    	$msg = str_replace('%time%', $time, $msg);
    	$msg = str_replace('%nickname%', $user, $msg);
    	

    
    	if(!empty($phone)){
			//$msg = str_replace('%id%', $booking_id, get_option('wpsms_event_no_tt'));
			global $sms;
			$sms->to = array($phone);
			$sms->msg = $msg;
			$sms->SendSMS();
    	}
    }

	if(get_option('wpsms_event_no_stats'))
		add_action('em_tickets_bookings_save_pre', 'wps_event_new_booking');
	
	// Quform
	function wps_quform_new_message() {
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$sms->msg = get_option('wpsms_quform_no_tt');
		$sms->SendSMS();
	}
	if(get_option('wpsms_quform_no_stats'))
		add_action('iphorm_pre_process', 'wps_quform_new_message');
	
	// Gravityform
	function wps_gform_new_message($entry, $form) {
		global $sms;
		$sms->to = array(get_option('wp_admin_mobile'));
		$string = get_option('wpsms_gform_tt');
		foreach($form['fields'] as $items) {
			$result[] = $entry[$items['id']];
		}
		$template_vars = array(
			'title'			=> $form['title'],
			'ip'			=> $entry['ip'],
			'source_url'	=> $entry['source_url'],
			'user_agent'	=> $entry['user_agent'],
			'content'		=> implode("\n", $result)
		);
		$final_message = preg_replace('/%(.*?)%/ime', "\$template_vars['$1']", $string);
		$sms->msg = $final_message;
		$sms->SendSMS();
	}
	if(get_option('wpsms_gform_stats'))
		add_action('gform_after_submission', 'wps_gform_new_message', 10, 2);