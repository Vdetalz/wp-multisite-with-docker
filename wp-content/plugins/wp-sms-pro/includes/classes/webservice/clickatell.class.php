<?php
	class clickatell extends WP_SMS {
		private $wsdl_link = "http://api.clickatell.com/http/";
		public $tariff = "http://www.clickatell.com";
		public $unitrial = false;
		public $unit;
		public $flash = "enable";
		public $isflash = false;

		public function __construct() {
			parent::__construct();
			
			$this->has_key = true;
		}

		public function SendSMS() {
			$to = str_replace('+', '', implode(',', $this->to));
			$msg = urlencode($this->msg);
			
			$result = file_get_contents("{$this->wsdl_link}sendmsg?user={$this->username}&password={$this->password}&api_id={$this->has_key}&to={$to}&text={$msg}");
			
			if(preg_match("/ERR/", $result))
				return 0;
			
			if($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}

		public function GetCredit() {
			$result = file_get_contents("{$this->wsdl_link}getbalance?api_id={$this->has_key}&user={$this->username}&password={$this->password}");
			
			if(preg_match("/ERR/", $result))
				return 0;
			
			return preg_replace("/[a-zA-Z :]/", "", $result);
		} 
	}
?>