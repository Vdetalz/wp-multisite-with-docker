<?php
	class nexmo extends WP_SMS {
		private $wsdl_link = "https://rest.nexmo.com/";
		public $tariff = "https://www.nexmo.com/";
		public $unitrial = true;
		public $unit;
		public $flash = "enable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
		}
		
		public function SendSMS() {
			
			$msg = urlencode($this->msg);
			
			foreach($this->to as $number) {
				$result = file_get_contents("{$this->wsdl_link}sms/json?api_key={$this->username}&api_secret={$this->password}&from={$this->from}&to={$number}&text={$msg}");
			}
			
			if ($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}
		
		public function GetCredit() {
			$result = file_get_contents("{$this->wsdl_link}account/get-balance/{$this->username}/{$this->password}");
			
			$result = json_decode($result);
			
			return $result->value;
		}
	}
?>