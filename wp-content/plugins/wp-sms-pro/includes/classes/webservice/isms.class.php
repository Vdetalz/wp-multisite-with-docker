<?php
	class isms extends WP_SMS {
		private $wsdl_link = "https://www.isms.com.my/";
		public $tariff = "https://www.isms.com.my/";
		public $unitrial = false;
		public $unit;
		public $flash = "enable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
		}
		
		public function SendSMS() {
			
			$msg = urlencode($this->msg);
			
			foreach($this->to as $number) {
				$result = file_get_contents("{$this->wsdl_link}isms_send.php?un={$this->username}&pwd={$this->password}&dstno={$number}&msg={$msg}&type=1&sendid={$this->from}");
			}
			
			if ($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}
		
		public function GetCredit() {
			$result = file_get_contents("{$this->wsdl_link}isms_balance.php?un={$this->username}&pwd={$this->password}");
			if( preg_replace('/[^0-9]/', '', $result) == 1008 ) {
				return false;
			} else {
				return $result;
			}
		}
	}
?>