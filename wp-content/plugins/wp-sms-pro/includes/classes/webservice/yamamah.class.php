<?php
	class yamamah extends WP_SMS {
		private $wsdl_link = "https://services.yamamah.com/yamamahwebservicev2.2/SMSService.asmx?WSDL";
		public $tariff = "http://yamamah.com";
		public $unitrial = true;
		public $unit;
		public $flash = "disable";
		public $isflash = false;

		public function __construct() {
			parent::__construct();
			ini_set("soap.wsdl_cache_enabled", "0");
		}

		public function SendSMS() {
			$client = new SoapClient($this->wsdl_link);
			
			$result = $client->SendBulkSMS($this->username, $this->password, $this->from, $this->from, implode(',', $this->to), time());
			
			if($result) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}

		public function GetCredit() {
			$client = new SoapClient($this->wsdl_link);
			
			$result = $client->GetCredit($this->username, $this->password);
			
			return $result->GetCreditResult;
		}
	}
?>