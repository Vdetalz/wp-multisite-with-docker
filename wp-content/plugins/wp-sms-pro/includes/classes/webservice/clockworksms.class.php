<?php
	class clockworksms extends WP_SMS {
		private $wsdl_link = "";
		public $tariff = "http://www.clockworksms.com/";
		public $unitrial = true;
		public $unit;
		public $flash = "enable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
			require 'includes/clockworksms/class-Clockwork.php';
		}

		public function SendSMS() {
		
			$clockwork = new Clockwork( $this->username );
			
			foreach($this->to as $items) {
				$to[] = array('to' => $items, 'message' => $this->msg);
			}
			
			$result = $clockwork->send($to);
			
			if($result['success']) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}

		public function GetCredit() {
			$clockwork = new Clockwork( $this->username );
			
			$result = $clockwork->checkBalance();
			
			return $result['balance'];
		}
	}
?>