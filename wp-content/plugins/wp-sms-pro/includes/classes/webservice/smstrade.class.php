<?php
	class smstrade extends WP_SMS {
		private $wsdl_link = "http://gateway.smstrade.de/";
		public $tariff = "http://www.smstrade.de/";
		public $unitrial = false;
		public $unit;
		public $flash = "enable";
		public $isflash = false;

		public function __construct() {
			parent::__construct();
		}

		public function SendSMS() {
			$to = implode(';', $this->to);
			$timestamp = time();
			$msg = str_replace(" ", "+", $this->msg);
			
			$result = file_get_contents($this->wsdl_link."/bulk/?key={$this->password}&to={$to}&route=gold&from={$this->from}&message={$msg}&senddate={$timestamp}");
			
			if($result == 'OK') {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}

		public function GetCredit() {
			$result = file_get_contents($this->wsdl_link."/credits/?key=".$this->password);
			
			if($result == 'ERROR')
				return 0;
			
			return $result;
		}
	}
?>