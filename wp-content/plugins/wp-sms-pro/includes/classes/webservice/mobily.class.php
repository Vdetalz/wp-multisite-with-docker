<?php
	class mobily extends WP_SMS {
		private $wsdl_link = "http://www.mobily.ws/api/";
		public $tariff = "http://www.mobily.ws/";
		public $unitrial = false;
		public $unit;
		public $flash = "enable";
		public $isflash = false;
		
		public function __construct() {
			parent::__construct();
			require "includes/mobily/functionUnicode.php";
		}
		
		public function SendSMS() {
			$to = implode(',', $this->to);
			$msg = convertToUnicode($this->msg);
			
			foreach($this->to as $number) {
				$result = file_get_contents("{$this->wsdl_link}msgSend.php?mobile={$this->username}&password={$this->password}&numbers={$to}&sender={$this->from}&msg={$msg}&applicationType=24");
			}
			
			if ($result == 1) {
				$this->InsertToDB($this->from, $this->msg, $this->to);
				$this->Hook('wp_sms_send', $result);
			}
			
			return $result;
		}
		
		public function GetCredit() {
			$result = file_get_contents("{$this->wsdl_link}balance.php?mobile={$this->username}&password={$this->password}");
			
			if($result == 1 || $result == 2) {
				return false;
			} else {
				return $result;
			}
		}
	}
?>