<div class="wrap">
  <?php include( dirname( __FILE__ ) . '/tabs.php' ); ?>
  <table class="form-table">
    <form method="post" action="options.php" name="form">
      <?php wp_nonce_field('update-options');?>
      <tr>
        <td><?php _e('Your Mobile Number (Admin number)', 'wp-sms'); ?>:</td>
        <td>
          <input type="text" dir="ltr" style="width: 200px;" name="wp_admin_mobile" value="<?php echo get_option('wp_admin_mobile'); ?>"/>
        </td>
      </tr>
      
      <tr>
        <td><?php _e('Second Admin Number', 'wp-sms'); ?>:</td>
        <td>
          <input type="text" dir="ltr" style="width: 200px;" name="wp_admin_mobile_second" value="<?php echo get_option('wp_admin_mobile_second'); ?>"/>
        </td>
      </tr>

      <tr>
        <td><?php _e('Your mobile country code', 'wp-sms'); ?>:</td>
        <td>
          <input type="text" dir="ltr" style="width: 200px;" name="wp_sms_mcc" value="<?php echo get_option('wp_sms_mcc'); ?>"/>
          <p class="description"><?php _e('Enter your mobile country code. (For example: Iran 09, Australia 61)', 'wp-sms'); ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
          <p class="submit">
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="wp_admin_mobile,wp_admin_mobile_second,wp_sms_mcc" />
            <input type="submit" class="button-primary" name="Submit" value="<?php _e('Update', 'wp-sms'); ?>" />
          </p>
        </td>
      </tr>
    </form> 
  </table>
</div>