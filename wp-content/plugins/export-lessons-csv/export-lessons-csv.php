<?php
/*
Plugin Name: Export Lessons CSV
Description:  Export lessons to CSV file
Version: 1.0
Author: Web4Pro
Author URI:
*/

class Export_Lessons_CSV
{
    const PAGE_SLUG = 'events-manager-completed-lessons';
    public $from;
    public $to;
    
    /**
     * Run plugin
     */
    public function run()
    {
        if(filter_input(INPUT_GET, 'page') === self::PAGE_SLUG && filter_input(INPUT_GET, 'submit_export')) {
            $results = $this->get_lessons();
            if(!empty($results)){
                $lessons = $this->formation_of_array_for_csv($results);
                $this->from = filter_input(INPUT_GET, 'start_period');
                $this->to = filter_input(INPUT_GET, 'end_period');
                $this->array_to_csv_download($lessons, $this->create_file_name(str_replace('/', '-', $this->from), str_replace('/', '-', $this->to)));
            }
        }
    }
    
    /**
     * Get lessons from date
     * @return null|array
     */
    private function get_lessons()
    {
        if(function_exists('eme_get_lessons')) {
            $lessons = eme_get_lessons(FALSE);
            return $lessons['results'];
        }
        return NULL;
    }


    
    /**
     * Get full info about lessons
     * @param array $array
     * @return array
     */
    private function formation_of_array_for_csv(array $array)
    {
        $final_surveys_array[] = array(
            "Lesson Date and Time", "Tutor", "Student", "Student nick name", "Minutes"
        );

        foreach($array as $lesson) {

            $row = array();
            $row[] =  date('Y-m-d H:i', dut_prepare_date($lesson['event_start_date'] . ' ' . $lesson['event_start_time'], 8)) . ' GMT +8';
            $row[] = $lesson['event_name'];
            
            $event = new EM_Event($lesson['event_id']);
            if($event->ID == NULL) {
                continue;
            }
            $user = new WP_User($event->get_bookings()->bookings[0]->person_id);

            $row[] = $user->user_email;
            $row[] = $user->nickname;
            $row[] = date('H:i', strtotime($lesson['event_end_time']) - strtotime($lesson['event_start_time']));
         
            $final_surveys_array[] = $row;
        }
        
        return $final_surveys_array;
    }
    
    
    /**
     * Start download csv file
     * @param array $array
     * @param string $filename
     * @param string $delimiter
     */
    private function array_to_csv_download(array $array, $filename = "export.csv", $delimiter=",")
    {
        $f = fopen('php://memory', 'w');
        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        fpassthru($f);
        exit;
    }

    /**
     * Create current CSV file name
     * @param $from
     * @param $to
     * @return string
     */
    private function create_file_name($from, $to)
    {
        if(empty($from)) {
            $from = 'Beginning';
        }
        if(empty($to)) {
            $to = date('Y-m-d', dut_prepare_date(date('Y-m-d'), -8));
        }

        return "lessons_csv_FROM_{$from}_TO_{$to}.csv";
    }
    
}

// init page
if(is_admin()) {
    $export = new Export_Lessons_CSV();
    $export->run();
}