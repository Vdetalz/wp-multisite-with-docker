msgid ""
msgstr ""
"Project-Id-Version: Google Сaptcha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-25 11:34+0300\n"
"PO-Revision-Date: \n"
"Last-Translator: BestWebSoft team <wp@bestwebsoft.com>\n"
"Language-Team: the BestWebSoft Team <plugin@bestwebsoft.com>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: .\n"

#: google-captcha.php:34 google-captcha.php:422
msgid "Google Captcha Settings"
msgstr "Налаштування Google Captcha"

#: google-captcha.php:202
msgid "Warning"
msgstr "Увага"

#: google-captcha.php:202
msgid ""
"It has been found more than one reCAPTCHA in current form. In this case "
"reCAPTCHA will not work properly. Please remove all unnecessary reCAPTCHA "
"blocks."
msgstr ""
"В поточній формі знайдено більш ніж один блок reCAPTCHA. В цьому разі "
"reCAPTCHA не буде працювати коректно. Будь-ласка видаліть усі зайві блоки "
"reCAPTCHA."

#: google-captcha.php:206
msgid "Error: You have entered an incorrect reCAPTCHA value."
msgstr "Помилка: Ви ввели невірне значення Captcha."

#: google-captcha.php:327
msgid "Normal"
msgstr "Нормальний"

#: google-captcha.php:328
msgid "Compact"
msgstr "Компактний"

#: google-captcha.php:334
msgid "Site key"
msgstr "Ключ сайту"

#: google-captcha.php:339
msgid "Secret Key"
msgstr "Секретний ключ"

#: google-captcha.php:347
msgid "Login form"
msgstr "Форма логіну"

#: google-captcha.php:348
msgid "Registration form"
msgstr "Форма реєстрації"

#: google-captcha.php:349
msgid "Reset password form"
msgstr "Форма відновлення паролю"

#: google-captcha.php:350
msgid "Comments form"
msgstr "Форма коментарів"

#: google-captcha.php:369
msgid "Enter site key"
msgstr "Введіть ключ сайту"

#: google-captcha.php:370 google-captcha.php:376
msgid "WARNING: The captcha will not display while you don't fill key fields."
msgstr ""
"УВАГА: Капча не буде відображатись, поки ви не заповните необхідні поля."

#: google-captcha.php:375
msgid "Enter secret key"
msgstr "Введіть секретний ключ"

#: google-captcha.php:401
msgid "Settings saved"
msgstr "Налаштування збережено"

#: google-captcha.php:407
msgid "All plugin settings were restored."
msgstr "Налаштування плагіна були відновлені."

#: google-captcha.php:424 google-captcha.php:1128 google-captcha.php:1150
msgid "Settings"
msgstr "Налаштування"

#: google-captcha.php:425
msgid "Custom code"
msgstr "Користувацький код"

#: google-captcha.php:426
msgid "Go PRO"
msgstr "Перейти на PRO версію"

#: google-captcha.php:431
msgid ""
"Only one reCAPTCHA can be displayed on the page, it's related to reCAPTCHA "
"version 1 features."
msgstr ""
"У зв'язку з особливостями роботи reCAPTCHA версії 1 на сторінці може "
"відображатися тільки одна reCAPTCHA."

#: google-captcha.php:446
#, php-format
msgid ""
"If you would like to add a Google Captcha (reCAPTCHA) to your page or post, "
"please use %s button"
msgstr ""
"Якщо ви хочете додати Google Captcha (reCAPTCHA) на вашу сторінку або пост, "
"то використовуйте кнопку %s"

#: google-captcha.php:452
#, php-format
msgid ""
"You can add the Google Captcha (reCAPTCHA) to your page or post by clicking "
"on %s button in the content edit block using the Visual mode. If the button "
"isn't displayed or you would like to add the Google Captcha (reCAPTCHA) to "
"your own form , please use the shortcode %s"
msgstr ""
"Ви можете додати Google Captcha (reCAPTCHA) на вашу сторінку або пост, "
"натиснувши на кнопку %s в блоці редагування контенту в режимі Visual. Якщо "
"кнопка не відображається або ви хочете додати Google Captcha (reCAPTCHA) в "
"вашу власну форму, будь ласка, використовуйте шорткод %s"

#: google-captcha.php:460
msgid "Authentication"
msgstr "Ідентифікація"

#: google-captcha.php:461
#, php-format
msgid "Before you are able to do something, you must to register %shere%s"
msgstr "Щоб виконувати якісь дії, спершу зареєструйтесь %sтут%s"

#: google-captcha.php:462
msgid "Enter site key and secret key, that you get after registration."
msgstr "Введіть відкритий і секретний ключі, які ви отримали після реєстрації."

#: google-captcha.php:477
msgid "Test Keys"
msgstr "Тестування ключів"

#: google-captcha.php:480
msgid "Options"
msgstr "Опції"

#: google-captcha.php:483
msgid "Enable reCAPTCHA for"
msgstr "Включити  reCAPTCHA для"

#: google-captcha.php:487
msgid "WordPress default"
msgstr "Стандартних форм WordPress"

#: google-captcha.php:496
msgid "This option is available only for network or for main blog"
msgstr "Ця опція доступна тільки для мережі або для основного блогу"

#: google-captcha.php:504
msgid "Plugins"
msgstr "Плагінів"

#: google-captcha.php:512 google-captcha.php:517
#, php-format
msgid "You should %s to use this functionality"
msgstr "Щоб використовувати цей функціонал %s"

#: google-captcha.php:513
msgid "activate"
msgstr "активувати"

#: google-captcha.php:513
msgid "for network"
msgstr "для мережі"

#: google-captcha.php:518
msgid "download"
msgstr "завантажити"

#: google-captcha.php:522
msgid "Check off for adding captcha to forms on their settings pages."
msgstr "Увімкніть для додавання капчи до форм на сторінках їх налаштувань."

#: google-captcha.php:533 google-captcha.php:610
msgid "Close"
msgstr "Закрити"

#: google-captcha.php:545 google-captcha.php:646
msgid "Unlock premium options by upgrading to Pro version"
msgstr "Активуйте преміум опції оновившись до Pro версії"

#: google-captcha.php:548 google-captcha.php:649
msgid "Learn More"
msgstr "Детальніше"

#: google-captcha.php:554
#, php-format
msgid ""
"If you would like to add Google Captcha (reCAPTCHA) to a custom form see %s"
msgstr ""
"Якщо ви хочете додати Google Captcha (reCAPTCHA) в кастомний форму, дивіться "
"%s"

#: google-captcha.php:554 google-captcha.php:1151
msgid "FAQ"
msgstr "FAQ"

#: google-captcha.php:559
msgid "Hide reCAPTCHA in Comments form for"
msgstr "Приховати reCAPTCHA у Формі коментарів для"

#: google-captcha.php:571
msgid "reCAPTCHA version"
msgstr "Версія reCAPTCHA"

#: google-captcha.php:574 google-captcha.php:575 google-captcha.php:576
#: google-captcha.php:577
msgid "version"
msgstr "версія"

#: google-captcha.php:583 google-captcha.php:596
msgid "reCAPTCHA theme"
msgstr "reCAPTCHA theme"

#: google-captcha.php:584 google-captcha.php:597 google-captcha.php:628
msgid "for version"
msgstr "для версії"

#: google-captcha.php:614
msgid "reCAPTCHA language"
msgstr "Мова reCAPTCHA"

#: google-captcha.php:621
msgid "Use the current site language"
msgstr "Використовувати поточну мову сайту"

#: google-captcha.php:621
msgid "Using"
msgstr "Використовуючи"

#: google-captcha.php:627
msgid "reCAPTCHA size"
msgstr "Розмір reCAPTCHA"

#: google-captcha.php:656
msgid "Save Changes"
msgstr "Зберегти зміни"

#: google-captcha.php:710 google-captcha.php:1167
msgid "To use Google Captcha you must get the keys from"
msgstr "Щоб використовувати Google Captcha, вам необхідно отримати ключі з"

#: google-captcha.php:711 google-captcha.php:1168
msgid "here"
msgstr "тут"

#: google-captcha.php:712 google-captcha.php:1169
msgid "and enter them on the"
msgstr "і вставте їх у"

#: google-captcha.php:714 google-captcha.php:1171
msgid "plugin setting page"
msgstr "сторінку налаштувань плагіну"

#: google-captcha.php:900 google-captcha.php:948
msgid "Error"
msgstr "Помилка"

#: google-captcha.php:900 google-captcha.php:948
msgid "You have entered an incorrect reCAPTCHA value."
msgstr "Ви ввели невірне значення reCAPTCHA."

#: google-captcha.php:978 google-captcha.php:1007 google-captcha.php:1033
msgid "ERROR"
msgstr "ПОМИЛКА"

#: google-captcha.php:978 google-captcha.php:1007
msgid "You have entered an incorrect reCAPTCHA value"
msgstr "Ви ввели невірне значення reCAPTCHA"

#: google-captcha.php:1033
msgid ""
"You have entered an incorrect reCAPTCHA value. Click the BACK button on your "
"browser, and try again."
msgstr ""
"Помилка: Ви ввели невірне значення Captcha. Клікніть НАЗАД у своєму браузері "
"та спробуйте знову."

#: google-captcha.php:1064
msgid "Please, complete the captcha and submit \"Test verification\""
msgstr ""
"Будь ласка, заповніть капчу і натисніть кнопку \"Тестування перевірки\""

#: google-captcha.php:1068
msgid "Test verification"
msgstr "Тестування перевірки"

#: google-captcha.php:1082 google-captcha.php:1091
msgid "The user response was missing."
msgstr "Відповідь користувача відсутня."

#: google-captcha.php:1084
msgid "The Secret Key is missing."
msgstr "Секретний ключ відсутній."

#: google-captcha.php:1087 google-captcha.php:1096
msgid "The Secret Key is invalid"
msgstr "Секретний ключ некоректний"

#: google-captcha.php:1088 google-captcha.php:1097
msgid "Check your domain configuration"
msgstr "Перевірте конфігурацію вашого домену"

#: google-captcha.php:1089 google-captcha.php:1098
msgid "and enter it again"
msgstr "і введіть його знову"

#: google-captcha.php:1092 google-captcha.php:1100
msgid "The user response is invalid."
msgstr "Некоректна відповідь користувача."

#: google-captcha.php:1109
msgid "The verification is successfully completed."
msgstr "Перевірка завершена успішно."

#: google-captcha.php:1152
msgid "Support"
msgstr "Техпідтримка"

#~ msgid ""
#~ "Google Captcha version 2 will not work correctly, since the option "
#~ "\"allow_url_fopen\" is disabled. Please contact your hosting support "
#~ "service."
#~ msgstr ""
#~ "Google Captcha версії 2 не працюватиме коректно, тому що відключена опція "
#~ "\"allow_url_fopen \". Будь ласка, зверніться в службу підтримки вашого "
#~ "хостингу."

#~ msgid ""
#~ "Google Captcha version 2 will not work correctly, since the option "
#~ "\"allow_url_fopen\" is disabled in the PHP settings of your hosting."
#~ msgstr ""
#~ "версія 2 плагіну Google Captcha не працюватиме коректно, тому що опція "
#~ "\"allow_url_fopen\" заблокована в PHP налаштуваннях вашого хостінгу."

#~ msgid "Read more."
#~ msgstr "Дізнатись більше."

#~ msgid "powered by"
#~ msgstr "розроблено компанією"

#~ msgid "Activate"
#~ msgstr "Активувати"

#~ msgid "Download"
#~ msgstr "Завантажити"

#~ msgid "Enable CAPTCHA"
#~ msgstr "Включити  reCAPTCHA"

#~ msgid "You should"
#~ msgstr "Вам слід"

#~ msgid "Notice:"
#~ msgstr "Увага:"

#~ msgid ""
#~ "The plugin's settings have been changed. In order to save them please "
#~ "don't forget to click the 'Save Changes' button."
#~ msgstr ""
#~ "Налаштування плагіну змінено. Щоб зберегти їх, будь ласка, не забудьте "
#~ "клікнути 'Зберегти зміни'."

#~ msgid "Contact form"
#~ msgstr "Контактна форма"

#~ msgid "Activate contact form"
#~ msgstr "Активувати контактну форму"

#~ msgid "Download contact form"
#~ msgstr "Завантажити контактну форму"

#~ msgid "Google Captcha"
#~ msgstr "Google Captcha"

#~ msgid "Enable Google Captcha for:"
#~ msgstr "Відображати Google Captcha для:"

#~ msgid "Hide captcha for:"
#~ msgstr "Не відображати Google Captcha для:"

#~ msgid "Theme:"
#~ msgstr "Тема:"

#~ msgid ""
#~ "An unexpected error has occurred. If this message appears again, please "
#~ "contact the Administrator."
#~ msgstr ""
#~ "Сталась невідома помилка, якщо ви постійно бачите це повідомлення "
#~ "зв'яжіться будь ласка з Адміністратором"

#~ msgid "requires"
#~ msgstr "потребує"

#~ msgid ""
#~ "or higher, that is why it has been deactivated! Please upgrade WordPress "
#~ "and try again."
#~ msgstr ""
#~ "або вище, тому його було деактивовано! Оновіть WordPress та спробуйте ще "
#~ "раз."

#~ msgid "Back to the WordPress"
#~ msgstr "Повернутися на WordPress"

#~ msgid "If you enjoy our plugin, please give it 5 stars on WordPress"
#~ msgstr ""
#~ "Якщо вам сподобався наш плагін, будь ласка, поставте нам 5 зірочок на "
#~ "WordPress"

#~ msgid "Rate the plugin"
#~ msgstr "Оцінити плагін"

#~ msgid "If there is something wrong about it, please contact us"
#~ msgstr "Якщо у вас виникли проблеми, будь ласка, повідомте нам"

#~ msgid "Public Key"
#~ msgstr "Відкритий ключ"

#~ msgid "Private Key"
#~ msgstr "Закритий ключ"

#~ msgid "Enter public key"
#~ msgstr "Введіть відкритий ключ"

#, fuzzy
#~ msgid "Themes"
#~ msgstr "Тема:"

#~ msgid "WARNING: The captcha will not display while you not fill key fields."
#~ msgstr ""
#~ "ПРЕДУПРЕЖДЕНИЕ: Google Captcha не будет отображаться пока вы не заполните "
#~ "ключевые поля."
