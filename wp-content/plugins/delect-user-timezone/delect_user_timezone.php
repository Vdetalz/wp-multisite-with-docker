<?php

/*
  Plugin Name: User Timezone
  Description: Detect User Timezone
  Author: Web4Pro
  Version: 1.0
 */

/**
 * Init Plugin
 */

function dut_init(){

    wp_register_script('dut_init_timezone_script_cookie', plugins_url('jquery.cookie.js', __FILE__), array('jquery') ); // Custom scripts
    wp_enqueue_script('dut_init_timezone_script_cookie'); // Enqueue it!

    wp_register_script('dut_init_timezone_script', plugins_url('user_timezone.js', __FILE__), array('jquery') ); // Custom scripts
    wp_enqueue_script('dut_init_timezone_script'); // Enqueue it!

    dut_set_user_timezone();

}


add_action('init', 'dut_init');

/**
 * Add / rewrite User time zone (For example : 8 OR -8)
 * @param bool $user_id
 * @param bool $hours
 */

function dut_set_user_timezone($user_id = false, $hours = false){


    $user_id = !$user_id ? get_current_user_id() : $user_id;
    $user = new WP_User($user_id);
    $hours = !$hours ? $_COOKIE['user_hours_1'] : $hours;

    if( ! isset( $_COOKIE['user_hours_1'] ) || in_array( 'author', $user->roles ) ) return;

    update_user_meta($user_id, '_user_timezone', $hours);

}

/**
 * Get user timezone by user ID (If user id not exists than function return time zone of current user)
 * @param bool $user_id
 * @return mixed
 */

function dut_get_user_timezone($user_id = false){

	$user_id = !$user_id ? get_current_user_id() : $user_id;

	$user = get_user_by('id', $user_id);

	if(isset($user->roles) && !empty($user->roles) && is_array($user->roles)) {
		if (in_array('author', $user->roles)) {

			$timezone = get_user_meta($user_id, '_user_timezone', true);

			if( empty( $timezone ) || is_numeric( $timezone ) ){
				$timezone = 'Asia/Hong_Kong';
				update_user_meta($user_id, '_user_timezone', $timezone);
			}

			$dateTimeZone = new DateTimeZone($timezone);
			$dateTime = new DateTime("now", $dateTimeZone);
			$timeOffset = $dateTimeZone->getOffset($dateTime);

			$offset = $timeOffset / 60 / 60;

			return $offset;

		} else {

			return !get_user_meta($user_id, '_user_timezone', true) ? 0 : get_user_meta($user_id, '_user_timezone', true);
		}
	}
	else{
		return !get_user_meta($user_id, '_user_timezone', true) ? 0 : get_user_meta($user_id, '_user_timezone', true);
	}
}

/**
 * Function prepare date. For example : dut_prepare_date('2015-12-12 12:12:12', -8);
 * @param $datetime
 * @param $timezone
 * @return bool|string (date in format Y-m-d H:i:s)
 */

function dut_prepare_date($datetime, $timezone){

    if(!$timezone || !$datetime) return strtotime($datetime);

    return strtotime($datetime) + ($timezone * 60 * 60);

}

/**
 * The function return Timestamp of current user
 * @return int
 */

function dut_get_current_user_timestamp(){  

    return $_COOKIE['user_datetime_1'] ? strtotime($_COOKIE['user_datetime_1']) : 0;

}

/**
 * Return array of all countries with their timezones
 * @return array
 */

function dut_get_country_timezones(){
    return DateTimeZone::listIdentifiers(DateTimeZone::ALL);
}

/**
 * @param $timestamp
 * @return string
 */

function dut_prepare_timezone($timestamp){
    $str ='GMT';
    $str.= $timestamp < 0 ? '-' : '+';
    //$str.=gmdate('H:i', $timestamp);
    $str.=gmdate('H:i', abs($timestamp));
    return $str;
}