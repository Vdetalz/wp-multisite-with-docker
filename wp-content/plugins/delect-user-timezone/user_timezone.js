jQuery(document).ready(function ($) {

    $.cookie('user_hours_1', null, {path:'/'});

    var d = new Date();
    var timezone = d.getTimezoneOffset() / (-60);

    $.cookie('user_hours_1', timezone, {path: '/'});

    var _date = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    var _time = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

    $.cookie('user_datetime_1', _date + ' ' + _time, {path: '/'});

});