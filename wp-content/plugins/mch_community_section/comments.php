<?php

/*
 * Plugin name: My Chinese Hub Community Section
 * Author: Web4Pro
 * Description: The plugin create Comments for Course Units
 */

/**
 * Single comment template
 * @param $comment
 * @param $args
 * @param $depth
 */

function mch_comments_template($comment, $args, $depth){

    $_GLOBALS['comment'] = $comment; //Comment Object
    $user = get_user_by('email', get_comment_author_email()); //User Object
    $name = empty($user->first_name) || empty($user->last_name) ? $user->nickname : $user->first_name . ' ' . $user->last_name; //User name/nickname
    ?>

    <div class="mch-comment mch-comment-<?php comment_ID();?>">
        <div class="mch-comment-content"><?php echo wpautop(do_shortcode(get_comment_text()));?></div>
        <div class="mch-comment-info">
            <?php _e('By:');?> <span class="mch-comment-author"><?php echo $name;?></span> (<?php comment_date();?>)<?php echo !empty($user->school) ? ', ' . $user->school : '';?>
            <div class="mch-comment-like-block"><?php echo mch_dislike_comment('-', get_comment_ID());?> <?php echo mch_counter_likes(get_comment_ID());?> <?php echo mch_like_comment('+', get_comment_ID());?></div>
        </div>
    </div>

<?php

}


/**
 * Select count likes of specific comment
 * @param bool $comment_id
 * @return int|mixed
 */

function mch_counter_likes($comment_id = false){
    global $comment;

    $comment_id = !$comment_id ? $comment->ID : $comment_id;
    $likes = get_comment_meta($comment_id, 'mch_comment_likes', true);

    if($likes > 0){
        $color_class = 'mch-counter-green';
        $plus = '+';
    }
    else if($likes < 0) $color_class = 'mch-counter-red';

    return !$likes ? '<span class="comment-likes-count '. $color_class .'">0</span>' : '<span class="comment-likes-count '. $color_class .'">' . $plus . $likes . '</span>';
}


/**
 * @param string $label
 * @param bool $comment_id
 * @return string
 */

function mch_like_comment($label = '+', $comment_id = false) {

    global $comment;
    $comment_id = !$comment_id ? $comment->ID : $comment_id;

    if(!mch_is_user_like_post($comment_id) && get_current_user_id())
        return "<a href='#' class='mch-like-comment like-inc' data-comment-like-type='like' data-comment-id='$comment_id'>$label</a>";

}


/**
 * @param string $label
 * @param bool $comment_id
 * @return string
 */

function mch_dislike_comment($label = '-', $comment_id = false) {

    global $comment;
    $comment_id = !$comment_id ? $comment->ID : $comment_id;

    if(!mch_is_user_like_post($comment_id) && get_current_user_id())
        return "<a href='#' class='mch-like-comment like-dec' data-comment-like-type='dislike' data-comment-id='$comment_id'>$label</a>";

}


/**
 * Ajax action for like comment
 */

function mch_like_comment_ajax_action(){

    $comment_id = $_POST['comment_id'];
    $like_type = $_POST['like_type'];

    if(!$comment_id || !$like_type || !get_current_user_id() || mch_is_user_like_post($comment_id) ){
        echo json_encode(array(
            'error' => 1,
            'message' => ''
        ));
        die();
    }

    switch($like_type){

        case 'like':
            $likes = (int)get_comment_meta($comment_id, 'mch_comment_likes', true) + 1;
            if( update_comment_meta($comment_id, 'mch_comment_likes', $likes) ){
                $arr = mch_get_user_comments_liked();
                $arr[] = $comment_id;
                update_user_meta(get_current_user_id(), 'mch_comment_likes', $arr);
                echo json_encode(array(
                    'error' => 0,
                    'likes' => get_comment_meta($comment_id , 'mch_comment_likes', true),
                ));
                die();
            }
            else{
                echo json_encode(array(
                    'error' => 1,
                    'message' => ''
                ));
                die();
            }
            break;

        case 'dislike':
            $likes = (int)get_comment_meta($comment_id, 'mch_comment_likes', true) - 1;
            if( update_comment_meta($comment_id, 'mch_comment_likes', $likes) ){
                $arr = mch_get_user_comments_liked();
                $arr[] = $comment_id;
                update_user_meta(get_current_user_id(), 'mch_comment_likes', $arr);
                echo json_encode(array(
                    'error' => 0,
                    'likes' => get_comment_meta($comment_id , 'mch_comment_likes', true)
                ));
                die();
            }
            else{
                echo json_encode(array(
                    'error' => 1,
                    'message' => ''
                ));
                die();
            }
            break;

        default:
            echo json_encode(array(
                'error' => 1,
                'message' => ''
            ));
            die();
    }

    //die();

}

add_action( 'wp_ajax_mch-comment-like', 'mch_like_comment_ajax_action' );


/**
 * @param bool $comment_id
 * @param bool $user_id
 * @return bool
 */

function mch_is_user_like_post($comment_id = false, $user_id = false){
    global $comment;

    $user_id = $user_id ? $user_id : get_current_user_id();
    $comment_id = $comment_id ? $comment_id : $comment->ID;

    if(!$user_id) return false;

    $comments_like_array = mch_get_user_comments_liked($user_id);

    if(!$comments_like_array) return false;

    if(in_array($comment_id, $comments_like_array)){
        return true;
    }

    return false;

}


/**
 * Open comments for specific post types
 * @param $open
 * @param $post_id
 * @return bool
 */

function mch_unit_comments_open( $open, $post_id ) {

    $post = get_post( $post_id );

    if ( get_current_blog_id() == 11 && ( 'post' == $post->post_type || 'photo' == $post->post_type ) ) {
        return true;
    } else {
        if ( 'course_unit' == $post->post_type || 'photo' == $post->post_type) {
            return true;
        }
    }

    return false;

}

add_filter( 'comments_open', 'mch_unit_comments_open', 10, 2 );


/**
 * Display Comments list
 * @param bool $post_id
 */

function mch_get_list_comments($post_id = false){

    global $post;
    $post_id = $post_id ? $post_id : $post->ID;

    $comments = get_comments(array(
        'post_id' => $post_id,
        'status' => 'approve' //Change this to the type of comments to be displayed
    ));

    wp_list_comments(array('callback' => 'mch_comments_template'), $comments);

}


/**
 * Display Comment Form for specific post
 * @param bool $post_id
 */

function mch_comment_form($post_id = false){

    global $post;

    $post_id = $post_id ? $post_id : $post->ID;

    comment_form(array(
        'label_submit'  => __( 'PUBLISH' ),
        'comment_notes_after' => '',
        'comment_notes_before' => '',
        'logged_in_as' => '',
        'title_reply' => '',
    ), $post_id);

    unset($_SESSION['comment_added']);
}

/**
 * Append scripts
 */

function mch_ajax_comment_scripts(){
    wp_register_script('mch-ajax-comments', plugins_url('assets/js/comments-ajax-action.js', __FILE__), array('jquery'), '1.0.0', true); // Conditional script(s)
    wp_enqueue_script('mch-ajax-comments'); // Enqueue it!

    wp_register_style('mch-css-comments', plugins_url('assets/css/comments_style.css', __FILE__)); // Conditional script(s)
    wp_enqueue_style('mch-css-comments'); // Enqueue it!
}

add_action('init', 'mch_ajax_comment_scripts');


/**
 * Get liked user comments
 * @param bool $user_id
 * @return array|mixed
 */

function mch_get_user_comments_liked($user_id = false){

    $user_id = $user_id ? $user_id : get_current_user_id();
    $liked = get_user_meta($user_id, 'mch_comment_likes', true);

    return !$liked ? array() : $liked;

}


/**
 * @param $atts
 * @param $content
 * @return string
 */
function mch_comment_img_shortcode($atts, $content){
    if ( FALSE === filter_var( $content, FILTER_VALIDATE_URL ) ) return ;
    ob_start();
    $size = getimagesize(esc_url($content));
    if ( $size ){

        if ( get_current_blog_id() == 11 ):?>

            <a href="<?php echo $content;?>" onclick="return false;" class="mch-comment-image">
                <img src="<?php echo $content;?>"/>
            </a>

            <?php else: ?>
            <div class="comment-data-block">
                <a href="<?php echo $content;?>" onclick="return false;" class="mch-comment-image">
                    <img src="<?php echo $content;?>" width="100%"/>
                    <div class="img-marker"></div>
                </a>
            </div>
    <?php endif;
    }
    return ob_get_clean();
}

add_shortcode('img', 'mch_comment_img_shortcode');


/**
 * @param $atts
 * @param $content
 * @return string
 */

function mch_comment_video_shortcode($atts, $content){
    if ( FALSE === filter_var( $content, FILTER_VALIDATE_URL ) ) return ;
    ob_start();
    if(mch_get_youtube_thumbnail($content)){
        $img = mch_get_youtube_thumbnail($content);
        $marker="<div class='youtube-marker'></div>";
    }
    else{
        $img = mch_get_vimeo_thumbnail($content);
        $marker="<div class='vimeo-marker'></div>";
    }

    $img = !$img ? get_template_directory_uri().'/img/player.png' : $img;

    if ( get_current_blog_id() == 11 ):?>


<a href="<?php echo $content;?>" onclick="return false;" data-mce-href="<?php echo $content;?>" class="popup-youtube mch-comment-video">
        <img src="<?php echo $img;?>"/>
    </a>
        <?php else : ?>

        <span class="comment-data-block">
<a href="<?php echo $content;?>" onclick="return false;" data-mce-href="<?php echo $content;?>" class="popup-youtube mch-comment-video">
    <img src="<?php echo $img;?>"/>
    <?php echo $marker;?></a></span>

<?php endif;
return ob_get_clean();
}

add_shortcode('video', 'mch_comment_video_shortcode');


/**
 * Redirect to current page
 * @param $location
 * @return mixed
 */

function redirect_after_comment($location)
{
    $_SESSION['comment_added'] = 1;
    return $_SERVER["HTTP_REFERER"] . '#comment';
}

add_filter('comment_post_redirect', 'redirect_after_comment');


/**
 * Get Youtube video id from URL
 * @param $url
 * @return bool
 */

function mch_youtube_id_from_url($url) {
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
    ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}


/**
 * Get Thumbnail of youtube video
 * @param $url
 * @return bool|string
 */

function mch_get_youtube_thumbnail($url){

    if(mch_youtube_id_from_url($url) && mch_youtube_id_from_url($url) != NULL){
        return 'http://img.youtube.com/vi/' . mch_youtube_id_from_url($url) . '/2.jpg';
    }
    else
        return false;

}


/**
 * Get thumbnail of vimeo video
 * @param $url
 * @return bool
 */

function mch_get_vimeo_thumbnail($url){

    if(getVimeoInfo($url) && getVimeoInfo($url) != NULL){
        $info = getVimeoInfo($url);
        return $info['thumbnail_medium'];
    }
    else return false;

}


/**
 * @param $link
 * @return mixed
 */

function getVimeoInfo($link)
{
    if (preg_match('~^http://(?:www\.)?vimeo\.com/(?:clip:)?(\d+)~', $link, $match))
    {
        $id = $match[1];
    }
    else
    {
        $id = substr($link,10,strlen($link));
    }

    if (!function_exists('curl_init')) die('CURL is not installed!');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/$id.php");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = unserialize(curl_exec($ch));
    $output = $output[0];
    curl_close($ch);
    return $output;
}


/**
 * Changes comment textarea
 * @return string
 */

function mch_comment_editor() {

    ob_start();

    wp_editor( '', 'comment', array(
        'wpautop' => false,
        'textarea_rows' => 3,
        'quicktags' => false,
        'media_buttons' => false, // show insert/upload button(s) to users with permission
        'tinymce' => array(
            'menubar' => false,
            'toolbar1' => 'bold,italic,underline,strikethrough,link,unlink,|,mch_vimeo_button,mch_youtube_button, mch_image_button',
            'toolbar2' => false
        ),

    ) );
    $args = ob_get_clean();
    return $args;
}

add_filter( 'comment_form_field_comment', 'mch_comment_editor' );


/**
 * Hide menu bar in comment textarea (TinyMCE)
 * @param $init_array
 * @return mixed
 */

function mch_mce_before_init($init_array)
{
    $init_array['menubar'] = false;
    return $init_array;
}

add_filter('tiny_mce_before_init', 'mch_mce_before_init');