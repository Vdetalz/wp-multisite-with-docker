<?php

add_shortcode('user_statistic', 'create_user_statistic');

/**
 * Main user statistic shortcode function
 * @return mixed
 */
function create_user_statistic()
{
    /**
     * If user not have role editor or author
     * we redirect him to the home page of the CURRENT site
     */
    if (!current_user_can('read_private_pages')) {
        wp_redirect(get_home_url(get_current_blog_id()));
    }

    global $blog_id;

    //ID = 1 - is ID for my chinese tutor site.
    if ($blog_id == 1) {
        //Shortcode content will be getting from separate function
        $shortcode_content = get_mc_tutor_statistic_content();
        return $shortcode_content;
    }
    //ID = 1 - is ID for my chinese hub site.
    if ($blog_id == 5) {
        //Shortcode content will be getting from separate function
        $shortcode_content = get_mc_hub_statistic_content();
        return $shortcode_content;
    }
}

/***********************************************************************************************************************
 * MY CHINESE TUTOR CONTENT CREATING PART
 * All code below will depends with MyChineseTutor site.
 * My Chinese Hub site code part will be separated the same comment
 ***********************************************************************************************************************/

/**
 * Content creating for MyChinese tutor statistic page
 * @return string
 */
function get_mc_tutor_statistic_content()
{

    ob_start();
    //Print form
    mct_generate_select_user_form();
    //Print statistic body
    mct_generate_user_statistic_body();

    return ob_get_clean();
}

/**
 * Function generates form with drop-down list of current site users and button "Search"
 */
function mct_generate_select_user_form()
{
    //Add script for accordion section
    mct_body_accordion_script();

    //Getting all users from MCTutor and sort it by email
    $args = array(
        'blog_id' => 1,
        'order' => 'ASC',
        'orderby' => 'email',
    );

    $userQuery = new WP_User_Query($args);
    ?>
    <?php if (isset($userQuery->results) && !empty($userQuery->results)) : ?>
    <form method="post">
        <select name="user_id">
            <option value=""><?php _e('Please Enter the Name of the Student'); ?></option>
            <?php foreach ($userQuery->results as $user): ?>
                <option
                    value="<?php _e($user->ID); ?>" <?php selected($user->ID, $_POST['user_id']) ?>><?php _e($user->data->user_email); ?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" value="<?php _e('Search'); ?>">
    </form>
<?php endif; ?>
    <?php
}

/**
 * Generates HTML of user statistic body section
 * 1. Avatar and main info section
 * 2. Accordion section
 */
function mct_generate_user_statistic_body()
{
    //This code will work if in top select drop-down will be selected some user
    if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
        //We search user by it's ID
        $args = array(
            'blog_id' => 1,
            'search' => $_POST['user_id'],
            'search_columns' => array('id'),
            'number' => 1,
            'order' => 'ASC',
            'orderby' => 'email',
        );
        $userQuery = new WP_User_Query($args);
        //If query is correct we can generate HTML
        if (isset($userQuery->results) && !empty($userQuery->results)) {

            foreach ($userQuery->results as $user) {
                //Function for point one - Avatar and main info section
                mct_get_user_statistic_header($user);
                //Function for point two - Accordion section
                mct_get_user_statistic_body($user);
            }

        }
    }
}

/**
 * This function generates HTML for user header info - avatar and main info
 * @param WP_User $user
 */
function mct_get_user_statistic_header(WP_User $user)
{
    //Getting information from user_meta table which we will need in this section
    $location = get_user_meta($user->ID, 'location', true);
    $skype = get_user_meta($user->ID, 'skype_name', true);

    ?>
    <div class="header-user-name"><?php print $user->data->user_email; ?></div>

    <div class="main-user-info-wrap">
        <div class="user-avatar"><?php echo get_avatar($user->ID); ?></div>
        <div class="user-information">
            <div class="user-location"><span class="user-info-title"><?php _e('Location'); ?>:</span> <span><?php echo $location; ?></span></div>
            <div class="user-skype"><span class="user-info-title"><?php _e('Skype'); ?>:</span> <span><?php echo $skype; ?></span></div>
            <div class="user-email"><span class="user-info-title"><?php _e('E-Mail'); ?>:</span> <span><?php echo $user->data->user_email; ?></span></div>
        </div>
    </div>

    <?php
}

/**
 * Function includes Mct_User_History. C.O.
 */
function include_mct_history_class()
{
    $plugin_dir = plugin_dir_path(__FILE__);
    $plugin_dir = str_replace('\\', '/', $plugin_dir);
    include($plugin_dir . 'classes/Mct_User_History.php');
}

/**
 * This function generates user history and HTML for user accordion section
 * @param WP_User $user
 */
function mct_get_user_statistic_body(WP_User $user)
{
    //Including class to generating history for required user
    include_mct_history_class();

    $User_History = new Mct_User_History($user);
    //Getting array of user history
    $history = $User_History->getUserHistory();

    //If we got correct array in previous action - generates HTML for it
    if (isset($history[$user->ID]) && !empty($history[$user->ID])) {
        mct_get_user_history_html($history[$user->ID]);
    }

}

/**
 * Function creates section for history accordion
 * @param $user_history_array
 */
function mct_get_user_history_html($user_history_array)
{
    //Get history array from common $user_history_array
    $history = $user_history_array['history'];
    if (isset($history) && !empty($history)) { ?>
        <div class="user-history-wrapper">
            <?php
            //Generating HTML for each element
            foreach ($history as $history_element) {
                mct_get_user_history_element_html($history_element);
            }
            ?>
            <!-- Pagination UL-->
            <ul class="pagination" id="jquery-pagination"></ul>
            <!-- End Pagination UL-->
        </div>
        <?php
    }
}

/**
 * This function generates HTML for one accordion element
 * @param $history_element
 */
function mct_get_user_history_element_html($history_element)
{
    //Get first 50 symbols from comment of current history element
    $title = format_history_element_title($history_element['comment']);
    $author = get_user_by('id', $history_element['author_id']);
    switch_to_blog(5);
    global $wpdb;
    $table = $wpdb->prefix . 'wpcw_courses';
    $SQL = $wpdb->prepare("SELECT *
			FROM $table
			WHERE course_id = %d
			", $history_element['course_level']);

    $course_details = $wpdb->get_row($SQL);
    restore_current_blog();

    ?>
    <div class="history-elem-wrap">
        <div class="he-header">

            <?php if (isset($history_element['lesson_date']) && !empty($history_element['lesson_date'])) : ?>
                <span class="he-date"><?php echo $history_element['lesson_date']; ?></span>
            <?php endif; ?>
            <?php if (isset($title) && !empty($title)) : ?>
                <span class="he-comment"><?php echo $title; ?></span>
            <?php endif; ?>
            <span class="he-info-open">I</span>
        </div>
        <div class="he-body">

            <span class="he-info-close">minus</span>

            <div class="he-b-date">
                <span class="he-b-date-title"><?php _e('Lesson date'); ?> : </span>

                <?php if (isset($history_element['lesson_date']) && !empty($history_element['lesson_date'])) : ?>
                    <span class="he-b-date-value"><?php echo $history_element['lesson_date']; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-course">
                <span class="he-b-course-title"><?php _e('Course'); ?> : </span>

                <?php if (isset($history_element['course_group']) && !empty($history_element['course_group'])) : ?>
                    <span class="he-b-course-value"><?php echo $history_element['course_group']; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-level">
                <span class="he-b-level-title"><?php _e('Level'); ?> : </span>

                <?php if (isset($course_details) && !empty($course_details)) : ?>
                    <span class="he-b-level-value"><?php echo $course_details->course_number; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-module">
                <span class="he-b-module-title"><?php _e('Module'); ?> : </span>

                <?php if (isset($history_element['module_id']) && !empty($history_element['module_id'])) : ?>
                    <span class="he-b-module-value"><?php echo $history_element['module_id']; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-lesson">
                <span class="he-b-lesson-title"><?php _e('Lesson'); ?> : </span>

                <?php if (isset($history_element['lesson_id']) && !empty($history_element['lesson_id'])) : ?>
                    <span class="he-b-lesson-value"><?php echo $history_element['lesson_id']; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-comment">
                <span class="he-b-comment-title"><?php _e('Comment'); ?> : </span>

                <?php if (isset($history_element['comment']) && !empty($history_element['comment'])) : ?>
                    <span class="he-b-comment-value"><?php echo $history_element['comment']; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-teacher">
                <span class="he-b-comment-title"><?php _e('Teacher'); ?> : </span>

                <?php if (isset($author->data->display_name) && !empty($author->data->display_name)) : ?>
                    <span class="he-b-comment-value"><?php echo $author->data->display_name; ?></span>
                <?php endif; ?>

            </div>

        </div>
    </div>
    <?php
}

/**
 * Return first 50 symbols of entered string
 * @param $string
 * @return string
 */
function format_history_element_title($string)
{
    if (strlen($string) <= 50) {
        //Return entered string if is 50 symbole or less
        return $string;
    } else {
        return substr($string, 0, 50) . '...';
    }
}

/**
 * Attach accordion script
 */
function mct_body_accordion_script()
{
    wp_enqueue_script('mct-user-statistic-accordion', plugins_url('assets/js/mct_user_statistic_accordion.js', __FILE__), array('jquery'), '1.0', true);
    //Add pagination plugin
    wp_enqueue_script('mct-user-statistic-pagination', plugins_url('assets/js/jquery.twbsPagination.js', __FILE__), array('jquery'), '1.0', true);
}


/***********************************************************************************************************************
 * MY CHINESE HUB CONTENT CREATING PART
 * All code below will depends with MyChineseHub site.
 ***********************************************************************************************************************/


/**
 * Content creating for MyChinese Hub statistic page
 * @return string
 */
function get_mc_hub_statistic_content()
{
    ob_start();
    //Select form for MyChinese Hub and Tutor is the same
    //So we use MCY function
    mct_generate_select_user_form();

    //Print statistic body
    mch_generate_user_statistic_body();

    return ob_get_clean();
}

/**
 * Function generate user statistic body for MyChineseHub
 */
function mch_generate_user_statistic_body()
{
    //This code will work if in top select drop-down will be selected some user
    if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
        //We search user by it's ID
        $args = array(
            'blog_id' => 1,
            'search' => $_POST['user_id'],
            'search_columns' => array('id'),
            'number' => 1,
            'order' => 'ASC',
            'orderby' => 'email',
        );
        $userQuery = new WP_User_Query($args);
        //If query is correct we can generate HTML
        if (isset($userQuery->results) && !empty($userQuery->results)) {

            foreach ($userQuery->results as $user) {
                //Function for point one - Avatar and main info section
                //We use MCT function again - this information will be the same
                mct_get_user_statistic_header($user);
                //Function for point two - Accordion section
                mch_get_user_statistic_body($user);
            }

        }
    }
}

/**
 * Configure SQL query form user history
 * @param WP_User $user
 */
function mch_get_user_statistic_body(WP_User $user)
{
    global $wpdb;
    //Set table name to variable
    $table_name = $wpdb->prefix . "teachers_user_list";
    //Get user history object
    $full_history_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user->ID . " ORDER BY complete_date DESC", OBJECT);

    if (isset($full_history_sql) && !empty($full_history_sql)) {
        //If we got correct user history - generate HTML for it
        get_user_history_html($full_history_sql);
    }
}

/**
 * Function creates section for history accordion
 * @param $full_history
 * @return bool
 */
function get_user_history_html($full_history)
{
    if (isset($full_history) && !empty($full_history)) {
        ?>
        <div class="user-history-wrapper">
            <?php
            //Generating HTML for each element
            foreach ($full_history as $history_element) {
                mch_get_user_history_element_html($history_element);
            }
            ?>
            <!-- Pagination UL-->
            <ul class="pagination" id="jquery-pagination"></ul>
            <!-- End Pagination UL-->
        </div>
        <?php
    } else {
        return false;
    }
}

/**
 * This function generates HTML for one accordion element
 * @param $history_element
 */
function mch_get_user_history_element_html($history_element)
{
    //Extract variables from History Element object
    $history_lesson_completed_time = $history_element->complete_date;
    $author = get_user_by('id', $history_element->author_id);
    $history_course_id = $history_element->course_level_id;
    $history_module_id = $history_element->module_id;
    $history_lesson_id = $history_element->lesson_id;
    $history_comment = str_replace('\\', '', $history_element->comment);
    $normal_date = date('d/m/y', $history_lesson_completed_time);
    // lesson information
    $course_details = WPCW_courses_getCourseDetails($history_course_id);
    $module_details = WPCW_modules_getModuleDetails($history_module_id);
    $lesson_number = (int)get_lesson_number($history_lesson_id)->unit_number - 1;
    if($lesson_number < 0) {
        $lesson_number = false;
    }
    if($lesson_number === 0) {
        $lesson_number = 'Intro';
    }
    //Get first 50 symbols from comment of current history element
    $title = format_history_element_title($history_comment);
    ?>

    <div class="history-elem-wrap">
        <div class="he-header">

            <?php if (isset($normal_date) && !empty($normal_date)) : ?>
                <span class="he-date"><?php echo $normal_date; ?></span>
            <?php endif; ?>
            <?php if (isset($title) && !empty($title)) : ?>
                <span class="he-comment"><?php echo $title; ?></span>
            <?php endif; ?>
            <span class="he-info-open">I</span>
        </div>
        <div class="he-body">

            <span class="he-info-close">minus</span>

            <div class="he-b-date">
                <span class="he-b-date-title"><?php _e('Lesson date'); ?> : </span>

                <?php if (isset($normal_date) && !empty($normal_date)) : ?>
                    <span class="he-b-date-value"><?php echo $normal_date; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-course">
                <span class="he-b-course-title"><?php _e('Pathway'); ?> : </span>

                <?php if (isset($course_details) && !empty($course_details)) : ?>
                    <span class="he-b-course-value"><?php echo $course_details->course_title; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-module">
                <span class="he-b-module-title"><?php _e('Module'); ?> : </span>

                <?php if (isset($module_details) && !empty($module_details)) : ?>
                    <span class="he-b-module-value"><?php echo $module_details->module_number; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-lesson">
                <span class="he-b-lesson-title"><?php _e('Lesson'); ?> : </span>

                <?php if (isset($lesson_number) && !empty($lesson_number)) : ?>
                    <span class="he-b-lesson-value"><?php echo $lesson_number; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-comment">
                <span class="he-b-comment-title"><?php _e('Comment'); ?> : </span>

                <?php if (isset($history_comment) && !empty($history_comment)) : ?>
                    <span class="he-b-comment-value"><?php echo $history_comment; ?></span>
                <?php endif; ?>

            </div>

            <div class="he-b-teacher">
                <span class="he-b-comment-title"><?php _e('Teacher'); ?> : </span>

                <?php if (isset($author->data->display_name) && !empty($author->data->display_name)) : ?>
                    <span class="he-b-comment-value"><?php echo $author->data->display_name; ?></span>
                <?php endif; ?>

            </div>

        </div>
    </div>

    <?php

}

