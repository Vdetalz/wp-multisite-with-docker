<?php

/**
 * Class for generating correct user bookings history on My Chinese Tutor site
 * Class Mct_User_History
 */
class Mct_User_History extends MCT_Lessons_History
{
    private $_user;

    public function __construct($user)
    {
        $this->_user = $user;
    }

    /**
     * Function returns User History array
     * @return array|bool|void
     */
    public function getUserHistory()
    {
        $events_list = $this->get();

        return $events_list;
    }

    /**
     * Getting user history array
     * @return array|void
     */
    public function get(){
        $users_list = $subscriber = array();
        //Get user from $this->_user variable
        $users_list = self::get_history_users_list();

        if(empty($users_list)){
            return;
        }

        //Setting up required variables which will be needed on work with UserHistory
        foreach($users_list as $user_info){
            $subscriber_id = $user_info->subscriber_id;
            $user_data = get_userdata($subscriber_id);
            $subscriber[$subscriber_id]['id']       = $subscriber_id;
            $subscriber[$subscriber_id]['nickname'] = get_user_meta($user_info->subscriber_id, 'nickname', true);
            $subscriber[$subscriber_id]['email']    = $user_data->data->user_email;
            $subscriber[$subscriber_id]['skype']    = get_user_meta($subscriber_id, 'rpr_skype_name', true);;
            $subscriber[$subscriber_id]['history']  = MCT_Lessons_List::get_subscriber_history($user_info->subscriber_id);
            $subscriber[$subscriber_id]['complete_date']  = date('Y-m-d', $user_info->complete_date);
            $subscriber[$subscriber_id]['complete_time']  = date('H:i', $user_info->complete_date);
            $subscriber[$subscriber_id]['message']  = $user_info->message;
        }

        return $subscriber;
    }

    /**
     * Returns User array which connected with $this->_user variable
     * @return mixed
     */
    protected function get_history_users_list(){
        global $wpdb;
        $start = 0;
        $limit = 10;

        $users_list = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM
                 (SELECT * FROM " . $wpdb->prefix . 'teachers_user_list' . " WHERE `subscriber_id` = %d ORDER BY `complete_date` DESC ) `list`
                  GROUP BY `subscriber_id` LIMIT %d, %d",
                $this->_user->ID,
                $start,
                $limit
            )
        );

        return $users_list;

    }
}