<?php
/*
Plugin Name: My Chinese Network Configuration
Plugin URI:
Description: Plugin with common settings for all network sites.
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/
define('USER_HOURS_TABLE', 'users_hours_balance');
define('USER_LOGS_TABLE', 'users_logs_info');

$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include_once($plugin_dir . 'common_subscription_page.php');

/*************************
 * Include user statistic page
 *************************/

$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include($plugin_dir . 'mch_user_statistic_page.php');

add_action('user_register', 'add_trial_when_user_reg');
add_action('init', 'free_trial_user_access');
add_action('edd_pre_complete_purchase', 'add_meta_data_on_payment');
add_action('user_register', 'auto_access_all_courses');

/**
 * Add 24 hours of trial period when user register
 *
 * @param $user_id
 */
function add_trial_when_user_reg($user_id)
{
    $current_time = time();
    $sec_in_day = 86400;

    if ($tutor_trial_days = get_site_option('mychinese_tutor_trial_period')) {
        $tutor_trial_time = $tutor_trial_days * $sec_in_day + $current_time;
        update_user_meta($user_id, 'tutor_ending_trial_time', $tutor_trial_time);
        update_user_meta($user_id, 'tutor_trial_lesson', 'true');
    }

    if ($hub_trial_days = get_site_option('mychinese_hub_trial_period')) {
        $hub_trial_time = $hub_trial_days * $sec_in_day + $current_time;
        update_user_meta($user_id, 'hub_ending_trial_time', $hub_trial_time);
    }

    add_balance_on_user_register($user_id);
}

function add_balance_on_user_register($user_id){
    if(empty($user_id)){
        return;
    }

    global $wpdb, $switched;
    $hours_save = null;

    $tutor_trial_days = get_site_option('mychinese_tutor_trial_period');
    $expiration_date = strtotime('+' . $tutor_trial_days . ' days', time());

    switch_to_blog(1);
        $hours_balance_table = $wpdb->prefix . USER_HOURS_TABLE;
        $user_logs_table = $wpdb->prefix . USER_LOGS_TABLE;
    restore_current_blog();

    $hours_save = $wpdb->insert(
        $hours_balance_table ,
        array(
            'user_id'        =>  $user_id,
            'expired_date'   =>  $expiration_date,
            'balance'        =>  '1',
            'download_id'    => '',
            'price_id'       => ''
        )
    );

    if(empty($hours_save)){
          return;
    }

    $wpdb->insert(
        $user_logs_table,
        array(
            'user_id'          =>  $user_id,
            'date'             =>  current_time('mysql'),
            'action'           =>  __('User Registered'),
            'change_balance'   =>  '+1',
            'balance'          =>  mct_get_user_balance($user_id),
            'delivery_method'  =>  NULL
        )
    );

}

/**;
 * Show content of all courses if user have trial period
 */
function free_trial_user_access()
{
    $current_time = time();
    $user_id = get_current_user_id();
    $blog_details = get_blog_details();

    if ($blog_details->blog_id == 1) {
        if (!is_user_logged_in()) {
            return;
        }

        $trial_period_time = get_user_meta($user_id, 'tutor_ending_trial_time', true);
        if ((!empty($trial_period_time) && $trial_period_time) && get_user_meta($user_id, 'tutor_trial_lesson', true)) {
            if ($current_time > $trial_period_time) {
                delete_user_meta($user_id, 'tutor_trial_lesson');
                delete_user_meta($user_id, 'tutor_ending_trial_time');
            }
        }
    }

    if ($blog_details->blog_id == 5) {
        $trial_period_time = get_user_meta($user_id, 'hub_ending_trial_time', true);
        if ($trial_period_time && !empty($trial_period_time)) {
            if ($current_time < $trial_period_time) {
                remove_filter('the_content', 'edd_cr_filter_content');
            }
        }
    }
}

/**
 *Function, which set user as subscriber of each site, when he buying subscription
 */
function add_meta_data_on_payment($payment_id)
{
    $user_id = get_current_user_id();
    $cart_items = edd_get_cart_contents();
    $current_time = time();
    $blog_details = get_blog_details();
    //edd_empty_cart();
    if ($cart_items && !empty($cart_items)) {
        foreach ($cart_items as $item) {
            if (isset($item['options']['recurring']['period']) && !empty($item['options']['recurring']['period'])) {
                $period = $item['options']['recurring']['period'];
            } else {
                $meta_variables = get_post_meta($item['id'], 'edd_variable_prices');
                if ($meta_variables && !empty($meta_variables)) {
                    foreach ($meta_variables as $value) {
                        if ($value && !empty($value)) {
                            foreach ($value as $val) {
                                $period = $val['period'];
                            }
                        }
                    }
                }
            }
        }
    }


    if ($period && !empty($period)) {
        switch ($period) {

            case 'day':
                $additional_time = 86400;
                break;

            case 'week':
                $additional_time = 86400 * 7;
                break;

            case 'month':
                $additional_time = 86400 * 30;
                break;

            case 'year':
                $additional_time = 86400 * 365;
                break;

            case 'quart':
                $additional_time = 86400 * 30 * 3;
                break;

        }
    }

    if ($additional_time && !empty($additional_time)) {
        $ending_subscription_time = $current_time + $additional_time;
    }

    if ($blog_details->blog_id == 1) {
        $tutor_site_ending_subscr = get_user_meta($user_id, 'tutor_subscription_ending', true);

        if ($tutor_site_ending_subscr && !empty($tutor_site_ending_subscr)) {
            if ($tutor_site_ending_subscr < $ending_subscription_time) {
                update_user_meta($user_id, 'tutor_subscription_ending', $ending_subscription_time);
            }
        } else {
            update_user_meta($user_id, 'tutor_subscription_ending', $ending_subscription_time);
        }
    }

    if ($blog_details->blog_id == 5) {
        $hub_site_ending_subscr = get_user_meta($user_id, 'hub_subscription_ending', true);

        if ($hub_site_ending_subscr && !empty($hub_site_ending_subscr)) {
            if ($hub_site_ending_subscr < $ending_subscription_time) {
                update_user_meta($user_id, 'hub_subscription_ending', $ending_subscription_time);
            }
        } else {
            update_user_meta($user_id, 'hub_subscription_ending', $ending_subscription_time);
        }
    }
}

// MCT Trial Period settings page
add_action('network_admin_menu', 'mct_trial_conf_menu');

function mct_trial_conf_menu()
{
    add_submenu_page('settings.php', __('Trial Subscription Configuration'), __('Trial Configuration'), 'trial_configuration', 'trial_configuration', 'mct_trial_settings_page');
}

add_action('network_admin_edit_mct_save_trial_period', 'save_network_settings_page');

function save_network_settings_page()
{
    $success = array();
    $status = 'error';

    if (!empty($_POST['mychinese_tutor_trial_period']) || $_POST['mychinese_tutor_trial_period'] === '') {
        if (is_numeric($_POST['mychinese_tutor_trial_period'])) {
            update_site_option('mychinese_tutor_trial_period', $_POST['mychinese_tutor_trial_period']);
            $success[] = 'true';
        } else {
            $success[] = 'false';
        }
    }

    if (!empty($_POST['mychinese_hub_trial_period']) || $_POST['mychinese_hub_trial_period'] === '') {
        if (is_numeric($_POST['mychinese_hub_trial_period'])) {
            update_site_option('mychinese_hub_trial_period', $_POST['mychinese_hub_trial_period']);
            $success[] = 'true';
        } else {
            $success[] = 'false';
        }
    }

    if (!in_array('false', $success)) {
        $status = 'updated';
    }

    wp_redirect(add_query_arg(array('page' => 'trial_configuration', 'mct_status' => $status), network_admin_url('settings.php')));
    exit();
}

/**
 * Update info message
 */

function mct_trial_admin_notice()
{
    if (empty($_GET['mct_status'])) {
        return;
    }

    $status = $_GET['mct_status'];

    if ($status === 'updated') {
        $message = __('Successfully updated');
    } else {
        $message = __('Update Error: Value should be numeric');
    }

    add_settings_error(
        'mct_save_trial_period_fields',
        esc_attr('settings_updated'),
        $message,
        $status
    );

    settings_errors('mct_save_trial_period_fields');
}

add_action('network_admin_notices', 'mct_trial_admin_notice');

function mct_trial_settings_page()
{
    ?>
    <div class="wrap">
        <h2><?php _e('MCT Trial Period'); ?></h2>

        <form method="post" action="edit.php?action=mct_save_trial_period">
            <?php settings_fields('trial-settings-group'); ?>
            <?php do_settings_sections('trial-settings-group'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <h2><?php _e('Trial Period for: '); ?></h2>
                    </th>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('My Chinese Tutor'); ?></th>
                    <td>
                        <input type="text" name="mychinese_tutor_trial_period"
                               value="<?php echo esc_attr(get_site_option('mychinese_tutor_trial_period')); ?>"
                               size="4"/><?php _e(' days'); ?>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e('My Chinese Hub'); ?></th>
                    <td>
                        <input type="text" name="mychinese_hub_trial_period"
                               value="<?php echo esc_attr(get_site_option('mychinese_hub_trial_period')); ?>"
                               size="4"/><?php _e(' days'); ?>
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
<?php
}

/**
 * function checked user does have free lesson
 */

function is_free($user_id, $site_id)
{
    $blog_details = get_blog_details();
    $current_time = time();

    $sites_free_info = array(
        1 => 'tutor_ending_trial_time',
        5 => 'hub_ending_trial_time'
    );

    $trial_period_time = get_user_meta($user_id, $sites_free_info[$blog_details->blog_id], true);
    if ($trial_period_time && !empty($trial_period_time)) {
        if ($current_time < $trial_period_time) {
            return true;
        } else {
            return false;
        }
    }
}


function auto_access_all_courses($user_id)
{
    global $wpdb;

    $SQL = "SELECT *
			FROM ch_5_wpcw_courses
			ORDER BY course_title
			";

    $course_list = $wpdb->get_results($SQL);

    foreach ($course_list as $item) {
        $courseID = $item->course_id;

        $wpdb->query($wpdb->prepare("
							INSERT INTO ch_5_wpcw_user_courses (user_id, course_id, course_progress) VALUES(%d, %d, 0)
							", $user_id, $courseID));

    }
}

/**
 * Function, which adding "Students" user role
 */

function add_student_role()
{
    add_role(
        'students',
        __('Students'),
        array(
            'read' => true,
            'edit_posts' => false,
            'delete_posts' => false,
            'level_0' => true
        )
    );

}

function delete_student_role()
{
    remove_role('student');
}

add_action('init', 'add_student_role');


//register_deactivation_hook(__FILE__, 'delete_student_role');


function add_quartal_period($periods = NULL)
{

    $periods = array(
        'day' => _x('Daily', 'Billing period', 'edd-recurring'),
        'week' => _x('Weekly', 'Billing period', 'edd-recurring'),
        'month' => _x('Monthly', 'Billing period', 'edd-recurring'),
        'quart' => _x('Quarterly', 'Billing period', 'edd-recurring'),
        'year' => _x('Yearly', 'Billing period', 'edd-recurring'),
    );

    return $periods;
}

add_filter('edd_recurring_periods', 'add_quartal_period');


/**
 *
 *
 * ************************
 * EDD  Functions
 * ************************
 *
 *
 */

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (is_plugin_active('easy-digital-downloads/easy-digital-downloads.php')) {
    remove_action('edd_purchase_form_after_cc_form', 'edd_checkout_submit', 9999);
    remove_action('edd_purchase_form_before_submit', 'edd_checkout_final_total', 999);
    remove_action('edd_checkout_form_top', 'edd_show_payment_icons');
    remove_action('edd_purchase_form_before_submit', 'edd_terms_agreement');
    remove_action('edd_checkout_form_top', 'edd_discount_field', -1);
    remove_action('edd_purchase_form_register_fields', 'edd_get_register_fields');
    remove_action('edd_purchase_form_login_fields', 'edd_get_login_fields');
    remove_action('edd_purchase', 'edd_process_purchase_form');
    remove_action('wp_ajax_edd_process_checkout', 'edd_process_purchase_form');
    remove_action('wp_ajax_nopriv_edd_process_checkout', 'edd_process_purchase_form');
    remove_action('edd_purchase_form_before_submit', 'edd_terms_agreement');
    remove_action('edd_payment_mode_top', 'edd_show_payment_icons');
    remove_action('edd_cart_empty', 'edd_empty_checkout_cart');

    add_action('edd_purchase_form_after_cc_form', 'mct_edd_checkout_submit');
    add_action('edd_checkout_form_top', 'mct_edd_checkout_final_total');
    add_action('edd_purchase_form_before_submit', 'mct_edd_terms_agreement');
    add_action('edd_purchase_form_before_submit', 'mct_edd_show_payment_icons');
    add_action('edd_checkout_table_discount_first', 'mct_edd_discount_field');
    add_action('edd_purchase_form_register_fields', 'mct_edd_get_register_fields');
    add_action('edd_purchase_form_login_fields', 'mct_edd_get_login_fields');

    // register by email
    add_action('edd_purchase', 'mct_edd_process_purchase_form');
    add_action('wp_ajax_edd_process_checkout', 'mct_edd_process_purchase_form');
    add_action('wp_ajax_nopriv_edd_process_checkout', 'mct_edd_process_purchase_form');
    add_action('edd_insert_user', 'mct_edd_insert_user', 10, 2);

    // redirect from empty cart
    add_action('edd_cart_empty', 'mct_edd_empty_checkout_cart');

    function mct_edd_empty_checkout_cart()
    {
        $page_id = edd_get_option('mct_edd_empty_cart_redirect');
        $purchase_page = edd_get_option('purchase_page');
        if ($page_id && $page_id <> $purchase_page) {
            $redirect = get_permalink(edd_get_option('mct_edd_empty_cart_redirect'));
            wp_redirect($redirect);
            exit();
        } else {
            echo edd_empty_cart_message();
        }
    }


    // Add settings to EDD plugin
    add_filter('edd_settings_extensions', 'mct_edd_settings', 1);

    function mct_edd_settings($settings)
    {
        // get pages list
        $args = array(
            'sort_order' => 'ASC',
            'sort_column' => 'post_title',
            'post_type' => 'page',
            'post_status' => 'publish'
        );
        $pages = get_pages($args);

        $options[''] = __('Select page');
        foreach ($pages as $page) {
            $options[$page->ID] = $page->post_title;
        }

        $new_settings = array(
            array(
                'id' => 'mct_edd_settings',
                'name' => '<strong>' . __('Pages Settings', 'edd_cr') . '</strong>',
                'type' => 'header',
            ),
            array(
                'id' => 'mct_edd_empty_cart_redirect',
                'name' => __('Empty Cart Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_tutors_page_redirect',
                'name' => __('Tutors Page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options

            ),
            array(
                'id' => 'mch_course_page_redirect',
                'name' => __('Course page redirect', 'mct_conf'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_pricing_page_redirect',
                'name' => __('Pricing Page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_pricing_page_second_step_redirect',
                'name' => __('Pricing Page Second Step Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_checkout_page_redirect',
                'name' => __('Checkout page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_subscriber_login_redirect',
                'name' => __('Subscribers login Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mct_edd_author_login_redirect',
                'name' => __('Authors login Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mch_forum_page',
                'name' => __('Forum page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mch_login_page',
                'name' => __('Login page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
            array(
                'id' => 'mch_photos_page',
                'name' => __('Photos page Redirect', 'edd_cr'),
                'type' => 'select',
                'options' => $options
            ),
        );

        $settings = array_merge($settings, $new_settings);

        return $settings;
    }


    /**
     * Renders the Checkout Submit section
     *
     * @return void
     */
    function mct_edd_checkout_submit()
    {
        ?>
        <fieldset id="edd_purchase_submit" class="purchase-submit">
            <?php do_action('edd_purchase_form_before_submit'); ?>
        </fieldset>

        <?php edd_checkout_hidden_fields(); ?>
        <?php echo edd_checkout_button_purchase(); ?>

        <?php do_action('edd_purchase_form_after_submit'); ?>
    <?php
    }


    /**
     * Renders the Checkout Agree to Terms, this displays a checkbox for users to
     * agree the T&Cs set in the EDD Settings. This is only displayed if T&Cs are
     * set in the EDD Settings.
     * @global $edd_options Array of all the EDD Options
     * @return void
     */
    function mct_edd_terms_agreement()
    {
        global $edd_options;
        if (isset($edd_options['show_agree_to_terms'])) {
            ?>
            <fieldset id="edd_terms_agreement" class="terms-agreement">
                <div id="edd_terms" style="display:none;">
                    <?php
                    do_action('edd_before_terms');
                    echo wpautop(stripslashes($edd_options['agree_text']));
                    do_action('edd_after_terms');
                    ?>
                </div>
                <div id="edd_show_terms" class="show-terms">
                    <a href="#" class="edd_terms_links e-btn-open"><?php _e('Show Terms', 'edd'); ?></a>
                    <a href="#" class="edd_terms_links e-btn-close"
                       style="display:none;"><?php _e('Hide Terms', 'edd'); ?></a>
                </div>
            </fieldset>
            <div class="check-agree">
                <input name="edd_agree_to_terms" class="required" type="checkbox" id="edd_agree_to_terms" value="1"/>
                <label
                    for="edd_agree_to_terms"><?php echo isset($edd_options['agree_label']) ? stripslashes($edd_options['agree_label']) : __('Agree to Terms?', 'edd'); ?></label>
            </div>
        <?php
        }
    }

    /**
     * Shows the final purchase total at the bottom of the checkout page
     *
     * @return void
     */
    function mct_edd_checkout_final_total()
    {
        global $blog_id;
        $user_id = get_current_user_id();
        $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
        $hub_cart = get_user_meta($user_id, 'hub_cart', true);
            if ($tutor_cart && !empty($tutor_cart)) {
                switch_to_blog(1);
                $download_id = $tutor_cart[0]['id'];
                $price_id = $tutor_cart[0]['options']['price_id'];
                $price_without_currency = edd_get_price_option_amount($download_id, $price_id);
                $price = edd_currency_filter(edd_format_amount($price_without_currency));
                $subtotal = $price_without_currency;
                restore_current_blog();
            } elseif ($hub_cart && !empty($hub_cart)) {
                switch_to_blog(5);
                $download_id = $hub_cart[0]['id'];
                $price_id = $hub_cart[0]['options']['price_id'];
                $price_without_currency = edd_get_price_option_amount($download_id, $price_id);
                $price = edd_currency_filter(edd_format_amount($price_without_currency));
                $subtotal = $price_without_currency;
                restore_current_blog();
            }
            else {
                $subtotal = edd_cart_subtotal();
                $price = edd_currency_filter(edd_format_amount(edd_get_cart_total()));
            }

        ?>
        <div id="edd_final_total_wrap" class="wrap-final-total">
            <div class="title-total"><?php _e('Total:', 'edd'); ?></div>
            <div class="edd_cart_amount final-total" data-subtotal="<?php echo $subtotal; ?>"
                 data-total="<?php echo $subtotal; ?>"><?php echo $price; ?></div>
        </div>
    <?php
    }

    /**
     * Renders the Discount Code field which allows users to enter a discount code.
     * This field is only displayed if there are any active discounts on the site else
     * it's not displayed.
     *
     * @return void
     */
    function mct_edd_discount_field()
    {

        if (isset($_GET['payment-mode']) && edd_is_ajax_disabled()) {
            return; // Only show before a payment method has been selected if ajax is disabled
        }

        if (edd_has_active_discounts() && edd_get_cart_total()) :

            $color = edd_get_option('checkout_color', 'blue');
            $color = ($color == 'inherit') ? '' : $color;
            $style = edd_get_option('button_style', 'button');
            ?>
            <div id="edd_discount_code" class="enter-discount">
                <div id="edd-discount-code-wrap" class="edd-cart-adjustment">
                    <label class="edd-label" for="edd-discount">
                        <?php _e('Have a discount code? Enter code here:', 'edd'); ?>
                        <img src="<?php echo EDD_PLUGIN_URL; ?>assets/images/loading.gif" id="edd-discount-loader"
                             style="display:none;"/>
                    </label>
                    <input class="edd-input" type="text" id="edd-discount" name="edd-discount"
                           placeholder="<?php _e('Enter discount', 'edd'); ?>"/>
                    <input type="submit"
                           class="edd-apply-discount edd-submit button <?php echo $color . ' ' . $style; ?>"
                           value="<?php echo _x('Apply', 'Apply discount at checkout', 'edd'); ?>"/>
                    <span id="edd-discount-error-wrap" class="edd_errors" style="display:none;"></span>
                </div>
            </div>

        <?php
        endif;
    }

    /**
     * Renders the user registration fields. If the user is logged in, a login
     * form is displayed other a registration form is provided for the user to
     * create an account.
     *
     * @return string
     */
    function mct_edd_get_register_fields()
    {

        global $edd_options;
        global $user_ID;

        if (is_user_logged_in())
            $user_data = get_userdata($user_ID);

        $show_register_form = edd_get_option('show_register_form', 'none');

        ob_start(); ?>
        <fieldset id="edd_register_fields">

            <?php if ($show_register_form == 'both') { ?>
                <p id="edd-login-account-wrap"><?php _e('Already have an account?', 'edd'); ?> <a
                        href="<?php echo add_query_arg('login', 1); ?>" class="edd_checkout_register_login e-btn"
                        data-action="checkout_login"><?php _e('Login', 'edd'); ?></a></p>
            <?php } ?>

            <?php do_action('edd_register_fields_before'); ?>

            <fieldset id="edd_register_account_fields" class="checkout-user-info">
                <legend><?php _e('Create an account', 'edd');
                    if (!edd_no_guest_checkout()) {
                        echo ' ' . __('(optional)', 'edd');
                    } ?></legend>
                <?php do_action('edd_register_account_fields_before'); ?>
                <div id="edd-user-login-wrap" class="wrap-block-info">
                    <div class="item-title">
                        <label for="edd_user_login">
                            <?php _e('Nickname', 'edd'); ?>
                            <?php if (edd_no_guest_checkout()) { ?>
                                <span class="edd-required-indicator">*</span>
                            <?php } ?>
                        </label>
                    </div>
                    <div class="item-input">
                        <input name="edd_user_login" id="edd_user_login" class="<?php if (edd_no_guest_checkout()) {
                            echo 'required ';
                        } ?>edd-input" type="text" placeholder="<?php _e('Nickname', 'edd'); ?>"
                               title="<?php _e('Username', 'edd'); ?>"/>
                    </div>
                </div>
                <div id="edd-user-pass-wrap" class="wrap-block-info">
                    <div class="item-title">
                        <label for="password">
                            <?php _e('Password', 'edd'); ?>
                            <?php if (edd_no_guest_checkout()) { ?>
                                <span class="edd-required-indicator">*</span>
                            <?php } ?>
                        </label>
                        <span
                            class="edd-description"><?php _e('The password used to access your account.', 'edd'); ?></span>
                    </div>
                    <div class="item-input">
                        <input name="edd_user_pass" id="edd_user_pass" class="<?php if (edd_no_guest_checkout()) {
                            echo 'required ';
                        } ?>edd-input" placeholder="<?php _e('Password', 'edd'); ?>" type="password"/>
                    </div>
                </div>
                <div id="edd-user-pass-confirm-wrap" class="edd_register_password wrap-block-info">
                    <div class="item-title">
                        <label for="password_again">
                            <?php _e('Password Again', 'edd'); ?>
                            <?php if (edd_no_guest_checkout()) { ?>
                                <span class="edd-required-indicator">*</span>
                            <?php } ?>
                        </label>
                        <span class="edd-description"><?php _e('Confirm your password.', 'edd'); ?></span>
                    </div>
                    <div class="item-input">
                        <input name="edd_user_pass_confirm" id="edd_user_pass_confirm"
                               class="<?php if (edd_no_guest_checkout()) {
                                   echo 'required ';
                               } ?>edd-input" placeholder="<?php _e('Confirm password', 'edd'); ?>" type="password"/>
                    </div>
                </div>
                <?php do_action('edd_register_account_fields_after'); ?>
            </fieldset>

            <?php do_action('edd_register_fields_after'); ?>

            <input type="hidden" name="edd-purchase-var" value="needs-to-register"/>

            <?php do_action('edd_purchase_form_user_info'); ?>

        </fieldset>
        <?php
        echo ob_get_clean();
    }

    /**
     * Gets the login fields for the login form on the checkout. This function hooks
     * on the edd_purchase_form_login_fields to display the login form if a user already
     * had an account.
     *
     * @return string
     */
    function mct_edd_get_login_fields()
    {
        global $edd_options;

        $color = isset($edd_options['checkout_color']) ? $edd_options['checkout_color'] : 'gray';
        $color = ($color == 'inherit') ? '' : $color;
        $style = isset($edd_options['button_style']) ? $edd_options['button_style'] : 'button';

        $show_register_form = edd_get_option('show_register_form', 'none');

        ob_start(); ?>
        <fieldset id="edd_login_fields">
            <?php if ($show_register_form == 'both') { ?>
                <p id="edd-new-account-wrap">
                    <?php _e('Need to create an account?', 'edd'); ?>
                    <a href="<?php echo remove_query_arg('login'); ?>" class="edd_checkout_register_login e-btn"
                       data-action="checkout_register">
                        <?php _e('Register', 'edd');
                        if (!edd_no_guest_checkout()) {
                            echo ' ' . __('or checkout as a guest.', 'edd');
                        } ?>
                    </a>
                </p>
            <?php } ?>
            <?php do_action('edd_checkout_login_fields_before'); ?>
            <div id="edd-user-login-wrap" class="wrap-block-info">
                <div class="item-title">
                    <label class="edd-label" for="edd-username"><?php _e('E-mail', 'edd'); ?></label>
                </div>
                <div class="item-input">
                    <input class="<?php if (edd_no_guest_checkout()) {
                        echo 'required ';
                    } ?>edd-input" type="text" name="edd_user_login" id="edd_user_login" value=""
                           placeholder="<?php _e('E-mail', 'edd'); ?>"/>
                </div>
            </div>
            <div id="edd-user-pass-wrap" class="edd_login_password wrap-block-info">
                <div class="item-title">
                    <label class="edd-label" for="edd-password"><?php _e('Password', 'edd'); ?></label>
                </div>
                <div class="item-input">
                    <input class="<?php if (edd_no_guest_checkout()) {
                        echo 'required ';
                    } ?>edd-input" type="password" name="edd_user_pass" id="edd_user_pass"
                           placeholder="<?php _e('Your password', 'edd'); ?>"/>
                    <input type="hidden" name="edd-purchase-var" value="needs-to-login"/>
                </div>
            </div>
            <div id="edd-user-login-submit" class="wrap-submit">
                <input type="submit" class="edd-submit button <?php echo $color; ?>" name="edd_login_submit"
                       value="<?php _e('Login', 'edd'); ?>"/>
            </div>
            <?php do_action('edd_checkout_login_fields_after'); ?>
        </fieldset><!--end #edd_login_fields-->
        <?php
        echo ob_get_clean();
    }

    /**
     * Process Purchase Form
     *
     * Handles the purchase form process.
     *
     * @access      private
     * @return      void
     */
    function mct_edd_process_purchase_form()
    {
        global $blog_id;
        $_POST['edd_user_nickname'] = $_POST['edd_user_login'];
        $_POST['edd_user_login'] = $_POST['edd_email'];
        $user_id = get_current_user_id();
        $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
        $hub_cart = get_user_meta($user_id, 'hub_cart', true);
        // Make sure the cart isn't empty
        if (!edd_get_cart_contents() && !edd_cart_has_fees()) {
            $valid_data = array();
            if ($tutor_cart && !empty($tutor_cart)) {
                $download_id = (int)$tutor_cart[0]['id'];
                $options = $tutor_cart[0]['options'];
            } elseif($hub_cart && !empty($hub_cart)){
                $download_id = (int)$hub_cart[0]['id'];
                $options = $hub_cart[0]['options'];
            } else {
                edd_set_error('empty_cart', __('Your cart is empty', 'edd'));
            }
        } else {
            // Validate the form $_POST data
            $valid_data = edd_purchase_form_validate_fields();

            // Allow themes and plugins to hook to errors
            do_action('edd_checkout_error_checks', $valid_data, $_POST);
        }

        $is_ajax = isset($_POST['edd_ajax']);

        // Process the login form
        if (isset($_POST['edd_login_submit'])) {
            edd_process_purchase_login();
        }

        // Validate the user
        $user = edd_get_purchase_form_user($valid_data);

        ///if ( edd_get_errors() || ! $user ) {
//        if ( edd_get_errors() || ! $user ) {
//            if ( $is_ajax ) {
//                do_action( 'edd_ajax_checkout_errors' );
//                edd_die();
//            } else {
//                return false;
//            }
//        }


        if ($is_ajax) {
            echo 'success';
            edd_die();
        }

        // Setup user information
        $user_info = array(
            'id' => $user['user_id'],
            'email' => $user['user_email'],
            'first_name' => $user['user_first'],
            'last_name' => $user['user_last'],
            'discount' => $valid_data['discount'],
            'address' => $user['address']
        );

        $auth_key = defined('AUTH_KEY') ? AUTH_KEY : '';

        // Setup purchase information

            if ($tutor_cart && !empty($tutor_cart)) {
                switch_to_blog(1);
                if (($download_id && $options) && (!empty($download_id) && !empty($download_id))) {
                    $User = new WP_User($user_id);
                    $user_info = array(
                        'id' => $User->id,
                        'email' => $User->user_email,
                        'first_name' => $User->first_name,
                        'last_name' => $User->last_name,
                        'discount' => '',
                        'address' => ''
                    );
                    edd_add_to_cart($download_id, $options);
                    $purchase_data = array(
                        'downloads' => edd_get_cart_contents(), //$tutor_cart
                        'fees' => edd_get_cart_fees(), // Any arbitrary fees that have been added to the cart
                        'subtotal' => edd_get_cart_subtotal(), // Amount before taxes and discounts
                        'discount' => edd_get_cart_discounted_amount(), // Discounted amount
                        'tax' => edd_get_cart_tax(), // Taxed amount
                        'price' => edd_get_cart_total(), // Amount after taxes
                        'purchase_key' => strtolower(md5($user['user_email'] . date('Y-m-d H:i:s') . $auth_key . uniqid('edd', true))), // Unique key
                        'user_email' => $user['user_email'],
                        'date' => date('Y-m-d H:i:s', current_time('timestamp')),
                        'user_info' => stripslashes_deep($user_info),
                        'post_data' => $_POST,
                        'cart_details' => edd_get_cart_content_details(),
                        'gateway' => $_POST['edd-gateway'], //$valid_data['gateway'],
                        'card_info' => $valid_data['cc_info']
                    );
                }
            } elseif ($hub_cart && !empty($hub_cart)) {
                switch_to_blog(5);
                if (($download_id && $options) && (!empty($download_id) && !empty($download_id))) {
                    $User = new WP_User($user_id);
                    $user_info = array(
                        'id' => $User->id,
                        'email' => $User->user_email,
                        'first_name' => $User->first_name,
                        'last_name' => $User->last_name,
                        'discount' => '',
                        'address' => ''
                    );
                    edd_add_to_cart($download_id, $options);
                    $purchase_data = array(
                        'downloads' => edd_get_cart_contents(), //$tutor_cart
                        'fees' => edd_get_cart_fees(), // Any arbitrary fees that have been added to the cart
                        'subtotal' => edd_get_cart_subtotal(), // Amount before taxes and discounts
                        'discount' => edd_get_cart_discounted_amount(), // Discounted amount
                        'tax' => edd_get_cart_tax(), // Taxed amount
                        'price' => edd_get_cart_total(), // Amount after taxes
                        'purchase_key' => strtolower(md5($user['user_email'] . date('Y-m-d H:i:s') . $auth_key . uniqid('edd', true))), // Unique key
                        'user_email' => $user['user_email'],
                        'date' => date('Y-m-d H:i:s', current_time('timestamp')),
                        'user_info' => stripslashes_deep($user_info),
                        'post_data' => $_POST,
                        'cart_details' => edd_get_cart_content_details(),
                        'gateway' => $_POST['edd-gateway'], //$valid_data['gateway'],
                        'card_info' => $valid_data['cc_info']
                    );
                }
            } else {
                $purchase_data = array(
                    'downloads' => edd_get_cart_contents(),
                    'fees' => edd_get_cart_fees(), // Any arbitrary fees that have been added to the cart
                    'subtotal' => edd_get_cart_subtotal(), // Amount before taxes and discounts
                    'discount' => edd_get_cart_discounted_amount(), // Discounted amount
                    'tax' => edd_get_cart_tax(), // Taxed amount
                    'price' => edd_get_cart_total(), // Amount after taxes
                    'purchase_key' => strtolower(md5($user['user_email'] . date('Y-m-d H:i:s') . $auth_key . uniqid('edd', true))), // Unique key
                    'user_email' => $user['user_email'],
                    'date' => date('Y-m-d H:i:s', current_time('timestamp')),
                    'user_info' => stripslashes_deep($user_info),
                    'post_data' => $_POST,
                    'cart_details' => edd_get_cart_content_details(),
                    'gateway' => $valid_data['gateway'],
                    'card_info' => $valid_data['cc_info']
                );
            }

        // Add the user data for hooks
        $valid_data['user'] = $user;

        // Allow themes and plugins to hook before the gateway
        do_action('edd_checkout_before_gateway', $_POST, $user_info, $valid_data);

        // If the total amount in the cart is 0 and returned from paypal. This redirected to pricing page
        if(!$purchase_data['price'] && $purchase_data['post_data']['edd-gateway'] == 'paypal' && !$purchase_data['user_id']) {
            wp_redirect(home_url() . '/pricing');
        }
        // If the total amount in the cart is 0, send to the manual gateway. This emulates a free download purchase
        elseif (!$purchase_data['price']) {
            // Revert to manual
            $purchase_data['gateway'] = 'manual';
            $_POST['edd-gateway'] = 'manual';
        }

        // Allow the purchase data to be modified before it is sent to the gateway
        $purchase_data = apply_filters(
            'edd_purchase_data_before_gateway',
            $purchase_data,
            $valid_data
        );

        // Setup the data we're storing in the purchase session
        $session_data = $purchase_data;

        // Make sure credit card numbers are never stored in sessions
        unset($session_data['card_info']['card_number']);

        // Used for showing download links to non logged-in users after purchase, and for other plugins needing purchase data.
        edd_set_purchase_session($session_data);

        // Send info to the gateway for payment processing


        if($tutor_cart) {
            if ($purchase_data['gateway'] == 'manual') {
                edd_manual_payment($purchase_data);
                restore_current_blog();
            }
            if ($purchase_data['gateway'] == 'paypal') {
                edd_process_paypal_purchase($purchase_data);
            }
        }
        elseif($hub_cart){
            if ($purchase_data['gateway'] == 'manual') {
                edd_manual_payment($purchase_data);
                restore_current_blog();
            }
            if ($purchase_data['gateway'] == 'paypal') {
                edd_process_paypal_purchase($purchase_data);
            }
        }
        else{
            edd_send_to_gateway($purchase_data['gateway'], $purchase_data);
            edd_die();
        }


    }

    /**
     * Added nickname for user
     *
     */
    function mct_edd_insert_user($user_id, $user_data)
    {
        update_user_meta($user_id, 'nickname', $_POST['edd_user_nickname']);
    }

    /**
     * Show Payment Icons by getting all the accepted icons from the EDD Settings
     * then outputting the icons.
     *
     * @since 1.0
     * @return void
     */
    function mct_edd_show_payment_icons()
    {

        if (edd_show_gateways() && did_action('edd_payment_mode_top')) {
            return;
        }

        $payment_methods = edd_get_option('accepted_cards', array());

        if (empty($payment_methods)) {
            return;
        }

        echo '<div class="add-payment-icons">';

        foreach ($payment_methods as $key => $card) {

            if (edd_string_is_image_url($key)) {

                echo '<img class="payment-icon" src="' . esc_url($key) . '"/>';

            } else {

                $card = strtolower(str_replace(' ', '', $card));

                if (has_filter('edd_accepted_payment_' . $card . '_image')) {

                    $image = apply_filters('edd_accepted_payment_' . $card . '_image', '');

                } else {

                    $image = edd_locate_template('images' . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR . $card . '.gif', false);
                    $content_dir = WP_CONTENT_DIR;

                    if (function_exists('wp_normalize_path')) {

                        // Replaces backslashes with forward slashes for Windows systems
                        $image = wp_normalize_path($image);
                        $content_dir = wp_normalize_path($content_dir);

                    }

                    $image = str_replace($content_dir, WP_CONTENT_URL, $image);

                }

                if (edd_is_ssl_enforced() || is_ssl()) {

                    $image = edd_enforced_ssl_asset_filter($image);

                }

                switch ($key) {
                    case 'mastercard':
                        echo '<i class="fa fa-credit-card"></i>';
                        break;
                    case 'visa':
                        echo '<i class="fa fa-cc-visa"></i>';
                        break;
                    case 'americanexpress':
                        echo '<i class="fa fa-cc-amex"></i>';
                        break;
                    case 'discover':
                        echo '<i class="fa fa-cc-discover"></i>';
                        break;
                    default:
                        echo '<i class="fa fa-' . $key . '"></i>';
                }

            }

        }

        echo '</div>';
    }
}


/**
 *
 *
 * ************************
 * PRICING TABLE (In the future will be multisite)
 * ************************
 *
 *
 */

/**
 * The function register entity with rows for pricing table
 */

function mcn_create_pricing_table_entity()
{

    if (post_type_exists('pricing_table_rows')) return;

    register_post_type('pricing_table_rows',
        array(
            'labels' => array(
                'name' => __('Pricing table rows'),
                'singular_name' => __('Pricing table row')
            ),
            'public' => true,
        )
    );

}

add_action('init', 'mcn_create_pricing_table_entity');


/**
 * Delete textarea body from pricing_table_rows post type
 */

function mcn_delete_editor()
{
    remove_post_type_support('pricing_table_rows', 'editor');
}

add_action('init', 'mcn_delete_editor');


/**
 * Add custom post type for characteristics of the pricing table
 */

function mcn_create_pricing_table_characteristics()
{

    if (post_type_exists('pt_characteristics')) return;

    register_post_type('pt_characteristics',
        array(
            'labels' => array(
                'name' => __('Pricing Table Characteristics'),
                'singular_name' => __('Pricing Table Characteristic')
            ),
            'public' => true,
            'show_in_menu' => false
        )
    );

}

add_action('init', 'mcn_create_pricing_table_characteristics');


/**
 * Function add new columns in the Pricing rows table
 * @param $columns
 * @return array
 */

function mcn_add_pricing_table_columns($columns)
{

    $new_columns = array(
        'mch_course_group' => __('Course Group'),
        'mch_is_recommended' => __('Recommended'),
    );
    return array_merge($columns, $new_columns);
}

add_filter('manage_pricing_table_rows_posts_columns', 'mcn_add_pricing_table_columns');


/**
 * The function append values in the custom rows
 * @param $column
 * @param $post_id
 */

function mcn_custom_columns($column, $post_id)
{
    switch ($column) {
        case 'mch_course_group' :
            echo get_the_title(get_post_meta($post_id, 'mch_course_group', true));
            break;
        case 'mch_is_recommended' :
            echo get_post_meta($post_id, 'mch_is_recommended_pricing_row', true) == 'true' ? '<b style="color:green;">Recommended</b>' : '';
            break;
    }
}

add_action('manage_posts_custom_column', 'mcn_custom_columns', 10, 2);


/**
 * The function create a metabox with settings of created rows
 */

function mcn_create_pricing_table_rows_metabox()
{

    add_meta_box(
        'mch_pricing_table_rows_metabox',
        __('Pricing Table Settings'),
        'mcn_pricing_table_rows_metabox_callback',
        'pricing_table_rows'
    );

}

add_action('add_meta_boxes', 'mcn_create_pricing_table_rows_metabox');

/**
 * The function create a metabox with characteristics of download
 */

function mcn_create_download_characteristics_metabox()
{

    global $post;

    if (!mcn_get_pricing_table_rows('mch_course_group', $post->ID)) return;

    add_meta_box(
        'mch_download_characteristics_metabox',
        __('Pricing Table Characteristics'),
        'mcn_pricing_table_characteristics_metabox_callback',
        'download'
    );

}

add_action('add_meta_boxes', 'mcn_create_download_characteristics_metabox');


/**
 * View of the download characteristics metabox
 */

function mcn_pricing_table_characteristics_metabox_callback()
{

    global $post;

    $rows = mcn_get_pricing_table_rows('mch_course_group', $post->ID);
    $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $post->ID);

    ?>

    <button class="button-secondary add_pricing_table_characteristic"><?php _e('Add New Characteristic'); ?></button>
    <input type="hidden" name="json_price_entity_data" value='<?php echo json_encode($rows); ?>'/>
    <input type="hidden" name="entities_num" value='<?php echo count($characteristics); ?>'/>

    <p><?php _e('Enter your pricing table characteristics.'); ?></p>

    <table class="widefat edd_repeatable_table">
        <thead>
        <tr>
            <th><?php _e('Title'); ?></th>
            <?php foreach ($rows as $row): ?>
                <th style="text-align:center;"><?php _e($row); ?></th>
            <?php endforeach; ?>
            <th></th>
        </tr>
        </thead>
        <tbody class="ui-sortable pricing_table_sharacteristics_body">

        <?php if ($characteristics): $i = 0; ?>

            <?php foreach ($characteristics as $characteristic): $i++;; ?>

                <tr class="edd_variable_prices_wrapper edd_repeatable_row add_pricing_table_characteristic_form">
                    <td>
                        <input type="text" name="pricing_table_characteristics[<?php echo $i; ?>][title]"
                               value="<?php echo $characteristic->post_title; ?>"
                               placeholder="Enter your Characteristic Title here" style="width:100%"/>
                        <input type="hidden" name="pricing_table_characteristics[<?php echo $i; ?>][post]"
                               value="<?php echo $characteristic->ID; ?>">
                    </td>

                    <?php foreach ($rows as $key => $row): ?>
                        <td style="text-align:center;">
                            <input
                                style="width:70px;"
                                type="text" value="<?php echo get_post_meta($characteristic->ID, 'post-' . $key . '-check', true); ?>"
                                name="pricing_table_characteristics[<?php echo $i; ?>][checkboxes][<?php echo $key; ?>]"/>
                        </td>
                    <?php endforeach; ?>
                    <td>
                            <span class="trash">
                                <a href="javascript:void(0)" class="delete_characteristics_link"
                                   data="<?php echo $characteristic->ID; ?>"><?php _e('Delete'); ?></a>
                            </span>
                    </td>
                </tr>

            <?php endforeach; ?>

        <?php endif; ?>

        </tbody>
    </table>

<?php

}

/**
 * The view of the settings metabox
 */

function mcn_pricing_table_rows_metabox_callback()
{

    global $post;

    $downloads = mcn_get_downloads();

    ?>
    <p>
        <label for="mch_course_group"><?php _e('Main Course Group: '); ?></label>
        <select name="mch_course_group">
            <?php if ($downloads): ?>
                <option value="0"><?php echo __('-- Select Course Group --'); ?></option>
                <?php foreach ($downloads as $download): ?>
                    <option
                        value="<?php echo $download->ID; ?>" <?php selected($download->ID, get_post_meta($post->ID, 'mch_course_group', true)); ?>><?php echo __($download->post_title); ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </p>

    <p>
        <label for="mch_is_recommended"><?php _e('Recommended this Entity: '); ?></label>
        <input type="checkbox"
               name="mch_is_recommended_pricing_row" <?php checked(get_post_meta($post->ID, 'mch_is_recommended_pricing_row', true), 'true'); ?>/>
    </p>

<?php

}

/**
 * The function save settings of entity of Price table
 */

function mcn_save_table_rows_settings()
{

    global $post;

    if ($_POST['mch_course_group']) {
        update_post_meta($post->ID, 'mch_course_group', $_POST['mch_course_group']);

        if (isset($_POST['mch_is_recommended_pricing_row']))
            update_post_meta($post->ID, 'mch_is_recommended_pricing_row', 'true');
        else
            update_post_meta($post->ID, 'mch_is_recommended_pricing_row', 'false');
    } else return;

}

/**
 * The function return array of Course Groups
 * @return array|bool
 */

function mcn_get_downloads()
{

    $downloads = get_posts(array(
        'post_type' => 'download',
        'numberposts' => '-1',
        'post_status' => 'publish'
    ));

    if (!$downloads) return false;

    return $downloads;

}

add_action('save_post', 'mcn_save_table_rows_settings');


/**
 * Function add new row in the download metabox table
 * @param $post_id
 */

function mcn_edd_download_table_head($post_id)
{
    ?>
    <th><?php _e('Pricing Table Row', 'edd'); ?></th>
<?php
}

add_action('edd_download_price_table_head', 'mcn_edd_download_table_head');

/**
 * get pricing_table_rows postype
 * @return bool
 */

function mcn_get_pricing_table_rows($key = '', $value = '')
{

    $meta_query = '';

    if ($key && $value)
        $meta_query = array(
            array(
                'key' => $key,
                'value' => $value,
            )
        );

    $rows = get_posts(array(
        'post_type' => 'pricing_table_rows',
        'meta_query' => $meta_query,
        'orderby' => 'ID',
        'order' => 'ASC',
        'numberposts' => '-1',
    ));

    if (!$rows) return false;

    foreach ($rows as $row)
        $new_rows[$row->ID] = $row->post_title;

    return $new_rows;
}

/**
 * @param $download_id
 * @param $key
 * @param $args
 */

function mcn_edd_pricing_table_rows_metabox($download_id, $key, $args)
{

    global $post;

    $pricing_table_rows = mcn_get_pricing_table_rows('mch_course_group', $download_id);

    ?>
    <td>
        <?php echo EDD()->html->select(array(
            'name' => 'edd_variable_prices[' . $key . '][pricing_table_row]',
            'options' => $pricing_table_rows,
            'class' => 'edd_variable_prices_pricing_table_row',
            'show_option_all' => false,
            'selected' => get_post_meta($post->ID, 'edd_variable_prices_row_' . $key, true),
            'show_option_none' => _x('None', 'no dropdown items', 'edd'),

        )); ?>
    </td>
<?php

}

add_action('edd_download_price_table_row', 'mcn_edd_pricing_table_rows_metabox', 999, 3);

/**
 * Save download table custom row
 * @param $post_id
 */

function mcn_edd_pricing_table_rows_metabox_save($post_id)
{
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['mct_nonce']) && wp_verify_nonce($_POST['mct_nonce'], basename(__FILE__))) ? 'true' : 'false';

    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }

    if (isset($_POST['edd_variable_prices']) && is_array($_POST['edd_variable_prices'])) {
        foreach ($_POST['edd_variable_prices'] as $key => $value) {

            update_post_meta($post_id, 'edd_variable_prices_row_' . $key, $value['pricing_table_row']);
        }
    }
}

add_action('save_post', 'mcn_edd_pricing_table_rows_metabox_save');

/**
 * The function append js file to project
 */

function mcn_append_pricing_table_script()
{
    wp_register_script('add_pricing_Table_form', plugins_url('assets/js/new_pricing_table_characteristics_form.js', __FILE__), array('jquery'), '1.0.0', true); // Custom scripts
    wp_enqueue_script('add_pricing_Table_form'); // Enqueue it!
    wp_register_script('cancel_tutor_subscr', plugins_url('assets/js/tutor_lesson_cancel.js', __FILE__), array('jquery'), '1.0.0'); // Custom scripts
    wp_enqueue_script('cancel_tutor_subscr'); // Enqueue it!
}

add_action('init', 'mcn_append_pricing_table_script');


/**
 * Handler of the characteristics metabox
 */

function mcn_save_pricing_table_characteristics_metabox()
{

    global $post;

    if ($post->post_type != 'download' || !$_POST['pricing_table_characteristics']) return;

    $data = $_POST['pricing_table_characteristics'];

    $arr_post = array();

    $rows = mcn_get_pricing_table_rows('mch_course_group', $post->ID);

    foreach ($data as $characteristic) {

        if (!$characteristic['title']) continue;


        $arr_post['post_title'] = $characteristic['title'];
        $arr_post['post_type'] = 'pt_characteristics';
        $arr_post['post_status'] = 'publish';
        $arr_post['post_author'] = 1;

        remove_action('save_post', 'mcn_save_pricing_table_characteristics_metabox');

        if (isset($characteristic['post'])) {
            $arr_post['ID'] = $characteristic['post'];
            $_id = wp_update_post($arr_post);
        } else {
            $_id = wp_insert_post($arr_post);
        }

        add_action('save_post', 'mcn_save_pricing_table_characteristics_metabox');

        foreach ($rows as $key => $row) {
            delete_post_meta($_id, 'post-' . $key . '-check');
        }

        if ($_id) {
            update_post_meta($_id, 'course_group_pricing_characteristic', $post->ID);

            if (count($characteristic['checkboxes'])) {
                foreach ($characteristic['checkboxes'] as $key => $checkbox) {
                    update_post_meta($_id, 'post-' . $key . '-check', $checkbox);

                }
            }
        }

    }

}

add_action('save_post', 'mcn_save_pricing_table_characteristics_metabox');

/**
 * Get pricing table characteristics
 * @param string $key
 * @param string $value
 * @return array|bool
 */

function mcn_get_pricing_table_characteristics($key = '', $value = '')
{

    $meta_query = '';

    if ($key && $value)
        $meta_query = array(
            array(
                'key' => $key,
                'value' => $value,
            )
        );

    $characteristics = get_posts(array(
        'post_type' => 'pt_characteristics',
        'meta_query' => $meta_query,
        'orderby' => 'ID',
        'order' => 'ASC',
        'numberposts' => '-1',
    ));

    if (!$characteristics) return false;

    return $characteristics;

}

/**
 * Ду магик блин, просто проходите мимо
 * @param $download_id
 * @return bool
 */

function mcn_get_normalized_period_array($download_id)
{

    $edd_prices = get_post_meta($download_id, 'edd_variable_prices');

    if (!$edd_prices) return false;

    $magic = array();

    foreach ($edd_prices[0] as $key => $edd) {
        if ($edd['pricing_table_row'] == '-1') continue;
        $magic[$edd['period']][$edd['pricing_table_row']] = array('price_id' => $key, 'amount' => $edd['amount']);
    }

    return $magic;

}

/**
 * return css class recommended table row
 * @param $key
 * @return string
 */

function mcn_get_recommended_row_class($key)
{

    $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);

    if ($rec == 'true')
        return "pricing_row_recommended";
    else
        return "pricing_row_unrecommended";

}


/**
 * return css class of checked\uncheched characteristic
 * @param $characteristic_id
 * @param $key
 * @return string
 */

function mcn_get_characteristic_class($characteristic_id, $key)
{

    $is_checked = get_post_meta($characteristic_id, 'post-' . $key . '-check', true);

    if ($is_checked)
        return "pricing_table_check";
    else
        return "pricing_table_uncheck";

}

/**
 * @param $data
 * The view of the pricing table
 */

function mcn_pricing_table_view($data)
{
    $periods_array = add_quartal_period();
    $downloads = array();
    extract($data);
    if( $_GET['download_id'] ) $id = $_GET['download_id'];
    if( $_GET['update_download_id'] ) $id = $_GET['update_download_id'];
    
    if (isset($_GET['download_id']) || isset($_GET['update_download_id'])) {
        foreach ($downloads as $download){
            if ($id == $download->ID) {
                $title = get_post_meta($download->ID, '_aioseop_title', true);
            }
        }
    } else {
        $page = get_post(get_the_ID());
        $title = $page->post_title; 
    }
    ?>
    <h1><?php echo $title; ?></h1>
    <div class="table-wrap">
        <?php
        foreach ($downloads as $download):

            $rows = mcn_get_pricing_table_rows('mch_course_group', $download->ID);

            $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $download->ID);
            $periods = mcn_get_normalized_period_array($download->ID);

            if ($rows && $characteristics): $_id = $download->ID;?>

                <table border="1"
                       class="pricing_table pricing_table_<?php echo $download->ID; ?>" <?php if ($id != $download->ID) echo 'style="display:none;"'; ?>>
                    <tr class="header-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):

                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>

                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>">  <?php /*returned classes pricing_table pricing_row_recommended, pricing_table pricing_row_unrecommended*/ ?>

                                <?php if ($rec == 'true'): ?>
                                    <?php _e('Recommended'); ?>
                                    <?php
                                    if (get_current_blog_id() == 1) {
                                        $badge_body = get_post_meta($download->ID, 'dwn_badge_price', true);
                                        $badge_footer = get_post_meta($download->ID, 'dwn_badge_text', true);
                                        if(isset($badge_body) && !empty($badge_body) && isset($badge_footer) && !empty($badge_footer)) {
                                            ?>
                                            <div class="pricing-badge badge-<?php echo $download->ID; ?>">
                                                <span class="dwn-badge-body"><?php echo $badge_body; ?></span>
                                                <span class="dwn-badge-footer"><?php echo $badge_footer; ?></span>
                                            </div>
                                        <?php
                                        }
                                    }
                                endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr class="title-td-table">
                        <td>
                            <?php if ((isset($_GET['download_id']) && !empty($_GET['download_id'])) || (isset($_GET['update_download_id']) && !empty($_GET['update_download_id']))): ?>
                                <?php $d_id = $_GET['download_id'] ? $_GET['download_id'] : $_GET['update_download_id'];
                                $rows = mcn_get_pricing_table_rows('mch_course_group', $d_id);
                                $characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $d_id);
                                $periods = mcn_get_normalized_period_array($d_id);
                                $download_title = get_the_title($d_id);
                                ?>
                                <?php echo $download_title; ?>
                            <?php else: ?>
                                <select name="pricing_table_switcher">
                                    <?php foreach ($downloads as $download):
                                        $_rows = mcn_get_pricing_table_rows('mch_course_group', $download->ID);
                                        $_characteristics = mcn_get_pricing_table_characteristics('course_group_pricing_characteristic', $download->ID);

                                        if (!$_rows || !$_characteristics) continue;?>

                                        <option
                                            value="<?php echo $download->ID; ?>" <?php selected($download->ID, $_id); ?>><?php _e($download->post_title); ?></option>

                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                        </td>
                        <?php foreach ($rows as $key => $row): ?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"><?php _e($row); ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($characteristics as $characteristic): ?>
                        <tr class="content-table">
                            <td><?php _e($characteristic->post_title); ?></td>
                            <?php foreach ($rows as $key => $row):
                                $check = get_post_meta($characteristic->ID, 'post-' . $key . '-check', true);?>
                                <td class="<?php echo mcn_get_recommended_row_class($key); ?> <?php echo mcn_get_characteristic_class($characteristic->ID, $key); ?>">     <?php /* return class="pricing_table pricing_table_check" or class="pricing_table pricing_table_uncheck"*/ ?>
                                    <?php if ($check == 'true' || $check == 'on'): ?>
                                        <i class="fa fa-check"></i>
                                    <?php elseif($check && $check != 'false' && $check != 'off' ): ?>
                                        <?php echo $check;?>
                                    <?php else:?>
                                        <i class="fa fa-minus"></i>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="separate-table border">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr class="separate-table">
                        <td></td>
                        <?php foreach ($rows as $key => $row):
                            $rec = get_post_meta($key, 'mch_is_recommended_pricing_row', true);?>
                            <td class="<?php echo mcn_get_recommended_row_class($key); ?>"></td>
                        <?php endforeach; ?>
                    </tr>

                    <?php foreach ($periods as $key => $period): ?>
                        <?php if (isset($_GET['download_id']) && !empty($_GET['download_id'])): ?>
                            <?php $_id = $_GET['download_id']; ?>
                        <?php endif; ?>
                        <tr class="footer-table">

                            <td><?php echo($periods_array[$key] . " Subscription: "); ?></td>
<?php
/**
 * We must delete 'if' after Sam add all content. It sound stay only
 * "<td class="<?php echo mcn_get_recommended_row_class($post); ?>">
 *  <?php if (array_key_exists($post, $rows)): ?>
 *  <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>">$<?php echo $value['amount']; ?></a>
 *   <?php endif; ?>
 *   </td>"
 */
?>
                            <?php foreach ($period as $post => $value): ?>
                            <?php $currency = edd_get_currency(); ?>
                                <?php if ($_id == 8 || $_id == 26 || $_id == 957) { ?>
                                    <?php if ($value['price_id'] !== 1 && $value['price_id'] % 5 !== 0) { ?>
                                        <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                            <?php if (array_key_exists($post, $rows)): ?>
                                                <span
                                                    class="not-avaliable"><?php echo __('Unavailable'); ?></span>
                                            <?php endif; ?>
                                        </td>
                                    <?php } else { ?>
                                        <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                            <?php if (array_key_exists($post, $rows)): ?>
                                                <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>"><?php echo $currency; ?> $<?php echo $value['amount']; ?></a>
                                            <?php endif; ?>
                                        </td>
                                    <?php } ?>
                                <?php } else { ?>
                                    <td class="<?php echo mcn_get_recommended_row_class($post); ?>">
                                        <?php if (array_key_exists($post, $rows)): ?>
                                            <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo $_id; ?>&edd_options[price_id]=<?php echo $value['price_id']; ?>"><?php echo $currency; ?> $<?php echo $value['amount']; ?></a>
                                        <?php endif; ?>
                                    </td>
                                <?php } ?>
                            <?php endforeach; ?>

                        </tr>

                    <?php endforeach; ?>

                </table>

            <?php endif; ?>

        <?php endforeach; ?>
    </div>
<?php

}


/**
 * Data of the pricing table & shortcode handler
 * @param $atts
 */

function mcn_pricing_table_handler($atts)
{

    $current = $atts['current'];

    if (!$current) return;

    $downloads = mcn_get_downloads();

    if (!$downloads) return;

    $current_post_status = get_post_status($current);
    if($current_post_status != 'publish') $current = $downloads[0]->ID;

    mcn_pricing_table_view(
        array(
            'id' => $current,
            'downloads' => $downloads
        )
    );
}

add_shortcode('network_pricing_table', 'mcn_pricing_table_handler');


/**
 * Ajax delete characteristics from admin panel
 */

function mcn_delete_post_from_admin()
{

    if (wp_delete_post($_POST['id'])) {
        echo 1;
    } else {
        echo 0;
    }

    die(); // this is required to terminate immediately and return a proper response
}

add_action('wp_ajax_delete_pricing_characteristic', 'mcn_delete_post_from_admin');


/**
 * Remove admin bar for subscribers
 */
add_action('set_current_user', 'mct_hide_admin_bar');

function mct_hide_admin_bar()
{
    if (!current_user_can('activate_plugins')) {
        show_admin_bar(false);
    }
}

// MCT Auto subscription settings page
add_action('network_admin_menu', 'mct_courses_auto_subscription_menu');

function mct_courses_auto_subscription_menu()
{
    add_submenu_page('settings.php', __('Auto Subscription Configuration'), __('Auto Subscription'), 'auto_subscription', 'auto_subscription', 'mct_auto_subscription_settings_page');
}

function mct_auto_subscription_settings_page()
{
    ?>
    <div class="wrap">
        <h2><?php _e('Auto subscription'); ?></h2>

        <form method="post" action="edit.php?action=mct_save_auto_subscription">
            <?php
            $args = array(
                'post_type' => 'download'
            );
            $downloads_query = new WP_Query($args);
            switch_to_blog(5);
            $args = array(
                'post_type' => 'download'
            );
            $downloads_query_hub = new WP_Query($args);
            restore_current_blog(); ?>

            <?php if ($downloads_query->have_posts()) : ?>
                <?php $values = unserialize(get_site_option('mychinese_auto_subscription')); ?>
                <table>
                    <tr>
                        <th><h3><?php _e('Tutor courses', 'mct_tutor'); ?></h3></th>
                        <th><h3><?php _e('Hub courses (Primitive)', 'mct_tutor'); ?></h3></th>
                    </tr>
                    <?php while ($downloads_query->have_posts()) : $downloads_query->the_post();
                        $tutor_id = get_the_ID(); ?>
                        <tr>
                            <td>
                                <?php the_title(); ?><?php _e(' => '); ?>
                            </td>
                            <?php if ($downloads_query_hub->have_posts()) : ?>
                                <td>
                                    <select name="tutor-course-<?php the_ID(); ?>">
                                        <?php while ($downloads_query_hub->have_posts()) : $downloads_query_hub->the_post(); ?>
                                            <option
                                                <?php
                                                if ($values['tutor-course-' . $tutor_id] == 'hub-course-' . get_the_ID()):
                                                    ?>
                                                    selected="selected"
                                                <?php
                                                endif;
                                                ?>
                                                value="hub-course-<?php the_ID(); ?>">
                                                <?php the_title(); ?>
                                            </option>
                                        <?php endwhile; ?>
                                        <option
                                            <?php
                                            if ($values['tutor-course-' . $tutor_id] == 'hub-course-none'):
                                                ?>
                                                selected="selected"
                                            <?php
                                            endif;
                                            ?>
                                            value="<?php _e('hub-course-none'); ?>"><?php _e('- None -'); ?></option>
                                    </select>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endwhile; ?>
                </table>
            <?php endif; ?>

            <?php submit_button(); ?>
        </form>
    </div>
<?php
}


add_action('network_admin_edit_mct_save_auto_subscription', 'mct_save_auto_subscription');

function mct_save_auto_subscription()
{
    $success = array();
    $status = 'error';
    if (isset($_POST['submit'])) {
        $data = array();
        foreach ($_POST as $key => $value) {
            if ($key == 'submit') {
                continue;
            }
            $data[$key] = $value;
        }
        $values = serialize($data);
        if (update_site_option('mychinese_auto_subscription', $values)) {
            $success[] = 'true';
        } else {
            $success[] = 'false';
        }
    }
    if (!in_array('false', $success)) {
        $status = 'updated';
    }
    wp_redirect(add_query_arg(array('page' => 'auto_subscription', 'mct_status' => $status), network_admin_url('settings.php')));
    exit();
}

add_action('edd_complete_purchase', 'add_to_user_meta', 10, 1);
/*
 * Add info about primitive access on Hub courses to usermeta
 */
function add_to_user_meta($payment_id)
{
    $blog_details = get_blog_details();
    if ($blog_details->blog_id == 1) {
        if (!is_user_logged_in()) {
            return;
        }
        $data = array();
        $values = unserialize(get_site_option('mychinese_auto_subscription'));
        $payment_data = edd_get_payment_meta($payment_id);
        $data['download_id'] = $values['tutor-course-' . $payment_data['downloads'][0]['id']];
        if ($payment_data['downloads'][0]['options']['price_id'] <= 4) {
            $data['price_id'] = 1;
        } else {
            $data['price_id'] = 5;
        }
        $user_id = $payment_data['user_info']['id'];
        $usermeta_to_update = array();
        $usermeta_to_add = array();
        if ($usermeta_to_update = unserialize(get_user_meta($user_id, 'free_access_to_primitive_hub_courses', true))) {
            $usermeta_to_update[] = $data;
            update_user_meta($user_id, 'free_access_to_primitive_hub_courses', serialize($usermeta_to_update));
        } else {
            $usermeta_to_add[] = $data;
            add_user_meta($user_id, 'free_access_to_primitive_hub_courses', serialize($usermeta_to_add));
        }
        $download_id = explode('hub-course-', $data['download_id']);
        update_download_expiration($user_id, $download_id[1], $data['price_id']);
    }
}


add_action('wp', 'mch_user_has_primitive_access');
/**
 * Check to see if a user has access to some content and show it
 */
function mch_user_has_primitive_access()
{
    $blog_details = get_blog_details();
    if ($blog_details->blog_id == 5) {
        global $post;
        $user_id = get_current_user_id();
        $list_allowed_to = maybe_unserialize(get_user_meta($user_id, 'free_access_to_primitive_hub_courses', true));
        $current_time = time();
        if ($list_allowed_to) {
            foreach ($list_allowed_to as $allowed_to) {
                $allowed_download_id = explode('hub-course-', $allowed_to['download_id']);
                $expiration_date = get_expiration_date($user_id, $allowed_download_id[1], $allowed_to['price_id']);
                $all_expiration_dates = get_user_and_download_expiration_dates($user_id, $allowed_download_id[1]);
                $download_id = explode('hub-course-', $allowed_to['download_id']);
                $restricted = edd_cr_is_restricted($post->ID);
                if ($restricted) {
                    if ($download_id[1] == $restricted[0]['download'] && $allowed_to['price_id'] == $restricted[0]['price_id']) {
                        if ($current_time < $expiration_date) {
                            remove_filter('the_content', 'edd_cr_filter_content');
                        }
                    } elseif ($download_id[1] == $restricted[0]['download'] && $restricted[0]['price_id'] === 'all') {
                        if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                            foreach ($all_expiration_dates as $exp_date) {
                                if ($current_time < $exp_date) {
                                    remove_filter('the_content', 'edd_cr_filter_content');
                                    break;
                                }
                            }
                        } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                            if ($current_time < $all_expiration_dates) {
                                remove_filter('the_content', 'edd_cr_filter_content');
                            }
                        }
                    }
                }
            }
        }
    }
}


/**
 * Function to update user subscription expiration time
 * @param $user_id
 * @param $download_id
 * @param $price_id
 * @return boolexpiration
 */
function update_download_expiration($user_id, $download_id, $price_id)
{
    global $wpdb;
    switch_to_blog(5);
    $table_name = $wpdb->prefix . 'download_expiration';
    $prices = get_post_meta($download_id, 'edd_variable_prices', true);
    if (isset($prices[$price_id]['period'])) {
        $period = $prices[$price_id]['period'];
    }
    restore_current_blog();

    switch ($period) {
        case 'day':
            $new_exp_time = time() + 86400;
            break;
        case 'week':
            $new_exp_time = time() + (86400 * 7);
            break;
        case 'month':
            $new_exp_time = time() + (86400 * 30);
            break;
        case 'quart':
            $new_exp_time = time() + (86400 * 30 * 3);
            break;
        case 'year':
            $new_exp_time = time() + (86400 * 365);
            break;
        default:
            $new_exp_time = time();
            break;
    }

    $sql = $wpdb->prepare("SELECT * FROM " . $table_name . " WHERE user_id = %d AND download_id = %d AND price_id = %d", $user_id, $download_id, $price_id);
    $query = $wpdb->query($sql);

    if ($query && !empty($query)) {
        $update_sql = $wpdb->prepare("UPDATE " . $table_name . " SET expired_date = %s WHERE user_id = %d AND download_id = %d AND price_id = %d", $new_exp_time, $user_id, $download_id, $price_id);
        $update_query = $wpdb->query($update_sql);
        if ($update_query) {
            return true;
        } else {
            return false;
        }
    } else {
        $update_sql = $wpdb->prepare("INSERT INTO " . $table_name . " (user_id, download_id, price_id, expired_date) VALUES (%d, %d, %d, %s)", $user_id, $download_id, $price_id, $new_exp_time);
        $update_query = $wpdb->query($update_sql);
        if ($update_query) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * *******************************************************
 */

function mc_close_indexing()
{

    global $post;
    $type = $post->post_type;

    //don't index posts of this post type
    $post_types = array('homepage_slider', 'event', 'course_unit', 'pricing_table_rows', 'download', 'wysiwyg-widget');
    $meta[] = '<meta name="robots" content="noindex,nofollow,noodp,noydir">';

    $meta = implode('', $meta);

    if (in_array($type, $post_types)) {
        echo $meta;
    }

}

add_action('wp_head', 'mc_close_indexing');

function update_payment_global($payment)
{
    global $remember_payment;
    if ($payment && !empty($payment)) {
        $remember_payment = $payment;
    }
}

add_action('edd_remember_payment', 'update_payment_global');

function redirect_with_parameters($redirect, $gateway, $query_string)
{
    global $blog_id, $edd_receipt_args, $edd_options, $remember_payment;
    $user_id = get_current_user_id();
    $hub_cart = get_user_meta($user_id, 'hub_cart');
    $tutor_cart = get_user_meta($user_id, 'tutor_cart');
    $serialize_site_options = get_site_option('mychinese_auto_subscription', true);
    $site_options = maybe_unserialize($serialize_site_options);
    $payment_data = get_post($remember_payment);
    $payment_meta = get_post_meta($remember_payment);
    $edd_payment_meta = maybe_unserialize($payment_meta['_edd_payment_meta'][0]);
    $download_id = $edd_payment_meta['downloads'][0]['id'];
    $period = $edd_payment_meta['downloads'][0]['options']['recurring']['period'];

    if ($blog_id == 5) {
        if (isset($tutor_cart[0]) && !empty($tutor_cart[0])) {
            return $redirect;
        } else {
            if ($download_id && !empty($download_id)) {
                foreach ($site_options as $key => $option) {
                    $tutor_download = preg_replace("/[^0-9]/", '', $key);
                    $hub_download = preg_replace("/[^0-9]/", '', $option);
                    if (isset($hub_download) && !empty($hub_download)) {

                        if ($download_id == $hub_download) {
                            $link = $tutor_download;
                            break;
                        }
                    }
                }
            }
        }

        if ($link && !empty($link)) {
            $redirect = $redirect . '/?download_id=' . $link . '&period=' . $period;
        }
    }
    else if ($blog_id == 1){
        if(isset($hub_cart[0]) && !empty($hub_cart[0])){
            return $redirect;
        }
        else{
            if($download_id && !empty($download_id)){
                foreach ($site_options as $key => $option) {
                    $tutor_download = preg_replace("/[^0-9]/", '', $key);
                    $hub_download = preg_replace("/[^0-9]/", '', $option);
                    if (isset($tutor_download) && !empty($tutor_download)) {
                        if ($download_id == $tutor_download) {
                            if ($hub_download && !empty($hub_download)) {
                                $link = $hub_download;
                            }
                        }
                    }
                }
            }
            if ($link && !empty($link)) {
                $redirect = $redirect . '/?download_id=' . $link . '&period=' . $period;
            }
        }
    }

    return $redirect;
}

add_filter('edd_success_page_redirect', 'redirect_with_parameters', 10, 3);
add_action('wp_head', 'mc_close_indexing');

function after_payment_delete_meta($payment_id){//, $new_status, $old_status){
        $user_id = get_current_user_id();
        $tutor_cart = get_user_meta($user_id, 'tutor_cart', true);
        $hub_cart = get_user_meta($user_id, 'hub_cart', true);

        if (isset($tutor_cart[0]) && !empty($tutor_cart[0])) {
            delete_user_meta($user_id, 'tutor_cart');
            $_SESSION['tutor_purchase'] = true;

        } elseif (isset($hub_cart[0]) && !empty($hub_cart[0])) {

            restore_current_blog();
            delete_user_meta($user_id, 'hub_cart');
            $_SESSION['hub_purchase'] = true;

        }
}

add_action( 'edd_pre_complete_purchase',  'after_payment_delete_meta');

/**
 *
 *
 * *********************************** USER ACCOUNT *********************************************
 *
 *
 */

/**
 * Shortcode request you to the paypal for cancel subscription
 * @param $atts
 * @return string
 */

function mc_cancel_subscription_link($atts){

    $title = $atts['title'] ? $atts['title'] : __('Cancel');

    $cancel_url = 'https://www.paypal.com/cgi-bin/customerprofileweb?cmd=_manage-paylist';
    $cancel_url =  '<a href="' . $cancel_url . '" class="edd-recurring-cancel" target="_blank" title="' . $title .'">' . $title . '</a>';

    return $cancel_url;

}


add_shortcode('mc_cancel_subscription_link', 'mc_cancel_subscription_link');

/**
 * Get expiration date for user, by download id and pricing id
 * @param $user_id
 * @param $download_id
 * @param $price_id
 * @return int
 */
function get_expiration_date($user_id, $download_id, $price_id)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'download_expiration';

    $sql = $wpdb->prepare("SELECT expired_date FROM " . $table_name . " WHERE user_id = %d AND download_id = %d AND price_id = %d AND expired_date != '' ORDER BY id DESC LIMIT 1", $user_id, $download_id, $price_id);
    $query = $wpdb->get_var($sql);

    if (isset($query) && !empty($query)) {
        return $query;
    } else {
        return false;
    }
}
/**
 * The function get current subscription of user (multisite)
 * @param bool $user_id
 * @param bool $blog_id
 * @return array|bool
 */

function mc_get_current_subscription($user_id = false, $blog_id = false){

    global $wpdb;

    $user_id = !$user_id ? get_current_user_id() : $user_id;
    $blog_id = !$blog_id ? get_current_blog_id() : $blog_id;

    if( ! $user_id || ! $blog_id ) return false;

    switch_to_blog( $blog_id );

    $table = $blog_id == 1? $wpdb->prefix . 'users_hours_balance' : $wpdb->prefix . 'download_expiration';
    $title = $blog_id == 1? __('My Chinese Tutor Subscription') : __('My Chinese Hub Subscription');
    $timestamp = strtotime(date('Y-m-d H:i:s'));

    $sql = "SELECT * FROM " . $table . "
                               WHERE user_id = '$user_id'
                               AND expired_date > $timestamp";

    $sql = $wpdb->get_results($sql);

    if( ! $sql ){
        restore_current_blog();
        return false;
    }

	try{

		foreach($sql as $key => $value){

			$value->download = edd_get_download($value->download_id);
			$value->pricing_table_row = edd_get_variable_prices($value->download_id);
			$value->pricing_table_row = get_the_title($value->pricing_table_row[$value->price_id]['pricing_table_row']);
			$value->title = $title;
			$downloads[] = $value;
		}

		restore_current_blog();

	}
	catch(Exception $e){
		$downloads = false;
	}

    return $downloads;

}

/**
 * Subscriptions block
 * @param $data
 */

function mc_user_subscriptions_view($data, $blog_id = false){

    if(!$data) return;

    $blog_id = $blog_id ? $blog_id : get_current_blog_id();

    foreach($data as $key => $value):?>

        <div class="ac-row">
            <span class='user-account-title'><?php echo $value->title;?></span>
	        <div class="right-block">
		        <span class='user-account-data dont-hide'>
                <?php echo $value->download->post_title . ' (' . $value->pricing_table_row . ')';?>
			        <button class='user-show-manage-subscription subscription'><?php _e('Manage');?></button>
            </span>
		        <div class="user-manage-subscription" style="display:none;">
			        <a href="<?php switch_to_blog($blog_id); echo add_query_arg('update_download_id',  $value->download->ID, get_permalink(340)); restore_current_blog();?>" target="_blank"><?php _e('Update');?></a>
			        <?php echo do_shortcode('[mc_cancel_subscription_link title=Cancel]');?>
		        </div>
	        </div>
        </div>

        <?php if($blog_id == 1):?>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Current balance:');?></span>
			    <div class="right-block">
				    <span class='user-account-data dont-hide'><?php echo $value->balance . ($value->balance == 1 ? ' hour' : ' hours');?></span>
			    </div>
            </div>

        <?php endif;?>

        <div class="ac-row">
            <span class='user-account-title'><?php _e('Expiration:');?></span>
	        <div class="right-block">
		        <span class='user-account-data dont-hide'><?php echo date('d/m/Y', $value->expired_date);?></span>
	        </div>
        </div>

    <?php endforeach;

}

/**
 * The view template of User Account page
 * @param $data
 */

function mc_user_account_view($data){
    extract($data);
    ?>

    <div class="account-page">

    <!-- USER AVATAR, NICKNAME -->

    <div class="user-account-info-block avatar-block">
		<div class="ac-row">
		    <div class="user-account-title">
		        <a href='javascript:void(0)' class="wp-user-avatar">
			        <?php echo get_avatar($user->ID);?>
		        </a>
                <?php echo do_shortcode('[avatar_upload user=' . $user->ID . ' profile_edit_frontend=true]'); ?>
	        </div>
			<div class="right-block">
				<form class="form" method="POST" action="">
					<input type="text" name="user_account[nickname]" value="<?php echo $user->nickname;?>">
					<span class="user-name"><?php echo $user->nickname;?></span>
					<input type="hidden" name="change_nickname" value="1"/>
					<input type="button" class="user-account-edit dont-hide" value="<?php _e('Edit');?>"/>
				</form>
			</div>
		</div>
    </div>

    <!-- END OF USER AVATAR, NICKNAME -->

    <!-- USER INFO FORM -->

    <form method="POST" action="">

        <input type="hidden" name="change_user_info" value="1">

        <div class="user-account-info-block">

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Email Address:');?></span>
	            <div class="right-block">
		            <span class='user-account-data dont-hide'><?php echo $user->user_email;?></span>
	            </div>
            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('First Name:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->first_name;?></span>
		            <input type="text" name="user_account[first_name]" value="<?php echo $user->first_name;?>"/>
	            </div>

            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Last Name:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->last_name;?></span>
		            <input type="text" name="user_account[last_name]" value="<?php echo $user->last_name;?>"/>
	            </div>
            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Skype Username:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->skype_name;?></span>
		            <input type="text" name="user_account[skype_name]" value="<?php echo $user->skype_name;?>"/>
	            </div>
            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Polycom Number:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->polycom_name;?></span>
		            <input type="text" name="user_account[polycom_name]" value="<?php echo $user->polycom_name;?>"/>
	            </div>
            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Year / Level:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->year_level;?></span>
		            <input type="text" name="user_account[year_level]" value="<?php echo $user->year_level;?>"/>
	            </div>
            </div>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Institution:');?></span>
	            <div class="right-block">
		            <span class='user-account-data'><?php echo $user->school;?></span>
		            <input type="text" name="user_account[school]" value="<?php echo $user->school;?>"/>
	            </div>
            </div>

            <input type="button" class="user-account-edit dont-hide" value="<?php _e('Edit');?>"/>

        </div>

    </form>

    <!-- END OF USER INFO FORM -->

    <?php

    //Hub Subscriptions
    mc_user_subscriptions_view($hub_downloads, 5);

    //Tutor subscriptions
    mc_user_subscriptions_view($tutor_downloads, 1);

    if(get_current_blog_id() == 1):?>

    <div class="ac-row">
        <div class="account-info-row">
            <div class="bookings_list">
                <h4 class="m-ta-center"><?php _e('You are currently booked for:'); ?></h4>
                    <?php echo do_shortcode('[events_list scope=future bookings=user]'); ?>
            </div>
        </div>
    </div>
    <div class="white-popup-block mfp-hide" id="late-cancel-popup">
        <p><?php _e("You are about to cancel a lesson within 12 hours of the start time. Cancellation within 12 hours of your lesson "
                . "means your lesson will not be re-credited to your balance. Would you like to cancel the lesson?", 'mct'); ?></p>
        <div class="buttons">
            <a href="" type="button" class="e-btn" id="late-lesson-cancel-yes"><?php _e('Cancel this lesson', 'mct'); ?></a>
            <a href="" type="button" class="e-btn" id="late-lesson-cancel-no"><?php _e("Don't cancel this lesson", 'mct'); ?></a>
        </div>
    </div>

    <?php endif;

    //POPUP WITH SEND FEEDBACK WINDOW
    if(get_current_blog_id() == 1)
        mc_send_feedback_window();

    ?>
    </div>
    <?php
}

/**
 * Shortcode handler [user_account]
 */

function mc_user_account_handler(){

    $user = new WP_User(get_current_user_id());

    mc_user_account_view(array(
        'user' => $user,
        'hub_downloads' => mc_get_current_subscription($user->ID, 5),
        'tutor_downloads' => mc_get_current_subscription($user->ID, 1),
    ));

}

add_shortcode('user_account', 'mc_user_account_handler');

/**
 * Feedback Window & Button
 */

function mc_send_feedback_window(){

    $user = wp_get_current_user();

    ?>

    <div class="account-info-row last-row">
        <div class="feedback-wrapper m-ta-center">
            <h4><?php echo __('Help us get better. Provide service feedback!'); ?></h4>
            <a href="#magn-popup" class="e-btn open-popup">Send Feedback</a>


            <form method="post" class="magn-popup white-popup-block mfp-hide" id="magn-popup">
                <h2><?php echo __('PROVIDE SERVICE FEEDBACK'); ?></h2>

                <div class="wrap-block-info">
                    <div class="item-title">
                        <label for="email"><?php echo __('Your email address '); ?><span class="red">*</span></label>
                    </div>
                    <div class="item-input">
                        <input type="text" class="zdesc-email text" id="zdesc_email" name="z_email" value="<?php echo $user->user_email;?>">
                    </div>
                </div>

                <div class="wrap-block-info">
                    <div class="item-title">
                        <label
                            for="dropdown_assessment"><?php echo __('Where 10 is competely satisfied, ') ?> <br /><?php echo __('how satisfied are you with this service?'); ?>
                            <span class="red">*</span></label>
                    </div>
                    <div class="item-input">
                        <select name="dropdown_assessment" id="dropdown_assessment">
                            <?php for ($i = 1; $i <= 10; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php echo selected(5, $i);?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="wrap-block-info">
                    <div class="item-title">
                        <label for="zdesc_comment"><?php echo __('Your message'); ?><span class="red">*</span></label>
                    </div>
                    <div class="item-input">
                        <textarea name="description" id="zdesc_comment"></textarea>
                    </div>
                </div>

                <div class="wrap-submit">
                    <input type="hidden" name="z_is_zendesk_form" value="send_feedback">
                    <input type="submit" value="<?php echo __('Send Feedback'); ?>" class="e-btn" >
                </div>
            </form>

        </div>
    </div>
<?php

}

/**
 * Append CSS AS JS files for user account
 */

function mc_append_user_account_files(){

    wp_enqueue_script('mch-user_account', plugins_url('assets/js/user_account.js', __FILE__), array('jquery'), '1.0', true);
    wp_enqueue_style('mch-user_account-css', plugins_url('assets/css/user_account.css', __FILE__));

}

add_action('wp', 'mc_append_user_account_files');

/**
 * User Account Update Action
 */

function mc_user_account_edit_action(){

    global $post;

    if(($post->ID != 930 && $post->ID != 301) || !$_POST['user_account'] || !get_current_user_id()) return;

    $uid = get_current_user_id();

    if($_POST['change_nickname'] ==1 && !empty($_POST['user_account']['nickname'])){
        update_user_meta($uid, 'nickname', $_POST['user_account']['nickname']);
    }
    else if($_POST['change_user_info'] == 1){
        update_user_meta($uid, 'skype_name', $_POST['user_account']['skype_name']);
        update_user_meta($uid, 'polycom_name', $_POST['user_account']['polycom_name']);
        update_user_meta($uid, 'first_name', $_POST['user_account']['first_name']);
        update_user_meta($uid, 'last_name', $_POST['user_account']['last_name']);
        update_user_meta($uid, 'year_level', $_POST['user_account']['year_level']);
        update_user_meta($uid, 'school', $_POST['user_account']['school']);
    }
    else return;

    //I decided that I wouldn't use Foreach in this part of code

}

add_action('wp', 'mc_user_account_edit_action');

/**
 * FUnction delete booking
 */

function mc_delete_booking(){

    $user = new WP_User(get_current_user_id());

    if (isset($_GET['booking_cancel']) && !empty($_GET['booking_cancel'])) {
        $current_booking = get_booking_by_id((int)$_GET['booking_cancel']);
        if ($current_booking && !empty($current_booking)) {
            $booking_to_cancel = new EM_Booking($_GET['booking_cancel']);
            $event = $booking_to_cancel->get_event();

            do_action('mct_cancel_booking', $event);

            $booking_to_cancel->cancel($user->user_email);
            $booking_to_cancel->delete();
        }
    }

}

add_action('wp', 'mc_delete_booking');
add_shortcode('mc_cancel_subscription_link', 'mc_cancel_subscription_link');

/**
 * Upload avatar  on tutor and hub
 */

function mc_upload_user_avatar()
{
    if(!empty($_FILES['wpua-file'])){
        if(isset($_POST['action']) && $_POST['action'] == 'update' && isset($_POST['user_id']) && !empty($_POST['user_id'])){
            WP_User_Avatar::wpua_action_process_option_update($_POST['user_id']);
        }
        else{
            WP_User_Avatar::wpua_action_process_option_update(get_current_user_id());
        }
    }
}

/**
 * Avatar filter, get avatar from hub, tutor
 * @param $avatar
 * @param $id_or_email
 * @param $size
 * @param $default
 * @param $alt
 * @return null|string|void|WP_Post
 */

function mc_my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {

    if($id_or_email == 'unknown@gravatar.com') return $avatar;

    $user_id = $id_or_email;

    if(!$user_id) return;

    global $wpdb;

    $attach_id = get_user_meta($user_id, $wpdb->prefix . 'user_avatar', true);
    if($attach_id){
        $avatar = get_post($attach_id);
        $avatar = $avatar->guid;
        $avatar = "<img alt='{$alt}' src='$avatar' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
		return $avatar;
    }
    else{
        $avatar = get_avatar('unknown@gravatar.com');
		return $avatar;
    }

	return $avatar;

}

add_filter( 'get_avatar' , 'mc_my_custom_avatar' , 11 , 5 );

function remove_student_role(){
    $role = 'student';
    remove_role( $role );
}

add_action('wp', 'remove_student_role');

function mc_user_logged_on_hide_open(){
    if(is_user_logged_in()):?><div style="display:none;"><?php endif;
}

function mc_user_logged_text($atts, $content = ''){
    if(!is_user_logged_in())
        echo do_shortcode($content);
}

add_shortcode('user_logged_text', 'mc_user_logged_text');


add_filter('wpmu_signup_user_notification_subject', 'custom_sign_up_mail_subject');
/*
 * Change sign up mail subject.
 */
function custom_sign_up_mail_subject(){
    return get_bloginfo('name') . ': ' . __('Activate email address');
}

add_filter('wpmu_signup_user_notification_email', 'custom_sign_up_mail_body');
/*
 * Change sign up mail body.
 */
function custom_sign_up_mail_body(){
    $output = __("To activate your account and ");
    if(get_current_blog_id() === 1){
        $output .= __("book your free Chinese lesson");
    } else{
        $output .= __("to receive your free 24 hour ALL ACCESS PASS");
    }
    $output .= __(" please click on the following link."
            . "\n\n%s\n\n"
            . "Once your account is activated you will receive another email with your login details. Thanks!");
    return $output;
}


/**
 * Get user's sum balance
 * @param $user_id
 * @return mixed
 */
function mct_get_user_balance($user_id){
    if(empty($user_id)){
        return;
    }
    switch_to_blog(1);

    global $wpdb;

    $balance = $wpdb->get_var(
        $wpdb->prepare(
            "SELECT SUM(balance)
             FROM " . $wpdb->prefix . USER_HOURS_TABLE . "
		     WHERE `user_id` = %d AND `expired_date` >= %d
	        ",
            $user_id,
            time()
        )
    );

    restore_current_blog();

    return !empty($balance) ? $balance : 0;
}

function mc_add_settings_field(){
    add_settings_field( 'mc_setting-footer-text', 'Footer copyright text', 'mc_setting_footer_text', 'general');
}

add_action('admin_menu', 'mc_add_settings_field');

function mc_setting_footer_text(){
    ?><input type="text" name="mc_footer_text" value="<?php echo get_option('mc_footer_text');?>"/><?php
}

function mc_save_settings_field(){
    if(!isset($_POST['mc_footer_text'])) return;
    update_option('mc_footer_text', $_POST['mc_footer_text']);
}

add_action('init', 'mc_save_settings_field');

add_action('admin_menu', 'mc_tutors_banner');

function mc_tutors_banner(){
    add_settings_field( 'mc_tutors_banner_text', 'Tutor\'s banner text', 'mc_tutors_banner_text', 'general');
}

function mc_tutors_banner_text(){
    ?><textarea name = "mc_tutor_baner" rows = "3" cols = "30" wrap="hard"
    ><?php echo esc_attr( get_option('mc_tutor_baner') ) ?></textarea><?php
}

add_action('init', 'mc_save_tutor_banner');

function mc_save_tutor_banner(){
    if(!isset($_POST['mc_tutor_baner'])) return;
    update_option('mc_tutor_baner', $_POST['mc_tutor_baner']);
}


function author_add_cap_edit_user(){
    $role = get_role( 'author' );
    $role->add_cap( 'edit_user' );
}

add_action('wp', 'author_add_cap_edit_user');


add_action('admin_init','users_redirect');

function users_redirect(){

    $user_id = get_current_user_id();
    $user = get_userdata($user_id);
    $user_roles = $user->roles;

    if(isset($user_id) && !empty($user_id)){
        if(isset($user) && !empty($user)) {
            if (isset($user_roles) && !empty($user_roles) && is_array($user_roles)) {
                if ((in_array('subscriber', $user_roles) && !defined('DOING_AJAX')) || (in_array('students', $user_roles) && !defined("DOING_AJAX"))) {
                    $site_url = get_bloginfo('home');
                    wp_redirect($site_url);
                    die();
                }
            }
        }
    }

}

function mc_user_subscription_list_wp_profile($data, $blog_id)
{
    if (!$data) return;

    $blog_id = $blog_id ? $blog_id : get_current_blog_id();

    foreach ($data as $key => $value):?>
        <div class="sc-wrapper">
            <div class="ac-row">

                <div class="right-block">
		        <span class='user-account-data dont-hide'>
                <strong><?php echo $value->download->post_title . ' (' . $value->pricing_table_row . ')'; ?></strong>
            </span>
                </div>
            </div>

            <?php if ($blog_id == 1): ?>

                <div class="ac-row">
                    <span class='user-account-title'><?php _e('Current balance:'); ?></span>

                    <div class="right-block">
                    <span
                        class='user-account-data dont-hide'><?php echo $value->balance . ($value->balance == 1 ? ' hour' : ' hours'); ?></span>
                    </div>
                </div>

            <?php endif; ?>

            <div class="ac-row">
                <span class='user-account-title'><?php _e('Expiration:'); ?></span>

                <div class="right-block">
                    <span class='user-account-data dont-hide'><?php echo date('d/m/Y', $value->expired_date); ?></span>
                </div>
            </div>
            <?php if ($blog_id == 1): ?>
                <div class="cl-btn-wrapper">
                    <input type="button"
                           class="cl-btn btn btn-warning"
                           value="<?php echo __('Cancel'); ?>"
                           data-download="<?php echo $value->download_id; ?>"
                           data-price="<?php echo $value->price_id; ?>"
                           data-exp="<?php echo $value->expired_date; ?>"
                           data-user="<?php echo $value->user_id; ?>">
                    <div class="aj-loader" style="display: none; "><img src="<?php echo plugins_url('/assets/images/loader.gif', __FILE__); ?>"></div>
                </div>
            <?php endif; ?>
        </div>

    <?php endforeach;

}

function add_user_subscriptions_to_profile( $user ){
    $tutor_subscription = mc_get_current_subscription($user->ID, 1);
    $hub_subscription = mc_get_current_subscription($user->ID, 5);
    ?>
    <h2><?php echo __('User subscriptions'); ?></h2>
    <div class="subscr-list-wrapper">
        <div class="tutor-subscriptions">
            <h3><?php echo __('MyChineseTutor Subscriptions:'); ?></h3>
            <div class="subscriptions-list"><?php mc_user_subscription_list_wp_profile($tutor_subscription, 1); ?></div>
        </div>

        <div class="hub-subscriptions">
            <h3><?php echo __('MyChineseHub Subscriptions:'); ?></h3>
            <div class="subscriptions-list"><?php mc_user_subscription_list_wp_profile($hub_subscription, 5); ?></div>
        </div>
    </div>
<?php
}

add_action( 'show_user_profile', 'add_user_subscriptions_to_profile' );
add_action( 'edit_user_profile', 'add_user_subscriptions_to_profile' );

function admin_add_styles(){
    wp_enqueue_style( 'user_edit_style', plugins_url('/assets/css/user_edit.css', __FILE__) );
}

add_action('admin_enqueue_scripts', 'admin_add_styles');

add_filter( 'newuser_notify_siteadmin', 'change_mail_about_new_user', 10, 2);

/*
 * Add user nickname to notify admin mail when new user registered.
 */
function change_mail_about_new_user($msg, $user ){
    global $wpdb;
    switch_to_blog(1);
    $prefix = $wpdb->prefix;
    restore_current_blog();
    $sql = $wpdb->prepare("SELECT `meta` FROM `" . $prefix . "signups` WHERE `user_email`=%s", $user->data->user_email);
    $query = $wpdb->get_var($sql);
    $meta = maybe_unserialize($query);
    return __('New User name: ') . $meta['user_name'] . "\n" . $msg;
}

/* GOOGLE ANALYTICS GOALS */

add_action('signup_finished', 'registration_google_goals');
add_action('activate_header', 'activation_google_goals');
add_action('preprocess_signup_form', 'registration_page_google_goals');
add_action('first_purchase_page', 'purchase_google_goals', 1, 4);
add_action('common_thank_you_page', 'purchase_google_goals', 1, 4);


/**
 * Goal Registration on the site
 */
function registration_google_goals(){
    echo '<script type="text/javascript" > if (typeof (_gaq) !== "undefined") { _gaq.push(["_trackEvent", "Registration", "Registration on the site", "Registration on the site"]);}</script>';
}

/**
 * Goal Confirm Registration on the site
 */
function activation_google_goals(){
    echo '<script type="text/javascript" > if (typeof (_gaq) !== "undefined") { _gaq.push(["_trackEvent", "Registration", "Confirm registration", "Confirm registration"]);}</script>';
}

/**
 * Goal Go to Registration page on the site
 */
function registration_page_google_goals(){
    echo '<script type="text/javascript" > if (typeof (_gaq) !== "undefined") { _gaq.push(["_trackEvent", "Registration", "Registration page", "Registration page"]);}</script>';
}

/**
 * Goal Subscribing to a package
 */
function purchase_google_goals($order_id, $store_name, $order_total, $order_items){
?>
    <script type="text/javascript">
        if (typeof (_gaq) !== "undefined") {
            _gaq.push(["_trackEvent", "Subscribing to a package", "Subscribing to a package", "Subscribing to a package"]);
            _gaq.push(['_addTrans',
                '<?php echo $order_id;?>',            // transaction ID - required
                '<?php echo $store_name;?>',          // affiliation or store name
                '<?php echo $order_total;?>',         // total - required
           ]);
           <?php foreach ($order_items as $item):?>
            _gaq.push(['_addItem',
                '<?php echo $order_id;?>',            // transaction ID - required
                '<?php echo $item['sku'];?>',           // SKU/code - required
                '<?php echo $item['name'];?>',          // product name
                '<?php echo $item['variation'];?>',     // category or variation
                '<?php echo $item['price'];?>',         // unit price - required
                '1'                                 // quantity - required
            ]);
            <?php endforeach;?>
            _gaq.push(['_trackTrans']);
        }
    </script>
<?php
}

function mct_ajax_cancel_subscription(){
    global $wpdb;
    if(isset($_POST['download_id']) && !empty($_POST['download_id'])){
        $download_id = $_POST['download_id'];
    }
    if(isset($_POST['price_id']) && !empty($_POST['price_id'])){
        $price_id = $_POST['price_id'];
    }
    if(isset($_POST['expired_date']) && !empty($_POST['expired_date'])){
        $expired_date = $_POST['expired_date'];
    }
    if(isset($_POST['user_id']) && !empty($_POST['user_id'])){
        $user_id = $_POST['user_id'];
    }


    switch_to_blog(1);

    $table_name = $wpdb->prefix . 'users_hours_balance';

    $hours_sql = $wpdb->prepare("SELECT balance FROM $table_name WHERE user_id = %d AND download_id = %d AND price_id = %d AND expired_date = %d", $user_id, $download_id, $price_id, $expired_date);
    $hours = $wpdb->get_var($hours_sql);

    $update = $wpdb->update(
        $table_name,
        array(
            'expired_date' => 0
        ),
        array(
            'user_id' => $user_id,
            'download_id' => $download_id,
            'price_id' => $price_id,
            'expired_date' => $expired_date
        )
    );

    if($update && !empty($update)){
        do_action('mct_user_unsubscribe', $user_id);

        $CTRL_Balance_log = new CTRL_Balance_logs();
        $log_id = $CTRL_Balance_log->set((object)array(
            'user_id'          =>  $user_id,
            'date'             =>  current_time('mysql'),
            'action'           =>  __('AdminCancelSubscr'),
            'change_balance'   =>  $hours,
            'balance'          =>  get_user_balance($user_id),
            'delivery_method'  =>  NULL,
        ));

        echo $update;
    }

    restore_current_blog();
    die;
}

add_action('wp_ajax_cancel_tutor_subscription', 'mct_ajax_cancel_subscription');

function update_users_expired_date(){
    global $wpdb;
    $args = array(
        'fields'       => 'all',
    );

    $Users = new WP_User_Query($args);

    if($Users->results && !empty($Users->results)){
        foreach($Users->results as $user){
            $user_id = $user->ID;
            $table_name = 'ch_users_hours_balance';
            $expider_dates_sql = $wpdb->prepare("SELECT id, expired_date FROM $table_name WHERE user_id = %d", $user_id);
            $expired_dates_query = $wpdb->get_results($expider_dates_sql);

            foreach($expired_dates_query as $exp_date_obj){
                $record_id = $exp_date_obj->id;
                $current_date_unix = $exp_date_obj->expired_date;
                $current_date = date('d.m.Y', $current_date_unix);
                $new_time = strtotime($current_date . ' 00:00:00');
                $wpdb->update(
                    $table_name,
                    array(
                        'expired_date' => $new_time
                    ),
                    array(
                        'id' => $record_id
                    )
                );
            }

        }
    }
}

//update_users_expired_date();

function mct_get_current_user_role()
{
    global $wp_roles;

    $current_user = wp_get_current_user();

    $roles = $current_user->roles;

    $role = array_shift($roles);

    return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role]) : false;
}

/**
 * Add currency to the price.
 */
function edd_usd_currency_filter( $formatted ) {
	$currency = edd_get_currency();
	$result =  $currency . ' ' . $formatted;
	return $result;
}
add_filter( 'edd_usd_currency_filter_before', 'edd_usd_currency_filter' );