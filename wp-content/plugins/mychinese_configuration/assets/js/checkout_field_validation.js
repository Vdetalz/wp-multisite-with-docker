jQuery(document).ready(function ($) {
    $('form.edd_form').submit(function() {
        var email_input = $('input[type="email"]');
        var name_input = $('input[name="edd_first"]');
        var acception = $('input[type="checkbox"]');

        email_input.removeClass('false-input');
        name_input.removeClass('false-input');

        if (email_input) {
            if (email_input.length > 0) {
                var email_value = email_input.val();
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if (email_value != '') {
                    if (pattern.test(email_value)) {
                        var name = name_input.val();
                        if(name.length > 2){
                            if(!acception.prop("checked") == true){
                                return false;
                            }
                        }
                        else{
                            name_input.addClass('false-input');
                            return false;
                        }
                    }
                    else{
                        email_input.addClass('false-input');
                        return false;
                    }
                }
                else{
                    email_input.addClass('false-input');
                    return false;
                }
            }
            else{
                return false
            }
        }
        else{
            return false;
        }

        //validation if user not logged in

        var login_name_input = $('input[name="edd_user_login"]');
        var pass_input = $('input[name="edd_user_pass"]');
        var pass_confirm_input = $('input[name="edd_user_pass_confirm"]');

        login_name_input.removeClass('false-input');
        pass_input.removeClass('false-input');
        pass_confirm_input.removeClass('false-input');
        $('.password-attention').remove();

        if(login_name_input){
            if(login_name_input.val().length < 3){
                login_name_input.addClass('false-input');
                return false;
            }
        }

        if(pass_input){
            if(pass_input.val().length < 6){
                pass_input.addClass('false-input');
                return false;
            }
            else{
                if(pass_confirm_input){
                    if(pass_confirm_input.val() != pass_input.val()){
                        pass_confirm_input.addClass('false-input');
                        pass_confirm_input.after('<p class="password-attention">Password and confirm password fields must be the same</p>');
                        return false;
                    }
                }
            }
        }

    });
});