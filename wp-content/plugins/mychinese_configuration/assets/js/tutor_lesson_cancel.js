jQuery(document).ready(function($){
    $('.cl-btn').on('click', function(){
        var download_id = $(this).data('download');
        var price_id = $(this).data('price');
        var expired_date = $(this).data('exp');
        var user_id = $(this).data('user');

        var clicked_element = $(this);

        $.ajax({
            url: ajaxurl,
            method: 'post',
            data: {
                action: 'cancel_tutor_subscription',
                download_id : download_id,
                price_id : price_id,
                expired_date: expired_date,
                user_id: user_id
            },
            beforeSend: function(){
                clicked_element.hide();
                clicked_element.parents().eq(1).find('.aj-loader').show();
            },
            success: function(result){
                if(result.length > 0){
                    clicked_element.parents().eq(1).remove();
                }
            }
        });
    })
});