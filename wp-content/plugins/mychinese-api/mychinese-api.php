<?php
/*
Plugin Name: Mychinese API
Description:  Mychinese API plugin
Version: 1.0
Author: Web4Pro
Author URI:
*/

define( 'MYCHINESE_PLUGIN_URL', plugin_dir_path( __FILE__ ) );
define( 'HUB_BLOG_ID', 5 );

add_action( 'plugins_loaded', 'mychinese_plugin_override' );

function mychinese_plugin_override() {
    include_once( MYCHINESE_PLUGIN_URL . 'class.mychinese.api.php' );
    include_once( MYCHINESE_PLUGIN_URL . 'mychinese-api.inc.php' );
}

/**
 * Initialize routes for API
 */
function mc_register_route(){
  $controller = new MyChineseApi;
  $controller->register_routes();
}

add_action( 'rest_api_init', 'mc_register_route', 0 );

/**
 * On plugin activation - creates new tables for API
 */
function mychinese_api_activate() {
    global $wpdb;

    $table_name = $wpdb->prefix . "devices";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  classroom_teacher_id int(11) NOT NULL,
				  platform VARCHAR(255) NOT NULL,
				  token VARCHAR(255) NOT NULL,
				  UNIQUE KEY id (id)
				) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
register_activation_hook(__FILE__, 'mychinese_api_activate');

function mychinese_api_error_logs() {
  global $wpdb;

  $api_logs_table = $wpdb->prefix . 'api_logs';
  if($wpdb->get_var("show tables like '$api_logs_table'") != $api_logs_table) {

    $sql = "CREATE TABLE IF NOT EXISTS " . $api_logs_table . " (
                  id bigint(11) NOT NULL AUTO_INCREMENT,
                  uid bigint(11) DEFAULT '0' NOT NULL,
                  date datetime NOT NULL,
        				  action VARCHAR(255) NULL,
        				  error VARCHAR(255) NULL,
				  UNIQUE KEY id (id)
        );";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  }
}
register_activation_hook(__FILE__, 'mychinese_api_error_logs');

/**
 * @param $device_info
 *
 * @return array
 * Function sand push notification by device token.
 */
function mc_push_notification($device_info){

	if(is_array($device_info) && empty($device_info[0]->platform) || empty($device_info[0]->token)){
		return;
	}

	$msg_payload = array (
		'mtitle' => __('Hi!'),
		'mdesc' => __('How was your most recent lesson?'),
	);

	$push_res = array();

	foreach($device_info as $device){
		if($device->platform === 'android'){
			$push_res[] = PushNotifications::android($msg_payload, $device->token);
		}
		elseif($device->platform === 'ios'){
			$push_res[] = PushNotifications::iOS($msg_payload, $device->token);
		}
	}

	return $push_res;
}


add_action('admin_menu', 'wpautop_control_menu');

function wpautop_control_menu() {
    add_management_page( "MCT Feedback Export", "MCT Feedback Export", "admin user", "mct-feedback-export.php", 'mct_feedback_export_callback' );
}

function mct_feedback_export_callback(){
?>
    <h1><?php _e('MCT Feedback Logs Export'); ?></h1>
    <form id="export-logs" method="post">
        <input id="submit" class="button button-primary" type="submit" value="Download Export File" name="submit">
        <?php wp_nonce_field( 'download', '_wpnonce' ) ?>
    </form>
<?php
}

add_action('init', 'save_file');

function save_file(){
    if(isset( $_POST['_wpnonce']) && wp_verify_nonce( $_POST['_wpnonce'], 'download' )){
        $plugin_url = plugin_dir_path(__FILE__);
        $filename = "logs.csv";
        $file = $plugin_url . 'logs/' . $filename;

        if(file_exists($file)) {
            $type = filetype($file);
            header("Content-type: $type");

            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
            header('Pragma: no-cache');
            header('Expires: 0');
            readfile($file);
            exit;
        }
    }
}