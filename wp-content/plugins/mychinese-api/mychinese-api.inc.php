<?php

/**
 * add function for rest-api plugin.
 */
if (!function_exists('wp_json_encode')) {
    function wp_json_encode($data, $options = 0, $depth = 512) {
        /*
         * json_encode() has had extra params added over the years.
         * $options was added in 5.3, and $depth in 5.5.
         * We need to make sure we call it with the correct arguments.
         */
        if (version_compare(PHP_VERSION, '5.5', '>=')) {
            $args = array($data, $options, $depth);
        }
        elseif (version_compare(PHP_VERSION, '5.3', '>=')) {
            $args = array($data, $options);
        }
        else {
            $args = array($data);
        }

        $json = call_user_func_array('json_encode', $args);

        // If json_encode() was successful, no need to do more sanity checking.
        // ... unless we're in an old version of PHP, and json_encode() returned
        // a string containing 'null'. Then we need to do more sanity checking.
        if (FALSE !== $json && (version_compare(PHP_VERSION, '5.5', '>=') || FALSE === strpos($json, 'null'))) {
            return $json;
        }

        try {
            $args[0] = _wp_json_sanity_check($data, $depth);
        }
        catch (Exception $e) {
            return FALSE;
        }

        return call_user_func_array('json_encode', $args);
    }
}


/**
 * Should be delete after fix
 */
if (!function_exists('_wp_json_sanity_check')) {
    function _wp_json_sanity_check( $data, $depth ) {
        if ( $depth < 0 ) {
            throw new Exception( 'Reached depth limit' );
        }

        if ( is_array( $data ) ) {
            $output = array();
            foreach ( $data as $id => $el ) {
                // Don't forget to sanitize the ID!
                if ( is_string( $id ) ) {
                    $clean_id = _wp_json_convert_string( $id );
                } else {
                    $clean_id = $id;
                }

                // Check the element type, so that we're only recursing if we really have to.
                if ( is_array( $el ) || is_object( $el ) ) {
                    $output[ $clean_id ] = _wp_json_sanity_check( $el, $depth - 1 );
                } elseif ( is_string( $el ) ) {
                    $output[ $clean_id ] = _wp_json_convert_string( $el );
                } else {
                    $output[ $clean_id ] = $el;
                }
            }
        } elseif ( is_object( $data ) ) {
            $output = new stdClass;
            foreach ( $data as $id => $el ) {
                if ( is_string( $id ) ) {
                    $clean_id = _wp_json_convert_string( $id );
                } else {
                    $clean_id = $id;
                }

                if ( is_array( $el ) || is_object( $el ) ) {
                    $output->$clean_id = _wp_json_sanity_check( $el, $depth - 1 );
                } elseif ( is_string( $el ) ) {
                    $output->$clean_id = _wp_json_convert_string( $el );
                } else {
                    $output->$clean_id = $el;
                }
            }
        } elseif ( is_string( $data ) ) {
            return _wp_json_convert_string( $data );
        } else {
            return $data;
        }

        return $output;
    }
}

if (!function_exists('_wp_json_convert_string')) {
    function _wp_json_convert_string( $string ) {
        static $use_mb = null;
        if ( is_null( $use_mb ) ) {
            $use_mb = function_exists( 'mb_convert_encoding' );
        }

        if ( $use_mb ) {
            $encoding = mb_detect_encoding( $string, mb_detect_order(), true );
            if ( $encoding ) {
                return mb_convert_encoding( $string, 'UTF-8', $encoding );
            } else {
                return mb_convert_encoding( $string, 'UTF-8', 'UTF-8' );
            }
        } else {
            return wp_check_invalid_utf8( $string, true );
        }
    }
}
/**
 * Insert information about teachers devices
 * @param array $arg in this format:
 *
 *   array(
 *       'classroom_teacher_id' => $classroom_teacher_id,
 *       'platform' => $platform,
 *       'token' => $token,
 *   );
 *
 * @return int $last_id
 */
function insert_device_info(array $arg) {
    global $wpdb;
    global $blog_id;

    if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
    }

    $table_name = $wpdb->prefix . "devices";


    // check if classroom_teacher_id with token exist
    $result = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE classroom_teacher_id = %d AND token = %s", $arg['classroom_teacher_id'], $arg['token']));

    if(is_null($result)) {
        $wpdb->insert(
            $table_name,
            array(
                'classroom_teacher_id' => $arg['classroom_teacher_id'],
                'platform' => $arg['platform'],
                'token' => $arg['token'],
            ),
            array('%d', '%s', '%s')
        );
        $last_id = $wpdb->insert_id;
        restore_current_blog();
        if(!$last_id){
            set_log($arg['classroom_teacher_id'], 'save device','Device info, not saved');
        }

        return $last_id;
    }

    return;
}

/**
 * Delete info about device
 *
 * @param $token
 * @param $platform
 *
 * @return int|false $result
 */
function delete_device_info($uid, $token, $platform) {
    global $wpdb;
    global $blog_id;

    if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
    }

    $table_name = $wpdb->prefix . "devices";

    $delete = $wpdb->delete(
        $table_name,
        array('token' => $token, 'platform' => $platform),
        array('%s', '%s')
    );

    restore_current_blog();

    if(!$delete){
        set_log($uid, 'Delete device', 'Device info not deleted.');
    }

    return $delete;
}

/**
 * Rate survey
 *
 * @param int $survey_id
 * @param float $star_rating
 * @param string $comment
 */
function update_survey($survey_id, $star_rating, $comment, $user_id) {
    global $wpdb;
    global $blog_id;

    if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
    }

    $table_name = $wpdb->prefix . "lesson_survey";
    $result = $wpdb->update(
        $table_name,
        array(
            'star_rating' => $star_rating,
            'comment' => $comment,
            'is_rated' => 1
        ),
        array('survey_id' => $survey_id),
        array('%f', '%s', '%d'),
        array('%d')
    );
    restore_current_blog();

    if(!$result){
        set_log($user_id, 'Update survey', 'Survey info not updated.');
    }

    return $result;
}

/**
 * Send emails if survey rating less than 2
 *
 * @param string|array $emails Array or comma-separated list of email addresses to send message.
 * @param array $arg
 *
 */
function send_emails($emails, array $arg) {
    global $blog_id;

    if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
    }

    $user_nicename = $arg['user']->data->user_nicename;
    $user_email = $arg['user']->data->user_email;
    $user_first_name = get_user_meta($arg['user']->data->ID, 'first_name', true);
    $user_last_name = get_user_meta($arg['user']->data->ID, 'last_name', true);

    $user_full_name = $user_first_name . ' ' . $user_last_name;

    $school_name = get_user_meta($arg['user']->ID, 'school', true);
    $teacher_first_name = get_user_meta($arg['teacher_id'], 'first_name', true);
    $teacher_last_name = get_user_meta($arg['teacher_id'], 'last_name', true);
    $teacher_full_name = $teacher_first_name . ' ' . $teacher_last_name;
    $lesson_date = date('d/m/y H:i', $arg['survey_date']);

    $full_lesson_details = "Pathway #{$arg['pathway']} | Module #{$arg['module']} | Lesson #{$arg['lesson']} | Date $lesson_date";

    // Create message Subject and Body
    $subject = __('Low ranking lesson', 'mct_hub');
    $body = __('Classroom teacher name: ', 'mct_hub') . $user_full_name . "\n";
    $body .= __('Classroom teacher email: ', 'mct_hub') . $user_email . "\n";
    $body .= __('School name: ', 'mct_hub') . $school_name . "\n";
    $body .= __('Chinese teacher name: ', 'mct_hub') . $teacher_full_name . "\n";
    $body .= __('Lessons details: ', 'mct_hub') . $full_lesson_details . "\n";
    $body .= __('Star rating: ', 'mct_hub') . $arg['rating'] . "\n";
    $body .= __('Low star reason: ', 'mct_hub') . $arg['comment'] . "\n";

	// Low ranking action.
	do_action('mch_low_ranking_app', $arg['user']->data->ID, array(
	  'description' => $subject . ' | ' . $user_full_name,
	  'detail' => $body,
	  'category' => __('classroom teacher issue'),
	  'dueDate' => date('Y-m-d'),
	));
    // send emails
    $email = wp_mail($emails, $subject, $body);

    if(!$email){
        set_log($arg['user']->ID, 'Send email', 'Send email error.');
    }
}

/**
 * @param $uid
 * @param $action
 * @param $error
 * @return mixed
 * Function save logs to table, if action return error
 */

function set_log($uid, $action, $error){
    global $wpdb;

    $api_logs_table = $wpdb->prefix . 'api_logs';

    $result = $wpdb->insert(
        $api_logs_table,
        array(
            'uid'    => $uid,
            'date'   => gmdate("Y-m-d\TH:i:s\Z", time()),
            'action' => $action,
            'error' => $error
        ),
        array('%d', '%s', '%s', '%s')
    );

    return $result;
}

/**
 * Function save information about login/logout actions.
 *
 * @param $action
 */

function session_logs($user_id, $action){
    $plugin_path = plugin_dir_path ( __FILE__ );
    $logs_dir = $plugin_path . 'logs';
    $headers = "Date,User Id,Email,Action\n";

    if (!file_exists($logs_dir)) {
        mkdir($logs_dir, 0755, true);
    }

    $log_filename = $logs_dir . '/logs.csv';
    if (!file_exists($log_filename)) {
        file_put_contents($log_filename, $headers, FILE_APPEND | LOCK_EX);
    }

    $user = get_user_by('id', $user_id);

    if(empty($user)){
        return;
    }

    $email = $user->data->user_email;
    $date = date("Y-m-d H:i:s");

    $session_info = sprintf("%s, %d, %s, %s\n", $date, $user_id, $email, $action);

    $file = file_put_contents($log_filename, $session_info, FILE_APPEND | LOCK_EX);

    return $session_info;
}

