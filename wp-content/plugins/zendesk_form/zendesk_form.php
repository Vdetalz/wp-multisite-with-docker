<?php
/*
Plugin Name: Zendesk forms
Version: 2.0
Author: Web4Pro
Description: Zendesk integration
*/

/**
 * Zendesk Menu
 */

function zendesk_menu_page(){

    add_menu_page(
        __( 'Zendesk Integration' ),
        __( 'Zendesk' ),
        10,
        'zendesk_options',
        'zendesk_page',
        plugin_dir_url( __FILE__ ) . '/z-ico.png',
        5000
    );

}

/**
 * Define const values
 */

function zendesk_set_const(){

    $zdAPIkey = get_option('zdapikey');
    $zdUser = get_option('zduser');
    $zdURL = get_option('zdurl');

    return array('APIKEY'=> $zdAPIkey, 'USER' => $zdUser, 'URL' => $zdURL);

}

/**
 * Zendesk Submit settings Form
 */

function zendesk_page(){

    zendesk_submit_form();

    $p = zendesk_set_const();

    ?>

    <div class="wrap">

        <h2><?php _e('Zendesk Integration');?></h2>

        <form action="" method="POST" style="background:#fff; border:1px solid #d1d1d1; padding:10px 20px; display:inline-block;">

            <?php wp_nonce_field( 'zendesk_nonce_field', '_zendesk_nonce_field_951' ); ?>

            <p>
                <label for=""><?php _e( 'Zendesk API Key:' );?></label></br>
                <input type="text" style="height:35px; width:300px;" name="zdapikey" value="<?php echo $p['APIKEY'];?>"/>
            </p>

            <p>
                <label for=""><?php _e( 'Zendesk User Email:' );?></label></br>
                <input type="text" style="height:35px; width:300px;" name="zduser" value="<?php echo $p['USER'];?>"/>
            </p>

            <p>
                <label for=""><?php _e( 'Zendesk URL API:' );?></label></br>
                <input type="text" style="height:35px; width:300px;" name="zdurl" value="<?php echo $p['URL'];?>"/>
            </p>

            <p>
                <input type="submit" class="button button-primary" value="<?php _e( 'Update' );?>"/>
            </p>

        </form>

    </div>

    <?php

}

/**
 * Handler of Zendesk settings form
 */

function zendesk_submit_form(){

    // Check if our nonce is set.
    if ( ! isset( $_POST['_zendesk_nonce_field_951'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['_zendesk_nonce_field_951'], 'zendesk_nonce_field' ) ) {
        return;
    }

    if(!empty($_POST['zdapikey'])){

        if(!get_option('zdapikey'))
            add_option('zdapikey', $_POST['zdapikey']);
        else
            update_option('zdapikey', $_POST['zdapikey']);

    }

    if(!empty($_POST['zduser'])){

        if(!get_option('zduser'))
            add_option('zduser', $_POST['zduser']);
        else
            update_option('zduser', $_POST['zduser']);

    }

    if(!empty($_POST['zdurl'])){

        if(!get_option('zdurl'))
            add_option('zdurl', $_POST['zdurl']);
        else
            update_option('zdurl', $_POST['zdurl']);

    }


}

add_action( 'admin_menu', 'zendesk_menu_page' );

/**
 * @param $url
 * @param $json
 * @param $action
 * @return array|mixed
 */

function curlWrap($url, $json, $action)
{

    $p = zendesk_set_const();

    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
    curl_setopt($ch, CURLOPT_URL, $p['URL'].$url);
    curl_setopt($ch, CURLOPT_USERPWD, $p['USER']."/token:".$p['APIKEY']);
    switch($action){
        case "POST":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            break;
        case "GET":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            break;
        case "DELETE":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            break;
        default:
            break;
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    curl_close($ch);

    $decoded = json_decode($output);

    return $decoded;
}

function zendesk_send_form(){

    $email = trim($_POST['z_email']);
    $is_zendesk_form = $_POST['z_is_zendesk_form'];
    $description = '';

    if(!empty($email)){

        if($is_zendesk_form == 'contact_us'){

            $name = $_POST['first_name']. " " . $_POST['last_name'];

            if(!empty($_POST['school']))
            $description .= 'School/ Institution : ' . $_POST['school'] . '\n';

            if(!empty($_POST['town']))
                $description .= 'Town : ' . $_POST['town'] . '\n';

            if(!empty($_POST['phone_number']))
                $description .= 'Phone Number : ' . $_POST['phone_number'] . '\n';

            $description.= '\n'.$_POST['text_message'];
            
            $from = 'MyChineseTeacher - Contact Me | ' . $name;

        } else if($is_zendesk_form == 'send_feedback'){


            $name = $email;

            $from = 'MyChineseTutor | ' . $email;

            $description = $_POST['description'];

            if(!empty($_POST['dropdown_assessment']))
                $description .= '\nYour mark is : ' . $_POST['dropdown_assessment'];

        }
        else return false;

        $create = json_encode(array(
                'ticket' => array(
                    'subject' => $from,
                    'description' => $description,
                    'requester' => array(
                        'name' => $name,
                        'email' => $email
                    )
                )
            ),
            JSON_FORCE_OBJECT
        );

        $data = curlWrap("/tickets.json", $create, "POST");

    }
    else return false;

}

add_action('init', 'zendesk_send_form');