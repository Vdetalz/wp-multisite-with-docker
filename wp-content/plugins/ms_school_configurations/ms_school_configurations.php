<?php
/*
 * plugin name: my sister school configurations
 * author: web4pro
 * description: the plugin has specific configurations for mysisterschool site.
 * version: 1.0
 */

/**
 * Add specific 'School' field for MSS site.
 */
defined('MSS_SCHOOL_FIELD') || define('MSS_SCHOOL_FIELD', 'mss_school');

/**
 * add new user role and pages on plugin activation
 */
function mss_add_settings_on_plugin_activation() {
	add_role( 'ss_users', 'ss user', array(
		'read'    => TRUE,
		'level_0' => TRUE
	) );

	$post_id = wp_insert_post( array(
		'post_type'    => 'page',
		'post_status'  => 'publish',
		'post_title'   => __( 'school page' ),
		'post_content' => '[mss_assigned_cluster_schools]',
	) );

	add_option( 'school_list_page', $post_id ); //school list page id

	$post_id = wp_insert_post( array(
		'post_type'    => 'page',
		'post_status'  => 'publish',
		'post_title'   => __( 'gallery' ),
		'post_content' => '[show_photos]',
	) );

	add_option( 'photo_page', $post_id );

	$post_id = wp_insert_post( array(
		'post_type'    => 'page',
		'post_status'  => 'publish',
		'post_title'   => __( 'create post' ),
		'post_content' => '[create_post_form]',
	) );

	add_option( 'create_post_page', $post_id );

}

register_activation_hook( __FILE__, 'mss_add_settings_on_plugin_activation' );


/**
 * shortcode for user home page (assigned schools from cluster)
 *
 * @param $atts
 *
 * @return bool|string
 */
function mss_assigned_cluster_schools( $atts ) {

	//merge default attr array with inputted array
	$atts = shortcode_atts( array(
		'user_id'          => get_current_user_id(),
		'pagination'       => TRUE,
		'schools_per_page' => 9,
		'access_message'   => __( 'you have no access on this page' )
	), $atts );

	//get assigned school id
	if ( $school_id = mss_get_user_school( $atts['user_id'], TRUE ) ) {

		//get cluster of this school
		$term = wp_get_post_terms( $school_id, 'cluster' );

		//if cluster exists
		if ( isset( $term[0] ) && ! empty( $term[0] ) ) {

			//select schools by cluster
			$wp_query = new wp_query();

			$query_string = "showposts=" . $atts['schools_per_page'] . "&post_type=school
                    &post_status=publish&cluster=" . $term[0]->slug;

			//if pagination is enabled
			if ( $atts['pagination'] ) {
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$query_string .= "&paged=" . $paged;
			}

			$wp_query->query( $query_string );

			if ( $wp_query->have_posts() ) {
				ob_start(); ?>
				<section class="mss-gallery">
					<?php

					while ( $wp_query->have_posts() ) {
						$wp_query->the_post();

						//load template for single school
						load_template( dirname( __FILE__ ) . '/templates/school-list-single.php', FALSE );
					}
					?>
				</section>
				<?php
				$blog_term = get_term_by( 'slug', 'blog', 'category' );

				echo "<div class=\"mss-gallery-links\">";
				echo mss_get_the_blog_link( $blog_term, TRUE );
				echo mss_get_the_gallery_link( TRUE );
				echo "</div>";
				echo "<div class='mss_pagination'>";
				echo mss_pagination( $wp_query ); //show pagination
				echo "</div>";

				return ob_get_clean();
			}
		}
	} else {
		echo "<p>" . $atts['access_message'] . "</p>"; //show message if user have no access on this page
	}
}

add_shortcode( 'mss_assigned_cluster_schools', 'mss_assigned_cluster_schools' );

/**
 * return school id by user id
 *
 * @param bool $user_id
 * @param bool $return_id
 *
 * @return array|bool|null|wp_post
 */
function mss_get_user_school( $user_id = false, $return_id = false ) {
    //get user id
    $user_id = !$user_id ? get_current_user_id() : $user_id;
    //get school id from user
    $school_id = get_user_meta( $user_id, MSS_SCHOOL_FIELD, true );

    if ( $return_id ) {
        return $school_id;
    }

    //if post exists
    if ( $school_id && get_post_type( $school_id ) === 'school' ) {
        return get_post( $school_id );
    } else {
        return false;
    }
}

/**
 * return user cluster
 *
 * @param $user_id
 * @param bool|false $return_id
 *
 * @return bool
 */
function mss_get_user_cluster( $user_id = FALSE, $return_id = FALSE ) {
	//get user id
	$user_id = ! $user_id ? get_current_user_id() : $user_id;

	//get user school id
	$school_id = mss_get_user_school( $user_id, TRUE );

	if ( $school_id ) {
		$term = wp_get_post_terms( $school_id, 'cluster' );
		if ( isset( $term[0] ) && ! empty( $term[0] ) ) {
			if ( $return_id ) {
				return $term[0]->term_id;
			} else {
				return $term[0];
			}
		}
	}

	return 'all';
}

/**
 * return pagination
 *
 * @param $wp_query
 *
 * @return array|string|void
 */
function mss_pagination( $wp_query ) {
	$big = 999999999;

	return paginate_links( array(
		'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
		'format'    => '?paged=%#%',
		'current'   => max( 1, get_query_var( 'paged' ) ),
		'total'     => $wp_query->max_num_pages,
		'prev_text' => __( 'prev' ),
		'next_text' => __( 'next' ),
	) );
}

/**
 * redirect on schools page after login
 *
 * @param $redirect_to
 * @param $request
 * @param $user
 *
 * @return false|string
 */
function mss_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'ss_users', $user->roles ) && $school_list_page_id = get_option( 'school_list_page' ) ) {
			return get_permalink( $school_list_page_id );
		}
	}

	return home_url();
}

add_filter( 'login_redirect', 'mss_login_redirect', 999999, 3 );

/**
 * return blog link by cluster
 *
 * @param $term
 * @param bool|false $html
 * @param string $title
 *
 * @return bool|string
 */
function mss_get_the_blog_link( $term, $html = FALSE, $title = 'Blog' ) {
	if ( $term ) {
		if ( ! $html ) {
			return esc_url( get_term_link( $term ) );
		} else {
			return "<a href='" . esc_url( get_term_link( $term ) ) . "' class=\"blog-link\">$title</a>";
		}

	} else {
		return FALSE;
	}
}

/**
 * return photo page link by cluster
 *
 * @param $term
 * @param bool|false $html
 * @param string $title
 *
 * @return bool|string
 */
function mss_get_the_gallery_link( $html = FALSE, $title = 'Gallery' ) {
	if ( get_option( 'photo_page' ) ) {
		$link = esc_url( get_permalink( get_option( 'photo_page' ) ) );
		if ( ! $html ) {
			return $link;
		} else {
			return "<a href='$link' class='gallery-link'>$title</a>";
		}
	} else {
		return FALSE;
	}
}

/**
 * check is user has access to the cluster
 *
 * @param $cluster_id
 * @param null $user_id
 *
 * @return bool
 */
function mss_has_cluster_access( $cluster_id, $user_id = FALSE ) {
	if ( $cluster_id ) {
		$user_id = ! $user_id ? get_current_user_id() : $user_id;

		if ( $user_id ) {
			$school = mss_get_user_school( $user_id ); //get post object of school

			if ( has_term( $cluster_id, 'cluster', $school ) ) {
				return TRUE;
			}
		}
	}

	return FALSE;
}


/**
 * Check is user has access to the post
 *
 * @param $post
 * @param null $user_id
 *
 * @return bool
 */
function mss_has_blog_post_access( $post, $user_id = NULL ) {

	//If post does not exists
	if ( ! $post ) {
		return FALSE;
	}

	if ( $post->post_type != 'post' ) {
		return TRUE;
	}

	$user_id = $user_id === NULL ? get_current_user_id() : $user_id;

	//if Admin or Editor
	if ( user_can( $user_id, 'edit_pages' ) ) {
		return TRUE;
	}

	//If user is author of the post
	if ( $post->post_author == $user_id ) {
		return TRUE;
	}

	$cluster_id = get_post_meta( $post->ID, 'mss_cluster_id' );

	//If Public post
	if ( $cluster_id == 'all' ) {
		return TRUE;
	}

	//If user has access to the cluster of the post
	if ( mss_has_cluster_access( $cluster_id, $user_id ) ) {
		return TRUE;
	}

	return FALSE; //By default
}

/**
 * create post form (html)
 */
function mss_create_post_form() {
	if ( isset( $_get['post_id'] ) ) {
		$cpost = get_post( $_get['post_id'] );

		if ( ( ! isset( $cpost->post_author ) || $cpost->post_author != get_current_user_id() ) || ! current_user_can( 'edit_posts' ) ) {
			wp_redirect( esc_url( get_permalink( get_option( 'create_post_page' ) ) ) );
		}
	}
	load_template( dirname( __FILE__ ) . '/templates/create-post-form.php', FALSE );
}

add_shortcode( 'create_post_form', 'mss_create_post_form' );

/**
 * create post form submit handler
 */
function mss_create_post_form_submit() {
	session_start();
	if ( ! isset( $_POST['mss_post'] ) ) {
		return;
	}

	$post_title   = $_POST['mss_post']['post_title'];
	$post_content = $_POST['post_content'];
	$user_id      = get_current_user_id();

	if ( ! empty( $post_title ) && ! empty( $post_content ) ) {
		$blog_term = get_term_by( 'slug', 'blog', 'category' );

		$postarr = array(
			'post_title'   => $post_title,
			'post_content' => $post_content,
			'post_type'    => 'post',
		);

		if ( isset( $_POST['mss_post']['post_id'] ) ) {
			$cats[] = $blog_term->term_id;
			//check if user can edit this post
			$query_post_id = $_SERVER['QUERY_STRING'];
			if ( ! empty( $query_post_id ) ) {

				$query_post_id = explode( '=', $query_post_id );
				$post_id       = $_POST['mss_post']['post_id'];
				$post          = get_post( $post_id );

				if ( isset( $query_post_id[1] ) && $query_post_id[1] == $post_id
				     && ( $post->post_author == $user_id || user_can( $user_id, 'edit_pages' ) )
				) {

					//Set some parameters for current post
					$postarr['ID']          = $_POST['mss_post']['post_id'];
					$postarr['post_author'] = $post->post_author;

					if ( user_can( $post->post_author, 'edit_pages' ) ) {
						$postarr['post_status'] = 'publish';
					} else {
						$postarr['post_status'] = 'draft';
					}

					$cat_id = mss_get_user_category( $post->post_author, TRUE );

					if ( $cat_id ) {
						$cats[] = $cat_id;
					}

					$postarr['post_category'] = $cats;

					//Post Updating
					$post_id = wp_update_post( $postarr );
				} else {
					$post_id                            = FALSE;
					$_SESSION['post_create']['error'][] = __( 'You are not author of this post!' );
				}
			} else {
				$post_id                            = FALSE;
				$_SESSION['post_create']['error'][] = __( 'You can\'t create post by this way!' );
			}
		} else {
			$cat_id = mss_get_user_category( $user_id, TRUE );

			$cats[] = $blog_term->term_id;

			if ( $cat_id ) {
				$cats[] = $cat_id;
			}
			$postarr['post_author'] = $user_id;

			// publish post without moderation.
			$postarr['post_status'] = 'publish';
//            if ( user_can( $user_id, 'edit_pages' ) )
//                $postarr['post_status'] = 'publish';
//            else
//                $postarr['post_status'] = 'draft';

            $postarr['post_category'] = $cats;
            $post_id = wp_insert_post($postarr);
        }

        if ( $post_id ) {
            $cluster_id = mss_get_user_cluster( $user_id, true );
            if ( user_can ( $user_id, 'edit_pages' ) ) {
                update_post_meta( $post_id, 'mss_cluster_id', 'all' );
            } else {
                update_post_meta( $post_id, 'mss_cluster_id', $cluster_id );
            }

            $_SESSION['post_create']['message'][] = __( 'The post has created and sent for moderation!' );

            //Redirect after save
            if ( $postarr['post_status'] == 'publish' ) {
                $post_url = esc_url( get_permalink( $post_id ) );
                $blog_cat_id = get_cat_ID('Blog');
                $blog_cat_link = get_category_link($blog_cat_id);
                wp_redirect( $blog_cat_link );
                exit();
            }
        }
    } else {
        $_SESSION['post_create']['message'][] = __( 'Please fill all fields!' );
    }
    wp_redirect( esc_url( get_permalink( get_option( 'create_post_page' ) ) ) );
    die();
}

add_action( 'init', 'mss_create_post_form_submit' );

/**
 * @param $post
 * @param string $title
 *
 * @return string|void
 */
function mss_next_photo_link( $post, $title = 'Next' ) {
	$cluster_id = mss_get_user_cluster( FALSE, TRUE );
	if ( isset( $post->ID ) && $cluster_id ) {
		global $wpdb;
		$next_id = $wpdb->get_var( "select ID from $wpdb->posts
                        inner join $wpdb->postmeta
                        on ID = post_id
                        where ID < $post->ID
                        and post_type = 'photo'
                        and post_status = 'publish'
                        and meta_key = 'mss_cluster_id' and meta_value IN($cluster_id, 'all')
                        order by ID desc
                        limit 1" );

		//if post exists
		if ( $next_id && 'publish' == get_post_status( $next_id ) ) {
			return "<a href='" . esc_url( get_permalink( $next_id ) ) . "'>" . __( $title ) . "</a>";
		}
	}

	return;
}

/**
 * @param $post
 * @param string $title
 *
 * @return string|void
 */
function mss_prev_photo_link( $post, $title = 'Previous' ) {
	$cluster_id = mss_get_user_cluster( FALSE, TRUE );
	if ( isset( $post->ID ) && $cluster_id ) {
		global $wpdb;
		$prev_id = $wpdb->get_var( "select ID from $wpdb->posts
                        inner join $wpdb->postmeta
                        on ID = post_id
                        where ID > $post->ID
                        and post_status = 'publish'
                        and post_type = 'photo'
                        and meta_key = 'mss_cluster_id' and meta_value IN($cluster_id, 'all')
                        order by ID asc
                        limit 1" );

		//if post exists
		if ( $prev_id && 'publish' == get_post_status( $prev_id ) ) {
			return "<a href='" . esc_url( get_permalink( $prev_id ) ) . "'>" . __( $title ) . "</a>";
		}
	}

	return;
}

/**
 * @param $post_id
 */
function mss_save_post( $post ) {

	if ( isset( $_POST['mss_cluster'] ) ) {
		if ( empty( $_POST['mss_cluster'] ) ) {
			delete_post_meta( $post, 'mss_cluster_id' );
		} else {
			update_post_meta( $post, 'mss_cluster_id', $_POST['mss_cluster'] );
		}
	} else {
		if ( user_can( $post->post_author, 'edit_pages' ) ) {
			update_post_meta( $post->ID, 'mss_cluster_id', 'all' );
		} else {
			$cluster_id = mss_get_user_cluster( $post->post_author, TRUE );
			if ( $cluster_id ) {
				update_post_meta( $post->ID, 'mss_cluster_id', $cluster_id );
			}
		}
	}
}

add_action( 'save_post_post', 'mss_save_post', 1 );
add_action( 'future_to_publish', 'mss_save_post', 1 );
add_action( 'new_to_publish', 'mss_save_post', 1 );
add_action( 'draft_to_publish', 'mss_save_post', 1 );

/**
 * Return Author String specific Format for My Sister School
 *
 * @param $user_id
 *
 * @return string
 */
function mss_user_publish_string( $user_id ) {
	if ( ! $user_id ) {
		return NULL;
	}

	$user       = new WP_User( $user_id );
	$first_name = $user->user_firstname;
	$last_name  = $user->user_lastname;
	$year_level = $user->get( 'year_level' );
	$school     = mss_get_user_school( $user_id );

	if ( $first_name && $last_name ) {
		$string_name = $first_name . ' ' . $last_name;
	} else {
		$string_name = $user->data->display_name;
	}

	if ( $year_level ) {
		$year_level_string = ', Year ' . $year_level;
	}

	if ( $school ) {
		$school_string = ', ' . $school->post_title;
	}

	return $string_name . $year_level_string . $school_string;
}

/**
 * Return User emails that can get emails from subscribe2
 *
 * @param $emails
 * @param $post_id
 */
function mss_sort_subscribers_for_s2( $emails, $post_id ) {
	if ( ! $post_id ) {
		return $emails;
	}

	//Get Post Cluster
	$cluster_id = get_post_meta( $post_id, 'mss_cluster_id', TRUE );

	if ( $cluster_id != 'all' && ! empty( $cluster_id ) ) {
		foreach ( $emails as $r_key => $recipient ) {
			$r_user = get_user_by( 'email', $recipient );

			if ( $r_user ) {
				if ( ! mss_has_cluster_access( $cluster_id, $r_user->ID ) ) {
					unset( $emails[ $r_key ] );
				}
			}
		}
	}

	return $emails;
}

add_filter( 's2_send_plain_excerpt_subscribers', 'mss_sort_subscribers_for_s2', 10, 2 );
add_filter( 's2_send_plain_fullcontent_subscribers', 'mss_sort_subscribers_for_s2', 10, 2 );
add_filter( 's2_send_html_excerpt_subscribers', 'mss_sort_subscribers_for_s2', 10, 2 );
add_filter( 's2_send_html_fullcontent_subscribers', 'mss_sort_subscribers_for_s2', 10, 2 );
add_filter( 's2_send_public_subscribers', 'mss_sort_subscribers_for_s2', 10, 2 );

/**
 * @return string
 */
function mss_subscribe2_shortcode_extended() {
	global $wpdb;
	//If user does not logged
	if ( ! get_current_user_id() ) {
		return NULL;
	}

	$user_id = get_current_user_id();
	$user    = new WP_User( $user_id );

	$subscribed = get_user_meta( $user_id, $wpdb->prefix . 's2_subscribed', TRUE );

	if ( ! $subscribed || $subscribed == '' ) {
		$is_subscribed = FALSE;
	} else {
		$is_subscribed = TRUE;
	}

	if ( isset( $_POST['mss_subscribe'] ) ) {

		$s2        = new s2class();
		$s2->s2_mu = TRUE;
		if ( $s2->is_public( $_POST['email'] ) ) {
			echo __( "Only Registered users can subscribe on the blog!" );
		} else {
			if ( $is_subscribed ) {
				$s2->one_click_handler( $user_id, 'unsubscribe' );
				echo __( "You have successfully unsubscribed!" );
			} else {
				$s2->one_click_handler( $user_id, 'subscribe' );
				echo __( "You have successfully subscribed!" );
			}
		}

	} else {
		ob_start(); ?>
		<div class="subscribe-wrapp">
			<form method="post">
				<p>
					<label for="s2email"><?php _e( 'Your email' ); ?>
						:</label><br>
					<input disabled class="filled" name="email" id="s2email"
					       value="<?php echo $user->user_email; ?>" size="20"
					       type="text">
				</p>
				<p>
					<?php if ( $is_subscribed ) : ?>
						<input name="mss_subscribe" value="Unsubscribe"
						       type="submit">
					<?php else : ?>
						<input name="mss_subscribe" value="Subscribe"
						       type="submit">
					<?php endif; ?>
				</p>
			</form>
		</div>
		<?php return ob_get_clean();
	}
}

add_shortcode( 'mss_subscribe2_shortcode_extended', 'mss_subscribe2_shortcode_extended' );

/**
 * @param $settings
 *
 * @return mixed
 */
function mss_tinymce_settings( $settings ) {
	if ( $settings['selector'] == '#comment' ) {
		$settings['auto_focus'] = 'comment';
	}

	return $settings;
}

add_filter( 'tiny_mce_before_init', 'mss_tinymce_settings' );

/**
 * Return User Category
 *
 * @param bool $user_id
 * @param bool $return_id
 *
 * @return array|bool|mixed|null|object|WP_Error
 */
function mss_get_user_category( $user_id = FALSE, $return_id = FALSE ) {
	global $wpdb;
	$user_id = ! $user_id ? get_current_user_id() : $user_id;

	$user_cat = get_user_meta( $user_id, $wpdb->prefix . 'user_category', TRUE );
	if ( $user_cat ) {
		return $return_id ? $user_cat : get_term( $user_cat, 'category' );
	}

	return FALSE;
}

/**
 * Return all categories of users
 *
 * @return array|WP_Error
 */
function mss_get_user_categories() {
	$term = get_term_by( 'slug', 'blog', 'category' );

	return get_term_children( $term->term_id, $term->taxonomy );
}

///**
// * @param $id
// * @param $comment
// * @return bool
// */
//function mss_post_comment_mail($id, $comment){
//
//    //Get Comment Post
//    $photo_post = get_post( $comment->comment_post_ID );
//
//    if ( $photo_post->post_type === 'photo' || $photo_post->post_type == 'post' ) {
//        //Get Comment Author
//        $photo_author = get_userdata( $photo_post->post_author );
//
//        if ( get_current_user_id() !== $photo_author->ID ) {
//            $author_nickname = get_user_meta( $photo_author->ID, 'nickname', true );
//            $nickname = get_user_meta( $comment->user_id, 'nickname', true );
//            $user_mail = $photo_author->user_email;
//            $subject = __( 'Someone has commented on your Post on the MySisterSchool' );
//            $link = get_permalink( $photo_post->ID );
//            $message = __( 'Hi ', 'mch' ) . $author_nickname . "!\n" .
//                    $nickname . __(' has commented on a post that you created. Check it out: ' ) . $link . "\n" .
//                    __('Have a great day!') . "\n" .
//                    __('My Education Group');
//            wp_mail($user_mail, $subject, $message);
//        }
//    }
//    return true;
//}
//add_action( 'wp_insert_comment', 'mss_post_comment_mail', 10, 2 );

function mss_get_all_clusters() {
	return get_terms( 'cluster' );
}

/**
 * Create Cluster Metabox for post, photo posttypes
 */
function mss_add_cluster_metabox() {
	add_meta_box( 'cluster_metabox', __( 'Cluster Settings' ), 'mss_cluster_metabox_content', 'photo', 'side' );
	add_meta_box( 'cluster_metabox', __( 'Cluster Settings' ), 'mss_cluster_metabox_content', 'post', 'side' );
}

add_action( 'add_meta_boxes', 'mss_add_cluster_metabox' );

/**
 * Cluster Metabox Callback
 */
function mss_cluster_metabox_content() {
	load_template( dirname( __FILE__ ) . '/templates/cluster-metabox.php', FALSE );
}

function mss_category_wp_query() {
    global $query_string, $cat, $wp_query;
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $query_string .= "&paged=" . $paged;
    $query_args = array(
        'paged' => $paged,
        'post_type' => 'post',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'term_id',
                'terms'    => array( $cat ),
            ),
        ),
    );
    // if Admin or Editor.
    $user_id = get_current_user_id();
    if ( user_can( $user_id, 'edit_pages' ) ) {
        $wp_query = new WP_Query($query_args);
    } elseif ( $cluster_id = mss_get_user_cluster( $user_id, true ) ) {
        $query_args['meta_query'] = array(
            array(
                'key'     => 'mss_cluster_id',
                'value'   => array( $cluster_id, 'all' ),
            ),
        );
        $wp_query = new WP_Query( $query_args );
    } else {
        $wp_query = null;
    }
}

/**
 * Shortcode for user home page (assigned schools from cluster).
 *
 * @param $atts
 *
 * @return bool|string
 */
function mss_assigned_cluster_schools_user( $atts ) {

    //merge default attr array with inputted array
    $atts = shortcode_atts( array(
        'user_id'          => get_current_user_id(),
        'pagination'       => TRUE,
        'schools_per_page' => 9,
        'access_message'   => __( 'you have no access on this page' )
    ), $atts );

    //get assigned school id
    if ( $school_id = mss_get_user_school( $atts['user_id'], TRUE ) ) {

        //get cluster of this school
        $term = wp_get_post_terms( $school_id, 'cluster' );

        //if cluster exists
        if ( isset( $term[0] ) && ! empty( $term[0] ) ) {

            //select schools by cluster
            $wp_query = new wp_query();

            $query_string = "showposts=" . $atts['schools_per_page'] . "&post_type=school
                    &post_status=publish&cluster=" . $term[0]->slug;

            //if pagination is enabled
            if ( $atts['pagination'] ) {
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $query_string .= "&paged=" . $paged;
            }

            $wp_query->query( $query_string );

            ob_start();
            load_template( dirname( __FILE__ ) . '/templates/clusters-partnerships.php', FALSE );

            if ( $wp_query->have_posts() ) {
                echo "<div class=\"mss-gallery-links\">";
                echo mss_get_the_blog_link( $blog_term, TRUE );
                echo mss_get_the_gallery_link( TRUE );
                echo "</div>";
                echo "<div class='mss_pagination'>";
                echo mss_pagination( $wp_query ); //show pagination
                echo "</div>";
            }

            return ob_get_clean();
        }
    } else {
        echo "<p>" . $atts['access_message'] . "</p>"; //show message if user have no access on this page
    }
}

add_shortcode( 'mss_assigned_cluster_schools_user', 'mss_assigned_cluster_schools_user' );

/**
 * Return clasters which are associated with the school.
 *
 * @param bool $user_id
 * @param bool $return_id
 *
 * @return array|bool|null|wp_post
 */
function mss_get_user_clusters( $user_id = FALSE, $return_id = FALSE ) {
    //get user id
    $user_id = ! $user_id ? get_current_user_id() : $user_id;
    //get school id from user
    $school_id = get_user_meta( $user_id, MSS_SCHOOL_FIELD, TRUE );

    $terms = get_the_terms( $school_id, 'cluster' );

    if ( $return_id ) {
        return $school_id;
    }

    //if post exists
    if ( $school_id && get_post_type( $school_id ) === 'school' ) {
        return $terms;
    } else {
        return FALSE;
    }
}