<?php
$terms = mss_get_user_clusters( get_current_user_id() );
?>
<!--cluster-->
<section class="mss-gallery">
	<?php
	foreach ( $terms as $term ) {
		?>
		<div class="mss-gallery-item">
			<div class="item-content">
				<strong class="item-content-title">
			<span>
				<?php echo $term->name; ?>
</span>
				</strong>
			</div>
		</div>
	<?php } ?>
</section>
