<?php $title = null; $content = null;

if ( isset( $_GET['post_id'] ) && !empty ( $_GET['post_id'] ) ) {
    $edited_post = get_post( $_GET['post_id'] );
    if ( isset( $edited_post->post_title ) && isset( $edited_post->post_content ) ) {
        $title = $edited_post->post_title;
        $content = $edited_post->post_content;
    }
} ?>

<form action="" method="POST">
    <label for="post_title">
        <?php _e( 'Post Title' ); ?>
        <input type="text" name="mss_post[post_title]" value="<?php echo $title; ?>" required autofocus>
    </label>

    <button id="frontend-button" class="create-post-link" style="margin-top: 10px"><?php _e('Upload Image'); ?></button>

    <?php

    wp_editor( do_shortcode($content), 'post_content', array(
            //'wpautop' => false,
            'media_buttons' => false,
            'textarea_rows' => 12,
            'tabindex' => 4,
            'tinymce' => array(
                    'theme_advanced_buttons1' => 'bold, italic, ul, min_size, max_size',

            ),

    ) );
     ?>

    <?php if ( isset( $_GET['post_id'] ) ): ?>
        <input type="hidden" name="mss_post[post_id]" value="<?php _e( $_GET['post_id'] ); ?>">
    <?php endif; ?>

    <input type="submit" name="mss_post[submit_form]" value="<?php echo isset( $_GET['post_id'] ) ? __( 'Update Post' ) : __( 'Create Post' ); ?>">
</form>

<div class="ms-create-post-messages">
    <?php if ( isset( $_SESSION['post_create'] ) && ! empty( $_SESSION['post_create'] ) ) : ?>
        <?php foreach( $_SESSION['post_create'] as $key => $message_type) : ?>
            <?php foreach ( $message_type as $message ) : ?>
                <div class="error mss-create-<?php echo $key; ?>"><?php echo $message; ?></div>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php unset($_SESSION['post_create']); ?>
    <?php endif; ?>
</div>