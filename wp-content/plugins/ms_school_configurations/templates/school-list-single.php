<div class="mss-gallery-item">
	<div class="item-content">
		<strong class="item-content-title">
			<span>
				<?php the_title(); ?>
			</span>
		</strong>
		<?php the_post_thumbnail(); ?>
	</div>
</div>