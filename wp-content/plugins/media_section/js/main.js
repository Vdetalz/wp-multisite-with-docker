(function($) {
    $(document).ready(function() {
        $('.popup-media-video').magnificPopup({
            type: 'inline',
            midClick: true,
            callbacks: {
                open: function() {
                    var magnificPopup = $.magnificPopup.instance,
                    cur = magnificPopup.st.el;
                    var video_id = cur.data("id");
                    var video = document.getElementById("video-" + video_id);
                    document.getElementById("video-" + video_id).parentNode.classList.add("video-container");
                    video.play();
                }
            }
        });
    });
})(jQuery)