<?php
/*
Plugin Name: Media Section
Plugin URI:
Description: Media section for MyChinese Teacher
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

if(!is_admin()) {
    require(plugin_dir_path(__FILE__) . 'includes/Mobile_Detect.php');
}

/**
 * Register media_section post type.
 */
add_action('init', 'register_post_types');
function register_post_types(){
    $args = array(
        'labels' => array(
            'name'               => 'Media Section',
            'singular_name'      => 'Media Icon',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Media Icon',
            'edit_item'          => 'Edit Media Icon',
            'new_item'           => 'New Media Icon',
            'view_item'          => 'View Media Icon',
            'search_items'       => 'Search Media Icon',
            'not_found'          => 'Not found',
            'not_found_in_trash' => 'Not found in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Media Section',
        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'show_in_nav_menus'   => null,
        'supports' => array('title')
    );

    register_post_type('media_section', $args );
}

/**
 * Register the Widget
 */
add_action( 'widgets_init', create_function( '', 'register_widget("Media_Section_Widget");' ) );
class Media_Section_Widget extends WP_Widget
{
    const VIDEO_TYPE = 'video';
    const LINK_TYPE = 'link';

    /**
     * Constructor
     **/
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'media_section',
            'description' => 'Widget that uses the built in Media library.'
        );

        add_action('wp_enqueue_scripts', array($this, 'scripts'));

        parent::__construct( 'media_section', 'Media Section Widget', $widget_ops );

    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    public function widget( $args, $instance )
    {
        $title = '';
        if(!empty($instance['title'])) {
            $title = $instance['title'];
        }

        $query = new WP_Query(array(
            'post_type' => 'media_section',
            'post_status' => 'publish',
            'nopaging' => true
        ));
        $detect = new Mobile_Detect;
        !$detect->isMobile() ? $this->get_desktop_version($title, $query) : $this->get_mobile_version($query);
    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    public function update( $new_instance, $old_instance )
    {
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void
     **/
    public function form( $instance )
    {
        $title = __('Widget Image');
        if(isset($instance['title']))
        {
            $title = $instance['title'];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
    <?php
    }

    /**
     * Displays widget desktop version
     *
     * @param $title
     * @param $query
     */
    public function get_desktop_version($title, $query)
    {
        $popup_ids = array();
        foreach(array('media-large-screen js-custom-scroll', 'media-small-screen js-slick-init') as $class): ?>
            <div class="<?php echo $class; ?>">
                <div class="media-section">
                    <?php if($title): ?>
                        <h5><?php echo $title; ?></h5>
                    <?php endif; ?>

                    <?php
                    if ($query->have_posts()): ?>
                        <ul class="media-icons">
                            <?php while ( $query->have_posts() ):
                                $query->the_post();
                                $image = get_field('icon');
                                $type = get_field('type'); ?>
                                <li>
                                    <?php if($type === self::LINK_TYPE): $url = get_field('url'); ?>
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>"/>
                                        </a>
                                    <?php endif ;?>

                                    <?php if($type === self::VIDEO_TYPE): $video_file = get_field('video_file'); ?>
                                        <a href="#popup-<?php the_ID(); ?>" class="popup-media-video" data-id="<?php the_ID(); ?>">
                                            <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>"/>
                                        </a>

                                        <?php if(!in_array(get_the_ID(), $popup_ids)): $popup_ids[] = get_the_ID(); ?>
                                            <div id="popup-<?php the_ID(); ?>" class="mfp-hide">
                                                <video id="video-<?php the_ID(); ?>" controls>
                                                    <source src="<?php echo $video_file['url']; ?>" type="video/mp4">
                                                    Your browser does not support the video tag.
                                                </video>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif ;?>
                                </li>

                            <?php endwhile ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>

    <?php
    }

    /**
     * Displays widget mobile version
     *
     * @param $query
     */
    public function get_mobile_version($query)
    {
        $popup_ids = array();
        foreach(array('media-large-screen js-custom-scroll', 'media-small-screen js-slick-init') as $class): ?>
            <div class="<?php echo $class; ?>">
                <div class="media-section">
                    <?php
                    if ($query->have_posts()): ?>
                        <ul class="media-icons">
                            <?php while ( $query->have_posts() ):
                                $query->the_post();
                                $image = get_field('icon');
                                $type = get_field('type'); ?>
                                <li>
                                    <?php if($type === self::LINK_TYPE): $url = get_field('url'); ?>
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>"/>
                                        </a>
                                    <?php endif ;?>

                                    <?php if($type === self::VIDEO_TYPE): $video_file = get_field('video_file'); ?>
                                        <a href="#popup-<?php the_ID(); ?>" class="popup-media-video" data-id="<?php the_ID(); ?>">
                                            <img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['title']; ?>"/>
                                        </a>

                                        <?php if(!in_array(get_the_ID(), $popup_ids)): $popup_ids[] = get_the_ID(); ?>
                                            <div id="popup-<?php the_ID(); ?>" class="mfp-hide">
                                                <video id="video-<?php the_ID(); ?>" controls>
                                                    <source src="<?php echo $video_file['url']; ?>" type="video/mp4">
                                                    Your browser does not support the video tag.
                                                </video>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif ;?>
                                </li>

                            <?php endwhile ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>

    <?php
    }

    public function scripts()
    {
        if(is_active_widget(false, false, $this->id_base, true)){
            wp_register_script('media-main', plugin_dir_url(__FILE__) . 'js/main.js', array(), false, true);
            wp_enqueue_script('media-main');
        }
    }
}

function register_media_sidebar(){
    register_sidebar( array(
        'name' => 'Media section sidebar',
        'id' => 'media_section',
        'description' => 'Media Section Sidebar.',
        'before_widget' => '<li class="media-section-sidebar">',
        'after_widget' => '</li>',
        'before_title' => '<h2 class="widgettitle media-section">',
        'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'register_media_sidebar' );