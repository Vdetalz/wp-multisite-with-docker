<?php

class MCH_Emails_Page {
    const PAGE_SLUG = 'emails-setting-admin';
    const EMAIL_OPTION_NAME = 'mch_emails';
    const SKYPE_OPTION_NAME = 'mch_skype_names';
    const PHONE_OPTION_NAME = 'mch_phone_numbers';

    private $json_emails;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_emails_page' ) );
        add_action( 'admin_init', array( $this, 'init' ) );
        add_option(self::EMAIL_OPTION_NAME);

        $this->json_emails = get_option(self::EMAIL_OPTION_NAME);
    }

    /**
     * Add emails page
     */
    public function add_emails_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Manage emails',
            'Manage emails',
            'manage_options',
            self::PAGE_SLUG,
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        if(isset($_POST['numbers_submit'])) {
            update_option(self::SKYPE_OPTION_NAME, $_POST['skype']);
            update_option(self::PHONE_OPTION_NAME, $_POST['phone']);

	        update_option('of1_name', $_POST['of1-name']);
	        update_option('of1_f1_lable', $_POST['of1-f1-lable']);
	        update_option('of1_f1_value', $_POST['of1-f1-value']);
	        update_option('of1_f2_lable', $_POST['of1-f2-lable']);
	        update_option('of1_f2_value', $_POST['of1-f2-value']);

	        update_option('of2_name', $_POST['of2-name']);
	        update_option('of2_f1_lable', $_POST['of2-f1-lable']);
	        update_option('of2_f1_value', $_POST['of2-f1-value']);
	        update_option('of2_f2_lable', $_POST['of2-f2-lable']);
	        update_option('of2_f2_value', $_POST['of2-f2-value']);
        }

        ?>
        <h3><?php _e('Manage emails'); ?></h3>

        <?php $this->get_emails_table() ?>

        <div class="wrap">
                <form method="post">
                    <input type="email" id="email" name="email" placeholder="<?php _e('Enter new email'); ?>" />
                    <?php submit_button( __('Save email'), 'primary', 'email_submit'); ?>
                </form>

                <form method="post">
	                <style>
		                .offices-info, .offices-info td, .offices-info td p, .offices-info th, .form-wrap label{
			                width: 50px
		                }
		                .offices-info h3{
			                width: 90px;
		                }
	                </style>
	                <table class="form-table offices-info">
		                <tbody>
		                <tr>
			                <th>
				                <h3><?php _e('Office #1'); ?></h3>
			                </th>
		                </tr>
		                <tr>
			                <th scope="row">
				                <label for="of1-name"><?php _e('Office name'); ?></label>
			                </th>
			                <td>
				                <input id="of1-name" class="regular-text" type="text" value="<?php echo get_option('of1_name'); ?>" name="of1-name">
			                </td>
		                </tr>
		                <tr>
			                <th>
				               <h4><?php _e('Contacts'); ?></h4>
			                </th>
		                </tr>
		                <tr>
			                <td>
				                <label for="of1-f1-lable"><?php _e('Lable:'); ?></label>
			                </td>
			                <td>
				                <input id="of1-f1-lable" class="regular-text" type="text" value="<?php echo get_option('of1_f1_lable'); ?>" name="of1-f1-lable">
			                </td>
			                <td>
				                <label for="of1-f1-value"><?php _e('Value:'); ?></label>
			                </td>
			                <td>
				                <input id="of1-f1-value" class="regular-text" type="text" value="<?php echo get_option('of1_f1_value'); ?>" name="of1-f1-value">
			                </td>
		                </tr>
		                <tr>
			                <td>
				                <label for="of1-f2-lable"><?php _e('Lable:'); ?></label>
			                </td>
			                <td>
				                <input id="of1-f2-lable" class="regular-text" type="text" value="<?php echo get_option('of1_f2_lable'); ?>" name="of1-f2-lable">
			                </td>
			                <td>
				                <label for="of1-f2-value"><?php _e('Value:'); ?></label>
			                </td>
			                <td>
				                <input id="of1-f2-value" class="regular-text" type="text" value="<?php echo get_option('of1_f2_value'); ?>" name="of1-f2-value">
			                </td>
		                </tr>
						<tr>
							<th>
								<h3><?php _e('Office #2'); ?></h3>
							</th>
						</tr>
		                <tr>
			                <th scope="row">
				                <label for="of2-name"><?php _e('Office name'); ?></label>
			                </th>
			                <td>
				                <input id="of2-name" class="regular-text" type="text" value="<?php echo get_option('of2_name'); ?>" name="of2-name">
			                </td>
		                </tr>
		                <tr>
			                <th>
				                <h4><?php _e('Contacts'); ?></h4>
			                </th>
		                </tr>
		                <tr>
			                <td>
				                <label for="of2-f1-lable"><?php _e('Lable:'); ?></label>
			                </td>
			                <td>
				                <input id="of2-f1-lable" class="regular-text" type="text" value="<?php echo get_option('of2_f1_lable'); ?>" name="of2-f1-lable">
			                </td>
			                <td>
				                <label for="of2-f1-value"><?php _e('Value:'); ?></label>
			                </td>
			                <td>
				                <input id="of2-f1-value" class="regular-text" type="text" value="<?php echo get_option('of2_f1_value'); ?>" name="of2-f1-value">
			                </td>
		                </tr>
		                <tr>
			                <td>
				                <label for="of2-f2-lable"><?php _e('Lable:'); ?></label>
			                </td>
			                <td>
				                <input id="of2-f2-lable" class="regular-text" type="text" value="<?php echo get_option('of2_f2_lable'); ?>" name="of2-f2-lable">
			                </td>
			                <td>
				                <label for="of2-f2-value"><?php _e('Value:'); ?></label>
			                </td>
			                <td>
				                <input id="of2-f2-value" class="regular-text" type="text" value="<?php echo get_option('of2_f2_value'); ?>" name="of2-f2-value">
			                </td>
		                </tr>
		                </tbody>
	                </table>

                    <?php submit_button( __('Save numbers'), 'primary', 'numbers_submit'); ?>
                </form>
        </div>
    <?php
    }

    /**
     * Init function
     */
    public function init()
    {
        // delete email
        if(isset($_GET['delete'])) {
            $emails = json_decode($this->json_emails, true);
            foreach($emails as $key => $email) {
                if($email === $_GET['delete']) {
                    unset($emails[$key]);
                    update_option(self::EMAIL_OPTION_NAME, json_encode($emails));
                    break;
                }
            }

            header('Location: ' . $this->get_page_url());
        }

        // Submit
        if(isset($_POST['email_submit'])) {
            $email = $_POST['email'];
            // validate email
            if($email == '') {
                add_action( 'admin_notices', array( $this, 'error_notice' ), 99);
                return;
            }

            $email_json = json_encode(array($email));
            if($this->json_emails === '') {

                update_option(self::EMAIL_OPTION_NAME, $email_json);
                add_action( 'admin_notices', array( $this, 'success_notice' ), 99);

            } elseif(in_array($email, json_decode($this->json_emails, true))) {

                add_action( 'admin_notices', array( $this, 'email_exist_notice' ), 99);

            } else {

                $emails = json_decode($this->json_emails, true);
                $emails[] = $email;
                update_option(self::EMAIL_OPTION_NAME, json_encode($emails));
                add_action( 'admin_notices', array( $this, 'success_notice' ), 99);

            }

        }
    }

    /**
     * Error notice if email is blank
     */
    public function error_notice()
    {
        $class = "error";
        $message = "Email can't be blank.";
        echo"<div class=\"$class\"> <p>$message</p></div>";
    }

    /**
     * Success notice
     */
    public function success_notice()
    {
        $class = "updated";
        $message = "Success!";
        echo"<div class=\"$class\"> <p>$message</p></div>";
    }

    /**
     * Email exist notice
     */
    public function email_exist_notice()
    {
        $class = "updated";
        $message = "Email already exist.";
        echo"<div class=\"$class\"> <p>$message</p></div>";
    }

    /**
     * Show emails table
     */
    public function get_emails_table()
    {
        $json_emails = get_option(self::EMAIL_OPTION_NAME);
        ?>
        <table class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <th>
                        <?php _e("Email") ?>
                    </th>
                    <th>
                        <?php _e("Delete") ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php if($json_emails && $json_emails != '[]'): ?>
                    <?php foreach(json_decode($json_emails, true) as $email): ?>
                        <tr>
                            <td>
                                <?php echo $email; ?>
                            </td>
                            <td>
                                <a href="<?php echo $this->get_page_url() ?>&delete=<?php echo $email ?>"><?php _e("Delete"); ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td><?php _e("Emails not found."); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
            </thead>
        </table>
    <?php
    }

    /**
     * @return string Page url
     */
    public function get_page_url()
    {
        return get_admin_url(null, 'options-general.php?page=' . self::PAGE_SLUG);
    }
}

// init page
if(is_admin()) {
    $emails_page = new MCH_Emails_Page();
}
