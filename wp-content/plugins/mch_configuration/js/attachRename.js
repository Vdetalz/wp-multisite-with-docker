jQuery(document).ready(function ($) {

    var current_row;

    $('.default-hidden').hide();

    $('.edit-current-file').on('click', function () {

        current_row = $(this).parents().eq(1);

        $(this).hide();
        current_row.find('.file-title').hide();
        current_row.find('.file-country').hide();
        current_row.find('.default-hidden').show();
        current_row.find('.process-saving').hide();

    });

    $('.save-current-title').on('click', function () {
        current_row = $(this).parents().eq(1);
        var current_fid = $(this).data('fileid');
        var entered_title = current_row.find('input.entered-title').val();
        var entered_country = current_row.find('.entered-country').val();

        $.ajax({
            url: Ajax.ajaxurl,
            type: 'post',
            data: {
                action: 'ajax_save_file_title',
                file_id: current_fid,
                entered_title: entered_title,
                entered_country: entered_country
            },
            beforeSend: function () {
                current_row.find('.save-current-title').hide();
                current_row.find('.process-saving').show();
            },
            success: function (responce) {
                var $new_title = $(responce).filter('.file-title');
                var new_title = $new_title.text();
                var $new_country = $(responce).filter('.file-country');
                var new_country = $new_country.text();

                current_row.find('.file-title').text(new_title);
                current_row.find('input.entered-title.default-hidden').val(new_title);
                current_row.find('.file-country').text(new_country);
                current_row.find('input.entered-country.default-hidden').val(new_country);

                current_row.find('.edit-current-file').show();
                current_row.find('.file-title').show();
                current_row.find('.file-country').show();
                current_row.find('.process-saving').hide();
                current_row.find('.default-hidden').hide();
            }
        });

    });

});
