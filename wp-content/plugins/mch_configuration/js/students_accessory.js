jQuery(document).ready(function($){
    var teacher_id = $('.current-teacher-value').val();

	$('.my-students').live('click', function(){

        $(this).addClass('active');
        $('.all-students').removeClass('active');
		$('.student-accessory-value').val(teacher_id);
        $('form.selection-students').submit();
	});

	$('.all-students').live('click', function(){
        $(this).addClass('active');
        $('.my-students').removeClass('active');
		$('.student-accessory-value').val(0);
        $('form.selection-students').submit();
	});

	if($('.active').hasClass('my-students')){
		$('.student-accessory-value').val(teacher_id);
	}
	else if($('.active').hasClass('all-students')){
		$('.student-accessory-value').val(0);
	}

    $('#students_sort').change(function(){
        $('form.selection-students').submit();
    });

});