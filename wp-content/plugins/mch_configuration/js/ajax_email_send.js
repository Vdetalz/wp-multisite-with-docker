jQuery(document).ready(function($){
    $('.mail-lesson').click(function(e){
        e.preventDefault();
        var lesson_id = $('#lesson_id').val();
        var course_id = $('#course_id').val();
        var module_id = $('#module_id').val();

        $.ajax({
            url: Ajax.ajaxurl,
            type: 'post',
            data: 'action=ajax_send_course_by_email&course_id=' + course_id + '&module_id=' + module_id + '&lesson_id=' + lesson_id,
            success: function(result){
                $('.mail-lesson').addClass('sent-done');
            }
        });
    });
});