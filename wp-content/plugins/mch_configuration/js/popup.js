jQuery(document).ready(function($){
	$('body.teacher-portal').append('<div id="blackout" class="history-popup-wrap"></div>');

	var boxWidth = 1000;
	$(window).resize(centerBox);
	$(window).scroll(centerBox);
	centerBox();

	/*
	 * History pop-up js functional
	 */
	$('[class*=history-item-wrapper]').die().live('click', function(e) {

		/* Preventing default actions */
		e.preventDefault();
		e.stopPropagation();

		/* Get id */
		var name = $(this).attr('class'),
		    id = $(this).attr('id'),
		    scrollPos = $(window).scrollTop();

		$('#popup-box-'+id).show();
		$('#blackout').show();
		$('body').css('overflow', 'hidden').scrollTop(scrollPos);
		$('html').scrollTop(scrollPos);
	});

	$('[class*=history-popup-box-]').live('click', function(e) {

		e.stopPropagation();
	});
	$('#blackout').live('click', function() {
		var scrollPos = $(window).scrollTop();

		$('[id^=popup-box-]').hide();
		$('#blackout').hide();
		$("body").css("overflow","auto");
		$('html').scrollTop(scrollPos);
	});

    $('.close').live('click', function() {
        var scrollPos = $(window).scrollTop();

        $('[id^=popup-box-]').hide();
        $('#blackout').hide();
        $("body").css("overflow","auto");
        $('html').scrollTop(scrollPos);
    });
	/**
	 * Full history pop-up functional
	 */
	$('.show-all-lessons-button').live('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var name = $(this).attr('class'),
		    id = $(this).attr('id'),
		    scrollPos = $(window).scrollTop();

		$('#popup-box-'+id).show();
		$('#blackout').show();
		$('body').css('overflow', 'hidden');
		$('html').scrollTop(scrollPos);
	});

	$('[class*=show-full-history-button]').live('click', function(e) {

		e.stopPropagation();
	});

    $('#blackout, .close').live('click', function() {
		var scrollPos = $(window).scrollTop();

		$('[id^=popup-box-]').hide();
		$('#blackout').hide();
		$("body").css("overflow","auto");
		$('html').scrollTop(scrollPos);
	});

    function centerBox() {

        var winWidth = jQuery(window).width(),
            winHeight = jQuery(document).height(),
            scrollPos = jQuery(window).scrollTop(),
            disWidth = (winWidth - boxWidth) / 2,
            disHeight = scrollPos + 150;

        $('.popup-box').css({'width' : boxWidth+'px', 'left' : disWidth+'px', 'top' : disHeight+'px'});
        $('#blackout').css({'height' : winHeight+'px'});

        return false;
    }

});

