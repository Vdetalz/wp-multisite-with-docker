/*global jQuery, document, ajaxurl, window, console*/
jQuery(document).ready(function ($) {
    "use strict";

    $('body').on('change', 'select.edd_cr_download', function () {
        var $this = $(this), download_id = $this.val(), key = $this.attr('data-key'), postData;

        if (parseInt(download_id) > 0) {
            $this.parent().next('td').find('select').remove();
            $this.parent().next().find('.edd_cr_loading').show();

            postData = {
                action : 'mch_add_price_variation',
                download_id: download_id,
                key: key
            };

            $.ajax({
                type: "POST",
                data: postData,
                url: ajaxurl,
                success: function (response) {
                    if (response) {
                        $this.parent().next('td').find('.edd_cr_variable_none').hide();
                        $(response).appendTo($this.parent().next('td'));
                    } else {
                        $this.parent().next('td').find('.edd_cr_variable_none').show();
                    }
                }
            }).fail(function (data) {
                if (window.console && window.console.log) {
                }
            });

            $this.parent().next().find('.edd_cr_loading').hide();
        } else {
            $this.parent().next('td').find('.edd_cr_variable_none').show();
            $this.parent().next('td').find('.edd_price_options_select').remove();
        }
    });

    $('.access-form').accordion( {heightStyle: "content" });
});

/**
 * From EDD plugin
 */
jQuery(document).ready(function ($) {

    /**
     * Download Configuration Metabox
     */
    var MCH_Download_Configuration = {
        init : function() {
            this.add();
            this.move();
            this.remove();
            this.type();
            this.prices();
        },
        clone_repeatable : function(row) {

            // Retrieve the highest current key
            var key = highest = 1;

            row.parent().find( 'tr.edd_repeatable_row .edd_cr_download' ).each(function() {
                var current = $(this).attr( 'data-key' );
                 if(current !== undefined) {
                     var re = /\[+([^\[\]]+)\]+/g;
                     var matches = current.match(re);
                     current = matches[1].replace(re, '$1')
                     if (parseInt(current) > highest) {
                         highest = parseInt(current);
                     }
                 }
            });
            key = highest += 1;


            clone = row.clone();

            /** manually update any select box values */
            clone.find( 'select' ).each(function() {
                $( this ).val( row.find( 'select[name="' + $( this ).attr( 'name' ) + '"]' ).val() );
            });

            clone.removeClass( 'edd_add_blank' );

            clone.find( 'td input, td select' ).val( '' );
            clone.find( 'input, select' ).each(function() {
                var name = $( this ).attr( 'name' );

                name = name.replace( /\[(\d+)\]/, '[' + parseInt( key ) + ']');

                $( this ).attr( 'name', name ).attr( 'id', name );

                var keyVal = $(this).attr('data-key');
                if(keyVal !== undefined){
                    var re = /\[+([^\[\]]+)\]+/g;
                    var matches =  keyVal.match(re);

                    if (matches != null){
                        $(this).attr( 'data-key', '[' + matches[0].replace(re, '$1') +'][' + parseInt( key ) +']' );
                    }
                    else{
                        $(this).attr( 'data-key', key );
                    }
                }

            });

            clone.find( 'span.edd_price_id' ).each(function() {
                $( this ).text( parseInt( key ) );
            });

            return clone;
        },

        add : function() {
            $( 'body' ).on( 'click', '.submit .edd_add_repeatable', function(e) {
                e.preventDefault();
                var button = $( this ),
                    row = button.parent().parent().prev( 'tr' ),
                    clone = MCH_Download_Configuration.clone_repeatable(row);
                clone.insertAfter( row );
            });
        },

        move : function() {

            $(".edd_repeatable_table tbody").sortable({
                handle: '.edd_draghandle', items: '.edd_repeatable_row', opacity: 0.6, cursor: 'move', axis: 'y', update: function() {
                    var count  = 0;
                    $(this).find( 'tr' ).each(function() {
                        $(this).find( 'input.edd_repeatable_index' ).each(function() {
                            $( this ).val( count );
                        });
                        count++;
                    });
                }
            });

        },

        remove : function() {
            $( 'body' ).on( 'click', '.edd_remove_repeatable', function(e) {
                e.preventDefault();

                var row   = $(this).parent().parent( 'tr' ),
                    count = row.parent().find( 'tr' ).length - 1,
                    type  = $(this).data('type'),
                    repeatable = 'tr.edd_repeatable_' + type + 's';

                /** remove from price condition */
                $( '.edd_repeatable_condition_field option[value=' + row.index() + ']' ).remove();

                if( count > 1 ) {
                    $( 'input, select', row ).val( '' );
                    row.fadeOut( 'fast' ).remove();
                } else {
                    switch( type ) {
                        case 'price' :
                            alert( edd_vars.one_price_min );
                            break;
                        case 'file' :
                            alert( edd_vars.one_file_min );
                            break;
                        default:
                            alert( edd_vars.one_field_min );
                            break;
                    }
                }

                /* re-index after deleting */
                $(repeatable).each( function( rowIndex ) {
                    $(this).find( 'input, select' ).each(function() {
                        var name = $( this ).attr( 'name' );
                        name = name.replace( /\[(\d+)\]/, '[' + rowIndex+ ']');
                        $( this ).attr( 'name', name ).attr( 'id', name );
                    });
                });
            });
        },

        type : function() {

            $( 'body' ).on( 'change', '#_edd_product_type', function(e) {

                if ( 'bundle' === $( this ).val() ) {
                    $( '#edd_products' ).show();
                    $( '#edd_download_files' ).hide();
                    $( '#edd_download_limit_wrap' ).hide();
                } else {
                    $( '#edd_products' ).hide();
                    $( '#edd_download_files' ).show();
                    $( '#edd_download_limit_wrap' ).show();
                }

            });

        },

        prices : function() {
            $( 'body' ).on( 'change', '#edd_variable_pricing', function(e) {
                $( '.edd_pricing_fields,.edd_repeatable_table .pricing' ).toggle();
            });
        }

    };

    MCH_Download_Configuration.init();

});
