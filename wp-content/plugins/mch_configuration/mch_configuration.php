<?php
/*
Plugin Name: MCH Configuration
Plugin URI:
Description: Plugin for MyChinese Mecca (Hub) site settings
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

/**
 * Include teacher portal
 *************************/
$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include($plugin_dir . 'mch_teacher_portal.php');

/*************************
 * Include course_page
 *************************/

$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include($plugin_dir . 'mch_course_page.php');

/*************************
 * Include edd_expiration
 *************************/

$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include($plugin_dir . 'edd_expiration.php');

// create admin page to manage emails
include($plugin_dir . 'mch_emails_page.php');

/**
 * On plugin activation - creates new DB for the teachers complete lessons
 */
function mch_configuration_activate()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "teachers_user_list";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id bigint(11) NOT NULL AUTO_INCREMENT,
				  author_id bigint(11) DEFAULT '0' NOT NULL,
				  subscriber_id bigint(11) DEFAULT '0' NOT NULL,
				  course_level_id bigint(11) DEFAULT '0' NOT NULL,
				  module_id bigint(11) DEFAULT '0' NOT NULL,
				  lesson_id bigint(11) DEFAULT '0' NOT NULL,
				  comment text NOT NULL,
				  complete_date VARCHAR(255) NULL,
				  UNIQUE KEY id (id)
				);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "next_week_lesson";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  user_id int(11) NOT NULL,
				  teacher_id int(11) NOT NULL,
				  pathway int(11) NOT NULL,
				  module int(11) NOT NULL,
				  lesson VARCHAR(10) NOT NULL,
				  UNIQUE KEY id (id)
				) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "devices";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  classroom_teacher_id int(11) NOT NULL,
				  platform VARCHAR(255) NOT NULL,
				  token VARCHAR(255) NOT NULL,
				  UNIQUE KEY id (id)
				) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "lesson_survey";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  survey_id int(11) NOT NULL AUTO_INCREMENT,
				  pathway int(11) NOT NULL,
				  module int(11) NOT NULL,
				  lesson VARCHAR(255) NOT NULL,
				  lesson_id int(11) NOT NULL,
				  chinese_teacher_id int(11) NOT NULL,
				  classroom_teacher_id int(11) NOT NULL,
				  star_rating decimal(10,1),
				  comment TEXT,
				  is_rated TINYINT(1) DEFAULT '0',
				  technology_rating TINYINT UNSIGNED NOT NULL,
				  ct_rating TINYINT UNSIGNED NOT NULL,
				  survey_date int(11) NOT NULL,
                                  is_cancelled TINYINT(1) DEFAULT '0',
				  UNIQUE KEY survey_id (survey_id)
				) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
register_activation_hook(__FILE__, 'mch_configuration_activate');

add_action('admin_menu', 'mch_admin_menu');

/**
 * MCH Configuration admin menu
 */
function mch_admin_menu()
{

    if (function_exists('add_menu_page') && function_exists('EDD')) {
        add_menu_page(
            __('MCH Configuration'),
            __('MCH Configuration'),
            null,
            basename(__FILE__),
            'mch_lessons_templates_access'
        );
    }
    if (function_exists('add_submenu_page') && function_exists('EDD')) {
        add_submenu_page(
            basename(__FILE__),
            __('MCH Most recent lesson'),
            __('MCH most recent lesson'),
            1,
            'admin_most_recent_lesson',
            'mch_most_recent_lesson'
        );
    }
    if (function_exists('add_submenu_page') && function_exists('EDD')) {
        add_submenu_page(
            basename(__FILE__),
            __('MCH E-mailable downloads'),
            __('MCH E-mailable downloads'),
            2,
            'admin_emailable_downloads',
            'mch_emailable_downloads'
        );
    }
}


include_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (is_plugin_active('edd-content-restriction/edd-content-restriction.php')) {

    /**
     * Settings page for lessons templates access
     *
     */
    function mch_lessons_templates_access()
    {
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('jquery-ui-accordion');
        wp_enqueue_script('mch-admin', plugins_url('assets/js/admin.js', __FILE__), array('jquery'), '1.0');

        wp_enqueue_style('mch-admin', plugins_url('assets/css/admin.css', __FILE__));
        wp_enqueue_style('mch-admin', plugins_url('//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"', __FILE__));
        wp_enqueue_style('mch-admin-jqu', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css', false);

        $downloads = get_posts(array('post_type' => 'download', 'posts_per_page' => -1));

        $templates = array(
            'vocabulary' => __('Vocabulary lists', 'mch_conf'),
            'dialogues' => __('Dialogues', 'mch_conf'),
            'vocab_video' => __('Videos', 'mch_conf'),
            'quiz' => __('Lesson Quiz', 'mch_conf'),
            'explanation' => __('Written Explanation', 'mch_conf'),
            'video_explainer' => __('Video Explainer', 'mch_conf'),
            'view_forum' => __('View Forum', 'mch_conf'),
            'ask_forum' => __('Ask a Question on the Forum', 'mch_conf'),
            'end_test' => __('End Test', 'mch_conf'),
            'final_exam' => __('End of course exam', 'mch_conf'),
            'files' => __('Files', 'mch_conf'),
            'links' => __('Links', 'mch_conf'),
            'community' => __('Community', 'mch_conf'),
        );

        if (isset($_POST['mch_templates_access']) && !empty($_POST['mch_templates_access'])) {
            foreach ($_POST['mch_templates_access'] as $key => $items) {
                foreach ($items as $item) {
                    $has_items = false;
                    if ('any' !== $item['download'] && !empty($item['download'])) {
                        $option = get_option($key . '_template_access');

                        if (!$option) {
                            add_option($key . '_template_access', $_POST['mch_templates_access'][$key]);
                        }

                        $has_items = true;

                    } else if ('any' == $item['download']) {
                        $has_items = true;
                    }

                    if ($has_items) {
                        update_option($key . '_template_access', $_POST['mch_templates_access'][$key]);
                    } else {
                        delete_option($key . '_template_access');
                    }
                }
            }
        }

        foreach ($templates as $key => $title) {
            $options[$key] = get_option($key . '_template_access');
        }


        ?>
        <h2><?php echo __('MCT Lessons Templates Access'); ?></h2>
        <form method="post" action="">
            <div class="access-form">
                <?php foreach ($templates as $key => $title): $i = 0; ?>
                    <h3><?php echo $title; ?></h3>
                    <?php if ($downloads) : ?>
                        <div id="edd-cr-options" class="edd_meta_table_wrap">
                            <table class="widefat edd_repeatable_table" width="100%" cellpadding="0" cellspacing="0">
                                <thead>
                                <th><?php echo edd_get_label_singular(); ?></th>
                                <th><?php echo sprintf(__('%s Variation', 'mch_conf'), edd_get_label_singular()); ?></th>
                                <th style="width: 2%"></th>
                                </thead>
                                <tbody>
                                <?php if ($options[$key]): ?>
                                    <?php foreach ($options[$key] as $option): ?>
                                        <tr class="edd-cr-option-wrapper edd_repeatable_row">
                                            <td>
                                                <select
                                                    name="mch_templates_access[<?php echo $key; ?>][<?php echo $i; ?>][download]"
                                                    id="edd_cr_download[<?php echo $key; ?>][<?php echo $i; ?>][download]"
                                                    class="edd_cr_download"
                                                    data-key="<?php echo '[' . $key . '][' . $i . ']'; ?>">
                                                    <option value=""><?php echo __('None', 'mch_conf'); ?></option>
                                                    <option value="any" <?php selected('any', $option['download']); ?>>
                                                        <?php echo sprintf(__('Customers who have purchased any %s', 'mch_conf'), edd_get_label_singular()); ?>
                                                    </option>
                                                    <?php
                                                    foreach ($downloads as $download) {
                                                        echo '<option value="' . absint($download->ID) . '" ' . selected($option['download'], $download->ID, false) . '>' . esc_html(get_the_title($download->ID)) . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <?php if (isset($option['price_id'])) :
                                                    $prices = get_post_meta($option['download'], 'edd_variable_prices', true);
                                                    ?>
                                                    <select
                                                        class="edd_price_options_select edd-select edd-select edd_cr_download"
                                                        name="mch_templates_access[<?php echo $key; ?>][<?php echo $i; ?>][price_id]">
                                                        <option
                                                            value="all" <?php selected('all', $$option['price_id'], false); ?>><?php _e('All prices', 'edd_cr'); ?></option>
                                                        <?php
                                                        if ($prices) {
                                                            foreach ($prices as $id => $data) {
                                                                echo '<option value="' . absint($id) . '" ' . selected($id, $option['price_id'], false) . '>' . esc_html($data['name']) . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <p class="edd_cr_variable_none"
                                                       style="display: none;"><?php _e('None', 'edd_cr'); ?></p>
                                                <?php else: ?>
                                                    <p class="edd_cr_variable_none"><?php _e('None', 'edd_cr'); ?></p>
                                                <?php endif; ?>
                                                <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>"
                                                     class="waiting edd_cr_loading" style="display:none;"/>
                                            </td>
                                            <td>
                                                <a href="#" class="edd_remove_repeatable" data-type="price"
                                                   style="background: url(<?php echo admin_url('/images/xit.gif'); ?>) no-repeat;">&times;</a>
                                            </td>
                                        </tr>

                                        <?php $i++; endforeach; ?>
                                <?php else: ?>
                                    <tr class="edd-cr-option-wrapper edd_repeatable_row">
                                        <td>
                                            <select name="mch_templates_access[<?php echo $key; ?>][0][download]"
                                                    id="edd_cr_download[<?php echo $key; ?>][0][download]"
                                                    class="edd_cr_download"
                                                    data-key="<?php echo '[' . $key . '][0]'; ?>">
                                                <option value=""><?php echo __('None', 'mch_conf'); ?></option>
                                                <option value="any">
                                                    <?php echo sprintf(__('Customers who have purchased any %s', 'mch_conf'), edd_get_label_singular()); ?>
                                                </option>
                                                <?php
                                                foreach ($downloads as $download) {
                                                    echo '<option value="' . absint($download->ID) . '">' . esc_html(get_the_title($download->ID)) . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php
                                            echo '<p class="edd_cr_variable_none">' . __('None', 'mch_conf') . '</p>';
                                            ?>
                                            <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>"
                                                 class="waiting edd_cr_loading" style="display:none;"/>
                                        </td>
                                        <td>
                                            <a href="#" class="edd_remove_repeatable" data-type="price"
                                               style="background: url(<?php echo admin_url('/images/xit.gif'); ?>) no-repeat;">&times;</a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td class="submit" colspan="4" style="float: none; clear:both; background:#fff;">
                                        <a class="button-secondary edd_add_repeatable"
                                           style="margin: 6px 0;"><?php _e('Add New Download', 'mch_conf'); ?></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div><input class="btn btn-primary" value="<?php _e('Save', 'mch_conf'); ?>" type="submit"></div>
        </form>
        <?php
    }

    function mch_emailable_downloads()
    {
        if (isset($_POST['emailable_downloads_list']) && !empty($_POST['emailable_downloads_list'])) {
            update_option('emailable_downloads_list', $_POST['emailable_downloads_list']);
            $current_list = get_option('emailable_downloads_list');
        } else {
            $current_list = get_option('emailable_downloads_list');
        }

        $downloads_list = get_downloads_list();
        if ($downloads_list->have_posts()): ?>
            <h3><?php echo __('Select downloads', 'mch_conf'); ?></h3>
            <form method="post">
                <select multiple name="emailable_downloads_list[]">
                    <?php
                    while ($downloads_list->have_posts()): $downloads_list->the_post();
                        $download_id = get_the_ID();
                        $download_title = get_the_title($download_id); ?>
                        <option
                            value="<?php echo $download_id; ?>" <?php selected(true, in_array($download_id, $current_list)); ?>><?php echo $download_title; ?></option>
                        <?php
                    endwhile;
                    ?>
                </select>

                <p>
                    <input type="submit" value="<?php echo __('Save changes', 'mch_conf'); ?>"/>
                </p>
            </form>
            <?php
        endif;

        ?>

        <?php
    }

    /**
     * Check to see if a user has access to some content
     * @param bool $user_id
     * @param string $template_type
     * @return bool
     */

    function mch_user_has_access($user_id = false, $template_type = '')
    {

        $has_access = false;
        $restricted_to = get_option($template_type . '_template_access');

        $current_time = time();

        $trial_period = get_user_meta($user_id, 'hub_ending_trial_time', true);

        // If no user is given, use the current user
        if (!$user_id) {
            $user_id = get_current_user_id();
        }

        // Admins have full access
        if (current_user_can('manage_options')) {
            $has_access = true;
        }

        //If trial time - grant all access
        if ($current_time < $trial_period) {
            $has_access = true;
        }

        $list_allowed_to = maybe_unserialize(get_user_meta($user_id, 'free_access_to_primitive_hub_courses', true));
        if ($restricted_to && !$has_access) {

            foreach ($restricted_to as $item => $data) {

                if (empty($data['download'])) {
                    $has_access = true;
                }

                $expiration_date = get_expiration_date($user_id, $data['download'], $data['price_id']);
                $all_expiration_dates = get_user_and_download_expiration_dates($user_id, $data['download']);


                // If restricted to any customer and user has purchased something
                if ('any' === $data['download'] && edd_has_purchases($user_id) && is_user_logged_in()) {
                    if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                        foreach ($all_expiration_dates as $exp_date) {
                            if ($current_time < $exp_date) {
                                $has_access = true;
                                break;
                            }
                        }
                    } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                        if ($current_time < $all_expiration_dates) {
                            $has_access = true;
                            break;
                        }
                    }
                } elseif ('any' === $data['download']) {
                    if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                        foreach ($all_expiration_dates as $exp_date) {
                            if ($current_time < $exp_date) {
                                $has_access = true;
                                break;
                            }
                        }
                    } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                        if ($current_time < $all_expiration_dates) {
                            $has_access = true;
                            break;
                        }
                    }
                }

                // Check if user has access to download from usermeta
                if (!$has_access && $list_allowed_to) {
                    foreach ($list_allowed_to as $allowed_to) {
                        $allowed_download_id = explode('hub-course-', $allowed_to['download_id']);
                        if ($data['download'] == $allowed_download_id[1] && $data['price_id'] === 'all') {
                            if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                                foreach ($all_expiration_dates as $exp_date) {
                                    if ($current_time < $exp_date) {
                                        $has_access = true;
                                        break;
                                    }
                                }
                            } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                                if ($current_time < $all_expiration_dates) {
                                    $has_access = true;
                                    break;
                                }
                            }
                        } elseif ($data['download'] == $allowed_download_id[1] && $data['price_id'] == $allowed_to['price_id']) {
                            if ($current_time < $expiration_date) {
                                $has_access = true;
                                break;
                            }
                        }
                    }
                }

                // Check for variable prices
                if (!$has_access) {

                    if (edd_has_variable_prices($data['download'])) {
                        if (strtolower($data['price_id']) !== 'all' && !empty($data['price_id'])) {
                            if (edd_has_user_purchased($user_id, $data['download'], $data['price_id'])) {
                                if ($current_time < $expiration_date) {
                                    $has_access = true;
                                }
                            }

                        } else {
                            if (edd_has_user_purchased($user_id, $data['download'])) {
                                if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                                    foreach ($all_expiration_dates as $exp_date) {
                                        if ($current_time < $exp_date) {
                                            $has_access = true;
                                            break;
                                        }
                                    }
                                } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                                    if ($current_time < $all_expiration_dates) {
                                        $has_access = true;
                                    }
                                }
                            }
                        }
                    } else {
                        if (is_user_logged_in() && edd_has_user_purchased($user_id, $data['download'])) {
                            if ($all_expiration_dates && !empty($all_expiration_dates) && is_array($all_expiration_dates)) {
                                foreach ($all_expiration_dates as $exp_date) {
                                    if ($current_time < $exp_date) {
                                        $has_access = true;
                                        break;
                                    }
                                }
                            } elseif ($all_expiration_dates && !empty($all_expiration_dates) && !is_array($all_expiration_dates)) {
                                if ($current_time < $all_expiration_dates) {
                                    $has_access = true;
                                }
                            }
                        }
                    }
                }
            }

        } else {
            // Just in case we're checking something unrestricted...
            $has_access = true;
        }

        return $has_access;
    }
}

/******************************************************************************************************/
/**
 *  In admin side admin can view most recent lesson for all users, who connected with specific school
 */
function mch_most_recent_lesson()
{
    ?>
    <h2><?php echo __('Most recent lesson'); ?></h2>
    <h4><?php echo __('List of schools'); ?></h4>
    <ol class="school-list">
        <?php
        $args = array(
            'blog_id' => $GLOBALS['blog_id'],
            'role' => 'students',
            'orderby' => 'nickname',
            'order' => 'DESC'
        );

        $user_query = new WP_User_Query($args);
        if ($user_query && !empty($user_query->results)) {
            $school_array = array();
            foreach ($user_query->results as $user) {
                $user_id = $user->data->ID;
                $school = get_user_meta($user_id, 'school', true);
                if ($school && !empty($school)) {
                    $school_array[] = $school;
                }
            }
        }
        $clear_school_array = array_unique($school_array);
        foreach ($clear_school_array as $clear_school) {
            ?>
            <li>
                <a href="?page=admin_most_recent_lesson&school=<?php echo $clear_school; ?>"><?php echo $clear_school; ?></a>
            </li>
        <?php } ?>
    </ol>
    <?php
    if (isset($_GET['school']) && !empty($_GET['school'])) {
        $school = $_GET['school'];

        $args = array(
            'blog_id' => $GLOBALS['blog_id'],
            'role' => 'students',
            'meta_key' => 'school',
            'meta_value' => $school,
            'orderby' => 'nickname',
            'order' => 'DESC'
        );

        $second_user_query = new WP_User_Query($args);
        if (isset($second_user_query->results) && !empty($second_user_query->results)) {
            ?>
            <h2 class="recent-lesson-title"><?php echo $school . __(' students'); ?></h2>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?php echo __('Student name'); ?></th>
                    <th><?php echo __('Year level'); ?></th>
                    <th><?php echo __('Course'); ?></th>
                    <th><?php echo __('Module'); ?></th>
                    <th><?php echo __('Lesson'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($second_user_query->results as $user) {

                    $user_id = $user->data->ID;
                    $user_name = $user->data->user_nicename;
                    $last_lesson_id = get_user_meta($user_id, 'most_recent_lesson', true);
                    $last_module_id = get_user_meta($user_id, 'most_recent_module', true);
                    $last_course_id = get_user_meta($user_id, 'most_recent_course', true);
                    $year_level = get_user_meta($user_id, 'year_level', true);
                    $course_object = get_course_by_id($last_course_id);
                    $module_object = get_module_by_id($last_module_id);
                    $unit_object = get_unit_by_unit_id($last_lesson_id);
                    $unit_number = $unit_object->unit_number;
                    $module_number = $module_object->module_number;
                    $course_name = $course_object->course_title;
                    $module_name = $module_object->module_title;
                    $unit_name = get_the_title($last_lesson_id);

                    if ($last_lesson_id && !empty($last_lesson_id)) {
                        ?>
                        <tr>
                            <td><?php echo $user_name; ?></td>
                            <td><?php echo $year_level; ?></td>
                            <td><?php echo $course_name . ' ' . __('(Course #') . $last_course_id . ')'; ?></td>
                            <td><?php echo $module_name . ' ' . __('(Module #') . $module_number . ')'; ?></td>
                            <td><?php echo $unit_name . ' ' . __('(Unit #') . $unit_number . ')'; ?></td>
                        </tr>
                        <?php
                    }

                } ?>
                </tbody>
            </table>
            <?php
        }
    }
}

/**
 * Getting unit object by unit ID
 * @param $unit_id
 *
 * @return bool or object
 */
function get_unit_by_unit_id($unit_id)
{
    global $wpdb, $wpcwdb;

    $unit_sql = $wpdb->prepare("SELECT *
					FROM $wpcwdb->units_meta
					WHERE unit_id = %d", $unit_id);
    $item = $wpdb->get_results($unit_sql);

    if (isset($item[0]) && !empty($item[0])) {
        return $item[0];
    } else {
        return false;
    }
}

if (!function_exists('get_module_by_id')) {
  /**
   * Getting module object by module ID
   * @param $module_id
   *
   * @return bool or object
   */
  function get_module_by_id($module_id) {
    global $wpdb, $wpcwdb;

    $module_sql = $wpdb->prepare("SELECT *
					FROM $wpcwdb->modules
					WHERE module_id = %d
					ORDER BY module_order, module_title ASC
					", $module_id);

    $item = $wpdb->get_results($module_sql);

    if (isset($item[0]) && !empty($item[0])) {
      return $item[0];
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Getting course object by course ID
 * @param $course_id
 *
 * @return bool or object
 */
function get_course_by_id($course_id)
{
    global $wpdb, $wpcwdb;

    $sql = "SELECT * FROM $wpcwdb->courses
	WHERE course_id= $course_id
	ORDER BY course_title";

    $item = $wpdb->get_results($sql);

    if (isset($item[0]) && !empty($item[0])) {
        return $item[0];
    } else {
        return false;
    }
}

/*********************************************************************************/

/**
 * Check for download price variations
 *
 * @return      void
 */
function mch_add_price_variation()
{

    $download_id = absint($_POST['download_id']);
    $key = (isset($_POST['key']) ? $_POST['key'] : 0);

    if (get_post_meta($download_id, '_variable_pricing', true)) {
        $prices = get_post_meta($download_id, 'edd_variable_prices', true);
        $variable_prices = apply_filters('edd_get_variable_prices', $prices, $download_id);
        if ($variable_prices) :?>
            <select class="edd_price_options_select edd-select edd-select edd_cr_download"
                    name="mch_templates_access<?php print $key; ?>[price_id]">
                <option value="all"><?php print esc_html(__('All prices', 'edd_cr')); ?></option>
                <?php foreach ($variable_prices as $price_id => $price) : ?>
                    <option
                        value="<?php print esc_attr($price_id); ?>"><?php print esc_html($price['name']); ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif;
    }

    die();
}

add_action('wp_ajax_mch_add_price_variation', 'mch_add_price_variation');


function mch_get_quizes_data($fullArray = false)
{

    global $wpdb;

    $results = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "wpcw_quizzes");

    $arr[0] = __('-- Select Quiz --', 'wp_courseware');

    if (!$fullArray) {

        foreach ($results as $result) {
            $arr[$result->quiz_id] = __($result->quiz_title);
        }
    } else {
        return $results;
    }
    return $arr;

}

/*******************************************************************************************/

/**
 * All actions
 */


function append_tempate_inputs_script()
{
    wp_enqueue_script('mch-admin-template-inputs', plugins_url('assets/js/input_switcher.js', __FILE__), array('jquery'), '1.0', true);
    // countries select.
    ob_start(); ?>
    <select name="%name" style="width:100%;" class="default-hidden entered-country">
        <option value="All"><?php _e('All'); ?></option>
        <?php foreach (muc_get_country_array() as $country): ?>
            <option value="<?php echo $country ?>">
                <?php echo $country ?>
            </option>
        <?php endforeach; ?>
    </select>
    <?php $countries = ob_get_clean();
    wp_localize_script('mch-admin-template-inputs', 'courseData', array('countries_select' => $countries));
    wp_enqueue_script('script-rename', plugins_url('/js/attachRename.js', __FILE__));
    wp_localize_script('script-rename', 'Ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_action('admin_init', 'append_tempate_inputs_script');

/***********************************************************/

/**
 *
 * LESSONS TEMPLATES ADMIN
 *
 */

/**********************************************************/


/**
 * ******************************   META BOXES   *************************************
 * */


/**
 * the function create metaboxes of the lessons templates
 */

function create_lessons_templates_metaboxes()
{

    global $post;

    if ($post->post_type != 'course_unit') return;

    //Primary School Course Introduction

    add_meta_box(
        'primary-school-course-introduction',
        __('Primary School Course Introduction'),
        'primary_school_course_introduction_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_primary-school-course-introduction', 'add_template_metabox_box_classes');

    //Normal Format - Final Test

    add_meta_box(
        'normal-format-final-test',
        __('Normal Format - Final Test'),
        'normal_format_final_test_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_normal-format-final-test', 'add_template_metabox_box_classes');

    //Primary School Course Format

    add_meta_box(
        'primary-school-course-format',
        __('Primary School Course Format'),
        'primary_school_course_format_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_primary-school-course-format', 'add_template_metabox_box_classes');

    //Primary School Course Format

    add_meta_box(
        'normal-format',
        __('Normal Format'),
        'normal_format_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_normal-format', 'add_template_metabox_box_classes');

    //Primary School Course Format

    add_meta_box(
        'normal-format-introduction',
        __('Normal Format - Introduction'),
        'normal_format_introduction_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_normal-format-introduction', 'add_template_metabox_box_classes');

    add_meta_box(
        'normal-format-end-of-course-exam',
        __('Normal Format - End of course exam'),
        'normal_format_end_of_course_exam_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_normal-format-end-of-course-exam', 'add_template_metabox_box_classes');

    add_meta_box(
        'primary-school-final-test',
        __('Primary School - Final Test'),
        'primary_school_final_test_metabox',
        'course_unit',
        'normal',
        'default'
    );

    add_filter('postbox_classes_course_unit_primary-school-final-test', 'add_template_metabox_box_classes');

}

add_action('add_meta_boxes', 'create_lessons_templates_metaboxes');

/**
 * ***********************************   META BOXES VIEWS     *************************************
 */


/**
 *
 */
function primary_school_final_test_metabox()
{
    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));

    ?>
    <h2><?php _e('Primary School Final Test Links'); ?></h2>

    <input type="hidden" class="hidden_active_template_data" name="unit_templates[primary_school_final_test][active]"
           value="false"/>

    <p>
        <label for=""><?php _e('First Link:'); ?></label></br>
        <input value="<?php echo $data['psh_first_test_url']; ?>" type="text" style="width:30%;"
               placeholder="<?php _e('Enter Your Quiz Link'); ?>"
               name="unit_templates[primary_school_final_test][psh_first_test_url]">
    </p>

    <p>
        <label for=""><?php _e('Second Link:'); ?></label></br>
        <input value="<?php echo $data['psh_second_test_url']; ?>" type="text" style="width:30%;"
               placeholder="<?php _e('Enter Your Quiz Link'); ?>"
               name="unit_templates[primary_school_final_test][psh_second_test_url]">
    </p>

    <?php
}

/**
 * Primary School Course Introduction Metabox View
 */

function primary_school_course_introduction_metabox()
{

    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
    $files = get_attached_media('', $post->ID);

    ?>
    <p><i><?php _e('If you have a text content than you must input it in the main Content Area.'); ?></i></p>

    <h2><?php _e('Video Novel'); ?></h2>
    <p>
        <label for=""><?php _e('Video Title:'); ?></label><br/>
        <input type="text" name="unit_templates[primary_school_course_introduction][video_name]"
               value="<?php echo $data['video_name']; ?>" placeholder="<?php _e('For Example: Video Novel'); ?>"
               style="width:30%;" value="<?php _e('Video Novel'); ?>">
    </p>

    <p>
        <label for=""><?php _e('Youtube / Vimeo Link:'); ?></label><br/>
        <input type="text" name="unit_templates[primary_school_course_introduction][video_link]" style="width:30%;"
               value="<?php echo $data['video_link']; ?>"
               placeholder="<?php _e('For example: http://vimeo.com/14190306'); ?>">
    </p>

    <?php display_downloads('primary_school_course_introduction', $files); ?>

    <h2><?php _e('End of Module Tests'); ?></h2>

    <p>
        <label for=""><?php _e('End of Module Reading and Culture Test:'); ?></label><br/>
        <input type="text" style="width:30%;" name="unit_templates[primary_school_course_introduction][module_link]"
               value="<?php echo $data['module_link']; ?>"
               placeholder="<?php _e('End of Module Reading and Culture Test'); ?>"
               value="">
    </p>
    <p>
        <label for=""><?php _e('End of Module Aural Test:'); ?></label><br/>
        <input type="text" style="width:30%;" name="unit_templates[primary_school_course_introduction][module_link_2]"
               value="<?php echo $data['module_link_2']; ?>"
               placeholder="<?php _e('End of Module Aural Test'); ?>"
               value="">
    </p>

    <input type="hidden" class="hidden_active_template_data"
           name="unit_templates[primary_school_course_introduction][active]" value="false"/>

    <?php

}


/**
 * Normal Format - Final Test Metabox View
 */

function normal_format_final_test_metabox()
{

    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));

    ?>

    <h2><?php _e('Quick Quiz'); ?></h2>

    <input type="hidden" class="hidden_active_template_data" name="unit_templates[normal_format_final_test][active]"
           value="false"/>

    <?php //display_quizes('normal_format_final_test', $quizes, $data);
    ?>

    <p>
        <label for=""><?php _e('Quiz Link:'); ?></label></br>
        <input value="<?php echo $data['quiz_url']; ?>" type="text" style="width:30%;"
               placeholder="<?php _e('Enter Your Quiz Link'); ?>"
               name="unit_templates[normal_format_final_test][quiz_url]">
    </p>

    <?php

}


/**
 * Normal Format - End of course exa, View
 */

function normal_format_end_of_course_exam_metabox()
{

    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));

    ?>

    <h2><?php _e('Quick Quiz'); ?></h2>

    <input type="hidden" class="hidden_active_template_data"
           name="unit_templates[normal_format_end_of_course_exam][active]"
           value="false"/>

    <?php //display_quizes('normal_format_final_test', $quizes, $data);
    ?>

    <p>
        <label for=""><?php _e('Quiz Link:'); ?></label></br>
        <input value="<?php echo $data['quiz_url']; ?>" type="text" style="width:30%;"
               placeholder="<?php _e('Enter Your Quiz Link'); ?>"
               name="unit_templates[normal_format_end_of_course_exam][quiz_url]">
    </p>

    <?php

}


/**
 * Normal Format Metabox Views
 */

function normal_format_metabox()
{

    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
    $quizes = mch_get_quizes_data(true);

    $words = get_post_meta($post->ID, 'vocab_list', true);

    $dialogue = get_post_meta($post->ID, 'unit_template_dialogue', 'true');
    $written_explanations = get_post_meta($post->ID, 'unit_template_written_explanations', 'true');

    ?>

    <input type="hidden" class="hidden_active_template_data" name="unit_templates[normal_format][active]"
           value="false"/>

    <h2><?php _e('Video Explainer'); ?></h2>
    <p>
        <label for=""><?php _e('Video Title:'); ?></label><br/>
        <input type="text" name="unit_templates[normal_format][video_name]" value="<?php echo $data['video_name']; ?>"
               placeholder="<?php _e('For Example: Video Novel'); ?>" style="width:30%;"
               value="<?php echo $data['video_name']; ?>">
    </p>

    <p>
        <label for=""><?php _e('Youtube / Vimeo Link:'); ?></label><br/>
        <input type="text" name="unit_templates[normal_format][video_link]" style="width:30%;"
               value="<?php echo $data['video_link']; ?>"
               placeholder="<?php _e('For example: http://vimeo.com/14190306'); ?>">
    </p>

    <h2><?php _e('Dialogue'); ?></h2>

    <?php wp_editor($dialogue, 'dialogue', $settings = array(
    'textarea_name' => 'unit_templates[normal_format][dialogue]'
)); ?>

    <h2><?php _e('Written Explanations'); ?></h2>

    <?php wp_editor($written_explanations, 'written_explanations', $settings = array(
    'textarea_name' => 'unit_templates[normal_format][written_explanations]'
)); ?>

    <?php display_words_table('normal_format', $words); ?>

    <?php display_quizes('normal_format', $quizes, $data); ?>

    <?php
}

/**
 * Normal Format Introduction
 */

function normal_format_introduction_metabox()
{

    global $post;
    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
    $words = get_post_meta($post->ID, 'vocab_list', true);
    $module_learning_outcomes = get_post_meta($post->ID, 'unit_template_module_learning_outcomes', true);
    ?>

    <input type="hidden" class="hidden_active_template_data" name="unit_templates[normal_format_introduction][active]"
           value="false"/>

    <?php display_words_table('normal_format_introduction', $words); ?>

    <h2><?php _e('Module Learning Outcomes'); ?></h2>

    <?php wp_editor($module_learning_outcomes, 'module-learning-outcomes', $settings = array(
    'textarea_name' => 'unit_templates[normal_format_introduction][module_learning_outcomes]'
)); ?>

    <?php

}

/**
 * Primary School Course Format Metabox View
 */

function primary_school_course_format_metabox()
{

    global $post;

    $data = unserialize(get_post_meta($post->ID, 'unit_template_data', true));
    $quizes = mch_get_quizes_data(true);
    $files = get_attached_media('', $post->ID);
    $interesting_links = get_post_meta($post->ID, 'unit_template_interesting_links', true);


    ?>

    <input type="hidden" class="hidden_active_template_data" name="unit_templates[primary_school_course_format][active]"
           value="false"/>

    <h2><?php _e('Videos Options'); ?></h2>

    <p>
        <label for=""><?php _e('Pronunciation Video'); ?></label>
        <input type="text" name="unit_templates[primary_school_course_format][pr_video_name]"
               placeholder="<?php _e('For example : Pronunciation Video'); ?>"
               value="<?php echo $data['pr_video_name']; ?>">
    </p>

    <p>
        <label for=""><?php _e('Youtube / Vimeo Link'); ?></label>
        <input type="text" name="unit_templates[primary_school_course_format][pr_video_link]"
               placeholder="<?php _e('For example: http://vimeo.com/14190306'); ?>"
               value="<?php echo $data['pr_video_link']; ?>">
    </p>

    <p>
        <label for=""><?php _e('Lesson Recap'); ?></label>
        <input type="text" name="unit_templates[primary_school_course_format][lr_video_name]"
               value="<?php echo $data['lr_video_name']; ?>"
               placeholder="<?php _e('For example: Lesson Recap'); ?>">
    </p>

    <p>
        <label for=""><?php _e('Youtube / Vimeo Link'); ?></label>
        <input type="text" name="unit_templates[primary_school_course_format][lr_video_link]"
               placeholder="<?php _e('For example: http://vimeo.com/14190306'); ?>"
               value="<?php echo $data['lr_video_link']; ?>">
    </p>

    <?php display_downloads('primary_school_course_format', $files); ?>

    <p>
        <button class="button insert-media add_media"><?php _e('Add Download Files'); ?></button>
    </p>

    <h2><?php _e('Interesting Links'); ?></h2>

    <?php wp_editor($interesting_links, 'interesting-links', $settings = array(
    'textarea_name' => 'unit_templates[primary_school_course_format][interesting_links]'
)); ?>

    <?php display_quizes('primary_school_course_format', $quizes, $data); ?>

    <?php

}

/**
 * *****************************************  APPEND SCRIPTS   ******************************************
 */

/**
 * append media button
 */

//function mch_append_template_metaboxes_scripts()
//{
//
//    if (!did_action('wp_enqueue_media')) {
//        wp_enqueue_media();
//    }
//
//    wp_enqueue_style('thickbox'); // call to media files in wp
//    wp_enqueue_script('thickbox');
//    wp_enqueue_script('media-upload');
//
//}
//
//add_action('init', 'mch_append_template_metaboxes_scripts');

add_action('save_post', 'save_unit_templates');

/**
 * Save unit templates data
 */

function save_unit_templates()
{
    global $post;

    $wp_upload_dir = wp_upload_dir();

    if ($post->post_type != 'course_unit' || !$_POST['unit_templates']) return;

    foreach ($_POST['unit_templates'] as $key => $template) {

        if ($template['active'] == 'false') continue;

        unset($template['active']);

        if (isset($template['quiz_id'])) {

            $meta_obj = mch_get_units_meta($post->ID);
            if ($meta_obj) {

                global $wpdb;

                $wpdb->update($wpdb->prefix . 'wpcw_quizzes',
                    array(
                        'parent_course_id' => 0,
                        'parent_unit_id' => 0
                    ),
                    array(
                        'parent_unit_id' => $post->ID
                    )
                );

                $wpdb->update($wpdb->prefix . 'wpcw_quizzes',
                    array(
                        'parent_course_id' => $meta_obj[0]->parent_course_id,
                        'parent_unit_id' => $post->ID,
                    ),
                    array(
                        'quiz_id' => $template['quiz_id'],
                    )
                );

            }

        }

        if (count($template['file_title'])) {

            foreach ($template['file_title'] as $i => $title) {

                $temp_files = array(
                    'name' => $_FILES['unit_templates']['name'][$key]['file'][$i],
                    'tmp_name' => $_FILES['unit_templates']['tmp_name'][$key]['file'][$i],
                    'type' => $_FILES['unit_templates']['type'][$key]['file'][$i],
                    'error' => $_FILES['unit_templates']['error'][$key]['file'][$i],
                    'size' => $_FILES['unit_templates']['size'][$key]['file'][$i],
                );

                $handle = wp_handle_upload($temp_files, array('test_form' => false));

                if (!$handle['error']) {

                    $filename = $handle['file'];

                    $attachment = array(
                        'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
                        'post_mime_type' => $handle['type'],
                        'post_title' => $title,
                        'post_content' => '',
                        'post_status' => 'inherit'
                    );

                    $attach_id = wp_insert_attachment($attachment, $filename, $post->ID);

                    require_once(ABSPATH . 'wp-admin/includes/image.php');

                    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                    wp_update_attachment_metadata($attach_id, $attach_data);
                    // save file country field.
                    update_post_meta($attach_id, 'country', $template['file_country'][$i]);
                }

            }
        }

        if (($key == 'normal_format_introduction' || $key == 'normal_format') && $_POST['words'][$key]) {

            delete_post_meta($post->ID, 'vocab_list');

            foreach ($_POST['words'][$key] as $word) {

                foreach ($word as $key2 => $w) {
                    $final_words[$key2][] = $w;
                }

            }

            add_post_meta($post->ID, 'vocab_list', $final_words, false);

            unset($final_words);

        }

        $template['template_name'] = $_POST['wpcw_units_choose_template_list'];

        if ($template['written_explanations']) {
            update_post_meta($post->ID, 'unit_template_written_explanations', $template['written_explanations']);
            unset($template['written_explanations']);
        }

        if ($template['dialogue']) {
            update_post_meta($post->ID, 'unit_template_dialogue', $template['dialogue']);
            unset($template['dialogue']);
        }

        if ($template['interesting_links']) {
            update_post_meta($post->ID, 'unit_template_interesting_links', $template['interesting_links']);
            unset($template['interesting_links']);
        }

        if ($template['module_learning_outcomes']) {
            update_post_meta($post->ID, 'unit_template_module_learning_outcomes', $template['module_learning_outcomes']);
            unset($template['module_learning_outcomes']);
        }

        update_post_meta($post->ID, 'unit_template_data', maybe_serialize($template));

    }

}

function display_downloads($post_name, $files = NULL)
{
    ?>
    <h2><?php _e('Downloads'); ?>
        <button data="<?php echo $post_name; ?>"
                class="button-secondary add-new-file"><?php _e('Add New Download File'); ?></button>
    </h2>


    <table class="widefat edd_repeatable_table table-files" style="width:50%;">
        <thead>

        <th><?php _e('Title'); ?></th>
        <th><?php _e('File'); ?></th>
        <th><?php _e('Country'); ?></th>
        <th><?php _e('File'); ?></th>
        <th><?php _e('Action'); ?></th>

        </thead>

        <?php foreach ($files as $param => $file): $file_country = get_post_meta($file->ID, 'country', true); ?>
            <tr>

                <td>
                    <span class="file-title"><?php echo $file->post_title; ?></span>
                    <input type="text" class="default-hidden entered-title" value="<?php echo $file->post_title; ?>">
                </td>
                <td class="file-guid"><a target="_blank"
                                         href="<?php echo $file->guid; ?>"><?php echo $file->guid; ?></a></td>
                <td>
                    <span class="file-country"><?php echo $file_country; ?></span>
                    <select class="default-hidden entered-country">
                        <option value="All"><?php _e('All'); ?></option>
                        <?php foreach (muc_get_country_array() as $country): ?>
                            <option value="<?php echo $country ?>" <?php selected($country, $file_country) ?>>
                                <?php echo $country ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td valign="middle"><a href="javascript:void(0)" data="<?php echo $file->ID; ?>"
                                       onclick="delete_file(this);" class="delete_file_link"><?php _e('Delete'); ?></a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="edit-current-file"><? _e('Edit'); ?></a>
                    <a href="javascript:void(0)" class="default-hidden save-current-title"
                       data-fileid="<?php echo $file->ID; ?>">
                        <?php _e('Save'); ?>
                    </a>
                    <span class="default-hidden process-saving"><?php _e('Processing'); ?>...</span>
                </td>

            </tr>

        <?php endforeach; ?>

    </table>
    <?php

}

/**
 * @param $post_name
 * @param $words
 */

function display_words_table($post_name, $words)
{

    ?>

    <h2><?php _e('Module Learning Vocab List'); ?>
        <button data="<?php echo $post_name; ?>"
                class="button-secondary add-new-word"><?php _e('Add New Word'); ?></button>
    </h2>

    <table class="widefat edd_repeatable_table table-words" style="width:40%;">
        <thead>
        <th>English</th>
        <th>Pinyin</th>
        <th>Chinese</th>
        <th><b><?php _e('Video URL'); ?></b></th>
        <th></th>
        </thead>

        <?php if ($words): ?>
            <?php foreach ($words as $word): ?>

                <tr>
                    <td><input type="text" value="<?php echo $word[0]; ?>"
                               name="words[<?php echo $post_name; ?>][english][]" style="width:100%;"/></td>
                    <td><input type="text" value="<?php echo $word[1]; ?>"
                               name="words[<?php echo $post_name; ?>][pinyin][]" style="width:100%;"/></td>
                    <td><input type="text" value="<?php echo $word[2]; ?>"
                               name="words[<?php echo $post_name; ?>][chinese][]" style="width:100%;"/></td>
                    <td><input type="text" value="<?php echo $word[3]; ?>"
                               name="words[<?php echo $post_name; ?>][url][]" style="width:100%;"/></td>
                    <td><a href="javascript:void(0)" onlick="delete_word(this);" class="delete_word_link">Delete</a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php

}

/**
 * @param $post_name
 * @param $quizes
 * @param $data
 */

function display_quizes($post_name, $quizes, $data)
{

    ?>
    <h2><?php _e("Quick Quiz"); ?></h2>
    <?php

    global $post;


    //$quizes = $quizes[0];
    $unitData = mch_get_units_meta($post->ID);

    if (!$quizes):?>
        <p><?php _e('Your Quizes list is empty!'); ?></p>
    <?php elseif ($unitData[0]->parent_module_id && $unitData[0]->parent_course_id): ?>

        <p>
            <label for=""><?php _e('Select your Quiz'); ?></label>
            <select name="unit_templates[<?php echo $post_name; ?>][quiz_id]">
                <option value="0"><?php _e('-- Select Quiz --', 'wp_courseware'); ?></option>
                <?php foreach ($quizes as $quiz): ?>
                    <option <?php selected($post->ID, $quiz->parent_unit_id); ?>
                        value="<?php echo $quiz->quiz_id; ?>"><?php _e($quiz->quiz_title); ?></option>
                <?php endforeach; ?>
            </select>
        </p>

        <?php
    else: ?>

        <p><?php _e('Please, append a Course and a Module to your unit!'); ?></p>

    <?php endif;

}

add_action('post_edit_form_tag', 'post_edit_form_tag');

function post_edit_form_tag()
{
    echo ' enctype="multipart/form-data"';
}

/**
 * Added new class to the templates metaboxes
 * @param array $classes
 * @return array
 */

function add_template_metabox_box_classes($classes = array())
{

    $classes[] = 'templates_metaboxes';

    return $classes;

}

/*************************
 * Ajax delete files from admin panel
 */

function mcn_delete_files_from_admin()
{

    if (wp_delete_attachment($_POST['id'])) {
        echo 1;
    } else {
        echo 0;
    }

    die(); // this is required to terminate immediately and return a proper response
}

add_action('wp_ajax_delete_files', 'mcn_delete_files_from_admin');

/**
 * @param $atts
 */

function mch_show_quiz($atts)
{

    if (!$atts['post_id']) return;

    if (function_exists('WPCW_quizzes_getQuizDetails')) {

        $user_id = get_current_user_id();

        $post = get_post($atts['post_id']);

        $quiz_view = new WPCW_UnitFrontend($post);

        echo $quiz_view->render_quizzes_handleQuizRendering(true);

    }

}

/**
 * @param $unitID
 * @return mixed
 */

function mch_get_units_meta($unitID)
{
    global $wpcwdb, $wpdb;

    // Get a list of all units for this course in absolute order
    $parentCourseID = $wpdb->get_results("
			SELECT *
			FROM $wpcwdb->units_meta
			WHERE unit_id = $unitID
		");

    return $parentCourseID;

}

add_shortcode('mch_show_quiz', 'mch_show_quiz');

add_action('add_meta_boxes', 'subscription_options_box');
/*
 * Meta box for Subscription Options button redirect.
 */
function subscription_options_box()
{
    add_meta_box(
        'subscription_options_box',
        __('Subscription Options', 'mch'),
        'subscription_options_box_content',
        'product-description',
        'normal',
        'high'
    );
}

function subscription_options_box_content($post)
{
    wp_nonce_field(plugin_basename(__FILE__), 'subscription_options_box_content_nonce');

    $args = array(
        'post_type' => 'download'
    );
    $downloads_query = new WP_Query($args);

    if ($downloads_query->have_posts()) : ?>
        <select name="download_id_option">
            <?php while ($downloads_query->have_posts()) : $downloads_query->the_post(); ?>
                <option
                    value="<?php the_ID(); ?>" <?php selected(get_the_ID(), get_post_meta($post->ID, 'download_id_option', true)); ?>><?php the_title(); ?></option>
            <?php endwhile; ?>
        </select>
    <?php endif;
    wp_reset_postdata();
}

add_action('save_post', 'subscription_options_box_save');
function subscription_options_box_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!wp_verify_nonce($_POST['subscription_options_box_content_nonce'], plugin_basename(__FILE__)))
        return;

    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return;
    } else {
        if (!current_user_can('edit_post', $post_id))
            return;
    }
    $download_id_option = $_POST['download_id_option'];
    update_post_meta($post_id, 'download_id_option', $download_id_option);
}


add_shortcode('subscription_options_btn', 'subscription_options_function');
/*
 * Subscription Options button shortcode.
 */
function subscription_options_function($atts)
{
    global $post;
    if (!$atts['title']) {
        $atts['title'] = __('Subscription Options', 'mch');
    }
    $download_id = get_post_meta($post->ID, 'download_id_option', true);
    if ($download_id) {
        $link = get_permalink(edd_get_option('mct_edd_pricing_page_redirect')) . '?update_download_id=' . $download_id;
    } else {
        $link = get_permalink(edd_get_option('mct_edd_pricing_page_redirect'));
    }
    return "<a href='" . $link . "' class='e-btn " . $atts['class'] . "'>" . $atts['title'] . "</a>";
}

add_action('init', 'mch_editor_role_configurations');

function mch_editor_role_configurations()
{
    if (current_user_can('delete_others_pages')) {
        show_admin_bar(true);
        add_filter('wpcw_back_menus_access_training_courses', 'mch_delete_others_pages', 10000);
    }
}

function mch_delete_others_pages()
{
    return 'delete_others_pages';
}

function ajax_save_file_title()
{
    if (isset($_POST['file_id']) && !empty($_POST['file_id'])) {
        $file_id = $_POST['file_id'];
        $file = get_post($file_id);

        if (isset($file) && !empty($file)) {
            if (isset($_POST['entered_title']) && !empty($_POST['entered_title'])) {
                $entered_title = $_POST['entered_title'];
                $entered_title = str_replace('\\', '', $entered_title);
                $file->post_title = $entered_title;
                wp_update_post($file);
                // save new country.
                $entered_country = $_POST['entered_country'];
                update_post_meta($file_id, 'country', $entered_country);
                $entered_country = get_post_meta($file_id, 'country', true)?>
                <span class="file-title"><?php echo $file->post_title; ?></span>
                <input type="text" class="default-hidden entered-title" value="<?php echo $file->post_title; ?>">
                <span class="file-country"><?php echo $entered_country; ?></span>
                <input type="text" class="default-hidden entered-country" value="<?php echo $entered_country; ?>">
                <?php
            }
        }
    }
    exit;
}

add_action('wp_ajax_ajax_save_file_title', 'ajax_save_file_title');

/**
 * Insert survey lesson
 * @param array $arg in this format:
 *
 *   array(
 *       'pathway' => $pathway,
 *       'module' => $module,
 *       'lesson' => $lesson,
 *       'lesson_id' => $lesson_id,
 *       'chinese_teacher_id' => $chinese_teacher_id,
 *       'classroom_teacher_id' => $classroom_teacher_id,
 *       'technology_rating' => $technology_rating,
 *       'ct_rating' => $ct_rating,
 *       'survey_date' => $survey_date
 *   );
 *
 */
function insert_survey_lesson(array $arg) {
    global $wpdb;

    $table_name = $wpdb->prefix . "lesson_survey";
    
    $insert_data =  array(
            'pathway' => $arg['pathway'],
            'module' => $arg['module'],
            'lesson' => $arg['lesson'],
            'lesson_id' => $arg['lesson_id'],
            'chinese_teacher_id' => $arg['chinese_teacher_id'],
            'classroom_teacher_id' => $arg['classroom_teacher_id'],
            'technology_rating' => $arg['technology_rating'],
            'ct_rating' => $arg['ct_rating'],
            'survey_date' => $arg['survey_date'],
    );
    $insert_args = array('%d', '%d', '%s', '%d', '%d', '%d', '%d', '%d', '%d');
        
    if (isset($arg['is_cancelled'])) {
        $insert_data['is_cancelled'] = 1;
        $insert_data['comment'] = $arg['comment'];
        $insert_data['star_rating'] = $arg['star_rating'];
        $insert_args = array_merge($insert_args, array('%d', '%s', '%d'));
    } else {
        // set previous survey unavailable for evaluating
        $wpdb->query("UPDATE $table_name SET is_rated=NULL WHERE is_rated=0 AND is_cancelled=0 ORDER BY survey_id DESC LIMIT 1");
    }

    // Insert current survey
    $wpdb->insert(
        $table_name,
        $insert_data,
        $insert_args
    );
}

/**
 * Ajax get school list by location on the Teacher’s portal
 */

function mch_location_schools()
{

	if (!empty($_POST['location'])) {
		$location = trim($_POST['location']);
		$current_teacher  = get_current_user_id();
		$location_schools = array();

		$args = array(
			'blog_id'  => $GLOBALS['blog_id'],
			'role'     => 'students',
			'orderby'  => 'user_nicename',
			'order'    => 'ASC',
			'meta_key' => 'location',
			'meta_value' => $location,
		);

		$user_query = new WP_User_Query( $args );
		if ( isset( $user_query->results ) && ! empty( $user_query->results ) ) {

			$school_array   = array();
			$location_array = array();

			foreach ( $user_query->results as $user ) {
				$user_id  = $user->data->ID;
				$school   = get_user_meta( $user_id, 'school', TRUE );

				if ( ! empty( $school ) && ! empty( $location ) ) {
					$school_array[] = $school;
				}
			}
		}
		?>
        <?php
			ob_start();
            if ($school_array && !empty($school_array)) {
                $school_array = array_unique($school_array);
                foreach ($school_array as $school_name) {
	                ?>
	                <option value="<?php echo $school_name; ?>" ><?php echo $school_name; ?></option>
                <?php
                }
            }
			echo ob_get_clean();
        ?>
	<?php
	}
	die(); // this is required to terminate immediately and return a proper response
}

add_action('wp_ajax_mch_location_schools', 'mch_location_schools');

/**
 * Attach lesson files.
 * @param array $files
 * @param PHPMailer $Letter
 * @param int $user_id
 */
function mch_set_attachments($files, PHPMailer $Letter, $user_id = null) {
    if ($user_id == null) {
        $user = get_current_user_id();
    }
    $user_country = get_user_meta($user_id, MUC_COUNTRY_META_KEY, true);
    if ($files && !empty($files)) {
        foreach ($files as $file) {
            if (isset($file->guid) && !empty($file->guid)) {
                $download_file = get_attached_file($file->ID);
                if ($download_file && !empty($download_file)) {
                    $file_country = get_post_meta($file->ID, 'country', true);
                    if ($user_country == $file_country || $file_country == 'All' || empty($file_country)) {
                        $Letter->addAttachment($download_file);
                    }
                }
            }
        }
    }
}