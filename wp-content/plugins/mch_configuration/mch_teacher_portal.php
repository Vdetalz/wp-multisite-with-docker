<?php
$plugin_dir = plugin_dir_path(__FILE__);
$plugin_dir = str_replace('\\', '/', $plugin_dir);
include($plugin_dir . 'PushNotifications.php');

add_shortcode('teacher_portal', 'filter_select_shortcode');

/**
 * Shortcode for Teacher Portal page
 * @return string
 */
function filter_select_shortcode()
{
    global $wpdb, $wpcwdb;


    /**
     * Start updating user
     */

    if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
        if (isset($_POST['input_user_first_name']) && !empty($_POSt['input_user_first_name']) ||
            isset($_POST['input_user_last_name']) && !empty($_POSt['input_user_last_name']) ||
            isset($_POST['input_user_email']) && !empty($_POSt['input_user_email'])
        ) {
            wp_update_user(array(
                'ID' => $_POST['user_id'],
                'first_name' => $_POST['input_user_first_name'],
                'last_name' => $_POST['input_user_last_name'],
                'user_email' => $_POST['input_user_email']
            ));
        }

        if (isset($_POST['input_user_level']) && !empty($_POST['input_user_level'])) {
            update_user_meta($_POST['user_id'], 'year_level', $_POST['input_user_level']);
        }
        if (isset($_POST['input_user_skype']) && !empty($_POST['input_user_skype'])) {
            update_user_meta($_POST['user_id'], 'skype_name', $_POST['input_user_skype']);
        }
        if (isset($_POST['input_user_university']) && !empty($_POST['input_user_university'])) {
            update_user_meta($_POST['user_id'], 'school', $_POST['input_user_university']);
        }
        if (isset($_POST['input_user_location']) && !empty($_POST['input_user_location'])) {
            update_user_meta($_POST['user_id'], 'location', $_POST['input_user_location']);
        }
    }

    $args = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => 'students',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );

    $current_teacher = get_current_user_id();
	$location_schools = array();

    $user_query = new WP_User_Query($args);
    if (isset($user_query->results) && !empty($user_query->results)) {

        $school_array = array();
        $location_array = array();

        foreach ($user_query->results as $user) {
            $user_id = $user->data->ID;
            $school = get_user_meta($user_id, 'school', true);
            $location = get_user_meta($user_id, 'location', true);

            if ($location && !empty($location)) {
                $location_array[] = $location;
            }

	        if (!empty($school) && !empty($location)) {
		        $school_array[$location][] = $school;
	        }
        }
	    sort($location_array);

        $get_location_name = stripcslashes( $_GET['location_name'] );
        $get_school_name = stripcslashes( $_GET['school_name'] );
	    // Get school list for current location
	    if(!empty($_GET['location_name'])){
		    $location_schools = $school_array[$get_location_name];
	    }elseif(!empty($school_array[$location_array[0]])){
		    $location_schools = $school_array[$location_array[0]];
	    }
	    sort($location_schools);

        ob_start();
        ?>
        <form method="get" class="selection-students">

        <ul class="students-accessory">
            <li
                class="my-students <?php if (isset($_GET['students_accessory']) && $_GET['students_accessory'] != 0) echo 'active'; ?>"><?php echo __('My students'); ?></li>
            <?php echo '/'; ?>
            <li
                class="all-students <?php if (isset($_GET['students_accessory']) && $_GET['students_accessory'] == 0) echo 'active'; ?>"><?php echo __('All students'); ?></li>
        </ul>
        <input type="hidden" name="teacher_id" value="<?php echo $current_teacher; ?>"
               class="current-teacher-value"/>
        <input type="hidden" value="" name="students_accessory" class="student-accessory-value"/>
        <div class="search-students">
            <select name="location_name">
                <?php
                if ($location_array && !empty($location_array)) {
                    $location_array = array_unique($location_array);
                    foreach ($location_array as $location_name) {
                        ?>
                        <option
                            value="<?php echo $location_name; ?>" <?php selected($get_location_name, $location_name); ?>><?php echo $location_name; ?></option>
                    <?php
                    }
                }
                ?>
            </select>
	        <?php if ($location_schools && !empty($location_schools)) {?>
	            <select name="school_name" id="location-scools">
	                <?php
		                $location_schools = array_unique($location_schools);
	                    foreach ($location_schools as $school_name) {
	                        ?>
	                        <option
	                            value="<?php echo $school_name; ?>" <?php selected($get_school_name, $school_name); ?>><?php echo $school_name; ?></option>
	                    <?php
	                    }
	                ?>
	            </select>
	        <?php } ?>
            <input type="submit" class="e-btn" value="<?php echo __('Search'); ?>"/>
        </div>

        <?php

        if ($get_school_name && !empty($get_school_name)) {
            $school_arg = $get_school_name;
        }

        if ($get_location_name && !empty($location_name)) {
            $location_arg = $get_location_name;
        }

        $user_list_args = array(
            'blog_id' => $GLOBALS['blog_id'],
            'role' => 'students',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'location',
                    'value' => $location_arg
                ),
                array(
                    'key' => 'school',
                    'value' => $school_arg,
                ),
            ),
            'orderby' => 'user_nicename',
            'order' => 'ASC'
        );

        if (isset($_GET['students_sort']) && !empty($_GET['students_sort'])) {
            $sort = $_GET['students_sort'];
        } else {
            $sort = 'desc';
        }

        $user_list_query = new WP_User_Query($user_list_args);
        if (!$user_list_query->results || empty($user_list_query->results)) {
            ?>
            <?php if (isset($_GET['students_accessory']) && !empty($_GET['students_accessory'])): ?>
                <div class="count-user">
                    <?php echo __(' No students founded:'); ?>
                </div>
            <?php endif; ?>
        <?php
        }
        if ($user_list_query->results && !empty($user_list_query->results)) {
            $users = $user_list_query->results;
            $new_user_list = array();
            if (isset($_GET['students_accessory']) && !empty($_GET['students_accessory']) || ($_GET['students_accessory'] == 0 && !is_null($_GET['students_accessory']))) {

                if ($_GET['students_accessory'] != 0) {
                    foreach ($users as $user) {
                        $user_id = $user->data->ID;
                        $teacher_id = $_GET['students_accessory'];
                        $table_name = $wpdb->prefix . "teachers_user_list";

                        $sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " AND author_id=" . $teacher_id . " ORDER BY complete_date " . $sort, ARRAY_A);
                        if ($sql && !empty($sql)) {
                            $new_user_list[] = $user;
                        }
                    }
                } else {
                    $new_user_list = $users;
                }
            }

            $table_name = $wpdb->prefix . "teachers_user_list";
            $sort_user_list_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " ORDER BY complete_date " . $sort, ARRAY_A);
            $new_users = array();
            $new_users_id = array();
            foreach ($sort_user_list_sql as $new_user) {
                $new_users_id[] = $new_user['subscriber_id'];
            }

            foreach ($new_user_list as $user) {
                if (!in_array($user->data->ID, $new_users_id)) {
                    $new_users_id[] = $user->data->ID;
                }
            }

            $new_users_id = array_unique($new_users_id);
            foreach ($new_users_id as $new_user) {
                foreach ($new_user_list as $user) {
                    if ($new_user == $user->data->ID) {
                        $new_users[] = $user;
                        continue 2;
                    }
                }
            }

            if ($new_users && !empty($new_users)) {
                $users_count = count($new_users);
            }
            ?>

            <div class="count-user">
                <?php if ($users_count && !empty($users_count)) { ?>
                    <?php echo $users_count . __(' students are found:'); ?>
                <?php
                } else {
                    if (isset($_GET['students_accessory']) && !empty($_GET['students_accessory'])):?>
                        <?php echo __(' No students founded:'); ?>
                    <?php endif;
                }
                ?>
            </div>

            <div class="students-sort select-without-border">
            <?php if ($users_count > 1): ?>
                <label for="students_sort"><?php echo __(' Sort by: '); ?></label>
                <select name="students_sort" id="students_sort">
                    <option
                        value="desc" <?php selected($_GET['students_sort'], 'desc'); ?>><?php echo __('Previous lessons completed'); ?></option>
                    <option
                        value="asc" <?php selected($_GET['students_sort'], 'asc'); ?>><?php echo __('Previous lessons completed (back order)'); ?></option>
                </select>
                </div>
            <?php endif; ?>
            </form>
            <?php if ($users_count && !empty($users_count)) { ?>
                <div class="students-list-wrapper">

                <?php

                $course_list = WPCW_courses_getCourseList();

                $k = 0; //It's all is counters
                $j = 1;
                $people_page_counter = 0;
                $page_counter = 0;

                foreach ($new_users as $user) {
                    $user_id = $user->data->ID;
                    $user_first_name = get_user_meta($user_id, 'first_name', true);
                    $user_last_name = get_user_meta($user_id, 'last_name', true);
                    $user_skype = get_user_meta($user_id, 'skype_name', true);
                    $user_level = get_user_meta($user_id, 'year_level', true);
                    $user_email = $user->data->user_email;
                    $teacher_id = get_current_user_id();
                    $user_avatar = get_avatar($user_id);

                    $table_name = $wpdb->prefix . "teachers_user_list";
                    $time_query = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " ORDER BY complete_date DESC LIMIT 1", OBJECT);
                    if (isset($time_query[0]) && !empty($time_query[0])) {
                        $last_time = $time_query[0]->complete_date;
                    }

                    $time_from_last_lesson = time_elapsed_string($last_time);

                    ?>
                    <?php if ($people_page_counter == 0) { //if ($people_page_counter % 5 == 0) { ?>
                        <div class="people-page" id="<?php echo $people_page_counter; ?>" data-page="<?php echo $page_counter + 1; ?>">
                    <?php } ?>

                    <div class="single-student-wrapper">
                    <form method="post" action="">
                        <input type="hidden" value="<?php echo $user_id; ?>" name="user_id" class="user-id"
                               id="student-<?php echo $j; ?>">
                        <input type="hidden" value="<?php echo $teacher_id; ?>" name="teacher_id" class="teacher-id"
                               id="student-<?php echo $j; ?>">

                        <div class="top-student-info">
                            <div class="student-photo">
                                <?php echo $user_avatar; ?>
                            </div>
                            <div class="student-info">
                                <div class="student-name">
                                    <span
                                        class="edit-hide"><?php echo $user_first_name . ' ' . $user_last_name; ?></span>
                                    <span class="last-change"><?php echo $time_from_last_lesson; ?></span>

                                    <div class="ac-row">
                                        <div class="user-account-title">

                                            <span class="edit-show"
                                                  style="display: none"><?php echo __('First name '); ?></span>
                                        </div>
                                        <div class="right-block">
                                            <input type="text" name="input_user_first_name" style="display: none"
                                                   value="<?php echo $user_first_name; ?>" class="edit-show">
                                        </div>
                                    </div>

                                    <div class="ac-row">
                                        <div class="user-account-title">
                                            <span class="edit-show"
                                                  style="display: none"><?php echo __('Last name '); ?></span>
                                        </div>
                                        <div class="right-block">
                                            <input type="text" name="input_user_last_name" style="display: none"
                                                   value="<?php echo $user_last_name; ?>" class="edit-show">
                                        </div>
                                    </div>
                                </div>
                                <div class="student-email">
                                    <span class="edit-hide"><?php echo $user_email; ?></span>
                                </div>
                                <div class="student-level">
                                    <div class="ac-row">
                                        <div class="user-account-title">
                                            <?php echo __('Level '); ?><span
                                                class="edit-hide"><?php echo $user_level; ?></span>
                                        </div>
                                        <div class="right-block">
                                            <input type="text" class="edit-show" name="input_user_level"
                                                   style="display: none" value="<?php echo $user_level; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="student-university">
                                    <div class="ac-row">
                                        <div class="user-account-title">
                                            <span class="edit-hide school_arg_color"><?php echo $school_arg; ?></span>
                                            <span class="edit-show"
                                                  style="display: none"><?php echo __('School '); ?></span>
                                        </div>
                                        <div class="right-block">
                                            <input type="text" name="input_user_university" style="display: none"
                                                   value="<?php echo $school_arg; ?>" class="edit-show">
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="open-info">Show</div>
                        </div>

                        <div class="hiding-part">
                            <div class="bottom-student-info">
                                <div class="student-info">
                                    <div class="student-location">
                                        <div class="ac-row">
                                            <div class="user-account-title">
                                                <span class="edit-hide"><?php echo __('Location: '); ?></span>
                                                <span class="edit-show"
                                                      style="display: none"><?php echo __('Location '); ?></span>
                                            </div>
                                            <div class="right-block">
                                                <span class="edit-hide"><?php echo $location_arg; ?></span>
                                                <input type="text" name="input_user_location" style="display: none"
                                                       value="<?php echo $location_arg; ?>" class="edit-show">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="student-skype">
                                        <div class="ac-row">
                                            <div class="user-account-title">
                                                <?php echo __('Skype: '); ?>
                                            </div>
                                            <div class="right-block">
                                                <?php
                                                if (isset($user_skype) && !empty($user_skype)) {
                                                    ?>
                                                    <span class="edit-hide"><?php echo $user_skype; ?></span>
                                                    <input type="text" name="input_user_skype" style="display: none"
                                                           value="<?php echo $user_skype; ?>" class="edit-show">
                                                <?php } else { ?>
                                                    <input type="text" name="input_user_skype" style="display: none"
                                                           value="" class="edit-show">
                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="student-inner-email">
                                        <div class="ac-row">
                                            <div class="user-account-title">
                                                <?php echo __('E-mail: ') ?>
                                            </div>
                                            <div class="right-block">
                                                <span
                                                    class="edit-hide"><?php echo $user_email; //Email is doubling (for remember) ?></span>
                                                <input type="text" name="input_user_email" style="display: none"
                                                       value="<?php echo $user_email; ?>" class="edit-show">
                                            </div>
                                        </div>


                                    </div>
                                    <span class="user-edition" id="student-<?php echo $j; ?>">Edit</span>
                                    <input type="submit" class="edit-submit-show student-account-edit save-prof-button"
                                           style="display: none" value="save profile">

                                    <div class="student-details">
                                        <?php echo __('About student: ', 'mch'); ?>
                                        <textarea class="student-details" name="student-details"
                                                  data-student-id="<?php echo $user->ID; ?>"><?php echo get_user_meta($user->ID, 'student_about', true); ?></textarea>
                                        <input type="button" class="student-about-save"
                                               value="<?php _e('Save', 'mch') ?>"
                                               data-user-id="<?php echo $user->ID; ?>"/>
                                    </div>
                                </div>
                    </form>

                    <div class="lesson-history-wrapper">
                        <span class="lesson-history-topic"><?php echo __('Lesson history: '); ?></span>

                        <div class="lesson-history-buttons">
                            <div class="lessons-loop" id="student-<?php echo $j; ?>">
                                <?php
                                /**
                                 * Lesson history part
                                 */
                                $table_name = $wpdb->prefix . "teachers_user_list";
                                $history_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " ORDER BY complete_date DESC LIMIT 3", OBJECT);
                                $i = 0;
                                foreach ($history_sql as $history_record) {
                                    $history_lesson_completed_time = $history_record->complete_date;
                                    $history_teacher_id = $history_record->author_id;
                                    $teacher_first_name = get_user_meta($history_teacher_id, 'first_name', true);
                                    $teacher_last_name = get_user_meta($history_teacher_id, 'last_name', true);
                                    $history_course_id = $history_record->course_level_id;
                                    $history_module_id = $history_record->module_id;
                                    $history_lesson_id = $history_record->lesson_id;
                                    $history_comment = $history_record->comment;
	                                $history_comment .= isset( $history_record->cancel_comment ) ? ' ' . $history_record->cancel_comment : '';

                                    $normal_date = date('d/m/y', $history_lesson_completed_time);
                                    $history_date = date('d/m/y H:i', $history_lesson_completed_time);

                                    $course_name = WPCW_courses_getCourseDetails($history_course_id)->course_title;
                                    $module_name = WPCW_modules_getModuleDetails($history_module_id)->module_title;
                                    $lesson_name = get_the_title($history_lesson_id);
                                    ?>
                                    <div class="history-wrapper">
                                        <div class="history-item-wrapper" id="<?php if ($j > 1) {
                                            echo "a-$j-$i";
                                        } else {
                                            echo $i;
                                        } ?>">
                                            <?php echo $normal_date; ?>
                                            <input type="hidden" value="<?php echo $history_lesson_completed_time; ?>"/>
                                        </div>
                                        <div class="history-popup-box" id="popup-box-<?php if ($j > 1) {
                                            echo "a-$j-$i";
                                        } else {
                                            echo $i;
                                        } ?>">
                                            <div class="inner-history-popup">
                                                <div class="close">X</div>
                                                <h2><?php echo __('Lesson history: '); ?></h2>

                                                <div class="history-popup-content">
                                                    <div class="history_course_id item-history">
                                                        <span><?php echo __('Course: ') ?></span><?php echo $course_name; ?>
                                                    </div>
                                                    <div class="history_module_id item-history">
                                                        <span><?php echo __('Module: ') ?></span><?php echo $module_name; ?>
                                                    </div>
                                                    <div class="history_lesson_id item-history">
                                                        <span><?php echo __('Lesson: ') ?></span><?php echo $lesson_name; ?>
                                                    </div>
                                                    <div class="history-comment item-history">
                                                        <span><?php echo __('Comment: ') ?></span><?php echo $history_comment; ?>
                                                    </div>
                                                    <div class="history-complete-lesson-time item-history">
                                                        <span><?php echo __('Lesson complete date:') ?></span><?php echo $history_date; ?>
                                                    </div>
                                                    <div class="history-teacher-name item-history">
                                                        <span><?php echo __('Teacher: ') ?></span><?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $i++;
                                }
                                ?>
                            </div>
                            <div class="all-complete-lessons-wrapper" id="<?php echo $j; ?>">
                                <div class="show-full-history-button">
                                    <input type="button" class="show-all-lessons-button e-btn-show-lesson"
                                           value="Show all lessons"
                                           id="full-history-<?php echo $j; ?>"/>
                                </div>
                                <div class="full-history-popup-box" id="popup-box-full-history-<?php echo $j; ?>">
                                    <div class="inner-history-popup">
                                        <div class="close">X</div>
                                        <h2><?php echo __('Lesson history: '); ?></h2>

                                        <div class="history-popup-content">
                                            <?php
                                            $table_name = $wpdb->prefix . "teachers_user_list";
                                            $full_history_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " ORDER BY complete_date DESC", OBJECT);
                                            $i = 1;
                                            foreach ($full_history_sql as $full_history_record) {


                                                $history_lesson_completed_time = $full_history_record->complete_date;
                                                $history_teacher_id = $full_history_record->author_id;
                                                $teacher_first_name = get_user_meta($history_teacher_id, 'first_name', true);
                                                $teacher_last_name = get_user_meta($history_teacher_id, 'last_name', true);
                                                $history_course_id = $full_history_record->course_level_id;
                                                $history_module_id = $full_history_record->module_id;
                                                $history_lesson_id = $full_history_record->lesson_id;
                                                $history_comment = $full_history_record->comment;
                                                $normal_date = date('d/m/y', $history_lesson_completed_time);
                                                $history_date = date('d/m/y H:i', $history_lesson_completed_time);

                                                $course_name = WPCW_courses_getCourseDetails($history_course_id)->course_title;
                                                $module_name = WPCW_modules_getModuleDetails($history_module_id)->module_title;
                                                $lesson_name = get_the_title($history_lesson_id);
                                                ?>

                                                <div class="single-history-item">
                                                    <div class="single-course-name item-history">
                                                        <span><?php echo __('Course name: ') ?></span><?php echo $course_name; ?>
                                                    </div>
                                                    <div class="single-module-name item-history">
                                                        <span><?php echo __('Module name: ') ?></span><?php echo $module_name; ?>
                                                    </div>
                                                    <div class="single-lesson-name item-history">
                                                        <span><?php echo __('Lesson name: ') ?></span><?php echo $lesson_name; ?>
                                                    </div>
                                                    <div class="single-comment item-history">
                                                        <span><?php echo __('Teacher\'s comment: ') ?></span><?php echo $history_comment; ?>
                                                    </div>
                                                    <div class="single-lesson-complete_data item-history">
                                                        <span><?php echo __('Lesson completed at: ') ?></span><?php echo $history_date; ?>
                                                    </div>
                                                    <div class="single-teacher-name item-history">
                                                        <span><?php echo __('Teacher name: ') ?></span><?php echo $teacher_first_name . $teacher_last_name; ?>
                                                    </div>
                                                </div>

                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="lesson-number-block-wrapper">
                        <div class="wrap-select-block">
                            <div class="title-block"><?php echo __('Pathway/level'); ?></div>
                            <div class="path-level-wrapper select-block">
                                <select class="path-level-select" name="path-level-select"
                                        id="student-<?php echo $j; ?>">
                                    <?php
                                    /**
                                     * Path/level drop-down selector
                                     */
                                    $course_number = 1;
                                    foreach ($course_list as $course) {

                                        $sql = "SELECT *
                                        FROM $wpcwdb->courses
                                        WHERE course_title= '$course'
                                        AND course_group_id = '16'
                                        ORDER BY course_title";

                                        $items = $wpdb->get_results($sql);

                                        foreach ($items as $item) {
                                            $id = $item->course_id;?>
                                            <option value="<?php echo $id; ?>" data-path-number="<?php echo $course_number++; ?>"><?php echo $course; ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="wrap-select-block">
                            <div class="title-block"><?php echo __('Module: '); ?></div>
                            <div class="module-id-wrapper select-block">
                                <div class="module-removed-part" id="student-<?php echo $j; ?>">
                                    <select class="module-select" name="module-select" id="student-<?php echo $j; ?>">
                                        <?php
                                        /**
                                         * Module drop-sown selector
                                         */
                                        $sql = "SELECT *
                                            FROM $wpcwdb->courses
                                            WHERE course_group_id = '16'
                                            ORDER BY course_title";

                                        $course_item = $wpdb->get_results($sql);

                                        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
                                        $i = 1;
                                        foreach ($module_details as $module) {
                                            $id = $i; ?>
                                            <option
                                                value="<?php echo $module->module_id; ?>"><?php echo $id; ?></option>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="wrap-select-block m-fr">
                            <div class="title-block"><?php echo __('Lesson: '); ?></div>
                            <div class="select-block">
                                <div class="lesson-removed-part" id="student-<?php echo $j; ?>">
                                    <select class="lesson-select" name="lesson-select" id="student-<?php echo $j; ?>">
                                        <?php
                                        /**
                                         * Lesson drop-down selector
                                         */
                                        $sql = "SELECT *
                                                FROM $wpcwdb->courses
                                                WHERE course_group_id = '16'
                                                ORDER BY course_title";

                                        $course_item = $wpdb->get_results($sql);

                                        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
                                        foreach ($module_details as $module) {
                                            if (reset($module_details) == $module) {
                                                $units_list = WPCW_units_getListOfUnits($module->module_id);
                                                $i = 1;
                                                foreach ($units_list as $unit) {
                                                    $unit_metadata = get_post_meta($unit->ID, 'unit_template_data');
                                                    $massive = maybe_unserialize($unit_metadata[0]);

                                                    $id = $i; ?>
                                                    <?php if ($massive['template_name'] == 'template-primary-school-course-introduction-page.php') { ?>
                                                        <option
                                                            value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                                                    <? } else { ?>
                                                        <option
                                                            value="<?php echo $unit->ID; ?>"><?php echo $id; ?></option>
                                                        <?php $i++;
                                                    }
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="plus-button-wrapper">
                                    <span class="aj-loader"><img
                                            src="<?php echo plugins_url('img/loader.gif', __FILE__); ?>"></span>
                                    <span class="aj-accept"><img
                                            src="<?php echo plugins_url('img/accept.png', __FILE__); ?>"></span>
                                    <input type="button" class="e-btn-plus plus-button" value="+"
                                           id="student-<?php echo $j; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="survey-rating">
                        <?php $stars_options = get_stars_options(); ?>
                        <div class="wrap-select-block m-fr">
                            <div class="title-block"><?php echo __('Technology'); ?></div>
                            <div class="select-block">
                                <select name="technology_rating" class="technology-rating" id="student-<?php echo $j; ?>">
                                    <?php foreach($stars_options as $key => $option): ?>
                                        <option value="<?php echo $key ?>" <?php selected(5, $key, true); ?>><?php echo $option; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="wrap-select-block m-fr">
                            <div class="title-block"><?php echo __('Classroom Teacher Quality'); ?></div>
                            <div class="select-block">
                                <select name="ct_rating" class="ct-rating" id="student-<?php echo $j; ?>">
                                    <?php foreach($stars_options as $key => $option): ?>
                                        <option value="<?php echo $key ?>" <?php selected(5, $key, true); ?>><?php echo $option; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="lesson-comment-wrapper">
                        <textarea name="lesson-comment" class="lesson-comment" cols="30" rows="6"
                                  id="student-<?php echo $j; ?>"></textarea>
                    </div>

                    <?php
                    /**
                     * Starts next lesson section
                     */

                    ?>

                    <div class="next-lesson-number-block-wrapper" id="student-<?php echo $j; ?>">
                        <div class="wrap-select-block">
                            <div class="title-block"><?php echo __('Pathway/level'); ?></div>
                            <div class="next-path-level-wrapper select-block">
                                <select class="next-path-level-select" name="next-path-level-select"
                                        id="student-<?php echo $j; ?>">
                                    <?php
                                    /**
                                     * Path/level drop-down selector
                                     */
                                    $course_number = 1;
                                    foreach ($course_list as $course) {

                                        $sql = "SELECT *
                                        FROM $wpcwdb->courses
                                        WHERE course_title= '$course'
                                        AND course_group_id = '16'
                                        ORDER BY course_title";

                                        $items = $wpdb->get_results($sql);

                                        foreach ($items as $item) {
                                            $id = $item->course_id; ?>
                                            <option value="<?php echo $id; ?>" data-path-number="<?php echo $course_number++; ?>"><?php echo $course; ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="wrap-select-block">
                            <div class="title-block"><?php echo __('Module: '); ?></div>
                            <div class="next-module-id-wrapper select-block">
                                <div class="next-module-removed-part" id="student-<?php echo $j; ?>">
                                    <select class="next-module-select" name="next-module-select"
                                            id="student-<?php echo $j; ?>">
                                        <?php
                                        /**
                                         * Module drop-sown selector
                                         */
                                        $sql = "SELECT *
                                            FROM $wpcwdb->courses
                                            WHERE course_group_id = '16'
                                            ORDER BY course_title
                                            LIMIT 1";

                                        $course_item = $wpdb->get_results($sql);

                                        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
                                        $i = 1;
                                        foreach ($module_details as $module) {
                                            $id = $i; ?>
                                            <option
                                                value="<?php echo $module->module_id; ?>"><?php echo $id; ?></option>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="wrap-select-block m-fr">
                            <div class="title-block"><?php echo __('Lesson: '); ?></div>
                            <div class="next-lesson-id-wrapper select-block">
                                <div class="next-lesson-removed-part select-block" id="student-<?php echo $j; ?>">
                                    <select class="next-lesson-select" name="next-lesson-select"
                                            id="student-<?php echo $j; ?>">
                                        <?php
                                        /**
                                         * Lesson drop-down selector
                                         */
                                        $sql = "SELECT *
                                            FROM $wpcwdb->courses
                                            WHERE course_group_id = '16'
                                            ORDER BY course_title
                                            LIMIT 1";

                                        $course_item = $wpdb->get_results($sql);

                                        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
                                        foreach ($module_details as $module) {
                                            if (reset($module_details) == $module) {
                                                $units_list = WPCW_units_getListOfUnits($module->module_id);
                                                $i = 1;
                                                foreach ($units_list as $unit) {
                                                    ?>
                                                    <?php if ($unit == reset($units_list)) { ?>
                                                        <option
                                                            value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                                                    <?php } else { ?>
                                                        <option
                                                            value="<?php echo $unit->ID; ?>"><?php echo $i; ?></option>
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- START CANCELLATION BUTTON -->

                    <div class="cancel-button-wrapper" id="student-<?php echo $j; ?>">
                        <div class="cancel-btn" data-mfp-src="#poor-tech-popup"><?php echo __('Lesson was cancelled'); ?></div>

                        <span class="aj-loader"><img
                                src="<?php echo plugins_url('img/loader.gif', __FILE__); ?>"></span>
                        <span class="aj-accept"><img
                                src="<?php echo plugins_url('img/accept_orange.png', __FILE__); ?>"></span>
                    </div>

                    <!-- END CANCELLATION BUTTON -->

                    <div class="teacher-name">
                        <?php
                        $teacher_sql = "SELECT author_id FROM " . $table_name . " WHERE subscriber_id=" . $user_id;
                        $teacher_query = $wpdb->get_results($teacher_sql, OBJECT);
                        $teachers_array = array();
                        ?>
                        <?php echo __('Teacher: ');
                        foreach ($teacher_query as $teacher) {
                            $teachers_array[] = $teacher->author_id;
                        }
                        $teachers_array = array_unique($teachers_array);
                        foreach ($teachers_array as $teacher_id) {
                            $teacher_first_name = get_user_meta($teacher_id, 'first_name', true);
                            $teacher_last_name = get_user_meta($teacher_id, 'last_name', true);
                            if ($teacher_id == end($teachers_array)) {
                                ?><span><?php echo $teacher_first_name . ' ' . $teacher_last_name; ?></span><?php
                            } else {
                                echo $teacher_first_name . ' ' . $teacher_last_name . ', ';
                            }
                        }
                        ?>
                    </div>

                    <span class="photo-hide"><?php echo __('Edit User Photo'); ?></span>
                    <span class="photo-show"
                          style="display: none"><?php echo do_shortcode('[avatar_upload user=' . $user_id . ' profile_edit_frontend=true]'); ?>
                        <span>

                    </div>
                    <?php $people_page_counter++; ?>
                    <?php if ($people_page_counter == 0) { ?>
                        </div>
                    <?php
                    } elseif ($people_page_counter % 6 == 0) {
                        ?>
                        </div>
                        </div>
                        <?php
                        $page_counter++; ?>
                        <div class="people-page" id="<?php echo $people_page_counter; ?>" data-page="<?php echo $page_counter + 1; ?>">

                    <?php
                    } else {
                        ?>
                        </div>
                    <?php } ?>
                    <?php

                    $k++;
                    $j++;
                }
                ?>
                </div>
                
                <div class="white-popup-block mfp-hide poor-tech-popup" id="poor-tech-popup">
                    <p><?php _e("Was this lesson cancelled due to problems with technology?", 'mch'); ?></p>
                    <div class="buttons">
                        <a href="#cancel-detail-message" type="button" class="e-btn" id="poor-tech-yes"><?php _e('Yes', 'mch'); ?></a>
                        <a href="" type="button" class="e-btn" id="poor-tech-no"><?php _e("No", 'mch'); ?></a>
                    </div>
                </div>

		      <!-- Popup with textarea field for explain lesson cenceled detail -->
		      <div class="white-popup-block mfp-hide cancel-detail-message students-list-wrapper" id="cancel-detail-message">
		        <p><?php _e("Can you explain what went wrong with the technology please?", 'mch'); ?></p>
			    <p><textarea class="cancel-comment lesson-comment" rows="6" cols="30" name="cancel-comment"></textarea></p>
		        <p><a class="save-cancel-detail e-btn" type="button" href="#"><?php _e('Save', 'mch'); ?></a></p>
		      </div>
                
                <ul class="pagination-sm" id="jquery-pagination"></ul>
            <?php } ?>
        <?php
        }
    }
    $return = ob_get_contents();
    ob_end_clean();
    return $return;

}

/**
 * Ajax function, which switch module and lesson drop-down selectors when teacher change Path/level selector
 */
function ajax_module_level_switcher()
{
    global $wpcwdb, $wpdb;
    if ($_POST['course_id'] && !empty($_POST['course_id'])) {
        $course_id = $_POST['course_id'];

        $sql = "SELECT *
			FROM $wpcwdb->courses
			WHERE course_id= '$course_id'
			ORDER BY course_title";

        $course_item = $wpdb->get_results($sql);

        if ($_POST['student_id'] && !empty($_POST['student_id'])) {
            $student_id = $_POST['student_id'];
        }

        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id); ?>
        <select class="module-select" name="module-select" id="student-<?php echo $student_id; ?>">
            <?php
            $i = 1;
            foreach ($module_details as $module) {
                ?>
                <option value="<?php echo $module->module_id; ?>"><?php echo $i; ?></option>
                <?php
                $i++;
            } ?>
        </select>

        <select class="lesson-select" name="lesson-select" id="student-<?php echo $student_id; ?>">
            <?php
            $sql = "SELECT *
				FROM $wpcwdb->courses
				WHERE course_id= '$course_id'
				ORDER BY course_title";

            $course_item = $wpdb->get_results($sql);

            $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
            foreach ($module_details as $module) {
                if (reset($module_details) == $module) {
                    $units_list = WPCW_units_getListOfUnits($module->module_id);
                    $i = 1;
                    foreach ($units_list as $unit) {
                        $unit_metadata = get_post_meta($unit->ID, 'unit_template_data');
                        $massive = maybe_unserialize($unit_metadata[0]);
                        ?>
                        <?php if ($massive['template_name'] == 'template-primary-school-course-introduction-page.php') { ?>
                            <option value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $unit->ID; ?>"><?php echo $i; ?></option>
                            <?php
                            $i++;
                        }
                    }
                } else {
                    break;
                }
            }
            ?>
        </select>

        <?php

        $emailable_downloads = get_option('emailable_downloads_list');
        $course_restricted = get_courses_with_restrict($emailable_downloads[0]);

        if (isset($course_restricted) && !empty($course_restricted)) {
            foreach ($course_restricted as $course) {
                if ($_POST['course_id'] == $course['course_id']) {
                    ?>
                    <div class="next-lesson-checker"></div>
                <?php
                }
            }
        }
    }
    die;
}

/**
 * Ajax function which change lesson selector when teacher change module selector
 */
function ajax_lesson_switcher()
{
    if (isset($_POST['module_id']) && !empty($_POST['module_id'])) {
        $module_id = $_POST['module_id'];

        $units_list = WPCW_units_getListOfUnits($module_id);
        $i = 1;
        ?>
        <select class="lesson-select" name="lesson-select">
            <?php
            if ($units_list && !empty($units_list)) {
                foreach ($units_list as $unit) {
                    $unit_metadata = get_post_meta($unit->ID, 'unit_template_data');
                    $massive = maybe_unserialize($unit_metadata[0]);

                    $id = $i; ?>
                    <?php if ($massive['template_name'] == 'template-primary-school-course-introduction-page.php') { ?>
                        <option value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $unit->ID; ?>"><?php echo $i; ?></option>
                        <?php
                        $i++;
                    }
                }
            } ?>
        </select>
    <?php
    }
    die();
}

function ajax_new_lesson_select()
{
    global $wpdb, $wpcwdb;

    if ($_POST['next_course_id'] && !empty($_POST['next_course_id'])) {
        $course_id = $_POST['next_course_id'];

        $sql = "SELECT *
			FROM $wpcwdb->courses
			WHERE course_id= '$course_id'
			AND course_group_id = '16'
			ORDER BY course_title";

        $course_item = $wpdb->get_results($sql);

        $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id); ?>
        <select class="next-module-select" name="next-module-select">
            <?php
            $i = 1;
            foreach ($module_details as $module) {
                ?>
                <option value="<?php echo $module->module_id; ?>"><?php echo $i; ?></option>
                <?php
                $i++;
            } ?>
        </select>

        <select class="next-lesson-select" name="next-lesson-select">
            <?php
            $sql = "SELECT *
				FROM $wpcwdb->courses
				WHERE course_id= '$course_id'
				ORDER BY course_title";

            $course_item = $wpdb->get_results($sql);

            $module_details = WPCW_courses_getModuleDetailsList($course_item[0]->course_id);
            foreach ($module_details as $module) {
                if (reset($module_details) == $module) {
                    $units_list = WPCW_units_getListOfUnits($module->module_id);
                    $i = 1;
                    foreach ($units_list as $unit) {
                        $unit_metadata = get_post_meta($unit->ID, 'unit_template_data');
                        $massive = maybe_unserialize($unit_metadata[0]);

                        $id = $i; ?>
                        <?php if ($massive['template_name'] == 'template-primary-school-course-introduction-page.php') { ?>
                            <option value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $unit->ID; ?>"><?php echo $i; ?></option>
                            <?php
                            $i++;
                        }
                    }
                } else {
                    break;
                }
            }
            ?>
        </select>
    <?php
    }

    if (isset($_POST['next_module']) && !empty($_POST['next_module'])) {
        $module_id = $_POST['next_module'];

        $units_list = WPCW_units_getListOfUnits($module_id);
        $i = 1;
        ?>
        <select class="next-lesson-select" name="lesson-select">
            <?php
            if ($units_list && !empty($units_list)) {
                foreach ($units_list as $unit) {
                    $unit_metadata = get_post_meta($unit->ID, 'unit_template_data');
                    $massive = maybe_unserialize($unit_metadata[0]);
                    ?>
                    <?php if ($massive['template_name'] == 'template-primary-school-course-introduction-page.php') { ?>
                        <option value="<?php echo $unit->ID; ?>"><?php echo __('Intro'); ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $unit->ID; ?>"><?php echo $i; ?></option>
                        <?php
                        $i++;
                    }
                }
            } ?>
        </select>
    <?php
    }
    die();
}

/**
 * Ajax function, which save selected lesson as complete to database
 */
function save_lesson_history()
{
	global $wpdb;
	if (!isset($_POST['cancel_lesson']) || empty($_POST['cancel_lesson']) || $_POST['cancel_lesson'] == false) {
		if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
			$user_id = $_POST['user_id'];
		}
		if (isset($_POST['teacher_id']) && !empty($_POST['teacher_id'])) {
			$teacher_id = $_POST['teacher_id'];
		}
		if (isset($_POST['course_id']) && !empty($_POST['course_id'])) {
			$course_id = $_POST['course_id'];
		}
		if (isset($_POST['module_id']) && !empty($_POST['module_id'])) {
			$module_id = $_POST['module_id'];
		}
		if (isset($_POST['lesson_id']) && !empty($_POST['lesson_id'])) {
			$lesson_id = $_POST['lesson_id'];
		}
		if (isset($_POST['comment']) && !empty($_POST['comment'])) {
			$comment = $_POST['comment'];
		}

		$cancel_comment = isset( $_POST['cancel_comment'] ) ? sanitize_text_field( $_POST['cancel_comment'] ) : '';

		if (isset($_POST['next_course_id']) && !empty($_POST['next_course_id'])) {
			$next_course_id = $_POST['next_course_id'];
		}
		if (isset($_POST['next_module_id']) && !empty($_POST['next_module_id'])) {
			$next_module_id = $_POST['next_module_id'];
		}
		if (isset($_POST['next_lesson_id']) && !empty($_POST['next_lesson_id'])) {
			$next_lesson_id = $_POST['next_lesson_id'];
		}
		if (isset($_POST['path_number']) && !empty($_POST['path_number'])) {
			$path_number = (int)$_POST['path_number'];
		}
		if (isset($_POST['next_path_number']) && !empty($_POST['next_path_number'])) {
			$next_path_number = (int)$_POST['next_path_number'];
		}
		if (isset($_POST['technology_rating']) && !empty($_POST['technology_rating'])) {
			$technology_rating = (int)$_POST['technology_rating'];
		}
		if (isset($_POST['ct_rating']) && !empty($_POST['ct_rating'])) {
			$ct_rating = (int)$_POST['ct_rating'];
		}

		$date = time();

		$normal_date = date('d/m/y', $date);
		$history_date = date('d/m/y H:i', $date);
		$teacher_first_name = get_user_meta($teacher_id, 'first_name', true);
		$teacher_last_name = get_user_meta($teacher_id, 'last_name', true);

		$table_name = $wpdb->prefix . "teachers_user_list";

		$course_details = WPCW_courses_getCourseDetails($course_id);
		$module_details = WPCW_modules_getModuleDetails($module_id);
		$lesson_number = (int)get_lesson_number($lesson_id)->unit_number - 1;
		$next_lesson_number = (int)get_lesson_number($next_lesson_id)->unit_number - 1;
		if($lesson_number === 0) {
			$lesson_number = 'Intro';
		}

		if($lesson_number !== 'Intro') {
			// send emails if rating is less then 3
			if($technology_rating < 3 || $ct_rating < 3) {
				if(($emails = get_option('mch_emails')) !== false) {
					$emails = json_decode($emails, true);
					send_low_quality_emails($emails, array(
						'user_id' => $user_id,
						'teacher_id' => $teacher_id,
						'pathway' => $path_number,
						'module' => $module_details->module_number,
						'lesson' => $lesson_number,
						'survey_date' => $date,
						'technology_rating' => $technology_rating,
						'ct_rating' => $ct_rating,
					));
				}
			}

			// Insert history lesson
			$devices_table = $wpdb->prefix . "devices";
			$result        = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $devices_table WHERE classroom_teacher_id = %d", $user_id ) );

			if ( ! is_null( $result ) ) {
				insert_survey_lesson( array(
					'pathway'              => $path_number,
					'module'               => $module_details->module_number,
					'lesson'               => $lesson_number,
					'lesson_id'            => $lesson_id,
					'chinese_teacher_id'   => $teacher_id,
					'classroom_teacher_id' => $user_id,
					'technology_rating' => $technology_rating,
					'ct_rating' => $ct_rating,
					'survey_date'          => $date
				) );

				if ( function_exists( 'mc_push_notification' ) ) {
					mc_push_notification( $result );
				}
			}
		}

		$wpdb->insert(
			$table_name,
			array(
				'author_id'       => $teacher_id,
				'subscriber_id'   => $user_id,
				'course_level_id' => $course_id,
				'module_id'       => $module_id,
				'lesson_id'       => $lesson_id,
				'comment'         => $comment,
				'cancel_comment'  => $cancel_comment,
				'complete_date'   => $date
			)
		);

		$course_name = $course_details->course_title;
		$module_name = $module_details->module_title;
		$lesson_name = get_the_title($lesson_id);

		update_user_meta($user_id, 'most_recent_lesson', $lesson_id);
		update_user_meta($user_id, 'most_recent_module', $module_id);
		update_user_meta($user_id, 'most_recent_course', $course_id);
		?>
		<div class="ajax-answer">
			<div class="history-wrapper">
				<div class="history-item-wrapper" id="0">
					<?php echo $normal_date; ?>
					<input type="hidden" value="<?php echo $date; ?>"/>
				</div>
				<div class="history-popup-box" id="popup-box-0">
					<div class="inner-history-popup">
						<div class="close">X</div>
						<h2><?php echo __('Lesson history: '); ?></h2>

						<div class="history-popup-content">
							<div class="history_course_id item-history">
								<span><?php echo __('Course: ') ?></span><?php echo $course_name; ?></div>
							<div class="history_module_id item-history">
								<span><?php echo __('Module: ') ?></span><?php echo $module_name; ?></div>
							<div class="history_lesson_id item-history">
								<span><?php echo __('Lesson: ') ?></span><?php echo $lesson_name; ?></div>
							<div class="history-comment item-history">
								<span><?php echo __('Comment: ') ?></span><?php echo $comment; ?></div>
							<div class="history-complete-lesson-time item-history">
								<span><?php echo __('Lesson complete date:') ?></span><?php echo $history_date; ?></div>
							<div class="history-teacher-name item-history">
								<span><?php echo __('Teacher: ') ?></span><?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="full-history-popup-box" id="popup-box-full-history">
			<div class="inner-history-popup">
				<div class="close">X</div>
				<h2><?php echo __('Lesson history: '); ?></h2>

				<div class="history-popup-content">
					<?php
					$full_history_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " ORDER BY complete_date DESC", OBJECT);
					$i = 1;
					foreach ($full_history_sql as $full_history_record) {


						$history_lesson_completed_time = $full_history_record->complete_date;
						$history_teacher_id = $full_history_record->author_id;
						$teacher_first_name = get_user_meta($history_teacher_id, 'first_name', true);
						$teacher_last_name = get_user_meta($history_teacher_id, 'last_name', true);
						$history_course_id = $full_history_record->course_level_id;
						$history_module_id = $full_history_record->module_id;
						$history_lesson_id = $full_history_record->lesson_id;
						$history_comment = $full_history_record->comment;

						$history_comment .= isset( $full_history_record->cancel_comment ) ? ' ' . $full_history_record->cancel_comment : '';

						$normal_date = date('d/m/y', $history_lesson_completed_time);
						$history_date = date('d/m/y H:i', $history_lesson_completed_time);

						$course_name = WPCW_courses_getCourseDetails($history_course_id)->course_title;
						$module_name = WPCW_modules_getModuleDetails($history_module_id)->module_title;
						$lesson_name = get_the_title($history_lesson_id);
						?>
						<div class="single-history-item">
							<div class="single-course-name item-history">
								<span><?php echo __('Course name: ') ?></span><?php echo $course_name; ?></div>
							<div class="single-module-name item-history">
								<span><?php echo __('Module name: ') ?></span><?php echo $module_name; ?></div>
							<div class="single-lesson-name item-history">
								<span><?php echo __('Lesson name: ') ?></span><?php echo $lesson_name; ?></div>
							<div class="single-comment item-history">
								<span><?php echo __('Teacher\'s comment: ') ?></span><?php echo $history_comment; ?>
							</div>
							<div class="single-lesson-complete_data item-history">
								<span><?php echo __('Lesson completed at: ') ?></span><?php echo $history_date; ?></div>
							<div class="single-teacher-name item-history">
								<span><?php echo __('Teacher name: ') ?></span><?php echo $teacher_first_name . $teacher_last_name; ?>
							</div>
						</div>
						<?php
						$i++;
					}
					?>
				</div>
			</div>
		</div>
		<?php
		if (isset($next_course_id) && !empty($next_course_id)) {
			if (isset($next_module_id) && !empty($next_module_id)) {
				if (isset($next_lesson_id) && !empty($next_lesson_id)) {
					//Save next lesson id to user meta
					$next_lesson = array(
						'download_id' => 16,
						'course_id' => $next_course_id,
						'module_id' => $next_module_id,
						'unit_id' => $next_lesson_id
					);
					update_user_meta($user_id, 'next_lesson_id', $next_lesson);

					// Insert information about next lesson for API
//                    $next_course_number = WPCW_courses_getCourseDetails($next_course_id)->course_number;
					$next_module_number = WPCW_modules_getModuleDetails($next_module_id)->module_number;
					$next_lesson_number = (int)get_lesson_number($next_lesson_id)->unit_number - 1;
					if($next_lesson_number === 0) {
						$next_lesson_number = 'Intro';
					}

					insert_next_week_lesson(array(
						'user_id' => $user_id,
						'teacher_id' => $teacher_id,
						'pathway' => $next_path_number,
						'module' => $next_module_number,
						'lesson' => $next_lesson_number
					));

					/**
					 * Here we go e-mail send next lesson to the user's mail
					 */
					//$user_id,
					$lesson_object = get_post($next_lesson_id);

					$author_first_name = get_user_meta($teacher_id, 'first_name', true);

					$author_data = get_userdata($teacher_id);

					$author_email = $author_data->data->user_email;

					$unit = get_unit_by_unit_id($_POST['next_lesson_id']);
					$unit_number = ($unit->unit_number - 1);

					$user_data = get_userdata($user_id);
					$user_email = $user_data->data->user_email;
					$user_first_name = get_user_meta($user_id, 'first_name', true);
					$user_last_name = get_user_meta($user_id, 'last_name', true);
					$username = $user_first_name . ' ' . $user_last_name;


					$course = get_course_by_id($next_course_id);
					$course_title = $course->course_title;


					$module = get_module_by_id($next_module_id);
					$module_title = $module->module_title;
					$module_number = $module->module_number;

					$lesson_content = $lesson_object->post_content;
					$lesson_title = $lesson_object->post_title;
					$lesson_metadata = get_post_meta($_POST['next_lesson_id'], 'unit_template_data');
					if (isset($lesson_metadata[0]) && !empty($lesson_metadata[0])) {
						$massive = maybe_unserialize($lesson_metadata[0]);
						if (isset($massive) && !empty($massive)) {

							if ($massive['template_name'] != 'template-normal-format-introduction.php' && $massive['template_name'] != 'template-primary-school-course-introduction-page.php') {
								//if ((int)$unit_number == 1) {
								$single_video_name = $massive['video_name'];
								$single_video_link = $massive['video_link'];
								$module_link = $massive['module_link'];
								$module_learning_outcomes = $massive['module_learning_outcomes'];
								$dialogue = $massive['dialogue'];
								$written_explanations = $massive['written_explanations'];
								$interesting_links = get_post_meta($_POST['next_lesson_id'], 'unit_template_interesting_links', true);//$massive['interesting_links'];
								$quiz_id = $massive['quiz_id'];
								$external_quiz_url = $massive['quiz_url'];
								$pronunciation_video_name = $massive['pr_video_name'];
								$pronunciation_video_link = $massive['pr_video_link'];
								$lesson_recap_name = $massive['lr_video_name'];
								$lesson_recap_link = $massive['lr_video_link'];
								$files = get_attached_media('', $_POST['next_lesson_id']);

								require_once(plugin_dir_path(__FILE__) . 'libs/phpmailer/PHPMailerAutoload.php');

								$Letter = new PHPMailer();
								$Letter->isHTML(true);

								if ($author_first_name && !empty($author_first_name)) {
									$Letter->FromName = ucfirst($author_first_name);
								} else {
									$Letter->FromName = 'MCT_Admin';
								}

								if ($author_email && !empty($author_email)) {
									$Letter->From = $author_email;
									$Letter->addBCC('systememails@mychineseteacher.org', 'Regina');
								} else {
									$Letter->From = 'info@myeducationgroup.org';
									$Letter->addBCC('systememails@mychineseteacher.org', 'Regina');
								}

								$Letter->addAddress($user_email, $username);
                                mch_set_attachments($files, $Letter, $user_id);

								$Letter->Subject = __('My Chinese Hub: ', 'mct_hub') . $lesson_title;
								$Letter->Body = '<p>' . __('Hi ', 'mch_conf') . $user_first_name . ',<br /><br />';
								$Letter->Body .= __('Next lesson we will be covering - ', 'mch_conf') . $course_title . ' - Module ' . $module_number . ' - ' . $lesson_title . '<br /></p>';
								//$Letter->Body .= __('Next lesson we will be covering - ', 'mch_conf') . $course_title . ' - ' . $module_title . ' - ' . $lesson_title . '<br /></p>';

								/**
								 * attaching single video link to letter
								 */
								if ($single_video_link && !empty($single_video_link)) {
									$Letter->Body .= '<p>' . __('Here are the links to following videos:', 'mch_conf') . '<br />';
									if ($single_video_name && !empty($single_video_name)) {
										$Letter->Body .= '<a href="' . $single_video_link . '">' . $single_video_name . '</a><br /></p>';
									} else {
										$Letter->Body .= '<a href="' . $single_video_link . '">' . $single_video_link . '</a><br /></p>';
									}
								}

								/**
								 * Attaching video links to letter
								 */
								if ((isset($pronunciation_video_link) && !empty($pronunciation_video_link)) || (isset($lesson_recap_link) && !empty($lesson_recap_link))) {
									$Letter->Body .= '<p>' . __('Here are the links to following videos:', 'mch_conf') . '<br />';
									if (isset($lesson_recap_link) && !empty($lesson_recap_link)) {
										if (isset($lesson_recap_name) && !empty($lesson_recap_name)) {
											$Letter->Body .= '<a href="' . $lesson_recap_link . '">' . $lesson_recap_name . '</a><br />';
										} else {
											$Letter->Body .= '<a href="' . $lesson_recap_link . '">' . $lesson_recap_link . '</a><br />';
										}
									}

									if (isset($pronunciation_video_link) && !empty($pronunciation_video_link)) {
										if (isset($pronunciation_video_name) && !empty($pronunciation_video_name)) {
											$Letter->Body .= '<a href="' . $pronunciation_video_link . '">' . $pronunciation_video_name . '</a><br />';
										} else {
											$Letter->Body .= '<a href="' . $pronunciation_video_link . '">' . $pronunciation_video_link . '</a><br />';
										}
									}
									$Letter->Body .= '</p>';
								}

								if (isset($interesting_links) && !empty($interesting_links)) {
									$Letter->Body .= '<p>' . __('Here are some other links that your class might be interested in:', 'mch_conf') . '</p>';
									$Letter->Body .= $interesting_links;
								}

								$Letter->Body .= '<p>' . __('You can also find the flashcards and pictures attached. If this is a recap lesson, then there won\'t be any attachments.', 'mch_conf') . '<br /></p>';
								$Letter->Body .= '<p>' . __('If you have any questions or concerns, please don\'t hesitate to send us a question.', 'mch_conf') . '<br /></p>';
								$Letter->Body .= '<p>' . __('Kind regards,') . '<br />';
								$Letter->Body .= __('The My Chinese Teacher team') . '</p>';

								if (!$Letter->send()) {
									echo 'Message could not be sent.';
									echo 'Mailer Error: ' . $Letter->ErrorInfo;
								} else {
									echo 'Message has been sent';
								}
							}
							else{

								$single_video_name = $massive['video_name'];
								$single_video_link = $massive['video_link'];
								$module_link = $massive['module_link'];
								$module_learning_outcomes = $massive['module_learning_outcomes'];
								$dialogue = $massive['dialogue'];
								$written_explanations = $massive['written_explanations'];
								$interesting_links = get_post_meta($_POST['next_lesson_id'], 'unit_template_interesting_links', true);//$massive['interesting_links'];
								$quiz_id = $massive['quiz_id'];
								$external_quiz_url = $massive['quiz_url'];
								$pronunciation_video_name = $massive['pr_video_name'];
								$pronunciation_video_link = $massive['pr_video_link'];
								$lesson_recap_name = $massive['lr_video_name'];
								$lesson_recap_link = $massive['lr_video_link'];
								$files = get_attached_media('', $_POST['next_lesson_id']);

								require_once(plugin_dir_path(__FILE__) . 'libs/phpmailer/PHPMailerAutoload.php');

								$Letter = new PHPMailer();
								$Letter->isHTML(true);


								if ($author_first_name && !empty($author_first_name)) {
									$Letter->FromName = ucfirst($author_first_name);
								} else {
									$Letter->FromName = 'MCT_Admin';
								}

								if ($author_email && !empty($author_email)) {
									$Letter->From = $author_email;
									$Letter->addBCC('systememails@mychineseteacher.org', 'Regina');
								} else {
									$Letter->From = 'info@myeducationgroup.org';
									$Letter->addBCC('systememails@mychineseteacher.org', 'Regina');
								}

								$Letter->addAddress($user_email, $username);
                                mch_set_attachments($files, $Letter, $user_id);

								$Letter->Subject = __('My Chinese Hub: ', 'mct_hub') . $lesson_title;
								$Letter->Body = '<p>' . __('Hi ', 'mch_conf') . $user_first_name . ',<br /><br />';
								$Letter->Body .= __('Over the next ten weeks, we will be covering - ', 'mch_conf') . $course_title . ' Module ' . $module_number . '<br /></p>';

								$Letter->Body .= '<p>' . __('You can find the lesson plans, learning outcomes, workbook and answerbook attached. The flashcards will be sent lesson by lesson every week.', 'mch_conf') . '<br /></p>';
								$Letter->Body .= '<p>' . __('If you have any questions or concerns, please don\'t hesitate to send us a question.', 'mch_conf') . '<br /></p>';
								$Letter->Body .= '<p>' . __('Kind regards,') . '<br />';
								$Letter->Body .= __('The My Chinese Teacher team') . '</p>';

								if (!$Letter->send()) {
									echo 'Message could not be sent.';
									echo 'Mailer Error: ' . $Letter->ErrorInfo;
								} else {
									echo 'Message has been sent';
								}
							}
						}
					}
				}
			}
		}
	} else {
		$date = time();
		if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
			$user_id = $_POST['user_id'];
		}
		if (isset($_POST['teacher_id']) && !empty($_POST['teacher_id'])) {
			$teacher_id = $_POST['teacher_id'];
		}
		if (isset($_POST['course_id']) && !empty($_POST['course_id'])) {
			$course_id = $_POST['course_id'];
		}
		if (isset($_POST['module_id']) && !empty($_POST['module_id'])) {
			$module_id = $_POST['module_id'];
		}
		if (isset($_POST['lesson_id']) && !empty($_POST['lesson_id'])) {
			$lesson_id = $_POST['lesson_id'];
		}
		if (isset($_POST['technology_rating']) && !empty($_POST['technology_rating'])) {
			$technology_rating = (int)$_POST['technology_rating'];
		}
		if (isset($_POST['ct_rating']) && !empty($_POST['ct_rating'])) {
			$ct_rating = (int)$_POST['ct_rating'];
		}
		if (isset($_POST['path_number']) && !empty($_POST['path_number'])) {
			$path_number = (int)$_POST['path_number'];
		}
		$normal_date = date('d/m/y', $date);
		$history_date = date('d/m/y H:i', $date);
		$teacher_first_name = get_user_meta($teacher_id, 'first_name', true);
		$teacher_last_name = get_user_meta($teacher_id, 'last_name', true);

		$comment = isset( $_POST['comment'] ) ? sanitize_text_field( $_POST['comment'] ) : '';
		$cancel_comment = isset( $_POST['cancel_comment'] ) ? sanitize_text_field( $_POST['cancel_comment'] ) : '';

		if (isset($_POST['poor_tech']) && $_POST['poor_tech'] === '1') {

			//Send cancel email.
			$user_first_name = get_user_meta($user_id, 'first_name', true);
			$user_last_name = get_user_meta($user_id, 'last_name', true);
			$teacher_name = $teacher_first_name . ' ' . $teacher_last_name;
			$user_name = $user_first_name . ' ' . $user_last_name;
			$course_details = WPCW_courses_getCourseDetails($course_id);
			$module_details = WPCW_modules_getModuleDetails($module_id);
			$coursename = $course_details->course_title;
			$modulename = $module_details->module_number;
			$lesson_number = (int)get_lesson_number($lesson_id)->unit_number - 1;


			if($lesson_number === 0) {
				$lesson_number = 'Intro';
			}


			$subject = __('Lesson was cancelled');
			$message = $teacher_name . ' ' . __('cancelled') . ' ' . $coursename . ' '
			  . __('course') . ', ' . __('module') . ' ' . $modulename . ', '
			  . __('lesson') . ' ' . $lesson_number . ' ' . __('at') . ' ' . $user_name . '.' .
			  ' Cancel comment: ' . $cancel_comment;

			// send emails.
			if (($mch_emails = get_option('mch_emails')) !== false) {
				$mch_emails = json_decode($mch_emails, true);
				wp_mail($mch_emails, $subject, $message);
			}

			//CSV export
			insert_survey_lesson( array(
				'pathway'               => $path_number,
				'module'                => $modulename,
				'lesson'                => $lesson_number,
				'lesson_id'             => $lesson_id,
				'chinese_teacher_id'    => $teacher_id,
				'classroom_teacher_id'  => $user_id,
				'technology_rating'     => $technology_rating,
				'ct_rating'             => $ct_rating,
				'survey_date'           => $date,
				'is_cancelled'          => 1,
				'comment'               => __('Cancelled due to poor technology'),
				'star_rating'           => 0,
			) );

			// cancelled lesson action.
			do_action('mch_cancelled_lesson', $user_id, array(
					  'description' => $subject . ' | ' . $user_name,
					  'detail' => $message,
					  'category' => __('classroom teacher issue'),
					  'dueDate' => date('Y-m-d'),
			));
		}

		$table_name = $wpdb->prefix . "teachers_user_list";

		$wpdb->insert(
			$table_name,
			array(
				'author_id' => $teacher_id,
				'subscriber_id' => $user_id,
				'course_level_id' => null,
				'module_id' => null,
				'lesson_id' => null,
				'comment' => $comment,
				'cancel_comment' => $cancel_comment,
				'complete_date' => $date
			)
		);

		$comment .= isset( $cancel_comment ) ? ' ' . $cancel_comment : '';
		$course_name = '';
		$module_name = '';
		$lesson_name = '';
		?>
		<div class="ajax-answer">
			<div class="history-wrapper">
				<div class="history-item-wrapper" id="0">
					<?php echo $normal_date; ?>
					<input type="hidden" value="<?php echo $date; ?>"/>
				</div>
				<div class="history-popup-box" id="popup-box-0">
					<div class="inner-history-popup">
						<div class="close">X</div>
						<h2><?php echo __('Lesson history: '); ?></h2>

						<div class="history-popup-content">
							<div class="history_course_id item-history">
								<span><?php echo __('Course: ') ?></span><?php echo $course_name; ?></div>
							<div class="history_module_id item-history">
								<span><?php echo __('Module: ') ?></span><?php echo $module_name; ?></div>
							<div class="history_lesson_id item-history">
								<span><?php echo __('Lesson: ') ?></span><?php echo $lesson_name; ?></div>
							<div class="history-comment item-history">
								<span><?php echo __('Comment: ') ?></span><?php echo $comment; ?></div>
							<div class="history-complete-lesson-time item-history">
								<span><?php echo __('Lesson complete date:') ?></span><?php echo $history_date; ?></div>
							<div class="history-teacher-name item-history">
								<span><?php echo __('Teacher: ') ?></span><?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="full-history-popup-box" id="popup-box-full-history">
			<div class="inner-history-popup">
				<div class="close">X</div>
				<h2><?php echo __('Lesson history: '); ?></h2>

				<div class="history-popup-content">
					<?php
					$full_history_sql = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE subscriber_id=" . $user_id . " ORDER BY complete_date DESC", OBJECT);
					$i = 1;
					foreach ($full_history_sql as $full_history_record) {


						$history_lesson_completed_time = $full_history_record->complete_date;
						$history_teacher_id = $full_history_record->author_id;
						$teacher_first_name = get_user_meta($history_teacher_id, 'first_name', true);
						$teacher_last_name = get_user_meta($history_teacher_id, 'last_name', true);
						$history_course_id = $full_history_record->course_level_id;
						$history_module_id = $full_history_record->module_id;
						$history_lesson_id = $full_history_record->lesson_id;
						$history_comment = $full_history_record->comment;
						$normal_date = date('d/m/y', $history_lesson_completed_time);
						$history_date = date('d/m/y H:i', $history_lesson_completed_time);

						$course_name = WPCW_courses_getCourseDetails($history_course_id)->course_title;
						$module_name = WPCW_modules_getModuleDetails($history_module_id)->module_title;
						$lesson_name = get_the_title($history_lesson_id);
						?>
						<div class="single-history-item">
							<div class="single-course-name item-history">
								<span><?php echo __('Course name: ') ?></span><?php echo $course_name; ?></div>
							<div class="single-module-name item-history">
								<span><?php echo __('Module name: ') ?></span><?php echo $module_name; ?></div>
							<div class="single-lesson-name item-history">
								<span><?php echo __('Lesson name: ') ?></span><?php echo $lesson_name; ?></div>
							<div class="single-comment item-history">
								<span><?php echo __('Teacher\'s comment: ') ?></span><?php echo $history_comment; ?>
							</div>
							<div class="single-lesson-complete_data item-history">
								<span><?php echo __('Lesson completed at: ') ?></span><?php echo $history_date; ?></div>
							<div class="single-teacher-name item-history">
								<span><?php echo __('Teacher name: ') ?></span><?php echo $teacher_first_name . $teacher_last_name; ?>
							</div>
						</div>
						<?php
						$i++;
					}
					?>
				</div>
			</div>
		</div>
	<?php
	}

	die();
}

/**
 * enabling scripts
 */
function teacher_portal_enqueue_scripts()
{
    wp_enqueue_script('ajax_module_lesson_switch', plugins_url('js/module_lesson_switch.js', __FILE__), array('jquery'));
    wp_enqueue_script('lesson_history_save', plugins_url('js/lesson_history_save.js', __FILE__), array('jquery'));
    wp_enqueue_script('popup_script', plugins_url('js/popup.js', __FILE__), array('jquery'));
    wp_enqueue_script('hide_students_script', plugins_url('js/students_hider.js', __FILE__), array('jquery'));
    wp_enqueue_script('jquery_pagination', plugins_url('js/jquery.twbsPagination.js', __FILE__), array('jquery'));
    wp_enqueue_script('jquery_pagination_start', plugins_url('js/pagination_start.js', __FILE__), array('jquery'));
    wp_enqueue_script('students_accessory', plugins_url('js/students_accessory.js', __FILE__), array('jquery'));
    wp_enqueue_style('popup_style', plugins_url('css/style.css', __FILE__));
    wp_localize_script('ajax_module_lesson_switch', 'Ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_localize_script('lesson_history_save', 'Ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_action('wp_ajax_ajax_module_level_switcher', 'ajax_module_level_switcher');
add_action('wp_ajax_ajax_new_lesson_select', 'ajax_new_lesson_select');
add_action('wp_ajax_save_lesson_history', 'save_lesson_history');
add_action('wp_ajax_ajax_lesson_switcher', 'ajax_lesson_switcher');
add_action('wp_enqueue_scripts', 'teacher_portal_enqueue_scripts');


function time_elapsed_string($ptime)
{
    if (!$ptime) {
        return false;
    }
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'min',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hour' => 'hours',
        'min' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
        }
    }
}

add_action('wp_ajax_save_student_about', 'save_student_about');
add_action('wp_ajax_nopriv_save_student_about', 'save_student_about');

function save_student_about()
{
    global $wpdb;

    if (isset($_POST['student_id']) && !empty($_POST['student_id']) && intval($_POST['student_id'])) {
        $student_id = intval($_POST['student_id']);
    }
    if (isset($_POST['student_about']) && !empty($_POST['student_about'])) {
        $student_about = esc_textarea($_POST['student_about']);
    }
    update_user_meta($student_id, 'student_about', $student_about);
    echo 1;
    die();
}

/**
 * Insert information about next lesson
 * @param array $arg in this format:
 *
 *   array(
 *       'user_id' => $user_id,
 *       'teacher_id' => $teacher_id,
 *       'pathway' => $next_course,
 *       'module' => $next_module,
 *       'lesson' => $next_lesson
 *   );
 *
 */
function insert_next_week_lesson(array $arg) {
	global $wpdb;

	$table_name = $wpdb->prefix . "next_week_lesson";
	// check relevance of lesson
	$result = $wpdb->get_row(
		$wpdb->prepare("SELECT * FROM $table_name WHERE user_id = %d", $arg['user_id'])
	);

	if(is_null($result)) {
		$wpdb->insert(
			$table_name,
			array(
				'user_id' => $arg['user_id'],
				'teacher_id' => $arg['teacher_id'],
				'pathway' => $arg['pathway'],
				'module' => $arg['module'],
				'lesson' => $arg['lesson']
			),
			array('%d', '%d', '%d', '%d', '%s')
		);
	} else {
		$wpdb->update(
			$table_name,
			array(
				'teacher_id' => $arg['teacher_id'],
				'pathway' => $arg['pathway'],
				'module' => $arg['module'],
				'lesson' => $arg['lesson']
			),
			array('user_id' => $arg['user_id']),
			array('%d', '%d', '%d', '%s'),
			array('%d')
		);
	}
}

/**
 * Getting unit object by unit ID
 * @param $unit_id
 *
 * @return bool or object
 */
function get_lesson_number($unit_id)
{
    global $wpdb;

    $units_table = $wpdb->prefix . 'wpcw_units_meta';
    $unit_sql = $wpdb->prepare("SELECT *
					FROM $units_table
					WHERE unit_id = %d", $unit_id);
    $item = $wpdb->get_results($unit_sql);

    if (isset($item[0]) && !empty($item[0])) {
        return $item[0];
    } else {
        return false;
    }
}

/**
 * Get stars
 */
 function get_stars_options()
 {
    return array(
        1 => '1 star',
        '2 stars',
        '3 stars',
        '4 stars',
        '5 stars'
    );
 }

/**
 * Send emails if survey technology or quality less than 3
 *
 * @param string|array $emails Array or comma-separated list of email addresses to send message.
 * @param array $arg
 *
 */
function send_low_quality_emails($emails, array $arg) {
    global $blog_id;

    if($blog_id != HUB_BLOG_ID){
        switch_to_blog(HUB_BLOG_ID);
    }
    $user_data = get_userdata($arg['user_id']);
    $user_email = $user_data->user_email;
    $user_first_name = get_user_meta($arg['user_id'], 'first_name', true);
    $user_last_name = get_user_meta($arg['user_id'], 'last_name', true);

    $user_full_name = $user_first_name . ' ' . $user_last_name;

    $school_name = get_user_meta($arg['user_id'], 'school', true);
    $teacher_first_name = get_user_meta($arg['teacher_id'], 'first_name', true);
    $teacher_last_name = get_user_meta($arg['teacher_id'], 'last_name', true);
    $teacher_full_name = $teacher_first_name . ' ' . $teacher_last_name;
    $lesson_date = date('d/m/y H:i', $arg['survey_date']);

    $full_lesson_details = "Pathway #{$arg['pathway']} | Module #{$arg['module']} | Lesson #{$arg['lesson']} | Date $lesson_date";

    // Create message Subject and Body
    $subject = __('Low ranking lesson', 'mct_hub');
    $body = __('Classroom teacher name: ', 'mct_hub') . $user_full_name . "\n";
    $body .= __('Classroom teacher email: ', 'mct_hub') . $user_email . "\n";
    $body .= __('School name: ', 'mct_hub') . $school_name . "\n";
    $body .= __('Chinese teacher name: ', 'mct_hub') . $teacher_full_name . "\n";
    $body .= __('Lessons details: ', 'mct_hub') . $full_lesson_details . "\n";
    $body .= __('Technology: ', 'mct_hub') . $arg['technology_rating'] . "\n";
    $body .= __('Classroom Teacher Quality: ', 'mct_hub') . $arg['ct_rating'] . "\n";

	// Low ranking action.
	do_action('mch_low_ranking', $arg['user_id'], array(
		  'description' => $subject . ' | ' . $user_full_name,
		  'detail' => $body,
		  'category' => __('classroom teacher issue'),
		  'dueDate' => date('Y-m-d'),
	));
    // send emails
    $email = wp_mail($emails, $subject, $body);

    if(!$email){
        set_log($arg['user']->ID, 'Send email', 'Send email error.');
    }
}