<?php
/*
Plugin Name: MCH Course page shortcode
Plugin URI:
Description: Plugin for MyChinese Mecca (Hub) site Course page
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

register_activation_hook(__FILE__, 'add_course_meta_table');

add_shortcode('mch_course_page', 'course_page_schortcode');


function add_course_meta_table()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "wpcw_courses_meta";

    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
				  id bigint(11) NOT NULL AUTO_INCREMENT,
				  course_id bigint(11) DEFAULT '0' NOT NULL,
				  course_meta_key text NOT NULL,
				  course_meta_value LONGTEXT NOT NULL,
				  UNIQUE KEY id (id)
				);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

}

/**
 * Shortcode for course page
 */
function course_page_schortcode()
{
    global $wpdb, $wpcwdb;

    $user_id = get_current_user_id();

    $has_purchase = edd_get_users_purchased_products($user_id);

    $user_expirations = get_all_expirations_by_user($user_id);

    $primitive_access = maybe_unserialize(get_user_meta($user_id, 'free_access_to_primitive_hub_courses', true));
    
    $page_access = false;
    if ($has_purchase && !empty($has_purchase) && is_array($has_purchase) || $primitive_access) {
        if ($user_expirations) {
            foreach ($user_expirations as $exp) {
                if (time() < $exp) {
                    $page_access = true;
                    break;
                }
            }
        }
    }

    if ($page_access == false) {
        $trial_period_time = get_user_meta($user_id, 'hub_ending_trial_time', true);
        if (time() < (int)$trial_period_time) {
            $page_access = true;
        }

    }

    if(current_user_can('administrator')){
        $page_access = true;
    }

    ob_start(); ?>
    <?php if ($page_access == true) { ?>
    <div class="select-content-wrapper">
    <form action="" method="get" class="lesson-switch-form">
    <div class="drop-down-wrapper">
        <div class="link-to-tutor">
            <?php
            switch_to_blog(1);
            $tutor_url = home_url();
            restore_current_blog();
            ?>
            <a class="e-btn-tutor" href="<?php echo $tutor_url; ?>"><?php echo __('Book a tutor'); ?></a>
        </div>
        <div class="wrap-selects select-without-border">
            <div class="box-select">
                <select name="download_number" class="downloads-list">
                    <?php
                    //WP_Query for all downloads
                    $downloads_post_list = get_downloads_list();
                    $current_time = time();
                    $list_allowed_to = maybe_unserialize(get_user_meta($user_id, 'free_access_to_primitive_hub_courses', true));

                    if ($downloads_post_list->have_posts()):
                        $post_counter = 0;
                        $first_download_id = false;
                        while ($downloads_post_list->have_posts()): $downloads_post_list->the_post();
                            $download_id = get_the_ID();
                            $download_title = get_the_title($download_id);

                            //Get downloads which have resitrictions
                            $restrict_download = get_restricted_downloads($download_id);
                            $can_access = edd_cr_user_can_access(get_current_user_id(), $restrict_download[$download_id]['restricted'], $restrict_download[$download_id]['unit_id']);

                            $trial_period_time = get_user_meta($user_id, 'hub_ending_trial_time', true);

                            // Check if user has access to download from usermeta
                            $can_access_from_usermeta = false;
                            if ($list_allowed_to) {
                                foreach ($list_allowed_to as $allowed_to) {
                                    $allowed_download_id = explode('hub-course-', $allowed_to['download_id']);
                                    $expiration_date = get_expiration_date($user_id, $allowed_download_id[1], $allowed_to['price_id']);
                                    if ($allowed_download_id[1] == $download_id && time() < $expiration_date) {
                                        $can_access_from_usermeta = true;
                                        break;
                                    }
                                }
                            }

                            $download_access = false;
                            $prices = get_post_meta($download_id, 'edd_variable_prices', true);
                            $count_prices = count($prices);

                            // problem cycle
                            if ($restrict_download[$download_id]['unit_id'] && is_array($restrict_download[$download_id]['unit_id'])) {
                                foreach ($restrict_download[$download_id]['unit_id'] as $current_unit_id) {
                                    $restricted = edd_cr_is_restricted($current_unit_id);
                                    $current_has_access = edd_cr_user_can_access(get_current_user_id(), $restricted, $current_unit_id);
                                    if ($current_has_access['status'] == true) {
                                        $download_access = true;
                                        break;
                                    }
                                }
                            }

                            $expirations = get_user_and_download_expiration_dates($user_id, $download_id);

                            if ($expirations && !empty($expirations)) {
                                foreach ($expirations as $expiration) {
                                    if ($current_time < $expiration) {
                                        $download_access = true;
                                    }
                                }
                            }


                            $access = false;
                            if ($has_purchase && !empty($has_purchase) && is_array($has_purchase)) {
                                foreach ($has_purchase as $download) {
                                    if ($download->ID == $download_id) {
                                        $access = true;
                                    }
                                }
                            } else {
                                if ($has_purchase->ID == $download_id) {
                                    $access = true;
                                }
                            }

                            if ($access == true || $can_access_from_usermeta == true || $current_time < $trial_period_time) {
                                if ($download_access == true || $can_access_from_usermeta == true || $current_time < $trial_period_time) {

                                    if (!$first_download_id) {
                                        $first_download_id = $download_id;
                                    }
                                    ?>
                                    <option value="<?php echo $download_id; ?>" <?php
                                    if (isset($_GET['download_number']) && !empty($_GET['download_number'])) {
                                        selected($_GET['download_number'], $download_id);
                                    }
                                    ?>><?php echo $download_title; ?></option>
                                    <?php
                                    $post_counter++;
                                }
                            }
                        endwhile;
                    endif;
                    ?>

                </select>
            </div>

            <?php if (function_exists('get_course_by_id')) { ?>
                <div class="box-select">
                    <label for="level_grade"><?php echo __('Course: '); ?></label>
                    <select name="level_grade" class="level-grade" id="level_grade">

                        <?php
                        if (isset($_GET['download_number']) && !empty($_GET['download_number'])) {
                            $course_restinct_id = $_GET['download_number'];
                        } else {
                            $course_restinct_id = $first_download_id;
                        }


                        $course_restricted = get_courses_with_restrict($course_restinct_id);

                        if ($course_restricted && !empty($course_restricted)) {
                            $default_course_isset = false;
                            $i = 1;
                            foreach ($course_restricted as $restrict) {
                                if (isset($restrict['course_id']) && !empty($restrict['course_id'])) {
                                    if (!$default_course_isset) {
                                        $default_course = $restrict['course_id'];
                                        $default_course_isset = true;
                                    }
                                    $course_object = get_course_by_id($restrict['course_id']);
                                    $course_title = $course_object->course_title;
                                    ?>
                                    <option
                                        value="<?php echo $restrict['course_id']; ?>" <?php
                                    if (isset($_GET['level_grade']) && !empty($_GET['level_grade'])) {
                                        selected($_GET['level_grade'], $restrict['course_id']);
                                    }
                                    ?>><?php echo $course_title;//__('Pathway ') . $i;  ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
            <?php
            } else {
                echo __('Please, enable MCH Configuration plugin');
            }
            ?>
            <div class="box-select">
                <label for="module_number"><?php echo __('Module: '); ?></label>
                <select name="module_number" class="module-number" id="module_number">

                    <?php
                    if (isset($_GET['level_grade']) && !empty($_GET['level_grade'])) {
                        $module_list = WPCW_courses_getModuleDetailsList($_GET['level_grade']);
                    } else {
                        if ($default_course && !empty($default_course)) {
                            $module_list = WPCW_courses_getModuleDetailsList($default_course);
                        }
                    }

                    $default_module_isset = false;
                    if ($module_list && !empty($module_list)) {
                        foreach ($module_list as $module) {
                            $module_id = $module->module_id;
                            $module_number = $module->module_number;
                            $module_title = $module->module_title;
                            if (!$default_module_isset) {
                                $default_module = $module_id;
                                $default_module_isset = true;
                            } ?>
                            <option value="<?php echo $module_id; ?>" <?php
                            if (isset($_GET['module_number']) && !empty($_GET['module_number'])) {
                                selected($_GET['module_number'], $module_id);
                            }
                            ?>><?php echo $module_number; //$module_title; ?></option>
                        <?php
                        }
                    }
                    ?>

                </select>
            </div>
        </div>
    </div>


    <div class="lesson-wrapper">
        <?php
        if (isset($_GET['module_number']) && !empty($_GET['module_number'])) {
            $lesson_list = WPCW_units_getListOfUnits($_GET['module_number']);
        } else {
            if ($default_module && !empty($default_module)) {
                $lesson_list = WPCW_units_getListOfUnits($default_module);
            }
        }

        ?>
        <div class="wrap-links">
            <?php if (isset($_GET['level_grade']) && !empty($_GET['level_grade']) && isset($_GET['module_number']) && !empty($_GET['module_number'])) { ?>
                <?php $emailable_downloads_list = get_option('emailable_downloads_list'); ?>
                <?php if (in_array($_GET['download_number'], $emailable_downloads_list)) { ?>
                    <div class="mail-lesson"><a class="e-btn-grey e-btn-send-mail e-with-title"
                                                href="#"><?php echo __('Send mail'); ?><span
                                class="e-title"><?php echo __('Send mail'); ?></span></a><i
                            class="fa fa-check"></i></div>
                <?php } ?>
            <?php } ?>
            <?php if (mch_user_has_access($user_id, 'view_forum')): ?>
                <?php $forum_page = edd_get_option('mch_forum_page'); ?>
                <div class="forum-link"><a class="e-btn-grey e-btn-forum e-with-title"
                                           href="<?php echo get_permalink($forum_page); ?>"><?php echo __('Ask on forum'); ?>
                        <span
                            class="e-title"><?php echo __('Ask on forum'); ?></span></a>
                </div>
            <?php endif; ?>
        </div>
        <div class="select-lesson">
            <span class="level-number-text"><?php echo __('Lesson: '); ?></span>
            <ul class="lessons-list">
                <?php
                if ($lesson_list && !empty($lesson_list)) {
                    $i = 1;
                    foreach ($lesson_list as $lesson) {
                        $lesson_list_template_massive = get_post_meta($lesson->ID, 'unit_template_data');
                        foreach ($lesson_list_template_massive as $massive) {
                            $unserialized_massive = maybe_unserialize($massive);
                            break;
                        }
                        $list_template_name = $unserialized_massive['template_name'];

                        if($list_template_name == 'template-normal-format-introduction.php' || $list_template_name == 'template-primary-school-course-introduction-page.php'){ ?>
                            <li class="single-lesson-item-wrapper<?php if(isset($_GET['actual_lesson']) && $lesson->ID == $_GET['actual_lesson']){
                                echo ' active-lesson"';
                                }
                            else{
                                echo '"';
                            }?>
                                ><a
                                href="#"><?php echo __('Intro'); //$title; ?></a></li>
                        <input type="hidden" value="<?php echo $lesson->ID; ?>"/>
                 <?php  }
                        elseif($list_template_name == 'template-normal-format-final-test.php' || $list_template_name == 'template-primary-school-final-test.php'){ ?>
                            <li class="single-lesson-item-wrapper<?php if(isset($_GET['actual_lesson']) && $lesson->ID == $_GET['actual_lesson']){
                                echo ' active-lesson"';
                            }
                            else{
                                echo '"';
                            }?>
                                ><a
                                href="#"><?php echo __('Test'); //$title; ?></a></li>
                        <input type="hidden" value="<?php echo $lesson->ID; ?>"/>
                 <?php  }
                        elseif($list_template_name == 'template-normal-format-end-of-course-exam.php'){ ?>
                            <li class="single-lesson-item-wrapper<?php if(isset($_GET['actual_lesson']) && $lesson->ID == $_GET['actual_lesson']){
                                echo ' active-lesson"';
                            }
                            else{
                                echo '"';
                            }?>
                                ><a
                                href="#"><?php echo __('Exam'); //$title; ?></a></li>
                        <input type="hidden" value="<?php echo $lesson->ID; ?>"/>
                 <?php }
                        else{
                            $title = get_the_title($lesson->ID); ?>
                            <li class="single-lesson-item-wrapper<?php if(isset($_GET['actual_lesson']) && $lesson->ID == $_GET['actual_lesson']){
                                echo ' active-lesson"';
                            }
                            else{
                                echo '"';
                            }?>
                                ><a
                                    href="#"><?php echo $i; //$title; ?></a></li>
                            <input type="hidden" value="<?php echo $lesson->ID; ?>"/>
                            <?php
                            $i++;
                        }


                    }
                }
                ?>
            </ul>
            <input type="hidden" name="actual_lesson" value="" class="actual-lesson"/>
        </div>
    </div>
    </form>

    <?php if (isset($_GET['actual_lesson']) && !empty($_GET['actual_lesson'])) { ?>
        <?php if (isset($_GET['level_grade']) && !empty($_GET['level_grade']) && isset($_GET['module_number']) && !empty($_GET['module_number'])) { ?>
            <?php if (in_array($_GET['download_number'], $emailable_downloads_list)) { ?>
                <input type="hidden" name="course_id" value="<?php echo $_GET['level_grade']; ?>" id="course_id">
                <input type="hidden" name="module_id" value="<?php echo $_GET['module_number']; ?>" id="module_id">
                <input type="hidden" name="lesson_id" value="<?php echo $_GET['actual_lesson']; ?>" id="lesson_id">
            <?php } ?>
        <?php } ?>

        </div>

        <?php
        $lesson_template_massive = get_post_meta($_GET['actual_lesson'], 'unit_template_data');
        foreach ($lesson_template_massive as $massive) {
            $unserialized_massive = maybe_unserialize($massive);
            break;
        }
        $template_name = $unserialized_massive['template_name'];
        $template = get_template_directory();
        $new_template = str_replace('\\', '/', $template);

        if ($template_name && !empty($template_name)) {
            ?>
            <div class="template-lesson-body">
                <div class="manual-lesson-switch">
                    <?php
                    /**
                     * Selecting all units from current module
                     * using $lesson_list, announced upper
                     */

                    $current_lesson_meta = get_unit_meta($_GET['actual_lesson']);
                    $current_lesson_number = $current_lesson_meta->unit_number;
                    if ($current_lesson_number == 1) {
                        $next_lesson = get_next_unit($current_lesson_number, $_GET['module_number']);
                        $lesson_title = get_the_title($_GET['actual_lesson']);
                        $current_module = get_module_by_id($_GET['module_number']);
                        $current_module_number = $current_module->module_number;

                        ?>
                        <div class="module-lesson-title">
                            <?php echo __('Module ') . $current_module_number . ': ' . $lesson_title; ?>
                        </div>
                        <?php
                        if ($next_lesson && !empty($next_lesson)) {
                            ?>
                            <span class="next-lesson">
                            <a href="#">Next</a>
                            <input type="hidden" value="<?php echo $next_lesson; ?>" class="next-lesson-value"/>
                        </span>

                        <?php
                        }

                    } elseif ($current_lesson_number == count($lesson_list)) {
                        $prev_lesson = get_prev_unit($current_lesson_number, $_GET['module_number']);
                        $lesson_title = get_the_title($_GET['actual_lesson']);
                        $current_module = get_module_by_id($_GET['module_number']);
                        $current_module_number = $current_module->module_number;
                        if ($prev_lesson && !empty($prev_lesson)) {
                            ?>
                            <span class="prev-lesson">
                            <a href="#">Prev</a>
                            <input type="hidden" value="<?php echo $prev_lesson; ?>" class="prev-lesson-value"/>
                        </span>
                        <?php
                        }
                        ?>
                        <div class="module-lesson-title">
                            <?php echo __('Module ') . $current_module_number . ': ' . $lesson_title; ?>
                        </div>
                    <?php
                    } else {
                        $next_lesson = get_next_unit($current_lesson_number, $_GET['module_number']);
                        $prev_lesson = get_prev_unit($current_lesson_number, $_GET['module_number']);
                        $lesson_title = get_the_title($_GET['actual_lesson']);
                        $current_module = get_module_by_id($_GET['module_number']);
                        $current_module_number = $current_module->module_number;
                        if ($prev_lesson && !empty($prev_lesson)) {
                            ?>
                            <span class="prev-lesson">
                            <a href="#">Prev</a>
                            <input type="hidden" value="<?php echo $prev_lesson; ?>" class="prev-lesson-value"/>
                        </span>
                        <?php
                        }
                        ?>
                        <div class="module-lesson-title">
                            <?php echo __('Module ') . $current_module_number . ': ' . $lesson_title; ?>
                        </div>

                        <?php
                        if ($next_lesson && !empty($next_lesson)) {
                            ?>
                            <span class="next-lesson">
                            <a href="#">Next</a>
                            <input type="hidden" value="<?php echo $next_lesson; ?>" class="next-lesson-value"/>
                        </span>

                        <?php
                        }
                    }
                    ?>
                </div>
                <div class="author-date-description">
                    <?php
                    $lesson_object = get_post($_GET['actual_lesson']);
                    $author_id = $lesson_object->post_author;
                    $author_name = get_the_author_meta('first_name', $author_id);

                    $updated_time = get_post_modified_time();
                    $updated_date = date('d F Y', $updated_time);

                    echo __('Interactive Lesson Completed: ') . '<span class="lesson-author-name">' . $author_name . '</span><span class="lesson-modify-date">' . __(' On: ') .
                        '<span>' . $updated_date . '</span>';
                    ?>

                </div>
                <?php
                include_once($new_template . '/' . $template_name);
                ?>
            </div>
        <?php
        }
        ?>
    <?php
    } else {
        ?>
        </div>
    <?php } ?>
<?php
} else {
    $pricing_page = edd_get_option('mct_edd_pricing_page_redirect');

    ?>
    <div class="error-msg">
        <?php echo __("You don't have the appropriate subscription to see this page. "); ?>
        <a href="<?php echo get_the_permalink($pricing_page); ?>"><?php echo __('Visit our pricing page for your subscription options.'); ?></a>
    </div>
<?php
}
    $result = ob_get_clean();
    return $result;

}

add_action('wp_enqueue_scripts', 'course_page_enqueue_scripts');
function course_page_enqueue_scripts()
{
    wp_enqueue_script('form_submitter', plugins_url('js/form_submitter.js', __FILE__), array('jquery'));
    wp_enqueue_script('ajax_email_send', plugins_url('js/ajax_email_send.js', __FILE__), array('jquery'));
    wp_localize_script('ajax_email_send', 'Ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

/**
 * @return WP_Query object
 */
function get_downloads_list()
{
    $args = array(
        'post_type' => 'download',
        'post_status' => 'publish'
    );

    $query = new WP_Query($args);

    return $query;
}

/**
 * @param int $course_id
 * @param string $meta_key
 *
 * @return bool|mixed
 */
function get_course_meta($course_id = 0, $meta_key = '')
{
    global $wpdb;

    if (!is_int($course_id)) {
        return FALSE;
    }

    if (!is_string($meta_key)) {
        return FALSE;
    }

    $table_name = $wpdb->prefix . "wpcw_courses_meta";

    $sql = $wpdb->get_var(" SELECT course_meta_value FROM " . $table_name . " WHERE course_id ='" . $course_id . "' AND course_meta_key ='" . $meta_key . "';");

    if ($sql && !empty($sql)) {
        $return = maybe_unserialize($sql);
    } else {
        return FALSE;
    }

    return $return;
}

/**
 * @param int $course_id
 * @param string $meta_key
 * @param        $meta_value
 *
 * @return bool
 */
function update_course_meta($course_id = 0, $meta_key = '', $meta_value)
{
    global $wpdb;

    if (!is_int($course_id)) {
        return FALSE;
    }

    if (!is_string($meta_key)) {
        return FALSE;
    }

    $table_name = $wpdb->prefix . "wpcw_courses_meta";

    $meta_value = maybe_serialize($meta_value);

    $updated_meta = get_course_meta($course_id, $meta_key);

    if (!$updated_meta) {
        $sql = $wpdb->insert(
            $table_name,
            array(
                'course_id' => $course_id,
                'course_meta_key' => $meta_key,
                'course_meta_value' => $meta_value
            ),
            array('%d', '%s', '%s')
        );
    } else {
        $sql = $wpdb->update(
            $table_name,
            array('course_meta_value' => $meta_value),
            array('course_id' => $course_id, 'course_meta_key' => $meta_key),
            array('%s'),
            array('%d', '%s')
        );
    }

    if ($sql) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * @param $download_id
 * @return array
 */
function get_courses_with_restrict($download_id)
{
    global $wpdb, $wpcwdb;
    $table_name = $wpdb->prefix . "wpcw_courses_meta";
    $course_list = WPCW_courses_getCourseList();
    $user_id = get_current_user_id();
    $return = array();
    foreach ($course_list as $course) {
        $checker = false;
        $sql = $wpdb->prepare("SELECT *
				FROM $wpcwdb->courses
				WHERE course_title=%d
				ORDER BY course_title
				", $course);
        $items = $wpdb->get_results($sql);

        if ($items && !empty($items)) {
            foreach ($items as $item) {

                if ($item->course_title == $course) {
                    $course_id = (int)$item->course_id;
                    $course_restrictions = get_course_meta($course_id, '_edd_cr_restricted_to');
                    if ($course_restrictions && !empty($course_restrictions)) {
                        foreach ($course_restrictions as $restriction) {
                            if ($restriction['download'] == $download_id) {
                                $checker = true;
                            }
                        }
                        if ($checker == true) {
                            $return[]['course_id'] = $item->course_id;
                        }
                    }
                }
            }
        }
    }
    return $return;
}

/**
 * @param $download_id
 * @return bool
 */
function get_restricted_downloads($download_id)
{
    global $wpdb, $wpcwdb;

    $checker = false;
    $course_list = WPCW_courses_getCourseList();
    foreach ($course_list as $course) {

        $sql = $wpdb->prepare("SELECT *
				FROM $wpcwdb->courses
				WHERE course_title=%d
				ORDER BY course_title
				", $course);
        $items = $wpdb->get_results($sql);

        foreach ($items as $item) {
            if ($item->course_title == $course) {
                $module_details = WPCW_courses_getModuleDetailsList($item->course_id);
                if ($module_details && !empty($module_details)) {
                    foreach ($module_details as $module) {
                        $units_list = WPCW_units_getListOfUnits($module->module_id);
                        if ($units_list && !empty($units_list)) {
                            foreach ($units_list as $unit) {
                                $restrict = get_post_meta($unit->ID, '_edd_cr_restricted_to', false);
                                if (isset($restrict[0][0]['download']) && (string)$restrict[0][0]['download'] == (string)$download_id) {
                                    $checker[$download_id]['bool'] = true;
                                    $checker[$download_id]['unit_id'][] = $unit->ID;
                                    $checker[$download_id]['restricted'] = edd_cr_is_restricted($unit->ID);
                                } elseif (isset($restrict[0]['download']) && (string)$restrict[0]['download'] == (string)$download_id) {
                                    $checker[$download_id]['bool'] = true;
                                    $checker[$download_id]['unit_id'][] = $unit->ID;
                                    $checker[$download_id]['restricted'] = edd_cr_is_restricted($unit->ID); //Check number of downloads for which unit is restricted
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $checker;
}

/**
 * Getting all meta data of unit from table units_meta
 * @param $unit_id
 * @return bool
 */
function get_unit_meta($unit_id)
{
    global $wpdb, $wpcwdb;

    $sql = $wpdb->prepare("SELECT * FROM $wpcwdb->units_meta WHERE unit_id = %d", (int)$unit_id);
    $unit_meta = $wpdb->get_results($sql);

    if (isset($unit_meta[0]) && !empty($unit_meta[0])) {
        return $unit_meta[0];
    } else {
        return false;
    }
}

function get_next_unit($unit_number, $module_id)
{
    global $wpdb, $wpcwdb;
    $next_unit_number = $unit_number + 1;

    $sql = $wpdb->prepare("SELECT `unit_id` FROM $wpcwdb->units_meta WHERE unit_number = %d AND parent_module_id = %d", $next_unit_number, $module_id);
    $next_unit_id = $wpdb->get_results($sql);

    if (isset($next_unit_id[0]->unit_id) && !empty($next_unit_id[0]->unit_id)) {
        return $next_unit_id[0]->unit_id;
    } else {
        return false;
    }
}

function get_prev_unit($unit_number, $module_id)
{
    global $wpdb, $wpcwdb;
    $next_unit_number = $unit_number - 1;

    $sql = $wpdb->prepare("SELECT `unit_id` FROM $wpcwdb->units_meta WHERE unit_number = %d AND parent_module_id = %d", $next_unit_number, $module_id);
    $next_unit_id = $wpdb->get_results($sql);

    if (isset($next_unit_id[0]->unit_id) && !empty($next_unit_id[0]->unit_id)) {
        return $next_unit_id[0]->unit_id;
    } else {
        return false;
    }
}

/*********************************************************
 **** EMAIL Sender
 *********************************************************/

function ajax_send_course_by_email()
{
    if (isset($_POST['lesson_id']) && !empty($_POST['lesson_id'])) {
        $lesson_object = get_post($_POST['lesson_id']);
        $author_id = $lesson_object->post_author;

        $author_first_name = get_user_meta($author_id, 'first_name', true);;
        $author_last_name = get_user_meta($author_id, 'last_name', true);;
        $author_full_name = ucfirst($author_first_name) . ' ' . ucfirst($author_last_name);

        $unit = get_unit_by_unit_id($_POST['lesson_id']);
        $unit_number = $unit->unit_number;
        $user_id = get_current_user_id();
        $user_data = get_userdata($user_id);
        $user_email = $user_data->data->user_email;
        $user_first_name = get_user_meta($user_id, 'first_name', true);
        $user_last_name = get_user_meta($user_id, 'last_name', true);
        $username = $user_first_name . ' ' . $user_last_name;

        $course_id = $_POST['course_id'];
        $course = get_course_by_id($course_id);
        $course_title = $course->course_title;

        $module_id = $_POST['module_id'];
        $module = get_module_by_id($module_id);
        $module_title = $module->module_title;

        $lesson_content = $lesson_object->post_content;
        $lesson_title = $lesson_object->post_title;

        $lesson_metadata = get_post_meta($_POST['lesson_id'], 'unit_template_data');
        if (isset($lesson_metadata[0]) && !empty($lesson_metadata[0])) {
            $massive = maybe_unserialize($lesson_metadata[0]);
            if (isset($massive) && !empty($massive)) {
                if ((int)$unit_number == 1) {
                    $single_video_name = $massive['video_name'];
                    $single_video_link = $massive['video_link'];
                    $module_link = $massive['module_link'];
                    $module_learning_outcomes = $massive['module_learning_outcomes'];
                    $dialogue = $massive['dialogue'];
                    $written_explanations = $massive['written_explanations'];
                    $interesting_links = get_post_meta($_POST['lesson_id'], 'unit_template_interesting_links', true);
                    $quiz_id = $massive['quiz_id'];
                    $external_quiz_url = $massive['quiz_url'];
                    $pronunciation_video_name = $massive['pr_video_name'];
                    $pronunciation_video_link = $massive['pr_video_link'];
                    $lesson_recap_name = $massive['lr_video_name'];
                    $lesson_recap_link = $massive['lr_video_link'];
                    $files = get_attached_media('', $_POST['lesson_id']);

                    $vocabulary_list_metadata = get_post_meta($_POST['lesson_id'], 'vocab_list');
//                    if (isset($vocabulary_list_metadata[0]) && !empty($vocabulary_list_metadata[0])) {
//                        $vocabulary_list = maybe_unserialize($vocabulary_list_metadata[0]);
//                        $vocab_array = array();
//
//                        foreach ($vocabulary_list as $vocabulary) {
//                            $vocab_array['english'][] = $vocabulary[0];
//                            $vocab_array['pinyin'][] = $vocabulary[1];
//                            $vocab_array['chinese'][] = $vocabulary[2];
//                            $vocab_array['video_link'][] = $vocabulary[3];
//                        }
//                    }
                    //Enabling PHP-Mailer

                    require_once(plugin_dir_path(__FILE__) . 'libs/phpmailer/PHPMailerAutoload.php');

                    $Letter = new PHPMailer();
                    $Letter->isHTML(true);
                    $Letter->From = 'info@myeducationgroup.org';
                    $Letter->FromName = 'MCT_Admin';
                    $Letter->addAddress($user_email, $username);

                    $Letter->Subject = __('My Chinese Hub: ', 'mch_conf') . $lesson_title;
                    $Letter->Body = '<p>Hi ' . $user_first_name . ', <br />';
                    $Letter->Body .= __('We are starting a new module', 'mch_conf') . ' - ' . $module_title . '<br /></p>';

                    mch_set_attachments($files, $Letter, $user_id);

                    if (isset($interesting_links) && !empty($interesting_links)) {
                        $Letter->Body .= '<p>' . __('Here are some other links that your class might be interested in:', 'mch_conf') . '</p>';
                        $Letter->Body .= $interesting_links;
                    }

                    $Letter->Body .= __('Please find attached some learning material, including learning outcomes and workbooks for the module.', 'mch_conf') . '<br />';

                    $Letter->Body .= '<p>' . __('At the end of the module, your students can also test what they have learnt by completing the following online test: ', 'mch_comf') . $module_link . '<br /></p>';
                    $Letter->Body .= '<p>' . __('If you just finished a module, then make sure you test your students with last module\'s test!', 'mch_conf') . '<br /></p>';
                    $Letter->Body .= '<p>' . __('If you have any questions or concerns, please don\'t hesitate to get in contact.', 'mch_conf') . '<br /></p>';
                    $Letter->Body .= '<p>' . __('Kind regards,') . '<br />';
                    $Letter->Body .= $author_full_name . __(' The My Chinese Teacher team') . '</p>';

                    if (!$Letter->send()) {
                        echo 'Message could not be sent.';
                        echo 'Mailer Error: ' . $Letter->ErrorInfo;
                    } else {
                        echo 'Message has been sent';
                    }
                } else {
                    $single_video_name = $massive['video_name'];
                    $single_video_link = $massive['video_link'];
                    $module_link = $massive['module_link'];
                    $module_learning_outcomes = $massive['module_learning_outcomes'];
                    $dialogue = $massive['dialogue'];
                    $written_explanations = $massive['written_explanations'];
                    $interesting_links = get_post_meta($_POST['lesson_id'], 'unit_template_interesting_links', true);
                    $quiz_id = $massive['quiz_id'];
                    $external_quiz_url = $massive['quiz_url'];
                    $pronunciation_video_name = $massive['pr_video_name'];
                    $pronunciation_video_link = $massive['pr_video_link'];
                    $lesson_recap_name = $massive['lr_video_name'];
                    $lesson_recap_link = $massive['lr_video_link'];

                    $files = get_attached_media('', $_POST['lesson_id']);

//                    $vocabulary_list_metadata = get_post_meta($_POST['lesson_id'], 'vocab_list');
//                    if (isset($vocabulary_list_metadata[0]) && !empty($vocabulary_list_metadata[0])) {
//                        $vocabulary_list = maybe_unserialize($vocabulary_list_metadata[0]);
//                        $vocab_array = array();
//
//                        foreach ($vocabulary_list as $vocabulary) {
//                            $vocab_array['english'][] = $vocabulary[0];
//                            $vocab_array['pinyin'][] = $vocabulary[1];
//                            $vocab_array['chinese'][] = $vocabulary[2];
//                            $vocab_array['video_link'][] = $vocabulary[3];
//                        }
//                    }
                    //Enabling PHP-Mailer

                    require_once(plugin_dir_path(__FILE__) . 'libs/phpmailer/PHPMailerAutoload.php');

                    $Letter = new PHPMailer();
                    $Letter->isHTML(true);
                    $Letter->From = 'info@myeducationgroup.org';
                    $Letter->FromName = 'MCT_Admin';
                    $Letter->addAddress($user_email, $username);
                    mch_set_attachments($files, $Letter, $user_id);

                    $Letter->Subject = __('My Chinese Hub: ', 'mct_hub') . $lesson_title;
                    $Letter->Body = '<p>' . __('Hi ', 'mch_conf') . $user_first_name . ',' . '<br />';
                    $Letter->Body .= __('Next lesson we will be covering - ', 'mch_conf') . $course_title . ' - ' . $module_title . ' - ' . $lesson_title . '<br /></p>';

                    /**
                     * attaching single video link to letter
                     */
                    if ($single_video_link && !empty($single_video_link)) {
                        $Letter->Body .= '<p>' . __('Here are the links to following videos:', 'mch_conf') . '<br />';
                        if ($single_video_name && !empty($single_video_name)) {
                            $Letter->Body .= '<a href="' . $single_video_link . '">' . $single_video_name . '</a><br /></p>';
                        } else {
                            $Letter->Body .= '<a href="' . $single_video_link . '">' . $single_video_link . '</a><br /></p>';
                        }
                    }

                    /**
                     * Attaching video links to letter
                     */
                    if ((isset($pronunciation_video_link) && !empty($pronunciation_video_link)) || (isset($lesson_recap_link) && !empty($lesson_recap_link))) {
                        $Letter->Body .= '<p>' . __('Here are the links to following videos:', 'mch_conf') . '<br />';
                        if (isset($lesson_recap_link) && !empty($lesson_recap_link)) {
                            if (isset($lesson_recap_name) && !empty($lesson_recap_name)) {
                                $Letter->Body .= '<a href="' . $lesson_recap_link . '">' . $lesson_recap_name . '</a><br />';
                            } else {
                                $Letter->Body .= '<a href="' . $lesson_recap_link . '">' . $lesson_recap_link . '</a><br />';
                            }
                        }

                        if (isset($pronunciation_video_link) && !empty($pronunciation_video_link)) {
                            if (isset($pronunciation_video_name) && !empty($pronunciation_video_name)) {
                                $Letter->Body .= '<a href="' . $pronunciation_video_link . '">' . $pronunciation_video_name . '</a><br />';
                            } else {
                                $Letter->Body .= '<a href="' . $pronunciation_video_link . '">' . $pronunciation_video_link . '</a><br />';
                            }
                        }
                        $Letter->Body .= '</p>';
                    }

                    if (isset($interesting_links) && !empty($interesting_links)) {
                        $Letter->Body .= '<p>' . __('Here are some other links that your class might be interested in:', 'mch_conf') . '</p>';
                        $Letter->Body .= $interesting_links;
                    }

                    $Letter->Body .= '<p>' . __('You can also find the flashcards and pictures attached.', 'mch_conf') . '<br /></p>';
                    $Letter->Body .= '<p>' . __('If you have any questions or concerns, please don\'t hesitate to send us a question.', 'mch_conf') . '<br /></p>';
                    $Letter->Body .= '<p>' . __('Kind regards,') . '<br />';
                    $Letter->Body .= $author_full_name . __(' The My Chinese Teacher team') . '</p>';

                    if (!$Letter->send()) {
                        echo 'Message could not be sent.';
                        echo 'Mailer Error: ' . $Letter->ErrorInfo;
                    } else {
                        echo 'Message has been sent';
                    }
                }
            }
        }
    }
    die();
}

add_action('wp_ajax_ajax_send_course_by_email', 'ajax_send_course_by_email');
