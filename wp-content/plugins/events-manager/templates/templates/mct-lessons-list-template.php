<?php
/*
 * Tamplate for display bookings list, using with shortcode mct_lessons_list;
 */
?>
<div class="lessons-block">
    <div class="lessons-block-title">
        <?php if($args['display'] == 'slide'):?>
            <a href="#" class="block-slide-down e-btn"><?php print $title; ?></a>
        <?php else: ?>
            <h3><?php print $title; ?></h3>
        <?php endif; ?>
    </div>
    <div class="lessons <?php !empty($args['display']) ? print $args['display'] : ''; ?>">
        <div class="users">
            <?php if(!empty($events) && is_array($events)): ?>
                <?php foreach($events as $event_id => $event): ?>
                    <?php if(!empty($event['users']) && count($event['users']) > 0): ?>
                        <?php foreach($event['users'] as $uid => $user): ?>
                            <div class="user-block-wrap">
                                <div class="user-block" id="event-<?php print $event_id; ?>">
                                    <div class="event-time">
                                        <?php print date('dM Y, D h:i a', dut_prepare_date($event['event_start_date'] . ' ' . $event['event_start_time'], dut_get_user_timezone())); ?>
                                    </div>
                                    <div class="student-top-box">
                                        <div class="user-photo">
                                            <?php print get_avatar( $uid, 64 ); ?>
                                        </div>
                                        <div class="user-info-box">
                                            <div class="user-name"><?php print $user->data->nickname; ?></div>
                                            <div class="user-message"><?php print $user->data->message; ?></div>
                                        </div>
                                        <div class="open-info"><?php _e('i'); ?></div>
                                    </div>
                                    <div class="hide-student-info-block student-bottom-box">
                                        <div class="student-info">
                                            <?php $skype_name = get_user_meta($uid, 'skype_name', true); ?>
                                            <?php if(!empty($skype_name)): ?>
                                                <div class="contact-info-item"><?php print __('Skype: ') . $skype_name; ?></div>
                                            <?php endif; ?>
                                            <div class="contact-info-item"><?php print __('Email: ') . $user->data->user_email; ?></div>
                                            </div>
                                        <?php if(!empty($user->data->history)): ?>
                                            <!-- lesson history block -->
                                            <div class="lessons-history lesson-history-wrapper">
                                                <span class="lesson-history-topic"><?php print __( 'Lesson history:' ); ?></span>
                                                <div class="lesson-history-buttons">
                                                    <div class="lessons-loop">
                                                <?php foreach($user->data->history as $history): ?>
                                                    <?php $teacher_first_name = get_user_meta( $history['author_id'], 'first_name', true ); ?>
                                                    <?php $teacher_last_name  = get_user_meta( $history['author_id'], 'last_name', true );  ?>
                                                    <div class="history-wrapper" id="history-<?php print $history['event_id']; ?>">
                                                        <div class="history-item-wrapper">
                                                            <a href="#modal-wrap-history-<?php print $history['event_id']; ?>" class="mpf-link">
                                                                <?php echo $history['lesson_date']; ?>
                                                            </a>
                                                        </div>
                                                        <!-- History popup box -->
                                                        <div class="history-popup-box mfp-hide" id="modal-wrap-history-<?php print $history['event_id']; ?>">
                                                            <h2><?php echo __( 'Lesson history: ' ); ?></h2>
                                                            <div class="history-popup-content">
                                                                <div class="history_course_id item-history">
                                                                    <span><?php echo __( 'Course: ' ); ?></span>
                                                                    <?php echo $history['course_group']; ?>
                                                                </div>
                                                                <div class="history_level_id item-history">
                                                                    <span><?php echo __( 'Level: ' ); ?></span>
                                                                    <?php echo $history['course_level']; ?>
                                                                </div>
                                                                <div class="history_module_id item-history">
                                                                    <span><?php echo __( 'Module: ' );?></span>
                                                                    <?php echo $history['module_id']; ?>
                                                                </div>
                                                                <div class="history_lesson_id item-history">
                                                                    <span><?php echo __( 'Lesson: ' ); ?></span>
                                                                    <?php echo $history['lesson_id']; ?>
                                                                </div>
                                                                <div class="history-comment item-history">
                                                                    <span><?php echo __( 'Comment: ' ); ?></span>
                                                                    <?php echo $history['comment']; ?>
                                                                </div>
                                                                <?php if(!empty($history_date)):?>
                                                                    <div class="history-complete-lesson-time item-history">
                                                                        <span><?php echo __( 'Lesson date:' ); ?></span>
                                                                        <?php echo $history_date; ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="history-taecher-name item-history">
                                                                    <span><?php echo __( 'Teacher: ' ); ?></span>
                                                                    <?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End history popup box -->
                                                    </div>
                                                <?php endforeach; ?>
                                                    </div>
                                                    <div class="all-complete-lessons-wrapper">
                                                        <div class="show-full-history-button">
                                                            <input type="button" class="all-lessons-history show-all-lessons-button e-btn-show-lesson" name="lessons-history" value="<?php print __('Show all history'); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End lesson history block -->
                                        <?php endif; ?>
                                        <!-- User course info -->
                                        <?php if(!empty($user->data->history) || !empty($user->data->courses)):?>
                                            <div class="lesson-number-block-wrapper">
                                                <div class="wrap-select-block m-one-row course-group-wrapper">
                                                    <div class="title-block">
                                                        <?php print __( 'Course: ' ); ?>
                                                    </div>
                                                    <div class="select-block course-removed-part">
                                                        <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                            <span><?php print $user->data->history[0]['course_group'];?></span>
                                                        <?php else: ?>
                                                            <?php if(!empty($user->data->courses) && is_array($user->data->courses)): ?>
                                                                <select class="courses-group-select" name="courses-group-select">
                                                                    <?php foreach($user->data->courses as $course_info): ?>
                                                                        <option value="<?php print $course_info['course_group_id']; ?>"><?php print $course_info['course_name']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="wrap-select-block path-level-wrapper">
                                                    <div class="title-block">
                                                        <?php print __( 'Pathway/level:' ); ?>
                                                    </div>
                                                    <div class="select-block ch-select">
                                                        <div class="level-removed-part">
                                                            <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                                <span><?php print $user->data->history[0]['course_level'];?></span>
                                                            <?php else: ?>
                                                                <?php if(!empty($user->data->courses) && is_array($user->data->courses)): ?>
                                                                    <?php $courses_info = array_values($user->data->courses); ?>
                                                                    <select class="path-level-select" name="path-level-select">
                                                                        <?php foreach($courses_info[0]['levels'] as $level_id => $level_number): ?>
                                                                            <option value="<?php print $level_id; ?>"><?php print $level_number; ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wrap-select-block module-id-wrapper">
                                                    <div class="title-block">
                                                        <?php print __( 'Module: ' ); ?>
                                                    </div>
                                                    <div class="select-block ch-select">
                                                        <div class="module-removed-part">
                                                            <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                                <span><?php print $user->data->history[0]['module_id'];?></span>
                                                            <?php else: ?>
                                                                <select class="module-select" name="module-select">
                                                                    <?php if(!empty($user->data->courses)): ?>
                                                                        <?php $courses_info = array_values($user->data->courses); ?>
                                                                        <?php foreach($courses_info[0]['modules'] as $module): ?>
                                                                            <option value="<?php print $module['module_id']; ?>"><?php print $module['module_number']; ?></option>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </select>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wrap-select-block m-fr lesson-id-wrapper">
                                                    <div class="title-block">
                                                        <?php print __( 'Lesson: ' ); ?>
                                                    </div>
                                                    <div class="select-block ch-select">
                                                        <div class="lesson-removed-part">
                                                            <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                                <span><?php print $user->data->history[0]['lesson_id'];?></span>
                                                            <?php else: ?>
                                                                <?php $courses_info = array_values($user->data->courses); ?>
                                                                <select class="lesson-select" name="lesson-select" >
                                                                    <?php if(!empty($courses_info[0]['lessons'])): ?>
                                                                        <?php foreach($courses_info[0]['lessons'] as $lesson): ?>
                                                                            <option value="<?php print $lesson['unit_number']; ?>"><?php print $lesson['unit_number']; ?></option>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </select>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php if(empty($args['complete']) && !empty($args['scope'])):?>
                                                            <div class="plus-button-wrapper">
                                                                <input type="button" class="e-btn-plus plus-button" name="save-user-info" value="<?php print __('Save user info'); ?>">
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="lesson-comment-wrapper">
                                                <?php if((empty($args['scope']) || $args['scope'] !== 'past') || !empty($args['complete'])): ?>
                                                    <div class="title-block">
                                                        <?php print __( 'Comment: ' ); ?>
                                                    </div>
                                                    <div class="select-block lesson-comment">
                                                        <?php !empty($user->data->history[0]['comment']) ? print $user->data->history[0]['comment'] : ''; ?>
                                                    </div>
                                                <?php else: ?>
                                                    <textarea class="lesson-comment" cols="30" rows="6"></textarea>
                                                <?php endif; ?>
                                            </div>
                                            <!-- End user course info -->
                                        <?php endif; ?>
                                        <input type="hidden" value="<?php print $uid; ?>" name="user_id" class="user-id">
                                        <input type="hidden" value="<?php print get_current_user_id(); ?>" name="teacher_id" class="teacher-id">
                                        <input type="hidden" value="<?php print $event_id; ?>" name="event_id" class="event-id">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php print __('Lessons not found'); ?>
            <?php endif;?>
        </div>
    </div>
</div>