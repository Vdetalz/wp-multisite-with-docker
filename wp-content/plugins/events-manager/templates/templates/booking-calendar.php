<?php
/*
 * This file contains the HTML generated for full calendars. You can copy this file to yourthemefolder/plugins/events-manager/templates and modify it in an upgrade-safe manner.
 *
 * There are two variables made available to you:
 *
 * 	$calendar - contains an array of information regarding the calendar and is used to generate the content
 *  $args - the arguments passed to EM_Calendar::output()
 *
 * Note that leaving the class names for the previous/next links will keep the AJAX navigation working.
 */
$cal_count = count($calendar['cells']); //to prevent an extra tr
$col_count = $tot_count = 1; //this counts collumns in the $calendar_array['cells'] array
$col_max = count($calendar['row_headers']); //each time this collumn number is reached, we create a new collumn, the number of cells should divide evenly by the number of row_headers
?>
<?php if($cal_count > 0): ?>
	<?php if (!empty($calendar['em_date'])): ?>
		<input type="hidden" name="em_cur_ajax_data" value="<?php echo $calendar['em_date']; ?>"/>
	<?php endif; ?>
    <div class="em-calendar-wrapper em-calendar fullcalendar b-calendar fixedHolder" id="triggerScroll">
        <div class="b-cal-header fixedHolder-item js-sticky-header">
            <ul class="cal-controls">
                <li class="cal-prev">
                    <a data-date="<?php echo $calendar['weeks']['prev_week']; ?>" href="" class="mc-calnav full-link em-calnav-prev fa fa-angle-left fa-2x"></a>
                </li>
                <li class="cal-next">
                    <a  data-date="<?php echo $calendar['weeks']['next_week']; ?>" href="" class="mc-calnav full-link em-calnav-next fa fa-angle-right fa-2x"></a>
                </li>
            </ul>
            <div class="cl-title month_name"><?php echo ucfirst(date_i18n('d M', $calendar['start_day'])) . ' - ' . ucfirst(date_i18n('d M, Y', $calendar['end_day'])); ?></div>
        </div>

        <div class="cl-time-wrap">
            <div class="cl-time-cols cl-time-header fixedHolder-item fixedHolder-itemBottom js-sticky-header-1">
                <?php foreach($calendar['cells'] as $date => $cell_data ): ?>
                    <div class="cl-time-col">
                        <span><?php echo date('D, d', strtotime($date)); ?></span>
                        <span class="cl-time-col-month"><?php echo date('M', strtotime($date)); ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php foreach($calendar['periods'] as $period => $intervals): $is_open = FALSE; ?>
                <div class="cl-time-block">
                    <div class="cl-time-block-top calendar_block_wrap">
                        <div class="cl-time-cols">
                            <div class="cl-time-col m-full">
                                <a href="#" class="js-cal-time-toggle"><?php print MCT_EM_Calendar::get_time_period($period); ?><i class="e-toggle-arrow fa fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="cl-time-block-bottom">
                    <?php foreach($intervals as $interval): ?>
                        <div  class="cl-time-cols">
                            <?php foreach($calendar['cells'] as $day => $events ): ?>
                                <?php if( !empty($events[$period][$interval]['events']) && count($events[$period][$interval]['events']) > 0 ): ?>
                                    <?php foreach($events[$period][$interval]['events'] as $time => $event): ?>
                                        <div id="event_<?php echo $event->event_id; ?>" class="cl-time-col event_<?php echo $event->event_id; ?>">
                                            <?php if(!empty($event->get_bookings()->bookings[0])): ?>
                                                <a href="" class="taken"><?php _e('Taken', 'mct_tutor'); ?></a>
                                            <?php else: $user_timezone = dut_get_user_timezone(); ?>
                                                <a href="#" data-event-id="<?php echo $event->event_id; ?>" data-event-date="<?php echo date('D, d M, Y', dut_prepare_date(date('Y-m-d H:i', $time), $user_timezone)); ?>" class="event_author_link del-lesson add-booking" ><?php print MCT_EM_Calendar::get_time_interval(dut_prepare_date(date('Y-m-d H:i', $time), $user_timezone), $calendar['lesson_type']); ?></a> <!--add-booking-->
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php if(!$is_open):
                                        $is_open = TRUE; ?>
                                        <script>
                                            var el_cl_time_block_bottom = jQuery('.event_<?php echo $event->event_id ?>').parent().parent();
                                            el_cl_time_block_bottom.parent().toggleClass('is-open').find('.e-toggle-arrow').toggleClass('fa-rotate-90');
                                            el_cl_time_block_bottom.show();
                                        </script>
                                    <?php endif ?>
                                <?php else: ?>
                                    <div class="cl-time-col"><a href="" class="na tutors_na"><?php _e('N/A', 'mct_tutor'); ?></a></div>
                                <?php endif;?>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
