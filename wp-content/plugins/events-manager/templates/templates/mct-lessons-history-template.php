<?php
/*
 * Tamplate for display lessons history list, using with shortcode mct_lessons_history;
 */
?>
<div class="lessons-block">
    <div class="lessons-block-title">
         <a href="#" class="block-slide-down e-btn"><?php print $title; ?></a>
    </div>
    <div class="lessons">
        <div class="users">
            <?php if(!empty($users)): ?>
                <?php foreach($users as $user_id => $user): ?>
                    <div class="user-block-wrap">
                        <div class="user-block" id="user-<?php print $user_id; ?>">
                            <div class="event-time">
                                <?php print date('dM Y, D h:i a', dut_prepare_date($user['complete_date'] . ' ' . $user['complete_time'], dut_get_user_timezone())); ?>
                            </div>
                            <div class="student-top-box">
                                <div class="user-photo">
                                    <?php print get_avatar( $user_id, 64 ); ?>
                                </div>
                                <div class="user-info-box">
                                    <div class="user-name"><?php print $user['nickname']; ?></div>
                                    <div class="user-message"><?php print $user['message']; ?></div>
                                </div>
                                <div class="open-info"><?php _e('i'); ?></div>
                            </div>
                            <div class="hide-student-info-block student-bottom-box">
                                <div class="student-info">
                                    <?php if(!empty($user['skype'])): ?>
                                        <div class="contact-info-item"><?php print __('Skype: ') . $user['skype']; ?></div>
                                    <?php endif; ?>
                                    <div class="contact-info-item"><?php print __('Email: ') . $user['email']; ?></div>
                                </div>
                                <?php if(!empty($user['history'])): ?>
                                    <!-- lesson history block -->
                                    <div class="lessons-history lesson-history-wrapper">
                                        <span class="lesson-history-topic"><?php print __( 'Lesson history:' ); ?></span>
                                        <div class="lesson-history-buttons">
                                            <div class="lessons-loop">
                                                <?php foreach($user['history'] as $history): ?>
                                                    <?php $teacher_first_name = get_user_meta( $history['author_id'], 'first_name', true ); ?>
                                                    <?php $teacher_last_name  = get_user_meta( $history['author_id'], 'last_name', true );  ?>
                                                    <div class="history-wrapper" id="history-<?php print $history['event_id']; ?>">
                                                        <div class="history-item-wrapper">
                                                            <a href="#modal-wrap-history-<?php print $history['event_id']; ?>" class="mpf-link">
                                                                <?php echo $history['lesson_date']; ?>
                                                            </a>
                                                        </div>
                                                        <!-- History popup box -->
                                                        <div class="history-popup-box mfp-hide" id="modal-wrap-history-<?php print $history['event_id']; ?>">
                                                            <h2><?php echo __( 'Lesson history: ' ); ?></h2>
                                                            <div class="history-popup-content">
                                                                <div class="history_course_id item-history">
                                                                    <span><?php echo __( 'Course: ' ); ?></span>
                                                                    <?php echo $history['course_group']; ?>
                                                                </div>
                                                                <div class="history_level_id item-history">
                                                                    <span><?php echo __( 'Level: ' ); ?></span>
                                                                    <?php echo $history['course_level']; ?>
                                                                </div>
                                                                <div class="history_module_id item-history">
                                                                    <span><?php echo __( 'Module: ' );?></span>
                                                                    <?php echo $history['module_id']; ?>
                                                                </div>
                                                                <div class="history_lesson_id item-history">
                                                                    <span><?php echo __( 'Lesson: ' ); ?></span>
                                                                    <?php echo $history['lesson_id']; ?>
                                                                </div>
                                                                <div class="history-comment item-history">
                                                                    <span><?php echo __( 'Comment: ' ); ?></span>
                                                                    <?php echo $history['comment']; ?>
                                                                </div>
                                                                <?php if(!empty($history_date)):?>
                                                                    <div class="history-complete-lesson-time item-history">
                                                                        <span><?php echo __( 'Lesson date:' ); ?></span>
                                                                        <?php echo $history_date; ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="history-taecher-name item-history">
                                                                    <span><?php echo __( 'Teacher: ' ); ?></span>
                                                                    <?php echo $teacher_first_name . ' ' . $teacher_last_name; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End history popup box -->

                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="all-complete-lessons-wrapper">
                                                <div class="show-full-history-button">
                                                    <input type="button" class="all-lessons-history show-all-lessons-button e-btn-show-lesson" name="lessons-history" value="<?php print __('Show all history'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End lesson history block -->
                                <?php endif; ?>
                                <!-- User course info -->
                                <?php if(!empty($user['history'])):?>
                                    <div class="lesson-number-block-wrapper">
                                        <div class="wrap-select-block m-one-row course-group-wrapper">
                                            <div class="title-block">
                                                <?php print __( 'Course: ' ); ?>
                                            </div>
                                            <div class="select-block course-removed-part">
                                                <?php if(!empty($user['history'][0]['course_group'])): ?>
                                                    <span><?php print $user['history'][0]['course_group'];?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="wrap-select-block path-level-wrapper">
                                            <div class="title-block">
                                                <?php print __( 'Pathway/level:' ); ?>
                                            </div>
                                            <div class="select-block ch-select">
                                                <div class="level-removed-part">
                                                    <?php if(!empty($user['history'][0]['course_group'])): ?>
                                                        <span><?php print $user['history'][0]['course_level']; ?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wrap-select-block module-id-wrapper">
                                            <div class="title-block">
                                                <?php print __( 'Module: ' ); ?>
                                            </div>
                                            <div class="select-block ch-select">
                                                <div class="module-removed-part">
                                                    <?php if(!empty($user['history'][0]['module_id'])): ?>
                                                        <span><?php print $user['history'][0]['module_id'];?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wrap-select-block m-fr lesson-id-wrapper">
                                            <div class="title-block">
                                                <?php print __( 'Lesson: ' ); ?>
                                            </div>
                                            <div class="select-block ch-select">
                                                <div class="lesson-removed-part">
                                                    <?php if(!empty($user['history'][0]['lesson_id'])): ?>
                                                        <span><?php print $user['history'][0]['lesson_id'];?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="lesson-comment-wrapper">
                                        <?php if(!empty($user['history'][0]['comment'])): ?>
                                            <div class="title-block">
                                                <?php print __( 'Comment: ' ); ?>
                                            </div>
                                            <div class="select-block lesson-comment">
                                                <?php !empty($user['history'][0]['comment']) ? print $user['history'][0]['comment'] : ''; ?>
                                            </div>
                                        <?php else: ?>
                                            <textarea class="lesson-comment" cols="30" rows="6"></textarea>
                                        <?php endif; ?>
                                    </div>
                                    <!-- End user course info -->
                                <?php endif; ?>
                                <input type="hidden" value="<?php print $user_id; ?>" name="user_id" class="user-id">
                                <input type="hidden" value="<?php print get_current_user_id(); ?>" name="teacher_id" class="teacher-id">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php if(!empty($pagination)){
                    print $pagination;
                }
                ?>
            <?php else: ?>
                <?php print __('History not found', 'mct');?>
            <?php endif;?>
        </div>
    </div>
</div>