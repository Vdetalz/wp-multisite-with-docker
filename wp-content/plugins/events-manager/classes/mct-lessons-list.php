<?php
class MCT_Lessons_List extends EM_Booking {

	public static function init(){
        add_action( 'wp_enqueue_scripts', array('MCT_Lessons_List', 'upcoming_lessons_enqueue_scripts'));
        add_action('em_bookings_added', array('MCT_Lessons_List', 'add_booking_meta'));
	}

	public function get($args = array()){
	 	global $wpdb;

        $bookings = array();
        $events_list = array();
        $EM_Events = EM_Events::get($args);
        foreach( $EM_Events as $EM_Event ){
            foreach( $EM_Event->get_bookings() as $EM_Booking ){
                if (($EM_Booking->status) == 1){
                    $events_list[$EM_Event->event_id]['users'][$EM_Booking->person->ID] = $EM_Booking->person;
                    $events_list[$EM_Event->event_id]['users'][$EM_Booking->person->ID]->data->nickname = get_user_meta($EM_Booking->person->ID, 'nickname', true);
                    $events_list[$EM_Event->event_id]['users'][$EM_Booking->person->ID]->data->message = $EM_Booking->booking_comment;
                    $events_list[$EM_Event->event_id]['users'][$EM_Booking->person->ID]->data->history = self::get_subscriber_history($EM_Booking->person->ID);

                    if((!empty($args['scope']) && $args['scope'] == 'past') && empty($args['complete'])){
                        $events_list[$EM_Event->event_id]['users'][$EM_Booking->person->ID]->data->courses = self::get_user_courses_info($EM_Booking->person->ID);
                    }

                    $events_list[$EM_Event->event_id]['event_start_date'] = $EM_Event->event_start_date;
                    $events_list[$EM_Event->event_id]['event_start_time'] = $EM_Event->event_start_time;
                    $events_list[$EM_Event->event_id]['event_end_time'] = $EM_Event->event_end_time;
                }
            }
        }
        /*  add condition for complete lessons or lessons that need feedback message      */
        if(!empty($args['scope']) && $args['scope'] == 'past'){
            $events_table = EM_EVENTS_TABLE;
            $locations_table = EM_LOCATIONS_TABLE;
            $teachers_user_list_table = $wpdb->prefix . 'teachers_user_list';
            $sql = "SELECT event_id FROM $teachers_user_list_table";
            $events_id = $wpdb->get_results($sql);

            if(!empty($events_id)){
                foreach($events_id as $id){
                    $complete_events_id[$id->event_id] = 'complete';
                }

                if(empty($args['complete'])){
                    $events_list = array_diff_key($events_list, $complete_events_id);
                }else{
                    $events_list = array_intersect_key($events_list, $complete_events_id);
                    if(empty($complete_events_id)){
                        $events_list = null;
                    }
                }
            }
            if(empty($events_id) && !empty($args['complete'])){
                $events_list = null;
            }
        }

        return $events_list;
	}

    /**
     * @param $subscriber_id
     * @param bool $full
     * @return mixed
     * get history from teachers_user_list table
     */
        public function get_subscriber_history($subscriber_id, $full = false){
        global $wpdb;
        $limit = '';

        if(!$full){
            $limit = ' LIMIT 3';
        }

        $users_history = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM " . $wpdb->prefix . "teachers_user_list WHERE `subscriber_id` = %d ORDER BY `complete_date` DESC" . $limit,
                $subscriber_id
            ),
            ARRAY_A
        );

        if(empty($users_history) && count($users_history) < 1){
            return;
        }

        $i = 0;
        foreach($users_history as $history ){
            $lesson_date = date('d/m/y', $history['complete_date']);
            $user_history[$i]['lesson_date'] = $lesson_date;
            $user_history[$i]['event_id'] = $history['event_id'];
            $user_history[$i]['author_id'] = $history['author_id'];
            //get download title from mychinese hub site
            switch_to_blog(5);
            $user_history[$i]['course_group'] = get_the_title($history['course_group_id']);
            $module = self::get_module_details($history['module_id']);
            $user_history[$i]['module_id'] = $module->module_number;

	        if(!empty($history['level_id'])){
		        $course_details = self::mct_get_course_info($history['level_id']);
		        if(!empty($course_details->course_number)){
			        $user_history[$i]['course_level'] = $course_details->course_number;
		        }
	        }

            restore_current_blog();
            $user_history[$i]['lesson_id'] = $history['lesson_id'];
            $user_history[$i]['comment'] = $history['comment'];

            $i++;
        }

        return $user_history;
    }

	
	public function output($args) {
        $events_list = '';
        $title = '';

        $args['owner'] = get_current_user_id();
        if(empty($args['display'])){
            $events_list = MCT_Lessons_List::get($args);
        }
        if(!empty($args['title'])){
            $title = $args['title'];
        }

        ob_start();
        em_locate_template('templates/mct-lessons-list-template.php', true, array('events' => $events_list,'args'=>$args, 'title' => $title));
        $users_array = ob_get_clean();

        return apply_filters('mct_upcomming_lessons', $users_array, $args);
	}


    function upcoming_lessons_enqueue_scripts() {
        wp_enqueue_script( 'students_hider', plugins_url( 'includes/js/calendar/students_hider.js', dirname(__FILE__ ) ), array( 'jquery' ) );
        wp_enqueue_script( 'common_script', plugins_url( 'includes/js/calendar/common_script.js', dirname(__FILE__ ) ), array( 'jquery' ) );
    }

    function add_booking_meta($EM_Booking, $meta_key = NULL, $meta_value = NULL){
        global $wpdb;
           if(empty($meta_key) || empty($meta_value)){
               return $wpdb->insert(EM_META_TABLE, array('object_id' => $EM_Booking->booking_id, 'meta_key' => 'booking-user-subscription', 'meta_value' => get_current_user_id()),array('%d','%s','%s'));
           }else{
               return $wpdb->insert(EM_META_TABLE, array('object_id' => $EM_Booking->booking_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value),array('%d','%s','%s'));
           }
    }

    function get_booking_meta($booking_id, $meta_key){
        global $wpdb;
        $meta_value = $wpdb->get_var(
                           $wpdb->prepare("SELECT meta_value
                                           FROM " . EM_META_TABLE . "
                                           WHERE meta_key = %s AND object_id = %s",
                                           $meta_key,
                                           $booking_id
                           )
                      );
        if(empty($meta_value)){
            return;
        }

        return $meta_value;
    }

    function get_user_courses_info($user_id){
        global $wpdb;
        $downloads_id = array();
        $courses_info = array();
        $courses = array();

        $edd_customers_table_name = $wpdb->prefix . 'edd_customers';
        $SQL = $wpdb->prepare("
            SELECT payment_ids FROM " . $edd_customers_table_name . "
            WHERE user_id = %d
        ", $user_id);

        $payment_ids = $wpdb->get_results($SQL);

        $sites_download_conformity = unserialize(get_site_option('mychinese_auto_subscription'));


        foreach($sites_download_conformity as $tutor_ids => $hub_ids){
            $tutor_id = explode('tutor-course-', $tutor_ids);
            $hub_id = explode('hub-course-', $hub_ids);
            $downloads_conformity[$tutor_id[1]] = $hub_id[1];
        }


        foreach($payment_ids as $payment_id){
            $ids = explode(',', $payment_id->payment_ids);
            foreach($ids as $id){
                $payment_meta  = edd_get_payment_meta( $id );

                $downloads_id = $payment_meta['downloads'][0]['id'];

                if(!apply_filters( 'edd_recurring_download_has_access', true, $user_id, $downloads_id, true )){
                    continue;
                }
                /* get download id from mychines hub site*/
                $hub_download_id = $downloads_conformity[$downloads_id];

                if(empty($hub_download_id)){
                    continue;
                }

                switch_to_blog(5);
                $courses_info[$downloads_id]['course_name'] = get_the_title($hub_download_id);
                restore_current_blog();

                $courses_info[$downloads_id]['course_group_id'] = $hub_download_id;
                /* get all user's courses*/
                $levels = self::get_levels_list($hub_download_id);

                $courses_info[$downloads_id]['levels'] = $levels;

                /* get all user's modules from first course*/
                if(is_array($levels)){
                    $level = array_keys($levels);
                    $modules = self::get_modules_list($level[0]);
                    $courses_info[$downloads_id]['modules'] = $modules;
                }
                /* get all user's modules from first course*/
                if(is_array($levels)){
                    $lessons = self::get_lessons_list($modules[0]['module_id']);
                    $courses_info[$downloads_id]['lessons'] = $lessons;

                }
            }
        }

        return $courses_info;
    }


    function get_levels_list($download_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $courses_levels = $wpdb->get_results("
                SELECT `course_id`, `course_number`
                FROM " . $wpdb->prefix . "wpcw_courses
                WHERE `course_group_id` = " . $download_id . "
                ORDER BY `course_number`
                "
            );
        }

        restore_current_blog();
        if(empty($courses_levels)){
            return;
        }
        foreach($courses_levels as $level){
            $level_list[$level->course_id] = $level->course_number;
        }


        return $level_list;
    }

    function get_modules_list($course_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $modules_list = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "wpcw_modules WHERE `parent_course_id` = $course_id ORDER BY `module_order`", ARRAY_A);
        }

        restore_current_blog();

        if(empty($modules_list)){
            return;
        }

        return $modules_list;
    }


    function get_lessons_list($module_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $lessons_list = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "wpcw_units_meta WHERE `parent_module_id` = $module_id ORDER BY `unit_number`", ARRAY_A);
        }
        restore_current_blog();

        if(empty($lessons_list)){
            return;
        }

        return $lessons_list;
    }

    function get_module_details($moduleID)
    {
        if (!$moduleID) {
            return false;
        }

        global $wpdb;

        $wpdb->show_errors();
        $table = $wpdb->prefix . 'wpcw_modules';
        $SQL = $wpdb->prepare("SELECT *
			FROM $table
			WHERE module_id = %d
			", $moduleID );

        return $wpdb->get_row($SQL);
    }

	function mct_get_course_info($course_id)
	{
		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}wpcw_courses
			WHERE course_id={$course_id}
			ORDER BY course_title";

		$item = $wpdb->get_results($sql);

		if (isset($item[0]) && !empty($item[0])) {
			return $item[0];
		} else {
			return false;
		}
	}

}

add_action('init', array('MCT_Lessons_List', 'init'));