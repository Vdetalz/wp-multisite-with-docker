<?php
class MCT_Lessons_Requiring_Feedback extends EM_Booking {

	public static function init(){
        add_action( 'wp_enqueue_scripts', array('MCT_Lessons_Requiring_Feedback', 'requiring_feedback_lessons_enqueue_scripts'));
	}

	public function get($args = array()){
	 	global $wpdb;

        $bookings = array();
        $lessons_list = array();

        $limit = !empty($args['limit']) ? (int)$args['limit'] : 10;
        $page = !empty($args['page']) ? (int)$args['page'] : 1;
        $offset = ($page-1) * $limit;

        $events_table = $wpdb->prefix . 'em_events';
        $teachers_user_list_table = $wpdb->prefix . 'teachers_user_list';
        $bookings_table = $wpdb->prefix . 'em_bookings';

        $event_owner = get_current_user_id();
        $date = date("Y-m-d H:i:s");

        $sql = "SELECT * FROM `{$events_table}` AS `ev`
                LEFT JOIN `{$bookings_table}` AS `bk` ON ev.event_id=bk.event_id
                WHERE ev.event_owner=%d AND bk.booking_id IS NOT NULL AND
                ev.event_id NOT IN (SELECT event_id FROM `{$teachers_user_list_table}`)
                AND ( event_end_date < CAST('{$date}' AS DATE) OR (event_end_date = CAST('{$date}' AS DATE) AND event_end_time < CAST('{$date}' AS time)))
                ORDER BY ev.event_end_date ASC, ev.event_end_time ASC, ev.event_name ASC LIMIT %d, %d";

        $lessons = $wpdb->get_results($wpdb->prepare($sql, $event_owner, $offset, $limit));

        foreach($lessons as $lesson){
            $lessons_list[$lesson->event_id]['user'] = new stdClass();
            $lessons_list[$lesson->event_id]['user']->user_id = $lesson->person_id;

            $user_info = get_userdata($lesson->person_id);
            $lessons_list[$lesson->event_id]['user']->user_email = $user_info->user_email;
            $lessons_list[$lesson->event_id]['user']->nickname   = get_user_meta($lesson->person_id, 'nickname', true);
            $lessons_list[$lesson->event_id]['user']->message    = $lesson->booking_comment;
            $lessons_list[$lesson->event_id]['user']->history    = self::get_subscriber_history($lesson->person_id);
            $lessons_list[$lesson->event_id]['user']->courses = self::get_user_courses_info($lesson->person_id);
            $lessons_list[$lesson->event_id]['event_start_date'] = $lesson->event_start_date;
            $lessons_list[$lesson->event_id]['event_start_time'] = $lesson->event_start_time;
            $lessons_list[$lesson->event_id]['event_end_time'] = $lesson->event_end_time;
        }

        return $lessons_list;
	}

    /**
     * Return max rows number.
     * @return mixed
     */
    public function get_total(){
        global $wpdb;

        $lessons_num = '';
        $events_table = $wpdb->prefix . 'em_events';
        $teachers_user_list_table = $wpdb->prefix . 'teachers_user_list';
        $bookings_table = $wpdb->prefix . 'em_bookings';

        $event_owner = get_current_user_id();
        $date = date("Y-m-d H:i:s");

        $sql = "SELECT COUNT(*) FROM `{$events_table}` AS `ev`
                LEFT JOIN `{$bookings_table}` AS `bk` ON ev.event_id=bk.event_id
                WHERE ev.event_owner=%d AND bk.booking_id IS NOT NULL AND
                ev.event_id NOT IN (SELECT event_id FROM `{$teachers_user_list_table}`)
                AND ( event_end_date < CAST('{$date}' AS DATE) OR (event_end_date = CAST('{$date}' AS DATE) AND event_end_time < CAST('{$date}' AS time)))
                ORDER BY ev.event_end_date";

        $lessons = $wpdb->get_var($wpdb->prepare($sql, $event_owner));

        return $lessons;
    }


    /**
     * @param $subscriber_id
     * @param bool $full
     * @return mixed
     * get history from teachers_user_list table
     */
     public function get_subscriber_history($subscriber_id, $full = false){
        global $wpdb;
        $limit = '';

        if(!$full){
            $limit = ' LIMIT 3';
        }

        $users_history = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM " . $wpdb->prefix . "teachers_user_list WHERE `subscriber_id` = %d ORDER BY `complete_date` DESC" . $limit,
                $subscriber_id
            ),
            ARRAY_A
        );

        if(empty($users_history) && count($users_history) < 1){
            return;
        }

        $i = 0;
        foreach($users_history as $history ){
            $lesson_date = date('d/m/y', $history['complete_date']);
            $user_history[$i]['lesson_date'] = $lesson_date;
            $user_history[$i]['event_id'] = $history['event_id'];
            $user_history[$i]['author_id'] = $history['author_id'];
            //get download title from mychinese hub site
            switch_to_blog(5);
            $user_history[$i]['course_group'] = get_the_title($history['course_group_id']);
            $module = self::get_module_details($history['module_id']);
            $user_history[$i]['module_id'] = $module->module_number;

	        if(!empty($history['level_id'])){
		        $course_details = self::mct_get_course_info($history['level_id']);
		        if(!empty($course_details->course_number)){
			        $user_history[$i]['course_level'] = $course_details->course_number;
		        }
	        }

            restore_current_blog();
            $user_history[$i]['lesson_id'] = $history['lesson_id'];
            $user_history[$i]['comment'] = $history['comment'];

            $i++;
        }

        return $user_history;
    }

    /**
     * @param $user_id
     * @return array
     */
    function get_user_courses_info($user_id){
        global $wpdb;
        $downloads_id = array();
        $courses_info = array();
        $courses = array();

        $edd_customers_table_name = $wpdb->prefix . 'edd_customers';
        $SQL = $wpdb->prepare("
            SELECT payment_ids FROM " . $edd_customers_table_name . "
            WHERE user_id = %d
        ", $user_id);

        $payment_ids = $wpdb->get_results($SQL);

        $sites_download_conformity = unserialize(get_site_option('mychinese_auto_subscription'));


        foreach($sites_download_conformity as $tutor_ids => $hub_ids){
            $tutor_id = explode('tutor-course-', $tutor_ids);
            $hub_id = explode('hub-course-', $hub_ids);
            $downloads_conformity[$tutor_id[1]] = $hub_id[1];
        }


        foreach($payment_ids as $payment_id){
            $ids = explode(',', $payment_id->payment_ids);
            foreach($ids as $id){
                $payment_meta  = edd_get_payment_meta( $id );

                $downloads_id = $payment_meta['downloads'][0]['id'];

                if(!apply_filters( 'edd_recurring_download_has_access', true, $user_id, $downloads_id, true )){
                    continue;
                }
                /* get download id from mychines hub site*/
                $hub_download_id = $downloads_conformity[$downloads_id];

                if(empty($hub_download_id)){
                    continue;
                }

                switch_to_blog(5);
                $courses_info[$downloads_id]['course_name'] = get_the_title($hub_download_id);
                restore_current_blog();

                $courses_info[$downloads_id]['course_group_id'] = $hub_download_id;
                /* get all user's courses*/
                $levels = self::get_levels_list($hub_download_id);

                $courses_info[$downloads_id]['levels'] = $levels;

                /* get all user's modules from first course*/
                if(is_array($levels)){
                    $level = array_keys($levels);
                    $modules = self::get_modules_list($level[0]);
                    $courses_info[$downloads_id]['modules'] = $modules;
                }
                /* get all user's modules from first course*/
                if(is_array($levels)){
                    $lessons = self::get_lessons_list($modules[0]['module_id']);
                    $courses_info[$downloads_id]['lessons'] = $lessons;

                }
            }
        }

        return $courses_info;
    }

    /**
     * Return module detail info.
     *
     * @param $moduleID
     * @return bool
     */
    function get_module_details($moduleID)
    {
        if (!$moduleID) {
            return false;
        }

        global $wpdb;

        $wpdb->show_errors();
        $table = $wpdb->prefix . 'wpcw_modules';
        $SQL = $wpdb->prepare("SELECT *
			FROM $table
			WHERE module_id = %d
			", $moduleID );

        return $wpdb->get_row($SQL);
    }

    /**
     * Return levels list.
     *
     * @param $download_id
     * @return mixed
     */
    function get_levels_list($download_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $courses_levels = $wpdb->get_results("
                SELECT `course_id`, `course_number`
                FROM " . $wpdb->prefix . "wpcw_courses
                WHERE `course_group_id` = " . $download_id . "
                ORDER BY `course_number`
                "
            );
        }

        restore_current_blog();
        if(empty($courses_levels)){
            return;
        }
        foreach($courses_levels as $level){
            $level_list[$level->course_id] = $level->course_number;
        }


        return $level_list;
    }

    /**
     * Return course info by course id.
     *
     * @param $course_id
     * @return bool
     */
    function mct_get_course_info($course_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}wpcw_courses
			WHERE course_id={$course_id}
			ORDER BY course_title";

        $item = $wpdb->get_results($sql);

        if (isset($item[0]) && !empty($item[0])) {
            return $item[0];
        } else {
            return false;
        }
    }

    /**
     * Return list of modules for course.
     *
     * @param $course_id
     * @return mixed
     */
    function get_modules_list($course_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $modules_list = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "wpcw_modules WHERE `parent_course_id` = $course_id ORDER BY `module_order`", ARRAY_A);
        }

        restore_current_blog();

        if(empty($modules_list)){
            return;
        }

        return $modules_list;
    }

    /**
     * Return list of lessons for module.
     * @param $module_id
     * @return mixed
     */
    function get_lessons_list($module_id){
        global $switched, $wpdb;

        switch_to_blog(5);
        if($switched){
            $lessons_list = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "wpcw_units_meta WHERE `parent_module_id` = $module_id ORDER BY `unit_number`", ARRAY_A);
        }
        restore_current_blog();

        if(empty($lessons_list)){
            return;
        }

        return $lessons_list;
    }

    /**
     * Hook wp_enqueue_scripts callback function.
     */
    function requiring_feedback_lessons_enqueue_scripts() {
        wp_enqueue_script( 'mct-ev-scripts', plugins_url( 'includes/js/scripts.js', dirname(__FILE__ ) ), array( 'jquery' ) );
        wp_localize_script( 'mct-ev-scripts', 'ajax', array( 'url' => admin_url( 'admin-ajax.php' )) );
    }

    /**
     * View shortcode function.
     *
     * @param $args
     * @return mixed|void
     */
	public function output($args) {
        $events_list = '';
        $title = '';

        $limit = !empty($args['limit']) ? (int)$args['limit'] : 10;
        $events_list = self::get($args);
        $max_rows = self::get_total();
        $pages_num = ceil($max_rows/$limit);
        $page = !empty($args['page']) ? (int)$args['page'] : 1;

        if(!empty($args['title'])){
            $title = $args['title'];
        }

        ob_start();
        em_locate_template('templates/mct-lessons-requiring-feedback-template.php', true, array('events' => $events_list, 'pages_num' => $pages_num, 'current_page' => $page, 'args'=>$args, 'title' => $title));
        $users_array = ob_get_clean();

        return apply_filters('mct_upcomming_lessons', $users_array, $args);
	}
}

add_action('init', array('MCT_Lessons_Requiring_Feedback', 'init'));