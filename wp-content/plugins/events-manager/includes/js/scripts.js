/**     add and remove loader div, use if you get something via ajax */
function togglePortalLoader(elem) {
    if(elem) {
        elem.remove();
    } else {
        return jQuery('<div />', {
            'class': 'site-loader'
        }).appendTo(jQuery('body'));
    }
}

jQuery(document).ready( function($){
    /* AJAX: Pagination */
    $('.mct-pagination a', '#requiring-feedback').live('click', function(e){
        e.preventDefault();
        var loader = togglePortalLoader();
        var page = $(this).data('page');
        $.ajax({
            type:'POST',
            //dataType : 'html',
            url:ajax.url,
            data: {
                action : 'get_requiring_feedback',
                page : page
            },
            success: function(data){
                togglePortalLoader(loader);
                var lessons = $(data).find('.lessons');
                if(lessons.length){
                    $('.lessons', '#requiring-feedback').html(lessons.html());
                    $('.lessons', '#requiring-feedback').find('select').chosen('destroy');
                    $('.lessons', '#requiring-feedback').find('select').chosen({disable_search_threshold:10});
                }
            }
        });
    });

    $('.mct-pagination a', '#upcoming-lessons').live('click', function(e){
        e.preventDefault();
        var loader = togglePortalLoader();
        var page = $(this).data('page');
        $.ajax({
            type:'POST',
            url:ajax.url,
            data: {
                action : 'get_upcoming_lessons',
                page : page
            },
            success: function(data){
                togglePortalLoader(loader);
                var lessons = $(data).find('.lessons');
                if(lessons.length){
                    $('.lessons', '#upcoming-lessons').html(lessons.html());
                    $('.lessons', '#upcoming-lessons').find('select').chosen('destroy');
                    $('.lessons', '#upcoming-lessons').find('select').chosen({disable_search_threshold:10});
                }
            }
        });
    });
});