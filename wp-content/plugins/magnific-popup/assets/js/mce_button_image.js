(function() {
    tinymce.PluginManager.add('mch_image_button', function( editor, url ) {
        editor.addButton( 'mch_image_button', {
            title: 'Insert Image',
            icon: 'icon mch_image_button',

            onclick: function() {
                editor.windowManager.open( {
                    title: 'Insert Image',
                    body: [{   type: 'textbox',
                        name: 'link',
                        label: 'Image Link'
                    }],
                    onsubmit: function( e ) {
                        editor.insertContent( '[img]' + e.data.link + '[/img]');
                    }
                });
            }

        });
    });
})();