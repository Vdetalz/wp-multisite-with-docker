(function ($, root, undefined) {

    $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
        
        $('.video-popup-button').magnificPopup({
            fixedContentPos: false,
            midClick: true,
            callbacks: {
                open: function() {
                    $('#mct-video').get(0).play();
                }
            }
        });
    });

})(jQuery, this);