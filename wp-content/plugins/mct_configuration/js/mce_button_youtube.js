(function() {
    tinymce.PluginManager.add('mch_youtube_button', function( editor, url ) {
        editor.addButton( 'mch_youtube_button', {
            title: 'Insert Youtube Video',
            icon: 'icon mch_youtube_button',

            onclick: function() {
                editor.windowManager.open( {
                    title: 'Insert YouTube Video',
                    body: [{   type: 'textbox',
                        size: 40,
                        name: 'name',
                        label: 'Popup Link Name:'
                    },{   type: 'textbox',
                        size: 40,
                        name: 'link',
                        label: 'Youtube Video Link'
                    }],
                    onsubmit: function( e ) {
                        editor.insertContent( '<a href="' + e.data.link + '" class="popup-youtube">' + e.data.name + '</a>');
                    }
                });
            }

        });
    });
})();