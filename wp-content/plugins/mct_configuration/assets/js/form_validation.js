jQuery(document).ready(function($){
    $('.nickname-not-filled').hide();
    $('.skype-not-filled').hide();

    $('.user-account-submit').click(function(){

        $('.nickname-not-filled').hide();
        $('.skype-not-filled').hide();

        if($('#nickname').val() && $('#skype_name').val()){
            if($('#nickname').val().length > 0 && $('#skype_name').val().length > 0){
                $('form.account-form').submit();
            }
            else{
                if($('#nickname').val().length <= 0){
                    $('.nickname-not-filled').show();
                }
                if($('#skype_name').val().length <= 0){
                    $('.skype-not-filled').show();
                }
            }
        }
        else{
            if(!$('#nickname').val()){
                $('.nickname-not-filled').show();
            }
            if(!$('#skype_name').val()){
                $('.skype-not-filled').show();
            }
        }
    });
});