<?php

/**
 * Ajax function, which save selected lesson as complete to database
 */
add_action( 'wp_ajax_mct_save_lesson_history', 'mct_save_lesson_history' );

function mct_save_lesson_history() {
    global $wpdb;
    $event_date = '';

    if ( isset( $_POST['user_id'] ) && !empty( $_POST['user_id'] ) ) {
        $user_id = $_POST['user_id'];
    }

    if ( isset( $_POST['event_id'] ) && !empty( $_POST['event_id'] ) ) {
        $event_id = $_POST['event_id'];
        $EM_Event = new EM_Event($event_id);
        $event_date = strtotime($EM_Event->event_start_date);
    }

    if ( isset( $_POST['teacher_id'] ) && !empty( $_POST['teacher_id'] ) ) {
        $teacher_id = $_POST['teacher_id'];
    }
    if ( isset( $_POST['course_id'] ) && !empty( $_POST['course_id'] ) ) {
        $course_id = $_POST['course_id'];
    }
    if ( isset( $_POST['module_id'] ) && !empty( $_POST['module_id'] ) ) {
        $module_id = $_POST['module_id'];
    }
    if ( isset( $_POST['lesson_id'] ) && !empty( $_POST['lesson_id'] ) ) {
        $lesson_id = $_POST['lesson_id'];
    }
    if ( isset( $_POST['comment'] ) && !empty($_POST['comment'])) {
        $comment = $_POST['comment'];
    }


    $table_name = $wpdb->prefix . "teachers_user_list";

    $wpdb->insert(
        $table_name,
        array(
            'event_id'        => $event_id,
            'author_id'       => $teacher_id,
            'subscriber_id'   => $user_id,
            'course_level_id' => $course_id,
            'module_id'       => $module_id,
            'lesson_id'       => $lesson_id,
            'comment'         => $comment,
            'complete_date'   => $event_date
        )
    );


    die();
}
