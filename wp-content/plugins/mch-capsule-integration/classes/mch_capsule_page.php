<?php

/** var $capsule_model MCH_Capsule_Employers_Model */

class MCH_Capsule_Page {
	const PAGE_SLUG = 'mch-capsule-page';
	const APP_NAME_OPTION = 'mch_capsule_app_name';

	private $capsule_model;

	/**
	 * @param MCH_Capsule_Employers_Model $capsule_model
	 */
	public function __construct(MCH_Capsule_Employers_Model $capsule_model) {
		$this->capsule_model = $capsule_model;
	}

	/**
	 * Start up
	 */
	public function init() {
		add_action('admin_init', array($this, 'delete'));
		add_action('admin_menu', array($this, 'add_page'));
	}

	/**
	 * Add emails page
	 */
	public function add_page() {
		// This page will be under "Settings"
		add_options_page(
		  __('Capsule Integration'),
		  __('Capsule Integration'),
		  'manage_options',
		  self::PAGE_SLUG,
		  array($this, 'create_admin_page')
		);
	}

	public function delete() {
		// delete employer.
		if($id = filter_input(INPUT_GET, 'delete_employer', FILTER_VALIDATE_INT)) {
			$this->capsule_model->delete($id);
			header('Location: ' . $this->get_page_url());
		}
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page() {
		?>
		<h2><?php _e('Capsule Integration'); ?></h2>

		<?php
		$this->get_app_name_form();
		$this->get_employers_form();
		?>

	<?php
	}

	/**
	 * Show emails table
	 */
	public function get_capsule_employers_table() {
		$employers = $this->capsule_model->get_all_employers();
		?>
		<table class="wp-list-table widefat fixed posts">
			<thead>
			<tr>
				<th>
					<?php _e("Name") ?>
				</th>
				<th>
					<?php _e("Token") ?>
				</th>
				<th>
					<?php _e("Delete") ?>
				</th>
			</tr>
			</thead>
			<tbody>
			<?php if(!empty($employers)): ?>
			  	<?php foreach($employers as $employer): ?>
				  	<tr>
						<td><?php echo $employer->name ?></td>
						<td><?php echo $employer->token ?></td>
						<td><a href="<?php echo $this->get_page_url() . '&delete_employer=' . $employer->id; ?>"><?php _e('Delete'); ?></a></td>
				  	</tr>
				<?php endforeach; ?>
		  	<?php else: ?>
				<tr>
					<td><?php _e("Employers Not Found."); ?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	<?php
	}

	/**
	 * Get App Name Form.
	 */
	private function get_app_name_form() {
		if (isset($_POST['save_app_name'])) {
			update_option(self::APP_NAME_OPTION, $_POST['app_name']);
		} ?>

		<div>
			<form method="post" action="<?php echo $this->get_page_url() ?>">
				<label
				  for="app-name"><?php _e('Enter Application name:'); ?></label><br/>
				<input type="text" id="app-name" name="app_name"
					   value="<?php echo get_option(self::APP_NAME_OPTION); ?>"/>
				<?php submit_button(__('Save'), 'primary', 'save_app_name'); ?>
			</form>
		</div>
	<?php
	}

	/**
	 * Get Employers Form and Table.
	 */
	private function get_employers_form() {
		?>
		<div>
			<?php
			if (isset($_POST['add_employer']) && !empty($_POST['name']) && !empty($_POST['token'])) {
				$this->capsule_model->name = $_POST['name'];
				$this->capsule_model->token = $_POST['token'];
				$this->capsule_model->save();
			}
			$this->get_capsule_employers_table();
			?>

			<div class="wrap">
				<form method="post">
					<input type="name" id="name" name="name"
						   placeholder="<?php _e('Enter name'); ?>" required/>
					<input type="token" id="token" name="token"
						   placeholder="<?php _e('Enter token'); ?>" required/>
					<?php submit_button(__('Add'), 'primary', 'add_employer'); ?>
				</form>
			</div>
		</div>
		<?php
	}

	/**
	 * @return string Page url.
	 */
	public function get_page_url() {
		return get_admin_url(NULL, 'options-general.php?page=' . self::PAGE_SLUG);
	}
}
