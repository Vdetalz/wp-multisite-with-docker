<div class="wrap table-responsive">
    <h2><?php _e('Student’s balance history'); ?></h2>
    <?php if(!empty($users_array) && is_array($users_array)): ?>
    <div class="form-group">
        <div class="input-group">
            <form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>">
                    <p><?php _e('Select user'); ?></p>
                    <select name="ctrl_user">
                        <option value=""><?php _e('-Select user-'); ?></option>
                        <?php foreach ($users_array as $user_id => $user_name): ?>
                               <option value="<?php print $user_id;  ?>" <?php if(!empty($_POST['ctrl_user'])){ selected( $_POST['ctrl_user'], $user_id ); } ?> ><?php print $user_name; ?></option>
                        <?php endforeach; ?>
                    </select><input type="submit" value="Submit" name="get_user_history" class="btn btn-default">
            </form>
        </div>
    </div>
    <?php endif; ?>
    <?php if(!empty($user_logs)): ?>
        <table class="widefat table table-bordered">
            <thead>
            <tr>
                <th style="border-right:1px solid #dfdfdf;"><?php print __('Date'); ?></th>
                <th style="border-right:1px solid #dfdfdf;"><?php print __('Action'); ?></th>
                <th style="border-right:1px solid #dfdfdf;"><?php print __('Change in Balance'); ?></th>
                <th style="border-right:1px solid #dfdfdf;"><?php print __('Balance'); ?></th>
            </tr>
            </thead>
                <?php foreach ($user_logs as $user_log): ?>
                    <tr class="alternate">
                        <td style="border-bottom:1px solid #dfdfdf;"><?php echo $user_log->date; ?></td>
                        <td style="border-bottom:1px solid #dfdfdf;"><?php echo $user_log->action; ?></td>
                        <td style="border-bottom:1px solid #dfdfdf;"><?php echo $user_log->change_balance; ?></td>
                        <td style="border-bottom:1px solid #dfdfdf;"><?php echo $user_log->balance; ?></td>
                    </tr>
                <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>