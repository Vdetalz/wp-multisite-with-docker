<div class="wrap table-responsive">
    <h2><?php print __('User\'s Balance'); ?></h2>
    <table class="table table-bordered" id="users-info">
        <thead>
        <tr>
            <th style="border-right:1px solid #dfdfdf;"><?php print __('Username'); ?></th>
            <th style="border-right:1px solid #dfdfdf;"><?php print __('Skypename'); ?></th>
            <th style="border-right:1px solid #dfdfdf;"><?php print __('Balance'); ?></th>
        </tr>
        </thead>
    </table>
</div>