<?php
/*
Plugin Name: MCH Students Export
Plugin URI:
Description: Plugin for MyChinese Mecca (Hub). Create CSV file with info of existing students
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

add_action('admin_menu', 'mch_students_export_admin_menu');

function mch_students_export_admin_menu()
{
    if (function_exists('add_menu_page')) {
        add_menu_page(
            __('MCT Students Export'),
            __('MCT Students Export'),
            'edit_posts',
            basename(__FILE__),
            'mch_students_export_options'
        );
    }
}

function mch_students_export_options()
{

    if (isset($_POST['do_export']) && !empty($_POST['do_export'])) {
        ob_end_clean();
        $result_csv_string = '';

        $args = array(
          'role' => 'students',
          'orderby' => 'login',
          'order' => 'ASC',
        );

        $Users = new WP_User_Query($args);

        $keys = array('Location', 'School', 'Year/Level', 'First Name',
          'Last Name', 'Email', 'Recent Course Name', 'Recent Module Name',
          'Recent Lesson Name', 'Last Completed Lesson Date (History)');

        if ($keys) {
            $result_csv_string .= implode(",", $keys) . "\n";
        }

        if ($Users->results && !empty($Users->results)) {
            foreach ($Users->results as $user) {
                $location = '';
                $school = '';
                $year_level = '';
                $first_name = '';
                $first_name = '';
                $last_name = '';
                $email = '';
                $course_name = '';
                $module_name = '';
                $lesson_name = '';
                $last_history_lesson_date = '';


                $user_id = $user->ID;
                $location = get_user_meta($user_id, 'location', true);
                $school = get_user_meta($user_id, 'school', true);
                $year_level = get_user_meta($user_id, 'year_level', true);
                $first_name = get_user_meta($user_id, 'first_name', true);
                $last_name = get_user_meta($user_id, 'last_name', true);
                $email = $user->data->user_email;
                $recent_course_id = get_user_meta($user_id, 'most_recent_course', true);
                $recent_module_id = get_user_meta($user_id, 'most_recent_module', true);
                $recent_lesson_id = get_user_meta($user_id, 'most_recent_lesson', true);
                //Get Course Entity
                $course_obj = get_course_by_id($recent_course_id);
                //Get Course Name
                $course_name = $course_obj->course_title;

                //Get Module Entity
                $module_obj = get_module_by_id($recent_module_id);
                //Get Module Name
                $module_name = $module_obj->module_title;

                //Get Lesson Entity
                $lesson_obj = get_post($recent_lesson_id);
                //Get Lesson Name
                $lesson_name = $lesson_obj->post_title;

                //Now we get last lesson from history from teacher portal
                $last_history_lesson = get_last_history_lesson($user_id);
                $last_history_lesson_date = date('d.M.o', $last_history_lesson);

                $first_name = str_replace(' ', '_', $first_name);
                $first_name = str_replace(',', '_', $first_name);

                $year_level = str_replace(',', '_', $year_level);
                $year_level = str_replace(',', '_', $year_level);

                $school = str_replace(',', '_', $school);
                $school = str_replace(',', '_', $school);

                $last_name = str_replace(' ', '_', $last_name);
                $last_name = str_replace(',', '_', $last_name);

                $result_csv_string .= $location . "," . $school . "," . $year_level . "," . $first_name . "," .
                  $last_name . "," . $email . "," . $course_name . "," . $module_name . ","  .
                  $lesson_name  . "," . $last_history_lesson_date  . "\n";

            }

            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="users_list.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');

            echo $result_csv_string;
            exit;

        }


    }

    ?><h2><?php echo __('MCT Students Export'); ?></h2>
    <p><?php echo __('When you will click on "Upload" button you will get CSV file with next info:'); ?></p>
    <ol>
        <li><?php echo __('Location'); ?></li>
        <li><?php echo __('School name'); ?></li>
        <li><?php echo __('Year level'); ?></li>
        <li><?php echo __('First name'); ?></li>
        <li><?php echo __('Last name'); ?></li>
        <li><?php echo __('Email address'); ?></li>
        <li><?php echo __('Most recent lesson Pathway Number'); ?></li>
        <li><?php echo __('Most recent lesson Module Number'); ?></li>
        <li><?php echo __('Most recent lesson Lesson Number'); ?></li>
        <li><?php echo __('Date of most recent lesson history completed'); ?></li>
    </ol>
    <form method="post">
        <input type="submit" name="do_export" value="<?php echo __('Export'); ?>">
    </form>
    <?php
}

/**
 * @param $subscriber_id
 * @return bool|null|string
 */
function get_last_history_lesson($subscriber_id)
{
    global $wpdb;

    $sql = $wpdb->prepare("SELECT `complete_date` FROM " . $wpdb->prefix . "teachers_user_list
                           WHERE `subscriber_id` = %d ORDER BY `complete_date` DESC LIMIT 1", $subscriber_id);
    $query = $wpdb->get_var($sql);
    if ($query && !empty($query)) {
        return $query;
    }
    return false;
}
