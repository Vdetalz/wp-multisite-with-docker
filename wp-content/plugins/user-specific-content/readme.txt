﻿=== User Specific Content ===
Contributors: bainternet,adsbycb 
Donate link:http://en.bainternet.info/donations
Tags: User content, user specific content,content by role, content by user
Requires at least: 2.9.2
Tested up to: 3.1.1
Stable tag: 0.6

This Plugin allows you to select specific users by user name, or by role name who can view a specific post content or page content.

== Description ==

This Plugin allows you to select specific users by user name, or by role name who can view a specific post content or page content.

Basically it adds a meta box to the post or page edit screen and lets the user select specific users by name or roles and then when you call that page content using “the\_content();” function it check using “the\_content” filter if the current user is one of the users you have selected or if his role match’s the roles you have selected and shows the content, otherwise it displays a message

**Features:**

*   You can select any number of Users you want by there names.
*   You can select any number of users Roles you want by there names.
*   Easy Customization of content blocked massage per post, or page.
*   Works with both posts, and pages.
*   Content to none logged in users only.

**Upcomming Features:**

*   Setup global default blocked message.
*   currently the plugin blocks when using `the_content` filter which will be extended to `the_excerpt` on admin selection.



Any feedback or suggestions are welcome.

Also check out my <a href=\"http://en.bainternet.info/category/plugins\">other plugins</a>

 

== Installation ==

1.  Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation.
2.  Then activate the Plugin from Plugins page.
3.  Done!
== Frequently Asked Questions ==

= I have Found a Bug, Now what? =

Simply use the <a href=\"http://wordpress.org/tags/user-specific-content?forum_id=10\">Support Forum</a> and thanks a head for doing that.
== Screenshots ==
1. User Specific Content metabox

== Changelog ==
0.6 Fixed all wp_debug warnings.

0.5 Fixed wp_debug warnings.

0.4 added simply logged-in user content only!
quick fixed block by role bug.

0.3 
added none logged-in user content only!

0.2 
added pages support

0.1 
initial release