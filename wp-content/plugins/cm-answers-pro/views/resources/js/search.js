jQuery(function($) {
	
	var performRequest = function(container, url, data) {
//		data.widget = 1;
		var widgetContainer = container.parents('.cma-questions-widget');
		if (url.indexOf('widgetCacheId') == -1) {
			data.widgetCacheId = container.data('widgetCacheId');
		}
		container.addClass('cma-loading');
		container.append($('<div/>', {"class":"cma-loader"}));
		$.ajax({
			method: "GET",
			url: url,
			data: data,
			success: function(response) {
				
				var code = $('<div>' + response +'</div>');
				var newContainer = code.find('.cma-questions-widget').clone();
				container.before(newContainer).remove();
				container = newContainer;
				container.find('.cma-backlink').remove();
				initHandlers(container);
			}
		});
	};
	
	
	var initHandlers = function(container) {
		if (typeof CMA_script_init == 'function') CMA_script_init($);
		navBarHandlerInit(container);
		paginationHandlerInit(container);
		loadAnswerHandlerInit(container);
		backlinkHandlerInit(container);
		postAnswerHandlerInit(container);
		categoryHandlerInit(container);
	};
	
	
	
	var paginationHandlerInit = function(container) {
		$('.cma-pagination a', container).click(function() {
			link = $(this);
			performRequest(link.parents('.cma-questions-widget'), link.attr('href'), {});
			return false;
		});
	};
	
	
	
	var navBarHandlerInit = function(container) {
		$('form.cma-filter', container).submit(function() {
			var form = $(this);
			var data = {};
			form.find(':input[name]').each(function() {
				data[this.name] = this.value;
			});
			// Choose a proper category URL
			var url = form.find('select.cma-filter-category-secondary option:selected').data('url');
			if (!url) {
				url = form.find('select.cma-filter-category-primary option:selected').data('url');
				if (!url) {
					url = form.find('select.cma-filter-category option:selected').data('url');
					if (!url) {
						url = form.attr('action');
					}
				}
			}
			performRequest(
				form.parents('.cma-questions-widget'),
				url,
				data
			);
			return false;
		});
	};
	
	
	
	var backlinkHandlerInit = function(container) {
		$('.cma-backlink', container).click(function() {
			var link = $(this);
			var widgetContainer = link.parents('.cma-questions-widget');
			var data = {};
			data.widgetCacheId = widgetContainer.data('widgetCacheId');
			widgetContainer.addClass('cma-loading');
			widgetContainer.append($('<div/>', {"class":"cma-loader"}));
			$.ajax({
				method: "GET",
				url: link.attr('href'),
				data: data,
				success: function(response) {
					var code = $('<div>' + response +'</div>');
					widgetContainer.html(code.find('.cma-questions-widget').html());
					initHandlers(widgetContainer);
				}
			});
			return false;
		});
	};
	
	
	var loadAnswerHandlerInit = function(container) {
		$('.cma-thread-title a', container).click(function(e) {
			
			// Allow to use middle-button to open thread in a new tab:
			if (e.which > 1 || e.shiftKey || e.altKey || e.metaKey || e.ctrlKey) return;
			
			var link = $(this);
			var container = link.parents('.cma-questions-container');
			var widgetContainer = container.parents('.cma-questions-widget');
			var data = {};
//			data.widget = 1;
			data.widgetCacheId = widgetContainer.data('widgetCacheId');
			data.backlink = widgetContainer.data('backlink');
			var commentsContainer = link.parents('#comments');
			if (commentsContainer.length) data.post_id = commentsContainer.data('postId');
			widgetContainer.addClass('cma-loading');
			widgetContainer.append($('<div/>', {"class":"cma-loader"}));
			$.ajax({
				method: "GET",
				url: link.attr('href'),
				data: data,
				success: function(response) {
					widgetContainer.html(response);
					initHandlers(widgetContainer);
				}
			});
			return false; // prevent default
		});
	};
	
	
	var postAnswerHandlerInit = function(container) {
		$('.cma-answer-form-container form', container).submit(function(ev) {
			ev.preventDefault();
			ev.stopPropagation();
			var form = $(this);
			form.addClass('.cma-loading');
			form.append($('<div/>', {'class':'cma-loader'}));
			var url = form.attr('action') + '?widgetCacheId=' + container.data('widgetCacheId');
			$.post(url, form.serialize(), function(response) {
				container.html(response);
				initHandlers(container);
				$('html,body').animate({scrollTop: container.find('.cma-messages').offset().top - 100});
			});
		});
	};
	
	
	var categoryHandlerInit = function(container) {
		$('a.cma-category-link, .cma-breadcrumbs a:first-of-type', container).click(function(e) {
				
			// Allow to use middle-button to open thread in a new tab:
			if (e.which > 1 || e.shiftKey || e.altKey || e.metaKey || e.ctrlKey) return;
			
			var link = $(this);
			var widgetContainer = link.parents('.cma-questions-widget');
			var data = {widgetCacheId: widgetContainer.data('widgetCacheId')};
			performRequest(
				widgetContainer,
				this.href,
				data
			);
			
			return false;
			
		});
	};
	
	
	initHandlers($('.cma-widget-ajax .cma-questions-widget'));
	
});
