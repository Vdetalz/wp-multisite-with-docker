(function ($) {
    function init () {
        $('.cma_tables a.delete').on('click', function (e) {
            var $this;
            $this = $(this);
            if(confirm('Are you sure? You will lose all your Questions, Answers and Settings!'))
            {
                return true;
            }
            else
            {
                e.preventDefault();
                return false;
            }
        });
    }

    $(document).ready(init);
})(jQuery);
