<div class='wrap'>
	<h2><?php _e('Add Action', 'automessage');?></h2>
	<a name='form-add-action' ></a>
	<div id="poststuff" class="metabox-holder">
		<div class="postbox">
			<h3 class="hndle" style='cursor:auto;'><span><?php _e('Add Action','automessage'); ?></span></h3>
			<div class="inside">
				<form method="post" action="?page=<?php echo $page;?>">
					<input type='hidden' name='action' value='addaction' />
					<input type='hidden' name='type' value='<?php echo $type;?>' />
					<input type='hidden' name='type' value='<?php echo $type;?>' />

					<?php wp_nonce_field('add-action');?>

					<table class="form-table">
						<tr class="form-field form-required" valign="top">
							<th style="" scope="row" valign="top"><?php _e('Action','automessage');?></th>
							<td>
								<?php if($this->hooks):?>
									<select name="hook[]" style="width: 40%;" multiple>
										<?php foreach($this->hooks as $key => $hook):?>
											<option value="<?php echo $key;?>"><?php echo $hook;?></option>
										<?endforeach;?>
									</select>
								<?php endif;?>
							</td>
						</tr>

						<tr class="form-field form-required" valign="top">
							<th style="" scope="row" valign="top"><?php _e('Performed once','automessage');?></th>
							<td>
								<input type="checkbox" name="use_once" value="1"/>
							</td>
						</tr>

						<tr class="form-field form-required" valign="top">
							<th style="" scope="row" valign="top"><?php _e('From Registered Date','automessage');?></th>
							<td>
								<input type="checkbox" name="from_registered" value="1"/>
							</td>
						</tr>

						<tr class="form-field form-required" valign="top">
							<th style="" scope="row" valign="top"><?php _e('Send to Students','automessage');?></th>
							<td>
								<input type="checkbox" name="students_send"/>
							</td>
						</tr>

						<tr class="form-field form-required">
							<th style="" scope="row" valign="top"><?php _e('Message delay','automessage');?></th>
							<td valign="top">

								<select name="period" style="width: 40%;">

									<?php for($n = 1; $n <= 31; $n++):?>

										<option value='<?php echo $n;?>'>

											<?php switch($n) {


												case 1:
													echo __("1 day", 'automessage');
													break;

												default:
													echo sprintf(__('%d days','automessage'), $n);
											}
											?>

										</option>

									<?php endfor;?>

								</select>



							</td>

						</tr>

						<tr class="form-field form-required">
							<th style="" scope="row" valign="top"><?php _e('Message Subject','automessage');?></th>
							<td valign="top"><input name="subject" type="text" size="50" title="<?php _e('Message subject');?>" style="width: 50%;" /></td>
						</tr>

						<tr class="form-field form-required">

							<th style="" scope="row" valign="top"><?php _e('Message','automessage');?></th>
							<td valign="top">
								<textarea name="message" style="width: 50%; float: left;" rows="15" cols="40"></textarea>

								<div class="instructions" style="float: left; width: 40%; margin-left: 10px;">

									<?php echo __('You can use the following constants within the message body to embed database information.','automessage');?>

									<br /><br />
									%blogname%<br />
									%blogurl%<br />
									%username%<br />
									%usernicename%<br/>
									%sitename%<br/>
									%siteurl%<br/>
									%usernickname%<br/>

								</div>

							</td>

						</tr>

					</table>

					<p class="submit">
						<input class="button-primary" type="submit" name="go" value="<?php _e('Add action', 'automessage');?>" /></p>
				</form>

			</div>
		</div>

	</div>