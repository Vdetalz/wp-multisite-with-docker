<div class='wrap'>

	<h2><?php _e('Add Action', 'automessage');?></h2>

	<a name='form-add-action' ></a>

	<div id="poststuff" class="metabox-holder">

		<div class="postbox">
			<h3 class="hndle" style='cursor:auto;'><span><?php _e('Add Action','automessage'); ?></span></h3>
			<div class="inside">

				<form method="post" action="?page=<?php ECHO $page;?>">
					<input type='hidden' name='action' value='addaction' />
					<input type='hidden' name='type' value='<?php echo $type;?>' />
					<input type='hidden' name='type' value='<?php echo $type;?>' />

					<?php wp_nonce_field('add-action');?>

					<table class="form-table">
						<tr class="form-field form-required" valign="top">
							<th style="" scope="row" valign="top"><?php _e('Action','automessage');?></th>
							<td>

								<select name="hook" style="width: 40%;" multiple>
									<option value="wpmu_new_user"><?php _e('Create new user','automessage');?></option>
									<option value="wpmu_all_access_expired"><?php _e('All Access Pass has expired','automessage');?></option>
									<option value="wpmu_subscribed_to_mct"><?php _e('Is currently subscribed to My Chinese Tutor subscription','automessage');?></option>
									<option value="wpmu_subscribed_to_mch"><?php _e('Is currently subscribed to My Chinese Hub subscription','automessage');?></option>
									<option value="wpmu_has_subscribed_to_mct"><?php _e('Has subscribed to My Chinese Tutor subscription in the past','automessage');?></option>
									<option value="wpmu_has_subscribed_to_mch"><?php _e('Has subscribed to My Chinese Hub subscription in the past','automessage');?></option>
								</select>

							</td>
						</tr>

						<tr class="form-field form-required">
							<th style="" scope="row" valign="top"><?php _e('Message delay','automessage');?></th>
							<td valign="top">
								<select name="period" style="width: 40%;">
									<?php for($n = 0; $n <= AUTOMESSAGE_POLL_MAX_DELAY; $n++): ?>
										<option value='<?php echo $n;?>'>
											<?php switch($n) {
												case 0:
													echo __("Send immediately", 'automessage');
													break;

												case 1:
													echo __("1 day", 'automessage');
													break;

												default:
													echo sprintf(__('%d days','automessage'), $n);
											} ?>
										</option>
									<?php endfor;?>
								</select>
								<input type="hidden" name="timeperiod" value="day" />
							</td>

						</tr>

						<tr class="form-field form-required">
							<th style="" scope="row" valign="top"><?php _e('Message Subject','automessage');?></th>
							<td valign="top"><input name="subject" type="text" size="50" title="<?php _e('Message subject');?>" style="width: 50%;" /></td>
						</tr>

						<tr class="form-field form-required">

							<th style="" scope="row" valign="top"><?php _e('Message','automessage');?></th>
							<td valign="top">
								<textarea name="message" style="width: 50%; float: left;" rows="15" cols="40"></textarea>

								<div class="instructions" style="float: left; width: 40%; margin-left: 10px;">

									<?php echo __('You can use the following constants within the message body to embed database information.','automessage');?>

									<br /><br />
									%blogname%<br />
									%blogurl%<br />
									%username%<br />
									%usernicename%<br/>
									%sitename%<br/>
									%siteurl%<br/>
									%usernickname%<br/>

								</div>
							</td>
						</tr>
					</table>

					<p class="submit">
						<input class="button-primary" type="submit" name="go" value="<?php _e('Add action', 'automessage');?>" /></p>
				</form>

			</div>
		</div>

	</div>
<?php