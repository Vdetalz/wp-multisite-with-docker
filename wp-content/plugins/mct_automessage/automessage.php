<?php
/*
Plugin Name: MCT Automessage
Description: This plugin allows emails to be scheduled and sent to new users.
Author: WEB4PRO
Version: 1.0
Author URI: http://web4pro.net
*/


global $automessage_basename;
$automessage_basename = plugin_basename(__FILE__);

// Load the libraries
require_once('includes/config.php');
require_once('includes/functions.php');
require_once('classes/class.automessage.php');
require_once('classes/class.user.php');

// Set up our location
set_automessage_url(__FILE__);
set_automessage_dir(__FILE__);


// Instantiate the class
$automsg = new automessage();