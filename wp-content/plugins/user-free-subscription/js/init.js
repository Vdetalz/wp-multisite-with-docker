jQuery(document).ready(function ($) {
    $(function() {
        $('.prod-exp-date input').datepicker();
    });

    $('.dwn-prod-wrapper').slice(1).hide();
    $('.prod-price-id').hide();

    $('.add-product').on('click', function(){
        $('.dwn-prod-wrapper').each(function(){
            if($(this).is(':hidden')){
                $(this).show();
                return false;
            }
        });
    });

    $('.dwn-single-product select').each(function(){
        if($(this).find(':selected').val()){
            $(this).parents().eq(1).show();
        }
    });

    $('.prod-price-id select').each(function(){
        if($(this).find(':selected').val()){
            $(this).parent().show();
        }
    });

    $('.dwn-single-product select').on('change', function(){
        var changed_element = $(this);
        var product_id = changed_element.find(':selected').val();
        $.ajax({
            type: 'post',
            url: Ajax.ajaxurl,
            data: {
                action: 'get_price_id',
                download_id: product_id
            },
            beforeSend:function(){
                changed_element.parents().eq(1).find('.prod-price-id select option').each(function(){
                    $(this).remove();
                });
            },
            success: function(data){
                changed_element.parents().eq(1).find('.prod-price-id').show();
                changed_element.parents().eq(1).find('.prod-price-id select').append(data);
            }
        });
    });

    $('.dwn-prod-wrapper').closest('form').on('submit', function(e){
        $('.dwn-prod-wrapper').each(function(){
            if($(this).is(':hidden')){
                $(this).remove();
            }
            if(!$(this).find('.dwn-single-product :selected').val()){
                $(this).remove();
            }
        });
        $(this).submit();
    });
});