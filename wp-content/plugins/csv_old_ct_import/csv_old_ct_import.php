<?php
/*
  Plugin Name: CSV Users Import from old MCT site
  Description:
  Author: Web4pro
  Version: 0.1
 */


add_action('admin_menu', 'mct_import_menu');

function mct_import_menu()
{
    add_menu_page('MCT Users Import', 'MCT Users Import', 'manage_options', 'mct-users-import', 'mct_users_import_options');
    add_submenu_page('mct-users-import', 'MCT Users Update logs', 'MCT Users Update logs', 'manage_options', 'mct-users-update-logs', 'mct_users_update_logs');
    add_submenu_page('mct-users-import', 'MCT Users Update role', 'MCT Users Update role', 'manage_options', 'mct-users-update-role', 'mct_users_update_role');
}

function mct_users_import_options()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    if (isset($_FILES['csv_import']) && !empty($_FILES['csv_import'])) {
        //parse_csv_file();
        parse_csv_file_updated();
    }
    ?>
    <h2><?php echo __('Please, choose .csv file'); ?></h2>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
        <input type="file" value="<?php echo __('Import'); ?>" accept=".csv" name="csv_import">

        <p><input type="submit" value="<?php echo __('Import'); ?>">

        <p>
    </form>
<?php
}

function parse_csv_file()
{
    $file = file_get_contents($_FILES['csv_import']['tmp_name']);

    $rows = str_getcsv($file, "\n");

    foreach ($rows as $row) {
        if ($row != reset($rows)) {
            $elements = str_getcsv($row, ",");

            $old_user_id = $elements[0];
            $user_first_name = $elements[1];
            $user_last_name = $elements[2];
            $user_nickname = $elements[3];
            $user_registered = $elements[4];
            $user_email = $elements[5];
            $user_role = $elements[6];
            $user_balance = $elements[7];
            $user_password = wp_generate_password(8, false);

            if (null == username_exists($user_email)) {
                $user_id = wp_create_user($user_email, $user_password, $user_email);

                // Set the nickname
                wp_update_user(
                    array(
                        'ID' => $user_id,
                        'nickname' => $user_nickname,
                        'first_name' => $user_first_name,
                        'last_name' => $user_last_name
                    )
                );
                $user = new WP_User($user_id);
                $user->set_role('subscriber');

                set_new_user_subscription($user_id, $user_balance);
                send_accesses_email($user_email, $user_first_name, $user_last_name, $user_nickname, $user_password);

            }
        }
    }
}

function set_new_user_subscription($user_id, $balance)
{
    global $wpdb;

    $download_id = 520;

    $end_timestamp = time() + 47335392; //add 18 month

    //If user is not loginned
    if (!$user_id && !get_current_user_id()) return false;

    //if user id is empty
    if (!$user_id)
        $user_id = get_current_user_id();

    $user = new WP_User($user_id);

    //If have no timestamp of ending subscription than give unlimited access
    if (!$end_timestamp)
        $end_timestamp = 999999999999;

    //Dance with each download ID
    if ($download_id && !empty($download_id)) {

        $download = edd_get_download($download_id);

        $temp_prices = edd_get_variable_prices($download_id);

        foreach ($temp_prices as $temp_key => $temp_value)
            $temp_array_prices[] = $temp_key;

        //Max price ID
        $price_id = max($temp_array_prices);

        unset($temp_array_prices);

        //If no price id in product
        if (!$price_id) return false;

        //Insert expiration date for product (download)
        $insert = $wpdb->insert(
            $wpdb->prefix . 'users_hours_balance',
            array(
                'user_id' => $user_id,
                'download_id' => $download_id,
                'expired_date' => $end_timestamp,
                'balance' => $balance,
                'price_id' => $price_id
            )
        );

        $period = mcn_get_normalized_period_array($download_id);

        if ($insert) {
            $purchase_downloads_temp = array(
                'id' => $download_id,
                'quantity' => 1,
                'options' => array(
                    'price_id' => $price_id,
                    'recurring' => array(
                        'period' => $period[count($period) - 1],
                        'times' => 0,
                        'signup_fee' => 0
                    ),
                ),

            );

            $purchase_downloads[] = $purchase_downloads_temp;

            $cart_details[] = array(
                'name' => $download->post_title,
                'id' => $download_id,
                'item_number' => $purchase_downloads_temp,
                'item_price' => edd_get_download_price($download_id),
                'quantity' => 1,
                'discount' => 0,
                'subtotal' => edd_get_download_price($download_id),
                'tax' => 0,
                'fees' => array(),
                'price' => edd_get_download_price($download_id)

            );

        }
    }

    if (count($purchase_downloads)) {

        $payment_id = edd_insert_payment(
            array(
                'price' => 0,
                'status' => 'publish',
                'user_email' => $user->user_email,
                'purchase_key' => strtolower(md5($user->user_email . date('Y-m-d H:i:s') . AUTH_KEY . uniqid('edd', true))),
                'currency' => 'USD',
                'downloads' => $purchase_downloads,
                'user_info' => array(
                    'id' => $user->ID,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->user_email,
                    'discount' => 'none',
                    'address' => false,
                ),
                'cart_details' => $purchase_downloads,
            )
        );

    } else {
        return false;
    }

    $wpdb->delete(
        $wpdb->prefix . 'users_hours_balance',
        array(
            'user_id' => $user_id,
            'download_id' => 0,
            'price_id' => 0
        )
    );

    return true;
}

function send_accesses_email($user_email, $user_first_name, $user_last_name, $user_nickname, $user_password)
{
    require_once(plugin_dir_path(__FILE__) . 'phpmailer/PHPMailerAutoload.php');
    $admin_email = get_option('admin_email');
    $Letter = new PHPMailer();
    $Letter->isHTML(true);

    if ($admin_email && !empty($admin_email)) {
        $Letter->From = $admin_email;
    } else {
        $Letter->From = 'admin@mychinesetutur.org';
    }

    $Letter->FromName = 'MCT_Admin';
    $Letter->addAddress($user_email, $user_first_name . ' ' . $user_last_name);
    $Letter->Subject = __('My Chinese Tutor. New site announce!', 'mct_hub');

    $Letter->Body = '<p>' . __('Hello , ') . $user_nickname . '<br />';

    $Letter->Body .= __('We are very excited to announce the launch of our new website!') . '</p>';
    $Letter->Body .= '<p>' . __('We have moved to a subscription model which allows you to get Chinese lessons at MUCH cheaper prices - from less than $10 per lesson!') . '</p>';
    $Letter->Body .= __('Your new access is:') . '<br />';
    $Letter->Body .= __('Login: ') . $user_email . '<br />';
    $Letter->Body .= __('Password: ') . $user_password . '</p>';
    $Letter->Body .= '<p>' . __('If you have any questions or concerns, please don\'t hesitate to contact us on info@myeducationgroup.org') . '</p>';
    $Letter->Body .= '<p>' . __('Happy Chinese learning!') . '<br />';
    $Letter->Body .= __('The My Chinese Tutor team') . '</p>';

    if (!$Letter->send()) {
        echo 'Message could not be sent to ' . $user_email;
        echo 'Mailer Error: ' . $Letter->ErrorInfo;
    } else {
        echo 'Message has been sent to ' . $user_email;
    }
}

function mct_users_update_logs()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if (isset($_FILES['csv_update']) && !empty($_FILES['csv_update'])) {
        update_user_logs();
    }
    ?>
    <h2><?php echo __('Please, choose .csv file'); ?></h2>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
        <input type="file" accept=".csv" name="csv_update">

        <p><input type="submit" value="<?php echo __('Update'); ?>">

        <p>
    </form>
<?php
}

function update_user_logs()
{
    global $wpdb;
    $file = file_get_contents($_FILES['csv_update']['tmp_name']);

    $rows = str_getcsv($file, "\n");

    foreach ($rows as $row) {
        if ($row != reset($rows)) {

            $elements = str_getcsv($row, ",");
            $user_email = $elements[5];
            $user_balance = $elements[7];

            $user_obj = get_user_by('email', $user_email);
            $user_id = $user_obj->data->ID;

            if (isset($user_id) && !empty($user_id)) {
                $table_name = $wpdb->prefix . 'users_logs_info';
                $date = '2015-04-14 11:00:00';
                $action = 'ImportedFromOldSite';

//                $select_sql = $wpdb->prepare("SELECT * FROM $table_name WHERE `user_id` = %d AND `action` = %s", $user_id, $action);
//                $select = $wpdb->get_row($select_sql);
//
//                if (!$select || empty($select)) {
//                    $update = $wpdb->insert(
//                        $table_name,
//                        array(
//                            'date' => $date,
//                            'action' => $action,
//                            'change_balance' => $user_balance,
//                            'balance' => $user_balance,
//                            'user_id' => $user_id,
//                            'delivery_method' => ''
//                        )
//                    );
//
//                    $delete_sql = $wpdb->prepare("DELETE FROM $table_name WHERE `user_id` = %d AND `action` = %s ", $user_id, 'User Registered');
//
//                    $delete = $wpdb->query($delete_sql);



                    if (update_user_meta($user_id, 'tutor_trial_lesson', false)) {
                        echo $user_email . ' log was updated <br />';
                  //  }

                }
            }
        }
    }
}

function mct_users_update_role(){
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if (isset($_FILES['csv_update_role']) && !empty($_FILES['csv_update_role'])) {
        update_user_role();
    }
    ?>
    <h2><?php echo __('Please, choose .csv file'); ?></h2>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
        <input type="file" accept=".csv" name="csv_update_role">

        <p><input type="submit" value="<?php echo __('Update'); ?>">

        <p>
    </form>
<?php
}

function update_user_role(){
    global $wpdb;
    $file = file_get_contents($_FILES['csv_update_role']['tmp_name']);

    $rows = str_getcsv($file, "\n");

    foreach ($rows as $row) {
        $elements = str_getcsv($row, ",");
        $user_email = $elements[9];
        switch_to_blog(2);
        $user_obj = get_user_by('email', $user_email);
        $user_id = $user_obj->data->ID;
        update_user_meta($user_id, 'ch_2_capabilities', array('students' => true));

        restore_current_blog();
    }
}

/**
 * Multiply add users with new fields.
 */
function parse_csv_file_updated()
{
    $file = file_get_contents( $_FILES['csv_import']['tmp_name'] );

    $rows = str_getcsv( $file, "\n" );

    foreach ( $rows as $row ) {
        if ( $row != reset($rows ) ) {
            $elements = str_getcsv( $row, "," );

            $user_location = $elements[0];
            $user_school  = getSchool( $elements[1] );
            $user_yearlevel = $elements[2];
            $user_first_name = $elements[3];
            $user_last_name = $elements[4];
            $user_nickname = $elements[5];
            $user_email = $elements[6];
            $user_password = $elements[7];
            $user_role = strtolower( $elements[8] );
            $user_country = $elements[10];

            if ( null == username_exists( $user_email ) ) {
                $user_id = wp_create_user( $user_email, $user_password, $user_email );

                // Set the nickname
                wp_update_user(
                  array(
                    'ID' => $user_id,
                    'nickname' => $user_nickname,
                    'first_name' => $user_first_name,
                    'last_name' => $user_last_name
                  )
                );
                $user = new WP_User( $user_id );
                $user->set_role( $user_role );

                set_new_user_subscription( $user_id, 0 );

                update_user_meta( $user_id, 'country', $user_country );
                update_user_meta( $user_id, 'location', $user_location );
                update_user_meta( $user_id, 'school', $user_school );
                update_user_meta( $user_id, 'year_level', $user_yearlevel );
                update_user_meta( $user_id, 'ch_2_capabilities', array( 'students' => true ) );
                update_user_meta( $user_id, 'ch_5_capabilities', array( 'students' => true ) );
                update_user_meta( $user_id, 'ch_7_capabilities', array( 'students' => true ) );
            }
        }
    }
}

/**
 * Returns formatted school.
 *
 * @param string $school
 *
 * @return string
 */
function getSchool( $school ) {
    $search = array( ' ', ',' );
    $replace = array( '_', '_' );
    $result = str_replace( $search , $replace , $school);
    return $result;
}
