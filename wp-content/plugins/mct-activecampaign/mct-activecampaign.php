<?php
/*
Plugin Name: MCT Active Campaign
Plugin URI:
Description: Provides access to ActiveCampaign for MCT.
Author: Web4pro
Version: 1.0
Author URI: http://web4pro.net
*/

defined('USER_HAVE_TRIAL_LESSON') || define('USER_HAVE_TRIAL_LESSON', 'user_have_trial_lesson');

/**
 * Register Scheduler.
 */
register_activation_hook(__FILE__, 'mct_activecampaign_activation');
function mct_activecampaign_activation() {
    wp_clear_scheduled_hook( 'mct_activecampaign_event' );
    wp_schedule_event( time(), 'daily', 'mct_activecampaign_event');
}

/**
 * Callback function for detect trial completed lessons.
 */
if( defined('DOING_CRON') && DOING_CRON ) {
    add_action('mct_activecampaign_event', 'mct_activecampaign_event_callback');
    function mct_activecampaign_event_callback() {
        $instance = mct_activecampaign_get_active_campaign_instance();
        if ($instance === null) {
            return;
        }
        $users = get_users(array(
            'role' => 'subscriber',
            'meta_key' => USER_HAVE_TRIAL_LESSON,
            'meta_value' => 'true'
        ));
        foreach($users as $user) {
            $subscription = get_user_meta( $user->ID, 'tutor_trial_lesson', true );
            if (!$subscription && is_user_have_completed_trial_lesson($user->ID)) {
                $instance->api('contact/tag/add', array(
                    'email' => $user->user_email,
                    'tags' => 'MCTutorTrialCompleted'
                ));
                delete_user_meta( $user->ID, USER_HAVE_TRIAL_LESSON);
            }
        }
    }
}

/**
 * Clean Scheduler.
 */
register_deactivation_hook( __FILE__, 'mct_activecampaign_deactivation');
function mct_activecampaign_deactivation() {
    wp_clear_scheduled_hook('mct_activecampaign_event');
}

add_action('wpmu_activate_user', 'mct_activecampaign_registration', 10, 3);
/**
 * Callback function for user registration.
 * @param $user_id
 */
function mct_activecampaign_registration($user_id, $password, $meta) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    update_user_meta($user_id, USER_HAVE_TRIAL_LESSON, 'true');
    $instance->api('contact/add', array(
        'email' => $meta['user_email'],
        'first_name' => $meta['user_name'],
        'tags' => 'MCTutorRegistered'
    ));
}

add_action('take_trial_lesson', 'mct_activecampaign_take_trial_lesson', 10, 2);
/**
 * Callback function for event: taking trial lesson.
 * @param EM_Booking $EM_Booking
 * @param int $user_id
 */
function mct_activecampaign_take_trial_lesson($user_id, $EM_Booking) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    $user = get_userdata($user_id);
    $instance->api('contact/tag/add', array(
        'email' => $user->user_email,
        'tags' => 'MCTutorTrialBooked'
    ));
}

add_action('trial_lesson_completed', 'mct_activecampaign_complete_trial_lesson', 10, 2);
/**
 * Callback function for event: complete trial lesson.
 * @param int $user_id
 */
function mct_activecampaign_complete_trial_lesson($user_id) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    $user = get_userdata($user_id);
    $instance->api('contact/tag/add', array(
        'email' => $user->user_email,
        'tags' => 'MCTutorTrialCompleted'
    ));
}

add_action('edd_complete_purchase', 'mct_activecampaign_subscribe');
/**
 * Callback function for event: user subscription.
 * @param int $payment_id
 */
function mct_activecampaign_subscribe($payment_id) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    $payment_data = edd_get_payment_meta($payment_id);
    $user_id = $payment_data['user_info']['id'];
    $user = get_userdata($user_id);
    $instance->api('contact/tag/add', array(
        'email' => $user->user_email,
        'tags' => 'MCTutorSubscribed'
    ));
}

add_action('mct_user_unsubscribe', 'mct_activecampaign_unsubscribe');
/**
 * Callback function for event: admin user unsubscription.
 * @param int $user_id
 */
function mct_activecampaign_unsubscribe($user_id) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    $user = get_userdata($user_id);
    $instance->api('contact/tag/add', array(
        'email' => $user->user_email,
        'tags' => 'MCTutorUnSubscribed'
    ));
}

add_action('edd_payment_delete', 'mct_activecampaign_payment_delete');
/**
 * Callback function for event: user payment delete.
 * @param int $payment_id
 */
function mct_activecampaign_payment_delete($payment_id) {
    $instance = mct_activecampaign_get_active_campaign_instance();
    if ($instance === null) {
        return;
    }
    $payment_data = edd_get_payment_meta($payment_id);
    $user_id = $payment_data['user_info']['id'];
    $user = get_userdata($user_id);
    $instance->api('contact/tag/add', array(
        'email' => $user->user_email,
        'tags' => 'MCTutorUnSubscribed'
    ));
}


/**
 * Get wrapper class for ActiveCampaign entity - Contact.
 * @return ActiveCampaign|null
 */
function mct_activecampaign_get_active_campaign_instance() {
    require_once("includes/ActiveCampaign.class.php");
    $instance = get_option("settings_activecampaign");
    if (empty($instance["api_url"]) || empty($instance["api_key"])) {
        return null;
    }
    return new ActiveCampaign($instance["api_url"], $instance["api_key"]);
}

/**
 * Check if user have completed trial lesson.
 *
 * @param int $user_id
 * @return bool
 */
function is_user_have_completed_trial_lesson($user_id) {
    global $wpdb;
    $bookings_table = $wpdb->prefix.'em_bookings';
    $events_table = $wpdb->prefix.'em_events';
    $meta_table = defined('EM_META_TABLE') ? EM_META_TABLE : $wpdb->prefix.'em_meta';
    $meta_key = defined('IS_TRIAL_LESSON_KEY') ? IS_TRIAL_LESSON_KEY : 'is_trial_lesson';
    $query = "SELECT booking_id FROM $bookings_table AS b
                                   LEFT JOIN $events_table AS e
                                   ON b.event_id = e.event_id
                                   LEFT JOIN $meta_table AS m
                                   ON b.booking_id = m.object_id
                                   WHERE b.person_id = $user_id
                                   AND m.meta_key = '$meta_key'
                                   AND e.event_end_date < '" . date('Y-m-d', dut_prepare_date(date('Y-m-d'), -8)) . "'";
    $results = $wpdb->get_results($query, ARRAY_A);

    return !empty($results);
}