Projet Epex
=============

The project is a Wordpress multisite based application which is developped in a docker environment.

Why ?
-----
EPEX SPOT SE est une bourse de l’électricité au comptant (spot) pour l'Allemagne, la France,
la Grande Bretagne, les Pays-Bas, la Belgique, l'Autriche, la Suisse et le Luxembourg.

Requirements
------------

* Serveur Apache
* PHP 7.2
* Mysql 5.7
* Docker 17 (Development)

Usage
-----

#### Cron

/


#### CLI

/


#### Running Tests

/


Installation
------------

## git clone docker
```
git clone https://vishchenko@bitbucket.org/vishchenko/mctdocker.git
```

## git clone project
```
cd app/
git clone https://path@to.your/repo .
```

## Initialization from docker host
cd ..

1. Turn on your docker
```
docker-compose up -d
```

2. Import database from host
```
docker exec -i mctdockerloc_mysql_1 mysql mctdocker_db -uroot -proot --default-character-set=utf8 < app/mctdocker_db.sql
```

3. Run project config init script (shell)
```
/

```

4. Set project domain to your hosts

```
127.0.0.1 mctdocker.loc
```

5. Test installation with your web browser
```
mctdocker.loc
```

6. Access to the site on your browser locally
```
Open the browser : `http://mctdocker.loc/`

PhpMyAdmin accessible : `http://mctdocker.loc:8001/`
```

Configuration
-------------
1. Global setsings
settings file wp-config.php in ./


```
define('DB_NAME', database.name)
```
```
define('DB_USER', database.user);
```
```
define('DB_PASSWORD', database.password);
```
```
define('DB_HOST', database.host:port);

#Example: define('DB_HOST', '0.0.0.0:3309')

#if not working run - docker inspect mctdockerloc_mysql_1
#find "Networks:{mctdockerloc_default: IPAddress: XXX.XX.X.X"
#define('DB_HOST', 'XXX.XX.X.X');
```
```
define('DOMAIN_CURRENT_SITE', 'domain');
define( 'WP_SITEURL', domain );
define( 'WP_HOME', domain );
```

Troubleshooting
---------------
/

FAQ
---
/

Deploy
-----------
/

Documentation
-------------

# Docker operations
## Launch containers
```
docker-compose up -d
```

## Docker drupal connect
```
docker-compose exec drupal bash
```

## Stop containers
```
docker-compose stop
```

## Inspect container
```
docker inspect container.name
```
